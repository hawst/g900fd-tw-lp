.class Lcom/sec/chaton/chat/ay;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 7616
    iput-object p1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8544
    if-ne p1, v10, :cond_3

    .line 8546
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setChoiceMode(I)V

    .line 8551
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-nez v0, :cond_1

    .line 8552
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 8553
    const-string v0, "DeleteMessageObserver_QUERY_MESSAGE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8555
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->v(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/v;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v4, v4, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v4, v6, v1

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 8560
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->clearChoices()V

    .line 8570
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->L(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8571
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aE(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 8587
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/16 v4, 0xb

    invoke-static {}, Lcom/sec/chaton/e/v;->b()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no = ? AND ( message_type = ? OR message_type = ? ) AND message_content_type != ?"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v7, v7, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v7, v6, v1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v8

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v9

    sget-object v1, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v10

    const-string v7, "message_is_failed , message_time , _id"

    move v1, v4

    move-object v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 8592
    :cond_3
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 8522
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 14

    .prologue
    .line 7620
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 7621
    const-string v1, "onQueryCompleted - Activity is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7622
    if-eqz p3, :cond_0

    .line 7623
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 8517
    :cond_0
    :goto_0
    return-void

    .line 7628
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ak(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7629
    if-eqz p3, :cond_0

    .line 7630
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto :goto_0

    .line 7634
    :cond_2
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 7636
    :pswitch_1
    const-string v1, "onQueryCompleted - QUERY_MESSAGE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7637
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->V(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 7638
    const-string v1, "onQueryCompleted - QUERY_MESSAGE : isQueryForMark true"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7639
    if-eqz p3, :cond_0

    .line 7640
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 7656
    :cond_3
    if-eqz p3, :cond_5

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 7658
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 7659
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7660
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "message_session_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 7663
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 7664
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 7665
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToLast()Z

    .line 7666
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    .line 7668
    const-string v2, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    const-string v5, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_8

    .line 7669
    const-string v2, "onQueryCompleted - QUERY_MESSAGE : update message"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7683
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7684
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 7688
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/ei;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 7689
    if-eqz v1, :cond_6

    .line 7690
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7693
    :cond_6
    if-eqz p3, :cond_9

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_9

    .line 7697
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->e(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 7698
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-eqz v1, :cond_7

    .line 7699
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 7702
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 7723
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    goto/16 :goto_0

    .line 7671
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->b(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 7672
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    const/16 v2, 0x14

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->v(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v4

    invoke-static {v4}, Lcom/sec/chaton/e/v;->a(I)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "message_inbox_no=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v9, v9, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7675
    const-string v1, "onQueryCompleted - QUERY_MESSAGE : inserted message, query QUERY_MESSAGE_PREVIEW"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7676
    if-eqz p3, :cond_0

    .line 7677
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 7711
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 7713
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v1, :cond_a

    .line 7715
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->w(Lcom/sec/chaton/chat/ChatFragment;)V

    goto :goto_1

    .line 7717
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto :goto_1

    .line 7728
    :pswitch_2
    const-string v1, "onQueryCompleted - QUERY_MESSAGE_PREVIEW"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7729
    if-eqz p3, :cond_b

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_b

    .line 7730
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 7731
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7732
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "message_session_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 7736
    :cond_b
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/ei;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 7737
    if-eqz v1, :cond_c

    .line 7738
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7741
    :cond_c
    if-eqz p3, :cond_e

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_e

    .line 7742
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->e(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 7743
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-eqz v1, :cond_d

    .line 7744
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 7747
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto/16 :goto_0

    .line 7749
    :cond_e
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 7751
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v1, :cond_f

    .line 7753
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->w(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 7755
    :cond_f
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto/16 :goto_0

    .line 7763
    :pswitch_3
    const-string v1, "onQueryCompleted - QUERY_MESSAGE_UNREAD_MARK"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7764
    if-eqz p3, :cond_0

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 7765
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7766
    const-string v1, "message_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 7767
    const-string v1, "message_content"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 7770
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 7771
    const-string v1, "%d,%s,%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v7, Lcom/sec/chaton/e/aj;->g:Lcom/sec/chaton/e/aj;

    invoke-virtual {v7}, Lcom/sec/chaton/e/aj;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v3

    const/4 v3, 0x1

    const-string v7, "mark"

    aput-object v7, v2, v3

    const/4 v3, 0x2

    const-string v7, "mark"

    invoke-static {v7}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7773
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v7, 0x1

    sub-long/2addr v5, v7

    invoke-static/range {v1 .. v6}, Lcom/sec/chaton/e/a/q;->a(Lcom/sec/chaton/e/a/u;ILjava/lang/String;Ljava/lang/String;J)J

    .line 7774
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->h(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto/16 :goto_0

    .line 7778
    :pswitch_4
    const-string v1, "onQueryCompleted - QUERY_MESSAGE_FOR_MARK"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7779
    if-eqz p3, :cond_10

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_10

    .line 7781
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    if-nez v1, :cond_10

    .line 7782
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7783
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "message_session_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 7787
    :cond_10
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/ei;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 7788
    if-eqz v1, :cond_11

    .line 7789
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7792
    :cond_11
    if-eqz p3, :cond_12

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_12

    .line 7793
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->e(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 7795
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/chaton/chat/az;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/az;-><init>(Lcom/sec/chaton/chat/ay;)V

    const-wide/16 v3, 0x514

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 7817
    :cond_12
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 7818
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->i(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 7819
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->h(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto/16 :goto_0

    .line 7825
    :pswitch_5
    const-string v1, "onQueryCompleted - QUERY_MESSAGE_LOAD_MORE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7831
    if-eqz p3, :cond_13

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_13

    .line 7833
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    if-nez v1, :cond_13

    .line 7834
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7835
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "message_session_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 7839
    :cond_13
    if-eqz p3, :cond_19

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_19

    .line 7840
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 7841
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->al(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v2

    sub-int v2, v1, v2

    .line 7842
    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3, v1}, Lcom/sec/chaton/chat/ChatFragment;->e(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 7844
    if-lez v2, :cond_14

    .line 7845
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/ei;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 7846
    if-eqz v1, :cond_14

    .line 7847
    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7853
    :cond_14
    if-nez v2, :cond_15

    .line 7854
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->w(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 7859
    :cond_15
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-eqz v1, :cond_17

    .line 7860
    if-lez v2, :cond_16

    .line 7861
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 7885
    :cond_16
    :goto_2
    const/16 v1, 0x13

    if-ne p1, v1, :cond_1b

    .line 7886
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->an(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 7865
    :cond_17
    if-lez v2, :cond_16

    .line 7866
    const/16 v1, 0x1e

    if-ge v2, v1, :cond_18

    .line 7867
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    goto :goto_2

    .line 7869
    :cond_18
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    goto :goto_2

    .line 7875
    :cond_19
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/ei;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 7876
    if-eqz v1, :cond_1a

    .line 7877
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7880
    :cond_1a
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    goto :goto_2

    .line 7887
    :cond_1b
    const/16 v1, 0xe

    if-ne p1, v1, :cond_0

    .line 7888
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->c(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto/16 :goto_0

    .line 7894
    :pswitch_6
    const-string v1, "onQueryCompleted - QUERY_INBOX"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7899
    if-eqz p3, :cond_1c

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_24

    .line 7903
    :cond_1c
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 7904
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    .line 7909
    :cond_1d
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ao(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 7910
    if-eqz p3, :cond_0

    .line 7911
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 7920
    :cond_1e
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 7923
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/o;->c(J)V

    .line 7927
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->l()Z

    move-result v1

    if-nez v1, :cond_1f

    .line 7928
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->k()V

    .line 7932
    :cond_1f
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ap(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 7933
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/chaton/e/i;->f()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_no IN"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v7}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/chaton/chat/eq;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7934
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7935
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 7936
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/chaton/chat/ChatFragment;->u:Z

    .line 7941
    if-eqz p3, :cond_20

    .line 7943
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7947
    :cond_20
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_22

    .line 7948
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 7952
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/chat/MyEditText;->getLineHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f99999a    # 1.2f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 7955
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aq(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 7957
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 7959
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 7960
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/sec/chaton/chat/MyEditText;->setSelection(I)V

    .line 7966
    :cond_21
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Setting alert_new_groupchat"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->j(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 7970
    :cond_22
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 7981
    :cond_23
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->as(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 7934
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 7974
    :cond_24
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->b(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7975
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ar(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 7976
    if-eqz p3, :cond_23

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_23

    .line 7977
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto :goto_3

    .line 7985
    :pswitch_7
    if-eqz p3, :cond_25

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2b

    .line 7986
    :cond_25
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->at(Lcom/sec/chaton/chat/ChatFragment;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "receivers"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 7987
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ap(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 7988
    :try_start_2
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->at(Lcom/sec/chaton/chat/ChatFragment;)Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "receivers"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v4, :cond_26

    aget-object v5, v3, v1

    .line 7989
    iget-object v6, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v6}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7990
    iget-object v6, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v6}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v6

    new-instance v7, Lcom/sec/chaton/a/a/a;

    invoke-direct {v7, v5}, Lcom/sec/chaton/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7988
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 7992
    :cond_26
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 7995
    :cond_27
    if-eqz p3, :cond_28

    .line 7996
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 7999
    :cond_28
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/chat/eq;->a(Ljava/util/List;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8000
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_29

    .line 8001
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_29

    .line 8002
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8005
    :cond_29
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->J(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 8085
    :goto_5
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatFragment;->h()V

    .line 8086
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->az(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 8087
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->at(Lcom/sec/chaton/chat/ChatFragment;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "receivers"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 8088
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->a([Ljava/lang/String;)V

    .line 8089
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 8091
    :cond_2a
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->m(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto/16 :goto_0

    .line 7992
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 8008
    :cond_2b
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ae(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2d

    .line 8009
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->av(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 8011
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aw(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2f

    .line 8012
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aw(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 8013
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->S(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8026
    :goto_6
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_2c

    .line 8027
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_2c

    .line 8028
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8032
    :cond_2c
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ax(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 8036
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->ay(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8037
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->k(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 8082
    :cond_2d
    :goto_7
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_5

    .line 8017
    :cond_2e
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/chat/eq;->a(Landroid/database/Cursor;Ljava/util/List;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_6

    .line 8023
    :cond_2f
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/chat/eq;->a(Landroid/database/Cursor;Ljava/util/List;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_6

    .line 8040
    :cond_30
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8041
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->J(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 8097
    :pswitch_8
    if-eqz p3, :cond_37

    .line 8098
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_31

    .line 8099
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QUERY_PARTICIPANTS_AFTER_onQueryCompleted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8105
    :cond_31
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ap(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 8108
    :try_start_4
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/chat/eq;->b(Landroid/database/Cursor;Ljava/util/List;Ljava/util/Map;)Z

    move-result v3

    .line 8109
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 8111
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_32

    .line 8112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QUERY_PARTICIPANTS_AFTER_onQueryCompleted - isParticipantsChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8115
    :cond_32
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->av(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 8121
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 8123
    :try_start_5
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v2, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    .line 8124
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v4, v4, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/a/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->h(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 8131
    :cond_33
    :goto_8
    if-eqz v3, :cond_35

    .line 8132
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8136
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_34

    .line 8137
    const-string v1, "QUERY_PARTICIPANTS_AFTER_onQueryCompleted : invalidate option "

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8139
    :cond_34
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 8143
    :cond_35
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aw(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_40

    .line 8144
    if-eqz v3, :cond_3e

    .line 8145
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aw(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 8146
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->S(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8170
    :goto_9
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8171
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->J(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8233
    :cond_36
    :goto_a
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 8241
    :cond_37
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatFragment;->h()V

    .line 8242
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->az(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 8243
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->at(Lcom/sec/chaton/chat/ChatFragment;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "receivers"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 8244
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->a([Ljava/lang/String;)V

    .line 8245
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 8247
    :cond_38
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->m(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    goto/16 :goto_0

    .line 8109
    :catchall_2
    move-exception v1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v1

    .line 8125
    :catch_0
    move-exception v1

    .line 8126
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b00b4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->h(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_8

    .line 8148
    :cond_39
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_3c

    .line 8149
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8160
    :cond_3a
    :goto_b
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->S(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3b

    .line 8161
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 8162
    const-string v1, "inbox_title"

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8163
    const-string v1, "inbox_participants"

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8164
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    const/16 v2, 0x12

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inbox_no=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v7, v7, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 8167
    :cond_3b
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->i(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_9

    .line 8151
    :cond_3c
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 8152
    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/a/a/a;

    .line 8153
    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_c

    .line 8155
    :cond_3d
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3a

    .line 8156
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_b

    .line 8177
    :cond_3e
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->S(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8182
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aw(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3f

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_3f

    .line 8183
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8196
    :cond_3f
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->X(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-nez v1, :cond_36

    .line 8197
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8198
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->J(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 8205
    :cond_40
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ae(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_44

    .line 8206
    if-eqz v3, :cond_36

    .line 8207
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_42

    .line 8208
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8222
    :cond_41
    :goto_d
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8223
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->J(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 8212
    :cond_42
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_43

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 8213
    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/a/a/a;

    .line 8214
    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_e

    .line 8216
    :cond_43
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_41

    .line 8217
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_d

    .line 8228
    :cond_44
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->ae(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8229
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->j(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_a

    .line 8250
    :pswitch_9
    if-eqz p3, :cond_46

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_46

    .line 8251
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 8253
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 8254
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "buddy_show_phone_number"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 8255
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "buddy_extra_info"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->k(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8256
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "buddy_msisdns"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8258
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_45

    .line 8259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QUERY_BUDDY_INFO - mShowPhoneNumber:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aA(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mExtraInfo:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aB(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mMsisdns:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aC(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8261
    :cond_45
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aC(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_46

    .line 8262
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_48

    .line 8263
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 8277
    :cond_46
    :goto_f
    if-eqz p3, :cond_47

    .line 8278
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 8281
    :cond_47
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aD(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 8282
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ag(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 8265
    :cond_48
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aC(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_46

    .line 8266
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aC(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->m(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 8267
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_49

    .line 8268
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;I)I

    goto :goto_f

    .line 8270
    :cond_49
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;I)I

    goto :goto_f

    .line 8285
    :cond_4a
    new-instance v1, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->V:Landroid/os/Handler;

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 8286
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/h;->f(Ljava/lang/String;Z)I

    goto/16 :goto_0

    .line 8292
    :pswitch_a
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 8295
    if-eqz p3, :cond_4c

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_4c

    .line 8296
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToLast()Z

    .line 8298
    const/4 v1, 0x0

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gt v1, v2, :cond_4b

    .line 8299
    const-string v1, "message_content"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 8300
    const-string v2, "message_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 8301
    const-string v3, "message_content_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v3

    .line 8302
    const-string v4, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 8305
    const-string v6, "message_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 8306
    const-string v7, "message_sever_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 8307
    const-string v8, "message_sender"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 8310
    const-string v9, "%d;%d;%s;%s"

    .line 8311
    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v11

    const/4 v2, 0x1

    invoke-virtual {v3}, Lcom/sec/chaton/e/w;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x2

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v2

    const/4 v1, 0x3

    invoke-static {v4}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v1

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 8313
    const-string v2, "inbox_last_message"

    invoke-virtual {v5, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8316
    const-string v1, "inbox_last_msg_no"

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8317
    const-string v1, "inbox_last_time"

    invoke-virtual {v5, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8318
    const-string v1, "inbox_last_msg_sender"

    invoke-virtual {v5, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8320
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inbox_no=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v7, v7, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 8328
    :cond_4b
    :goto_10
    if-eqz p3, :cond_0

    .line 8329
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 8324
    :cond_4c
    const-string v1, "inbox_last_message"

    const-string v2, ""

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8325
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inbox_no=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v7, v7, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_10

    .line 8362
    :pswitch_b
    if-eqz p3, :cond_0

    .line 8363
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 8369
    :pswitch_c
    if-eqz p3, :cond_4d

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_4d

    .line 8370
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 8371
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v12, v1, Lcom/sec/chaton/chat/ChatFragment;->B:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/buddy/a/c;

    const-string v2, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy_status_message"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "buddy_samsung_email"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "buddy_orginal_number"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "buddy_birthday"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "buddy_relation_hide"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Y"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4e

    const/4 v8, 0x0

    :goto_11
    const-string v9, "buddy_raw_contact_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-string v10, "buddy_push_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "buddy_is_new"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v13, "Y"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4f

    const/4 v11, 0x0

    :goto_12
    invoke-direct/range {v1 .. v11}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Z)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8375
    new-instance v2, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8376
    const-string v3, "PROFILE_BUDDY_INFO"

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->B:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Serializable;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 8377
    const/high16 v1, 0x4000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 8378
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/16 v3, 0x14

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8380
    :cond_4d
    if-eqz p3, :cond_0

    .line 8381
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 8371
    :cond_4e
    const/4 v8, 0x1

    goto :goto_11

    :cond_4f
    const/4 v11, 0x1

    goto :goto_12

    .line 8423
    :pswitch_d
    if-eqz p3, :cond_53

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_53

    .line 8424
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 8425
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 8426
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v3, "Y"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v2, v1}, Lcom/sec/chaton/chat/ChatFragment;->o(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 8427
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->L(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-nez v1, :cond_50

    .line 8429
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aE(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 8431
    :cond_50
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->L(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/ei;->a(Z)V

    .line 8435
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 8436
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_51

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_51

    .line 8437
    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-object v1, v2, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 8440
    :cond_51
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;J)J

    .line 8441
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 8442
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v2, "inbox_chat_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    .line 8445
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-nez v1, :cond_52

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aa(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 8446
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 8447
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 8450
    :cond_52
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_53

    .line 8451
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QUERY_INBOX_CHANGED - sessionId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",lasstTid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aF(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",chatType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8454
    :cond_53
    if-eqz p3, :cond_0

    .line 8455
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 8463
    :pswitch_e
    if-eqz p3, :cond_54

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_54

    .line 8465
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 8466
    const-string v1, "message_total_count"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 8467
    const-string v3, "message_sent_count"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 8468
    sub-long/2addr v1, v3

    .line 8470
    iget-object v5, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v5}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Lcom/sec/chaton/chat/ei;->a(J)V

    .line 8471
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/chat/ei;->b(J)V

    .line 8473
    :cond_54
    if-eqz p3, :cond_55

    .line 8474
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    .line 8477
    :cond_55
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->X(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-nez v1, :cond_56

    .line 8478
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatFragment;->p()V

    .line 8481
    :cond_56
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->aG(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 8488
    :pswitch_f
    if-eqz p3, :cond_57

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_57

    .line 8491
    new-instance v2, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8493
    const-string v1, "ACTIVITY_PURPOSE"

    const/4 v3, 0x5

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8494
    const-string v1, "chatType"

    iget-object v3, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8495
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ap(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 8496
    :try_start_7
    sget-object v4, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v5}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8497
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 8498
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8499
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/chaton/chat/ChatFragment;->p:Z

    .line 8510
    :goto_13
    if-eqz p3, :cond_0

    .line 8511
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 8497
    :catchall_3
    move-exception v1

    :try_start_8
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v1

    .line 8501
    :cond_57
    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 8502
    const v2, 0x7f0b02ba

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0078

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b035a

    new-instance v3, Lcom/sec/chaton/chat/ba;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/ba;-><init>(Lcom/sec/chaton/chat/ay;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_13

    .line 7634
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_a
        :pswitch_e
        :pswitch_f
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_9
        :pswitch_0
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    .line 8526
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 8527
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QUERY_UPDATE_INBOX - completed. result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8528
    if-lez p3, :cond_1

    .line 8529
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    sget v2, Lcom/sec/chaton/chat/notification/a;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;I)V

    .line 8539
    :cond_0
    :goto_0
    return-void

    .line 8534
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/notification/a;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8535
    iget-object v0, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    sget v2, Lcom/sec/chaton/chat/notification/a;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method
