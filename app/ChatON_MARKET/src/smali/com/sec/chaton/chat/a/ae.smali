.class public Lcom/sec/chaton/chat/a/ae;
.super Lcom/sec/chaton/chat/a/x;
.source "TextBubbleDrawer.java"

# interfaces
.implements Lcom/sec/chaton/chat/a/k;
.implements Lcom/sec/chaton/chat/a/m;
.implements Lcom/sec/chaton/chat/a/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/x;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const v0, 0x7f0b01a0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a_()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Lcom/sec/chaton/chat/a/x;->a_()V

    .line 30
    iget v0, p0, Lcom/sec/chaton/chat/a/ae;->A:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/ae;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/a/ae;->l:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ae;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public f()Lcom/sec/chaton/chat/a/n;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 79
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ae;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ae;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/ae;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ae;->u:Lcom/sec/chaton/e/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/a/ae;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ae;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public h()Lcom/sec/chaton/chat/a/n;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ae;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ae;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/ae;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ae;->u:Lcom/sec/chaton/e/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/a/ae;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ae;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ae;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ae;->p:Ljava/lang/String;

    .line 54
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ae;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ae;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ae;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ae;->k:Lcom/sec/chaton/chat/fk;

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/ae;->o:J

    const/4 v3, 0x1

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/sec/chaton/chat/fk;->a(Landroid/view/View;JZ)V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
