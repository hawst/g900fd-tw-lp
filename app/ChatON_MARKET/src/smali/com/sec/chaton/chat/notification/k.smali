.class Lcom/sec/chaton/chat/notification/k;
.super Landroid/os/Handler;
.source "FragmentScreenNotification.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 178
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 179
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 181
    if-nez v0, :cond_1

    .line 182
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "resultEntry is null"

    invoke-static {}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_2

    .line 189
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->b()V

    goto :goto_0

    .line 193
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_3

    .line 194
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->a(Lcom/sec/chaton/a/a/l;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 195
    if-eqz v1, :cond_3

    .line 196
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 200
    :cond_3
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_0

    .line 201
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 202
    const/16 v1, 0x18

    if-eq v0, v1, :cond_4

    const/16 v1, 0x17

    if-eq v0, v1, :cond_4

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 203
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->a(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->a(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 206
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->f(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->b(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->c(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->d(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v4}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->e(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->f(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/k;->a:Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->g(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method
