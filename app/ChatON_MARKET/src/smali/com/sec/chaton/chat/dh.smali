.class Lcom/sec/chaton/chat/dh;
.super Landroid/os/Handler;
.source "ChatInfoFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 191
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 193
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/m;

    .line 194
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/m;->a:Z

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->b(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->c(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->d(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/d/m;->b(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->e(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->e(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 201
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0097

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/chat/dj;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/dj;-><init>(Lcom/sec/chaton/chat/dh;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00dc

    new-instance v2, Lcom/sec/chaton/chat/di;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/di;-><init>(Lcom/sec/chaton/chat/dh;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 218
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 219
    iget-object v1, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->e(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 220
    iget-object v1, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->e(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 222
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_4

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->g(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    goto/16 :goto_0

    .line 225
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/dh;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x324
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
