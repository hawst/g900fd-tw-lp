.class public abstract Lcom/sec/chaton/chat/a/a;
.super Ljava/lang/Object;
.source "AbstractBubbleDrawer.java"


# instance fields
.field protected A:I

.field protected B:I

.field protected C:Ljava/lang/String;

.field protected D:Ljava/lang/String;

.field protected E:I

.field private F:Ljava/util/Calendar;

.field private G:Ljava/util/Date;

.field protected a:Ljava/util/Calendar;

.field protected b:Ljava/util/Date;

.field protected c:Landroid/content/Context;

.field protected d:Landroid/view/View;

.field protected e:Landroid/database/Cursor;

.field protected f:I

.field protected g:Z

.field protected h:Lcom/sec/chaton/chat/el;

.field protected i:F

.field protected j:Lcom/sec/common/f/c;

.field protected k:Lcom/sec/chaton/chat/fk;

.field protected l:Lcom/sec/chaton/e/r;

.field protected m:J

.field protected n:Ljava/lang/String;

.field protected o:J

.field protected p:Ljava/lang/String;

.field protected q:Ljava/lang/String;

.field protected r:Ljava/lang/String;

.field protected s:Ljava/lang/String;

.field protected t:Ljava/lang/String;

.field protected u:Lcom/sec/chaton/e/w;

.field protected v:Ljava/lang/String;

.field protected w:Ljava/lang/String;

.field protected x:Ljava/lang/String;

.field protected y:J

.field protected z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->a:Ljava/util/Calendar;

    .line 120
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->F:Ljava/util/Calendar;

    .line 122
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->b:Ljava/util/Date;

    .line 123
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->G:Ljava/util/Date;

    .line 124
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final a(Landroid/content/Context;Lcom/sec/chaton/e/r;Landroid/view/View;Landroid/database/Cursor;Lcom/sec/chaton/chat/el;ZLcom/sec/common/f/c;F)V
    .locals 3

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/chaton/chat/a/a;->c:Landroid/content/Context;

    .line 141
    iput-object p2, p0, Lcom/sec/chaton/chat/a/a;->l:Lcom/sec/chaton/e/r;

    .line 142
    iput-object p3, p0, Lcom/sec/chaton/chat/a/a;->d:Landroid/view/View;

    .line 143
    iput-object p4, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/a;->f:I

    .line 145
    iput-object p5, p0, Lcom/sec/chaton/chat/a/a;->h:Lcom/sec/chaton/chat/el;

    .line 146
    iput-boolean p6, p0, Lcom/sec/chaton/chat/a/a;->g:Z

    .line 147
    iput p8, p0, Lcom/sec/chaton/chat/a/a;->i:F

    .line 148
    iput-object p7, p0, Lcom/sec/chaton/chat/a/a;->j:Lcom/sec/common/f/c;

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_sever_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/a/a;->m:J

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_inbox_no"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->n:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/a/a;->o:J

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_read_status"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/a;->B:I

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_content_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->u:Lcom/sec/chaton/e/w;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_time"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/a/a;->y:J

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_content"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->p:Ljava/lang/String;

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_content_translated"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->q:Ljava/lang/String;

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_from_lang"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->r:Ljava/lang/String;

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_to_lang"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->s:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_is_spoken"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->t:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/a;->A:I

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_sender"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->v:Ljava/lang/String;

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_download_uri"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->w:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_formatted"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->x:Ljava/lang/String;

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_is_failed"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/a;->z:I

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "buddy_no"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->C:Ljava/lang/String;

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "buddy_name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->D:Ljava/lang/String;

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "buddy_profile_status"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/a;->E:I

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->p:Ljava/lang/String;

    iput-object v0, p5, Lcom/sec/chaton/chat/el;->az:Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->q:Ljava/lang/String;

    iput-object v0, p5, Lcom/sec/chaton/chat/el;->aG:Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->n:Ljava/lang/String;

    iput-object v0, p5, Lcom/sec/chaton/chat/el;->aE:Ljava/lang/String;

    .line 174
    iget-wide v0, p0, Lcom/sec/chaton/chat/a/a;->y:J

    iput-wide v0, p5, Lcom/sec/chaton/chat/el;->aA:J

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_session_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p5, Lcom/sec/chaton/chat/el;->aB:Ljava/lang/String;

    .line 176
    iget-wide v0, p0, Lcom/sec/chaton/chat/a/a;->m:J

    iput-wide v0, p5, Lcom/sec/chaton/chat/el;->aC:J

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v2, "message_read_status"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p5, Lcom/sec/chaton/chat/el;->aD:I

    .line 178
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    const/4 v0, 0x1

    iput-boolean v0, p5, Lcom/sec/chaton/chat/el;->aF:Z

    .line 186
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->D:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->D:Ljava/lang/String;

    .line 191
    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/a;->y:J

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/sec/chaton/chat/a/a;->b:Ljava/util/Date;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->a:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->b:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 194
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/a;->d_()V

    .line 196
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/a;->a()V

    .line 197
    return-void

    .line 182
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p5, Lcom/sec/chaton/chat/el;->aF:Z

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/chat/fk;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/chaton/chat/a/a;->k:Lcom/sec/chaton/chat/fk;

    .line 132
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method protected d_()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->G:Ljava/util/Date;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 224
    iget v0, p0, Lcom/sec/chaton/chat/a/a;->z:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/chat/a/a;->A:I

    if-nez v0, :cond_1

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->A:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 259
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v5, :cond_2

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 236
    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    if-eqz v1, :cond_4

    .line 237
    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 242
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->G:Ljava/util/Date;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    const-string v4, "message_time"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Date;->setTime(J)V

    .line 244
    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 248
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->F:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->G:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->F:Ljava/util/Calendar;

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->F:Ljava/util/Calendar;

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->F:Ljava/util/Calendar;

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 253
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->A:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/a;->b:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 239
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/a;->e:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    .line 257
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/a/a;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->A:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method
