.class public final enum Lcom/sec/chaton/chat/notification/d;
.super Ljava/lang/Enum;
.source "ChatONNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/chat/notification/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/chat/notification/d;

.field public static final enum b:Lcom/sec/chaton/chat/notification/d;

.field public static final enum c:Lcom/sec/chaton/chat/notification/d;

.field private static final synthetic f:[Lcom/sec/chaton/chat/notification/d;


# instance fields
.field private d:I

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 124
    new-instance v0, Lcom/sec/chaton/chat/notification/d;

    const-string v1, "MUTE_ON"

    const-string v2, "mute on"

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/sec/chaton/chat/notification/d;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/chat/notification/d;->a:Lcom/sec/chaton/chat/notification/d;

    .line 125
    new-instance v0, Lcom/sec/chaton/chat/notification/d;

    const-string v1, "MUTE_OFF"

    const-string v2, "mute off"

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/sec/chaton/chat/notification/d;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/chat/notification/d;->b:Lcom/sec/chaton/chat/notification/d;

    .line 126
    new-instance v0, Lcom/sec/chaton/chat/notification/d;

    const-string v1, "MUTE_EXPIRED"

    const-string v2, "mute expired"

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/sec/chaton/chat/notification/d;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/chat/notification/d;->c:Lcom/sec/chaton/chat/notification/d;

    .line 123
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/chat/notification/d;

    sget-object v1, Lcom/sec/chaton/chat/notification/d;->a:Lcom/sec/chaton/chat/notification/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/chat/notification/d;->b:Lcom/sec/chaton/chat/notification/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/chat/notification/d;->c:Lcom/sec/chaton/chat/notification/d;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/chat/notification/d;->f:[Lcom/sec/chaton/chat/notification/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 132
    iput p3, p0, Lcom/sec/chaton/chat/notification/d;->d:I

    .line 133
    iput-object p4, p0, Lcom/sec/chaton/chat/notification/d;->e:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/chat/notification/d;
    .locals 1

    .prologue
    .line 123
    const-class v0, Lcom/sec/chaton/chat/notification/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/chat/notification/d;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/sec/chaton/chat/notification/d;->f:[Lcom/sec/chaton/chat/notification/d;

    invoke-virtual {v0}, [Lcom/sec/chaton/chat/notification/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/chat/notification/d;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/d;->e:Ljava/lang/String;

    return-object v0
.end method
