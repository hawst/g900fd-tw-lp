.class public Lcom/sec/chaton/chat/ei;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "ChatListAdapter.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Landroid/content/Context;

.field private d:Landroid/view/LayoutInflater;

.field private e:Landroid/widget/ListView;

.field private f:Lcom/sec/common/f/c;

.field private g:Lorg/a/a/a/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/j",
            "<",
            "Lcom/sec/chaton/e/w;",
            "Lcom/sec/chaton/chat/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/sec/chaton/e/r;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Ljava/lang/String;

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:F

.field private o:Landroid/graphics/Typeface;

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private r:J

.field private s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private t:J

.field private u:Lcom/sec/chaton/chat/em;

.field private v:J

.field private w:Lcom/sec/chaton/chat/fk;

.field private x:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    const-class v0, Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    .line 57
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "message_session_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "message_read_status"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "message_content"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "message_is_failed"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/chat/ei;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListView;Landroid/database/Cursor;Lcom/sec/chaton/e/r;Landroid/graphics/Typeface;FLcom/sec/common/f/c;Lorg/a/a/a/a/j;Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/ListView;",
            "Landroid/database/Cursor;",
            "Lcom/sec/chaton/e/r;",
            "Landroid/graphics/Typeface;",
            "F",
            "Lcom/sec/common/f/c;",
            "Lorg/a/a/a/a/j",
            "<",
            "Lcom/sec/chaton/e/w;",
            "Lcom/sec/chaton/chat/a/a;",
            ">;",
            "Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111
    const v2, 0x7f030045

    sget-object v4, Lcom/sec/chaton/chat/ei;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 90
    sget-object v0, Lcom/sec/chaton/chat/em;->a:Lcom/sec/chaton/chat/em;

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->x:Landroid/graphics/Bitmap;

    .line 113
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->d:Landroid/view/LayoutInflater;

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ei;->i:Z

    .line 116
    iput-object p1, p0, Lcom/sec/chaton/chat/ei;->c:Landroid/content/Context;

    .line 117
    iput-object p2, p0, Lcom/sec/chaton/chat/ei;->e:Landroid/widget/ListView;

    .line 118
    iput-object p4, p0, Lcom/sec/chaton/chat/ei;->h:Lcom/sec/chaton/e/r;

    .line 120
    iput-object p7, p0, Lcom/sec/chaton/chat/ei;->f:Lcom/sec/common/f/c;

    .line 121
    iput-object p8, p0, Lcom/sec/chaton/chat/ei;->g:Lorg/a/a/a/a/j;

    .line 124
    iput-object p5, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    .line 125
    iput p6, p0, Lcom/sec/chaton/chat/ei;->n:F

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->p:Ljava/util/ArrayList;

    .line 128
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    .line 132
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_send"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->j:Ljava/lang/String;

    .line 133
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_receive"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->l:Ljava/lang/String;

    .line 134
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[constructor] send bubble:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " recv bubble:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ei;)Lcom/sec/chaton/chat/em;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    return-object v0
.end method

.method private a(Lcom/sec/chaton/chat/el;)V
    .locals 2

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1117
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 1118
    const-string v0, "send or receive bubble ID is empty"

    sget-object v1, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    :cond_1
    :goto_0
    return-void

    .line 1125
    :cond_2
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->o:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/sec/chaton/chat/ei;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1133
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->J:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/sec/chaton/chat/ei;->k()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/chat/el;IZJ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 650
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 756
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    if-eqz p3, :cond_7

    .line 655
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 657
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/chaton/chat/ej;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/chat/ej;-><init>(Lcom/sec/chaton/chat/ei;Lcom/sec/chaton/chat/el;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 684
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->a:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_2

    .line 685
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 686
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->b:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_4

    .line 687
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 688
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 690
    :cond_3
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 692
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->d:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_6

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 694
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 696
    :cond_5
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 698
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->c:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_0

    .line 699
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 703
    :cond_7
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 705
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/chaton/chat/ek;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/chat/ek;-><init>(Lcom/sec/chaton/chat/ei;Lcom/sec/chaton/chat/el;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 733
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->a:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_8

    .line 734
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 735
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->b:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_a

    .line 736
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 737
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 739
    :cond_9
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 741
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->d:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_d

    .line 742
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 743
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 745
    :cond_b
    iget-wide v0, p0, Lcom/sec/chaton/chat/ei;->v:J

    cmp-long v0, p4, v0

    if-gtz v0, :cond_c

    .line 746
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 748
    :cond_c
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 749
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 752
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    sget-object v1, Lcom/sec/chaton/chat/em;->c:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_0

    .line 753
    iget-object v0, p1, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ei;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ei;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/chaton/chat/ei;->h()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ei;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/chaton/chat/ei;->i()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ei;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/chat/ei;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    return-object v0
.end method

.method private h()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    .line 596
    iget-wide v0, p0, Lcom/sec/chaton/chat/ei;->t:J

    iget-wide v2, p0, Lcom/sec/chaton/chat/ei;->r:J

    add-long/2addr v0, v2

    .line 597
    iget-object v2, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    add-int/2addr v2, v3

    int-to-long v2, v2

    .line 599
    cmp-long v4, v2, v5

    if-nez v4, :cond_0

    .line 600
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ei;->f()V

    .line 609
    :goto_0
    cmp-long v0, v2, v5

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 610
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->w:Lcom/sec/chaton/chat/fk;

    invoke-interface {v1, v0}, Lcom/sec/chaton/chat/fk;->a(Z)V

    .line 611
    return-void

    .line 602
    :cond_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 603
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ei;->g()V

    goto :goto_0

    .line 605
    :cond_1
    sget-object v0, Lcom/sec/chaton/chat/em;->b:Lcom/sec/chaton/chat/em;

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    goto :goto_0

    .line 609
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private i()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    .line 614
    iget-wide v0, p0, Lcom/sec/chaton/chat/ei;->t:J

    iget-wide v2, p0, Lcom/sec/chaton/chat/ei;->r:J

    add-long/2addr v0, v2

    .line 615
    iget-object v2, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    add-int/2addr v2, v3

    int-to-long v2, v2

    .line 616
    sub-long v2, v0, v2

    .line 618
    cmp-long v4, v0, v5

    if-nez v4, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ei;->f()V

    .line 636
    :goto_0
    cmp-long v0, v2, v5

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 637
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->w:Lcom/sec/chaton/chat/fk;

    invoke-interface {v1, v0}, Lcom/sec/chaton/chat/fk;->a(Z)V

    .line 638
    return-void

    .line 621
    :cond_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 622
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ei;->g()V

    goto :goto_0

    .line 624
    :cond_1
    sget-object v0, Lcom/sec/chaton/chat/em;->d:Lcom/sec/chaton/chat/em;

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    .line 625
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 626
    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    .line 628
    const-string v1, "message_time"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/ei;->v:J

    goto :goto_0

    .line 636
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private j()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1144
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->m:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1145
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->m:Landroid/graphics/drawable/Drawable;

    .line 1149
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 1150
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 1151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "success to get drawable. Receive bubble ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1160
    :goto_0
    return-object v0

    .line 1155
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_3

    .line 1156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fail to get drawable. Receive bubble ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private k()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->k:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1165
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->g(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->k:Landroid/graphics/drawable/Drawable;

    .line 1169
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 1170
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 1171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "success to get drawable. Send bubble ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1180
    :goto_0
    return-object v0

    .line 1175
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_3

    .line 1176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fail to get drawable. Send bubble ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ei;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    return-object v0
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 147
    iput-wide p1, p0, Lcom/sec/chaton/chat/ei;->t:J

    .line 148
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/chaton/chat/ei;->x:Landroid/graphics/Bitmap;

    .line 209
    return-void
.end method

.method public a(Lcom/sec/chaton/chat/em;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    .line 189
    return-void
.end method

.method public a(Lcom/sec/chaton/chat/fk;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/sec/chaton/chat/ei;->w:Lcom/sec/chaton/chat/fk;

    .line 197
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ei;->i:Z

    .line 201
    return-void
.end method

.method public b()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    return-object v0
.end method

.method public b(J)V
    .locals 0

    .prologue
    .line 151
    iput-wide p1, p0, Lcom/sec/chaton/chat/ei;->r:J

    .line 152
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 17

    .prologue
    .line 468
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/chat/el;

    .line 470
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 472
    const-string v1, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 473
    const-string v1, "message_sever_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 475
    iget-wide v7, v2, Lcom/sec/chaton/chat/el;->c:J

    .line 478
    iput-wide v14, v2, Lcom/sec/chaton/chat/el;->c:J

    .line 480
    const-string v1, "message_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 481
    const-string v4, "message_sender"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 482
    const-string v4, "message_content_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v9

    .line 483
    const-string v4, "message_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 490
    const/4 v4, 0x1

    .line 491
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v10

    const-string v11, "chaton_id"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v10, v11, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 492
    const/4 v4, 0x0

    .line 496
    :cond_0
    if-eqz v4, :cond_6

    .line 497
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    .line 510
    :goto_0
    sget-object v5, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    if-eq v9, v5, :cond_7

    if-eqz v6, :cond_7

    const/4 v5, 0x3

    if-eq v6, v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/ei;->e:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_7

    .line 511
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/el;IZJ)V

    .line 519
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/chat/ei;->x:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_8

    .line 521
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ei;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ei;->x:Landroid/graphics/Bitmap;

    invoke-direct {v1, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 522
    iget-object v3, v2, Lcom/sec/chaton/chat/el;->n:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 523
    iget-object v1, v2, Lcom/sec/chaton/chat/el;->n:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 531
    :goto_2
    const/4 v3, 0x0

    .line 534
    :try_start_0
    iget-object v1, v2, Lcom/sec/chaton/chat/el;->ay:Lcom/sec/chaton/chat/a/a;

    if-eqz v1, :cond_1

    .line 535
    iget-object v1, v2, Lcom/sec/chaton/chat/el;->ax:Lcom/sec/chaton/e/w;

    if-ne v1, v9, :cond_a

    .line 536
    iget-object v3, v2, Lcom/sec/chaton/chat/el;->ay:Lcom/sec/chaton/chat/a/a;

    .line 540
    cmp-long v1, v7, v14

    if-eqz v1, :cond_9

    .line 541
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lcom/sec/chaton/chat/a/a;->a(Z)V

    .line 552
    :cond_1
    :goto_3
    if-nez v3, :cond_2

    .line 553
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/chat/ei;->g:Lorg/a/a/a/a/j;

    invoke-virtual {v1, v9}, Lorg/a/a/a/a/j;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/chat/a/a;

    move-object v3, v1

    .line 556
    :cond_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 557
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NumActive: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ei;->g:Lorg/a/a/a/a/j;

    invoke-virtual {v4}, Lorg/a/a/a/a/j;->b()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", NumIdle: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ei;->g:Lorg/a/a/a/a/j;

    invoke-virtual {v4}, Lorg/a/a/a/a/j;->a()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :cond_3
    iput-object v9, v2, Lcom/sec/chaton/chat/el;->ax:Lcom/sec/chaton/e/w;

    .line 561
    iput-object v3, v2, Lcom/sec/chaton/chat/el;->ay:Lcom/sec/chaton/chat/a/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 568
    :cond_4
    :goto_4
    if-eqz v3, :cond_5

    .line 570
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/chat/ei;->w:Lcom/sec/chaton/chat/fk;

    invoke-virtual {v3, v1}, Lcom/sec/chaton/chat/a/a;->a(Lcom/sec/chaton/chat/fk;)V

    .line 571
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ei;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/ei;->h:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/chat/ei;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/sec/chaton/chat/ei;->i:Z

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/chat/ei;->f:Lcom/sec/common/f/c;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/chaton/chat/ei;->n:F

    move-object/from16 v6, p1

    move-object v8, v2

    invoke-virtual/range {v3 .. v11}, Lcom/sec/chaton/chat/a/a;->a(Landroid/content/Context;Lcom/sec/chaton/e/r;Landroid/view/View;Landroid/database/Cursor;Lcom/sec/chaton/chat/el;ZLcom/sec/common/f/c;F)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 580
    :cond_5
    :goto_5
    const-wide/16 v1, 0x0

    cmp-long v1, v14, v1

    if-nez v1, :cond_b

    move-wide v1, v12

    .line 581
    :goto_6
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v1, v2, v0}, Lcom/sec/chaton/multimedia/audio/b;->a(JLandroid/view/View;)V

    .line 583
    return-void

    .line 501
    :cond_6
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 514
    :cond_7
    iget-object v1, v2, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 515
    iget-object v1, v2, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 516
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/chat/ei;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_1

    .line 525
    :cond_8
    iget-object v1, v2, Lcom/sec/chaton/chat/el;->n:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 527
    iget-object v1, v2, Lcom/sec/chaton/chat/el;->n:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 544
    :cond_9
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {v3, v1}, Lcom/sec/chaton/chat/a/a;->a(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_3

    .line 562
    :catch_0
    move-exception v1

    .line 563
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_4

    .line 564
    sget-object v4, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    .line 548
    :cond_a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/chat/ei;->g:Lorg/a/a/a/a/j;

    iget-object v4, v2, Lcom/sec/chaton/chat/el;->ax:Lcom/sec/chaton/e/w;

    iget-object v5, v2, Lcom/sec/chaton/chat/el;->ay:Lcom/sec/chaton/chat/a/a;

    invoke-virtual {v1, v4, v5}, Lorg/a/a/a/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_3

    .line 572
    :catch_1
    move-exception v1

    .line 573
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_5

    .line 574
    sget-object v2, Lcom/sec/chaton/chat/ei;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_5

    :cond_b
    move-wide v1, v14

    .line 580
    goto :goto_6
.end method

.method public c()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 185
    :cond_1
    return-void
.end method

.method public d()Lcom/sec/chaton/chat/em;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->e:Landroid/widget/ListView;

    .line 205
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 586
    sget-object v0, Lcom/sec/chaton/chat/em;->a:Lcom/sec/chaton/chat/em;

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    .line 587
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ei;->c()V

    .line 588
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 591
    sget-object v0, Lcom/sec/chaton/chat/em;->c:Lcom/sec/chaton/chat/em;

    iput-object v0, p0, Lcom/sec/chaton/chat/ei;->u:Lcom/sec/chaton/chat/em;

    .line 592
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ei;->c()V

    .line 593
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/16 v5, 0x8

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f030045

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 214
    new-instance v2, Lcom/sec/chaton/chat/el;

    invoke-direct {v2}, Lcom/sec/chaton/chat/el;-><init>()V

    .line 216
    iput-object v1, v2, Lcom/sec/chaton/chat/el;->a:Landroid/view/View;

    .line 219
    const v0, 0x7f07019e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->A:Landroid/widget/RelativeLayout;

    .line 221
    const v0, 0x7f0701a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->d:Landroid/widget/TextView;

    .line 222
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 225
    const v0, 0x7f0701a2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->D:Landroid/widget/RelativeLayout;

    .line 226
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->D:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 228
    const v0, 0x7f0701a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->e:Landroid/widget/TextView;

    .line 229
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 230
    const v0, 0x7f0701a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->f:Landroid/widget/TextView;

    .line 233
    const v0, 0x7f0701a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    .line 234
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 236
    const v0, 0x7f0701a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->l:Landroid/widget/ImageView;

    .line 237
    const v0, 0x7f0701a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->n:Landroid/widget/ImageView;

    .line 238
    const v0, 0x7f0701a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->k:Landroid/widget/TextView;

    .line 241
    const v0, 0x7f0701c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->B:Landroid/widget/RelativeLayout;

    .line 243
    const v0, 0x7f0701c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->C:Landroid/widget/TextView;

    .line 245
    const v0, 0x7f0701c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->F:Landroid/widget/ProgressBar;

    .line 247
    const v0, 0x7f0701c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    .line 249
    const v0, 0x7f0701c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->E:Landroid/widget/ProgressBar;

    .line 250
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 252
    const v0, 0x7f0701c8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    .line 253
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 254
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 256
    const v0, 0x7f0701ca

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->H:Landroid/widget/CheckBox;

    .line 258
    const v0, 0x7f0701aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->o:Landroid/widget/LinearLayout;

    .line 259
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLongClickable(Z)V

    .line 260
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 262
    const v0, 0x7f0701ad

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->p:Landroid/widget/LinearLayout;

    .line 264
    const v0, 0x7f0701ae

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    .line 265
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 267
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 269
    const v0, 0x7f0701af

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->r:Landroid/widget/TextView;

    .line 270
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    const v0, 0x7f0701b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->s:Landroid/widget/TextView;

    .line 273
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 274
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    const v0, 0x7f0701b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    .line 276
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 281
    const v0, 0x7f0701b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    .line 285
    const v0, 0x7f0701b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    .line 286
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 287
    const v0, 0x7f0701b5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ai:Landroid/widget/TextView;

    .line 288
    const v0, 0x7f0701b7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->aj:Landroid/widget/TextView;

    .line 289
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->ai:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 290
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->aj:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 293
    const v0, 0x7f0701ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    .line 294
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 295
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setLongClickable(Z)V

    .line 296
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 298
    const v0, 0x7f0701be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    .line 299
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 300
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLongClickable(Z)V

    .line 302
    const v0, 0x7f0701bf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    .line 303
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setLongClickable(Z)V

    .line 304
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 306
    const v0, 0x7f0701c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->x:Landroid/widget/TextView;

    .line 307
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->x:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 309
    const v0, 0x7f0701c1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->y:Landroid/widget/TextView;

    .line 310
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->y:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 312
    const v0, 0x7f0701c2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->z:Landroid/widget/ImageView;

    .line 313
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 316
    const v0, 0x7f0701ac

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ad:Landroid/widget/ProgressBar;

    .line 319
    const v0, 0x7f0701cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->h:Landroid/view/ViewGroup;

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/chat/ei;->d:Landroid/view/LayoutInflater;

    const v3, 0x7f030044

    iget-object v4, v2, Lcom/sec/chaton/chat/el;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    .line 322
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 325
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07019b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->m:Landroid/widget/ImageView;

    .line 326
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070183

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ao:Landroid/widget/LinearLayout;

    .line 329
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07017e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->U:Landroid/widget/LinearLayout;

    .line 331
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070180

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    .line 332
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07019c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->W:Landroid/widget/CheckBox;

    .line 334
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070181

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    .line 336
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07017d

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    .line 337
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 339
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070182

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    .line 340
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 342
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07017b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    .line 343
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 345
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07017c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    .line 346
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 348
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07017f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    .line 349
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07017a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    .line 350
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 352
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070179

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->J:Landroid/widget/LinearLayout;

    .line 353
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->J:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLongClickable(Z)V

    .line 354
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->J:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 356
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070186

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->K:Landroid/widget/LinearLayout;

    .line 359
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070187

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    .line 360
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 361
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 362
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 364
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070188

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->M:Landroid/widget/TextView;

    .line 365
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->M:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 367
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070189

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->N:Landroid/widget/TextView;

    .line 368
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->N:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 369
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->N:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    const v0, 0x7f07018a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    .line 377
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 378
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLongClickable(Z)V

    .line 382
    const v0, 0x7f07018b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    .line 383
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 384
    const v0, 0x7f07018c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->al:Landroid/widget/TextView;

    .line 385
    const v0, 0x7f07018e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->am:Landroid/widget/TextView;

    .line 386
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->al:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 387
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->am:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 390
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070184

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    .line 391
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 392
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setLongClickable(Z)V

    .line 394
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070185

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->X:Landroid/widget/ProgressBar;

    .line 398
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f07018f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->P:Landroid/widget/ImageView;

    .line 399
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->P:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 400
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->P:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setLongClickable(Z)V

    .line 403
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070196

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    .line 404
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 405
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLongClickable(Z)V

    .line 407
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070197

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    .line 408
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setLongClickable(Z)V

    .line 409
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 411
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070198

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->S:Landroid/widget/TextView;

    .line 412
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->S:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 414
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    const v3, 0x7f070199

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->T:Landroid/widget/TextView;

    .line 415
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->T:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 420
    const v0, 0x7f0701a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->an:Landroid/widget/LinearLayout;

    .line 441
    const v0, 0x7f070190

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ap:Landroid/widget/RelativeLayout;

    .line 442
    const v0, 0x7f070191

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    .line 443
    const v0, 0x7f070193

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->ar:Landroid/widget/RelativeLayout;

    .line 444
    const v0, 0x7f070195

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->as:Landroid/widget/TextView;

    .line 445
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 446
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->as:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 448
    const v0, 0x7f0701b8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->at:Landroid/widget/RelativeLayout;

    .line 449
    const v0, 0x7f0701b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    .line 450
    const v0, 0x7f0701bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->av:Landroid/widget/RelativeLayout;

    .line 451
    const v0, 0x7f0701bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/chat/el;->aw:Landroid/widget/TextView;

    .line 452
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 453
    iget-object v0, v2, Lcom/sec/chaton/chat/el;->aw:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ei;->o:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 458
    invoke-direct {p0, v2}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/el;)V

    .line 461
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 463
    return-object v1
.end method
