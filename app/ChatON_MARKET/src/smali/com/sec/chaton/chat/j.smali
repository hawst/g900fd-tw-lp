.class Lcom/sec/chaton/chat/j;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/graphics/Bitmap;

.field final synthetic b:Lcom/sec/chaton/chat/i;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/i;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1662
    iput-object p1, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iput-object p2, p0, Lcom/sec/chaton/chat/j;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1666
    iget-object v0, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v0, v0, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/chat/j;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1668
    iget-object v0, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v0, v0, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    new-instance v1, Lcom/sec/chaton/chat/dg;

    iget-object v2, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v2, v2, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {v1, v2, v4}, Lcom/sec/chaton/chat/dg;-><init>(Lcom/sec/chaton/chat/ChatFragment;Z)V

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/dg;)Lcom/sec/chaton/chat/dg;

    .line 1670
    iget-object v0, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v0, v0, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->F(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/dg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1671
    iget-object v0, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v0, v0, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 1672
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 1673
    iget-object v0, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v0, v0, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->F(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/dg;

    move-result-object v0

    new-array v1, v3, [Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v2, v2, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/dg;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1678
    :cond_0
    :goto_0
    return-void

    .line 1675
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v0, v0, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->F(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/dg;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/chaton/chat/j;->b:Lcom/sec/chaton/chat/i;

    iget-object v3, v3, Lcom/sec/chaton/chat/i;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/dg;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
