.class Lcom/sec/chaton/chat/ez;
.super Ljava/lang/Object;
.source "EditChatTitleDialog.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ex;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ex;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 148
    const/4 v1, 0x6

    if-ne p2, v1, :cond_3

    .line 151
    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    iget-object v2, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v2}, Lcom/sec/chaton/chat/ex;->c(Lcom/sec/chaton/chat/ex;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ex;->a(Lcom/sec/chaton/chat/ex;Ljava/lang/String;)Ljava/lang/String;

    .line 152
    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v1}, Lcom/sec/chaton/chat/ex;->d(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v1}, Lcom/sec/chaton/chat/ex;->d(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v1}, Lcom/sec/chaton/chat/ex;->a(Lcom/sec/chaton/chat/ex;)V

    .line 165
    :goto_0
    return v0

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    iget-object v1, v1, Lcom/sec/chaton/chat/ex;->c:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v2}, Lcom/sec/chaton/chat/ex;->d(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v3}, Lcom/sec/chaton/chat/ex;->e(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/d/m;->c(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    iget-object v1, v1, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    iget-object v1, v1, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_2

    .line 158
    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    iget-object v1, v1, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/chat/ez;->a:Lcom/sec/chaton/chat/ex;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ex;->dismiss()V

    goto :goto_0

    .line 165
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
