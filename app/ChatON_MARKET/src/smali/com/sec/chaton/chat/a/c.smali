.class public Lcom/sec/chaton/chat/a/c;
.super Lcom/sec/chaton/chat/a/x;
.source "AppLinkBubbleDrawer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/x;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 12

    .prologue
    const/16 v10, 0x8

    const/high16 v9, 0x41f00000    # 30.0f

    const/high16 v8, -0x1000000

    const/4 v7, 0x0

    .line 42
    invoke-super {p0}, Lcom/sec/chaton/chat/a/x;->a()V

    .line 44
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->p:Ljava/lang/String;

    .line 45
    const/4 v1, 0x0

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->parse(Ljava/lang/String;)Lcom/sec/chaton/io/entry/MessageType4Entry;

    move-result-object v0

    .line 53
    if-nez v0, :cond_0

    .line 54
    const-string v0, "onDrawBubble(), hide bubble,  empty parse result"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->e()V

    .line 185
    :goto_0
    return-void

    .line 59
    :cond_0
    instance-of v2, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    if-nez v2, :cond_1

    .line 60
    const-string v0, "onDrawBubble(), hide bubble,  not received full-contents yet"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->e()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 107
    const-string v0, "onDrawBubble(), exceptions!!!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->e()V

    goto :goto_0

    .line 65
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/MessageType4Entry;->getDisplayMessage()Ljava/lang/String;

    move-result-object v2

    .line 66
    check-cast v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    .line 67
    const-string v3, "android"

    const-string v4, "phone"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getParam(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;

    move-result-object v3

    .line 74
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getAppName()Ljava/lang/String;

    move-result-object v4

    .line 95
    if-eqz v3, :cond_2

    .line 96
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b024d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move-object v1, v2

    .line 112
    :goto_1
    if-eqz v3, :cond_7

    .line 115
    iget v2, p0, Lcom/sec/chaton/chat/a/c;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 117
    iget-object v3, p0, Lcom/sec/chaton/chat/a/c;->c:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v3, v1, v4}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 119
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->n()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 120
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 121
    iget-object v3, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v3, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->ar:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->as:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->as:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ap:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 98
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getAnythingExecuteUri()Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 100
    const-string v0, "%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto :goto_1

    .line 102
    :cond_3
    const-string v4, "%s\n\n%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto/16 :goto_1

    .line 123
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 135
    :cond_5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 136
    iget-object v3, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v3, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->av:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->aw:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aw:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->at:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 138
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 153
    :cond_7
    iget v0, p0, Lcom/sec/chaton/chat/a/c;->i:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 155
    iget-object v2, p0, Lcom/sec/chaton/chat/a/c;->c:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v2, v1, v3}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 157
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->n()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 158
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 159
    iget-object v2, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :goto_4
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->ar:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 166
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v7, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ap:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 161
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 171
    :cond_9
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 172
    iget-object v2, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    :goto_5
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->av:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 179
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v7, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->at:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 174
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 189
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->a(Z)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ap:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->at:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->av:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->av:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    const-string v0, ""

    return-object v0
.end method

.method protected d()V
    .locals 6

    .prologue
    .line 231
    .line 235
    const/4 v0, 0x0

    .line 237
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42de0000    # 111.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float v4, v1, v2

    .line 238
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->aq:Landroid/widget/TextView;

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->as:Landroid/widget/TextView;

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    .line 254
    :goto_0
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float v1, v4, v1

    .line 257
    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    sub-float v0, v1, v0

    .line 262
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/a/c;->a(F)F

    move-result v0

    .line 265
    float-to-int v1, v0

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 268
    const/high16 v1, 0x42340000    # 45.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 269
    return-void

    .line 246
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v1, Lcom/sec/chaton/chat/el;->au:Landroid/widget/TextView;

    .line 247
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->aw:Landroid/widget/TextView;

    .line 248
    iget-object v1, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->C:Landroid/widget/TextView;

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 25
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 27
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    :goto_0
    return-void

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 30
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 216
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->onClick(Landroid/view/View;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ar:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->av:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/c;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/chat/a/c;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/c;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/c;->e:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/chat/fk;->d(ZLandroid/database/Cursor;)V

    goto :goto_0
.end method
