.class public Lcom/sec/chaton/chat/ChatInfoMoreFragment;
.super Landroid/support/v4/app/Fragment;
.source "ChatInfoMoreFragment.java"

# interfaces
.implements Lcom/sec/chaton/buddy/da;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:[Ljava/lang/String;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Buddy;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/app/ProgressDialog;

.field private k:Lcom/sec/chaton/chat/eb;

.field private l:Lcom/sec/chaton/buddy/BuddyFragment;

.field private m:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/sec/chaton/chat/ChatInfoMoreActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    .line 135
    new-instance v0, Lcom/sec/chaton/chat/dx;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/dx;-><init>(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->m:Landroid/os/Handler;

    .line 406
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 386
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/chat/ea;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/chat/ea;-><init>(Lcom/sec/chaton/chat/ChatInfoMoreFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 404
    :goto_0
    return-void

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "inboxNO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c:Ljava/lang/String;

    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "buddyNO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d:Ljava/lang/String;

    .line 337
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "chatType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->e:I

    .line 339
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f:[Ljava/lang/String;

    .line 344
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 369
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 370
    const-string v0, "activity is null. don\'t show strong dialog"

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a(Z)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    .line 382
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Lcom/sec/chaton/buddy/BuddyFragment;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l:Lcom/sec/chaton/buddy/BuddyFragment;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->j:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->m:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->e:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 317
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 318
    const-string v0, "activity is null. don\'t show strong dialog"

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    new-instance v0, Lcom/sec/chaton/chat/eb;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/eb;-><init>(Lcom/sec/chaton/chat/ChatInfoMoreFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->k:Lcom/sec/chaton/chat/eb;

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->k:Lcom/sec/chaton/chat/eb;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/eb;->show()V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;[Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 251
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    array-length v2, p2

    move v0, v3

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v4, p2, v0

    .line 253
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->h:Ljava/util/ArrayList;

    .line 258
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->i:Ljava/util/ArrayList;

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->g:Ljava/util/ArrayList;

    move v5, v3

    .line 260
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_4

    .line 261
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/ArrayList;

    move v2, v3

    .line 262
    :goto_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 263
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    move v4, v3

    .line 264
    :goto_3
    array-length v6, p2

    if-ge v4, v6, :cond_1

    .line 265
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v6

    aget-object v7, p2, v4

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 266
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 264
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 260
    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 273
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    const-string v0, ""

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 276
    const-string v4, "unknown_buddy"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 277
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 282
    :cond_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 283
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 284
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 286
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 287
    const v2, 0x7f0b0065

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    .line 288
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v8, :cond_7

    .line 289
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b01a7

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 293
    :goto_5
    const v0, 0x7f0b014f

    new-instance v2, Lcom/sec/chaton/chat/dz;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/dz;-><init>(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/dy;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/dy;-><init>(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 309
    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 313
    :goto_6
    return-void

    .line 291
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b00bd

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    goto :goto_5

    .line 311
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a()V

    goto :goto_6
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 348
    const-string v0, "onActivityResult()"

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 351
    if-ne p2, v4, :cond_0

    .line 352
    const-string v0, "receivers"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 353
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 355
    const-string v2, "receivers"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    const-string v0, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    const-string v0, "buddyNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    const-string v0, "chatType"

    iget v2, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->e:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 361
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 362
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/eh;

    invoke-interface {v0}, Lcom/sec/chaton/chat/eh;->c()V

    .line 365
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 108
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b:Landroid/app/Activity;

    .line 109
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f070013

    .line 87
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 88
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f03002a

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setContentView(I)V

    .line 92
    if-nez p1, :cond_0

    .line 93
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l:Lcom/sec/chaton/buddy/BuddyFragment;

    const-string v2, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/da;)V

    .line 101
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b()V

    .line 102
    return-void

    .line 98
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/da;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 132
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d()V

    .line 133
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b:Landroid/app/Activity;

    .line 116
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 127
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 122
    return-void
.end method
