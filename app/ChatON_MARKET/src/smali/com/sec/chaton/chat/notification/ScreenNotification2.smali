.class public Lcom/sec/chaton/chat/notification/ScreenNotification2;
.super Landroid/support/v4/app/FragmentActivity;
.source "ScreenNotification2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static a:Ljava/lang/String;

.field private static final e:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Landroid/widget/EditText;

.field private C:Landroid/support/v4/view/ViewPager;

.field private D:Lcom/sec/chaton/chat/notification/w;

.field private E:Landroid/widget/LinearLayout;

.field private F:I

.field private G:Lcom/sec/chaton/chat/notification/g;

.field private H:Landroid/widget/ProgressBar;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/widget/LinearLayout;

.field private K:Landroid/widget/ImageView;

.field private L:Landroid/widget/ImageView;

.field private M:I

.field private N:Ljava/lang/Runnable;

.field private O:Landroid/os/Handler;

.field private P:Lcom/sec/chaton/e/a/u;

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Landroid/content/BroadcastReceiver;

.field private U:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field b:Ljava/lang/String;

.field c:Lcom/sec/chaton/e/a/v;

.field protected d:Landroid/os/Handler;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private k:Landroid/content/Context;

.field private l:Lcom/sec/chaton/chat/notification/a;

.field private m:Lcom/sec/chaton/util/ab;

.field private n:Lcom/sec/chaton/d/o;

.field private o:Landroid/widget/Toast;

.field private p:Landroid/app/KeyguardManager;

.field private q:Landroid/os/PowerManager;

.field private r:Landroid/os/PowerManager$WakeLock;

.field private s:Z

.field private t:Z

.field private u:I

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/ImageButton;

.field private y:Landroid/widget/Button;

.field private z:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    .line 86
    const-string v0, "is_black_popup"

    sput-object v0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 89
    const/16 v0, 0x63

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f:I

    .line 91
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g:I

    .line 92
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->h:I

    .line 94
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->i:I

    .line 95
    const/16 v0, 0x65

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->j:I

    .line 98
    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    .line 100
    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    .line 106
    iput-boolean v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    .line 107
    iput-boolean v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->t:Z

    .line 120
    iput v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    .line 128
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->M:I

    .line 130
    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    .line 134
    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b:Ljava/lang/String;

    .line 139
    iput-boolean v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->S:Z

    .line 148
    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->T:Landroid/content/BroadcastReceiver;

    .line 760
    new-instance v0, Lcom/sec/chaton/chat/notification/s;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/s;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->U:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 827
    new-instance v0, Lcom/sec/chaton/chat/notification/t;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/t;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->c:Lcom/sec/chaton/e/a/v;

    .line 860
    new-instance v0, Lcom/sec/chaton/chat/notification/u;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/u;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->d:Landroid/os/Handler;

    .line 1106
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/notification/ScreenNotification2;I)I
    .locals 0

    .prologue
    .line 83
    iput p1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/notification/ScreenNotification2;Lcom/sec/chaton/chat/notification/g;)Lcom/sec/chaton/chat/notification/g;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    return-object p1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x4

    .line 1099
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 1100
    if-le v1, v0, :cond_0

    .line 1103
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 497
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->v:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 499
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->E:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 502
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[setSimpleInfo] type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/notification/y;->d:Lcom/sec/chaton/chat/notification/y;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/y;)V

    .line 508
    const/16 v0, 0x64

    if-ne p1, v0, :cond_2

    .line 509
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->z:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->y:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 514
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->A:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 529
    :cond_1
    :goto_0
    return-void

    .line 515
    :cond_2
    const/16 v0, 0x65

    if-ne p1, v0, :cond_1

    .line 516
    const-string v0, ""

    .line 517
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v3, :cond_3

    .line 518
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0048

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v3, v3, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 522
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 523
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->z:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 526
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->y:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->A:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 520
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/notification/ScreenNotification2;Lcom/sec/chaton/chat/notification/y;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/y;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/chat/notification/y;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1033
    sget-object v0, Lcom/sec/chaton/chat/notification/y;->a:Lcom/sec/chaton/chat/notification/y;

    if-ne p1, v0, :cond_0

    .line 1034
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1035
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1047
    :goto_0
    return-void

    .line 1036
    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/notification/y;->b:Lcom/sec/chaton/chat/notification/y;

    if-ne p1, v0, :cond_1

    .line 1037
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1038
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1039
    :cond_1
    sget-object v0, Lcom/sec/chaton/chat/notification/y;->c:Lcom/sec/chaton/chat/notification/y;

    if-ne p1, v0, :cond_2

    .line 1040
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1041
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1044
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1045
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->N:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/chat/notification/ScreenNotification2;I)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(I)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/chat/notification/ScreenNotification2;)I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->u:I

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/chat/notification/ScreenNotification2;)I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/a;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->v:Landroid/widget/TextView;

    return-object v0
.end method

.method private h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->m:Lcom/sec/chaton/util/ab;

    const-string v1, "OFF"

    const-string v2, "OFF"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 365
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_1

    .line 366
    const-string v0, "OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 368
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_0

    .line 369
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isLocked:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isSecure:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    :cond_0
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    const-string v0, "ON"

    .line 377
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 382
    iput-boolean v5, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    .line 383
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 384
    :cond_0
    iput-boolean v6, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    .line 387
    :cond_1
    iget v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v1, v1, Lcom/sec/chaton/chat/notification/a;->m:I

    if-lt v0, v1, :cond_4

    .line 388
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_2

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[EXCEPTION] current index is smaller than unread count. Finish pop-up. index:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", count:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v1, v1, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b()V

    .line 483
    :cond_3
    :goto_0
    return-void

    .line 396
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b:Ljava/lang/String;

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    const-string v2, "0999"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->m:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting show receive message"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b:Ljava/lang/String;

    const-string v3, "OFF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v6, :cond_5

    .line 407
    sget-object v0, Lcom/sec/chaton/chat/notification/y;->c:Lcom/sec/chaton/chat/notification/y;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/y;)V

    .line 410
    :cond_5
    const-string v0, "** Information for current push popup **"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "> replyOn:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "> currentIndex:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "> current Inbox:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "> isUpdated:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->t:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->t:Z

    if-ne v0, v6, :cond_6

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    iget v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v2, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 420
    const-string v0, ">> update message, keep current page"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_3

    .line 422
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 427
    :cond_6
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    if-ne v0, v6, :cond_a

    .line 428
    const-string v0, ">> replying on... "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new message Inbox:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 430
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->w:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "1/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v4, v4, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 434
    iput v5, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    .line 437
    const-string v0, ">>> new message from same inbox. set currentIndex to 0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_7

    .line 439
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> sender:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " message:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    :cond_7
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_8

    .line 469
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :cond_8
    if-eqz v2, :cond_b

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->E:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 477
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    goto/16 :goto_0

    .line 443
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->w:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v4, v4, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v3, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 447
    const-string v0, ">>> new message from different inbox. "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_7

    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> sender:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " message:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 454
    :cond_a
    const-string v0, ">> not replying on... "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    iput v5, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->v:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->w:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "1/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 461
    const-string v0, "set currentIndex to 0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_7

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "extra text:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>> sender:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " message:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 475
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->E:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 481
    :cond_c
    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(I)V

    goto/16 :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->w:Landroid/widget/TextView;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2

    .prologue
    const/16 v1, 0x63

    .line 486
    const-string v0, ""

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v1, :cond_0

    .line 488
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 492
    :goto_0
    return-object v0

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic k(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->E:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 613
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 614
    const-string v1, "callAlertSetting"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 615
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->startActivity(Landroid/content/Intent;)V

    .line 616
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b()V

    .line 617
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 1090
    :try_start_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1091
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 1092
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1096
    :cond_0
    :goto_0
    return-void

    .line 1094
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic l(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/chat/notification/ScreenNotification2;)I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->M:I

    return v0
.end method

.method static synthetic n(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 596
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 597
    const-string v0, "inbox_unread_count"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->P:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inbox_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v6, v6, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 599
    return-void
.end method

.method protected a(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 632
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x2710

    if-ne v0, v1, :cond_1

    .line 633
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 634
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    const v1, 0x7f0b0031

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 635
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 636
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 637
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 638
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;Lcom/sec/chaton/chat/notification/i;)V
    .locals 3

    .prologue
    .line 808
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f()V

    .line 810
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget v1, v1, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    .line 811
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 812
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    iget-object v1, p2, Lcom/sec/chaton/chat/notification/i;->a:Ljava/lang/String;

    iget v2, p2, Lcom/sec/chaton/chat/notification/i;->b:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 813
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->c(J)V

    .line 814
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    iget-object v1, p2, Lcom/sec/chaton/chat/notification/i;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    .line 815
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    iget-wide v1, p2, Lcom/sec/chaton/chat/notification/i;->d:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->a(J)V

    .line 816
    return-void
.end method

.method protected b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 602
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/notification/a;->a(Z)Z

    .line 607
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l()V

    .line 608
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->finish()V

    .line 609
    invoke-virtual {p0, v2, v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->overridePendingTransition(II)V

    .line 610
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 622
    sput-boolean v0, Lcom/sec/chaton/global/GlobalApplication;->c:Z

    .line 624
    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    .line 626
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 629
    :cond_0
    return-void
.end method

.method protected d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget v0, v0, Lcom/sec/chaton/chat/notification/g;->f:I

    sget-object v1, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 645
    invoke-static {p0, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 646
    const-string v1, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 647
    const-string v1, "chatType"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget v2, v2, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 648
    const-string v1, "fromPush"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 649
    const-string v1, "Content"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 670
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->startActivity(Landroid/content/Intent;)V

    .line 671
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b()V

    .line 672
    return-void

    .line 651
    :cond_1
    invoke-static {p0, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 653
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 654
    const-string v1, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 655
    const-string v1, "chatType"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget v2, v2, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 656
    const-string v1, "fromPush"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 657
    const-string v1, "disable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 658
    iget-boolean v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    if-eqz v1, :cond_2

    .line 659
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 661
    :cond_2
    const-string v1, "buddyNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 663
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->E:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 664
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 665
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 666
    const-string v2, "lastTempMessage"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected e()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    const/4 v11, 0x0

    .line 675
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 676
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 758
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 682
    const/4 v1, -0x3

    if-eq v1, v0, :cond_2

    const/4 v1, -0x2

    if-ne v1, v0, :cond_a

    :cond_2
    move v0, v11

    .line 685
    :goto_1
    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 686
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o:Landroid/widget/Toast;

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 687
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o:Landroid/widget/Toast;

    invoke-virtual {v0, v11}, Landroid/widget/Toast;->setDuration(I)V

    .line 688
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 692
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o:Landroid/widget/Toast;

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 694
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o:Landroid/widget/Toast;

    invoke-virtual {v0, v11}, Landroid/widget/Toast;->setDuration(I)V

    .line 695
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 699
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v6, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    .line 700
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/i;

    .line 702
    if-nez v0, :cond_8

    .line 703
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_5

    .line 704
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can not find server. Start DB query inboxNo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :cond_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const-string v3, "inbox_no=?"

    new-array v4, v10, [Ljava/lang/String;

    aput-object v6, v4, v11

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 709
    if-nez v0, :cond_7

    .line 710
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_6

    .line 711
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to db query inboxNo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v11}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 718
    :cond_7
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 720
    const-string v1, "inbox_session_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 721
    const-string v1, "lasst_session_merge_time"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 722
    const-string v1, "inbox_participants"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 723
    const-string v1, "inbox_server_ip"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 724
    const-string v1, "inbox_server_port"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 726
    new-instance v3, Lcom/sec/chaton/chat/notification/i;

    invoke-direct/range {v3 .. v9}, Lcom/sec/chaton/chat/notification/i;-><init>(Ljava/lang/String;ILjava/lang/String;IJ)V

    .line 727
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v3

    .line 730
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Ljava/lang/String;Lcom/sec/chaton/chat/notification/i;)V

    .line 733
    iget-object v1, v0, Lcom/sec/chaton/chat/notification/i;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/d/o;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-nez v1, :cond_9

    .line 734
    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget v0, v0, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    aput-object v0, v7, v11

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v9, v2

    invoke-virtual/range {v3 .. v9}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 748
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a()V

    .line 752
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->E:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 753
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->H:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 755
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 756
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->finish()V

    goto/16 :goto_0

    .line 739
    :cond_9
    iget v1, v0, Lcom/sec/chaton/chat/notification/i;->e:I

    new-array v7, v1, [Ljava/lang/String;

    .line 740
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    aput-object v1, v7, v11

    .line 741
    iget-object v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    sget-object v4, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    iget v1, v1, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/chaton/chat/notification/i;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v9, v2

    invoke-virtual/range {v3 .. v9}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    goto :goto_2

    :cond_a
    move v0, v10

    goto/16 :goto_1
.end method

.method protected f()V
    .locals 5

    .prologue
    .line 819
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_0

    .line 820
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    .line 821
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 822
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    const-wide/32 v3, 0x75300

    add-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/d/o;->c(J)V

    .line 823
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n:Lcom/sec/chaton/d/o;

    .line 825
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 564
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 593
    :goto_0
    return-void

    .line 568
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/global/GlobalApplication;->c(Landroid/content/Context;)V

    goto :goto_0

    .line 573
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b()V

    goto :goto_0

    .line 578
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->d()V

    goto :goto_0

    .line 582
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e()V

    goto :goto_0

    .line 585
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->c()V

    goto :goto_0

    .line 589
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k()V

    goto :goto_0

    .line 564
    :sswitch_data_0
    .sparse-switch
        0x7f0702d7 -> :sswitch_0
        0x7f0702d8 -> :sswitch_1
        0x7f0702d9 -> :sswitch_2
        0x7f0703c9 -> :sswitch_5
        0x7f0703cf -> :sswitch_4
        0x7f0703d0 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 152
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 153
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 155
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "[onCreate]"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_0
    iput-object p0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/sec/multiwindow/MultiWindowManager;->isMultiWindowServiceEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    invoke-static {p0}, Landroid/sec/multiwindow/MultiWindow;->createInstance(Landroid/app/Activity;)Landroid/sec/multiwindow/MultiWindow;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    invoke-virtual {v0}, Landroid/sec/multiwindow/MultiWindow;->normalWindow()Z

    .line 165
    iput-boolean v5, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->S:Z

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x280000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-static {v0, v9, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o:Landroid/widget/Toast;

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->p:Landroid/app/KeyguardManager;

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->q:Landroid/os/PowerManager;

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->q:Landroid/os/PowerManager;

    const v1, 0x1000001a

    const-string v2, "cz"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->r:Landroid/os/PowerManager$WakeLock;

    .line 177
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->c:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->P:Lcom/sec/chaton/e/a/u;

    .line 179
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 180
    const v1, 0x7f0300d8

    invoke-virtual {v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 182
    invoke-virtual {p0, v5}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->requestWindowFeature(I)Z

    .line 185
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v5, :cond_3

    .line 187
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 188
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->u:I

    .line 203
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->setContentView(Landroid/view/View;)V

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    if-nez v0, :cond_2

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    .line 208
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    invoke-virtual {v0, v5}, Lcom/sec/chaton/chat/notification/a;->a(Z)Z

    .line 210
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->m:Lcom/sec/chaton/util/ab;

    .line 212
    const v0, 0x7f0703d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->H:Landroid/widget/ProgressBar;

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->H:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 215
    const v0, 0x7f0703c8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->v:Landroid/widget/TextView;

    .line 216
    const v0, 0x7f0703c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->w:Landroid/widget/TextView;

    .line 218
    const v0, 0x7f0703cf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/chat/notification/o;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/notification/o;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/chat/notification/p;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/notification/p;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->B:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/chat/notification/q;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/notification/q;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 266
    const v0, 0x7f0703ce

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->E:Landroid/widget/LinearLayout;

    .line 268
    const v0, 0x7f0703d0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->x:Landroid/widget/ImageButton;

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    const v0, 0x7f0702d8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->z:Landroid/widget/Button;

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->z:Landroid/widget/Button;

    const v1, 0x7f0b0176

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->z:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275
    const v0, 0x7f0702d9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->y:Landroid/widget/Button;

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->y:Landroid/widget/Button;

    const v1, 0x7f0b01a4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->y:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    const v0, 0x7f0702d7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->A:Landroid/widget/Button;

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->A:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->A:Landroid/widget/Button;

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->A:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 284
    const v0, 0x7f0703cc

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    .line 285
    new-instance v0, Lcom/sec/chaton/chat/notification/w;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/notification/w;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->D:Lcom/sec/chaton/chat/notification/w;

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->D:Lcom/sec/chaton/chat/notification/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->U:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 289
    const v0, 0x7f0703cd

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->I:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v7}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 294
    const v0, 0x7f0703c9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->J:Landroid/widget/LinearLayout;

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->J:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    const v0, 0x7f0703ca

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->K:Landroid/widget/ImageView;

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 299
    const v0, 0x7f0703cb

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->L:Landroid/widget/ImageView;

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 303
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/chat/notification/r;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/notification/r;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 329
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->i()V

    .line 332
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 333
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 334
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 336
    new-instance v1, Lcom/sec/chaton/chat/notification/x;

    invoke-direct {v1, p0, v9}, Lcom/sec/chaton/chat/notification/x;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;Lcom/sec/chaton/chat/notification/n;)V

    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->T:Landroid/content/BroadcastReceiver;

    .line 337
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->T:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 338
    return-void

    .line 190
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/am;->h(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v5, :cond_4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v0, 0xb

    :goto_1
    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->u:I

    .line 192
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    .line 193
    new-instance v0, Lcom/sec/chaton/chat/notification/n;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/n;-><init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->N:Ljava/lang/Runnable;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->N:Ljava/lang/Runnable;

    iget v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->M:I

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 191
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v0, 0xa

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1051
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 1052
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f()V

    .line 1053
    iput v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    .line 1054
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->G:Lcom/sec/chaton/chat/notification/g;

    .line 1055
    const-string v0, "onDestroy"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l:Lcom/sec/chaton/chat/notification/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/notification/a;->a(Z)Z

    .line 1059
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1060
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 1061
    const-string v0, "It is blackscreen. Release wake lock"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1066
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->S:Z

    if-eqz v0, :cond_2

    .line 1067
    invoke-static {p0}, Landroid/sec/multiwindow/MultiWindow;->createInstance(Landroid/app/Activity;)Landroid/sec/multiwindow/MultiWindow;

    move-result-object v0

    .line 1068
    invoke-virtual {v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow()Z

    .line 1071
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->T:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    .line 1072
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->T:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1074
    :cond_3
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 533
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 534
    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->setIntent(Landroid/content/Intent;)V

    .line 537
    const-string v0, "isUpdated"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->t:Z

    .line 539
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 540
    const-string v0, "[onNewIntent]"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 545
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->s:Z

    if-nez v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->O:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->N:Ljava/lang/Runnable;

    iget v2, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->M:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 549
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->D:Lcom/sec/chaton/chat/notification/w;

    if-eqz v0, :cond_2

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->D:Lcom/sec/chaton/chat/notification/w;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/w;->notifyDataSetChanged()V

    .line 552
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->C:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 555
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->t:Z

    if-nez v0, :cond_3

    .line 556
    iget v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->F:I

    .line 559
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->i()V

    .line 560
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1078
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 1079
    const-string v0, "onPause"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 342
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 343
    const-string v0, "onResume"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->u:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    .line 346
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 347
    const-string v0, "It is blackscreen. Wake lock"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->r:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 351
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->Q:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->R:Z

    if-nez v0, :cond_1

    .line 352
    iput-boolean v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->R:Z

    .line 355
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->Q:Z

    if-nez v0, :cond_2

    .line 356
    iput-boolean v3, p0, Lcom/sec/chaton/chat/notification/ScreenNotification2;->Q:Z

    .line 359
    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1084
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 1085
    const-string v0, "onstop"

    sget-object v1, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    return-void
.end method
