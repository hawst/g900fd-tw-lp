.class public Lcom/sec/chaton/chat/b/a;
.super Ljava/lang/Object;
.source "LanguageManager.java"


# instance fields
.field public A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public B:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:[Ljava/lang/String;

.field public z:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p0}, Lcom/sec/chaton/chat/b/a;->a()V

    .line 67
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    .line 227
    :goto_0
    return-object v0

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->a:Ljava/lang/String;

    goto :goto_0

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->c:Ljava/lang/String;

    goto :goto_0

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->d:Ljava/lang/String;

    goto :goto_0

    .line 206
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->e:Ljava/lang/String;

    goto :goto_0

    .line 208
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->f:Ljava/lang/String;

    goto :goto_0

    .line 210
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    goto :goto_0

    .line 212
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    goto :goto_0

    .line 214
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    goto :goto_0

    .line 221
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->j:Ljava/lang/String;

    goto :goto_0

    .line 224
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->k:Ljava/lang/String;

    goto :goto_0

    .line 227
    :cond_a
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    const v0, 0x7f0b03bc

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    .line 71
    const v0, 0x7f0b03b5

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->a:Ljava/lang/String;

    .line 72
    const v0, 0x7f0b03b4

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    .line 73
    const v0, 0x7f0b03b7

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->d:Ljava/lang/String;

    .line 74
    const v0, 0x7f0b03b8

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->e:Ljava/lang/String;

    .line 75
    const v0, 0x7f0b03b9

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->f:Ljava/lang/String;

    .line 76
    const v0, 0x7f0b03bb

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    .line 77
    const v0, 0x7f0b03ba

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    .line 78
    const v0, 0x7f0b03bd

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->j:Ljava/lang/String;

    .line 79
    const v0, 0x7f0b03be

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->k:Ljava/lang/String;

    .line 80
    const v0, 0x7f0b03b6

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->c:Ljava/lang/String;

    .line 82
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->l:Ljava/lang/String;

    .line 84
    const-string v0, "zh-CN"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    .line 85
    const-string v0, "en-UK"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->m:Ljava/lang/String;

    .line 86
    const-string v0, "en-US"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    .line 87
    const-string v0, "fr-FR"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    .line 88
    const-string v0, "de-DE"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    .line 89
    const-string v0, "it-IT"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    .line 90
    const-string v0, "ja-JP"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    .line 91
    const-string v0, "ko-KR"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    .line 92
    const-string v0, "pt-PT"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    .line 93
    const-string v0, "ru-RU"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    .line 94
    const-string v0, "es-ES"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    .line 96
    const-string v0, "pt"

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->x:Ljava/lang/String;

    .line 97
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->m:Ljava/lang/String;

    aput-object v1, v0, v4

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    aput-object v1, v0, v5

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    aput-object v1, v0, v6

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    aput-object v1, v0, v7

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->y:[Ljava/lang/String;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 110
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->a:Ljava/lang/String;

    aput-object v1, v0, v4

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    aput-object v1, v0, v5

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->d:Ljava/lang/String;

    aput-object v1, v0, v6

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->e:Ljava/lang/String;

    aput-object v1, v0, v7

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 119
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->z:[Ljava/lang/String;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/sec/chaton/chat/b/a;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->B:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    return-void
.end method

.method public a(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 346
    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 347
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v1

    .line 348
    invoke-virtual {v1}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v2

    .line 349
    if-nez p2, :cond_0

    .line 350
    invoke-virtual {p0}, Lcom/sec/chaton/chat/b/a;->b()Ljava/lang/String;

    move-result-object p2

    .line 352
    :cond_0
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 353
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 354
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 355
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 356
    iget-object v2, v2, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 357
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 358
    invoke-virtual {p0, v1, p1, v0, p3}, Lcom/sec/chaton/chat/b/a;->a(Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/ArrayAdapter;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 359
    invoke-virtual {p1, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 423
    :cond_1
    :goto_0
    return-void

    .line 362
    :cond_2
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->m:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 363
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 364
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 365
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 366
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 367
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 368
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 369
    iget-object v2, v2, Lcom/sec/chaton/chat/b/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 370
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 371
    invoke-virtual {p0, v1, p1, v0, p3}, Lcom/sec/chaton/chat/b/a;->a(Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/ArrayAdapter;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 372
    invoke-virtual {p1, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 375
    :cond_3
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 376
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 377
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 378
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 379
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 380
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 381
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 382
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 383
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 384
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 385
    iget-object v2, v2, Lcom/sec/chaton/chat/b/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 386
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 387
    invoke-virtual {p0, v1, p1, v0, p3}, Lcom/sec/chaton/chat/b/a;->a(Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/ArrayAdapter;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 388
    invoke-virtual {p1, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 391
    :cond_4
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 394
    :cond_5
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 395
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 396
    iget-object v2, v2, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 397
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 398
    invoke-virtual {p0, v1, p1, v0, p3}, Lcom/sec/chaton/chat/b/a;->a(Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/ArrayAdapter;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 399
    invoke-virtual {p1, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_0

    .line 402
    :cond_6
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 403
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 404
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 405
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 406
    iget-object v2, v2, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 407
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 408
    invoke-virtual {p0, v1, p1, v0, p3}, Lcom/sec/chaton/chat/b/a;->a(Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/ArrayAdapter;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 409
    invoke-virtual {p1, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_0

    .line 412
    :cond_7
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 413
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 414
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 415
    iget-object v3, v2, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 416
    iget-object v2, v2, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 417
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 418
    invoke-virtual {p0, v1, p1, v0, p3}, Lcom/sec/chaton/chat/b/a;->a(Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/ArrayAdapter;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 419
    invoke-virtual {p1, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_0
.end method

.method public a(Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/ArrayAdapter;Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/chat/b/c;",
            "Landroid/widget/Spinner;",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 435
    move v1, v2

    :goto_0
    invoke-virtual {p3}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 436
    invoke-virtual {p3, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/chaton/chat/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    invoke-virtual {p2, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 438
    const/4 v2, 0x1

    .line 441
    :cond_0
    return v2

    .line 435
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 289
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 290
    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    .line 331
    :goto_0
    return-object v0

    .line 294
    :cond_0
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 295
    if-nez v1, :cond_1

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    goto :goto_0

    .line 299
    :cond_1
    const-string v2, "en"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 300
    sget-object v1, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->m:Ljava/lang/String;

    goto :goto_0

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    goto :goto_0

    .line 308
    :cond_3
    const-string v0, "es"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    goto :goto_0

    .line 310
    :cond_4
    const-string v0, "fr"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    goto :goto_0

    .line 312
    :cond_5
    const-string v0, "de"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    goto :goto_0

    .line 314
    :cond_6
    const-string v0, "it"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 315
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    goto :goto_0

    .line 316
    :cond_7
    const-string v0, "ko"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    goto :goto_0

    .line 318
    :cond_8
    const-string v0, "ja"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    goto :goto_0

    .line 320
    :cond_9
    const-string v0, "zh"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    goto :goto_0

    .line 326
    :cond_a
    const-string v0, "pt"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    goto :goto_0

    .line 328
    :cond_b
    const-string v0, "ru"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 331
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    .line 260
    :goto_0
    return-object v0

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->m:Ljava/lang/String;

    goto :goto_0

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    goto :goto_0

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    goto :goto_0

    .line 239
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    goto :goto_0

    .line 241
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    goto :goto_0

    .line 243
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    goto :goto_0

    .line 245
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    goto :goto_0

    .line 247
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    goto :goto_0

    .line 254
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    goto :goto_0

    .line 257
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    goto :goto_0

    .line 260
    :cond_a
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Ljava/util/Locale;
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    .line 285
    :goto_0
    return-object v0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    new-instance v0, Ljava/util/Locale;

    const-string v1, "es"

    const-string v2, "ES"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 269
    sget-object v0, Ljava/util/Locale;->FRENCH:Ljava/util/Locale;

    goto :goto_0

    .line 270
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 271
    sget-object v0, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    goto :goto_0

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 273
    sget-object v0, Ljava/util/Locale;->ITALIAN:Ljava/util/Locale;

    goto :goto_0

    .line 274
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 275
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    goto :goto_0

    .line 276
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 277
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    goto :goto_0

    .line 278
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 279
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    goto :goto_0

    .line 280
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 281
    new-instance v0, Ljava/util/Locale;

    iget-object v1, p0, Lcom/sec/chaton/chat/b/a;->x:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 282
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/b/a;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 283
    new-instance v0, Ljava/util/Locale;

    const-string v1, "ru"

    const-string v2, "RU"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 285
    :cond_9
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 445
    .line 446
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    invoke-virtual {p0}, Lcom/sec/chaton/chat/b/a;->b()Ljava/lang/String;

    move-result-object p1

    .line 449
    :cond_0
    return-object p1
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    .line 454
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    iget-object p1, p0, Lcom/sec/chaton/chat/b/a;->n:Ljava/lang/String;

    .line 457
    :cond_0
    return-object p1
.end method
