.class public Lcom/sec/chaton/chat/a/r;
.super Lcom/sec/chaton/chat/a/x;
.source "GeoBubbleDrawer.java"

# interfaces
.implements Lcom/sec/chaton/chat/a/m;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/x;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f0b004c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x6

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 37
    invoke-super {p0}, Lcom/sec/chaton/chat/a/x;->a()V

    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/r;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->S:Landroid/widget/TextView;

    .line 47
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->T:Landroid/widget/TextView;

    .line 49
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 50
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->S:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 53
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->T:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 54
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    move-object v0, v1

    .line 70
    :goto_0
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 71
    const v6, 0x7f020195

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    :goto_1
    new-array v4, v8, [Ljava/lang/String;

    .line 80
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->p:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 81
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->p:Ljava/lang/String;

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 82
    array-length v6, v4

    if-le v6, v5, :cond_3

    .line 83
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    move v1, v5

    .line 84
    :goto_2
    array-length v5, v4

    if-ge v1, v5, :cond_2

    .line 85
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v4, v1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\n"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->x:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->y:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->z:Landroid/widget/ImageView;

    .line 61
    iget-object v6, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v6, v6, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 62
    iget-object v6, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v6, v6, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {v6, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v6, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v6, v6, Lcom/sec/chaton/chat/el;->x:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    iget-object v6, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v6, v6, Lcom/sec/chaton/chat/el;->y:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    iget-object v6, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v6, v6, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 73
    :cond_1
    const v6, 0x7f02018d

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 87
    :cond_2
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 91
    :cond_3
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->q:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 92
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080041

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 96
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    if-eqz v0, :cond_5

    .line 101
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    :cond_5
    :goto_3
    return-void

    .line 104
    :cond_6
    iget-object v4, p0, Lcom/sec/chaton/chat/a/r;->q:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 105
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    iget-object v1, p0, Lcom/sec/chaton/chat/a/r;->q:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    if-eqz v0, :cond_5

    invoke-static {}, Lcom/sec/chaton/chat/b/i;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 111
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 112
    new-instance v1, Lcom/sec/chaton/chat/a/s;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/a/s;-><init>(Lcom/sec/chaton/chat/a/r;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 121
    :cond_7
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    if-eqz v0, :cond_8

    .line 123
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    :cond_8
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 133
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->a(Z)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/r;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, ""

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    return v0
.end method

.method public h()Lcom/sec/chaton/chat/a/n;
    .locals 4

    .prologue
    .line 151
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/r;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/r;->p:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 156
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->onClick(Landroid/view/View;)V

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/r;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/chat/a/r;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/r;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/r;->e:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/chat/fk;->c(ZLandroid/database/Cursor;)V

    goto :goto_0
.end method
