.class Lcom/sec/chaton/chat/bc;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:J

.field final synthetic c:Z

.field final synthetic d:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/view/View;JZ)V
    .locals 0

    .prologue
    .line 10121
    iput-object p1, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    iput-object p2, p0, Lcom/sec/chaton/chat/bc;->a:Landroid/view/View;

    iput-wide p3, p0, Lcom/sec/chaton/chat/bc;->b:J

    iput-boolean p5, p0, Lcom/sec/chaton/chat/bc;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 10130
    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 10228
    :cond_0
    :goto_0
    return-void

    .line 10136
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 10138
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/chat/bc;->a:Landroid/view/View;

    invoke-virtual {v1, v3}, Lcom/sec/widget/HeightChangedListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 10141
    add-int/lit8 v1, v1, -0x1

    .line 10143
    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10151
    :goto_1
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 10152
    const-string v1, "message_content"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 10153
    const-string v1, "message_inbox_no"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 10154
    const-string v1, "message_download_uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 10155
    const-string v1, "message_content_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v7

    .line 10156
    const-string v1, "message_formatted"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 10159
    const-string v1, "message_is_file_upload"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 10170
    iget-boolean v0, p0, Lcom/sec/chaton/chat/bc;->c:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aA(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aH(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10174
    if-nez p2, :cond_2

    move v0, v10

    .line 10189
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 10227
    :goto_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto/16 :goto_0

    .line 10144
    :catch_0
    move-exception v0

    .line 10147
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    const-string v3, "_id=?"

    new-array v4, v10, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/sec/chaton/chat/bc;->b:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 10148
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    goto/16 :goto_1

    .line 10176
    :cond_2
    if-ne p2, v10, :cond_3

    move v0, v11

    .line 10177
    goto :goto_2

    .line 10178
    :cond_3
    if-ne p2, v11, :cond_8

    move v0, v12

    .line 10179
    goto :goto_2

    .line 10182
    :cond_4
    if-nez p2, :cond_5

    move v0, v10

    .line 10183
    goto :goto_2

    .line 10184
    :cond_5
    if-ne p2, v10, :cond_8

    move v0, v12

    .line 10185
    goto :goto_2

    .line 10191
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 10192
    if-eqz v1, :cond_6

    .line 10193
    const v0, 0x7f07017a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 10194
    if-eqz v0, :cond_6

    .line 10195
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v10

    .line 10196
    const/4 v11, -0x3

    if-eq v11, v10, :cond_6

    const/4 v11, -0x2

    if-ne v11, v10, :cond_7

    .line 10203
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static/range {v0 .. v9}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/view/View;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;I)V

    goto :goto_3

    .line 10199
    :cond_7
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_4

    .line 10206
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v4, v7}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    goto :goto_3

    .line 10209
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/chat/bc;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 10210
    const v1, 0x7f0b0158

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 10212
    invoke-static {v7}, Lcom/sec/chaton/chat/eq;->a(Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0042

    new-instance v3, Lcom/sec/chaton/chat/be;

    invoke-direct {v3, p0, v5}, Lcom/sec/chaton/chat/be;-><init>(Lcom/sec/chaton/chat/bc;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/bd;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/bd;-><init>(Lcom/sec/chaton/chat/bc;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 10224
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_3

    :cond_8
    move v0, v10

    goto/16 :goto_2

    .line 10189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
