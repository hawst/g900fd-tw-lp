.class public Lcom/sec/chaton/chat/ChatActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ChatActivity.java"

# interfaces
.implements Lcom/sec/chaton/chat/ct;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Lcom/sec/chaton/chat/ChatFragment;

.field private e:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

.field private f:Z

.field private h:Lcom/sec/chaton/base/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/chaton/chat/ChatActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ChatActivity;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->f:Z

    return-void
.end method

.method private c(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 78
    if-nez p1, :cond_1

    .line 121
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    const-string v2, "specialbuddy"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 83
    const-string v3, "eventpage"

    invoke-virtual {p1, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 84
    const-string v4, "liveprofile"

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 86
    if-ne v2, v1, :cond_2

    .line 87
    const-string v0, "checkSpecialBuddy(), spbd_intent, specialbuddy flag : true"

    sget-object v2, Lcom/sec/chaton/chat/ChatActivity;->g:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_2
    if-eqz v3, :cond_3

    .line 92
    const-string v0, "checkSpecialBuddy(), spbd_intent, eventPage flag : true"

    sget-object v2, Lcom/sec/chaton/chat/ChatActivity;->g:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 93
    goto :goto_0

    .line 96
    :cond_3
    if-eqz v4, :cond_4

    .line 97
    const-string v0, "checkSpecialBuddy(), spbd_intent, liveBuddyProfile flag : true"

    sget-object v2, Lcom/sec/chaton/chat/ChatActivity;->g:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 98
    goto :goto_0

    .line 102
    :cond_4
    const-string v2, "receivers"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 104
    if-eqz v2, :cond_0

    array-length v3, v2

    if-lez v3, :cond_0

    .line 108
    aget-object v2, v2, v0

    .line 117
    const-string v3, "0999"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 118
    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 334
    const-string v0, "showAgainPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 337
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 338
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatActivity;->a:Z

    if-nez v1, :cond_1

    .line 339
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 340
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    invoke-static {v3}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 342
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatActivity;->startActivity(Landroid/content/Intent;)V

    .line 345
    :cond_1
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatActivity;->a:Z

    .line 346
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 52
    const-string v1, "disable"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x400000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 54
    const-string v1, "disable"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 57
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatActivity;->c(Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->f:Z

    .line 59
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->f:Z

    if-eqz v0, :cond_1

    .line 61
    new-instance v0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-direct {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->e:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->e:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->h:Lcom/sec/chaton/base/d;

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->e:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    .line 69
    :goto_0
    return-object v0

    .line 65
    :cond_1
    new-instance v0, Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {v0}, Lcom/sec/chaton/chat/ChatFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->d:Lcom/sec/chaton/chat/ChatFragment;

    .line 66
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->d:Lcom/sec/chaton/chat/ChatFragment;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->h:Lcom/sec/chaton/base/d;

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->d:Lcom/sec/chaton/chat/ChatFragment;

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->finish()V

    .line 35
    return-void
.end method

.method public d()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 231
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatActivity;->d:Lcom/sec/chaton/chat/ChatFragment;

    if-eqz v1, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->y()Ljava/util/ArrayList;

    move-result-object v0

    .line 234
    :cond_0
    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->d:Lcom/sec/chaton/chat/ChatFragment;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->d:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 241
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 351
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 352
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 353
    const-string v0, "onActivityResult"

    sget-object v1, Lcom/sec/chaton/chat/ChatActivity;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->a:Z

    .line 356
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatActivity;->h:Lcom/sec/chaton/base/d;

    if-eqz v1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatActivity;->h:Lcom/sec/chaton/base/d;

    invoke-interface {v0}, Lcom/sec/chaton/base/d;->a()Z

    move-result v0

    .line 131
    :cond_0
    if-nez v0, :cond_1

    .line 133
    :try_start_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_1
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onNewIntent, Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPause()V

    .line 224
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->c:Z

    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 227
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 204
    if-eqz p1, :cond_0

    .line 205
    const-string v0, "mIsSpecialBuddy"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->f:Z

    .line 207
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 208
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatActivity;->f()V

    .line 177
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 185
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->c:Z

    if-nez v0, :cond_0

    .line 186
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatActivity;->c:Z

    .line 189
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->b:Z

    if-nez v0, :cond_1

    .line 190
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatActivity;->b:Z

    .line 192
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 196
    if-eqz p1, :cond_0

    .line 197
    const-string v0, "mIsSpecialBuddy"

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatActivity;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 199
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 200
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onStart()V

    .line 165
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStop, Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onStop()V

    .line 171
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 213
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatActivity;->f:Z

    if-nez v0, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 216
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 145
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatActivity;->finish()V

    .line 148
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
