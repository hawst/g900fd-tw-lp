.class public Lcom/sec/chaton/chat/a/v;
.super Lcom/sec/chaton/chat/a/u;
.source "LiveRecommendBubbleDrawer.java"

# interfaces
.implements Lcom/sec/chaton/chat/a/k;
.implements Lcom/sec/chaton/chat/a/m;


# instance fields
.field G:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

.field H:Z

.field I:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/u;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->H:Z

    .line 19
    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->I:Z

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/u;->a(Z)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/v;->G:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    .line 26
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/v;->H:Z

    .line 27
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/v;->I:Z

    .line 28
    return-void
.end method

.method protected b_()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    const/4 v5, 0x0

    .line 37
    invoke-super {p0}, Lcom/sec/chaton/chat/a/u;->b_()V

    .line 39
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->m()Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 51
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 56
    :cond_2
    iget v4, p0, Lcom/sec/chaton/chat/a/v;->i:F

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    .line 62
    iget-object v1, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->al:Landroid/widget/TextView;

    .line 63
    iget-object v1, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->am:Landroid/widget/TextView;

    .line 69
    :goto_1
    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 71
    invoke-virtual {v2, v5, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 72
    invoke-virtual {v1, v5, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 75
    const v0, 0x7f0b03c5

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 76
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 65
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    .line 66
    iget-object v1, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->ai:Landroid/widget/TextView;

    .line 67
    iget-object v1, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->aj:Landroid/widget/TextView;

    goto :goto_1
.end method

.method f()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->c(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/v;->G:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->G:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    if-eqz v0, :cond_0

    .line 86
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/v;->I:Z

    .line 90
    :goto_0
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/v;->H:Z

    .line 92
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->I:Z

    return v0

    .line 88
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->I:Z

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->c(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/chat/a/n;
    .locals 4

    .prologue
    .line 153
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/v;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/v;->p:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->k()Ljava/lang/String;

    move-result-object v1

    .line 159
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 163
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->m()Ljava/lang/String;

    move-result-object v1

    .line 164
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 5

    .prologue
    const v3, 0x7f0b00b4

    .line 173
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b03c5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    if-nez v0, :cond_1

    .line 175
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 176
    const-string v0, "onCopy() - title is null"

    const-string v1, "LiveRecommendBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 180
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->m()Ljava/lang/String;

    move-result-object v1

    .line 181
    if-nez v1, :cond_3

    .line 182
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_2

    .line 183
    const-string v1, "onCopy() - name is null"

    const-string v2, "LiveRecommendBubbleDrawer"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 187
    :cond_3
    const-string v2, "%s\n%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 188
    return-object v0
.end method

.method k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->H:Z

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->f()Z

    .line 100
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->I:Z

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->G:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->id:Ljava/lang/String;

    .line 103
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->H:Z

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->f()Z

    .line 111
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/v;->I:Z

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->G:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    .line 114
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/u;->onClick(Landroid/view/View;)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/v;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/chat/a/v;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/v;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/v;->e:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/chat/fk;->f(ZLandroid/database/Cursor;)V

    goto :goto_0
.end method
