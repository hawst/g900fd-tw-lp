.class public abstract Lcom/sec/chaton/chat/a/x;
.super Lcom/sec/chaton/chat/a/a;
.source "MessageBubbleDrawer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/sec/chaton/chat/a/l;


# instance fields
.field private F:Z

.field private G:Ljava/lang/Boolean;

.field private H:I

.field private I:Landroid/content/res/ColorStateList;

.field private J:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/a;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/x;->J:Z

    .line 75
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/chat/a/x;->H:I

    .line 76
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 601
    const/16 v0, 0x3e8

    .line 602
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->G:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 603
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 604
    invoke-static {p1}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 606
    const/16 v0, 0x64

    .line 608
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v0, :cond_2

    .line 609
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/x;->G:Ljava/lang/Boolean;

    .line 618
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->G:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 611
    :cond_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/x;->G:Ljava/lang/Boolean;

    goto :goto_0

    .line 614
    :cond_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/x;->G:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method protected a(F)F
    .locals 0

    .prologue
    .line 591
    return p1
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const v0, 0x7f0b01a0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/x;->F:Z

    .line 97
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->o()V

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->p()V

    .line 111
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->a_()V

    .line 114
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->b_()V

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->d()V

    .line 119
    return-void

    .line 94
    :cond_0
    iput-boolean v3, p0, Lcom/sec/chaton/chat/a/x;->F:Z

    goto :goto_0

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 123
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/a;->a(Z)V

    .line 125
    iput-object v2, p0, Lcom/sec/chaton/chat/a/x;->G:Ljava/lang/Boolean;

    .line 127
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->I:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected a_()V
    .locals 6

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 278
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 279
    iget v0, p0, Lcom/sec/chaton/chat/a/x;->A:I

    if-nez v0, :cond_2

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 285
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 293
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    :cond_0
    :goto_1
    return-void

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 297
    :cond_2
    iget v0, p0, Lcom/sec/chaton/chat/a/x;->A:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 308
    :cond_3
    iget v0, p0, Lcom/sec/chaton/chat/a/x;->A:I

    if-ne v0, v2, :cond_5

    .line 309
    iget v0, p0, Lcom/sec/chaton/chat/a/x;->B:I

    if-lez v0, :cond_4

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    const-string v1, "(%d)"

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/chat/a/x;->B:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 313
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 325
    :cond_5
    iget v0, p0, Lcom/sec/chaton/chat/a/x;->A:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    .line 326
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_6

    .line 327
    const-string v0, "Message type is fail."

    const-class v1, Lcom/sec/chaton/chat/a/x;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 333
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/x;->g:Z

    if-eqz v0, :cond_7

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 337
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 344
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Exceptional Message type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/chat/a/x;->A:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/chat/a/x;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 351
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->F:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 623
    const/4 v0, 0x1

    return v0
.end method

.method protected b_()V
    .locals 13

    .prologue
    const/16 v11, 0x3e8

    const/16 v10, 0x64

    const/4 v0, 0x0

    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 362
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->c()Ljava/lang/String;

    move-result-object v5

    .line 363
    iget v6, p0, Lcom/sec/chaton/chat/a/x;->i:F

    .line 371
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 372
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v1, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    .line 373
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->M:Landroid/widget/TextView;

    .line 374
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->N:Landroid/widget/TextView;

    .line 376
    iget-object v4, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 377
    iget-object v4, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 378
    iget-object v4, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 380
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0, v5}, Lcom/sec/chaton/chat/a/x;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 384
    iget-object v4, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v4, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385
    iget-object v4, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 387
    invoke-static {v5}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 388
    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 438
    :goto_0
    invoke-virtual {v3}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/chaton/chat/a/x;->I:Landroid/content/res/ColorStateList;

    .line 440
    iget-object v5, p0, Lcom/sec/chaton/chat/a/x;->q:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 441
    iget-object v5, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f080041

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 446
    :goto_1
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 447
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 448
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 452
    if-eqz v0, :cond_0

    .line 453
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 518
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->T:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 519
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 521
    return-void

    .line 390
    :cond_1
    invoke-virtual {v5, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 397
    :cond_2
    iget-object v4, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 398
    iget-object v4, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v4, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object v4, v5

    goto :goto_0

    .line 403
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v4, v1, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    .line 404
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v1, Lcom/sec/chaton/chat/el;->r:Landroid/widget/TextView;

    .line 405
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->s:Landroid/widget/TextView;

    .line 406
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    .line 408
    iget-object v7, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v7, v7, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    iget-object v7, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v7, v7, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 410
    iget-object v7, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v7, v7, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 412
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-direct {p0, v5}, Lcom/sec/chaton/chat/a/x;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 419
    invoke-static {v5}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 420
    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v12, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v0

    move-object v0, v12

    goto/16 :goto_0

    .line 422
    :cond_4
    invoke-virtual {v5, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v12, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v0

    move-object v0, v12

    goto/16 :goto_0

    .line 426
    :cond_5
    iget-object v7, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v7, v7, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 431
    iget-object v7, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v7, v7, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_0

    .line 443
    :cond_6
    iget-object v5, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f080047

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 458
    :cond_7
    if-eqz v4, :cond_c

    invoke-static {v4}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 459
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 460
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 463
    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f09013a

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 467
    :goto_3
    invoke-virtual {v3, v9, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 468
    invoke-virtual {v1, v9, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 470
    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 471
    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    iget v5, p0, Lcom/sec/chaton/chat/a/x;->H:I

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 473
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 478
    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/x;->q:Ljava/lang/String;

    iget v5, p0, Lcom/sec/chaton/chat/a/x;->H:I

    invoke-static {v2, v3, v5}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 481
    if-eqz v4, :cond_8

    invoke-direct {p0, v4}, Lcom/sec/chaton/chat/a/x;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 482
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 487
    :goto_4
    if-eqz v0, :cond_9

    invoke-static {}, Lcom/sec/chaton/chat/b/i;->a()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->s:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/chat/b/i;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 488
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 489
    new-instance v1, Lcom/sec/chaton/chat/a/y;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/a/y;-><init>(Lcom/sec/chaton/chat/a/x;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 484
    :cond_8
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 499
    :cond_9
    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 506
    :cond_a
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 507
    if-eqz v0, :cond_b

    .line 508
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 511
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    iget v1, p0, Lcom/sec/chaton/chat/a/x;->H:I

    invoke-static {v0, v4, v1}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_c
    move v2, v6

    goto :goto_3
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->p:Ljava/lang/String;

    return-object v0
.end method

.method public c_()V
    .locals 0

    .prologue
    .line 628
    return-void
.end method

.method protected d()V
    .locals 6

    .prologue
    .line 538
    .line 542
    const/4 v0, 0x0

    .line 544
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42de0000    # 111.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float v4, v1, v2

    .line 545
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->N:Landroid/widget/TextView;

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    .line 561
    :goto_0
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float v1, v4, v1

    .line 564
    if-eqz v0, :cond_2

    .line 565
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    sub-float v0, v1, v0

    .line 569
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/a/x;->a(F)F

    move-result v0

    .line 572
    float-to-int v1, v0

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 573
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 574
    const/high16 v1, 0x42240000    # 41.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    sub-float/2addr v0, v1

    .line 575
    float-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 579
    :goto_2
    return-void

    .line 553
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v1, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    .line 554
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->s:Landroid/widget/TextView;

    .line 555
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->C:Landroid/widget/TextView;

    goto :goto_0

    .line 577
    :cond_1
    float-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ChatON"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected n()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/x;->F:Z

    return v0
.end method

.method protected o()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 222
    .line 224
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->m:Landroid/widget/ImageView;

    check-cast v0, Lcom/sec/chaton/widget/ProfileImageView;

    .line 227
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->ao:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 228
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/util/bt;->c(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 250
    :goto_0
    invoke-virtual {v0, p0}, Lcom/sec/chaton/widget/ProfileImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    return-void

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->l:Landroid/widget/ImageView;

    check-cast v0, Lcom/sec/chaton/widget/ProfileImageView;

    .line 232
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 233
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 236
    iget-boolean v1, p0, Lcom/sec/chaton/chat/a/x;->g:Z

    if-eqz v1, :cond_1

    .line 237
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->C:Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/chat/a/x;->E:I

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;I)V

    .line 242
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->D:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 239
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto :goto_1

    .line 244
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    .line 246
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->k:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->k:Lcom/sec/chaton/chat/fk;

    invoke-interface {v0}, Lcom/sec/chaton/chat/fk;->t()V

    .line 187
    :cond_1
    :goto_0
    return-void

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->l:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->k:Lcom/sec/chaton/chat/fk;

    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->l:Landroid/widget/ImageView;

    check-cast v0, Lcom/sec/chaton/widget/ProfileImageView;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->C:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/x;->D:Ljava/lang/String;

    invoke-interface {v1, p1, v0, v2, v3}, Lcom/sec/chaton/chat/fk;->a(Landroid/view/View;Lcom/sec/chaton/widget/ProfileImageView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->m:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->k:Lcom/sec/chaton/chat/fk;

    invoke-interface {v0}, Lcom/sec/chaton/chat/fk;->i()V

    goto :goto_0

    .line 170
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ac:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->k:Lcom/sec/chaton/chat/fk;

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/x;->o:J

    const/4 v3, 0x0

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/sec/chaton/chat/fk;->a(Landroid/view/View;JZ)V

    goto :goto_0

    .line 172
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ag:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->af:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/chat/fk;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->k:Lcom/sec/chaton/chat/fk;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/chat/fk;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 179
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_9

    .line 180
    const-string v0, "onShare() - getTextContent() is null"

    const-string v1, "MessageBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->k:Lcom/sec/chaton/chat/fk;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/chat/fk;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/x;->J:Z

    .line 206
    const/4 v0, 0x0

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 191
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 194
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/x;->J:Z

    .line 195
    if-eqz v0, :cond_0

    .line 196
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/x;->J:Z

    .line 200
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected p()V
    .locals 5

    .prologue
    .line 257
    .line 259
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/x;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    .line 265
    :goto_0
    iget-wide v1, p0, Lcom/sec/chaton/chat/a/x;->y:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    .line 266
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    :goto_1
    return-void

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->C:Landroid/widget/TextView;

    goto :goto_0

    .line 268
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 270
    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/x;->b:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lcom/sec/chaton/chat/a/x;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    const-string v0, ""

    .line 641
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/x;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
