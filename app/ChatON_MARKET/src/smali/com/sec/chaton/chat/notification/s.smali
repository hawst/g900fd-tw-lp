.class Lcom/sec/chaton/chat/notification/s;
.super Ljava/lang/Object;
.source "ScreenNotification2.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/notification/ScreenNotification2;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 804
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 799
    return-void
.end method

.method public onPageSelected(I)V
    .locals 5

    .prologue
    const/16 v1, 0x63

    .line 764
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0, p1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;I)I

    .line 765
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e(Lcom/sec/chaton/chat/notification/ScreenNotification2;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    invoke-static {v2, v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;Lcom/sec/chaton/chat/notification/g;)Lcom/sec/chaton/chat/notification/g;

    .line 766
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->h(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 767
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->j(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->i(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 768
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    const-string v2, "0999"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 770
    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 771
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 776
    :goto_0
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_0

    .line 777
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onPageSelected] current index:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->e(Lcom/sec/chaton/chat/notification/ScreenNotification2;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isSpecialbuddy:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 780
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 781
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->l(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 782
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Ljava/lang/Runnable;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->m(Lcom/sec/chaton/chat/notification/ScreenNotification2;)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v1, :cond_3

    move v0, v1

    .line 787
    :goto_1
    if-nez p1, :cond_4

    .line 788
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    sget-object v1, Lcom/sec/chaton/chat/notification/y;->c:Lcom/sec/chaton/chat/notification/y;

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;Lcom/sec/chaton/chat/notification/y;)V

    .line 794
    :goto_2
    return-void

    .line 773
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->k(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 786
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    goto :goto_1

    .line 789
    :cond_4
    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_5

    .line 790
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    sget-object v1, Lcom/sec/chaton/chat/notification/y;->b:Lcom/sec/chaton/chat/notification/y;

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;Lcom/sec/chaton/chat/notification/y;)V

    goto :goto_2

    .line 792
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/s;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    sget-object v1, Lcom/sec/chaton/chat/notification/y;->a:Lcom/sec/chaton/chat/notification/y;

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a(Lcom/sec/chaton/chat/notification/ScreenNotification2;Lcom/sec/chaton/chat/notification/y;)V

    goto :goto_2
.end method
