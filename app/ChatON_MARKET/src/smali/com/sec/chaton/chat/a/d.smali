.class public Lcom/sec/chaton/chat/a/d;
.super Lcom/sec/chaton/chat/a/af;
.source "AudioBubbleDrawer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/af;-><init>()V

    return-void
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 11

    .prologue
    .line 196
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".tmp"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    invoke-static {p2}, Lcom/sec/chaton/util/r;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 198
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    new-instance v10, Ljava/lang/Thread;

    new-instance v0, Lcom/sec/chaton/chat/a/e;

    move-object v1, p0

    move v2, p1

    move-object v3, p3

    move-wide v4, p4

    move-wide/from16 v7, p6

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/chat/a/e;-><init>(Lcom/sec/chaton/chat/a/d;ZLjava/lang/String;JLjava/lang/String;JLjava/lang/String;)V

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 210
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 213
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f0b004b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 46
    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->a()V

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/d;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->w:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/d;->n:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/chaton/chat/a/d;->o:J

    iget-wide v6, p0, Lcom/sec/chaton/chat/a/d;->m:J

    iget-object v8, p0, Lcom/sec/chaton/chat/a/d;->v:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/chat/a/d;->a(ZLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/d;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->S:Landroid/widget/TextView;

    .line 59
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 60
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 62
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->S:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 76
    :goto_0
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/chaton/chat/a/d;->m:J

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/multimedia/audio/b;->d(J)Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->x:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 81
    const v3, 0xea60

    div-int v3, v2, v3

    .line 82
    div-int/lit16 v2, v2, 0x3e8

    rem-int/lit8 v2, v2, 0x3c

    .line 84
    const-string v4, "%d:%02d/%d:%02d"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_1
    :goto_1
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/chat/a/d;->m:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/multimedia/audio/b;->e(J)Lcom/sec/chaton/multimedia/audio/h;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_2

    .line 94
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/audio/h;->a(Landroid/view/View;)V

    .line 97
    :cond_2
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/chat/a/d;->m:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/multimedia/audio/b;->c(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 98
    const v0, 0x7f02015a

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 103
    :goto_2
    const v0, 0x7f02011a

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 105
    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    return-void

    .line 65
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    .line 66
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->x:Landroid/widget/TextView;

    .line 68
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 69
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 71
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->x:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 85
    :catch_0
    move-exception v2

    .line 86
    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 100
    :cond_4
    const v0, 0x7f02011e

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 110
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/af;->a(Z)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/d;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    .line 128
    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 131
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    :goto_1
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method protected a_()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->a_()V

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 42
    return-void
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    const-string v0, ""

    return-object v0
.end method

.method public c_()V
    .locals 3

    .prologue
    .line 167
    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->c_()V

    .line 172
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/d;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/d;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/b;->a(J)V

    .line 179
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/d;->m:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/b;->a(J)V

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/chat/a/n;
    .locals 4

    .prologue
    .line 146
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/d;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->w:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/d;->g()Z

    move-result v0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ChatON"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Voice/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/af;->onClick(Landroid/view/View;)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/d;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/chat/a/d;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/d;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/d;->e:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/d;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v3, Lcom/sec/chaton/chat/el;->a:Landroid/view/View;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/chat/fk;->b(ZLandroid/database/Cursor;Landroid/view/View;)V

    goto :goto_0
.end method
