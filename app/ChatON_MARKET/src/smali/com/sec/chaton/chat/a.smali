.class public Lcom/sec/chaton/chat/a;
.super Ljava/lang/Object;
.source "AdaptableSwitch.java"


# instance fields
.field a:Landroid/widget/Switch;

.field b:Landroid/widget/CheckBox;

.field c:Lcom/sec/chaton/chat/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-object v0, Lcom/sec/chaton/chat/b;->a:Lcom/sec/chaton/chat/b;

    iput-object v0, p0, Lcom/sec/chaton/chat/a;->c:Lcom/sec/chaton/chat/b;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/CheckBox;)V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/chaton/chat/b;->b:Lcom/sec/chaton/chat/b;

    iput-object v0, p0, Lcom/sec/chaton/chat/a;->c:Lcom/sec/chaton/chat/b;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/a;->a:Landroid/widget/Switch;

    .line 26
    iput-object p1, p0, Lcom/sec/chaton/chat/a;->b:Landroid/widget/CheckBox;

    .line 27
    return-void
.end method

.method public a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->c:Lcom/sec/chaton/chat/b;

    sget-object v1, Lcom/sec/chaton/chat/b;->a:Lcom/sec/chaton/chat/b;

    if-ne v0, v1, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->a:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/Switch;)V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/chaton/chat/b;->a:Lcom/sec/chaton/chat/b;

    iput-object v0, p0, Lcom/sec/chaton/chat/a;->c:Lcom/sec/chaton/chat/b;

    .line 19
    iput-object p1, p0, Lcom/sec/chaton/chat/a;->a:Landroid/widget/Switch;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/a;->b:Landroid/widget/CheckBox;

    .line 21
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->c:Lcom/sec/chaton/chat/b;

    sget-object v1, Lcom/sec/chaton/chat/b;->a:Lcom/sec/chaton/chat/b;

    if-ne v0, v1, :cond_2

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->a:Landroid/widget/Switch;

    if-nez v0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->a:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0

    .line 51
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->b:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->c:Lcom/sec/chaton/chat/b;

    sget-object v1, Lcom/sec/chaton/chat/b;->a:Lcom/sec/chaton/chat/b;

    if-ne v0, v1, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->a:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    .line 65
    :goto_0
    return v0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0
.end method
