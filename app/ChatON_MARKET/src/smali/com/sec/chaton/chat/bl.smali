.class Lcom/sec/chaton/chat/bl;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 12359
    iput-object p1, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f070573

    const/4 v1, 0x1

    .line 12363
    check-cast p1, Landroid/widget/CheckedTextView;

    .line 12364
    invoke-virtual {p1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    .line 12366
    if-eqz v0, :cond_1

    .line 12367
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->b()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 12368
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aM(Lcom/sec/chaton/chat/ChatFragment;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 12372
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aN(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 12373
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->f()V

    .line 12379
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->invalidateViews()V

    .line 12380
    return-void

    .line 12370
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aM(Lcom/sec/chaton/chat/ChatFragment;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 12375
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aM(Lcom/sec/chaton/chat/ChatFragment;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 12376
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aN(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 12377
    iget-object v0, p0, Lcom/sec/chaton/chat/bl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->g()V

    goto :goto_1
.end method
