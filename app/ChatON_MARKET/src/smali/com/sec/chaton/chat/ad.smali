.class Lcom/sec/chaton/chat/ad;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/a/p;

.field final synthetic b:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/a/p;)V
    .locals 0

    .prologue
    .line 4720
    iput-object p1, p0, Lcom/sec/chaton/chat/ad;->b:Lcom/sec/chaton/chat/ChatFragment;

    iput-object p2, p0, Lcom/sec/chaton/chat/ad;->a:Lcom/sec/chaton/chat/a/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 4723
    iget-object v0, p0, Lcom/sec/chaton/chat/ad;->b:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 4725
    iget-object v0, p0, Lcom/sec/chaton/chat/ad;->a:Lcom/sec/chaton/chat/a/p;

    invoke-interface {v0}, Lcom/sec/chaton/chat/a/p;->f()Lcom/sec/chaton/chat/a/n;

    move-result-object v7

    .line 4727
    iget-object v0, p0, Lcom/sec/chaton/chat/ad;->b:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ad;->b:Lcom/sec/chaton/chat/ChatFragment;

    const v2, 0x7f0b0166

    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v7, Lcom/sec/chaton/chat/a/n;->a:Ljava/lang/String;

    iget-object v3, v7, Lcom/sec/chaton/chat/a/n;->b:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v7, Lcom/sec/chaton/chat/a/n;->c:Lcom/sec/chaton/e/w;

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)Landroid/content/Intent;

    move-result-object v1

    .line 4728
    if-eqz v1, :cond_1

    .line 4733
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "TrunkShareCheckPopup"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4734
    iget-object v0, v7, Lcom/sec/chaton/chat/a/n;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v2, :cond_0

    iget-object v0, v7, Lcom/sec/chaton/chat/a/n;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v2, :cond_3

    .line 4735
    :cond_0
    iget-object v0, v7, Lcom/sec/chaton/chat/a/n;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    .line 4739
    :goto_0
    if-eqz v0, :cond_2

    .line 4740
    iget-object v0, p0, Lcom/sec/chaton/chat/ad;->b:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/chat/ae;

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/chat/ae;-><init>(Lcom/sec/chaton/chat/ad;Landroid/content/Intent;)V

    invoke-static {v0, v2}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 4747
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 4753
    :cond_1
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 4749
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ad;->b:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_0
.end method
