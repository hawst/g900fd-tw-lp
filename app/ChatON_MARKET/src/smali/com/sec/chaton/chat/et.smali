.class public Lcom/sec/chaton/chat/et;
.super Ljava/lang/Object;
.source "DuringCallController.java"

# interfaces
.implements Lcom/coolots/sso/a/b;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/Button;

.field private i:Lcom/coolots/sso/a/a;

.field private j:Z

.field private k:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/chaton/chat/et;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/widget/LinearLayout;)V
    .locals 3

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/et;->j:Z

    .line 45
    new-instance v0, Lcom/sec/chaton/chat/eu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/eu;-><init>(Lcom/sec/chaton/chat/et;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->k:Landroid/os/Handler;

    .line 96
    iput-object p1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    .line 97
    iput-object p2, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0700f6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->d:Landroid/widget/ImageView;

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0700f7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->e:Landroid/widget/TextView;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0700f8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->f:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0700f9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->g:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0700fa

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->h:Landroid/widget/Button;

    .line 103
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    .line 107
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/et;->e()V

    .line 108
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/widget/LinearLayout;)Lcom/sec/chaton/chat/et;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/sec/chaton/chat/et;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/chat/et;-><init>(Landroid/app/Activity;Landroid/widget/LinearLayout;)V

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 257
    if-nez p1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->e:Landroid/widget/TextView;

    const-string v1, "UN Owen"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    :goto_0
    return-void

    .line 262
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 264
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 265
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->f:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 276
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/et;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/chaton/chat/et;->j:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/et;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/chaton/chat/et;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/et;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/chat/et;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/chat/et;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/chat/ev;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/ev;-><init>(Lcom/sec/chaton/chat/et;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->h:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/chat/ew;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/ew;-><init>(Lcom/sec/chaton/chat/et;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 173
    const-string v0, "[ChatONV] call online"

    sget-object v1, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->l(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->d:Landroid/widget/ImageView;

    const v1, 0x7f02044f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 182
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->m(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/et;->a(Ljava/util/ArrayList;)V

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->k:Landroid/os/Handler;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v4, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v3, v4}, Lcom/coolots/sso/a/a;->i(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v1, v2, v3, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->h:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 190
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->d:Landroid/widget/ImageView;

    const v1, 0x7f02044e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 194
    .line 195
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 200
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/chat/et;->h:Landroid/widget/Button;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 201
    iget-object v2, p0, Lcom/sec/chaton/chat/et;->k:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 203
    iget-object v2, p0, Lcom/sec/chaton/chat/et;->g:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b034a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/sec/chaton/util/co;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->g:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->k:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/chat/et;->k:Landroid/os/Handler;

    const/4 v3, 0x1

    const/4 v4, 0x6

    invoke-virtual {v2, v3, v4, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 211
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 240
    const-string v0, "[ChatONV] conference member change message received"

    sget-object v1, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-direct {p0}, Lcom/sec/chaton/chat/et;->f()V

    .line 242
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ChatONV] onReceiveChangeCallState : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/coolots/sso/a/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    sparse-switch p1, :sswitch_data_0

    .line 236
    :goto_0
    :sswitch_0
    return-void

    .line 227
    :sswitch_1
    const-string v0, "[ChatONV] connected message received"

    sget-object v1, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-direct {p0}, Lcom/sec/chaton/chat/et;->f()V

    goto :goto_0

    .line 232
    :sswitch_2
    const-string v0, "[ChatONV] disconnected message received"

    sget-object v1, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-direct {p0}, Lcom/sec/chaton/chat/et;->g()V

    goto :goto_0

    .line 218
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x64 -> :sswitch_1
        0x65 -> :sswitch_1
    .end sparse-switch
.end method

.method public b()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    if-nez v0, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/b;)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->k(Landroid/content/Context;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ChatONV] not connected : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v2, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lcom/coolots/sso/a/a;->k(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ChatONV] connected : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v2, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lcom/coolots/sso/a/a;->k(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/et;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-direct {p0}, Lcom/sec/chaton/chat/et;->f()V

    goto :goto_0

    .line 147
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x64 -> :sswitch_0
        0x65 -> :sswitch_0
    .end sparse-switch
.end method

.method public c()V
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->i:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/et;->b:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/b;)V

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->k:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/chat/et;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 170
    :cond_0
    return-void
.end method
