.class public Lcom/sec/chaton/chat/background/e;
.super Landroid/os/Handler;
.source "GetMessageJobContainer.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/background/d;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/background/d;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    .line 244
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 245
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 250
    if-nez p1, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 254
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 255
    if-nez v0, :cond_2

    .line 256
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 257
    const-string v0, "resultEntry is null"

    const-string v1, "GetMessageJonContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 262
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    .line 264
    sparse-switch v1, :sswitch_data_0

    .line 312
    :cond_3
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 266
    :sswitch_0
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_3

    .line 267
    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    goto :goto_1

    .line 274
    :sswitch_1
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_4

    .line 275
    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    goto :goto_1

    .line 278
    :cond_4
    const-string v1, "get message deliveryChat success"

    const-string v2, "GetMessageJonContainer"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v6, v3, v4}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v0, v0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, v1, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v2, v2, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    goto :goto_1

    .line 286
    :sswitch_2
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_5

    .line 287
    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    goto :goto_1

    .line 290
    :cond_5
    const-string v1, "get message ForwardOnlineMessage success"

    const-string v2, "GetMessageJonContainer"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v6, v3, v4}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    goto/16 :goto_1

    .line 298
    :sswitch_3
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_6

    .line 299
    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    goto/16 :goto_1

    .line 302
    :cond_6
    const-string v1, "get message ForwardStoredMessage success"

    const-string v2, "GetMessageJonContainer"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v6, v3, v4}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/e;->a:Lcom/sec/chaton/chat/background/d;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    goto/16 :goto_1

    .line 264
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
        0x22 -> :sswitch_2
        0x24 -> :sswitch_3
    .end sparse-switch
.end method
