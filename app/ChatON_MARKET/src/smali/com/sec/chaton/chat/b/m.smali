.class public final enum Lcom/sec/chaton/chat/b/m;
.super Ljava/lang/Enum;
.source "TransErrors.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/chat/b/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/chat/b/m;

.field public static final enum b:Lcom/sec/chaton/chat/b/m;

.field public static final enum c:Lcom/sec/chaton/chat/b/m;

.field public static final enum d:Lcom/sec/chaton/chat/b/m;

.field public static final enum e:Lcom/sec/chaton/chat/b/m;

.field public static final enum f:Lcom/sec/chaton/chat/b/m;

.field private static final synthetic h:[Lcom/sec/chaton/chat/b/m;


# instance fields
.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4
    new-instance v0, Lcom/sec/chaton/chat/b/m;

    const-string v1, "SUCCESS"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/chaton/chat/b/m;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/chat/b/m;->a:Lcom/sec/chaton/chat/b/m;

    .line 5
    new-instance v0, Lcom/sec/chaton/chat/b/m;

    const-string v1, "ERROR_NOT_REQUESTED"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/chaton/chat/b/m;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/chat/b/m;->b:Lcom/sec/chaton/chat/b/m;

    .line 6
    new-instance v0, Lcom/sec/chaton/chat/b/m;

    const-string v1, "ERROR_SERVER_ERROR"

    const-wide/16 v2, 0x2

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/chaton/chat/b/m;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/chat/b/m;->c:Lcom/sec/chaton/chat/b/m;

    .line 7
    new-instance v0, Lcom/sec/chaton/chat/b/m;

    const-string v1, "ERROR_KEY_EXPIRED"

    const-wide/16 v2, 0x3

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/chaton/chat/b/m;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/chat/b/m;->d:Lcom/sec/chaton/chat/b/m;

    .line 8
    new-instance v0, Lcom/sec/chaton/chat/b/m;

    const-string v1, "ERROR_FAILED_ABNORMAL"

    const-wide/16 v2, 0x4

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/chaton/chat/b/m;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/chat/b/m;->e:Lcom/sec/chaton/chat/b/m;

    .line 9
    new-instance v0, Lcom/sec/chaton/chat/b/m;

    const-string v1, "ERROR_LOCAL_EXCEPTION"

    const/4 v2, 0x5

    const-wide/16 v3, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/chat/b/m;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/chat/b/m;->f:Lcom/sec/chaton/chat/b/m;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/chaton/chat/b/m;

    sget-object v1, Lcom/sec/chaton/chat/b/m;->a:Lcom/sec/chaton/chat/b/m;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/chat/b/m;->b:Lcom/sec/chaton/chat/b/m;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/chat/b/m;->c:Lcom/sec/chaton/chat/b/m;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/chat/b/m;->d:Lcom/sec/chaton/chat/b/m;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/chaton/chat/b/m;->e:Lcom/sec/chaton/chat/b/m;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/chat/b/m;->f:Lcom/sec/chaton/chat/b/m;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/chat/b/m;->h:[Lcom/sec/chaton/chat/b/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput-wide p3, p0, Lcom/sec/chaton/chat/b/m;->g:J

    .line 15
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/chat/b/m;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/chaton/chat/b/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/b/m;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/chat/b/m;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/chaton/chat/b/m;->h:[Lcom/sec/chaton/chat/b/m;

    invoke-virtual {v0}, [Lcom/sec/chaton/chat/b/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/chat/b/m;

    return-object v0
.end method
