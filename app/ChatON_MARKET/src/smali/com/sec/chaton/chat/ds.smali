.class Lcom/sec/chaton/chat/ds;
.super Ljava/lang/Object;
.source "ChatInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/sec/chaton/chat/ds;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 535
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 536
    const/4 v3, -0x3

    if-eq v3, v1, :cond_0

    const/4 v3, -0x2

    if-ne v3, v1, :cond_2

    .line 539
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ds;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0205

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 578
    :cond_1
    :goto_0
    return-void

    .line 544
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/chat/ds;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->p(Lcom/sec/chaton/chat/ChatInfoFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 548
    invoke-static {}, Lcom/sec/chaton/e/i;->a()Landroid/net/Uri;

    move-result-object v3

    .line 550
    const-string v5, ""

    .line 551
    const-string v1, ""

    .line 553
    iget-object v1, p0, Lcom/sec/chaton/chat/ds;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->k(Lcom/sec/chaton/chat/ChatInfoFragment;)[Ljava/lang/String;

    move-result-object v1

    .line 554
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 556
    array-length v6, v1

    if-lez v6, :cond_4

    .line 557
    const-string v5, ""

    .line 558
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "buddy_no NOT IN ( "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 559
    :goto_1
    array-length v6, v1

    if-ge v0, v6, :cond_3

    .line 560
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 559
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 563
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 567
    :cond_4
    const-string v7, "group_type, CASE WHEN group_name IS NULL THEN 1 ELSE 0 END,group_name, buddy_name COLLATE LOCALIZED ASC"

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/chat/ds;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->p(Lcom/sec/chaton/chat/ChatInfoFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/16 v1, 0x46

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
