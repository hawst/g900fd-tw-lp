.class public Lcom/sec/chaton/chat/ChatFragment;
.super Landroid/support/v4/app/Fragment;
.source "ChatFragment.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/chaton/base/d;
.implements Lcom/sec/chaton/chat/fi;
.implements Lcom/sec/chaton/chat/fk;
.implements Lcom/sec/chaton/f/f;
.implements Lcom/sec/chaton/multimedia/emoticon/i;
.implements Lcom/sec/widget/ak;


# static fields
.field public static final a:Ljava/lang/String;

.field private static cL:Ljava/lang/String;

.field private static cM:Ljava/lang/String;

.field private static cZ:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Ljava/lang/String;

.field public static m:Ljava/lang/String;

.field public static n:Ljava/lang/String;

.field static o:Z


# instance fields
.field A:Ljava/lang/String;

.field B:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;"
        }
    .end annotation
.end field

.field public C:Landroid/app/ProgressDialog;

.field public D:Landroid/app/ProgressDialog;

.field public E:Z

.field F:Lcom/sec/common/a/d;

.field G:Lcom/sec/common/a/d;

.field H:Ljava/lang/String;

.field public I:Z

.field J:Lcom/sec/chaton/f/a;

.field K:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/f/g;",
            ">;"
        }
    .end annotation
.end field

.field L:Ljava/io/File;

.field M:Landroid/database/ContentObserver;

.field N:Landroid/database/ContentObserver;

.field O:Landroid/os/Handler;

.field P:Landroid/database/ContentObserver;

.field Q:Landroid/database/ContentObserver;

.field R:Landroid/os/Handler;

.field S:Lcom/sec/chaton/e/a/v;

.field T:Landroid/view/View$OnClickListener;

.field U:Landroid/os/Handler;

.field V:Landroid/os/Handler;

.field W:Lcom/sec/chaton/e/b/d;

.field private final X:Ljava/lang/Object;

.field private Y:Lcom/sec/chaton/e/a/u;

.field private Z:Landroid/view/Menu;

.field private aA:Lcom/sec/chaton/chat/ei;

.field private aB:Landroid/widget/Toast;

.field private aC:Landroid/app/ProgressDialog;

.field private aD:Lcom/sec/chaton/d/o;

.field private aE:Lcom/sec/chaton/trunk/a/a;

.field private aF:I

.field private aG:Ljava/lang/String;

.field private aH:Ljava/lang/String;

.field private aI:Z

.field private aJ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aK:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private aL:Ljava/lang/Long;

.field private aM:Ljava/lang/String;

.field private aN:Ljava/lang/String;

.field private aO:Ljava/lang/String;

.field private aP:Ljava/lang/String;

.field private aQ:Ljava/lang/String;

.field private aR:Ljava/lang/String;

.field private aS:Ljava/lang/String;

.field private aT:Ljava/lang/String;

.field private aU:J

.field private aV:Ljava/lang/String;

.field private aW:I

.field private aX:I

.field private aY:Z

.field private aZ:Z

.field private aa:Z

.field private ab:Lcom/coolots/sso/a/a;

.field private ac:Ljava/io/File;

.field private ad:Lcom/sec/chaton/e/w;

.field private ae:Lcom/sec/chaton/multimedia/emoticon/ams/d;

.field private af:Landroid/widget/LinearLayout;

.field private ag:Landroid/widget/ImageView;

.field private ah:Landroid/widget/ImageView;

.field private ai:Landroid/widget/ImageButton;

.field private aj:Z

.field private ak:Z

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Landroid/widget/ImageButton;

.field private ao:Landroid/widget/ImageButton;

.field private ap:Landroid/widget/CheckedTextView;

.field private aq:Landroid/widget/FrameLayout;

.field private ar:Landroid/widget/FrameLayout;

.field private as:Landroid/view/View;

.field private at:Landroid/widget/LinearLayout;

.field private au:Landroid/widget/Button;

.field private av:Landroid/widget/Button;

.field private aw:Landroid/widget/Button;

.field private ax:Landroid/widget/TextView;

.field private ay:Landroid/widget/LinearLayout;

.field private az:Lcom/sec/widget/HeightChangedListView;

.field b:Landroid/view/View;

.field private bA:Lcom/sec/chaton/w;

.field private bB:Landroid/graphics/Bitmap;

.field private bC:Landroid/widget/ImageButton;

.field private bD:Landroid/widget/ImageButton;

.field private bE:Landroid/view/inputmethod/InputMethodManager;

.field private bF:Landroid/widget/ImageView;

.field private bG:Landroid/view/ViewGroup;

.field private bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

.field private bI:Landroid/os/Bundle;

.field private bJ:I

.field private bK:Lcom/sec/common/a/a;

.field private bL:Landroid/view/View;

.field private bM:I

.field private bN:Z

.field private bO:Z

.field private final bP:I

.field private final bQ:I

.field private bR:I

.field private bS:Z

.field private bT:Ljava/lang/String;

.field private bU:Lcom/sec/common/f/c;

.field private bV:Lcom/sec/chaton/widget/c;

.field private bW:Landroid/content/Intent;

.field private bX:Landroid/widget/ImageButton;

.field private bY:Landroid/widget/LinearLayout;

.field private bZ:Lcom/sec/chaton/multimedia/audio/k;

.field private ba:Ljava/lang/String;

.field private bb:I

.field private bc:Landroid/net/Uri;

.field private bd:Z

.field private be:Z

.field private bf:J

.field private bg:Z

.field private bh:Lcom/sec/chaton/chat/MyEditText;

.field private bi:Ljava/lang/String;

.field private bj:Ljava/lang/String;

.field private bk:Ljava/lang/String;

.field private bl:Ljava/lang/String;

.field private bm:Ljava/lang/String;

.field private bn:Ljava/lang/String;

.field private bo:Z

.field private bp:Z

.field private bq:I

.field private br:Ljava/lang/String;

.field private bs:Ljava/lang/String;

.field private bt:Ljava/lang/String;

.field private bu:Z

.field private bv:Ljava/lang/String;

.field private bw:Z

.field private bx:Z

.field private by:Z

.field private bz:Z

.field c:Lcom/sec/chaton/widget/ProfileImageView;

.field private cA:Landroid/view/View;

.field private cB:Landroid/view/View;

.field private cC:Landroid/widget/ImageView;

.field private cD:Landroid/view/ViewGroup;

.field private cE:Landroid/widget/TextView;

.field private cF:Landroid/widget/TextView;

.field private cG:Landroid/widget/LinearLayout;

.field private cH:Landroid/view/View;

.field private cI:Ljava/lang/String;

.field private cJ:Ljava/lang/String;

.field private cK:Z

.field private cN:Z

.field private cO:Z

.field private cP:Z

.field private cQ:Landroid/widget/ImageView;

.field private cR:Landroid/widget/ImageView;

.field private cS:Ljava/lang/String;

.field private cT:Ljava/lang/String;

.field private cU:Ljava/lang/String;

.field private cV:Lcom/sec/chaton/chat/eo;

.field private cW:Landroid/app/ProgressDialog;

.field private cX:Z

.field private cY:Z

.field private ca:Landroid/widget/FrameLayout;

.field private cb:Landroid/widget/LinearLayout;

.field private cc:Landroid/database/DataSetObserver;

.field private cd:Lcom/sec/chaton/multimedia/audio/a;

.field private final ce:Ljava/lang/String;

.field private final cf:Ljava/lang/String;

.field private cg:Landroid/widget/LinearLayout;

.field private ch:Landroid/widget/ImageView;

.field private ci:Landroid/widget/TextView;

.field private cj:Landroid/app/Dialog;

.field private ck:Lorg/a/a/a/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/a/j",
            "<",
            "Lcom/sec/chaton/e/w;",
            "Lcom/sec/chaton/chat/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private cl:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private cm:Landroid/app/Activity;

.field private cn:Z

.field private co:Ljava/lang/String;

.field private cp:Landroid/graphics/Bitmap;

.field private cq:Ljava/lang/String;

.field private final cr:I

.field private cs:Landroid/view/View$OnDragListener;

.field private ct:Landroid/view/View$OnDragListener;

.field private cu:I

.field private cv:Ljava/lang/String;

.field private cw:Z

.field private cx:Z

.field private cy:Z

.field private cz:Z

.field d:Landroid/widget/TextView;

.field private da:Lcom/sec/chaton/chat/et;

.field private db:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dc:Landroid/widget/RelativeLayout;

.field private dd:Z

.field private de:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

.field private df:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

.field private dg:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

.field private dh:Lcom/sec/chaton/chat/dg;

.field private di:Landroid/graphics/Bitmap;

.field private dj:Z

.field private dk:Lcom/sec/chaton/chat/cv;

.field private dl:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dm:Landroid/content/BroadcastReceiver;

.field private dn:Landroid/os/Handler;

.field private do:Landroid/view/View$OnClickListener;

.field private dp:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

.field public p:Z

.field public q:Ljava/lang/Boolean;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Z

.field public u:Z

.field public v:Ljava/lang/String;

.field public w:I

.field x:Lcom/sec/chaton/e/r;

.field y:Ljava/lang/String;

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 325
    const-class v0, Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    .line 527
    const-string v0, "extra fileuri"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->e:Ljava/lang/String;

    .line 528
    const-string v0, "extra participant"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    .line 529
    const-string v0, "extra title"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    .line 530
    const-string v0, "extra vcalendar"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->h:Ljava/lang/String;

    .line 531
    const-string v0, "extra vard name"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->i:Ljava/lang/String;

    .line 532
    const-string v0, "extra initchat"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->j:Ljava/lang/String;

    .line 533
    const-string v0, "extra showinput"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    .line 534
    const-string v0, "extra chagetitle"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->l:Ljava/lang/String;

    .line 535
    const-string v0, "extra invite"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->m:Ljava/lang/String;

    .line 537
    const-string v0, "extra inbox nobi"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    .line 567
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/chat/ChatFragment;->o:Z

    .line 816
    const-string v0, "Korean"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->cL:Ljava/lang/String;

    .line 817
    const-string v0, "English US"

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->cM:Ljava/lang/String;

    .line 830
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->cZ:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x1e

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 319
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 331
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    .line 405
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    .line 408
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    .line 409
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    .line 411
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ae:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    .line 418
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    .line 568
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->p:Z

    .line 570
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->q:Ljava/lang/Boolean;

    .line 571
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aC:Landroid/app/ProgressDialog;

    .line 580
    iput v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    .line 581
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aG:Ljava/lang/String;

    .line 582
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aH:Ljava/lang/String;

    .line 583
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aI:Z

    .line 584
    iput-boolean v4, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 585
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->u:Z

    .line 586
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    .line 587
    iput v2, p0, Lcom/sec/chaton/chat/ChatFragment;->w:I

    .line 599
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aU:J

    .line 600
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    .line 605
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aY:Z

    .line 606
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aZ:Z

    .line 645
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bw:Z

    .line 646
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bx:Z

    .line 647
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->by:Z

    .line 648
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bz:Z

    .line 649
    sget-object v0, Lcom/sec/chaton/w;->e:Lcom/sec/chaton/w;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bA:Lcom/sec/chaton/w;

    .line 683
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->B:Ljava/util/ArrayList;

    .line 696
    iput-boolean v4, p0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    .line 703
    iput v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bM:I

    .line 705
    iput-boolean v4, p0, Lcom/sec/chaton/chat/ChatFragment;->bN:Z

    .line 708
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bO:Z

    .line 712
    iput v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bP:I

    .line 713
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bQ:I

    .line 714
    iput v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bR:I

    .line 715
    iput-boolean v4, p0, Lcom/sec/chaton/chat/ChatFragment;->bS:Z

    .line 719
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->F:Lcom/sec/common/a/d;

    .line 720
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->G:Lcom/sec/common/a/d;

    .line 725
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->H:Ljava/lang/String;

    .line 749
    const-string v0, "TRANS_FAIL"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ce:Ljava/lang/String;

    .line 751
    const-string v0, "RECV_SUCC"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cf:Ljava/lang/String;

    .line 788
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    .line 789
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/flag/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cq:Ljava/lang/String;

    .line 790
    iput v4, p0, Lcom/sec/chaton/chat/ChatFragment;->cr:I

    .line 797
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cw:Z

    .line 798
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cx:Z

    .line 801
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    .line 802
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cz:Z

    .line 815
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cK:Z

    .line 818
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    .line 822
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->L:Ljava/io/File;

    .line 827
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cW:Landroid/app/ProgressDialog;

    .line 828
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cX:Z

    .line 829
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cY:Z

    .line 835
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->db:Ljava/util/ArrayList;

    .line 850
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->dj:Z

    .line 852
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    .line 884
    new-instance v0, Lcom/sec/chaton/chat/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/c;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dm:Landroid/content/BroadcastReceiver;

    .line 2095
    new-instance v0, Lcom/sec/chaton/chat/k;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/k;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dn:Landroid/os/Handler;

    .line 6863
    new-instance v0, Lcom/sec/chaton/chat/as;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/as;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->M:Landroid/database/ContentObserver;

    .line 6878
    new-instance v0, Lcom/sec/chaton/chat/at;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/at;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->N:Landroid/database/ContentObserver;

    .line 6941
    new-instance v0, Lcom/sec/chaton/chat/au;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/au;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->O:Landroid/os/Handler;

    .line 6980
    new-instance v0, Lcom/sec/chaton/chat/av;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/av;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->P:Landroid/database/ContentObserver;

    .line 6990
    new-instance v0, Lcom/sec/chaton/chat/aw;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/aw;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Q:Landroid/database/ContentObserver;

    .line 7026
    new-instance v0, Lcom/sec/chaton/chat/ax;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ax;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    .line 7616
    new-instance v0, Lcom/sec/chaton/chat/ay;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ay;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->S:Lcom/sec/chaton/e/a/v;

    .line 12038
    new-instance v0, Lcom/sec/chaton/chat/bk;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/bk;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->T:Landroid/view/View$OnClickListener;

    .line 12359
    new-instance v0, Lcom/sec/chaton/chat/bl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/bl;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->do:Landroid/view/View$OnClickListener;

    .line 13317
    new-instance v0, Lcom/sec/chaton/chat/br;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/br;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->U:Landroid/os/Handler;

    .line 14143
    new-instance v0, Lcom/sec/chaton/chat/cc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/cc;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->V:Landroid/os/Handler;

    .line 14423
    new-instance v0, Lcom/sec/chaton/chat/ce;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ce;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->W:Lcom/sec/chaton/e/b/d;

    .line 16025
    new-instance v0, Lcom/sec/chaton/chat/cm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/cm;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dp:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 16322
    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bY:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->cM:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ca:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private B()V
    .locals 2

    .prologue
    .line 879
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setDescendantFocusability(I)V

    .line 882
    :cond_0
    return-void
.end method

.method static synthetic C(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cb:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private C()V
    .locals 2

    .prologue
    .line 1917
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dl:Ljava/util/ArrayList;

    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1918
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dl:Ljava/util/ArrayList;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1921
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dl:Ljava/util/ArrayList;

    const-string v1, "video/"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1922
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dl:Ljava/util/ArrayList;

    const-string v1, "audio/"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1923
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dl:Ljava/util/ArrayList;

    const-string v1, "application/"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1925
    return-void
.end method

.method static synthetic D(Lcom/sec/chaton/chat/ChatFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dn:Landroid/os/Handler;

    return-object v0
.end method

.method private D()V
    .locals 6

    .prologue
    .line 2048
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    .line 2050
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b009a

    const v4, 0x7f020194

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2051
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2052
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b009b

    const v4, 0x7f020190

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2054
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0099

    const v4, 0x7f02018e

    const/16 v5, 0x9

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2055
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b009c

    const v4, 0x7f020199

    const/4 v5, 0x2

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2056
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2057
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b009d

    const v4, 0x7f020197

    const/4 v5, 0x3

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2060
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004b

    const v4, 0x7f02019a

    const/4 v5, 0x4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2062
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b009e

    const v4, 0x7f020191

    const/4 v5, 0x6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2063
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a2

    const v4, 0x7f020192

    const/16 v5, 0xa

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2064
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2065
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a1

    const v4, 0x7f020195

    const/16 v5, 0x8

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2067
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a0

    const v4, 0x7f02018f

    const/4 v5, 0x7

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2070
    invoke-static {}, Lcom/sec/chaton/c/a;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2071
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/f/g;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a3

    const v4, 0x7f020198

    const/16 v5, 0xb

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/f/g;-><init>(Landroid/content/res/Resources;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2074
    :cond_3
    return-void
.end method

.method static synthetic E(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/multimedia/audio/k;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bZ:Lcom/sec/chaton/multimedia/audio/k;

    return-object v0
.end method

.method private E()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2078
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bY:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2079
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ca:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2080
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cb:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2082
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->I:Z

    .line 2083
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bZ:Lcom/sec/chaton/multimedia/audio/k;

    if-eqz v0, :cond_0

    .line 2084
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bZ:Lcom/sec/chaton/multimedia/audio/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/k;->a(Z)V

    .line 2086
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 2087
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2088
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 2090
    :cond_1
    return-void
.end method

.method static synthetic F(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/dg;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    return-object v0
.end method

.method private F()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2554
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 2555
    const-string v0, "[initializeChatRoom] attach activity is null. return"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2626
    :goto_0
    return-void

    .line 2559
    :cond_0
    iput-boolean v4, p0, Lcom/sec/chaton/chat/ChatFragment;->bO:Z

    .line 2560
    iput-boolean v4, p0, Lcom/sec/chaton/chat/ChatFragment;->cX:Z

    .line 2562
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ag()V

    .line 2566
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ab()V

    .line 2570
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->O:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2573
    invoke-static {p0}, Lcom/sec/chaton/receiver/PushReceiver;->a(Lcom/sec/chaton/chat/fi;)V

    .line 2574
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->N:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2578
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->Q:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2608
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/a;->a()V

    .line 2609
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->G()V

    .line 2611
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->da:Lcom/sec/chaton/chat/et;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/et;->b()V

    .line 2612
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_1

    .line 2613
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-static {v0}, Lcom/sec/chaton/chat/b/i;->a(Landroid/widget/ListView;)V

    .line 2614
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->b()V

    .line 2617
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2618
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->c()V

    .line 2622
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2623
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aB()V

    .line 2625
    :cond_2
    iput-boolean v4, p0, Lcom/sec/chaton/chat/ChatFragment;->cx:Z

    goto :goto_0
.end method

.method static synthetic G(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/ei;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    return-object v0
.end method

.method private G()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v4, 0x8

    .line 2630
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bp:Z

    if-eqz v2, :cond_5

    .line 2631
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    if-nez v2, :cond_2

    .line 2632
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2633
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v2}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 2634
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2635
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2636
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2685
    :goto_0
    return-void

    .line 2638
    :cond_0
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-eqz v2, :cond_1

    .line 2639
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2640
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2641
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2642
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 2644
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2645
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2646
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 2650
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2651
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 2652
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2653
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 2655
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2656
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 2660
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2661
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    if-nez v2, :cond_8

    .line 2662
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v2}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_6

    .line 2663
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2664
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2665
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 2667
    :cond_6
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-eqz v2, :cond_7

    .line 2668
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2669
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2670
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 2672
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2673
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2674
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 2678
    :cond_8
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-gtz v3, :cond_9

    iget-boolean v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-eqz v3, :cond_a

    :cond_9
    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2679
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2680
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 2678
    goto :goto_1
.end method

.method static synthetic H(Lcom/sec/chaton/chat/ChatFragment;)Landroid/database/DataSetObserver;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cc:Landroid/database/DataSetObserver;

    return-object v0
.end method

.method private H()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2855
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2856
    const-string v0, "inbox_unread_count"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2857
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {}, Lcom/sec/chaton/e/q;->d()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "inbox_no = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 2858
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;)J

    .line 2859
    return-void
.end method

.method static synthetic I(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->P()V

    return-void
.end method

.method private I()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2865
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v1, v2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    .line 2866
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v1, :cond_0

    .line 2867
    const-string v1, "false : mMessageControl null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2897
    :goto_0
    return v0

    .line 2871
    :cond_0
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bO:Z

    if-eqz v1, :cond_1

    .line 2872
    const-string v1, "false : already paused"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2876
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/notification/a;->b(Ljava/lang/String;)V

    .line 2878
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 2879
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    .line 2880
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-wide v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aU:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->a(J)V

    .line 2884
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2885
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2886
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 2892
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    if-eqz v0, :cond_3

    .line 2893
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->J()V

    .line 2897
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic J(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    return-object v0
.end method

.method private J()V
    .locals 3

    .prologue
    .line 2902
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    const-string v1, "requestCode"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2904
    packed-switch v0, :pswitch_data_0

    .line 2958
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    .line 2959
    return-void

    .line 2907
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 2911
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/net/Uri;Z)V

    goto :goto_0

    .line 2915
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->j(Landroid/content/Intent;)V

    goto :goto_0

    .line 2919
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/net/Uri;Z)V

    goto :goto_0

    .line 2924
    :pswitch_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->i(Landroid/content/Intent;)V

    goto :goto_0

    .line 2928
    :pswitch_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->h(Landroid/content/Intent;)V

    goto :goto_0

    .line 2932
    :pswitch_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->g(Landroid/content/Intent;)V

    goto :goto_0

    .line 2936
    :pswitch_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->f(Landroid/content/Intent;)V

    goto :goto_0

    .line 2940
    :pswitch_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 2944
    :pswitch_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->c(Landroid/content/Intent;)V

    goto :goto_0

    .line 2948
    :pswitch_b
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->e(Landroid/content/Intent;)V

    goto :goto_0

    .line 2952
    :pswitch_c
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->d(Landroid/content/Intent;)V

    goto :goto_0

    .line 2904
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_1
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic K(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    return-object v0
.end method

.method private K()V
    .locals 9

    .prologue
    const/4 v1, -0x1

    .line 2970
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2971
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2975
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2976
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2979
    const-string v2, ""

    .line 2980
    const-string v0, ""

    .line 2982
    iget-boolean v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-eqz v3, :cond_4

    .line 2983
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    sget-object v6, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v3, v6, :cond_0

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    sget-object v6, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v3, v6, :cond_0

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    sget-object v6, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v3, v6, :cond_3

    .line 2984
    :cond_0
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    if-eqz v3, :cond_4

    .line 2985
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v3

    .line 2986
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 2987
    iget-boolean v6, p0, Lcom/sec/chaton/chat/ChatFragment;->aj:Z

    if-eqz v6, :cond_1

    .line 2988
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->al:Ljava/lang/String;

    .line 2996
    :cond_1
    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ";"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2998
    const-string v0, "inbox_last_temp_msg"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2999
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const-string v5, "inbox_no=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 3001
    :cond_2
    return-void

    .line 2991
    :cond_3
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    sget-object v6, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-ne v3, v6, :cond_4

    .line 2992
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v3

    .line 2993
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ba:Ljava/lang/String;

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_0
.end method

.method private L()V
    .locals 6

    .prologue
    const v5, 0x7f070571

    const v1, 0x7f070565

    const v4, 0x7f070573

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3004
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    if-eqz v0, :cond_2

    .line 3005
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 3006
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v5, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 3008
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    if-eqz v0, :cond_1

    .line 3009
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->d()Lcom/sec/chaton/chat/em;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/em;->a:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_0

    .line 3010
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3024
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->M()V

    .line 3027
    return-void

    .line 3012
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 3015
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 3019
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v1, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 3020
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v5, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0
.end method

.method static synthetic L(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    return v0
.end method

.method static synthetic M(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    return-object v0
.end method

.method private M()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3190
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    if-nez v0, :cond_1

    .line 3191
    const-string v0, "mChatRoomMenu is null"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3226
    :cond_0
    :goto_0
    return-void

    .line 3195
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    const v1, 0x7f070570

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3198
    if-eqz v0, :cond_0

    .line 3203
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3204
    :cond_2
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3207
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_4

    .line 3209
    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aW:I

    if-lez v1, :cond_5

    .line 3210
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;Z)V

    .line 3221
    :cond_4
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3223
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 3212
    :cond_5
    invoke-static {v0, v2}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;Z)V

    goto :goto_1
.end method

.method static synthetic N(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    return-object v0
.end method

.method private N()V
    .locals 2

    .prologue
    .line 3229
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3243
    :cond_0
    :goto_0
    return-void

    .line 3233
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    if-nez v0, :cond_2

    .line 3234
    const-string v0, "mChatRoomMenu is null"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3238
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    const v1, 0x7f070570

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3240
    if-eqz v0, :cond_0

    .line 3241
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private O()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 3246
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 3378
    :goto_0
    return-void

    .line 3251
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3253
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070118

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 3254
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3255
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v2, 0x7f07011a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/widget/ProfileImageView;

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->c:Lcom/sec/chaton/widget/ProfileImageView;

    .line 3256
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v2, 0x7f07011e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    .line 3257
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v2, 0x7f070120

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    .line 3258
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v2, 0x7f070122

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->d:Landroid/widget/TextView;

    .line 3260
    new-instance v1, Lcom/sec/chaton/chat/o;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/o;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3267
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 3268
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->c:Lcom/sec/chaton/widget/ProfileImageView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 3294
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 3295
    new-instance v1, Lcom/sec/chaton/chat/q;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/q;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3320
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3321
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 3326
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aW:I

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/widget/TextView;I)V

    .line 3332
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 3334
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    if-nez v0, :cond_3

    .line 3335
    const v0, 0x7f030003

    invoke-virtual {v1, v0, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    .line 3340
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3341
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/sec/common/actionbar/a;->a(Landroid/view/View;)V

    .line 3342
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    if-nez v0, :cond_4

    .line 3343
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    const v2, 0x7f07001f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    .line 3346
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cD:Landroid/view/ViewGroup;

    if-nez v0, :cond_5

    .line 3347
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    const v2, 0x7f070020

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cD:Landroid/view/ViewGroup;

    .line 3350
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    if-nez v0, :cond_6

    .line 3351
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    const v2, 0x7f070021

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    .line 3354
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cC:Landroid/widget/ImageView;

    if-nez v0, :cond_7

    .line 3355
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    const v2, 0x7f070022

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cC:Landroid/widget/ImageView;

    .line 3360
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cG:Landroid/widget/LinearLayout;

    if-nez v0, :cond_8

    .line 3361
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cA:Landroid/view/View;

    const v2, 0x7f07001d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cG:Landroid/widget/LinearLayout;

    .line 3362
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cG:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 3363
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cG:Landroid/widget/LinearLayout;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5, v5, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 3364
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cG:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/chaton/chat/r;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/r;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3373
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cB:Landroid/view/View;

    if-nez v0, :cond_9

    .line 3374
    const v0, 0x7f030004

    invoke-virtual {v1, v0, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cB:Landroid/view/View;

    .line 3377
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->e(I)V

    goto/16 :goto_0

    .line 3271
    :cond_a
    sget-object v0, Lcom/sec/chaton/util/bw;->c:Lcom/sec/chaton/util/bw;

    .line 3274
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_b

    .line 3275
    sget-object v0, Lcom/sec/chaton/util/bw;->b:Lcom/sec/chaton/util/bw;

    .line 3279
    :cond_b
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 3280
    const-string v0, ""

    .line 3281
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 3282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3286
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->c:Lcom/sec/chaton/widget/ProfileImageView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    goto/16 :goto_1

    .line 3284
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    goto :goto_2

    .line 3288
    :cond_d
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->c:Lcom/sec/chaton/widget/ProfileImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_1
.end method

.method static synthetic O(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cK:Z

    return v0
.end method

.method static synthetic P(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aR:Ljava/lang/String;

    return-object v0
.end method

.method private P()V
    .locals 5

    .prologue
    .line 3386
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    if-eqz v0, :cond_1

    .line 3434
    :cond_0
    :goto_0
    return-void

    .line 3390
    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/chat/ChatInfoActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3391
    const-string v0, "ACTIVITY_PURPOSE"

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3401
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    .line 3403
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->j:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3404
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3405
    const-string v0, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3406
    const-string v0, "chatType"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3407
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->l:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bz:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3408
    const-string v0, "buddyNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3411
    const-string v0, "inboxValid"

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3413
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3414
    const-string v0, "ACTIVITY_PURPOSE_ARG"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3415
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v2

    .line 3416
    :try_start_0
    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 3417
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3418
    const-string v0, "inbox_title_fixed"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aQ:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3419
    const-string v0, "inbox_is_change_skin"

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->u:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3420
    const-string v0, "inbox_background_style"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3421
    const-string v0, "inbox_send_bubble_style"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3422
    const-string v0, "inbox_receive_bubble_style"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3423
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3424
    const-string v0, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3429
    :cond_2
    const-string v0, "sessionID"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3432
    const/16 v0, 0x11

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 3417
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic Q(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->G()V

    return-void
.end method

.method private Q()Z
    .locals 4

    .prologue
    .line 3920
    const/4 v1, 0x0

    .line 3923
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3924
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3929
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3930
    return v0

    .line 3925
    :catch_0
    move-exception v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 3926
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3925
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aC:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private R()Z
    .locals 3

    .prologue
    .line 3938
    const/4 v0, 0x0

    .line 3941
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 3942
    const/4 v0, 0x1

    .line 3948
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVInstalled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3949
    return v0

    .line 3944
    :catch_0
    move-exception v1

    .line 3945
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic S(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aP:Ljava/lang/String;

    return-object v0
.end method

.method private S()Z
    .locals 4

    .prologue
    .line 3957
    const/4 v0, 0x0

    .line 3960
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 3961
    const/4 v0, 0x1

    .line 3967
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVAvaiable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3968
    return v0

    .line 3963
    :catch_0
    move-exception v1

    .line 3964
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    return-object v0
.end method

.method private T()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 5082
    new-instance v0, Lcom/sec/chaton/chat/ar;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ar;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 5089
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 5090
    if-eqz v2, :cond_0

    array-length v0, v2

    if-lez v0, :cond_0

    .line 5091
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 5092
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v5

    .line 5093
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Delete File] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5091
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5101
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 5102
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 5104
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 5106
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 5108
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/ct;

    invoke-interface {v0}, Lcom/sec/chaton/chat/ct;->c()V

    .line 5115
    :goto_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0167

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 5116
    return-void

    .line 5101
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 5110
    :cond_2
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Ljava/lang/String;)V

    .line 5111
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    .line 5112
    const v2, 0x7f0704a1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-class v4, Lcom/sec/chaton/chat/EmptyChatFragment;

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_2
.end method

.method private U()V
    .locals 2

    .prologue
    .line 5162
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 5164
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->B()V

    .line 5167
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 5168
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->J:Lcom/sec/chaton/f/a;

    if-nez v0, :cond_0

    .line 5169
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->D()V

    .line 5170
    new-instance v0, Lcom/sec/chaton/f/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->K:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/f/a;-><init>(Lcom/sec/chaton/chat/ChatFragment;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->J:Lcom/sec/chaton/f/a;

    .line 5173
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->J:Lcom/sec/chaton/f/a;

    invoke-virtual {v0}, Lcom/sec/chaton/f/a;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5174
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->J:Lcom/sec/chaton/f/a;

    invoke-virtual {v0}, Lcom/sec/chaton/f/a;->show()V

    .line 5177
    :cond_1
    return-void
.end method

.method static synthetic U(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->T()V

    return-void
.end method

.method private V()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 6308
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 6309
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6310
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    .line 6311
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    .line 6312
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    .line 6313
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->al:Ljava/lang/String;

    .line 6314
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->am:Ljava/lang/String;

    .line 6315
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aj:Z

    .line 6318
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 6320
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 6321
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bp:Z

    if-eqz v0, :cond_1

    .line 6322
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6323
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6324
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6340
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6341
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6342
    iput-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ba:Ljava/lang/String;

    .line 6343
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->K()V

    .line 6344
    return-void

    .line 6326
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    if-nez v0, :cond_2

    .line 6327
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6328
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6329
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 6332
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6333
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6334
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic V(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aZ:Z

    return v0
.end method

.method static synthetic W(Lcom/sec/chaton/chat/ChatFragment;)I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aX:I

    return v0
.end method

.method private W()V
    .locals 4

    .prologue
    .line 10952
    const v0, 0x7f0d000c

    .line 10954
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 10956
    const v2, 0x7f0b02b4

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/chat/bg;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/bg;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v2, v0, v3}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 10987
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 10990
    return-void
.end method

.method private X()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 11101
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11102
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b00c4

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 11128
    :cond_1
    :goto_0
    return-void

    .line 11106
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 11107
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 11111
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 11112
    const-string v1, "android.intent.extra.videoQuality"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 11113
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 11114
    const-wide/32 v1, 0x980000

    .line 11115
    const-string v3, "android.intent.extra.sizeLimit"

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 11116
    const-string v1, "video-size"

    const-string v2, "720x480"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11120
    const/16 v1, 0xc

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 11121
    :catch_0
    move-exception v0

    .line 11122
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 11123
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 11124
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic X(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    return v0
.end method

.method private Y()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 11131
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 11132
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 11134
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 11135
    const-string v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 11139
    const/16 v1, 0xb

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11147
    :cond_1
    :goto_0
    return-void

    .line 11140
    :catch_0
    move-exception v0

    .line 11141
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 11142
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 11143
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic Y(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aE()V

    return-void
.end method

.method static synthetic Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    return-object v0
.end method

.method private Z()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 11150
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11151
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b00c4

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 11198
    :cond_1
    :goto_0
    return-void

    .line 11155
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 11156
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 11160
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 11162
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    .line 11165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd_HHmmss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 11166
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "//Camera//"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 11167
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4

    .line 11168
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 11171
    :cond_4
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "//Camera//"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    .line 11185
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 11186
    const-string v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 11190
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 11191
    :catch_0
    move-exception v0

    .line 11192
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 11193
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 11194
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/sec/chaton/chat/b/c;Ljava/lang/String;Landroid/widget/Spinner;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 15507
    invoke-virtual {p3}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    move v2, v3

    .line 15509
    :goto_0
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 15511
    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/sec/chaton/chat/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15515
    :goto_1
    return v2

    .line 15509
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 15515
    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;J)J
    .locals 0

    .prologue
    .line 319
    iput-wide p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aC:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/ChatFragment;->b(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->di:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private a(Landroid/view/LayoutInflater;ZLcom/sec/chaton/chat/a;)Landroid/view/View;
    .locals 12

    .prologue
    .line 15222
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v8

    .line 15224
    const v0, 0x7f030052

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 15225
    const v0, 0x7f0701f9

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 15226
    const v0, 0x7f0701fc

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 15228
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15229
    invoke-static {}, Lcom/sec/chaton/chat/b/n;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15230
    if-eqz p3, :cond_0

    .line 15231
    const v1, 0x7f0701fa

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 15232
    invoke-virtual {p3, v1}, Lcom/sec/chaton/chat/a;->a(Landroid/widget/CheckBox;)V

    .line 15233
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15237
    :cond_0
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    if-eqz v1, :cond_7

    .line 15238
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 15244
    :goto_0
    const v1, 0x7f0701fd

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 15246
    const v2, 0x7f070202

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Landroid/widget/Spinner;

    .line 15247
    const v2, 0x7f070206

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Landroid/widget/Spinner;

    .line 15248
    const v2, 0x7f070200

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 15249
    const v3, 0x7f070203

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 15250
    new-instance v10, Lcom/sec/chaton/chat/cu;

    invoke-direct {v10, p0, v0, v2, v3}, Lcom/sec/chaton/chat/cu;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 15254
    if-eqz p3, :cond_1

    .line 15255
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    invoke-virtual {p3, v0}, Lcom/sec/chaton/chat/a;->a(Z)V

    .line 15258
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    if-eqz v0, :cond_8

    .line 15259
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 15265
    :goto_1
    const v0, 0x7f070201

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cR:Landroid/widget/ImageView;

    .line 15266
    const v0, 0x7f070204

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    .line 15267
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cR:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->c(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 15268
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 15269
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_2

    .line 15270
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_9

    .line 15271
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;Z)V

    .line 15276
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_5

    .line 15278
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "inbox_no=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v11, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v11, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 15283
    if-eqz v11, :cond_d

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_d

    .line 15284
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 15285
    const-string v0, "profile_url"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 15290
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    if-nez v0, :cond_3

    .line 15291
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/f;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 15292
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->L:Ljava/io/File;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 15294
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 15295
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;)V

    .line 15299
    :cond_3
    const-string v1, "NA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 15300
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->L:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_chat_profile.png_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 15301
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 15302
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 15303
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 15335
    :cond_4
    :goto_3
    if-eqz v11, :cond_5

    .line 15336
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 15339
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 15342
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->L:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_chat_profile.png_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 15344
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 15345
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;)V

    .line 15356
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x1090008

    const v2, 0x1020014

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/chat/b/b;->a(Landroid/content/Context;IILjava/util/List;)Lcom/sec/chaton/chat/b/b;

    move-result-object v1

    .line 15363
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x1090008

    const v3, 0x1020014

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v2, v3, v4}, Lcom/sec/chaton/chat/b/b;->a(Landroid/content/Context;IILjava/util/List;)Lcom/sec/chaton/chat/b/b;

    move-result-object v2

    .line 15372
    invoke-virtual {v8}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/chat/b/a;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 15373
    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/b/b;->add(Ljava/lang/Object;)V

    .line 15374
    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/b/b;->add(Ljava/lang/Object;)V

    goto :goto_5

    .line 15240
    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    .line 15261
    :cond_8
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 15262
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto/16 :goto_1

    .line 15273
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_2

    .line 15306
    :cond_a
    if-nez v0, :cond_b

    .line 15307
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/chaton/util/bw;->c:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_3

    .line 15311
    :cond_b
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->f(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_c

    .line 15320
    new-instance v1, Lcom/sec/chaton/chat/eo;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/chaton/chat/eo;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cV:Lcom/sec/chaton/chat/eo;

    .line 15321
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cV:Lcom/sec/chaton/chat/eo;

    const/16 v1, 0x3c

    const/16 v2, 0x3c

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_chat_profile.png_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->cT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->cS:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/chat/eo;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15322
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bU:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cV:Lcom/sec/chaton/chat/eo;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_3

    .line 15325
    :cond_c
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->L:Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cU:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 15326
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 15327
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;)V

    goto/16 :goto_3

    .line 15332
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/chaton/util/bw;->c:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_3

    .line 15348
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/chaton/util/bw;->b:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_4

    .line 15353
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_4

    .line 15377
    :cond_10
    const v0, 0x1090009

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/b/b;->setDropDownViewResource(I)V

    .line 15378
    const v0, 0x1090009

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/b/b;->setDropDownViewResource(I)V

    .line 15379
    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 15380
    invoke-virtual {v7, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 15388
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->cL:Ljava/lang/String;

    .line 15389
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->cM:Ljava/lang/String;

    .line 15390
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->cL:Ljava/lang/String;

    invoke-direct {p0, v8, v0, v6}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/b/c;Ljava/lang/String;Landroid/widget/Spinner;)I

    move-result v1

    .line 15391
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->cM:Ljava/lang/String;

    invoke-direct {p0, v8, v0, v7}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/b/c;Ljava/lang/String;Landroid/widget/Spinner;)I

    move-result v2

    .line 15392
    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 15393
    invoke-virtual {v7, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 15396
    invoke-virtual {v6}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Lcom/sec/chaton/chat/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->cL:Ljava/lang/String;

    .line 15399
    invoke-virtual {v6}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Lcom/sec/chaton/chat/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ChatFragment;->cM:Ljava/lang/String;

    .line 15403
    if-nez p2, :cond_11

    if-eqz p3, :cond_11

    .line 15405
    invoke-virtual {p3}, Lcom/sec/chaton/chat/a;->a()Z

    move-result v0

    invoke-direct {p0, v10, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cu;Z)V

    .line 15407
    new-instance v0, Lcom/sec/chaton/chat/cf;

    invoke-direct {v0, p0, v10}, Lcom/sec/chaton/chat/cf;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/cu;)V

    invoke-virtual {p3, v0}, Lcom/sec/chaton/chat/a;->a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 15437
    :cond_11
    return-object v9
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bE:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method private a(Landroid/view/View;I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 16084
    new-instance v0, Lcom/sec/chaton/util/m;

    sget-object v1, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    invoke-direct {v0, v1}, Lcom/sec/chaton/util/m;-><init>(Lcom/sec/chaton/e/w;)V

    .line 16085
    const v1, 0x7f0b0413

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/m;->setEmptyText(Ljava/lang/CharSequence;)V

    .line 16086
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-direct {v1, p1, v0, p2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;I)V

    .line 16088
    new-instance v0, Lcom/sec/chaton/chat/cn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/cn;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setItemSelectListener(Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V

    .line 16100
    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setGravity(I)V

    .line 16101
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setDirection(I)V

    .line 16102
    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setScrollEnabled(Z)V

    .line 16103
    const/16 v0, 0x14

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setPosition(II)V

    .line 16105
    return-object v1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/cv;)Lcom/sec/chaton/chat/cv;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/dg;)Lcom/sec/chaton/chat/dg;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/multimedia/audio/k;)Lcom/sec/chaton/multimedia/audio/k;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bZ:Lcom/sec/chaton/multimedia/audio/k;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;)Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;ZZ)Ljava/io/File;
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;ZZ)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ZZ)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 6074
    .line 6077
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 6078
    const-string v1, "content"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6079
    invoke-static {p1}, Lcom/sec/chaton/util/r;->c(Ljava/lang/String;)Lcom/sec/chaton/util/u;

    move-result-object v0

    .line 6080
    if-eqz v0, :cond_4

    .line 6081
    iget-object v0, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    :goto_0
    move-object v1, v0

    .line 6089
    :goto_1
    if-eqz v1, :cond_0

    .line 6091
    invoke-static {v1}, Lcom/sec/chaton/util/bc;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6094
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-static {v0, v1, v2, v4, v6}, Lcom/sec/chaton/util/bc;->b(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Landroid/net/Uri;Z)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 6105
    :goto_2
    if-nez v1, :cond_2

    move-object v1, v3

    .line 6124
    :cond_0
    :goto_3
    return-object v1

    .line 6085
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 6095
    :catch_0
    move-exception v0

    .line 6096
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v1, v3

    .line 6103
    goto :goto_2

    .line 6097
    :catch_1
    move-exception v0

    .line 6098
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v1, v3

    .line 6103
    goto :goto_2

    .line 6099
    :catch_2
    move-exception v0

    .line 6100
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v1, v3

    .line 6103
    goto :goto_2

    .line 6101
    :catch_3
    move-exception v0

    .line 6102
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v1, v3

    goto :goto_2

    .line 6112
    :cond_2
    if-nez p3, :cond_0

    .line 6116
    if-nez p2, :cond_3

    .line 6117
    invoke-direct {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->b(Ljava/io/File;)V

    goto :goto_3

    .line 6120
    :cond_3
    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3

    :cond_4
    move-object v0, v3

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 6020
    :try_start_0
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    .line 6021
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 6022
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    .line 6025
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    const-string v1, "requestCode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6030
    :goto_0
    return-void

    .line 6027
    :catch_0
    move-exception v0

    .line 6028
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 14812
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 14813
    const-string v0, "inbox_last_chat_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cu:I

    .line 14814
    const-string v0, "inbox_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    .line 14815
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    if-nez v0, :cond_0

    .line 14817
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 14818
    const-string v0, "ChatType is null"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14821
    :cond_0
    const-string v0, "inbox_chat_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    .line 14822
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14823
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "retrieveInboxInfoFromCursor - inboxNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", chatType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14826
    :cond_1
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cu:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_4

    .line 14829
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->I()Z

    move-result v0

    if-nez v0, :cond_3

    .line 14830
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 14881
    :cond_2
    :goto_0
    return-void

    .line 14833
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 14876
    :goto_1
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/b/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    .line 14877
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/b/a;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    goto :goto_0

    .line 14835
    :cond_4
    const-string v0, "inbox_session_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 14836
    const-string v0, "inbox_last_msg_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aL:Ljava/lang/Long;

    .line 14837
    const-string v0, "inbox_title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aP:Ljava/lang/String;

    .line 14838
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->z:I

    .line 14839
    const-string v0, "inbox_server_ip"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    .line 14840
    const-string v0, "inbox_server_port"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bb:I

    .line 14842
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 14843
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    .line 14844
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bb:I

    .line 14848
    :cond_5
    const-string v0, "inbox_title_fixed"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aQ:Ljava/lang/String;

    .line 14849
    const-string v0, "inbox_last_msg_sender"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aR:Ljava/lang/String;

    .line 14850
    const-string v0, "inbox_last_temp_msg"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cv:Ljava/lang/String;

    .line 14851
    const-string v0, "buddy_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aS:Ljava/lang/String;

    .line 14854
    const-string v0, "inbox_trunk_unread_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aW:I

    .line 14861
    const-string v0, "buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    .line 14862
    const-string v0, "Y"

    const-string v1, "inbox_valid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    .line 14863
    const-string v0, "inbox_last_timestamp"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    .line 14864
    const-string v0, "Y"

    const-string v1, "inbox_enable_noti"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    .line 14865
    const-string v0, "inbox_unread_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aX:I

    .line 14866
    const-string v0, "Y"

    const-string v1, "inbox_is_entered"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->be:Z

    .line 14868
    const-string v0, "lasst_session_merge_time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aU:J

    .line 14870
    const-string v0, "inbox_last_tid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    .line 14871
    const-string v0, "Y"

    const-string v1, "inbox_enable_translate"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    .line 14872
    const-string v0, "Y"

    const-string v1, "translate_outgoing_message"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    .line 14873
    const-string v0, "inbox_translate_my_language"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    .line 14874
    const-string v0, "inbox_translate_buddy_language"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method private a(Landroid/database/Cursor;Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 13027
    const-string v0, "message_inbox_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 13028
    const-string v0, "message_sever_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 13029
    const-string v0, "message_content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 13030
    const-string v0, "message_content_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v10

    .line 13031
    const-string v0, "message_sender"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 13033
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Lcom/sec/chaton/j/c/a;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13036
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->an()Z

    move-result v0

    if-nez v0, :cond_1

    .line 13055
    :cond_0
    :goto_0
    return-void

    .line 13041
    :cond_1
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    iget-object v11, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object v1, p2

    move-object v2, p0

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 13043
    invoke-direct {p0, v8, v9}, Lcom/sec/chaton/chat/ChatFragment;->b(J)Lcom/sec/chaton/widget/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    .line 13044
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v6}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 13046
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->show()V

    .line 13048
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Lcom/sec/chaton/j/c/a;->b(J)Lcom/sec/chaton/j/c/c;

    move-result-object v0

    .line 13050
    if-eqz v0, :cond_0

    .line 13051
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/c;->a(Lcom/sec/chaton/widget/c;)V

    .line 13052
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->U:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/c;->a(Landroid/os/Handler;)V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6065
    const-string v0, "temp_file_path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6066
    const-string v1, "sendWithText"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 6068
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;ZZ)Ljava/io/File;

    .line 6069
    return-void
.end method

.method private a(Landroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15872
    if-nez p1, :cond_0

    .line 15886
    :goto_0
    return-void

    .line 15876
    :cond_0
    const v0, 0x7f070570

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 15879
    if-eqz v0, :cond_1

    .line 15880
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 15884
    :cond_1
    const v0, 0x7f070571

    invoke-interface {p1, v0, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;I)V
    .locals 17

    .prologue
    .line 10646
    const-string v3, ""

    .line 10647
    const/4 v6, 0x0

    .line 10649
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v4

    .line 10650
    const/4 v5, -0x3

    if-eq v5, v4, :cond_0

    const/4 v5, -0x2

    if-ne v5, v4, :cond_2

    .line 10653
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    const v4, 0x7f0b0205

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V

    .line 10654
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setDuration(I)V

    .line 10655
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 10873
    :cond_1
    :goto_0
    return-void

    .line 10670
    :cond_2
    sget-object v4, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    sget-object v4, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    sget-object v4, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    sget-object v4, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    sget-object v4, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    sget-object v4, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    sget-object v4, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    sget-object v4, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_12

    .line 10679
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 10680
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v4, 0x7f0b00a6

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 10684
    :cond_3
    invoke-static/range {p6 .. p6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 10685
    new-instance v6, Ljava/io/File;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 10686
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, "."

    move-object/from16 v0, p6

    invoke-direct {v4, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 10687
    :goto_1
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 10688
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 10690
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v14, v3

    .line 10692
    :goto_2
    const/4 v12, 0x0

    .line 10693
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 10694
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v3, v4, :cond_5

    .line 10695
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v3

    if-nez v3, :cond_5

    .line 10696
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 10699
    :cond_5
    const/4 v12, 0x1

    .line 10702
    :cond_6
    sget-object v3, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v3, :cond_7

    sget-object v3, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v3, :cond_7

    sget-object v3, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-eq v0, v3, :cond_7

    sget-object v3, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_8

    .line 10706
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    move-wide/from16 v4, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p7

    move-object/from16 v11, p4

    invoke-virtual/range {v3 .. v12}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)J

    goto/16 :goto_0

    .line 10716
    :cond_8
    sget-object v3, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_9

    .line 10717
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    move-wide/from16 v7, p2

    move-object/from16 v13, p4

    invoke-virtual/range {v6 .. v13}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J

    goto/16 :goto_0

    .line 10728
    :cond_9
    sget-object v3, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_c

    .line 10729
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    .line 10730
    const-string v3, "\n"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 10732
    const/4 v3, 0x2

    aget-object v3, v4, v3

    .line 10733
    invoke-static {v3}, Lcom/sec/chaton/settings/downloads/u;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 10735
    array-length v3, v4

    const/4 v5, 0x6

    if-le v3, v5, :cond_b

    const-string v3, "mixed"

    const/4 v5, 0x0

    aget-object v5, v4, v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 10736
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 10737
    const/4 v3, 0x6

    :goto_3
    array-length v6, v4

    if-ge v3, v6, :cond_a

    .line 10738
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v4, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 10737
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 10740
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    const-wide/16 v14, -0x1

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    move-wide/from16 v7, p2

    invoke-virtual/range {v6 .. v16}, Lcom/sec/chaton/d/o;->b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)J

    goto/16 :goto_0

    .line 10749
    :cond_b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    const-wide/16 v14, -0x1

    const/16 v16, 0x0

    move-wide/from16 v7, p2

    invoke-virtual/range {v6 .. v16}, Lcom/sec/chaton/d/o;->b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)J

    goto/16 :goto_0

    .line 10760
    :cond_c
    sget-object v3, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_d

    .line 10761
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    move-wide/from16 v4, p2

    move-object/from16 v9, p4

    invoke-virtual/range {v3 .. v9}, Lcom/sec/chaton/d/o;->b(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)J

    goto/16 :goto_0

    .line 10768
    :cond_d
    sget-object v3, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_e

    .line 10769
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    move-wide/from16 v4, p2

    move-object/from16 v9, p4

    invoke-virtual/range {v3 .. v9}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)J

    goto/16 :goto_0

    .line 10783
    :cond_e
    const/4 v3, 0x1

    move/from16 v0, p9

    if-ne v0, v3, :cond_f

    if-eqz p4, :cond_f

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_f

    .line 10804
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    move-wide/from16 v6, p2

    move-object/from16 v9, p4

    move-object/from16 v13, p8

    move-object/from16 v15, p7

    invoke-virtual/range {v5 .. v15}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)J

    goto/16 :goto_0

    .line 10816
    :cond_f
    if-eqz v6, :cond_1

    .line 10830
    const/4 v14, 0x0

    .line 10831
    if-eqz p4, :cond_11

    .line 10832
    const-string v3, "\n"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 10834
    array-length v3, v4

    const/4 v5, 0x6

    if-le v3, v5, :cond_11

    .line 10835
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 10836
    const/4 v3, 0x6

    :goto_4
    array-length v7, v4

    if-ge v3, v7, :cond_10

    .line 10837
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v8, v4, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 10836
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 10839
    :cond_10
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 10843
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    move-wide/from16 v4, p2

    move-object/from16 v8, p7

    move-object/from16 v13, p8

    invoke-virtual/range {v3 .. v14}, Lcom/sec/chaton/d/o;->a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)J

    .line 10856
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/j/c/g;->a(J)Lcom/sec/chaton/j/c/i;

    move-result-object v3

    .line 10857
    if-eqz v3, :cond_1

    .line 10858
    new-instance v4, Lcom/sec/chaton/chat/bf;

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, p1

    invoke-direct {v4, v0, v1, v2}, Lcom/sec/chaton/chat/bf;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/e/w;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Lcom/sec/chaton/j/c/i;->a(Landroid/os/Handler;)V

    goto/16 :goto_0

    :cond_12
    move-object v14, v3

    goto/16 :goto_2
.end method

.method private a(Landroid/view/ViewGroup;Z)V
    .locals 2

    .prologue
    .line 15496
    if-nez p1, :cond_1

    .line 15503
    :cond_0
    return-void

    .line 15499
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 15500
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 15501
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 15500
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 2

    .prologue
    .line 15852
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15853
    if-gtz p2, :cond_1

    .line 15855
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 15863
    :cond_0
    :goto_0
    return-void

    .line 15859
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 15860
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;I)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Landroid/view/View;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 319
    invoke-direct/range {p0 .. p9}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/View;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Lcom/sec/chaton/chat/el;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/b/c;Lcom/sec/chaton/chat/el;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/cu;Z)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cu;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/el;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/el;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->b(Ljava/io/File;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 319
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->j(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;Lcom/sec/chaton/e/w;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/chat/ChatFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Ljava/lang/String;ZI)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;ZZ)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(ZZ)V

    return-void
.end method

.method private a(Lcom/sec/chaton/chat/b/c;Lcom/sec/chaton/chat/el;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 15640
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cW:Landroid/app/ProgressDialog;

    .line 15641
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cW:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 15642
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cW:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 15643
    new-instance v0, Lcom/sec/chaton/chat/cw;

    invoke-direct {v0, p0, p1, p3, p4}, Lcom/sec/chaton/chat/cw;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Ljava/lang/String;Ljava/lang/String;)V

    .line 15644
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/chaton/chat/el;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/cw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 15645
    return-void
.end method

.method private a(Lcom/sec/chaton/chat/cr;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;)V
    .locals 9

    .prologue
    const v3, 0x7f0b002f

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 1958
    sget-object v0, Lcom/sec/chaton/chat/cr;->a:Lcom/sec/chaton/chat/cr;

    if-ne p1, v0, :cond_6

    .line 1960
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    .line 1962
    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne p3, v1, :cond_2

    .line 1963
    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_1

    .line 1964
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v3, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2008
    :cond_0
    :goto_0
    return-void

    .line 1966
    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v8, v8}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;ZZ)Ljava/io/File;

    goto :goto_0

    .line 1970
    :cond_2
    sget-object v1, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    if-ne p3, v1, :cond_3

    move-object v0, p0

    move-object v1, p2

    move-object v3, v2

    move-object v4, p3

    move-object v5, v2

    .line 1971
    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto :goto_0

    .line 1974
    :cond_3
    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne p3, v1, :cond_5

    .line 1975
    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_4

    .line 1976
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v3, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    move-object v0, p0

    move-object v1, p2

    move-object v3, v2

    move-object v4, p3

    move-object v5, v2

    .line 1978
    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto :goto_0

    .line 1981
    :cond_5
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne p3, v0, :cond_0

    .line 1982
    if-eqz p4, :cond_0

    .line 1983
    const-string v0, ""

    const/4 v1, 0x1

    invoke-direct {p0, p4, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1987
    :cond_6
    sget-object v0, Lcom/sec/chaton/chat/cr;->b:Lcom/sec/chaton/chat/cr;

    if-ne p1, v0, :cond_0

    .line 1989
    sget-object v0, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    if-ne p3, v0, :cond_7

    move-object v0, p0

    move-object v1, p2

    move-object v3, v2

    move-object v4, p3

    move-object v5, v2

    .line 1990
    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto :goto_0

    .line 1993
    :cond_7
    sget-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq p3, v0, :cond_8

    sget-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne p3, v0, :cond_a

    .line 1994
    :cond_8
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    .line 1995
    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_9

    .line 1996
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v3, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_9
    move-object v3, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, v2

    move-object v7, v2

    .line 1998
    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2002
    :cond_a
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne p3, v0, :cond_0

    .line 2003
    if-eqz p4, :cond_0

    .line 2004
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0, p4}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/sec/chaton/chat/cu;Z)V
    .locals 5

    .prologue
    const v4, 0x7f08005d

    const v3, 0x7f08005c

    const v2, 0x7f070542

    .line 15443
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    if-eqz v0, :cond_2

    .line 15444
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/ViewGroup;Z)V

    .line 15450
    :goto_0
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 15451
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->b:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/ViewGroup;Z)V

    .line 15452
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->b:Landroid/view/ViewGroup;

    const v1, 0x7f070202

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 15453
    if-eqz v0, :cond_0

    .line 15454
    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 15455
    if-eqz v0, :cond_0

    .line 15456
    if-eqz p2, :cond_3

    .line 15457
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 15465
    :cond_0
    :goto_1
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 15466
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->c:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/ViewGroup;Z)V

    .line 15467
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->c:Landroid/view/ViewGroup;

    const v1, 0x7f070206

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 15468
    if-eqz v0, :cond_1

    .line 15469
    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 15470
    if-eqz v0, :cond_1

    .line 15471
    if-eqz p2, :cond_4

    .line 15472
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 15479
    :cond_1
    :goto_2
    return-void

    .line 15446
    :cond_2
    iget-object v0, p1, Lcom/sec/chaton/chat/cu;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/ViewGroup;Z)V

    goto :goto_0

    .line 15459
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 15474
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2
.end method

.method private a(Lcom/sec/chaton/chat/el;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 15563
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 15564
    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    .line 15565
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 15566
    const v1, 0x7f0b03af

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 15567
    const/4 v1, 0x0

    invoke-direct {p0, v0, v3, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/LayoutInflater;ZLcom/sec/chaton/chat/a;)Landroid/view/View;

    move-result-object v1

    .line 15568
    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    .line 15569
    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    .line 15570
    const v0, 0x7f0701fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 15571
    const v0, 0x7f0701f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 15572
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 15573
    const v0, 0x7f070202

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 15574
    const v3, 0x7f070206

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 15575
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v3

    .line 15576
    new-instance v4, Lcom/sec/chaton/chat/ch;

    invoke-direct {v4, p0, v3, v0, v1}, Lcom/sec/chaton/chat/ch;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 15595
    new-instance v0, Lcom/sec/chaton/chat/ci;

    invoke-direct {v0, p0, v3, v1}, Lcom/sec/chaton/chat/ci;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;)V

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 15613
    const v0, 0x7f0b0037

    new-instance v1, Lcom/sec/chaton/chat/cj;

    invoke-direct {v1, p0, v3, p1}, Lcom/sec/chaton/chat/cj;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Lcom/sec/chaton/chat/el;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 15621
    const v0, 0x7f0b0039

    new-instance v1, Lcom/sec/chaton/chat/ck;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/ck;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 15635
    invoke-virtual {v2}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 15637
    return-void
.end method

.method private a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6352
    new-instance v3, Lcom/sec/chaton/multimedia/image/c;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-eq p2, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v3, v4, v5, v0, v2}, Lcom/sec/chaton/multimedia/image/c;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 6353
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bU:Lcom/sec/common/f/c;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    invoke-virtual {v0, v4, v3}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 6354
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 6355
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6356
    iput-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    .line 6357
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    .line 6358
    iput-object p2, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    .line 6359
    iput-object p3, p0, Lcom/sec/chaton/chat/ChatFragment;->al:Ljava/lang/String;

    .line 6360
    iput-object p4, p0, Lcom/sec/chaton/chat/ChatFragment;->am:Ljava/lang/String;

    .line 6361
    iput-boolean p5, p0, Lcom/sec/chaton/chat/ChatFragment;->aj:Z

    .line 6363
    sget-object v0, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, p2, :cond_1

    .line 6364
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6368
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6369
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aI()V

    .line 6372
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->b()V

    .line 6373
    return-void

    :cond_0
    move v0, v2

    .line 6352
    goto :goto_0

    .line 6366
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method private a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 5218
    if-eqz p1, :cond_0

    .line 5220
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0xa00000

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 5221
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b00b0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 5253
    :cond_0
    :goto_0
    return-void

    .line 5226
    :cond_1
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5227
    const/4 v2, 0x0

    .line 5228
    :goto_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5229
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 5232
    :cond_2
    invoke-static {p4, v2}, Lcom/sec/chaton/util/r;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5233
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 5234
    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b002f

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b035a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 5236
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0

    .line 5241
    :cond_3
    const/4 v7, 0x0

    .line 5242
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_4

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5243
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_5

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5244
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 5246
    :cond_5
    const/4 v7, 0x1

    .line 5250
    :cond_6
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object/from16 v8, p5

    move-object v9, p2

    move-object v10, p3

    invoke-virtual/range {v0 .. v10}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    goto/16 :goto_0
.end method

.method private a(Ljava/io/File;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V
    .locals 8

    .prologue
    .line 8914
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 8915
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 8919
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/util/ArrayList;)Z

    .line 8934
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/sec/chaton/e/w;)V
    .locals 4

    .prologue
    .line 10591
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    .line 10642
    :cond_0
    :goto_0
    return-void

    .line 10594
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/d;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 10595
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 10596
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 10599
    const-string v0, "buddy_orginal_number"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 10600
    const/4 v0, 0x0

    .line 10601
    const-string v3, "buddy_msisdns"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_2

    .line 10602
    const-string v0, "buddy_msisdns"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 10614
    :cond_2
    invoke-virtual {p0, v2, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 10615
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 10616
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10617
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 10633
    :cond_3
    invoke-static {v0, p1}, Lcom/sec/common/util/i;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 10635
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 10639
    :cond_4
    if-eqz v1, :cond_0

    .line 10640
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 8597
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-eqz v0, :cond_1

    .line 8634
    :cond_0
    :goto_0
    return-void

    .line 8604
    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 8610
    sget-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v2, v0, :cond_2

    sget-object v0, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v2, v0, :cond_2

    sget-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v2, v0, :cond_6

    .line 8611
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8613
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 8614
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "restoreMediaFile uri:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8617
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8618
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 8620
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-direct {v0, v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 8622
    :goto_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 8623
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 8605
    :catch_0
    move-exception v0

    .line 8606
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 8626
    :cond_4
    const/4 v5, 0x1

    move-object v0, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 8628
    :cond_5
    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 8631
    :cond_6
    sget-object v0, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-ne v2, v0, :cond_0

    .line 8632
    invoke-virtual {p0, p2}, Lcom/sec/chaton/chat/ChatFragment;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    .line 9379
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 9380
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ao()Z

    move-result v0

    if-nez v0, :cond_0

    .line 9389
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ap()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9444
    :goto_0
    return-void

    .line 9394
    :cond_1
    if-nez p3, :cond_2

    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aq()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-nez v0, :cond_2

    .line 9395
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 9406
    :cond_2
    if-nez p3, :cond_3

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-eqz v0, :cond_3

    .line 9407
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 9408
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->V()V

    goto :goto_0

    .line 9413
    :cond_3
    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v7

    .line 9414
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/d/o;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 9416
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_4

    .line 9417
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 9422
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v5, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 9441
    :goto_1
    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 9432
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v5, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 13836
    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v1

    .line 13865
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 13867
    :cond_0
    if-nez p3, :cond_4

    .line 13868
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0201db

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    .line 13869
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aw()V

    .line 13907
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 13908
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ax:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 13910
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 13911
    const-string v0, "\n"

    const-string v2, " "

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 13915
    :cond_2
    if-eqz p3, :cond_6

    .line 13926
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ax:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0263

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13933
    :goto_1
    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p3, :cond_7

    .line 13934
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->at:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 13935
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->au:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/chat/bv;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bv;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13944
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aw:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/chat/bw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bw;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13951
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->av:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/chat/bx;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bx;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13962
    :goto_2
    return-void

    .line 13871
    :cond_4
    const-string v0, "NO_SEARCH"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 13880
    invoke-direct {p0, p2}, Lcom/sec/chaton/chat/ChatFragment;->p(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 13883
    iput-object p2, p0, Lcom/sec/chaton/chat/ChatFragment;->co:Ljava/lang/String;

    .line 13884
    invoke-direct {p0, p2}, Lcom/sec/chaton/chat/ChatFragment;->q(Ljava/lang/String;)V

    goto :goto_0

    .line 13886
    :cond_5
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cq:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 13887
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 13889
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 13891
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 13892
    if-eqz v0, :cond_1

    .line 13893
    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    .line 13894
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aw()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 13897
    :catch_0
    move-exception v0

    .line 13898
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13929
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ax:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0264

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 13960
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->at:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 7007
    if-eqz p1, :cond_1

    .line 7008
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    if-eqz v0, :cond_0

    .line 7009
    if-nez p2, :cond_0

    .line 7011
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    const-string v1, "TRANS_FAIL"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/a;->a(Ljava/lang/String;)V

    .line 7021
    :cond_0
    :goto_0
    return-void

    .line 7015
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    if-eqz v0, :cond_0

    .line 7016
    if-eqz p2, :cond_0

    .line 7017
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    const-string v1, "RECV_SUCC"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(ILandroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6036
    .line 6038
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move v0, v1

    .line 6053
    :goto_0
    return v0

    .line 6040
    :sswitch_0
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 6041
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aa()V

    goto :goto_0

    .line 6046
    :sswitch_1
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 6047
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->Z()V

    goto :goto_0

    .line 6038
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x15 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Landroid/content/ClipData;Lcom/sec/chaton/chat/cr;)Z
    .locals 13

    .prologue
    const v11, 0x7f0b002f

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1718
    .line 1726
    if-eqz p1, :cond_1d

    .line 1728
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v8

    .line 1731
    if-le v8, v3, :cond_21

    move v7, v4

    move v0, v4

    move-object v1, v2

    move-object v5, v2

    move v6, v4

    .line 1736
    :goto_0
    if-ge v7, v8, :cond_20

    .line 1737
    invoke-virtual {p1, v7}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v9

    .line 1739
    invoke-virtual {v9}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 1740
    invoke-virtual {v9}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    .line 1747
    :cond_0
    :goto_1
    if-le v0, v3, :cond_5

    move v12, v0

    move v0, v6

    move-object v6, v5

    move-object v5, v1

    move v1, v12

    .line 1752
    :goto_2
    if-ne v1, v3, :cond_1f

    if-eqz v5, :cond_1f

    if-eqz v6, :cond_1f

    move v1, v3

    .line 1758
    :goto_3
    const/4 v5, 0x2

    if-lt v8, v5, :cond_1

    if-eqz v1, :cond_1c

    .line 1759
    :cond_1
    invoke-virtual {p1, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v0

    .line 1760
    if-eqz v0, :cond_1b

    .line 1762
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 1764
    if-eqz v5, :cond_b

    .line 1765
    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1766
    if-eqz v0, :cond_1e

    .line 1767
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1769
    :goto_4
    invoke-virtual {v5}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    .line 1770
    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "android.intent.extra.TEXT"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1772
    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    if-eqz v6, :cond_a

    .line 1773
    const-string v8, "file://"

    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1775
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1776
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1785
    :cond_2
    :goto_5
    invoke-direct {p0, v6}, Lcom/sec/chaton/chat/ChatFragment;->i(Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v1

    .line 1787
    if-eqz v1, :cond_8

    .line 1788
    sget-object v4, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    if-ne v1, v4, :cond_7

    .line 1790
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1791
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1792
    invoke-direct {p0, v5}, Lcom/sec/chaton/chat/ChatFragment;->c(Landroid/content/Intent;)V

    .line 1805
    :goto_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 1806
    if-eqz v2, :cond_9

    .line 1807
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDrag_editview generate filepath : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_7
    move v0, v3

    .line 1913
    :goto_8
    return v0

    .line 1742
    :cond_4
    invoke-virtual {v9}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 1743
    invoke-virtual {v9}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 1745
    add-int/lit8 v0, v0, 0x1

    move v6, v7

    goto/16 :goto_1

    .line 1736
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 1779
    :cond_6
    invoke-static {v1}, Lcom/sec/chaton/util/r;->c(Ljava/lang/String;)Lcom/sec/chaton/util/u;

    move-result-object v1

    .line 1780
    if-eqz v1, :cond_2

    .line 1781
    iget-object v2, v1, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    goto :goto_5

    .line 1794
    :cond_7
    invoke-direct {p0, v6}, Lcom/sec/chaton/chat/ChatFragment;->i(Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0, v7}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cr;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto :goto_6

    .line 1798
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v11, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_6

    .line 1809
    :cond_9
    const-string v0, "onDrag_editview file is null"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 1814
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v11, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_7

    .line 1816
    :cond_b
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 1817
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 1818
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 1822
    const-string v6, "file"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1823
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 1825
    new-instance v6, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1827
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    .line 1828
    sget-object v7, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v7, :cond_c

    .line 1830
    sget-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-direct {p0, p2, v6, v0, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cr;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto :goto_7

    .line 1832
    :cond_c
    sget-object v7, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v7, :cond_d

    .line 1833
    sget-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-direct {p0, p2, v6, v0, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cr;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1835
    :cond_d
    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v2, :cond_e

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->d:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v2, :cond_e

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->e:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v2, :cond_e

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->c:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v2, :cond_e

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v2, :cond_e

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->j:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v2, :cond_e

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v2, :cond_e

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v2, :cond_f

    .line 1837
    :cond_e
    invoke-virtual {v5, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1838
    invoke-direct {p0, v5}, Lcom/sec/chaton/chat/ChatFragment;->c(Landroid/content/Intent;)V

    goto/16 :goto_7

    .line 1840
    :cond_f
    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_10

    .line 1841
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v11, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    .line 1844
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v11, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    .line 1850
    :cond_11
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->c(Ljava/lang/String;)Lcom/sec/chaton/util/u;

    move-result-object v0

    .line 1851
    if-eqz v0, :cond_15

    iget-object v1, v0, Lcom/sec/chaton/util/u;->b:Ljava/lang/String;

    if-eqz v1, :cond_15

    iget-object v1, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    if-eqz v1, :cond_15

    .line 1853
    iget-object v1, v0, Lcom/sec/chaton/util/u;->b:Ljava/lang/String;

    const-string v5, "image/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1855
    iget-object v1, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-direct {p0, p2, v1, v4, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cr;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 1870
    :goto_9
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 1871
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDrag_editview generate filepath : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1857
    :cond_12
    iget-object v1, v0, Lcom/sec/chaton/util/u;->b:Ljava/lang/String;

    const-string v5, "video/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1858
    iget-object v1, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    sget-object v4, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-direct {p0, p2, v1, v4, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cr;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto :goto_9

    .line 1860
    :cond_13
    iget-object v1, v0, Lcom/sec/chaton/util/u;->b:Ljava/lang/String;

    const-string v5, "audio/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1861
    iget-object v1, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    sget-object v4, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    invoke-direct {p0, p2, v1, v4, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/cr;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto :goto_9

    .line 1868
    :cond_14
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v1, v11, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_9

    .line 1876
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v11, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    .line 1885
    :cond_16
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_18

    .line 1888
    sget-object v1, Lcom/sec/chaton/chat/cr;->b:Lcom/sec/chaton/chat/cr;

    if-ne p2, v1, :cond_17

    .line 1889
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 1891
    :cond_17
    sget-object v1, Lcom/sec/chaton/chat/cr;->a:Lcom/sec/chaton/chat/cr;

    if-ne p2, v1, :cond_3

    .line 1892
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_7

    .line 1895
    :cond_18
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1a

    .line 1896
    sget-object v1, Lcom/sec/chaton/chat/cr;->b:Lcom/sec/chaton/chat/cr;

    if-ne p2, v1, :cond_19

    .line 1897
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 1899
    :cond_19
    sget-object v1, Lcom/sec/chaton/chat/cr;->a:Lcom/sec/chaton/chat/cr;

    if-ne p2, v1, :cond_3

    .line 1900
    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_7

    .line 1903
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v11, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    :cond_1b
    move v0, v3

    .line 1908
    goto/16 :goto_8

    .line 1910
    :cond_1c
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b0414

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v3

    .line 1911
    goto/16 :goto_8

    :cond_1d
    move v0, v4

    .line 1913
    goto/16 :goto_8

    :cond_1e
    move-object v1, v2

    goto/16 :goto_4

    :cond_1f
    move v1, v4

    goto/16 :goto_3

    :cond_20
    move v12, v0

    move v0, v6

    move-object v6, v5

    move-object v5, v1

    move v1, v12

    goto/16 :goto_2

    :cond_21
    move v0, v4

    move v1, v4

    goto/16 :goto_3
.end method

.method private a(Landroid/view/DragEvent;Lcom/sec/chaton/chat/cr;)Z
    .locals 1

    .prologue
    .line 1712
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 1713
    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/ClipData;Lcom/sec/chaton/chat/cr;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Landroid/view/DragEvent;Lcom/sec/chaton/chat/cr;)Z
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/DragEvent;Lcom/sec/chaton/chat/cr;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    return p1
.end method

.method static synthetic aA(Lcom/sec/chaton/chat/ChatFragment;)I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    return v0
.end method

.method private aA()V
    .locals 4

    .prologue
    .line 14114
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/chaton/chat/cb;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/cb;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 14141
    return-void
.end method

.method static synthetic aB(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aG:Ljava/lang/String;

    return-object v0
.end method

.method private aB()V
    .locals 2

    .prologue
    .line 14387
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ay:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 14388
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ay:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 14390
    :cond_0
    return-void
.end method

.method private aC()F
    .locals 5

    .prologue
    const v4, 0x7f090137

    .line 14445
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Default Font Size"

    sget-object v2, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/dk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 14446
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 14447
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sizeString : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14450
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    .line 14452
    :try_start_0
    invoke-static {v1}, Lcom/sec/chaton/settings/dk;->a(Ljava/lang/String;)Lcom/sec/chaton/settings/dk;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 14458
    :goto_0
    invoke-virtual {v0}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v0

    .line 14459
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_1

    .line 14460
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fontSizeName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14463
    :cond_1
    sget-object v1, Lcom/sec/chaton/settings/dk;->a:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 14464
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/am;->b(Landroid/content/Context;)F

    move-result v0

    .line 14479
    :goto_1
    return v0

    .line 14453
    :catch_0
    move-exception v2

    .line 14454
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 14455
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot get font size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14465
    :cond_2
    sget-object v1, Lcom/sec/chaton/settings/dk;->b:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 14466
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090135

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto :goto_1

    .line 14467
    :cond_3
    sget-object v1, Lcom/sec/chaton/settings/dk;->c:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 14468
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090136

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto :goto_1

    .line 14469
    :cond_4
    sget-object v1, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 14470
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto :goto_1

    .line 14471
    :cond_5
    sget-object v1, Lcom/sec/chaton/settings/dk;->e:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 14472
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto :goto_1

    .line 14473
    :cond_6
    sget-object v1, Lcom/sec/chaton/settings/dk;->f:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 14474
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto/16 :goto_1

    .line 14476
    :cond_7
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_8

    .line 14477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown fontSizeName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14479
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto/16 :goto_1
.end method

.method static synthetic aC(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aH:Ljava/lang/String;

    return-object v0
.end method

.method private aD()Landroid/graphics/Typeface;
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 14485
    .line 14487
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "Default Font Typeface"

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 14492
    :goto_0
    if-ne v0, v1, :cond_0

    .line 14493
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 14503
    :goto_1
    return-object v0

    .line 14488
    :catch_0
    move-exception v0

    move v0, v1

    .line 14490
    goto :goto_0

    .line 14495
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 14496
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14499
    :try_start_1
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_1

    .line 14501
    :catch_1
    move-exception v0

    .line 14502
    const-string v0, "Cannot make font from file"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14503
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_1
.end method

.method static synthetic aD(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aI:Z

    return v0
.end method

.method static synthetic aE(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aq:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private aE()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14511
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cw:Z

    if-eqz v0, :cond_0

    .line 14513
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aF()V

    .line 14514
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aG()V

    .line 14515
    iput-boolean v7, p0, Lcom/sec/chaton/chat/ChatFragment;->cw:Z

    .line 14571
    :goto_0
    return-void

    .line 14518
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onResume() ChatRoom] InboxNO : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14521
    :cond_1
    const-string v0, "onResume - QUERY_INBOX"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14522
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 14523
    const-string v0, ""

    .line 14524
    const-string v5, ""

    .line 14525
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v3, v4, :cond_2

    .line 14526
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 14527
    const-string v5, "buddy_no=?"

    move-object v4, v0

    .line 14536
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 14537
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/q;->a()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "inbox_no=?"

    new-array v6, v1, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14528
    :cond_2
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v3, v4, :cond_5

    .line 14529
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    .line 14530
    const-string v5, "inbox_no = ( SELECT inbox_no FROM inbox_buddy_relation WHERE buddy_no = ? )"

    move-object v4, v0

    goto :goto_1

    .line 14546
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/q;->g()Landroid/net/Uri;

    move-result-object v3

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    aput-object v4, v6, v7

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    aput-object v4, v6, v1

    const/4 v4, 0x2

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v7}, Lcom/sec/chaton/e/r;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14561
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/q;->a()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "inbox_no=?"

    new-array v6, v1, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move-object v4, v0

    goto :goto_1
.end method

.method static synthetic aF(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    return-object v0
.end method

.method private aF()V
    .locals 10

    .prologue
    const/4 v4, 0x3

    const-wide v5, 0x7fffffffffffffffL

    const/4 v3, 0x2

    const/4 v9, 0x0

    .line 14884
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15100
    :cond_0
    :goto_0
    return-void

    .line 14888
    :cond_1
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cu:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_4

    .line 14891
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_3

    .line 14892
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->b()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 14897
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/d/o;->c(J)V

    .line 14901
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->p()Z

    move-result v0

    if-nez v0, :cond_2

    .line 14904
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->k()V

    .line 14912
    :cond_2
    :goto_1
    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v8

    .line 14913
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {}, Lcom/sec/chaton/e/i;->f()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buddy_no IN"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/sec/chaton/chat/eq;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 14923
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15099
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 14908
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_2

    .line 14909
    const-string v0, "initInboxInfoAndQueryParticipant() - mMessageControl is null"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 14923
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 14925
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cv:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 14927
    array-length v0, v1

    if-le v0, v3, :cond_5

    aget-object v0, v1, v3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 14928
    const/4 v0, 0x1

    aget-object v2, v1, v0

    aget-object v3, v1, v3

    array-length v0, v1

    if-le v0, v4, :cond_c

    aget-object v0, v1, v4

    :goto_3
    invoke-direct {p0, v2, v3, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 14929
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->G()V

    .line 14932
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 14933
    array-length v0, v1

    if-lez v0, :cond_d

    const/4 v0, 0x0

    aget-object v0, v1, v0

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    .line 14936
    :cond_6
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    .line 14937
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_7

    .line 14938
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    .line 14943
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14960
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 14964
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/MyEditText;->getLineHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f99999a    # 1.2f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 14966
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cY:Z

    if-nez v1, :cond_8

    .line 14969
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 14971
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 14972
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 14973
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setSelection(I)V

    .line 14997
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Ljava/lang/String;)V

    .line 14999
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    if-eqz v0, :cond_e

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 15000
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    iget v4, p0, Lcom/sec/chaton/chat/ChatFragment;->bb:I

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 15002
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/d/o;->c(J)V

    .line 15005
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 15010
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_9

    .line 15011
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aL:Ljava/lang/Long;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;J)Z

    .line 15018
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_a

    .line 15019
    invoke-static {v9}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;)V

    .line 15074
    :cond_a
    :goto_5
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-nez v0, :cond_b

    .line 15076
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aq:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 15077
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ax()V

    .line 15083
    :cond_b
    const-string v0, "initInboxInfoAndQueryParticipant_QUERY_PARTICIPANTS_AFTER_INSERTED"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 15084
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/e/z;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v2, v9

    move-object v4, v9

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 15094
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->g()V

    goto/16 :goto_2

    :cond_c
    move-object v0, v9

    .line 14928
    goto/16 :goto_3

    .line 14933
    :cond_d
    const-string v0, ""

    goto/16 :goto_4

    .line 15025
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 15032
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/d/o;->c(J)V

    .line 15034
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_f

    .line 15035
    invoke-static {v9}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;)V

    goto :goto_5

    .line 15036
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_10

    .line 15042
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_a

    .line 15043
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 15044
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aL:Ljava/lang/Long;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;J)Z

    goto/16 :goto_5

    .line 15053
    :cond_10
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_a

    .line 15056
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    if-nez v0, :cond_11

    .line 15059
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object v3, p0

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    goto/16 :goto_5

    .line 15067
    :cond_11
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    iget v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bb:I

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object v3, p0

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    goto/16 :goto_5
.end method

.method private aG()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 15105
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->H()V

    .line 15106
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/ei;->a(Z)V

    .line 15107
    const-string v0, "QUERY_INBOX - QUERY_MESSAGE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 15111
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->be:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aX:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_2

    .line 15112
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/16 v1, 0xf

    iget v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aX:I

    invoke-static {v3}, Lcom/sec/chaton/e/v;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v9, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v4, v6, v8

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 15144
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->be:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v0, :cond_0

    .line 15145
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 15146
    const-string v0, "inbox_is_entered"

    const-string v1, "Y"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 15147
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v1, -0x1

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inbox_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 15179
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_3

    .line 15180
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/16 v1, 0x11

    sget-object v3, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "buddy_show_phone_number"

    aput-object v5, v4, v8

    const-string v5, "buddy_extra_info"

    aput-object v5, v4, v9

    const/4 v5, 0x2

    const-string v6, "buddy_msisdns"

    aput-object v6, v4, v5

    const-string v5, "buddy_no = ?"

    new-array v6, v9, [Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    aput-object v7, v6, v8

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 15189
    :goto_1
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    .line 15190
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->V:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/d/m;->d(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 15193
    :cond_1
    return-void

    .line 15125
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bR:I

    invoke-static {v1}, Lcom/sec/chaton/e/v;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v9, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v1, v6, v8

    move v1, v8

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15184
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ax()V

    goto :goto_1
.end method

.method static synthetic aG(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->B()V

    return-void
.end method

.method private aH()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 15202
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    if-eqz v2, :cond_1

    .line 15203
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v2}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v2

    if-le v2, v0, :cond_0

    .line 15208
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 15206
    goto :goto_0

    :cond_1
    move v0, v1

    .line 15208
    goto :goto_0
.end method

.method static synthetic aH(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bS:Z

    return v0
.end method

.method private aI()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 15648
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 15649
    if-nez v0, :cond_1

    .line 15697
    :cond_0
    :goto_0
    return-void

    .line 15653
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 15655
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v1, :cond_4

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 15658
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 15659
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 15660
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    if-eqz v0, :cond_3

    .line 15661
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    if-eqz v0, :cond_2

    .line 15663
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15664
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15675
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 15668
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15669
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    .line 15672
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15673
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    .line 15677
    :cond_4
    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 15679
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 15680
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15681
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    if-eqz v0, :cond_5

    .line 15682
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 15683
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15685
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15686
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 15689
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 15690
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bp:Z

    if-eqz v0, :cond_0

    .line 15691
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15692
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 15693
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method static synthetic aI(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aa()V

    return-void
.end method

.method static synthetic aJ(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ba:Ljava/lang/String;

    return-object v0
.end method

.method private aJ()V
    .locals 1

    .prologue
    .line 16165
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 16166
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->e()V

    .line 16168
    :cond_0
    return-void
.end method

.method static synthetic aK(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    return-object v0
.end method

.method private aK()V
    .locals 1

    .prologue
    .line 16171
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 16172
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 16173
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->f()V

    .line 16176
    :cond_0
    return-void
.end method

.method static synthetic aL(Lcom/sec/chaton/chat/ChatFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    return-object v0
.end method

.method static synthetic aM(Lcom/sec/chaton/chat/ChatFragment;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic aN(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic aO(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aj()Z

    move-result v0

    return v0
.end method

.method static synthetic aP(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->al()Z

    move-result v0

    return v0
.end method

.method static synthetic aQ(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aS:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aR(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aA()V

    return-void
.end method

.method static synthetic aS(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aT(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->co:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aU(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic aV(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cW:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic aW(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->F()V

    return-void
.end method

.method static synthetic aX(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/cv;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    return-object v0
.end method

.method private aa()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 11201
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 11202
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 11223
    :cond_0
    :goto_0
    return-void

    .line 11206
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 11207
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 11209
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 11210
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 11211
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 11212
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 11216
    :cond_2
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 11217
    :catch_0
    move-exception v0

    .line 11218
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 11219
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 11220
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic aa(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aH()Z

    move-result v0

    return v0
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 11229
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->H:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 11230
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ac()V

    .line 11250
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->H:Ljava/lang/String;

    .line 11253
    :cond_0
    :goto_1
    return-void

    .line 11233
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->H:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11237
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11239
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ad()V

    .line 11240
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ac()V

    goto :goto_1

    .line 11247
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ad()V

    .line 11248
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ac()V

    goto :goto_0
.end method

.method static synthetic ab(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cn:Z

    return v0
.end method

.method static synthetic ac(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cl:Ljava/util/ArrayList;

    return-object v0
.end method

.method private ac()V
    .locals 3

    .prologue
    .line 11256
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->l(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bB:Landroid/graphics/Bitmap;

    .line 11257
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bB:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 11258
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bk:Ljava/lang/String;

    const-string v1, "pa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11259
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bB:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 11260
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 11261
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bF:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 11268
    :goto_0
    return-void

    .line 11263
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bF:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bB:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 11266
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->j()V

    goto :goto_0
.end method

.method static synthetic ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    return-object v0
.end method

.method private ad()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11277
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bB:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 11278
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bF:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 11279
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bB:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 11280
    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bB:Landroid/graphics/Bitmap;

    .line 11282
    :cond_0
    return-void
.end method

.method static synthetic ae(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    return-object v0
.end method

.method private ae()V
    .locals 1

    .prologue
    .line 11411
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->de:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_0

    .line 11412
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->de:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->dismiss()V

    .line 11416
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->df:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_1

    .line 11417
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->df:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->dismiss()V

    .line 11420
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dg:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_2

    .line 11421
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dg:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->dismiss()V

    .line 11424
    :cond_2
    return-void
.end method

.method static synthetic af(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    return-object v0
.end method

.method private af()V
    .locals 1

    .prologue
    .line 11428
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->de:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_0

    .line 11429
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->de:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->hide()V

    .line 11433
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->df:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_1

    .line 11434
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->df:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->hide()V

    .line 11437
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dg:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_2

    .line 11438
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dg:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->hide()V

    .line 11441
    :cond_2
    return-void
.end method

.method private ag()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12066
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    .line 12067
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin_type"

    const-string v2, "pa"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bk:Ljava/lang/String;

    .line 12068
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Default Font Size"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bl:Ljava/lang/String;

    .line 12069
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_send"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    .line 12070
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_receive"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    .line 12071
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting enter key"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bo:Z

    .line 12072
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting push to talk"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bp:Z

    .line 12074
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->m()V

    .line 12076
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 12077
    const-string v0, "Preference information for chat room."

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12078
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, " > Skin Id: "

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12079
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, " > Font Size: "

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bl:Ljava/lang/String;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12080
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, " > Send Bubble: "

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12081
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, " > Receive Bubble: "

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12082
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, " > Send With Enter: "

    aput-object v1, v0, v3

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bo:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12083
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, " > Push to talk: "

    aput-object v1, v0, v3

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bp:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12084
    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, " > Compatibility: "

    aput-object v1, v0, v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "compatibility"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12086
    :cond_0
    return-void
.end method

.method static synthetic ag(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ax()V

    return-void
.end method

.method static synthetic ah(Lcom/sec/chaton/chat/ChatFragment;)I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bJ:I

    return v0
.end method

.method private ah()V
    .locals 5

    .prologue
    const v0, 0x7f0b01a0

    const/4 v1, 0x0

    .line 12575
    .line 12578
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_1

    move v2, v0

    move v0, v1

    .line 12591
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 12592
    if-lez v0, :cond_0

    .line 12593
    const-string v2, "("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12595
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bi:Ljava/lang/String;

    .line 12596
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bi:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12598
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v2, :cond_4

    .line 12599
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 12603
    :goto_1
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->d(Z)V

    .line 12605
    return-void

    .line 12581
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_2

    .line 12582
    const v2, 0x7f0b0063

    .line 12583
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12584
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 12585
    const v2, 0x7f0b0064

    .line 12586
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 12588
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown ChatType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v0

    move v0, v1

    goto/16 :goto_0

    .line 12601
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic ai(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ar()V

    return-void
.end method

.method private ai()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 13011
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aj()Z

    move-result v1

    if-nez v1, :cond_1

    .line 13023
    :cond_0
    :goto_0
    return v0

    .line 13015
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ak()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13019
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->al()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13023
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic aj(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->as()V

    return-void
.end method

.method private aj()Z
    .locals 2

    .prologue
    .line 13475
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 13476
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 13477
    const-string v0, "mAttachedActivity is null"

    const-class v1, Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13480
    :cond_0
    const/4 v0, 0x0

    .line 13482
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private ak()Z
    .locals 1

    .prologue
    .line 13489
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13490
    const/4 v0, 0x0

    .line 13492
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic ak(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bO:Z

    return v0
.end method

.method static synthetic al(Lcom/sec/chaton/chat/ChatFragment;)I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bM:I

    return v0
.end method

.method private al()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 13499
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13500
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b003d

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 13504
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic am(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bF:Landroid/widget/ImageView;

    return-object v0
.end method

.method private am()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 13511
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v1

    if-nez v1, :cond_1

    .line 13512
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b00dd

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 13516
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic an(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aB()V

    return-void
.end method

.method private an()Z
    .locals 3

    .prologue
    .line 13524
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 13526
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 13529
    const/4 v0, 0x0

    .line 13532
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private ao()Z
    .locals 1

    .prologue
    .line 13664
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->b(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic ao(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->I()Z

    move-result v0

    return v0
.end method

.method static synthetic ap(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    return-object v0
.end method

.method private ap()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 13686
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 13699
    :cond_0
    :goto_0
    return v0

    .line 13691
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 13692
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    const v2, 0x7f0b0155

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setText(I)V

    .line 13693
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 13694
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 13696
    goto :goto_0
.end method

.method private aq()Z
    .locals 3

    .prologue
    .line 13703
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 13705
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 13706
    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 13708
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13709
    const/4 v0, 0x0

    .line 13711
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic aq(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cY:Z

    return v0
.end method

.method private ar()V
    .locals 2

    .prologue
    .line 13739
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->D:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 13740
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b0261

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->D:Landroid/app/ProgressDialog;

    .line 13743
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 13744
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 13746
    :cond_1
    return-void
.end method

.method static synthetic ar(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aF()V

    return-void
.end method

.method private as()V
    .locals 1

    .prologue
    .line 13749
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->D:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13750
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 13752
    :cond_0
    return-void
.end method

.method static synthetic as(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aG()V

    return-void
.end method

.method static synthetic at(Lcom/sec/chaton/chat/ChatFragment;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    return-object v0
.end method

.method private at()Z
    .locals 1

    .prologue
    .line 13755
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 13756
    const/4 v0, 0x1

    .line 13758
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic au(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    return-object v0
.end method

.method private au()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 13762
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    if-nez v0, :cond_3

    .line 13763
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 13764
    const-string v0, "Initialize emoticon selection view."

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13767
    :cond_0
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 13769
    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    .line 13770
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 13771
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cB:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a(Landroid/view/View;)V

    .line 13772
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v1, :cond_1

    .line 13773
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v2, 0x42460000    # 49.5f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 13776
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 13777
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->setVisibility(I)V

    .line 13779
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->setEmoticonSelectedListener(Lcom/sec/chaton/multimedia/emoticon/i;)V

    .line 13781
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bG:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 13785
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    if-nez v0, :cond_2

    .line 13786
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    .line 13787
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 13788
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/chat/bu;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bu;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13796
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ar:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 13799
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 13802
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_4

    .line 13803
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cB:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Landroid/view/View;)V

    .line 13804
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->e(I)V

    .line 13805
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->N()V

    .line 13808
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->setVisibility(I)V

    .line 13809
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 13810
    return-void
.end method

.method private av()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 13813
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    if-nez v0, :cond_1

    .line 13827
    :cond_0
    :goto_0
    return-void

    .line 13817
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bH:Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->setVisibility(I)V

    .line 13818
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cH:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 13820
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 13821
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->O()V

    .line 13822
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    if-nez v0, :cond_0

    .line 13823
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->L()V

    .line 13824
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic av(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ah()V

    return-void
.end method

.method static synthetic aw(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aQ:Ljava/lang/String;

    return-object v0
.end method

.method private aw()V
    .locals 2

    .prologue
    .line 13990
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/ei;->a(Landroid/graphics/Bitmap;)V

    .line 13991
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    if-eqz v0, :cond_0

    .line 13992
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->invalidateViews()V

    .line 13994
    :cond_0
    return-void
.end method

.method private ax()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 14020
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ax:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 14021
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->at:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 14023
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 14024
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 14025
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 14028
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    .line 14029
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aw()V

    .line 14031
    :cond_1
    return-void
.end method

.method static synthetic ax(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bw:Z

    return v0
.end method

.method static synthetic ay(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bv:Ljava/lang/String;

    return-object v0
.end method

.method private ay()Z
    .locals 2

    .prologue
    .line 14034
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ay:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 14035
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 14036
    const-string v0, "isLoadingFromServer - true"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14038
    :cond_0
    const/4 v0, 0x1

    .line 14043
    :goto_0
    return v0

    .line 14040
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 14041
    const-string v0, "isLoadingFromServer - false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14043
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private az()V
    .locals 10

    .prologue
    const-wide/16 v3, 0x0

    const/4 v9, 0x0

    .line 14047
    iput-boolean v9, p0, Lcom/sec/chaton/chat/ChatFragment;->bN:Z

    .line 14049
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 14050
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 14051
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14052
    const-string v0, "loadMessageFromServer - network is not available"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 14111
    :cond_1
    :goto_0
    return-void

    .line 14057
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-nez v0, :cond_3

    .line 14058
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14059
    const-string v0, "loadMessageFromServer - did not mapping samsung account"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14064
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ay()Z

    move-result v0

    if-nez v0, :cond_1

    .line 14068
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 14069
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14070
    const-string v0, "loadMessageFromServer - don\'t need to load more message"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14075
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 14076
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14077
    const-string v0, "loadMessageFromServer - session is null, don\'t need to load more message"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14082
    :cond_5
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-nez v0, :cond_6

    .line 14083
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14084
    const-string v0, "inbox is invalid - don\'t need to load more message"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14089
    :cond_6
    const/16 v6, 0x1e

    .line 14092
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 14094
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 14095
    if-eqz v0, :cond_8

    .line 14096
    const-string v1, "message_time"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 14097
    sget-boolean v5, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v5, :cond_7

    .line 14098
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadMessageFromServer()-lastMsg : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "message_content"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_7
    move-wide v0, v1

    :goto_1
    move-wide v7, v0

    .line 14106
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->V:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static/range {v0 .. v6}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;I)V

    .line 14107
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ay:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 14108
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 14109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadMessageFromServer - inboxNo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",sessionId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",tid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",count:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14101
    :catch_0
    move-exception v0

    .line 14102
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-wide v7, v3

    .line 14103
    goto :goto_2

    :cond_8
    move-wide v0, v3

    goto :goto_1

    :cond_9
    move-wide v7, v3

    goto :goto_2
.end method

.method static synthetic az(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->by:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatFragment;I)I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bR:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bR:I

    return v0
.end method

.method private b(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 4860
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4861
    const-string v1, "content_type"

    invoke-virtual {p1}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4862
    const-string v1, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4864
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4865
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4874
    :goto_0
    const-string v1, "chatType"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4875
    const-string v1, "is_forward_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4877
    return-object v0

    .line 4867
    :cond_0
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4870
    const-string v1, "sub_content"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cp:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private b(Landroid/view/View;I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 16110
    new-instance v0, Lcom/sec/chaton/util/m;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-direct {v0, v1}, Lcom/sec/chaton/util/m;-><init>(Lcom/sec/chaton/e/w;)V

    .line 16111
    const v1, 0x7f0b0412

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/m;->setEmptyText(Ljava/lang/CharSequence;)V

    .line 16112
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-direct {v1, p1, v0, p2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;I)V

    .line 16113
    new-instance v0, Lcom/sec/chaton/chat/co;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/co;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setItemSelectListener(Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V

    .line 16121
    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setGravity(I)V

    .line 16122
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setDirection(I)V

    .line 16123
    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setScrollEnabled(Z)V

    .line 16124
    const/16 v0, 0x78

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setPosition(II)V

    .line 16126
    return-object v1
.end method

.method private b(J)Lcom/sec/chaton/widget/c;
    .locals 4

    .prologue
    .line 13437
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    if-nez v0, :cond_0

    .line 13438
    new-instance v0, Lcom/sec/chaton/widget/c;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/widget/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    .line 13439
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b0226

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setTitle(I)V

    .line 13440
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b01ce

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setMessage(Ljava/lang/CharSequence;)V

    .line 13441
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setCancelable(Z)V

    .line 13450
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/chat/bt;

    invoke-direct {v3, p0, p1, p2}, Lcom/sec/chaton/chat/bt;-><init>(Lcom/sec/chaton/chat/ChatFragment;J)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/widget/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 13470
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method private b(I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1694
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 1695
    new-instance v0, Lcom/sec/chaton/chat/dg;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/chat/dg;-><init>(Lcom/sec/chaton/chat/ChatFragment;Z)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    .line 1700
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    if-eqz v0, :cond_1

    .line 1701
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 1702
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_3

    .line 1703
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    new-array v1, v2, [Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->di:Landroid/graphics/Bitmap;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/dg;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1708
    :cond_1
    :goto_1
    return-void

    .line 1696
    :cond_2
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 1697
    new-instance v0, Lcom/sec/chaton/chat/dg;

    invoke-direct {v0, p0, v4}, Lcom/sec/chaton/chat/dg;-><init>(Lcom/sec/chaton/chat/ChatFragment;Z)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    goto :goto_0

    .line 1705
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->di:Landroid/graphics/Bitmap;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/dg;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method private b(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 5846
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 5847
    if-eqz v0, :cond_0

    .line 5849
    const-string v1, "AMS_FILE_PATH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5851
    const-string v1, "AMS_FILE_PATH_TO_MANAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5852
    const-string v2, "AMS_WITH_TEXT"

    invoke-virtual {v0, v2, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 5853
    const-string v2, "AMS_FILE_TYPE"

    invoke-virtual {v0, v2, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 5855
    if-nez v4, :cond_1

    .line 5894
    :cond_0
    :goto_0
    return-void

    .line 5858
    :cond_1
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5860
    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    .line 5861
    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5862
    sget-object v3, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    .line 5865
    :cond_2
    const-string v2, "jpg"

    .line 5867
    if-nez v9, :cond_6

    .line 5869
    sget-object v5, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v3, v5, :cond_3

    if-lez v0, :cond_3

    .line 5870
    new-instance v5, Lcom/sec/chaton/chat/fl;

    invoke-direct {v5, v1, v0}, Lcom/sec/chaton/chat/fl;-><init>(Ljava/lang/String;I)V

    .line 5871
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 5872
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5876
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_7

    .line 5877
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_5

    .line 5878
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5879
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 5883
    :cond_5
    const/4 v7, 0x1

    .line 5885
    :goto_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    new-array v6, v10, [Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J

    .line 5888
    :cond_6
    if-eqz v9, :cond_0

    move-object v5, p0

    move-object v6, v11

    move-object v7, v3

    move-object v9, v8

    .line 5890
    invoke-direct/range {v5 .. v10}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_7
    move v7, v10

    goto :goto_1
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 15196
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/b/e;->c()Landroid/os/Message;

    move-result-object v0

    .line 15197
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 15198
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/b/e;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 15199
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 14772
    const-string v0, "inbox_last_chat_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cu:I

    .line 14773
    const-string v0, "inbox_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 14774
    const-string v0, "inbox_last_msg_no"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aL:Ljava/lang/Long;

    .line 14775
    const-string v0, "inbox_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aP:Ljava/lang/String;

    .line 14776
    const-string v0, "_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->z:I

    .line 14777
    const-string v0, "inbox_server_ip"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    .line 14778
    const-string v0, "inbox_server_port"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bb:I

    .line 14779
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14780
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aM:Ljava/lang/String;

    .line 14781
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bb:I

    .line 14784
    :cond_0
    const-string v0, "inbox_title_fixed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aQ:Ljava/lang/String;

    .line 14785
    const-string v0, "inbox_last_msg_sender"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aR:Ljava/lang/String;

    .line 14786
    const-string v0, "inbox_last_temp_msg"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cv:Ljava/lang/String;

    .line 14787
    const-string v0, "buddy_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aS:Ljava/lang/String;

    .line 14788
    const-string v0, "inbox_trunk_unread_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aW:I

    .line 14789
    const-string v0, "buddy_no"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    .line 14790
    const-string v0, "inbox_last_timestamp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    .line 14791
    const-string v0, "Y"

    const-string v1, "inbox_enable_noti"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    .line 14792
    const-string v0, "inbox_unread_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aX:I

    .line 14793
    const-string v0, "Y"

    const-string v1, "inbox_is_entered"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->be:Z

    .line 14795
    const-string v0, "lasst_session_merge_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aU:J

    .line 14797
    const-string v0, "inbox_last_tid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    .line 14798
    const-string v0, "Y"

    const-string v1, "inbox_enable_translate"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    .line 14799
    const-string v0, "Y"

    const-string v1, "translate_outgoing_message"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    .line 14800
    const-string v0, "inbox_translate_my_language"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    .line 14801
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14802
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/b/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    .line 14803
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cK:Z

    .line 14807
    :goto_0
    const-string v0, "inbox_translate_buddy_language"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    .line 14808
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/b/a;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    .line 14809
    return-void

    .line 14805
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cK:Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->k(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/io/File;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 6131
    .line 6132
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v1, v3, :cond_2

    .line 6133
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v1, v3, :cond_1

    .line 6134
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 6135
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 6139
    :cond_1
    const/4 v7, 0x1

    .line 6142
    :goto_0
    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J

    .line 6144
    return-void

    :cond_2
    move v7, v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 9448
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 9452
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 9453
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/h;->b()Lcom/sec/common/f/b;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/f/b;->a(Ljava/util/List;Z)V

    .line 9456
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 9458
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 9462
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v1}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/widget/HeightChangedListView;->setSelectionFromTop(II)V

    .line 9464
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->k()V

    .line 9470
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bR:I

    .line 9473
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->B()V

    .line 9475
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    return p1
.end method

.method private b(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 13668
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 13670
    const/4 v2, -0x3

    if-eq v2, v1, :cond_0

    const/4 v2, -0x2

    if-ne v2, v1, :cond_2

    .line 13671
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    const v2, 0x7f0b0205

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(I)V

    .line 13672
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setDuration(I)V

    .line 13673
    if-eqz p1, :cond_1

    .line 13674
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 13680
    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ChatFragment;I)I
    .locals 0

    .prologue
    .line 319
    iput p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bR:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    return-object p1
.end method

.method private c(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5897
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5898
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    .line 5900
    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Lcom/sec/chaton/multimedia/doc/b;)Lcom/sec/chaton/e/w;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 5901
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cx:Z

    return p1
.end method

.method private c(Z)Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 14577
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    .line 14631
    :cond_0
    :goto_0
    return v6

    .line 14581
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/am;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14588
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->R()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14599
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 14600
    const-string v0, "buddy_no"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14601
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 14602
    const/16 v2, 0x27

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\',"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 14604
    :cond_3
    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14606
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "buddy_extra_info"

    aput-object v5, v2, v6

    const-string v5, "buddy_name"

    aput-object v5, v2, v7

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 14612
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v0, v2, :cond_6

    .line 14613
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v6

    .line 14615
    :cond_4
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 14616
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "voip=1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v7

    .line 14624
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    move v6, v0

    .line 14627
    :cond_6
    :goto_2
    if-eqz v1, :cond_0

    .line 14628
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 14619
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] voip not supported : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ChatFragment;I)I
    .locals 0

    .prologue
    .line 319
    iput p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bJ:I

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    return-object p1
.end method

.method private d(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5906
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5907
    sget-object v4, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 5908
    return-void
.end method

.method private d(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 16299
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16320
    :cond_0
    :goto_0
    return-void

    .line 16303
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cC:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 16307
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cF:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    .line 16309
    if-eqz p1, :cond_3

    move v0, v1

    .line 16313
    :goto_1
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->cC:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 16315
    if-ne v3, v1, :cond_2

    if-ne v0, v1, :cond_2

    .line 16316
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cD:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 16318
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cD:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cK:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ChatFragment;I)I
    .locals 0

    .prologue
    .line 319
    iput p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bM:I

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private e(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5913
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 5914
    instance-of v1, v0, Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 5915
    check-cast v0, Ljava/util/ArrayList;

    .line 5917
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 5919
    new-instance v1, Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5920
    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 5923
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->t(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/chat/ChatFragment;I)I
    .locals 0

    .prologue
    .line 319
    iput p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    return-object p1
.end method

.method private f(Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 5928
    if-eqz p1, :cond_3

    const-string v0, "GEOPOINT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 5929
    const/4 v4, 0x0

    .line 5930
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_0

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5931
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    .line 5932
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5933
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 5936
    :cond_1
    const/4 v4, 0x1

    .line 5938
    :cond_2
    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v7

    .line 5939
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    const-string v5, "GEOPOINT"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "GEOADDRESS"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)J

    .line 5940
    monitor-exit v7

    .line 5942
    :cond_3
    return-void

    .line 5940
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic f(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    return p1
.end method

.method static synthetic g(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aO:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic g(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    sput-object p0, Lcom/sec/chaton/chat/ChatFragment;->cL:Ljava/lang/String;

    return-object p0
.end method

.method private g(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5945
    if-eqz p1, :cond_0

    .line 5946
    new-instance v1, Ljava/io/File;

    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5947
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 5948
    sget-object v4, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 5950
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bN:Z

    return p1
.end method

.method static synthetic h(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aS:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic h(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    sput-object p0, Lcom/sec/chaton/chat/ChatFragment;->cM:Ljava/lang/String;

    return-object p0
.end method

.method private h(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5953
    if-eqz p1, :cond_0

    .line 5954
    new-instance v1, Ljava/io/File;

    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5955
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 5957
    sget-object v4, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 5959
    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cP:Z

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aZ:Z

    return p1
.end method

.method static synthetic i(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private i(Ljava/lang/String;)Lcom/sec/chaton/e/w;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1929
    .line 1931
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1932
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1938
    :goto_0
    const-string v2, "image/"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1939
    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    .line 1954
    :cond_0
    :goto_1
    return-object v1

    .line 1941
    :cond_1
    const-string v2, "video/"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1942
    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    goto :goto_1

    .line 1944
    :cond_2
    const-string v2, "audio/"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1945
    sget-object v1, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    goto :goto_1

    .line 1947
    :cond_3
    const-string v2, "text/plain"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1948
    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    goto :goto_1

    .line 1950
    :cond_4
    const-string v2, "application/"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951
    sget-object v1, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aP:Ljava/lang/String;

    return-object p1
.end method

.method private i(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 5962
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5965
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5966
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5974
    :goto_0
    if-nez v1, :cond_2

    .line 5975
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 5976
    const-string v0, "sendVideo file is null"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6007
    :cond_0
    :goto_1
    return-void

    .line 5968
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->c(Ljava/lang/String;)Lcom/sec/chaton/util/u;

    move-result-object v0

    .line 5969
    if-eqz v0, :cond_6

    .line 5970
    iget-object v1, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    goto :goto_0

    .line 5982
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v8, 0xa00000

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    .line 5983
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 5988
    :cond_3
    new-instance v2, Ljava/util/StringTokenizer;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "."

    invoke-direct {v2, v0, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 5990
    :goto_2
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 5991
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 5994
    :cond_4
    sget-object v2, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-static {v2, v0}, Lcom/sec/chaton/util/r;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5995
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 5996
    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b002f

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b035a

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 5998
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_1

    .line 6003
    :cond_5
    sget-object v2, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_6
    move-object v1, v3

    goto/16 :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aY:Z

    return p1
.end method

.method static synthetic j(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    return-object p1
.end method

.method private j(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 6010
    if-eqz p1, :cond_0

    .line 6011
    const-string v0, "preview_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cl:Ljava/util/ArrayList;

    .line 6012
    new-instance v0, Lcom/sec/chaton/chat/cx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/cx;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/c;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/cx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 6014
    :cond_0
    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2124
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2126
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 2135
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/b;->a(Ljava/lang/String;)V

    .line 2137
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    return v0
.end method

.method static synthetic j(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    return p1
.end method

.method static synthetic k(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aG:Ljava/lang/String;

    return-object p1
.end method

.method private k(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2802
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2803
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2805
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aE:Lcom/sec/chaton/trunk/a/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/trunk/a/a;->a(Ljava/util/List;)Lcom/sec/chaton/trunk/a/a/f;

    .line 2807
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bp:Z

    return v0
.end method

.method static synthetic k(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bw:Z

    return p1
.end method

.method static synthetic l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->di:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private l(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/high16 v3, 0x426c0000    # 59.0f

    .line 11285
    const/4 v0, 0x0

    .line 11288
    const-string v1, "skin_myskin.png_"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 11289
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 11291
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 11292
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v1

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v2

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;III)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 11303
    :cond_0
    :goto_0
    return-object v0

    .line 11294
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v1

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v2

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/util/ad;->b(Landroid/content/Context;III)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 11297
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/downloads/cd;->f(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/ck;

    move-result-object v1

    .line 11298
    if-eqz v1, :cond_0

    .line 11299
    iget-object v0, v1, Lcom/sec/chaton/settings/downloads/ck;->b:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method static synthetic l(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aH:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic l(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->by:Z

    return p1
.end method

.method static synthetic m(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->s(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private m(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 13354
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 13355
    const-string v1, "ACTIVITY_PURPOSE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 13356
    const-string v1, "URI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 13358
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 13359
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bo:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bx:Z

    return p1
.end method

.method static synthetic n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aV:Ljava/lang/String;

    return-object p1
.end method

.method private n(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 13362
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/vcard/ReadVCardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 13363
    const-string v1, "URI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 13364
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 13365
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->aI:Z

    return p1
.end method

.method static synthetic o(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aJ()V

    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->m(Ljava/lang/String;)V

    return-void
.end method

.method private o(Ljava/lang/String;)V
    .locals 8

    .prologue
    const v7, 0x7f0b040a

    const v6, 0x7f0b01ac

    const v5, 0x7f0b0037

    .line 13368
    if-eqz p1, :cond_0

    .line 13369
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 13370
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 13373
    const-string v0, "file://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 13374
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 13377
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 13379
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13380
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 13424
    :cond_0
    :goto_1
    return-void

    .line 13383
    :cond_1
    invoke-static {p1}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v1

    .line 13384
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->j:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v0, :cond_2

    .line 13385
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 13386
    invoke-virtual {v0, v6}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0207

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_1

    .line 13388
    :cond_2
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v0, :cond_0

    .line 13389
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 13390
    invoke-virtual {p0, v7}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 13391
    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->e:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v3, :cond_6

    .line 13392
    const v0, 0x7f0b040b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 13398
    :cond_3
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "market://search?q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&c=apps"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 13400
    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->m:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v3, :cond_4

    .line 13401
    const-string v0, "market://details?id=com.sec.android.app.contentviewer"

    .line 13403
    :cond_4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 13405
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    .line 13407
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_5

    .line 13408
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isMarketAvailable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", strUri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ChatListAdapter"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13411
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 13412
    invoke-virtual {v0, v6}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v3

    const v4, 0x7f0b0266

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/chat/bs;

    invoke-direct {v4, p0, v1, v2}, Lcom/sec/chaton/chat/bs;-><init>(Lcom/sec/chaton/chat/ChatFragment;ZLandroid/content/Intent;)V

    invoke-virtual {v3, v5, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 13420
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_1

    .line 13393
    :cond_6
    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->d:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v3, :cond_7

    .line 13394
    const v0, 0x7f0b040c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 13395
    :cond_7
    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v3, :cond_8

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->c:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v3, :cond_8

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v3, :cond_8

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v3, :cond_3

    .line 13396
    :cond_8
    invoke-virtual {p0, v7}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_9
    move-object v0, p1

    goto/16 :goto_0
.end method

.method static synthetic o(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    return p1
.end method

.method static synthetic p(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->n(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/chat/ChatFragment;Z)Z
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cn:Z

    return p1
.end method

.method private p(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 13997
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cq:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13998
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 14007
    :cond_0
    :goto_0
    return v0

    .line 14002
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cq:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 14003
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14004
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic q(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aK()V

    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->o(Ljava/lang/String;)V

    return-void
.end method

.method private q(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 14011
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->V:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 14012
    invoke-virtual {v0, p1}, Lcom/sec/chaton/d/h;->h(Ljava/lang/String;)V

    .line 14013
    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    return-object v0
.end method

.method private r(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 14687
    const/4 v0, 0x0

    .line 14689
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14697
    :cond_0
    :goto_0
    return v0

    .line 14693
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14694
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private s(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14701
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14702
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 14703
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "|"

    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 14705
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 14706
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 14707
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 14709
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x13

    if-lt v3, v4, :cond_0

    const-string v3, "10"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 14710
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chaton id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14718
    :goto_1
    const-string v2, ""

    goto :goto_0

    .line 14711
    :cond_0
    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 14712
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pin number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 14714
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "msisdn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14715
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 14722
    :cond_2
    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bN:Z

    return v0
.end method

.method static synthetic t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private t(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 16282
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16283
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/TabActivity;

    if-eqz v0, :cond_0

    .line 16284
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->i()I

    move-result v0

    .line 16285
    const v1, 0x7f070009

    if-ne v0, v1, :cond_0

    .line 16286
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 16287
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 16288
    instance-of v1, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;

    if-eqz v1, :cond_0

    .line 16289
    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxFragment;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/lang/String;)V

    .line 16295
    :cond_0
    return-void
.end method

.method static synthetic u(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cx:Z

    return v0
.end method

.method static synthetic v(Lcom/sec/chaton/chat/ChatFragment;)I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bR:I

    return v0
.end method

.method static synthetic w(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->az()V

    return-void
.end method

.method static synthetic x(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->V()V

    return-void
.end method

.method static synthetic y(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->E()V

    return-void
.end method

.method static synthetic z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->cL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z(Lcom/sec/chaton/chat/ChatFragment;)Z
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->an()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J
    .locals 9

    .prologue
    .line 12295
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0xa00000

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 12296
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b00b0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 12297
    const-wide/16 v0, -0x1

    .line 12304
    :goto_0
    return-wide v0

    .line 12301
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v1}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelectionFromTop(II)V

    .line 12302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    .line 12303
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    .line 12304
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 11

    .prologue
    .line 12321
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0xa00000

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 12322
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b00b0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 12323
    const-wide/16 v0, -0x1

    .line 12331
    :goto_0
    return-wide v0

    .line 12327
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v1}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/widget/HeightChangedListView;->setSelectionFromTop(II)V

    .line 12328
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    .line 12329
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    .line 12331
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/a/l;)Lcom/sec/common/a/a;
    .locals 3

    .prologue
    const v2, 0x7f0b0037

    .line 11544
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bK:Lcom/sec/common/a/a;

    .line 11546
    sget-object v0, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne p1, v0, :cond_0

    .line 11547
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bK:Lcom/sec/common/a/a;

    const v1, 0x7f0b0195

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/chat/bh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bh;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 11553
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bK:Lcom/sec/common/a/a;

    .line 11564
    :goto_0
    return-object v0

    .line 11554
    :cond_0
    sget-object v0, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne p1, v0, :cond_1

    .line 11555
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bK:Lcom/sec/common/a/a;

    const v1, 0x7f0b00c8

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/chat/bi;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bi;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 11561
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bK:Lcom/sec/common/a/a;

    goto :goto_0

    .line 11564
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 14671
    invoke-virtual {p0, p2}, Lcom/sec/chaton/chat/ChatFragment;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 14672
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/ChatFragment;->r(Ljava/lang/String;)Z

    move-result v0

    .line 14674
    if-nez v0, :cond_0

    .line 14675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 14683
    :cond_0
    :goto_0
    return-object p1

    .line 14679
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/chaton/chat/ChatFragment;->s(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 14680
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 14681
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 14683
    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 11581
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 11582
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 11583
    const-string v0, "setPreviewText - attach activity is null. return"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 11652
    :cond_0
    :goto_0
    return-void

    .line 11597
    :cond_1
    if-nez p1, :cond_2

    .line 11598
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 11599
    const-string v0, "setPreviewText - newMsgCount is zero. return"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11604
    :cond_2
    if-eqz p3, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11609
    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    .line 11610
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne p4, v0, :cond_4

    .line 11611
    invoke-static {p2}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 11612
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 11614
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, p2, v1}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 11615
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ci:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 11648
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ch:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, p3}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 11649
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 11616
    :cond_4
    sget-object v0, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    if-ne p4, v0, :cond_5

    .line 11618
    invoke-static {p2}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 11619
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ci:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 11622
    :cond_5
    const-string v0, ""

    .line 11623
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/a;

    .line 11624
    if-nez v0, :cond_6

    .line 11625
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 11630
    :goto_2
    const-string v1, "\n"

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 11631
    const-string v2, "mixed"

    aget-object v1, v1, v4

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 11632
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v1, v3, :cond_7

    .line 11633
    invoke-static {p4, p2, v0, v2}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 11634
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ci:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 11627
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 11635
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v1, v3, :cond_0

    .line 11637
    invoke-static {p4, p2, v2}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 11639
    sget-object v3, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    if-ne p4, v3, :cond_8

    .line 11640
    invoke-static {p4, p2, v0, v2}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 11642
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ci:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_8
    move-object v0, v1

    goto :goto_3
.end method

.method public a(J)V
    .locals 4

    .prologue
    .line 4921
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 4922
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 4923
    const v1, 0x7f0b0260

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 4924
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/chat/al;

    invoke-direct {v3, p0, p1, p2}, Lcom/sec/chaton/chat/al;-><init>(Lcom/sec/chaton/chat/ChatFragment;J)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/ak;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/ak;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4961
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 4962
    return-void
.end method

.method public a(Landroid/net/Uri;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 5200
    const-string v0, "[SendingMedia] Start - Picture"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5201
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5202
    const-string v1, "image/*"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 5203
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5204
    const-string v1, "sendMode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5205
    const-string v1, "randomFName"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5207
    if-eqz p2, :cond_0

    .line 5208
    const/16 v1, 0x15

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 5212
    :goto_0
    return-void

    .line 5210
    :cond_0
    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;J)V
    .locals 1

    .prologue
    .line 10895
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/j/c/a;->c(J)V

    .line 10897
    return-void
.end method

.method public a(Landroid/view/View;JZ)V
    .locals 9

    .prologue
    const v6, 0x7f0b0179

    const v5, 0x7f0b000f

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 10076
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-nez v0, :cond_1

    .line 10232
    :cond_0
    :goto_0
    return-void

    .line 10080
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10087
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 10095
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, p1}, Lcom/sec/widget/HeightChangedListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 10098
    add-int/lit8 v0, v0, -0x1

    .line 10100
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 10101
    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 10103
    if-eqz p4, :cond_2

    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bS:Z

    if-eqz v0, :cond_2

    .line 10105
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    .line 10106
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 10107
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 10108
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    move-object v6, v0

    .line 10115
    :goto_1
    const-string v0, "message_content_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    .line 10116
    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v0

    .line 10119
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v7

    .line 10120
    invoke-virtual {v7, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 10121
    new-instance v0, Lcom/sec/chaton/chat/bc;

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/bc;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/view/View;JZ)V

    invoke-virtual {v7, v6, v0}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 10230
    invoke-virtual {v7, v8}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    .line 10231
    invoke-virtual {v7}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 10110
    :cond_2
    new-array v0, v4, [Ljava/lang/CharSequence;

    .line 10111
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 10112
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    move-object v6, v0

    goto :goto_1
.end method

.method public a(Landroid/view/View;Lcom/sec/chaton/widget/ProfileImageView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x14

    .line 10922
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    if-eqz v0, :cond_0

    .line 10946
    :goto_0
    return-void

    .line 10932
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10933
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10934
    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10935
    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10936
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 10937
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 10940
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10941
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10942
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10943
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;Ljava/lang/String;JLcom/sec/chaton/e/w;)V
    .locals 1

    .prologue
    .line 10880
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/chaton/j/c/g;->a(Ljava/lang/String;J)V

    .line 10881
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 12134
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 12135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnHeightChanged - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12141
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 6229
    const/4 v2, 0x0

    .line 6230
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_0

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6231
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    .line 6232
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6233
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 6237
    :cond_1
    const/4 v2, 0x1

    .line 6241
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aj:Z

    if-eqz v0, :cond_3

    .line 6242
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->al:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->am:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    .line 6301
    :goto_0
    return-void

    .line 6246
    :cond_3
    const/4 v5, 0x0

    .line 6248
    sget-object v0, Lcom/sec/chaton/chat/cp;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 6286
    :pswitch_1
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->ba:Ljava/lang/String;

    const-wide/16 v9, -0x1

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    move v7, v2

    move-object/from16 v12, p3

    invoke-virtual/range {v3 .. v12}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)J

    goto :goto_0

    .line 6251
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6252
    const-string v5, "jpg"

    .line 6253
    new-instance v0, Lcom/sec/chaton/chat/fl;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ae:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/chat/fl;-><init>(Ljava/lang/String;Lcom/sec/chaton/multimedia/emoticon/ams/d;)V

    .line 6254
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 6255
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 6264
    :cond_4
    :pswitch_3
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    iget-object v11, p0, Lcom/sec/chaton/chat/ChatFragment;->al:Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    move-object v3, p0

    move v10, v2

    move-object/from16 v13, p3

    invoke-virtual/range {v3 .. v13}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    goto :goto_0

    .line 6268
    :pswitch_4
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v1

    .line 6269
    :try_start_0
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->al:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/chaton/chat/ChatFragment;->am:Ljava/lang/String;

    move v7, v2

    invoke-virtual/range {v3 .. v9}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)J

    .line 6277
    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 6280
    :pswitch_5
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ac:Ljava/io/File;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    const/4 v5, 0x0

    move-object v0, p0

    move-object/from16 v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6248
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 4881
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 4882
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4883
    const-string v1, "content_type"

    invoke-virtual {p1}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4884
    const-string v1, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4885
    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    if-ne p1, v1, :cond_2

    .line 4886
    :cond_0
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4893
    :goto_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4894
    const-string v1, "forward_sender_name"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4901
    :cond_1
    const-string v1, "chatType"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4915
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 4918
    return-void

    .line 4888
    :cond_2
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4890
    const-string v1, "sub_content"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/multimedia/emoticon/ams/d;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 12008
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 12009
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onAmsItemSelected. ItemType: "

    aput-object v1, v0, v5

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, ", FilePath: "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12012
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 12017
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_2

    .line 12018
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b00a6

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 12036
    :cond_1
    :goto_0
    return-void

    .line 12022
    :cond_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 12024
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 12025
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_1

    .line 12026
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t find ams file: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 12032
    :cond_3
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->ae:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    .line 12034
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 15519
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 15520
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cQ:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 15522
    return-void
.end method

.method public a(Ljava/io/File;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 5181
    const-string v0, "[SendingMedia] Start - Picture"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5182
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5183
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 5184
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5185
    const-string v1, "sendMode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5186
    const-string v1, "randomFName"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5188
    if-eqz p2, :cond_0

    .line 5189
    const/16 v1, 0x15

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 5193
    :goto_0
    return-void

    .line 5191
    :cond_0
    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 11908
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getSelectionStart()I

    move-result v1

    .line 11909
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getSelectionEnd()I

    move-result v2

    .line 11911
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/MyEditText;->getLineHeight()I

    move-result v3

    int-to-float v3, v3

    const v5, 0x3f99999a    # 1.2f

    mul-float/2addr v3, v5

    float-to-int v3, v3

    invoke-static {v0, p1, v3}, Lcom/sec/chaton/multimedia/emoticon/j;->b(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 11913
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/2addr v0, v5

    sub-int v5, v2, v1

    sub-int/2addr v0, v5

    const/16 v5, 0x2710

    if-lt v0, v5, :cond_0

    .line 11914
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0031

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 11921
    :goto_0
    return-void

    .line 11917
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 11918
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 11920
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/2addr v2, v1

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/chat/MyEditText;->setSelection(II)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 10552
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_1

    .line 10553
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 10554
    const-string v0, "message control is null. return"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 10588
    :cond_0
    :goto_0
    return-void

    .line 10561
    :cond_1
    invoke-static {p5}, Lcom/sec/chaton/e/r;->b(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "null"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10563
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 10564
    invoke-static {v1, p5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    .line 10565
    if-eqz v0, :cond_0

    .line 10566
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 10567
    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 10568
    invoke-virtual {v0, v8}, Lcom/sec/chaton/d/o;->a(Z)V

    .line 10572
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->e(Ljava/lang/String;)V

    .line 10576
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/o;->c(J)V

    .line 10580
    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aL:Ljava/lang/Long;

    const-string v4, ""

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    move-object v2, p3

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;J)Z

    goto :goto_0

    .line 10584
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 10585
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 10586
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aL:Ljava/lang/Long;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    move-object v1, p5

    move-object v2, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;JZ)Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 12615
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    if-nez v0, :cond_0

    .line 12625
    :goto_0
    return-void

    .line 12619
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    const v1, 0x7f070573

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 12620
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->d()Lcom/sec/chaton/chat/em;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/em;->c:Lcom/sec/chaton/chat/em;

    if-ne v0, v1, :cond_1

    .line 12621
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 12623
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0
.end method

.method public a(ZLandroid/database/Cursor;)V
    .locals 9

    .prologue
    .line 12889
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    .line 12914
    :cond_0
    :goto_0
    return-void

    .line 12893
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12897
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->al()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12901
    const-string v0, "message_inbox_no"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 12902
    const-string v0, "message_sever_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 12903
    const-string v0, "_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 12905
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v7, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 12906
    const-string v6, "messageId"

    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-nez v7, :cond_2

    :goto_1
    invoke-virtual {v5, v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 12907
    const-string v0, "inboxNo"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 12909
    const-string v0, "isValid"

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 12911
    const-string v0, "chatType"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 12912
    const-string v0, "sessionID"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 12913
    invoke-virtual {p0, v5}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 12906
    goto :goto_1
.end method

.method public a(ZLandroid/database/Cursor;Landroid/view/View;)V
    .locals 13

    .prologue
    .line 12814
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    .line 12841
    :cond_0
    :goto_0
    return-void

    .line 12818
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12823
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->al()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12827
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12832
    const-string v0, "message_inbox_no"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 12833
    const-string v0, "message_sever_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 12834
    const-string v0, "message_content"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 12835
    const-string v0, "message_content_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v10

    .line 12836
    const-string v0, "message_sender"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 12838
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v8, v9, v1}, Lcom/sec/chaton/j/c/a;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12839
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    const/4 v6, 0x0

    iget-object v11, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v1, p3

    move-object v2, p0

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a([Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 5259
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v1

    .line 5261
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_3

    .line 5262
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 5263
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5264
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    aget-object v3, p1, v0

    new-instance v4, Lcom/sec/chaton/a/a/a;

    aget-object v5, p1, v0

    invoke-direct {v4, v5}, Lcom/sec/chaton/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5262
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5270
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5272
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 5273
    const-string v0, "chatType"

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5274
    const-string v3, "receivers"

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 5276
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v3, 0x7f070009

    const-class v4, Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0, v3, v2, v4}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 5368
    :goto_1
    monitor-exit v1

    .line 5369
    return-void

    .line 5280
    :cond_1
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v3, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5281
    const-string v0, "chatType"

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5282
    const-string v3, "receivers"

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 5283
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5285
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 5287
    const-string v3, "lastTempMessage"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5291
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 5292
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/ct;

    invoke-interface {v0}, Lcom/sec/chaton/chat/ct;->c()V

    goto :goto_1

    .line 5368
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 5301
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_8

    .line 5305
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-nez v2, :cond_5

    .line 5308
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    if-nez v2, :cond_4

    .line 5309
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    .line 5312
    :cond_4
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    .line 5313
    :goto_2
    array-length v2, p1

    if-ge v0, v2, :cond_7

    .line 5314
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5315
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    aget-object v3, p1, v0

    new-instance v4, Lcom/sec/chaton/a/a/a;

    aget-object v5, p1, v0

    invoke-direct {v4, v5}, Lcom/sec/chaton/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5313
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5318
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 5319
    :goto_3
    array-length v3, p1

    if-ge v0, v3, :cond_6

    .line 5320
    const-string v3, "%d,%s,%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    invoke-virtual {v6}, Lcom/sec/chaton/e/aj;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aget-object v6, p1, v0

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    aget-object v7, p1, v0

    invoke-static {v6, v7}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5323
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aget-object v5, p1, v0

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 5325
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5326
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    aget-object v4, p1, v0

    new-instance v5, Lcom/sec/chaton/a/a/a;

    aget-object v6, p1, v0

    invoke-direct {v5, v6}, Lcom/sec/chaton/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5319
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 5328
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "chaton_id"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v2, v5}, Lcom/sec/chaton/e/a/q;->a(Lcom/sec/chaton/e/a/u;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5334
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 5341
    :cond_8
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 5342
    :goto_4
    array-length v2, p1

    if-ge v0, v2, :cond_9

    .line 5343
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5344
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    aget-object v3, p1, v0

    new-instance v4, Lcom/sec/chaton/a/a/a;

    aget-object v5, p1, v0

    invoke-direct {v4, v5}, Lcom/sec/chaton/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5342
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 5348
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 5351
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_b

    .line 5352
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->I()Z

    .line 5355
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v2, :cond_d

    .line 5356
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_c

    .line 5357
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    sget-object v3, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, p1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)J

    .line 5364
    :cond_c
    :goto_5
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    goto/16 :goto_1

    .line 5359
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v0, v2, :cond_c

    .line 5360
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_c

    .line 5361
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, p1, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Z)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 11373
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    if-eqz v2, :cond_0

    .line 11374
    iput-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    .line 11375
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aJ()V

    .line 11379
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v2}, Lcom/sec/widget/HeightChangedListView;->getChoiceMode()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 11380
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->q()V

    .line 11405
    :goto_0
    return v0

    .line 11382
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->at()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 11383
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 11394
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v1, v0}, Lcom/sec/widget/HeightChangedListView;->setFocusable(Z)V

    .line 11397
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->B()V

    goto :goto_0

    .line 11401
    :cond_2
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->b()V

    .line 11402
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->c()V

    .line 11403
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->d()V

    move v0, v1

    .line 11405
    goto :goto_0
.end method

.method public a(I)Z
    .locals 5

    .prologue
    const v1, 0x7f0b003d

    const/16 v3, 0x12

    const/4 v4, 0x0

    .line 10995
    packed-switch p1, :pswitch_data_0

    .line 11096
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 10998
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->W()V

    goto :goto_0

    .line 11001
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->Z()V

    goto :goto_0

    .line 11005
    :pswitch_3
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 11006
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 11008
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11009
    const-string v1, "inbox_NO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11010
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 11014
    :pswitch_4
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11015
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 11017
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11018
    const-string v1, "inbox_NO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11019
    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 11023
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11024
    const-string v1, "sendbutton"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 11025
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 11028
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->Y()V

    goto :goto_0

    .line 11032
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->X()V

    goto :goto_0

    .line 11036
    :pswitch_8
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 11037
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 11040
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/audio/VoiceListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11041
    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 11046
    :pswitch_9
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/amschaton/AMSFileListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11047
    const/16 v1, 0xf

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 11051
    :pswitch_a
    invoke-static {}, Lcom/sec/chaton/c/a;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 11052
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 11053
    const-string v1, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 11054
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.myfiles"

    const-string v3, "com.sec.android.app.myfiles.fileselector.SingleSelectorActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 11055
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 11056
    const-string v1, "CONTENT_TYPE"

    invoke-static {}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11057
    const-string v1, "CONTENT_EXTENSION"

    const-string v2, "scc"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11059
    const/16 v1, 0x12

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 11060
    :catch_0
    move-exception v0

    .line 11061
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 11062
    const-string v0, "ActivityNotFound(My files)"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 11064
    :cond_4
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 11065
    const-string v1, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 11066
    const-string v1, "CONTENT_TYPE"

    invoke-static {}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11067
    const-string v1, "CONTENT_EXTENSION"

    const-string v2, "scc"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11069
    const/16 v1, 0x12

    :try_start_1
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 11070
    :catch_1
    move-exception v0

    .line 11071
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 11072
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 11073
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 11080
    :cond_5
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11081
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 11086
    :pswitch_b
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SNOTE_PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 11087
    const-string v1, "SelectMode"

    const-string v2, "single"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11088
    const-string v1, "ReturnType"

    const-string v2, "Imageonly"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11089
    const/16 v1, 0x13

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 10995
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 13288
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 13289
    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 13291
    const/4 v0, 0x0

    .line 13292
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 13293
    const/4 v0, 0x1

    .line 13296
    :cond_0
    return v0
.end method

.method public b()V
    .locals 5

    .prologue
    .line 2814
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->requestFocus()Z

    .line 2817
    const-wide/16 v0, 0xc8

    .line 2819
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v2}, Lcom/sec/chaton/chat/MyEditText;->isShown()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2820
    const-wide/16 v0, 0x3e8

    .line 2830
    :cond_0
    new-instance v2, Ljava/util/Timer;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/Timer;-><init>(Z)V

    .line 2831
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 2832
    new-instance v4, Lcom/sec/chaton/chat/m;

    invoke-direct {v4, p0, v3}, Lcom/sec/chaton/chat/m;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;)V

    invoke-virtual {v2, v4, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 2850
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 11926
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 11928
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->B()V

    .line 11930
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    if-eqz v0, :cond_0

    .line 11931
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v1}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/widget/HeightChangedListView;->setSelectionFromTop(II)V

    .line 11932
    iput-boolean v5, p0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    .line 11933
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    .line 11936
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->ba:Ljava/lang/String;

    .line 11939
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/downloads/u;->i(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;

    move-result-object v0

    .line 11940
    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    if-ne v0, v1, :cond_2

    .line 11941
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 11954
    :goto_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    .line 11956
    if-eqz v1, :cond_1

    .line 11957
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 11958
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 11960
    sget-object v2, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    if-eq v0, v2, :cond_5

    .line 11961
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/chaton/chat/bj;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/bj;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11975
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11976
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 11977
    iput-boolean v5, p0, Lcom/sec/chaton/chat/ChatFragment;->aa:Z

    .line 11980
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aI()V

    .line 11981
    sget-object v0, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ad:Lcom/sec/chaton/e/w;

    .line 11983
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 11990
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->b()V

    .line 11992
    return-void

    .line 11943
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 11944
    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->c:Lcom/sec/chaton/settings/downloads/z;

    if-ne v0, v1, :cond_3

    .line 11945
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    const v2, 0x7f02014d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 11946
    :cond_3
    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->e:Lcom/sec/chaton/settings/downloads/z;

    if-ne v0, v1, :cond_4

    .line 11947
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    const v2, 0x7f02014e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 11949
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    const v2, 0x7f02014f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 11972
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public b(ZLandroid/database/Cursor;)V
    .locals 9

    .prologue
    .line 12921
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    .line 12946
    :cond_0
    :goto_0
    return-void

    .line 12925
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12929
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->al()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12933
    const-string v0, "message_inbox_no"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 12934
    const-string v0, "message_sever_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 12935
    const-string v0, "_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 12937
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v7, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 12938
    const-string v6, "messageId"

    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-nez v7, :cond_2

    :goto_1
    invoke-virtual {v5, v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 12939
    const-string v0, "inboxNo"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 12941
    const-string v0, "isValid"

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 12943
    const-string v0, "chatType"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 12944
    const-string v0, "sessionID"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 12945
    invoke-virtual {p0, v5}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 12938
    goto :goto_1
.end method

.method public b(ZLandroid/database/Cursor;Landroid/view/View;)V
    .locals 18

    .prologue
    .line 12953
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/chat/ChatFragment;->aj()Z

    move-result v1

    if-nez v1, :cond_1

    .line 13006
    :cond_0
    :goto_0
    return-void

    .line 12957
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/chat/ChatFragment;->ak()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12961
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/chat/ChatFragment;->al()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12965
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/chat/ChatFragment;->am()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12969
    const-string v1, "message_inbox_no"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 12970
    const-string v1, "message_sever_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 12971
    const-string v1, "message_sender"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 12973
    const-string v1, "message_content"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 12974
    const-string v1, "message_download_uri"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 12975
    const-string v1, "message_content_type"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v11

    .line 12977
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 12978
    :cond_2
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v1

    invoke-virtual {v1, v9, v10}, Lcom/sec/chaton/j/c/a;->a(J)Z

    move-result v1

    if-nez v1, :cond_3

    .line 12981
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/chat/ChatFragment;->an()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12986
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    invoke-virtual/range {v1 .. v13}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 12988
    if-eqz p1, :cond_3

    .line 12989
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10}, Lcom/sec/chaton/chat/ChatFragment;->b(J)Lcom/sec/chaton/widget/c;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    .line 12990
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 12992
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/c;->show()V

    .line 12994
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v1

    invoke-virtual {v1, v9, v10}, Lcom/sec/chaton/j/c/a;->b(J)Lcom/sec/chaton/j/c/c;

    move-result-object v1

    .line 12996
    if-eqz v1, :cond_3

    .line 12997
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/chat/ChatFragment;->bV:Lcom/sec/chaton/widget/c;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/c/c;->a(Lcom/sec/chaton/widget/c;)V

    .line 13003
    :cond_3
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v7

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    move-object/from16 v16, v0

    move-object/from16 v8, v17

    move-object/from16 v11, p3

    move/from16 v12, p1

    move-object/from16 v13, p2

    invoke-virtual/range {v7 .. v16}, Lcom/sec/chaton/multimedia/audio/b;->a(Ljava/lang/String;JLandroid/view/View;ZLandroid/database/Cursor;ILandroid/os/Handler;Lcom/sec/chaton/e/r;)V

    .line 13005
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/multimedia/audio/b;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 4965
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 4966
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 4967
    const v1, 0x7f0b025f

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 4968
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/chat/am;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/am;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4973
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 4974
    return-void
.end method

.method public c(ZLandroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v0, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x0

    .line 13111
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aj()Z

    move-result v1

    if-nez v1, :cond_1

    .line 13163
    :cond_0
    :goto_0
    return-void

    .line 13115
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ak()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13119
    const-string v1, "message_content"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 13121
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 13122
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b00de

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 13126
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 13127
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 13134
    const/4 v1, 0x0

    .line 13136
    array-length v3, v2

    if-le v3, v6, :cond_0

    .line 13137
    array-length v3, v2

    if-le v3, v0, :cond_5

    .line 13138
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 13139
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_3

    .line 13140
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 13142
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 13145
    :goto_2
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 13146
    const-string v3, "http://maps.google.com/maps?q=loc:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13147
    aget-object v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13149
    if-eqz v0, :cond_4

    .line 13150
    const-string v2, "[\\(\\)]"

    const-string v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 13151
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13155
    :cond_4
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 13156
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 13157
    :catch_0
    move-exception v0

    .line 13158
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b0405

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public c(ZLandroid/database/Cursor;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 13060
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ai()Z

    move-result v0

    if-nez v0, :cond_0

    .line 13071
    :goto_0
    return-void

    .line 13064
    :cond_0
    const-string v0, "message_download_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 13066
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13067
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/database/Cursor;Landroid/view/View;)V

    goto :goto_0

    .line 13069
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->m(Ljava/lang/String;)V

    goto :goto_0
.end method

.method c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 13300
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 13302
    const/4 v0, 0x0

    .line 13304
    const/16 v2, 0x40

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 13305
    if-eqz v1, :cond_0

    .line 13306
    const/4 v0, 0x1

    .line 13312
    :cond_0
    :goto_0
    return v0

    .line 13308
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 4977
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->G:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->G:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4999
    :goto_0
    return-void

    .line 4981
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 4982
    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 4983
    const v1, 0x7f0b005d

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 4984
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/chat/an;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/an;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4997
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->G:Lcom/sec/common/a/d;

    .line 4998
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->G:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 13628
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/text/DetailTextView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 13629
    const-string v1, "fulltext"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 13630
    const-string v1, "View All"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 13631
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 13632
    return-void
.end method

.method public d(ZLandroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 13168
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 13169
    const-string v0, "onAppLinkClickListener()"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13172
    :cond_0
    const-string v0, "message_content"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 13173
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13174
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 13175
    const-string v0, " - error #0. message contents is empty error"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 13283
    :cond_1
    :goto_0
    return-void

    .line 13182
    :cond_2
    :try_start_0
    invoke-static {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->parse(Ljava/lang/String;)Lcom/sec/chaton/io/entry/MessageType4Entry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 13192
    if-eqz v0, :cond_3

    instance-of v1, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    if-nez v1, :cond_4

    .line 13193
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 13194
    const-string v0, " - error #2. type error (not a full message)"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13183
    :catch_0
    move-exception v0

    .line 13184
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 13185
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 13186
    const-string v0, " - error #1. data parsing error"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13199
    :cond_4
    check-cast v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    .line 13200
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_5

    .line 13201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applink info : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13204
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getAppName()Ljava/lang/String;

    move-result-object v1

    .line 13206
    const-string v2, "android"

    const-string v3, "phone"

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getParam(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;

    move-result-object v0

    .line 13207
    if-nez v0, :cond_6

    .line 13208
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 13209
    const-string v0, " - error #3. there\'s not link action for android-phone"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13214
    :cond_6
    iget-object v2, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/ChatFragment;->c(Ljava/lang/String;)Z

    move-result v2

    .line 13215
    if-eqz v2, :cond_a

    .line 13216
    iget-object v1, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->executeUri:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 13217
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 13218
    const-string v0, " - error #4. there\'s not executeUri"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13225
    :cond_7
    :try_start_1
    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->executeUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 13232
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 13233
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;)Z

    move-result v0

    .line 13234
    if-nez v0, :cond_8

    .line 13235
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 13236
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;)Z

    move-result v0

    .line 13239
    :cond_8
    if-eqz v0, :cond_9

    .line 13240
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 13226
    :catch_1
    move-exception v0

    .line 13227
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 13228
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13242
    :cond_9
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 13243
    const-string v0, " - error #5. 3rd party does not support properly applink"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13247
    :cond_a
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_b

    .line 13248
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not installed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13250
    :cond_b
    iget-object v2, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->installUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 13251
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 13252
    const-string v0, " - error #6. there\'s not install info"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13258
    :cond_c
    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->installUrl:Ljava/lang/String;

    .line 13259
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 13260
    const v3, 0x7f0b01ac

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 13261
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b024e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 13262
    invoke-virtual {v2, v6}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v3, 0x7f0b0042

    new-instance v4, Lcom/sec/chaton/chat/bq;

    invoke-direct {v4, p0, v0}, Lcom/sec/chaton/chat/bq;-><init>(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/bp;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/bp;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 13281
    invoke-virtual {v2}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method

.method public d(ZLandroid/database/Cursor;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 13076
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ai()Z

    move-result v0

    if-nez v0, :cond_0

    .line 13087
    :goto_0
    return-void

    .line 13080
    :cond_0
    const-string v0, "message_download_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 13082
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13083
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/database/Cursor;Landroid/view/View;)V

    goto :goto_0

    .line 13085
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 5002
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 5003
    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 5008
    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 5010
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/chat/aq;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/aq;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/ap;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/ap;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 5021
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 5022
    return-void
.end method

.method public e(ZLandroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 15740
    const-string v0, "message_content"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 15743
    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v1

    .line 15744
    if-nez v1, :cond_0

    .line 15745
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLiveShareClickListener(), parsing error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 15758
    :goto_0
    return-void

    .line 15749
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLiveShareClickListener(), request entry : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 15750
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v3, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15751
    const-string v2, "specialbuddy"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 15752
    const-string v2, "receivers"

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->id:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 15753
    const-string v2, "key_intent_ctid"

    iget-object v3, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->hash:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 15754
    const-string v2, "key_web_url"

    iget-object v3, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->url:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 15755
    const-string v2, "key_buddy_name"

    iget-object v1, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 15757
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public e(ZLandroid/database/Cursor;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 13094
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ai()Z

    move-result v0

    if-nez v0, :cond_0

    .line 13105
    :goto_0
    return-void

    .line 13098
    :cond_0
    const-string v0, "message_download_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 13100
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13101
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/database/Cursor;Landroid/view/View;)V

    goto :goto_0

    .line 13103
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->o(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 14658
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14667
    :cond_0
    :goto_0
    return v0

    .line 14662
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14665
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5028
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_2

    .line 5029
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_0

    .line 5030
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->I()Z

    .line 5032
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 5034
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_1

    .line 5035
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)J

    .line 5063
    :cond_1
    :goto_0
    return-void

    .line 5038
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;)V

    .line 5039
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x1

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inbox_no IN (\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 5040
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "message_inbox_no=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 5041
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x3

    sget-object v3, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "participants_inbox_no=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 5043
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/a;->a(Ljava/lang/String;)Z

    .line 5044
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/g;->a(Ljava/lang/String;)Z

    .line 5045
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->T()V

    goto/16 :goto_0
.end method

.method public f(ZLandroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 15762
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 15814
    :goto_0
    return-void

    .line 15766
    :cond_0
    const-string v0, "message_content"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 15769
    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->c(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v1

    .line 15770
    if-nez v1, :cond_1

    .line 15771
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLiveRecommendClickListener(), parsing error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 15775
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 15776
    iget-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->id:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/af;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 15777
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLIveRecommendClickListener(), request entry : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 15778
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLIveRecommendClickListener(), isInServiceLiveBuddies: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 15779
    if-nez v0, :cond_2

    .line 15780
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b03ca

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 15784
    :cond_2
    const-string v0, "0999"

    iget-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 15785
    const-string v0, "onLiveRecommendClickListener(), id error : not a live buddy "

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15802
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 15803
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v3, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15808
    :goto_1
    const-string v2, "specialuserid"

    iget-object v3, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 15809
    iget-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 15810
    const-string v2, "speicalusername"

    iget-object v1, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 15813
    :cond_4
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 15806
    :cond_5
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v3, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1
.end method

.method public f(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 15525
    .line 15527
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 15528
    const-string v2, "filename="

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 15529
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cT:Ljava/lang/String;

    .line 15534
    new-instance v2, Lcom/sec/chaton/chat/cg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/cg;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 15541
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->L:Ljava/io/File;

    invoke-virtual {v3, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 15542
    if-eqz v2, :cond_0

    array-length v3, v2

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 15557
    :cond_1
    :goto_0
    return v0

    .line 15546
    :cond_2
    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cU:Ljava/lang/String;

    .line 15547
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cU:Ljava/lang/String;

    const-string v3, "_chat_profile.png_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 15548
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cS:Ljava/lang/String;

    .line 15550
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cT:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cS:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 15553
    goto :goto_0

    .line 15554
    :catch_0
    move-exception v1

    .line 15556
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 6872
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/e/z;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->M:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6873
    return-void
.end method

.method public h()V
    .locals 11

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x1

    const/high16 v10, 0x41f00000    # 30.0f

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 8638
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bx:Z

    if-nez v1, :cond_1

    .line 8889
    :cond_0
    :goto_0
    return-void

    .line 8642
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 8643
    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_8

    .line 8644
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8645
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bt:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 8646
    const-string v2, "Original name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bt:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n----------------\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8648
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 8649
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 8650
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x2710

    if-le v1, v2, :cond_3

    .line 8651
    const/16 v1, 0x270f

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 8656
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bA:Lcom/sec/chaton/w;

    sget-object v1, Lcom/sec/chaton/w;->b:Lcom/sec/chaton/w;

    if-ne v0, v1, :cond_7

    .line 8657
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 8659
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_5

    .line 8660
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 8661
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 8666
    :cond_5
    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v7

    .line 8668
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v2}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 8669
    monitor-exit v7

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 8671
    :cond_6
    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v7

    .line 8673
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 8674
    monitor-exit v7

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 8677
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v10}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, v5, v1}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 8678
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 8679
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setSelection(I)V

    goto/16 :goto_0

    .line 8683
    :cond_8
    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_b

    .line 8688
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "null"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 8689
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_a

    .line 8690
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    :cond_a
    move v4, v5

    .line 8695
    :goto_1
    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v8

    .line 8696
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    .line 8705
    monitor-exit v8

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 8744
    :cond_b
    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_e

    .line 8745
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 8746
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_d

    .line 8747
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 8750
    :cond_d
    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v6

    .line 8751
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 8752
    monitor-exit v6

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    throw v0

    .line 8753
    :cond_e
    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_11

    .line 8754
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 8755
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_10

    .line 8756
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 8759
    :cond_10
    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v6

    .line 8760
    :try_start_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 8761
    monitor-exit v6

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    throw v0

    .line 8762
    :cond_11
    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_14

    .line 8763
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 8764
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_13

    .line 8765
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 8768
    :cond_13
    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v6

    .line 8769
    :try_start_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/o;->c(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 8770
    monitor-exit v6

    goto/16 :goto_0

    :catchall_5
    move-exception v0

    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    throw v0

    .line 8771
    :cond_14
    iget v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    if-eq v1, v2, :cond_0

    .line 8776
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-nez v1, :cond_15

    .line 8777
    const-string v0, "[sendForward()] No sdcard"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8780
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward download_uri:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8781
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward content:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8783
    const-string v1, ""

    .line 8785
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    .line 8789
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "null"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    :cond_16
    move v2, v5

    .line 8793
    :goto_2
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bu:Z

    if-eqz v1, :cond_1a

    .line 8795
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    if-eqz v1, :cond_25

    .line 8796
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    const-string v7, "\n"

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move-object v7, v1

    .line 8799
    :goto_3
    if-eqz v7, :cond_0

    array-length v1, v7

    const/4 v8, 0x4

    if-le v1, v8, :cond_0

    .line 8802
    const-string v1, "file://"

    invoke-virtual {v4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 8803
    new-instance v1, Ljava/io/File;

    const/4 v8, 0x7

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8806
    :goto_4
    if-eqz v1, :cond_0

    .line 8807
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 8809
    array-length v8, v7

    if-le v8, v6, :cond_23

    const-string v8, "mixed"

    aget-object v9, v7, v0

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_23

    .line 8810
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    move v3, v6

    .line 8811
    :goto_5
    array-length v6, v7

    if-ge v3, v6, :cond_17

    .line 8812
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v9, v7, v3

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "\n"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 8811
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 8814
    :cond_17
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    .line 8818
    :goto_6
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v3, :cond_18

    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v0, v3, :cond_18

    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v3, :cond_19

    .line 8820
    :cond_18
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 8823
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8824
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v10}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, v7, v1}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 8825
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 8828
    :try_start_6
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/MyEditText;->setSelection(I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    .line 8829
    :catch_0
    move-exception v0

    .line 8830
    const-string v0, "Exception occurred while setting selection on edit text"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8835
    :cond_19
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v6

    move-object v0, p0

    move-object v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    goto/16 :goto_0

    .line 8845
    :cond_1a
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "buddy_dragdata"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "key_clipdata"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 8846
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "buddy_dragdata"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8847
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v1, "key_clipdata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ClipData;

    .line 8848
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->dd:Z

    if-eqz v1, :cond_1b

    .line 8849
    sget-object v1, Lcom/sec/chaton/chat/cr;->b:Lcom/sec/chaton/chat/cr;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/ClipData;Lcom/sec/chaton/chat/cr;)Z

    goto/16 :goto_0

    .line 8852
    :cond_1b
    sget-object v1, Lcom/sec/chaton/chat/cr;->a:Lcom/sec/chaton/chat/cr;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/ClipData;Lcom/sec/chaton/chat/cr;)Z

    goto/16 :goto_0

    .line 8861
    :cond_1c
    const-string v1, "content:/"

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 8862
    invoke-static {v4}, Lcom/sec/chaton/util/r;->c(Ljava/lang/String;)Lcom/sec/chaton/util/u;

    move-result-object v1

    .line 8863
    if-eqz v1, :cond_22

    .line 8864
    iget-object v1, v1, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    .line 8871
    :goto_7
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bA:Lcom/sec/chaton/w;

    sget-object v4, Lcom/sec/chaton/w;->b:Lcom/sec/chaton/w;

    if-eq v2, v4, :cond_1d

    iget v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v2}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v2

    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v2, v4, :cond_1d

    iget v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v2}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v2

    sget-object v4, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v2, v4, :cond_20

    .line 8873
    :cond_1d
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 8874
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    .line 8875
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x2710

    if-le v4, v5, :cond_1f

    .line 8876
    const/16 v4, 0x270f

    invoke-virtual {v2, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 8878
    :goto_8
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v10}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v2, v0, v4}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 8879
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 8884
    :goto_9
    iget v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8866
    :cond_1e
    const-string v1, "file:/"

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 8867
    new-instance v1, Ljava/io/File;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_7

    :cond_1f
    move-object v0, v2

    goto :goto_8

    :cond_20
    move-object v2, v3

    goto :goto_9

    :cond_21
    move-object v1, v3

    goto :goto_7

    :cond_22
    move-object v1, v3

    goto :goto_7

    :cond_23
    move-object v7, v3

    goto/16 :goto_6

    :cond_24
    move-object v1, v3

    goto/16 :goto_4

    :cond_25
    move-object v7, v3

    goto/16 :goto_3

    :cond_26
    move v2, v0

    goto/16 :goto_2

    :cond_27
    move v4, v0

    goto/16 :goto_1
.end method

.method public i()V
    .locals 5

    .prologue
    .line 10909
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    if-eqz v0, :cond_0

    .line 10917
    :goto_0
    return-void

    .line 10913
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10914
    const-string v1, "ME_DIALOG_NAME"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Push Name"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10915
    const-string v1, "ME_DIALOG_STATUSMSG"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "status_message"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10916
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public j()V
    .locals 3

    .prologue
    .line 11271
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203e9

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 11272
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 11273
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bF:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 11274
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 11656
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    if-eqz v0, :cond_0

    .line 11657
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    .line 11659
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    .line 11663
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cg:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 11665
    return-void
.end method

.method public l()V
    .locals 3

    .prologue
    .line 12054
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 12056
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/amschaton/AMSFileListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 12057
    const-string v1, "AMS_START_STATE"

    const/16 v2, 0x3ec

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 12058
    const/16 v1, 0xf

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 12059
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 12089
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    const-string v1, "skin_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 12090
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    .line 12102
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    const-string v1, "bubble_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 12103
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    .line 12115
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    const-string v1, "bubble_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 12116
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    .line 12127
    :cond_2
    :goto_2
    return-void

    .line 12091
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    const-string v1, "skin_bg_02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 12092
    const-string v0, "-2"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    goto :goto_0

    .line 12093
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    const-string v1, "skin_bg_03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 12094
    const-string v0, "-3"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    goto :goto_0

    .line 12095
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    const-string v1, "skin_bg_04"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 12096
    const-string v0, "-4"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    goto :goto_0

    .line 12098
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    const-string v1, "skin_bg_05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12099
    const-string v0, "-5"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bj:Ljava/lang/String;

    goto :goto_0

    .line 12104
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    const-string v1, "bubble_02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 12105
    const-string v0, "-2"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    goto :goto_1

    .line 12106
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    const-string v1, "bubble_03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 12107
    const-string v0, "-3"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    goto :goto_1

    .line 12108
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    const-string v1, "bubble_04"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 12109
    const-string v0, "-4"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    goto :goto_1

    .line 12111
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    const-string v1, "bubble_05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12112
    const-string v0, "-5"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bm:Ljava/lang/String;

    goto/16 :goto_1

    .line 12117
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    const-string v1, "bubble_02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 12118
    const-string v0, "-2"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    goto/16 :goto_2

    .line 12119
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    const-string v1, "bubble_03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 12120
    const-string v0, "-3"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    goto/16 :goto_2

    .line 12121
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    const-string v1, "bubble_04"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 12122
    const-string v0, "-4"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    goto/16 :goto_2

    .line 12124
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    const-string v1, "bubble_05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 12125
    const-string v0, "-5"

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bn:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public n()V
    .locals 9

    .prologue
    const/4 v1, 0x3

    const/4 v8, 0x0

    .line 12338
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aY:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    if-eqz v0, :cond_0

    .line 12339
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    const-string v4, "message_sender=? AND message_inbox_no=? AND message_content_type=?"

    new-array v5, v1, [Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v6, v5, v8

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v7}, Lcom/sec/chaton/e/w;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 12342
    :cond_0
    iput-boolean v8, p0, Lcom/sec/chaton/chat/ChatFragment;->aY:Z

    .line 12343
    return-void
.end method

.method public o()V
    .locals 0

    .prologue
    .line 12357
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 2228
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2230
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    .line 2231
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_19

    .line 2232
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2234
    const-string v0, "InBoxNO : "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2235
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "inboxNO"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2236
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "inboxNO"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    .line 2237
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2249
    :goto_0
    const-string v0, "\t"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2251
    const-string v0, "ChatType : "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2253
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "chatType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2254
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "chatType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    .line 2255
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    if-eqz v0, :cond_0

    .line 2256
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-virtual {v0}, Lcom/sec/chaton/e/r;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2262
    :cond_0
    :goto_1
    const-string v0, "\t"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2263
    const-string v0, "Participants : "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2265
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "receivers"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 2269
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "receivers"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2270
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v5, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v5, :cond_1

    .line 2271
    aget-object v0, v2, v1

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    .line 2273
    :cond_1
    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v5

    .line 2274
    :try_start_0
    array-length v6, v2

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_5

    aget-object v7, v2, v0

    .line 2275
    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2276
    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    new-instance v9, Lcom/sec/chaton/a/a/a;

    invoke-direct {v9, v7}, Lcom/sec/chaton/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2274
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2241
    :cond_2
    if-eqz p1, :cond_3

    const-string v0, "inbox_no"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2242
    const-string v0, "inbox_no"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    .line 2243
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " from SavedInstance"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 2247
    :cond_3
    const-string v0, "Not Contained"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 2259
    :cond_4
    const-string v0, "Not Contained"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 2279
    :cond_5
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2303
    :cond_6
    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2305
    const-string v0, "KEY_GROUP_NAME : "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2306
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "groupnmae"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2307
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "groupnmae"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->A:Ljava/lang/String;

    .line 2308
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mGroupName:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 2310
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aO:Ljava/lang/String;

    .line 2311
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2313
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    .line 2314
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    .line 2324
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "content_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2325
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "content_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    .line 2326
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "content_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatView content_type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bq:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 2328
    iput-boolean v10, p0, Lcom/sec/chaton/chat/ChatFragment;->bx:Z

    .line 2330
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "download_uri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2331
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "download_uri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    .line 2332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatView download_uri:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->br:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 2334
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "sub_content"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2335
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "sub_content"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    .line 2336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatView sub_content:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bs:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 2338
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "forward_sender_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2339
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "forward_sender_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bt:Ljava/lang/String;

    .line 2340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatView forward_sender_name:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bt:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 2343
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "is_forward_mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2344
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "is_forward_mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bu:Z

    .line 2345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChatView forward_mode:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bu:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 2348
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2349
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dj:Z

    .line 2351
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dj:Z

    if-eqz v0, :cond_d

    .line 2352
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    if-eqz v0, :cond_d

    .line 2353
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->b()V

    .line 2357
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "misForceShowKeyboard:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->dj:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 2359
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2360
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bv:Ljava/lang/String;

    .line 2361
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bz:Z

    .line 2362
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bz:Z

    if-eqz v0, :cond_f

    .line 2366
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bv:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    .line 2367
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    .line 2368
    iput-boolean v10, p0, Lcom/sec/chaton/chat/ChatFragment;->bw:Z

    .line 2372
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2373
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->by:Z

    .line 2389
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "inboxValid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2390
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "inboxValid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    .line 2391
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-nez v0, :cond_11

    .line 2393
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aq:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2397
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "trunkUnreadCount"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2398
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "trunkUnreadCount"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aW:I

    .line 2402
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "lastTempMessage"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2403
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "lastTempMessage"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    .line 2406
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "showPhoneNumber"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2407
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "showPhoneNumber"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    .line 2410
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "extraInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2411
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "extraInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aG:Ljava/lang/String;

    .line 2414
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "msisdns"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2415
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "msisdns"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aH:Ljava/lang/String;

    .line 2417
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 2418
    iput v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    .line 2438
    :cond_15
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v1, "groupId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2439
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v1, "groupId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    .line 2444
    :cond_16
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v1, "is_attach_on_the_edittext"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2445
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v1, "is_attach_on_the_edittext"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dd:Z

    .line 2463
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v1, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2464
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v1, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/w;

    check-cast v0, Lcom/sec/chaton/w;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bA:Lcom/sec/chaton/w;

    .line 2469
    :cond_18
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v1, "msgbox"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2470
    iput-boolean v10, p0, Lcom/sec/chaton/chat/ChatFragment;->cw:Z

    .line 2471
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->b(Landroid/os/Bundle;)V

    .line 2478
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1a

    if-eqz p1, :cond_1a

    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 2479
    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 2483
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 2485
    invoke-static {p0, v10}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 2487
    new-instance v0, Lcom/sec/chaton/chat/ei;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aD()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aC()F

    move-result v6

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatFragment;->bU:Lcom/sec/common/f/c;

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->ck:Lorg/a/a/a/a/j;

    iget-object v9, p0, Lcom/sec/chaton/chat/ChatFragment;->dp:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/chat/ei;-><init>(Landroid/content/Context;Landroid/widget/ListView;Landroid/database/Cursor;Lcom/sec/chaton/e/r;Landroid/graphics/Typeface;FLcom/sec/common/f/c;Lorg/a/a/a/a/j;Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    .line 2488
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2489
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/fk;)V

    .line 2492
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    if-eqz v0, :cond_1b

    .line 2493
    const-string v0, "file"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/u;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2494
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->U()V

    .line 2495
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    sget-object v1, Lcom/sec/chaton/u;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2502
    :cond_1b
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    .line 2504
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v1}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 2505
    new-instance v0, Lcom/sec/chaton/chat/l;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/l;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cc:Landroid/database/DataSetObserver;

    .line 2514
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cc:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/ei;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2516
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->O()V

    .line 2519
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 2520
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Ljava/lang/String;)V

    .line 2523
    :cond_1c
    return-void

    .line 2279
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2282
    :cond_1d
    const-string v0, "Not Contained"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2284
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v2, "groupId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2285
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v2, :cond_23

    .line 2286
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bI:Landroid/os/Bundle;

    const-string v5, "groupId"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v2, v0

    .line 2288
    :goto_6
    if-eqz v2, :cond_1f

    .line 2290
    iget-object v5, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v5

    .line 2291
    :try_start_3
    array-length v6, v2

    move v0, v1

    :goto_7
    if-ge v0, v6, :cond_1e

    aget-object v7, v2, v0

    .line 2292
    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2293
    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    new-instance v9, Lcom/sec/chaton/a/a/a;

    invoke-direct {v9, v7}, Lcom/sec/chaton/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2294
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2291
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 2296
    :cond_1e
    monitor-exit v5

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 2298
    :cond_1f
    const-string v0, "onActivityCreated() receiverList is NULL. ERROR!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2400
    :cond_20
    iput v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aW:I

    goto/16 :goto_4

    .line 2420
    :cond_21
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2421
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aH:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->s(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2422
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_22

    .line 2423
    iput v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    goto/16 :goto_5

    .line 2425
    :cond_22
    iput v10, p0, Lcom/sec/chaton/chat/ChatFragment;->aF:I

    goto/16 :goto_5

    :cond_23
    move-object v2, v3

    goto :goto_6
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 5373
    if-ne p2, v0, :cond_21

    .line 5375
    sparse-switch p1, :sswitch_data_0

    .line 5822
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 5378
    :sswitch_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 5379
    const-string v0, "[SendingMedia] Start - Crop picture"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5382
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_2

    .line 5383
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 5387
    :cond_2
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5388
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/net/Uri;Z)V

    goto :goto_0

    .line 5394
    :sswitch_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 5395
    const-string v0, "[SendingMedia] Start - Multiple Pictures"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5398
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_4

    .line 5399
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 5403
    :cond_4
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->j(Landroid/content/Intent;)V

    goto :goto_0

    .line 5406
    :sswitch_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 5407
    const-string v0, "[SendingMedia] Start - Capture Picture"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5411
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    if-eqz v0, :cond_6

    .line 5412
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1, v5, v5}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 5416
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_7

    .line 5417
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 5421
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 5422
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Ljava/io/File;Z)V

    goto :goto_0

    .line 5429
    :sswitch_4
    const-string v0, "receivers"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 5430
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->a([Ljava/lang/String;)V

    goto :goto_0

    .line 5536
    :sswitch_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 5537
    const-string v0, "[SendingMedia] Start - Video"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5540
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_9

    .line 5541
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5545
    :cond_9
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->i(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5549
    :sswitch_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_a

    .line 5550
    const-string v0, "[SendingMedia] Start - Record Video"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5553
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_b

    .line 5554
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5558
    :cond_b
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->i(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5561
    :sswitch_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_c

    .line 5562
    const-string v0, "[SendingMedia] Start - Calendar"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5565
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_d

    .line 5566
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5570
    :cond_d
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->h(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5573
    :sswitch_8
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_e

    .line 5574
    const-string v0, "[SendingMedia] Start - Contact"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5576
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_f

    .line 5577
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5580
    :cond_f
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->g(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5583
    :sswitch_9
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_10

    .line 5584
    const-string v0, "[SendingMedia] Start - Geo tag"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5586
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_11

    .line 5587
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5590
    :cond_11
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->f(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5662
    :sswitch_a
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_12

    .line 5663
    const-string v0, "[SendingMedia] Start - Animessage"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5666
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_13

    .line 5667
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5670
    :cond_13
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->b(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5674
    :sswitch_b
    const-string v0, "isClosing"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 5675
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->f()V

    goto/16 :goto_0

    .line 5677
    :cond_14
    const-string v0, "receivers"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 5678
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->j:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bv:Ljava/lang/String;

    .line 5679
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->l:Ljava/lang/String;

    invoke-virtual {p3, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bz:Z

    .line 5680
    const-string v1, ""

    .line 5681
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTmpTitle:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 5682
    if-nez v0, :cond_16

    .line 5683
    const-string v1, "null arrayResult"

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 5687
    :goto_1
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bz:Z

    if-eqz v1, :cond_15

    .line 5688
    const-string v1, "mTmpTitle not null!!!"

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 5689
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bv:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    .line 5690
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    .line 5691
    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aO:Ljava/lang/String;

    .line 5692
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5694
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aT:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aN:Ljava/lang/String;

    .line 5696
    :cond_15
    if-eqz v0, :cond_0

    .line 5697
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->a([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5685
    :cond_16
    const-string v1, "Not null arrayResult"

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 5712
    :sswitch_c
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_17

    .line 5713
    const-string v0, "[SendingMedia] Start - Picture"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5716
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_18

    .line 5717
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5720
    :cond_18
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5721
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 5725
    :sswitch_d
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_19

    .line 5726
    const-string v0, "[SendingMedia] Start - Document"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5729
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_1a

    .line 5730
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5734
    :cond_1a
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->c(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5738
    :sswitch_e
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1b

    .line 5739
    const-string v0, "[SendingMedia] Start - Voice"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5742
    :cond_1b
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_1c

    .line 5743
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5747
    :cond_1c
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->d(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5751
    :sswitch_f
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1d

    .line 5752
    const-string v0, "[SendingMedia] Start - S Note"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5755
    :cond_1d
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_1e

    .line 5756
    invoke-direct {p0, p3, p1}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5760
    :cond_1e
    invoke-direct {p0, p3}, Lcom/sec/chaton/chat/ChatFragment;->e(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5771
    :sswitch_10
    if-ne p2, v0, :cond_1f

    .line 5773
    const-string v0, "source_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5775
    const-string v1, "target_text"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5783
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 5785
    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5793
    :goto_2
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cY:Z

    .line 5798
    :cond_1f
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cz:Z

    if-eqz v0, :cond_0

    .line 5799
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->b()V

    goto/16 :goto_0

    .line 5791
    :cond_20
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->cZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 5805
    :cond_21
    const/16 v0, 0xc9

    if-ne p1, v0, :cond_22

    if-nez p2, :cond_22

    .line 5807
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cY:Z

    .line 5808
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->cZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 5810
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cz:Z

    if-eqz v0, :cond_0

    .line 5811
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->b()V

    goto/16 :goto_0

    .line 5815
    :cond_22
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/chat/ChatFragment;->a(ILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5819
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b003a

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    goto/16 :goto_0

    .line 5375
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_1
        0x4 -> :sswitch_4
        0x5 -> :sswitch_7
        0x6 -> :sswitch_9
        0x7 -> :sswitch_8
        0x9 -> :sswitch_e
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xf -> :sswitch_a
        0x10 -> :sswitch_c
        0x11 -> :sswitch_b
        0x12 -> :sswitch_d
        0x13 -> :sswitch_f
        0x14 -> :sswitch_0
        0x15 -> :sswitch_c
        0x16 -> :sswitch_2
        0xc9 -> :sswitch_10
    .end sparse-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 942
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 944
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    .line 945
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 8940
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 9375
    :cond_0
    :goto_0
    return-void

    .line 8944
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v1}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setSelection(I)V

    .line 8945
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->k()V

    goto :goto_0

    .line 8989
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->requestFocus()Z

    .line 8990
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9244
    :sswitch_2
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cO:Z

    if-eqz v0, :cond_1

    .line 9245
    iput-boolean v5, p0, Lcom/sec/chaton/chat/ChatFragment;->cX:Z

    .line 9246
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v0

    .line 9247
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 9248
    const-string v2, "com.sec.android.app.translator.TRANSLATE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 9249
    const-string v2, "do_not_use_select_result_dialog"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 9250
    const-string v2, "mode"

    const-string v3, "input"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9255
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/b/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 9256
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/chat/b/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 9258
    const-string v3, "source_language"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9259
    const-string v3, "source_text"

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v4}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9260
    const-string v3, "target_language"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9261
    const-string v3, "auto_start_translation"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 9262
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/chaton/chat/ChatFragment;->cZ:Ljava/lang/String;

    .line 9263
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Source Lang Code(DB) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->cI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9264
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Target Lang Code(DB) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->cJ:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9265
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Source Lang Code for S-Translator : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9266
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Target Lang Code for S-Translator : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9270
    const-string v0, "Calling Intent for translator App: com.sec.android.app.translator.TRANSLATE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9272
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cz:Z

    .line 9274
    const/16 v0, 0xc9

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 9277
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9284
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 9288
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 9289
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9293
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9298
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9303
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->U()V

    goto/16 :goto_0

    .line 9316
    :sswitch_4
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->at()Z

    move-result v0

    if-nez v0, :cond_4

    .line 9317
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->au()V

    .line 9320
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 9321
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 9322
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 9326
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/HeightChangedListView;->setFocusable(Z)V

    .line 9327
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/HeightChangedListView;->setItemsCanFocus(Z)V

    .line 9337
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/HeightChangedListView;->setFocusable(Z)V

    goto/16 :goto_0

    .line 9340
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 9352
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v5}, Lcom/sec/widget/HeightChangedListView;->setFocusable(Z)V

    .line 9354
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->B()V

    goto/16 :goto_0

    .line 8940
    :sswitch_data_0
    .sparse-switch
        0x7f0700fe -> :sswitch_0
        0x7f07010b -> :sswitch_3
        0x7f07010c -> :sswitch_4
        0x7f070113 -> :sswitch_1
        0x7f070116 -> :sswitch_2
    .end sparse-switch
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 10904
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    .line 10905
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 11308
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 11310
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 11312
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    if-eqz v0, :cond_0

    .line 11313
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->at()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11314
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 11322
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setMaxLines(I)V

    .line 11368
    :goto_1
    return-void

    .line 11316
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aK()V

    goto :goto_0

    .line 11324
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aJ()V

    .line 11325
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setMaxLines(I)V

    goto :goto_1
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 11487
    const-string v0, "onContextItemSelected!!!!"

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 11488
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 958
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 960
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 961
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Enter the ChatRoom] InboxNO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    :cond_0
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    .line 965
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    .line 966
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bf:J

    .line 968
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    .line 969
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v1

    .line 970
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    .line 971
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 972
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bE:Landroid/view/inputmethod/InputMethodManager;

    .line 974
    new-instance v0, Lcom/sec/chaton/trunk/a/a;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/trunk/a/a;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aE:Lcom/sec/chaton/trunk/a/a;

    .line 976
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->S:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    .line 978
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/util/i;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bS:Z

    .line 981
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bW:Landroid/content/Intent;

    .line 985
    new-instance v0, Lcom/sec/chaton/multimedia/audio/a;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/sec/chaton/multimedia/audio/a;-><init>(I)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    .line 987
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    const v1, 0x7f06000a

    const-string v2, "TRANS_FAIL"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/a;->a(ILjava/lang/String;)Z

    .line 990
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    const v1, 0x7f060005

    const-string v2, "RECV_SUCC"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/a;->a(ILjava/lang/String;)Z

    .line 993
    if-eqz p1, :cond_1

    const-string v0, "captureUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 994
    const-string v0, "captureUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    .line 997
    :cond_1
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    .line 1000
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1001
    sget-object v1, Lcom/sec/chaton/plugin/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1002
    sget-object v1, Lcom/sec/chaton/plugin/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1003
    sget-object v1, Lcom/sec/chaton/plugin/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1004
    sget-object v1, Lcom/sec/chaton/plugin/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1005
    sget-object v1, Lcom/sec/chaton/plugin/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1006
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1007
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/chat/b/c;->a(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cN:Z

    .line 1008
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->dm:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1011
    :cond_2
    return-void

    .line 971
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8

    .prologue
    .line 4636
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 4640
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    if-eqz v0, :cond_1

    .line 4850
    :cond_0
    :goto_0
    return-void

    .line 4646
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    if-eqz v0, :cond_0

    .line 4651
    new-instance v3, Lcom/sec/chaton/b/a;

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-direct {v3, v0, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 4654
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 4656
    iget-object v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/el;

    .line 4659
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->ay:Lcom/sec/chaton/chat/a/a;

    instance-of v1, v1, Lcom/sec/chaton/chat/a/x;

    if-eqz v1, :cond_0

    .line 4663
    iget-object v1, v0, Lcom/sec/chaton/chat/el;->ay:Lcom/sec/chaton/chat/a/a;

    check-cast v1, Lcom/sec/chaton/chat/a/x;

    .line 4665
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/a/x;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    .line 4666
    invoke-virtual {v3}, Lcom/sec/chaton/b/a;->clear()V

    .line 4668
    instance-of v2, v1, Lcom/sec/chaton/chat/a/k;

    if-eqz v2, :cond_2

    move-object v2, v1

    .line 4669
    check-cast v2, Lcom/sec/chaton/chat/a/k;

    .line 4671
    invoke-interface {v2}, Lcom/sec/chaton/chat/a/k;->i()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4672
    const v4, 0x7f0b005c

    invoke-virtual {v3, v4}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, Lcom/sec/chaton/chat/ab;

    invoke-direct {v5, p0, v2}, Lcom/sec/chaton/chat/ab;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/a/k;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 4695
    :cond_2
    instance-of v2, v1, Lcom/sec/chaton/chat/a/m;

    if-eqz v2, :cond_3

    move-object v2, v1

    .line 4696
    check-cast v2, Lcom/sec/chaton/chat/a/m;

    .line 4699
    invoke-interface {v2}, Lcom/sec/chaton/chat/a/m;->g()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4700
    const v4, 0x7f0b013a

    invoke-virtual {v3, v4}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, Lcom/sec/chaton/chat/ac;

    invoke-direct {v5, p0, v2}, Lcom/sec/chaton/chat/ac;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/a/m;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 4715
    :cond_3
    instance-of v2, v1, Lcom/sec/chaton/chat/a/p;

    if-eqz v2, :cond_4

    move-object v2, v1

    .line 4716
    check-cast v2, Lcom/sec/chaton/chat/a/p;

    .line 4719
    invoke-interface {v2}, Lcom/sec/chaton/chat/a/p;->e()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 4720
    const v4, 0x7f0b0165

    invoke-virtual {v3, v4}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, Lcom/sec/chaton/chat/ad;

    invoke-direct {v5, p0, v2}, Lcom/sec/chaton/chat/ad;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/a/p;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 4761
    :cond_4
    instance-of v2, v1, Lcom/sec/chaton/chat/a/o;

    if-eqz v2, :cond_5

    move-object v2, v1

    .line 4762
    check-cast v2, Lcom/sec/chaton/chat/a/o;

    .line 4764
    invoke-interface {v2}, Lcom/sec/chaton/chat/a/o;->k()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 4765
    const v4, 0x7f0b003c

    invoke-virtual {v3, v4}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, Lcom/sec/chaton/chat/af;

    invoke-direct {v5, p0, v2}, Lcom/sec/chaton/chat/af;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/a/o;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 4781
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v2, v4, :cond_7

    .line 4783
    iget-object v2, v0, Lcom/sec/chaton/chat/el;->ax:Lcom/sec/chaton/e/w;

    .line 4784
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v4

    .line 4785
    iget-object v5, v0, Lcom/sec/chaton/chat/el;->az:Ljava/lang/String;

    .line 4787
    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 4788
    iget-boolean v6, v0, Lcom/sec/chaton/chat/el;->aF:Z

    if-nez v6, :cond_7

    iget-object v6, v0, Lcom/sec/chaton/chat/el;->aG:Ljava/lang/String;

    if-nez v6, :cond_7

    const-string v6, "mixed"

    const/4 v7, 0x0

    aget-object v5, v5, v7

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    sget-object v5, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v2, v5, :cond_7

    .line 4790
    :cond_6
    const v2, 0x7f0b03af

    invoke-virtual {v3, v2}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v5, Lcom/sec/chaton/chat/ag;

    invoke-direct {v5, p0, v0, v4}, Lcom/sec/chaton/chat/ag;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/el;Lcom/sec/chaton/chat/b/c;)V

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 4812
    :cond_7
    instance-of v0, v1, Lcom/sec/chaton/chat/a/l;

    if-eqz v0, :cond_0

    .line 4814
    iget v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    .line 4816
    invoke-interface {v1}, Lcom/sec/chaton/chat/a/l;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4817
    const v2, 0x7f0b005b

    invoke-virtual {v3, v2}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/chat/ah;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/chaton/chat/ah;-><init>(Lcom/sec/chaton/chat/ChatFragment;ILcom/sec/chaton/chat/a/l;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const v1, 0x7f070570

    .line 3031
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    .line 3033
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3034
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3035
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->b(Landroid/view/MenuItem;)V

    .line 3036
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->L()V

    .line 3041
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3042
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 3044
    const v0, 0x7f0f0035

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-virtual {p2, v0, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 3048
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/Menu;)V

    .line 3050
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 3051
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1015
    iput-boolean v7, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    .line 1017
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bU:Lcom/sec/common/f/c;

    .line 1019
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dl:Ljava/util/ArrayList;

    .line 1020
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->C()V

    .line 1022
    new-instance v0, Lorg/a/a/a/a/j;

    new-instance v1, Lcom/sec/chaton/chat/a/f;

    invoke-direct {v1}, Lcom/sec/chaton/chat/a/f;-><init>()V

    const-wide/16 v4, -0x1

    invoke-direct/range {v0 .. v5}, Lorg/a/a/a/a/j;-><init>(Lorg/a/a/a/e;IBJ)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ck:Lorg/a/a/a/a/j;

    .line 1024
    const v0, 0x7f030027

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    .line 1026
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ag()V

    .line 1028
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dc:Landroid/widget/RelativeLayout;

    .line 1030
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aq:Landroid/widget/FrameLayout;

    .line 1031
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700fb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    .line 1032
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ar:Landroid/widget/FrameLayout;

    .line 1034
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->do:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070102

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bG:Landroid/view/ViewGroup;

    .line 1050
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700fd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/HeightChangedListView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    .line 1055
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, p0}, Lcom/sec/widget/HeightChangedListView;->setOnHeightChangedListener(Lcom/sec/widget/ak;)V

    .line 1060
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070101

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bL:Landroid/view/View;

    .line 1061
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bL:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1063
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070113

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    .line 1064
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1066
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bC:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1069
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070116

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    .line 1070
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bD:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1072
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f07010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/MyEditText;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    .line 1073
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    new-instance v1, Lcom/sec/chaton/chat/p;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/p;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1087
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    new-instance v1, Lcom/sec/chaton/chat/aa;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/aa;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1217
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    new-instance v1, Lcom/sec/chaton/chat/ao;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/ao;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1250
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    new-instance v1, Lcom/sec/chaton/chat/bb;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bb;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setOnEditTextImeBackListener(Lcom/sec/widget/w;)V

    .line 1258
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    new-instance v1, Lcom/sec/chaton/chat/bo;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bo;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1281
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    new-instance v1, Lcom/sec/chaton/chat/bz;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/bz;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1350
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1351
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->requestFocus()Z

    .line 1355
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700fe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cg:Landroid/widget/LinearLayout;

    .line 1356
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1357
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1358
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ch:Landroid/widget/ImageView;

    .line 1359
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070100

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ci:Landroid/widget/TextView;

    .line 1362
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    new-instance v1, Lcom/sec/chaton/chat/cl;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/cl;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1423
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f07010c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ao:Landroid/widget/ImageButton;

    .line 1424
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ao:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1425
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f07010b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->an:Landroid/widget/ImageButton;

    .line 1426
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->an:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1428
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f0700f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bF:Landroid/widget/ImageView;

    .line 1431
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f07010f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->af:Landroid/widget/LinearLayout;

    .line 1432
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070110

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ag:Landroid/widget/ImageView;

    .line 1433
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070111

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ah:Landroid/widget/ImageView;

    .line 1434
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070112

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ai:Landroid/widget/ImageButton;

    .line 1436
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ai:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/chat/cq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/cq;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1446
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aB:Landroid/widget/Toast;

    .line 1450
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b0056

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aC:Landroid/app/ProgressDialog;

    .line 1451
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->o()V

    .line 1452
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070115

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    .line 1453
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070104

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bY:Landroid/widget/LinearLayout;

    .line 1470
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/chat/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/d;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1483
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070103

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ca:Landroid/widget/FrameLayout;

    .line 1485
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ca:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1486
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ca:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1495
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v1, 0x7f070117

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cb:Landroid/widget/LinearLayout;

    .line 1496
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cb:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1497
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cb:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1499
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1501
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/chat/e;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/e;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1573
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bX:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/chat/f;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/f;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1587
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    const v2, 0x7f0700f5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/et;->a(Landroid/app/Activity;Landroid/widget/LinearLayout;)Lcom/sec/chaton/chat/et;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->da:Lcom/sec/chaton/chat/et;

    .line 1589
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002c

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    .line 1590
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->addHeaderView(Landroid/view/View;)V

    .line 1591
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    const v1, 0x7f07012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->at:Landroid/widget/LinearLayout;

    .line 1592
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    const v1, 0x7f07012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->au:Landroid/widget/Button;

    .line 1593
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    const v1, 0x7f070131

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->av:Landroid/widget/Button;

    .line 1594
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    const v1, 0x7f070130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aw:Landroid/widget/Button;

    .line 1595
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    const v1, 0x7f070132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ax:Landroid/widget/TextView;

    .line 1596
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->as:Landroid/view/View;

    const v1, 0x7f070133

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ay:Landroid/widget/LinearLayout;

    .line 1599
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Landroid/sec/multiwindow/MultiWindowManager;->isMultiWindowServiceEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1600
    new-instance v0, Lcom/sec/chaton/chat/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/g;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ct:Landroid/view/View$OnDragListener;

    .line 1620
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dc:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->ct:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1622
    new-instance v0, Lcom/sec/chaton/chat/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/h;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cs:Landroid/view/View$OnDragListener;

    .line 1641
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cs:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1642
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aq:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->cs:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1648
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1649
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->an:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->b(Landroid/view/View;I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->de:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    .line 1650
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ao:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/View;I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->df:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    .line 1653
    new-instance v0, Lcom/samsung/android/sdk/look/writingbuddy/SlookWritingBuddy;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/look/writingbuddy/SlookWritingBuddy;-><init>(Landroid/widget/EditText;)V

    .line 1654
    new-instance v1, Lcom/sec/chaton/chat/i;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/i;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/look/writingbuddy/SlookWritingBuddy;->setImageWritingListener(Lcom/samsung/android/sdk/look/writingbuddy/SlookWritingBuddy$ImageWritingListener;)V

    .line 1689
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->b:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11493
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 11494
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Go out the ChatRoom] inboxNO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 11496
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bE:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    if-eqz v0, :cond_1

    .line 11497
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 11498
    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bE:Landroid/view/inputmethod/InputMethodManager;

    .line 11501
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_2

    .line 11502
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->R:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    .line 11503
    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    .line 11512
    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 11514
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    if-eqz v0, :cond_3

    .line 11515
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cd:Lcom/sec/chaton/multimedia/audio/a;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/a;->a()V

    .line 11520
    :cond_3
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->b()V

    .line 11521
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->c()V

    .line 11522
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->d()V

    .line 11526
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->dm:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 11529
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->c()V

    .line 11536
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    if-eqz v0, :cond_4

    .line 11537
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 11538
    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    .line 11540
    :cond_4
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2162
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 2163
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bU:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 2166
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->e()V

    .line 2167
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/HeightChangedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2170
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Landroid/sec/multiwindow/MultiWindowManager;->isMultiWindowServiceEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171
    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cs:Landroid/view/View$OnDragListener;

    .line 2172
    iput-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ct:Landroid/view/View$OnDragListener;

    .line 2173
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dc:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2174
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/MyEditText;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2179
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cj:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cj:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2180
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cj:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2186
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 2187
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2188
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2189
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 2190
    const-string v0, "The Cursor of Adapter was closed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2195
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/fk;)V

    .line 2199
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ad()V

    .line 2205
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ck:Lorg/a/a/a/a/j;

    if-eqz v0, :cond_3

    .line 2206
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ck:Lorg/a/a/a/a/j;

    invoke-virtual {v0}, Lorg/a/a/a/a/j;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2216
    :cond_3
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2217
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->ae()V

    .line 2219
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    if-eqz v0, :cond_4

    .line 2220
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->dh:Lcom/sec/chaton/chat/dg;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/dg;->cancel(Z)Z

    .line 2224
    :cond_4
    return-void

    .line 2208
    :catch_0
    move-exception v0

    .line 2209
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 2210
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 949
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 951
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    .line 952
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 11482
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 11

    .prologue
    const/4 v4, -0x2

    const/4 v3, -0x3

    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 3438
    .line 3439
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 3440
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3911
    :cond_0
    :goto_0
    :pswitch_0
    return v10

    .line 3443
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->P()V

    goto :goto_0

    .line 3450
    :pswitch_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 3451
    if-eq v3, v0, :cond_1

    if-ne v4, v0, :cond_2

    .line 3452
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 3477
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_0

    .line 3484
    invoke-static {}, Lcom/sec/chaton/e/i;->a()Landroid/net/Uri;

    move-result-object v3

    .line 3486
    const-string v5, ""

    .line 3487
    const-string v0, ""

    .line 3489
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 3490
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 3492
    array-length v6, v0

    if-lez v6, :cond_4

    .line 3493
    const-string v5, ""

    .line 3494
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "buddy_no NOT IN ( "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3495
    :goto_1
    array-length v6, v0

    if-ge v1, v6, :cond_3

    .line 3496
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3495
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3499
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3503
    :cond_4
    const-string v7, "group_type, CASE WHEN group_name IS NULL THEN 1 ELSE 0 END,group_name, buddy_name COLLATE LOCALIZED ASC"

    .line 3505
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/16 v1, 0xd

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3525
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v5

    .line 3526
    invoke-virtual {v5, v10}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    .line 3528
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 3530
    const v0, 0x7f0b03af

    invoke-virtual {v5, v0}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 3533
    new-instance v6, Lcom/sec/chaton/chat/a;

    invoke-direct {v6}, Lcom/sec/chaton/chat/a;-><init>()V

    .line 3534
    invoke-static {}, Lcom/sec/chaton/chat/b/n;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3535
    const v0, 0x7f03014f

    invoke-virtual {v3, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 3536
    const v0, 0x7f07054b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    .line 3537
    invoke-virtual {v6, v0}, Lcom/sec/chaton/chat/a;->a(Landroid/widget/Switch;)V

    move-object v4, v2

    .line 3544
    :goto_2
    const v0, 0x7f070344

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3550
    invoke-direct {p0, v3, v1, v6}, Lcom/sec/chaton/chat/ChatFragment;->a(Landroid/view/LayoutInflater;ZLcom/sec/chaton/chat/a;)Landroid/view/View;

    move-result-object v7

    .line 3552
    const v1, 0x7f0701fd

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 3554
    const v2, 0x7f070202

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    .line 3555
    const v3, 0x7f070206

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    .line 3556
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v8

    .line 3561
    invoke-virtual {v5, v7}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    .line 3562
    invoke-virtual {v5, v4}, Lcom/sec/common/a/a;->a(Landroid/view/View;)Lcom/sec/common/a/a;

    .line 3563
    const v4, 0x7f0b03af

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 3565
    new-instance v0, Lcom/sec/chaton/chat/s;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/s;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3572
    new-instance v0, Lcom/sec/chaton/chat/t;

    invoke-direct {v0, p0, v8, v2, v3}, Lcom/sec/chaton/chat/t;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 3590
    new-instance v0, Lcom/sec/chaton/chat/u;

    invoke-direct {v0, p0, v8, v3}, Lcom/sec/chaton/chat/u;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Landroid/widget/Spinner;)V

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 3607
    const v0, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/chat/v;

    invoke-direct {v2, p0, v6, v1}, Lcom/sec/chaton/chat/v;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/a;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v0, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 3710
    const v0, 0x7f0b0039

    new-instance v1, Lcom/sec/chaton/chat/w;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/w;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v5, v0, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 3724
    invoke-virtual {v5}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 3539
    :cond_5
    const v0, 0x7f03010a

    invoke-virtual {v3, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    goto :goto_2

    .line 3728
    :pswitch_4
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    if-eqz v0, :cond_6

    .line 3729
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)I

    .line 3730
    iput-boolean v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    .line 3736
    :goto_3
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->d(Z)V

    goto/16 :goto_0

    .line 3733
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0, v1, v10}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)I

    .line 3734
    iput-boolean v10, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    goto :goto_3

    .line 3739
    :pswitch_5
    sput-boolean v1, Lcom/sec/chaton/chat/ChatFragment;->o:Z

    .line 3745
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 3746
    if-eq v3, v0, :cond_7

    if-ne v4, v0, :cond_8

    .line 3747
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 3752
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->e()V

    goto/16 :goto_0

    .line 3756
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aH()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3759
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ei;->c()V

    .line 3762
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Y:Lcom/sec/chaton/e/a/u;

    const/16 v1, 0xc

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/v;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3764
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b0030

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 3768
    :pswitch_7
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->r()V

    goto/16 :goto_0

    .line 3771
    :pswitch_8
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->q()V

    goto/16 :goto_0

    :pswitch_9
    move v5, v1

    .line 3778
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    if-nez v0, :cond_a

    .line 3779
    const-string v0, "ChatONV is not available"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3783
    :cond_a
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->S()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 3784
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 3786
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "trying to connect "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v5, :cond_b

    const-string v0, "video"

    :goto_5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " call..."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3789
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 3791
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v3, :cond_c

    .line 3792
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v7, "Push Name"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 3794
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b01f9

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 3786
    :cond_b
    const-string v0, "voice"

    goto :goto_5

    .line 3797
    :cond_c
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->X:Ljava/lang/Object;

    monitor-enter v3

    .line 3798
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v0, v4, v5}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .line 3799
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v4, :cond_e

    .line 3800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ChatONV] participants limit exceed - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v5, :cond_d

    const-string v0, "video"

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3801
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ad

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-virtual {v1, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3802
    monitor-exit v3

    goto/16 :goto_0

    .line 3804
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 3800
    :cond_d
    :try_start_1
    const-string v0, "voice"

    goto :goto_6

    .line 3804
    :cond_e
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3806
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatFragment;->ab:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v7, "Push Name"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v8, p0, Lcom/sec/chaton/chat/ChatFragment;->bT:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/chaton/e/a/f;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object v8, v2

    invoke-virtual/range {v3 .. v9}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 3808
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b01f9

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 3812
    :cond_f
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3813
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3816
    :cond_10
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->R()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3817
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3818
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3824
    :pswitch_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const-string v3, "layout_inflater"

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3825
    const v3, 0x7f030018

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 3826
    const v0, 0x7f070074

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 3828
    new-instance v3, Lcom/sec/chaton/chat/x;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/x;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3836
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const v4, 0x7f0b01eb

    invoke-virtual {p0, v4}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const v1, 0x7f0b01ea

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v10

    .line 3841
    new-instance v1, Lcom/sec/chaton/chat/y;

    invoke-direct {v1, p0, v0, v3}, Lcom/sec/chaton/chat/y;-><init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/widget/CheckBox;[Ljava/lang/CharSequence;)V

    .line 3861
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v4, 0x7f0b01e8

    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/z;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/z;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 3873
    invoke-interface {v0}, Lcom/sec/common/a/d;->b()Landroid/widget/ListView;

    move-result-object v1

    .line 3874
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 3875
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 3889
    :pswitch_b
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_11

    .line 3890
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v2, 0x7f0b003d

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 3892
    :cond_11
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->an()Z

    move-result v0

    if-ne v0, v10, :cond_0

    .line 3893
    new-instance v0, Lcom/sec/chaton/chat/cy;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/cy;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    new-array v2, v10, [Ljava/lang/Integer;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/cy;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 3899
    :pswitch_c
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3900
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->k(Ljava/lang/String;)V

    .line 3902
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3903
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/trunk/TrunkActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 3904
    const-string v1, "sessionId"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3905
    const-string v1, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3906
    const-string v1, "isValid"

    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3907
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_d
    move v5, v10

    goto/16 :goto_4

    .line 3440
    nop

    :pswitch_data_0
    .packed-switch 0x7f070566
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_d
        :pswitch_5
        :pswitch_c
        :pswitch_0
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public onPause()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2689
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 2691
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 2692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onPause() ChatRoom] InboxNO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2695
    :cond_0
    iput-boolean v5, p0, Lcom/sec/chaton/chat/ChatFragment;->bO:Z

    .line 2697
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cX:Z

    if-nez v0, :cond_1

    .line 2701
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->v:Ljava/lang/String;

    .line 2707
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->K()V

    .line 2709
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->n()V

    .line 2711
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->N:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2712
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->M:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2722
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->Q:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2737
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_2

    .line 2739
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 2740
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aD:Lcom/sec/chaton/d/o;

    const-wide/32 v3, 0x75300

    add-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/d/o;->c(J)V

    .line 2748
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/notification/a;->c(Ljava/lang/String;)V

    .line 2749
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/receiver/PushReceiver;->a(Lcom/sec/chaton/chat/fi;)V

    .line 2777
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->c()V

    .line 2780
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bZ:Lcom/sec/chaton/multimedia/audio/k;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->I:Z

    if-eqz v0, :cond_3

    .line 2781
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bZ:Lcom/sec/chaton/multimedia/audio/k;

    invoke-virtual {v0, v5}, Lcom/sec/chaton/multimedia/audio/k;->a(Z)V

    .line 2790
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 2792
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->da:Lcom/sec/chaton/chat/et;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/et;->c()V

    .line 2795
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2796
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a()Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b()V

    .line 2798
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 7

    .prologue
    const v6, 0x7f07056d

    const v5, 0x7f070569

    const v3, 0x7f070568

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3056
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 3057
    const-string v0, "onPrepareOptionsMenu called"

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3060
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 3063
    const v0, 0x7f070567

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3064
    if-eqz v0, :cond_1

    .line 3066
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-nez v2, :cond_d

    .line 3067
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3071
    :goto_0
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3074
    :cond_1
    const v0, 0x7f07056a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3075
    if-eqz v0, :cond_2

    .line 3076
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v2, :cond_e

    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aH()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 3077
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3081
    :goto_1
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3089
    :cond_2
    const v0, 0x7f07056f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3090
    if-eqz v0, :cond_3

    .line 3091
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3092
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3095
    :cond_3
    const v0, 0x7f07056b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3096
    if-eqz v0, :cond_4

    .line 3097
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v2, :cond_f

    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aH()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 3098
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3102
    :goto_2
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3105
    :cond_4
    const v0, 0x7f070566

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3106
    if-eqz v0, :cond_5

    .line 3107
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3108
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3111
    :cond_5
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3112
    if-eqz v0, :cond_6

    .line 3113
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 3114
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 3123
    :goto_3
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-nez v2, :cond_12

    .line 3124
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3128
    :goto_4
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3131
    :cond_6
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3132
    if-eqz v0, :cond_8

    .line 3133
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-eq v2, v3, :cond_7

    .line 3134
    invoke-interface {p1, v5}, Landroid/view/Menu;->removeItem(I)V

    .line 3137
    :cond_7
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-nez v2, :cond_13

    .line 3138
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3142
    :goto_5
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3145
    :cond_8
    const v0, 0x7f07056c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3146
    if-eqz v0, :cond_9

    .line 3147
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-eqz v2, :cond_14

    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aH()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 3148
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3152
    :goto_6
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3156
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v2

    .line 3157
    if-eqz v2, :cond_16

    .line 3159
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 3160
    if-eqz v3, :cond_a

    .line 3164
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_15

    invoke-direct {p0, v1}, Lcom/sec/chaton/chat/ChatFragment;->c(Z)Z

    move-result v0

    .line 3166
    :goto_7
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3167
    invoke-static {v3}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3173
    :cond_a
    :goto_8
    if-eqz v2, :cond_17

    .line 3174
    const v0, 0x7f07056e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3175
    if-eqz v0, :cond_c

    .line 3179
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_b

    invoke-direct {p0, v4}, Lcom/sec/chaton/chat/ChatFragment;->c(Z)Z

    move-result v1

    .line 3181
    :cond_b
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3182
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 3187
    :cond_c
    :goto_9
    return-void

    .line 3069
    :cond_d
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 3079
    :cond_e
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 3100
    :cond_f
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 3116
    :cond_10
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bg:Z

    if-eqz v2, :cond_11

    .line 3117
    const v2, 0x7f0b0293

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f0202fc

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto/16 :goto_3

    .line 3119
    :cond_11
    const v2, 0x7f0b0292

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f0202fd

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto/16 :goto_3

    .line 3126
    :cond_12
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_4

    .line 3140
    :cond_13
    iget-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_5

    .line 3150
    :cond_14
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_6

    :cond_15
    move v0, v1

    .line 3164
    goto :goto_7

    .line 3170
    :cond_16
    invoke-interface {p1, v6}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_8

    .line 3185
    :cond_17
    const v0, 0x7f07056e

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_9
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 2527
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 2529
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 2530
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onResume() ChatRoom] InboxNO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2533
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 2534
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2535
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2536
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2538
    new-instance v1, Lcom/sec/chaton/chat/cv;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/chat/cv;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/c;)V

    iput-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    .line 2539
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatFragment;->dk:Lcom/sec/chaton/chat/cv;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2541
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 2542
    const-string v0, "keyguard is displayed. Register receiver"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2551
    :cond_1
    :goto_0
    return-void

    .line 2546
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_3

    .line 2547
    const-string v0, "keyguard is not displayed"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->F()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 5827
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 5828
    const-string v0, "captureUri"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bc:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5833
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5834
    const-string v0, "inbox_no"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5837
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5838
    const-string v0, "session_id"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5842
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 5843
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 2156
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 2157
    return-void
.end method

.method public p()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 12384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    .line 12386
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->at()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12387
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->av()V

    .line 12390
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->L()V

    .line 12391
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->Z:Landroid/view/Menu;

    const v1, 0x7f070573

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 12393
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0158

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12395
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->t()V

    .line 12396
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/widget/HeightChangedListView;->setChoiceMode(I)V

    .line 12397
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v3}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    .line 12399
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aq:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 12400
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 12401
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 12405
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->b()V

    .line 12408
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    if-eqz v0, :cond_1

    .line 12409
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    sget-object v1, Lcom/sec/chaton/chat/em;->a:Lcom/sec/chaton/chat/em;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/em;)V

    .line 12413
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cG:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 12414
    return-void
.end method

.method public q()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12417
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->ak:Z

    .line 12418
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->L()V

    .line 12420
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cE:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12421
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->ap:Landroid/widget/CheckedTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 12423
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    if-eqz v0, :cond_0

    .line 12424
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/HeightChangedListView;->setChoiceMode(I)V

    .line 12425
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->az:Lcom/sec/widget/HeightChangedListView;

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->clearChoices()V

    .line 12428
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bd:Z

    if-eqz v0, :cond_1

    .line 12429
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aq:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 12432
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cG:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 12433
    return-void
.end method

.method public r()V
    .locals 4

    .prologue
    .line 12436
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aH()Z

    move-result v0

    if-nez v0, :cond_0

    .line 12437
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    const v1, 0x7f0b0030

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 12438
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->q()V

    .line 12572
    :goto_0
    return-void

    .line 12442
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 12443
    const v1, 0x7f0b02bb

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 12445
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aA:Lcom/sec/chaton/chat/ei;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ei;->d()Lcom/sec/chaton/chat/em;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/em;->c:Lcom/sec/chaton/chat/em;

    if-ne v1, v2, :cond_1

    .line 12446
    const v1, 0x7f0b0057

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 12450
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0042

    new-instance v3, Lcom/sec/chaton/chat/bn;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/bn;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/bm;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/bm;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 12570
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 12448
    :cond_1
    const v1, 0x7f0b03eb

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    goto :goto_1
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 12631
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    return v0
.end method

.method public t()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12848
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bE:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/MyEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 12850
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    .line 12853
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aJ()V

    .line 12856
    invoke-static {}, Lcom/sec/chaton/util/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12857
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->af()V

    .line 12860
    :cond_0
    return-void
.end method

.method public u()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 12864
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    if-eqz v0, :cond_1

    .line 12884
    :cond_0
    :goto_0
    return-void

    .line 12868
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 12872
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 12873
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 12877
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bE:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 12878
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatFragment;->aK()V

    .line 12883
    :cond_2
    :goto_1
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatFragment;->cy:Z

    goto :goto_0

    .line 12879
    :cond_3
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_2

    .line 12880
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->bE:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->bh:Lcom/sec/chaton/chat/MyEditText;

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1
.end method

.method public v()V
    .locals 2

    .prologue
    .line 13723
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->C:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 13724
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->C:Landroid/app/ProgressDialog;

    .line 13727
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 13728
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 13730
    :cond_1
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 13733
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->C:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13734
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 13736
    :cond_0
    return-void
.end method

.method public x()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 13965
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->cm:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 13966
    const v0, 0x7f0b01b4

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 13967
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aK:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/a;

    .line 13968
    const-string v1, ""

    .line 13969
    if-eqz v0, :cond_0

    .line 13970
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 13972
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b01b5

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 13973
    invoke-virtual {v2, v6}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/chat/ca;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/ca;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/by;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/by;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 13986
    invoke-virtual {v2}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 13987
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public y()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16161
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatFragment;->aJ:Ljava/util/ArrayList;

    return-object v0
.end method
