.class public Lcom/sec/chaton/chat/background/a;
.super Landroid/os/Handler;
.source "ChatBackgroundHandler.java"


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/chat/background/f;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/sec/chaton/chat/background/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/a;->b:Lcom/sec/chaton/chat/background/c;

    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/chat/background/a;->b()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/a;->b:Lcom/sec/chaton/chat/background/c;

    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/chat/background/a;->b()V

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/background/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    return-object v0
.end method

.method private a(Lcom/sec/chaton/chat/background/k;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 45
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "addRequestToJobList"

    const-string v1, "ChatBackgroundHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_0
    if-nez p1, :cond_2

    .line 50
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 51
    const-string v0, "addRequestToJobList - request is null"

    const-string v1, "ChatBackgroundHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_1
    :goto_0
    return-void

    .line 58
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->q()I

    move-result v0

    if-nez v0, :cond_3

    .line 59
    new-instance v0, Lcom/sec/chaton/chat/background/f;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/background/a;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->p()Landroid/app/Service;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->o()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->q()I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/background/f;-><init>(Landroid/os/Handler;Landroid/os/Looper;Landroid/app/Service;II)V

    .line 65
    :goto_1
    invoke-virtual {v0}, Lcom/sec/chaton/chat/background/f;->c()J

    move-result-wide v1

    .line 67
    invoke-virtual {v0, p1}, Lcom/sec/chaton/chat/background/f;->a(Lcom/sec/chaton/chat/background/h;)I

    move-result v3

    .line 68
    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    .line 69
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->h()J

    move-result-wide v1

    invoke-virtual {v0, v6, v3, v1, v2}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    goto :goto_0

    .line 62
    :cond_3
    new-instance v0, Lcom/sec/chaton/chat/background/d;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/background/a;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->p()Landroid/app/Service;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->o()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->q()I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/background/d;-><init>(Landroid/os/Handler;Landroid/os/Looper;Landroid/app/Service;II)V

    goto :goto_1

    .line 79
    :cond_4
    iget-object v3, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v3, p0, Lcom/sec/chaton/chat/background/a;->b:Lcom/sec/chaton/chat/background/c;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/background/c;->a()Ljava/util/concurrent/BlockingQueue;

    move-result-object v3

    .line 82
    if-eqz v3, :cond_5

    .line 83
    new-instance v4, Lcom/sec/chaton/chat/background/b;

    invoke-direct {v4, p0, v1, v2}, Lcom/sec/chaton/chat/background/b;-><init>(Lcom/sec/chaton/chat/background/a;J)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v3

    .line 91
    if-nez v3, :cond_1

    .line 92
    const/16 v3, 0x8

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->h()J

    move-result-wide v4

    invoke-virtual {v0, v6, v3, v4, v5}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 96
    :cond_5
    const/4 v3, 0x7

    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/k;->h()J

    move-result-wide v4

    invoke-virtual {v0, v6, v3, v4, v5}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/sec/chaton/chat/background/c;

    invoke-direct {v0}, Lcom/sec/chaton/chat/background/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/background/a;->b:Lcom/sec/chaton/chat/background/c;

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/chat/background/a;->b:Lcom/sec/chaton/chat/background/c;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/background/c;->b()V

    .line 42
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 35
    iget-object v0, p0, Lcom/sec/chaton/chat/background/a;->b:Lcom/sec/chaton/chat/background/c;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/background/c;->c()V

    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 103
    iget v0, p1, Landroid/os/Message;->what:I

    .line 104
    packed-switch v0, :pswitch_data_0

    .line 121
    :goto_0
    return-void

    .line 106
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/background/k;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/background/a;->a(Lcom/sec/chaton/chat/background/k;)V

    goto :goto_0

    .line 109
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 110
    iget-object v2, p0, Lcom/sec/chaton/chat/background/a;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/background/f;

    .line 111
    invoke-virtual {v0}, Lcom/sec/chaton/chat/background/f;->b()V

    .line 113
    const-string v0, "REQUEST_JOB_COMPLETED"

    const-string v1, "ChatBackgroundHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
