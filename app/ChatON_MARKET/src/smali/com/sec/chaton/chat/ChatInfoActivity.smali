.class public Lcom/sec/chaton/chat/ChatInfoActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ChatInfoActivity.java"

# interfaces
.implements Lcom/sec/chaton/buddy/db;
.implements Lcom/sec/chaton/chat/dw;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 49
    const-string v0, "showAgainPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 56
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 58
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-direct {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;-><init>()V

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    const v1, 0x7f0b01a6

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->b(I)V

    .line 64
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoActivity;->finish()V

    .line 23
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoActivity;->d()V

    .line 34
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 36
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 37
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 41
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoActivity;->finish()V

    .line 44
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
