.class Lcom/sec/chaton/chat/dg;
.super Landroid/os/AsyncTask;
.source "ChatFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Void;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Z)V
    .locals 0

    .prologue
    .line 16183
    iput-object p1, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 16184
    iput-boolean p2, p0, Lcom/sec/chaton/chat/dg;->b:Z

    .line 16185
    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/graphics/Bitmap;)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 16189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 16190
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 16193
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, p1, v3

    invoke-static {v3, v1, v2}, Lcom/sec/common/util/j;->a(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 16203
    :cond_0
    :goto_0
    return-object v0

    .line 16194
    :catch_0
    move-exception v1

    .line 16195
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 16196
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 16198
    :catch_1
    move-exception v1

    .line 16199
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 16200
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/io/File;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 16219
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 16221
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aO(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16240
    :goto_0
    return-void

    .line 16225
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 16227
    if-eqz p1, :cond_1

    .line 16228
    iget-boolean v0, p0, Lcom/sec/chaton/chat/dg;->b:Z

    if-eqz v0, :cond_3

    .line 16229
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    move-object v1, p1

    move-object v3, v2

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 16235
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 16236
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 16239
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_0

    .line 16231
    :cond_3
    iget-object v3, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    sget-object v5, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    const/4 v8, 0x0

    move-object v4, p1

    move-object v6, v2

    move-object v7, v2

    invoke-static/range {v3 .. v8}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/io/File;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16179
    check-cast p1, [Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/dg;->a([Landroid/graphics/Bitmap;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 16209
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 16211
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 16212
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 16214
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/dg;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 16215
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16179
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/dg;->a(Ljava/io/File;)V

    return-void
.end method
