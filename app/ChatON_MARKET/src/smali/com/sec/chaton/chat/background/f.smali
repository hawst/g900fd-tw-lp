.class public Lcom/sec/chaton/chat/background/f;
.super Ljava/lang/Object;
.source "JobContainer.java"


# instance fields
.field protected a:Landroid/os/Handler;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:Lcom/sec/chaton/e/r;

.field protected g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected h:Lcom/sec/chaton/d/o;

.field protected i:Ljava/lang/String;

.field protected j:I

.field protected k:J

.field protected l:J

.field protected m:Landroid/os/Handler;

.field protected n:Z

.field protected o:J

.field protected p:J

.field protected q:Lcom/sec/chaton/e/w;

.field protected r:Ljava/lang/String;

.field protected s:Landroid/app/Service;

.field protected t:I

.field protected u:I


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/os/Looper;Landroid/app/Service;II)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/sec/chaton/chat/background/f;->m:Landroid/os/Handler;

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    .line 99
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->l:J

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    .line 101
    iput-wide v2, p0, Lcom/sec/chaton/chat/background/f;->o:J

    .line 102
    iput-wide v2, p0, Lcom/sec/chaton/chat/background/f;->p:J

    .line 103
    iput-object p3, p0, Lcom/sec/chaton/chat/background/f;->s:Landroid/app/Service;

    .line 104
    iput p4, p0, Lcom/sec/chaton/chat/background/f;->t:I

    .line 105
    iput p5, p0, Lcom/sec/chaton/chat/background/f;->u:I

    .line 106
    new-instance v0, Lcom/sec/chaton/chat/background/g;

    invoke-direct {v0, p0, p2}, Lcom/sec/chaton/chat/background/g;-><init>(Lcom/sec/chaton/chat/background/f;Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/background/f;->a(Landroid/os/Handler;)V

    .line 107
    return-void
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 519
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 534
    :goto_0
    return v0

    .line 525
    :cond_1
    const/4 v2, 0x1

    .line 526
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 527
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 529
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/chat/background/h;)I
    .locals 4

    .prologue
    .line 310
    if-nez p1, :cond_0

    .line 311
    const/4 v0, 0x4

    .line 344
    :goto_0
    return v0

    .line 317
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->e:Ljava/lang/String;

    .line 318
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->b:Ljava/lang/String;

    .line 319
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->k:J

    .line 320
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->p:J

    .line 322
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->d()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->f:Lcom/sec/chaton/e/r;

    .line 323
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->e()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    .line 325
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    .line 327
    sget-object v0, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->f:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_1

    .line 328
    const/4 v0, 0x5

    goto :goto_0

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_2

    .line 332
    const/4 v0, 0x6

    goto :goto_0

    .line 336
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->c()[Ljava/lang/String;

    move-result-object v1

    .line 337
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_3

    .line 338
    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 342
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->r:Ljava/lang/String;

    .line 344
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/chaton/chat/background/f;->a:Landroid/os/Handler;

    .line 276
    return-void
.end method

.method public a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 586
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 587
    const/4 v2, -0x3

    if-eq v2, v1, :cond_0

    const/4 v2, -0x2

    if-ne v2, v1, :cond_2

    .line 588
    :cond_0
    const/4 v0, 0x2

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    move-result v0

    .line 603
    :cond_1
    :goto_0
    return v0

    .line 591
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/chat/background/f;->e()Z

    move-result v1

    if-nez v1, :cond_3

    .line 592
    const/16 v0, 0xd

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    move-result v0

    goto :goto_0

    .line 595
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/background/f;->a(Lcom/sec/chaton/e/r;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 596
    const/16 v0, 0x9

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    move-result v0

    goto :goto_0

    .line 599
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/background/f;->b(Lcom/sec/chaton/e/r;)I

    move-result v1

    .line 600
    if-eq v1, v0, :cond_1

    .line 601
    iget-wide v2, p0, Lcom/sec/chaton/chat/background/f;->p:J

    invoke-virtual {p0, v4, v1, v2, v3}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    move-result v0

    goto :goto_0
.end method

.method protected a(J)Z
    .locals 2

    .prologue
    .line 647
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->m:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 648
    const/4 v0, 0x0

    .line 653
    :goto_0
    return v0

    .line 650
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->m:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 651
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 652
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, p1, p2}, Ljava/lang/Long;-><init>(J)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 653
    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->m:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    goto :goto_0
.end method

.method protected a(Lcom/sec/chaton/e/r;)Z
    .locals 14

    .prologue
    const/4 v6, 0x2

    const/16 v13, 0xc

    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 137
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne p1, v0, :cond_4

    .line 139
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/e/q;->g()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "buddy_no=?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v4, v9

    aput-object v2, v4, v10

    invoke-virtual {p1}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_3

    .line 141
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    .line 155
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 156
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 248
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_10

    move v0, v9

    .line 271
    :goto_2
    return v0

    .line 143
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 144
    const-string v1, "inbox_last_chat_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 145
    const-string v2, "inbox_no"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    .line 146
    if-eq v1, v13, :cond_1

    .line 149
    const-string v1, "inbox_session_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    .line 150
    const-string v1, "inbox_server_ip"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/f;->i:Ljava/lang/String;

    .line 151
    const-string v1, "inbox_server_port"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/chat/background/f;->j:I

    goto :goto_0

    .line 160
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const-string v3, "inbox_chat_type=? AND inbox_participants=?"

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, "inbox_last_time DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 163
    if-eqz v11, :cond_5

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_b

    .line 164
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    .line 168
    :try_start_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 170
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v0, v3}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/util/Map;)Z

    .line 178
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v9

    :cond_6
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 180
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v6, "chaton_id"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 183
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_7

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Insert Participants : "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_7
    iget-object v6, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v6, v0, v1}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 191
    :cond_8
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.chaton.provider"

    invoke-static {v0, v1, v4}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 194
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 195
    const-string v1, "inbox_no"

    iget-object v3, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v1, "inbox_chat_type"

    invoke-virtual {p1}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 197
    const-string v1, "inbox_last_chat_type"

    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 198
    const-string v1, "inbox_participants"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 199
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 238
    :cond_9
    :goto_4
    if-eqz v11, :cond_a

    .line 239
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 242
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 200
    :catch_0
    move-exception v0

    .line 201
    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    .line 202
    :catch_1
    move-exception v0

    .line 203
    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    :cond_b
    move-object v0, v2

    .line 210
    :cond_c
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 212
    const-string v0, "inbox_no"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 213
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    new-array v5, v10, [Ljava/lang/String;

    const-string v0, "participants_buddy_no"

    aput-object v0, v5, v9

    const-string v6, "participants_inbox_no = ?"

    new-array v7, v10, [Ljava/lang/String;

    aput-object v12, v7, v9

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 214
    :cond_d
    if-eqz v0, :cond_13

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 215
    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    move v1, v9

    .line 220
    :goto_5
    if-eqz v1, :cond_c

    .line 221
    iput-object v12, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    .line 222
    const-string v1, "inbox_last_chat_type"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 223
    if-ne v1, v13, :cond_f

    .line 233
    :cond_e
    :goto_6
    if-eqz v0, :cond_9

    .line 234
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 226
    :cond_f
    const-string v1, "inbox_session_id"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    .line 227
    const-string v1, "inbox_server_ip"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/f;->i:Ljava/lang/String;

    .line 228
    const-string v1, "inbox_server_port"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/chat/background/f;->j:I

    goto :goto_6

    .line 254
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->b()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 265
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 266
    iput-boolean v10, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    :cond_11
    move v0, v10

    .line 271
    goto/16 :goto_2

    .line 262
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->i:Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/chat/background/f;->j:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_7

    :cond_13
    move v1, v10

    goto/16 :goto_5
.end method

.method protected a(ZIJ)Z
    .locals 4

    .prologue
    .line 609
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 611
    const-string v0, "packagename is empty"

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_0
    const/4 v0, 0x0

    .line 632
    :goto_0
    return v0

    .line 616
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.chat.background.MESSAGE_SEND_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 617
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 618
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 619
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 620
    const-string v1, "result_code"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 621
    const-string v1, "request_id"

    iget-wide v2, p0, Lcom/sec/chaton/chat/background/f;->k:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 622
    const-string v1, "message_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 623
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 625
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "result : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "result_code : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request_id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->k:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "message_id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packagename : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    :cond_2
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method protected b(Lcom/sec/chaton/e/r;)I
    .locals 12

    .prologue
    .line 350
    const/4 v0, 0x0

    .line 351
    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_13

    .line 352
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/e/a/s;

    move-result-object v0

    .line 353
    if-nez v0, :cond_0

    .line 354
    const/16 v0, 0xa

    .line 506
    :goto_0
    return v0

    :cond_0
    move-object v8, v0

    .line 357
    :goto_1
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_4

    .line 362
    :cond_1
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/f;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    iget-object v3, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    iget-object v6, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/chat/background/f;->r:Ljava/lang/String;

    iget-boolean v9, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    sget-object v10, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    move-object v5, p1

    invoke-virtual/range {v0 .. v10}, Lcom/sec/chaton/d/o;->a(JLjava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    .line 504
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->o:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const/4 v0, 0x1

    goto :goto_0

    .line 375
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    if-eqz v0, :cond_3

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    iget-object v3, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->r:Ljava/lang/String;

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    goto :goto_2

    .line 387
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    iget-object v3, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->r:Ljava/lang/String;

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    goto :goto_2

    .line 398
    :cond_4
    sget-object v0, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_6

    .line 399
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/f;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    iget-object v3, p0, Lcom/sec/chaton/chat/background/f;->f:Lcom/sec/chaton/e/r;

    iget-object v4, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    iget-boolean v6, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    iget-object v7, p0, Lcom/sec/chaton/chat/background/f;->r:Ljava/lang/String;

    sget-object v8, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    goto/16 :goto_2

    .line 410
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->f:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iget-boolean v4, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->r:Ljava/lang/String;

    sget-object v6, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    goto/16 :goto_2

    .line 420
    :cond_6
    iget-object v6, p0, Lcom/sec/chaton/chat/background/f;->r:Ljava/lang/String;

    .line 421
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 422
    const/16 v0, 0xb

    goto/16 :goto_0

    .line 424
    :cond_7
    const-string v0, "file://"

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 425
    const/4 v0, 0x7

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 442
    :goto_3
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 443
    const-string v0, ""

    .line 444
    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_c

    .line 445
    const-string v0, "."

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 460
    :goto_4
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/f;->p:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_11

    .line 461
    const/4 v0, 0x1

    iget v1, v8, Lcom/sec/chaton/e/a/s;->q:I

    if-ne v0, v1, :cond_10

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    iget-object v3, p0, Lcom/sec/chaton/chat/background/f;->f:Lcom/sec/chaton/e/r;

    iget-object v4, v8, Lcom/sec/chaton/e/a/s;->h:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    iget-boolean v7, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    iget-object v8, v8, Lcom/sec/chaton/e/a/s;->l:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    sget-object v11, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(JLcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    goto/16 :goto_2

    .line 426
    :cond_8
    const-string v0, "content://"

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 427
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 429
    if-eqz v1, :cond_12

    move-object v0, v6

    .line 430
    :cond_9
    :goto_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 431
    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 432
    const/4 v3, -0x1

    if-eq v2, v3, :cond_9

    .line 433
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 436
    :cond_a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_6
    move-object v10, v0

    .line 438
    goto/16 :goto_3

    .line 439
    :cond_b
    const/16 v0, 0xb

    goto/16 :goto_0

    .line 446
    :cond_c
    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_d

    .line 447
    const-string v0, "."

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    .line 448
    :cond_d
    sget-object v1, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_f

    .line 449
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "."

    invoke-direct {v1, v10, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :goto_7
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 452
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 455
    :cond_e
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    .line 457
    :cond_f
    const/16 v0, 0xc

    goto/16 :goto_0

    .line 476
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/f;->p:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    iget-object v7, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    iget-boolean v9, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    sget-object v11, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    move-object v6, p1

    invoke-virtual/range {v0 .. v11}, Lcom/sec/chaton/d/o;->a(JLjava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    goto/16 :goto_2

    .line 490
    :cond_11
    iget-object v2, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/chat/background/f;->q:Lcom/sec/chaton/e/w;

    iget-object v7, p0, Lcom/sec/chaton/chat/background/f;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    iget-boolean v9, p0, Lcom/sec/chaton/chat/background/f;->n:Z

    sget-object v11, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    move-object v6, p1

    invoke-virtual/range {v2 .. v11}, Lcom/sec/chaton/d/o;->a(Ljava/io/File;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Lcom/sec/chaton/msgsend/k;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/f;->o:J

    goto/16 :goto_2

    :cond_12
    move-object v0, v6

    goto/16 :goto_6

    :cond_13
    move-object v8, v0

    goto/16 :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/f;->g:Ljava/util/ArrayList;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/f;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/background/f;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 125
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 126
    const-string v0, "stopSelfResult( ) is true"

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_2
    :goto_0
    return-void

    .line 130
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 131
    const-string v0, "stopSelfResult( ) is false"

    const-string v1, "JobContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/f;->l:J

    return-wide v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->s:Landroid/app/Service;

    if-nez v0, :cond_0

    .line 300
    const/4 v0, 0x0

    .line 303
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/background/f;->s:Landroid/app/Service;

    iget v1, p0, Lcom/sec/chaton/chat/background/f;->t:I

    invoke-virtual {v0, v1}, Landroid/app/Service;->stopSelfResult(I)Z

    move-result v0

    goto :goto_0
.end method
