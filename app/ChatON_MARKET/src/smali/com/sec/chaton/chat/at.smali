.class Lcom/sec/chaton/chat/at;
.super Landroid/database/ContentObserver;
.source "ChatFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 6878
    iput-object p1, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 6881
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->V(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6884
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->W(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1e

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->c(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 6886
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/16 v1, 0x10

    iget-object v3, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->v(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/v;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v9, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v4, v4, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v4, v6, v8

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6895
    const-string v0, "onChange - QUERY_MESSAGE_FOR_MARK"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6919
    :goto_0
    return-void

    .line 6898
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->X(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6899
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/16 v1, 0xc

    iget-object v3, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/v;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6910
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-eqz v0, :cond_2

    .line 6911
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->c(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 6914
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->v(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/v;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v9, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v1, v6, v8

    move v1, v8

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6917
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onChange - QUERY_MESSAGE, enableAutoScroll : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/at;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
