.class public Lcom/sec/chaton/chat/eo;
.super Lcom/sec/common/f/a;
.source "ChatProfileImageDispatcherTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/io/File;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:I

.field private e:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/sec/chaton/e/r;

.field private n:I


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p2}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/eo;->a:Ljava/io/File;

    .line 53
    iput-object p1, p0, Lcom/sec/chaton/chat/eo;->b:Landroid/widget/ImageView;

    .line 54
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/chat/eo;->n:I

    .line 55
    iput-object p3, p0, Lcom/sec/chaton/chat/eo;->l:Ljava/lang/String;

    .line 56
    sget-object v0, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/chat/eo;->m:Lcom/sec/chaton/e/r;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p2}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/eo;->a:Ljava/io/File;

    .line 61
    iput-object p1, p0, Lcom/sec/chaton/chat/eo;->b:Landroid/widget/ImageView;

    .line 62
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/chat/eo;->n:I

    .line 63
    iput-object p3, p0, Lcom/sec/chaton/chat/eo;->l:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/sec/chaton/chat/eo;->m:Lcom/sec/chaton/e/r;

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/eo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/chat/eo;->l:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/io/File;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lcom/sec/chaton/chat/ep;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ep;-><init>(Lcom/sec/chaton/chat/eo;)V

    .line 127
    iget-object v1, p0, Lcom/sec/chaton/chat/eo;->a:Ljava/io/File;

    invoke-virtual {v1, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 137
    :cond_0
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->e()Landroid/widget/ImageView;

    move-result-object v1

    .line 79
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/chat/eo;->n:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 80
    iget-object v2, p0, Lcom/sec/chaton/chat/eo;->b:Landroid/widget/ImageView;

    if-nez v2, :cond_1

    .line 81
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 87
    :goto_0
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 88
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 90
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 83
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/eo;->b:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 84
    iget-object v1, p0, Lcom/sec/chaton/chat/eo;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/chaton/chat/eo;->d:I

    .line 69
    iput p2, p0, Lcom/sec/chaton/chat/eo;->e:I

    .line 70
    iput-object p3, p0, Lcom/sec/chaton/chat/eo;->i:Ljava/lang/String;

    .line 71
    iput-object p4, p0, Lcom/sec/chaton/chat/eo;->j:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/chat/eo;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_chat_profile.png_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/eo;->k:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 163
    .line 164
    iget-object v0, p0, Lcom/sec/chaton/chat/eo;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/chat/eo;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 167
    :cond_0
    if-eqz p1, :cond_2

    .line 168
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->f()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 169
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 175
    if-eqz p2, :cond_3

    .line 176
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->e()Landroid/widget/ImageView;

    move-result-object v1

    .line 177
    iget-object v2, p0, Lcom/sec/chaton/chat/eo;->c:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 178
    iget-object v2, p0, Lcom/sec/chaton/chat/eo;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 180
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/eo;->l:Ljava/lang/String;

    sget-object v4, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    .line 181
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 193
    :cond_2
    :goto_0
    return-void

    .line 184
    :cond_3
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 185
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v3, v5

    aput-object v0, v3, v6

    invoke-direct {v2, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/chat/eo;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/chat/eo;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->e()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 191
    const/16 v0, 0x64

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/sec/chaton/chat/eo;->n()Ljava/io/File;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->k()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/chat/eo;->d:I

    iget v3, p0, Lcom/sec/chaton/chat/eo;->e:I

    invoke-static {v1, v0, v2, v3}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->e()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 104
    const-class v1, Lcom/sec/chaton/chat/eo;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/eo;->m:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_1

    .line 110
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->e()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 114
    :goto_1
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/chaton/chat/eo;->a(Ljava/util/concurrent/Callable;J)V

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->e()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020151

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public c()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 148
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/chat/eo;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/eo;->j:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :try_start_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 152
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->k()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/chat/eo;->d:I

    iget v3, p0, Lcom/sec/chaton/chat/eo;->e:I

    invoke-static {v0, v1, v2, v3}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 153
    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setDensity(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    return-object v0

    .line 156
    :catch_0
    move-exception v0

    .line 157
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 209
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/eo;->a(Landroid/view/View;)V

    .line 211
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 214
    :cond_0
    return-void
.end method

.method public e()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 202
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eo;->e()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
