.class public Lcom/sec/chaton/chat/a/u;
.super Lcom/sec/chaton/chat/a/x;
.source "LiveBubbleDrawer.java"


# static fields
.field public static final F:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/chaton/chat/a/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/a/u;->F:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/x;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 14
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->a(Z)V

    .line 16
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/u;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 18
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 22
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, ""

    return-object v0
.end method

.method protected d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 44
    .line 48
    const/4 v2, 0x0

    .line 55
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42de0000    # 111.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    sub-float v4, v0, v1

    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/u;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->al:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->am:Landroid/widget/TextView;

    .line 62
    iget-object v2, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v2, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    .line 63
    iget-object v2, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    .line 79
    :goto_0
    invoke-virtual {v3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    sub-float v3, v4, v3

    .line 82
    if-eqz v2, :cond_3

    .line 83
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    sub-float v2, v3, v2

    .line 87
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/a/u;->a(F)F

    move-result v2

    .line 88
    const/high16 v3, 0x41e80000    # 29.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    sub-float v3, v2, v3

    .line 91
    cmpl-float v4, v2, v6

    if-lez v4, :cond_0

    .line 92
    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 94
    :cond_0
    cmpl-float v1, v3, v6

    if-lez v1, :cond_1

    .line 95
    float-to-int v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 97
    :cond_1
    return-void

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->C:Landroid/widget/TextView;

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->ai:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/chat/a/u;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aj:Landroid/widget/TextView;

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method
