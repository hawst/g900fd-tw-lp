.class public Lcom/sec/chaton/chat/a/ai;
.super Lcom/sec/chaton/chat/a/af;
.source "VideoBubbleDrawer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/af;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f0b004a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 56
    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->a()V

    .line 76
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/ai;->o:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    .line 90
    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 91
    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->w:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    const v1, 0x7f020199

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 117
    :goto_1
    return-void

    .line 93
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/ai;->m:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    .line 106
    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 113
    :cond_1
    new-instance v2, Lcom/sec/chaton/multimedia/image/c;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/ai;->w:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v2, v1, v3, v5, v4}, Lcom/sec/chaton/multimedia/image/c;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 114
    invoke-virtual {v2, v0}, Lcom/sec/chaton/multimedia/image/c;->a(Landroid/view/View;)V

    .line 115
    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->j:Lcom/sec/common/f/c;

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_1
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 121
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/af;->a(Z)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    .line 128
    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    :goto_0
    if-nez p1, :cond_0

    .line 139
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 140
    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->j:Lcom/sec/common/f/c;

    invoke-virtual {v1, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 143
    :cond_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    .line 133
    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->c()Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/sec/chaton/chat/a/n;
    .locals 5

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->w:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :goto_0
    return-object v0

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->w:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 46
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 47
    const-string v0, "onShare() - getTextContent() is null"

    const-string v1, "VideoBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_2
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/ai;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->w:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/af;->onClick(Landroid/view/View;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/ai;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/chat/a/ai;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/ai;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/ai;->e:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/chat/fk;->b(ZLandroid/database/Cursor;)V

    goto :goto_0
.end method
