.class Lcom/sec/chaton/chat/y;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:[Ljava/lang/CharSequence;

.field final synthetic c:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Landroid/widget/CheckBox;[Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 3841
    iput-object p1, p0, Lcom/sec/chaton/chat/y;->c:Lcom/sec/chaton/chat/ChatFragment;

    iput-object p2, p0, Lcom/sec/chaton/chat/y;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/sec/chaton/chat/y;->b:[Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3845
    if-nez p2, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3846
    iget-object v0, p0, Lcom/sec/chaton/chat/y;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/y;->c:Lcom/sec/chaton/chat/ChatFragment;

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3858
    :cond_0
    :goto_0
    return-void

    .line 3850
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/y;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->z(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3854
    iget-object v0, p0, Lcom/sec/chaton/chat/y;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3855
    iget-object v0, p0, Lcom/sec/chaton/chat/y;->b:[Ljava/lang/CharSequence;

    array-length v0, v0

    add-int/2addr p2, v0

    .line 3857
    :cond_2
    new-instance v0, Lcom/sec/chaton/chat/cy;

    iget-object v1, p0, Lcom/sec/chaton/chat/y;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {v0, v1}, Lcom/sec/chaton/chat/cy;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    new-array v1, v3, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/cy;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
