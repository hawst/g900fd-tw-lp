.class final enum Lcom/sec/chaton/chat/notification/y;
.super Ljava/lang/Enum;
.source "ScreenNotification2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/chat/notification/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/chat/notification/y;

.field public static final enum b:Lcom/sec/chaton/chat/notification/y;

.field public static final enum c:Lcom/sec/chaton/chat/notification/y;

.field public static final enum d:Lcom/sec/chaton/chat/notification/y;

.field private static final synthetic e:[Lcom/sec/chaton/chat/notification/y;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 142
    new-instance v0, Lcom/sec/chaton/chat/notification/y;

    const-string v1, "SHOW_LEFTRIGHT"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/chat/notification/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/chat/notification/y;->a:Lcom/sec/chaton/chat/notification/y;

    .line 143
    new-instance v0, Lcom/sec/chaton/chat/notification/y;

    const-string v1, "SHOW_LEFT"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/chat/notification/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/chat/notification/y;->b:Lcom/sec/chaton/chat/notification/y;

    .line 144
    new-instance v0, Lcom/sec/chaton/chat/notification/y;

    const-string v1, "SHOW_RIGHT"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/chat/notification/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/chat/notification/y;->c:Lcom/sec/chaton/chat/notification/y;

    .line 145
    new-instance v0, Lcom/sec/chaton/chat/notification/y;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/chat/notification/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/chat/notification/y;->d:Lcom/sec/chaton/chat/notification/y;

    .line 141
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/chat/notification/y;

    sget-object v1, Lcom/sec/chaton/chat/notification/y;->a:Lcom/sec/chaton/chat/notification/y;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/chat/notification/y;->b:Lcom/sec/chaton/chat/notification/y;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/chat/notification/y;->c:Lcom/sec/chaton/chat/notification/y;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/chat/notification/y;->d:Lcom/sec/chaton/chat/notification/y;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/chat/notification/y;->e:[Lcom/sec/chaton/chat/notification/y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/chat/notification/y;
    .locals 1

    .prologue
    .line 141
    const-class v0, Lcom/sec/chaton/chat/notification/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/y;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/chat/notification/y;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/sec/chaton/chat/notification/y;->e:[Lcom/sec/chaton/chat/notification/y;

    invoke-virtual {v0}, [Lcom/sec/chaton/chat/notification/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/chat/notification/y;

    return-object v0
.end method
