.class public Lcom/sec/chaton/chat/a/f;
.super Lorg/a/a/a/b;
.source "BubbleDrawerFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/a/a/a/b",
        "<",
        "Lcom/sec/chaton/e/w;",
        "Lcom/sec/chaton/chat/a/a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lorg/a/a/a/b;-><init>()V

    .line 89
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/chat/a/a;
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x0

    .line 22
    sget-object v1, Lcom/sec/chaton/chat/a/g;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 92
    :goto_0
    :pswitch_0
    return-object v0

    .line 24
    :pswitch_1
    new-instance v0, Lcom/sec/chaton/chat/a/z;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/z;-><init>()V

    goto :goto_0

    .line 28
    :pswitch_2
    new-instance v0, Lcom/sec/chaton/chat/a/ae;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/ae;-><init>()V

    goto :goto_0

    .line 33
    :pswitch_3
    new-instance v0, Lcom/sec/chaton/chat/a/t;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/t;-><init>()V

    goto :goto_0

    .line 37
    :pswitch_4
    new-instance v0, Lcom/sec/chaton/chat/a/b;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/b;-><init>()V

    goto :goto_0

    .line 41
    :pswitch_5
    new-instance v0, Lcom/sec/chaton/chat/a/r;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/r;-><init>()V

    goto :goto_0

    .line 45
    :pswitch_6
    new-instance v0, Lcom/sec/chaton/chat/a/h;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/h;-><init>()V

    goto :goto_0

    .line 49
    :pswitch_7
    new-instance v0, Lcom/sec/chaton/chat/a/d;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/d;-><init>()V

    goto :goto_0

    .line 53
    :pswitch_8
    new-instance v0, Lcom/sec/chaton/chat/a/ai;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/ai;-><init>()V

    goto :goto_0

    .line 57
    :pswitch_9
    new-instance v0, Lcom/sec/chaton/chat/a/i;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/i;-><init>()V

    goto :goto_0

    .line 62
    :pswitch_a
    new-instance v0, Lcom/sec/chaton/chat/a/q;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/q;-><init>()V

    goto :goto_0

    .line 72
    :pswitch_b
    new-instance v0, Lcom/sec/chaton/chat/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/c;-><init>()V

    goto :goto_0

    .line 76
    :pswitch_c
    new-instance v0, Lcom/sec/chaton/chat/a/w;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/w;-><init>()V

    goto :goto_0

    .line 80
    :pswitch_d
    new-instance v0, Lcom/sec/chaton/chat/a/v;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/v;-><init>()V

    goto :goto_0

    .line 88
    :pswitch_e
    new-instance v0, Lcom/sec/chaton/chat/a/ah;

    invoke-direct {v0}, Lcom/sec/chaton/chat/a/ah;-><init>()V

    goto :goto_0

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcom/sec/chaton/e/w;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/a/f;->a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/chat/a/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/chat/a/a;)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Lorg/a/a/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PASSIVE OBJECT: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/chat/a/f;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/sec/chaton/chat/a/a;->a(Lcom/sec/chaton/chat/fk;)V

    .line 103
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/sec/chaton/chat/a/a;->a(Z)V

    .line 104
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/chaton/e/w;

    check-cast p2, Lcom/sec/chaton/chat/a/a;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/chat/a/f;->a(Lcom/sec/chaton/e/w;Lcom/sec/chaton/chat/a/a;)V

    return-void
.end method
