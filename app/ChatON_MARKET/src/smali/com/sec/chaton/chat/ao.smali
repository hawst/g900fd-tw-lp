.class Lcom/sec/chaton/chat/ao;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 1217
    iput-object p1, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1221
    iget-object v1, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->l(Lcom/sec/chaton/chat/ChatFragment;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1222
    iget-object v1, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1, p2}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;I)V

    .line 1245
    :goto_0
    return v0

    .line 1227
    :cond_0
    const/4 v1, 0x4

    if-eq p2, v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->m(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v1

    if-ne v1, v0, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x42

    if-ne v1, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 1235
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1238
    :cond_2
    const/4 v1, 0x6

    if-ne p2, v1, :cond_3

    .line 1239
    iget-object v1, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1240
    iget-object v2, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/chat/MyEditText;->getLineHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f99999a    # 1.2f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v2, v1, v3}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1241
    iget-object v2, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/chaton/chat/MyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1242
    iget-object v2, p0, Lcom/sec/chaton/chat/ao;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/sec/chaton/chat/MyEditText;->setSelection(I)V

    goto :goto_0

    .line 1245
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
