.class Lcom/sec/chaton/chat/v;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/a;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/a;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 3607
    iput-object p1, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    iput-object p2, p0, Lcom/sec/chaton/chat/v;->a:Lcom/sec/chaton/chat/a;

    iput-object p3, p0, Lcom/sec/chaton/chat/v;->b:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 3610
    .line 3612
    iget-object v0, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->M(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->N(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->O(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 3613
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->z()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->A()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3615
    iget-object v0, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->z()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->c(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 3616
    iget-object v0, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 3617
    iget-object v0, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v2}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    move v0, v2

    .line 3622
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/chaton/chat/v;->a:Lcom/sec/chaton/chat/a;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/a;->a()Z

    move-result v3

    if-nez v3, :cond_b

    :cond_1
    iget-object v3, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/chaton/chat/v;->a:Lcom/sec/chaton/chat/a;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/a;->a()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 3625
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v4, v4, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/v;->a:Lcom/sec/chaton/chat/a;

    invoke-virtual {v5}, Lcom/sec/chaton/chat/a;->a()Z

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/e/a/n;->b(Landroid/content/ContentResolver;Ljava/lang/String;Z)I

    .line 3626
    iget-object v3, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v4, p0, Lcom/sec/chaton/chat/v;->a:Lcom/sec/chaton/chat/a;

    invoke-virtual {v4}, Lcom/sec/chaton/chat/a;->a()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/chaton/chat/ChatFragment;->e(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    move v3, v2

    .line 3630
    :goto_1
    iget-object v4, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->h(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/chaton/chat/v;->b:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_5

    :cond_3
    iget-object v4, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->h(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/chaton/chat/v;->b:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3633
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v5, v5, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/chat/v;->b:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;Z)I

    .line 3634
    iget-object v4, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v5, p0, Lcom/sec/chaton/chat/v;->b:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/chaton/chat/ChatFragment;->f(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 3646
    :cond_5
    iget-object v4, p0, Lcom/sec/chaton/chat/v;->a:Lcom/sec/chaton/chat/a;

    invoke-virtual {v4}, Lcom/sec/chaton/chat/a;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    if-nez v0, :cond_7

    :cond_6
    if-eqz v3, :cond_a

    :cond_7
    move v0, v2

    .line 3659
    :goto_2
    if-eqz v0, :cond_8

    .line 3661
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3662
    iget-object v3, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->g(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 3668
    const-string v3, "%d,%s,%s"

    new-array v4, v8, [Ljava/lang/Object;

    sget-object v5, Lcom/sec/chaton/e/aj;->i:Lcom/sec/chaton/e/aj;

    invoke-virtual {v5}, Lcom/sec/chaton/e/aj;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const-string v5, "TRANSLATE_ON"

    aput-object v5, v4, v2

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->A()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3677
    const-string v3, "%d,%s,%s"

    new-array v4, v8, [Ljava/lang/Object;

    sget-object v5, Lcom/sec/chaton/e/aj;->i:Lcom/sec/chaton/e/aj;

    invoke-virtual {v5}, Lcom/sec/chaton/e/aj;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const-string v1, "TRANSLATE_ON"

    aput-object v1, v4, v2

    invoke-static {}, Lcom/sec/chaton/chat/ChatFragment;->z()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3697
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->P(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v0, v4}, Lcom/sec/chaton/e/a/q;->a(Lcom/sec/chaton/e/a/u;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3706
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/v;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Q(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 3707
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 3708
    return-void

    .line 3687
    :cond_9
    const-string v3, "%d,%s,%s"

    new-array v4, v8, [Ljava/lang/Object;

    sget-object v5, Lcom/sec/chaton/e/aj;->j:Lcom/sec/chaton/e/aj;

    invoke-virtual {v5}, Lcom/sec/chaton/e/aj;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const-string v1, "TRANSLATE_OFF"

    aput-object v1, v4, v2

    const-string v1, "TRANSLATE_OFF"

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_2

    :cond_b
    move v3, v1

    goto/16 :goto_1

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method
