.class public Lcom/sec/chaton/chat/notification/a;
.super Ljava/lang/Object;
.source "ChatONNotificationManager.java"


# static fields
.field public static b:Lcom/sec/chaton/chat/notification/z;

.field public static c:I

.field public static d:I

.field public static e:I

.field public static f:I

.field public static g:I

.field public static h:I

.field public static i:I

.field private static x:Lcom/sec/chaton/chat/notification/a;


# instance fields
.field private A:Lcom/sec/chaton/util/ab;

.field private B:Landroid/app/ActivityManager;

.field private C:Landroid/app/KeyguardManager;

.field private D:Landroid/os/PowerManager;

.field private E:Z

.field private final F:Ljava/lang/Object;

.field private G:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final H:Ljava/lang/String;

.field private final I:Ljava/lang/String;

.field private J:Landroid/os/Handler;

.field public final a:Ljava/lang/String;

.field public j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/chat/notification/i;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/notification/e;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/notification/g;",
            ">;"
        }
    .end annotation
.end field

.field public m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:I

.field u:I

.field private v:I

.field private w:Landroid/content/Context;

.field private y:Landroid/media/MediaPlayer;

.field private z:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/chat/notification/a;->b:Lcom/sec/chaton/chat/notification/z;

    .line 96
    const/4 v0, 0x1

    sput v0, Lcom/sec/chaton/chat/notification/a;->c:I

    .line 97
    const/4 v0, 0x2

    sput v0, Lcom/sec/chaton/chat/notification/a;->d:I

    .line 98
    const/4 v0, 0x3

    sput v0, Lcom/sec/chaton/chat/notification/a;->e:I

    .line 99
    const/4 v0, 0x4

    sput v0, Lcom/sec/chaton/chat/notification/a;->f:I

    .line 103
    const/16 v0, 0xa

    sput v0, Lcom/sec/chaton/chat/notification/a;->g:I

    .line 104
    const/16 v0, 0xb

    sput v0, Lcom/sec/chaton/chat/notification/a;->h:I

    .line 105
    const/16 v0, 0xc

    sput v0, Lcom/sec/chaton/chat/notification/a;->i:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    .line 107
    iput-boolean v1, p0, Lcom/sec/chaton/chat/notification/a;->E:Z

    .line 112
    iput v1, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    .line 114
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->n:I

    .line 115
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->o:I

    .line 116
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->p:I

    .line 117
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->q:I

    .line 118
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->r:I

    .line 119
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->s:I

    .line 120
    const/16 v0, 0x40

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->t:I

    .line 121
    const/16 v0, 0x80

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->u:I

    .line 216
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->F:Ljava/lang/Object;

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->G:Ljava/util/ArrayList;

    .line 301
    const-string v0, "camera_ready"

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->H:Ljava/lang/String;

    .line 302
    const-string v0, "access_control_enabled"

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->I:Ljava/lang/String;

    .line 1697
    new-instance v0, Lcom/sec/chaton/chat/notification/b;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/chat/notification/b;-><init>(Lcom/sec/chaton/chat/notification/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->J:Landroid/os/Handler;

    .line 244
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    .line 246
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->B:Landroid/app/ActivityManager;

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->C:Landroid/app/KeyguardManager;

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->D:Landroid/os/PowerManager;

    .line 254
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->f()V

    .line 255
    return-void
.end method

.method private a(Lcom/sec/chaton/e/w;)I
    .locals 3

    .prologue
    .line 1653
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->n:I

    .line 1654
    sget-object v1, Lcom/sec/chaton/chat/notification/c;->b:[I

    invoke-virtual {p1}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1680
    :goto_0
    return v0

    .line 1656
    :pswitch_0
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->n:I

    goto :goto_0

    .line 1659
    :pswitch_1
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->o:I

    goto :goto_0

    .line 1662
    :pswitch_2
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->p:I

    goto :goto_0

    .line 1665
    :pswitch_3
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->q:I

    goto :goto_0

    .line 1668
    :pswitch_4
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->r:I

    goto :goto_0

    .line 1671
    :pswitch_5
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->s:I

    goto :goto_0

    .line 1674
    :pswitch_6
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->t:I

    goto :goto_0

    .line 1677
    :pswitch_7
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->u:I

    goto :goto_0

    .line 1654
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_7
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;
    .locals 2

    .prologue
    .line 258
    const-class v1, Lcom/sec/chaton/chat/notification/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/chat/notification/a;->x:Lcom/sec/chaton/chat/notification/a;

    if-nez v0, :cond_0

    .line 259
    new-instance v0, Lcom/sec/chaton/chat/notification/a;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/chaton/chat/notification/a;->x:Lcom/sec/chaton/chat/notification/a;

    .line 261
    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/notification/a;->x:Lcom/sec/chaton/chat/notification/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    .line 659
    if-nez p1, :cond_1

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 663
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 665
    if-eqz v0, :cond_0

    .line 667
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    int-to-float v0, v0

    .line 669
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 671
    const v0, 0x3ecccccd    # 0.4f

    .line 672
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeAlertSoundOnCall() - am.getStreamVolume() volume = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    invoke-virtual {p1, v0, v0}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0
.end method

.method private declared-synchronized a(ILcom/sec/chaton/chat/notification/g;Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 892
    monitor-enter p0

    .line 893
    :try_start_0
    sget v1, Lcom/sec/chaton/chat/notification/a;->c:I

    if-eq p1, v1, :cond_0

    sget v1, Lcom/sec/chaton/chat/notification/a;->e:I

    if-ne p1, v1, :cond_3

    .line 895
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    if-nez p2, :cond_2

    .line 897
    :cond_1
    const-string v1, "update with task"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/chaton/e/b/m;

    const/4 v4, 0x0

    invoke-direct {v3, v4, p1, p3}, Lcom/sec/chaton/e/b/m;-><init>(Lcom/sec/chaton/e/b/d;IZ)V

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 909
    :goto_0
    monitor-exit p0

    return v0

    .line 900
    :cond_2
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/notification/a;->b(ILcom/sec/chaton/chat/notification/g;Z)Z

    move-result v0

    goto :goto_0

    .line 905
    :cond_3
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/chaton/e/b/m;

    const/4 v4, 0x0

    invoke-direct {v3, v4, p1, p3}, Lcom/sec/chaton/e/b/m;-><init>(Lcom/sec/chaton/e/b/d;IZ)V

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 906
    const-string v1, "update with task"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 892
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/sec/chaton/chat/notification/g;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1018
    move v1, v2

    .line 1019
    :goto_0
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-ge v1, v0, :cond_2

    .line 1020
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    .line 1021
    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1022
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1023
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 1024
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MsgID has already exist: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    :cond_0
    const/4 v2, 0x1

    .line 1030
    :goto_1
    return v2

    .line 1019
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1029
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method private b(IZ)Landroid/app/Notification;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1034
    .line 1035
    sget v0, Lcom/sec/chaton/chat/notification/a;->c:I

    if-ne p1, v0, :cond_2

    .line 1036
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->c()Z

    move-result v0

    .line 1043
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_3

    .line 1045
    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/notification/a;->c(I)Landroid/app/Notification;

    move-result-object v1

    .line 1050
    :goto_1
    if-eqz p2, :cond_5

    .line 1051
    sget v2, Lcom/sec/chaton/chat/notification/a;->e:I

    if-ne p1, v2, :cond_4

    .line 1052
    iput-object v4, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 1053
    iput-object v4, v1, Landroid/app/Notification;->vibrate:[J

    .line 1054
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_0

    .line 1055
    const-string v2, "[getNotification] It\'s on mute. sound:null, vibrate:null"

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    .line 1067
    iput-object v4, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 1068
    iput-object v4, v1, Landroid/app/Notification;->vibrate:[J

    .line 1071
    :cond_1
    return-object v1

    .line 1038
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1047
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/notification/a;->d(I)Landroid/app/Notification;

    move-result-object v1

    goto :goto_1

    .line 1058
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/app/Notification;)V

    goto :goto_2

    .line 1062
    :cond_5
    iput-object v4, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 1063
    iput-object v4, v1, Landroid/app/Notification;->vibrate:[J

    goto :goto_2
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1075
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1076
    const-string v1, "com.sec.chaton.action.NOTI_WEB"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1077
    const-string v1, "callChatList"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1078
    const-string v1, "inboxNO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1079
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1080
    const-string v1, "fromPush"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1081
    const-string v1, "Content"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1083
    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 862
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 863
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 865
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 866
    return-object v0
.end method

.method private b(ILcom/sec/chaton/chat/notification/g;Z)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 957
    new-instance v0, Lcom/sec/chaton/chat/notification/i;

    iget-object v1, p2, Lcom/sec/chaton/chat/notification/g;->j:Ljava/lang/String;

    iget v2, p2, Lcom/sec/chaton/chat/notification/g;->k:I

    iget-object v3, p2, Lcom/sec/chaton/chat/notification/g;->i:Ljava/lang/String;

    iget v4, p2, Lcom/sec/chaton/chat/notification/g;->h:I

    iget-wide v5, p2, Lcom/sec/chaton/chat/notification/g;->l:J

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/chat/notification/i;-><init>(Ljava/lang/String;ILjava/lang/String;IJ)V

    .line 958
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    iget-object v2, p2, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 960
    iget-object v0, p2, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v1, p2, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 961
    if-ltz v1, :cond_2

    .line 963
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v7

    .line 964
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 965
    if-eq v1, v0, :cond_0

    .line 966
    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 964
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 969
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 970
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 971
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 975
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/chat/notification/e;

    iget-object v2, p2, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v3, p2, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    iget-object v4, p2, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/chaton/chat/notification/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 977
    invoke-direct {p0, p2}, Lcom/sec/chaton/chat/notification/a;->a(Lcom/sec/chaton/chat/notification/g;)Z

    move-result v0

    .line 978
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    .line 982
    if-eqz v0, :cond_3

    .line 989
    :goto_1
    return v0

    .line 986
    :cond_3
    invoke-virtual {p0, p1, p3}, Lcom/sec/chaton/chat/notification/a;->a(IZ)V

    goto :goto_1
.end method

.method private f(I)Z
    .locals 2

    .prologue
    .line 679
    const/4 v0, 0x0

    .line 680
    sget-object v1, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 681
    const/4 v0, 0x1

    .line 684
    :cond_0
    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 274
    const-string v0, "main_icon_badge_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    invoke-static {v1}, Lcom/sec/chaton/util/am;->a(I)V

    .line 276
    invoke-static {}, Lcom/sec/chaton/util/am;->x()V

    .line 278
    :cond_0
    return-void
.end method

.method private k()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 307
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v3, "Camera Preview ON"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v1, :cond_1

    .line 308
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 309
    const-string v0, "camera preview on"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_0
    :goto_0
    return v1

    .line 316
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "camera_ready"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_3

    .line 317
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.app.camera.Camera"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v2, v1, :cond_2

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.app.camera.Camcorder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v1, :cond_3

    .line 318
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[NOTI] Galaxy camera exception. getClassName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :cond_3
    :goto_1
    move v1, v0

    .line 325
    goto :goto_0

    .line 322
    :catch_0
    move-exception v1

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "process for the Galaxy camera "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private l()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 608
    .line 609
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->B:Landroid/app/ActivityManager;

    if-nez v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-string v3, "activity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->B:Landroid/app/ActivityManager;

    .line 612
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->B:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 613
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 614
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 616
    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v5, 0x64

    if-ne v4, v5, :cond_1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 617
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->B:Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 618
    const-string v3, "com.sec.chaton"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-class v3, Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 624
    :goto_0
    return v0

    :cond_2
    move v0, v2

    .line 621
    goto :goto_0

    :cond_3
    move v0, v2

    .line 624
    goto :goto_0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 628
    .line 629
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v2, "Ringtone"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 631
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 632
    const-string v2, "Silent"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 633
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 642
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 643
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 644
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    .line 647
    :cond_1
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    .line 648
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 649
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 650
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 651
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/media/MediaPlayer;)V

    .line 652
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->y:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 656
    :goto_1
    return-void

    .line 638
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 653
    :catch_0
    move-exception v0

    .line 654
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeAlertSound() cated Exception : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private n()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 931
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 932
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 934
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 935
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/e;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/e;->b:Ljava/lang/String;

    .line 936
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 937
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 934
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 941
    :cond_1
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 942
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 943
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 944
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 946
    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 941
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 949
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 950
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method private o()Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1453
    .line 1455
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dormant_switch_onoff"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_5

    .line 1456
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dormant_disable_notifications"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_5

    .line 1457
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dormant_always"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 1487
    :cond_0
    :goto_0
    return v0

    .line 1461
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1462
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 1463
    const/16 v4, 0xc

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 1464
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "dormant_start_hour"

    invoke-static {v2, v5, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 1465
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "dormant_start_min"

    invoke-static {v2, v6, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 1466
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v7, "dormant_end_hour"

    invoke-static {v2, v7, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 1467
    iget-object v7, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "dormant_end_min"

    invoke-static {v7, v8, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    .line 1469
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[NOTI] Blocking mode set time , current hour: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", current minute: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", start hour: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", start minute: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", end hour: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", end minute: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1474
    if-gt v5, v2, :cond_2

    if-ne v5, v2, :cond_3

    if-le v6, v7, :cond_3

    .line 1476
    :cond_2
    add-int/lit8 v2, v2, 0x18

    .line 1479
    :cond_3
    if-lt v5, v3, :cond_4

    if-ne v5, v3, :cond_5

    if-gt v6, v4, :cond_5

    .line 1480
    :cond_4
    if-lt v3, v2, :cond_0

    if-ne v3, v2, :cond_5

    if-lt v4, v7, :cond_0

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method private p()V
    .locals 9

    .prologue
    const-wide/32 v7, 0x5265c00

    const-wide/16 v5, 0x0

    const/4 v4, 0x3

    .line 1497
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd,HH:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1499
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute type"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute repeat"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1502
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v1, v2, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    .line 1503
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "Setting mute hour end Long"

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v3

    .line 1505
    add-long/2addr v1, v7

    .line 1506
    add-long/2addr v3, v7

    .line 1508
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "Setting mute hour start Long"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1509
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "Setting mute hour end Long"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1511
    sget-boolean v5, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v5, :cond_0

    .line 1512
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 1513
    invoke-virtual {v5, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1515
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1516
    const-string v6, "extend >start : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >end : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1518
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[deregiMuteAlert] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    :cond_0
    :goto_0
    return-void

    .line 1522
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 1523
    const-string v0, "[deregiMuteAlert] Deregi mute"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1525
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1526
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour start Long"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1527
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour end Long"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1528
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute repeat"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 914
    const/4 v3, -0x1

    .line 916
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 917
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/e;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v1

    .line 918
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 919
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/e;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/e;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 926
    :goto_2
    return v2

    .line 918
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 916
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v2, v3

    .line 926
    goto :goto_2
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v3, 0x1

    .line 1322
    const-string v0, ""

    .line 1324
    sget-object v1, Lcom/sec/chaton/chat/notification/c;->b:[I

    invoke-static {p1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1403
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1406
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 1407
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1409
    :cond_1
    return-object v0

    .line 1326
    :pswitch_0
    invoke-static {p2}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1329
    :pswitch_1
    invoke-static {p2}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330
    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    :cond_2
    move-object v0, p2

    .line 1333
    goto :goto_0

    .line 1335
    :pswitch_2
    if-eqz p4, :cond_3

    .line 1336
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1338
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0049

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1342
    :pswitch_3
    if-eqz p4, :cond_4

    .line 1343
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0055

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1345
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01cd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1349
    :pswitch_4
    if-eqz p4, :cond_5

    .line 1350
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1352
    :cond_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1356
    :pswitch_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1359
    :pswitch_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1362
    :pswitch_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1365
    :pswitch_8
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1369
    :pswitch_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1372
    :pswitch_a
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b022e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1375
    :pswitch_b
    if-eqz p4, :cond_6

    .line 1376
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0054

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1378
    :cond_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0051

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1383
    :pswitch_c
    invoke-static {p2, v3}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v1

    .line 1384
    if-eqz v1, :cond_0

    .line 1385
    iget-object v0, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 1391
    :pswitch_d
    invoke-static {p2, v3}, Lcom/sec/chaton/specialbuddy/g;->b(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v1

    .line 1392
    if-eqz v1, :cond_0

    .line 1393
    iget-object v0, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 1399
    :pswitch_e
    invoke-static {p2}, Lcom/sec/chaton/specialbuddy/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1324
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public a()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 266
    const-string v0, "main_icon_badge_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(I)V

    .line 268
    invoke-static {}, Lcom/sec/chaton/util/am;->x()V

    .line 270
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 286
    return-void
.end method

.method public a(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 993
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/chat/notification/a;->b(IZ)Landroid/app/Notification;

    move-result-object v1

    .line 1001
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011
    :goto_0
    const-string v0, "main_icon_badge_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(I)V

    .line 1013
    invoke-static {}, Lcom/sec/chaton/util/am;->x()V

    .line 1015
    :cond_0
    return-void

    .line 1002
    :catch_0
    move-exception v0

    .line 1003
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_1

    .line 1004
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1006
    :cond_1
    iget v0, v1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Landroid/app/Notification;->defaults:I

    .line 1007
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public a(Landroid/app/Notification;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 688
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->i()Ljava/lang/String;

    move-result-object v1

    .line 690
    const-string v2, "ALL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MELODY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 695
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v2

    if-nez v2, :cond_4

    .line 696
    :cond_1
    iput-object v0, p1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 713
    :goto_0
    const-string v0, "ALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 714
    new-array v0, v4, [J

    fill-array-data v0, :array_0

    iput-object v0, p1, Landroid/app/Notification;->vibrate:[J

    .line 729
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v1, "LED Indicator"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730
    const v0, -0x5a0ec

    iput v0, p1, Landroid/app/Notification;->ledARGB:I

    .line 731
    const/16 v0, 0x1f4

    iput v0, p1, Landroid/app/Notification;->ledOnMS:I

    .line 732
    const/16 v0, 0x1388

    iput v0, p1, Landroid/app/Notification;->ledOffMS:I

    .line 733
    iget v0, p1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/app/Notification;->flags:I

    .line 736
    :cond_3
    return-void

    .line 699
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v3, "Ringtone"

    invoke-virtual {v2, v3, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 701
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 702
    const-string v3, "Silent"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 703
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 710
    :cond_5
    :goto_2
    iput-object v0, p1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    goto :goto_0

    .line 708
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 718
    :cond_7
    const-string v2, "VIBRATION"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 720
    new-array v1, v4, [J

    fill-array-data v1, :array_1

    iput-object v1, p1, Landroid/app/Notification;->vibrate:[J

    .line 722
    iput-object v0, p1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    goto :goto_1

    .line 723
    :cond_8
    const-string v2, "OFF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 724
    iput-object v0, p1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 725
    iput-object v0, p1, Landroid/app/Notification;->vibrate:[J

    goto :goto_1

    .line 714
    :array_0
    .array-data 8
        0x64
        0x3e8
        0x3e8
    .end array-data

    .line 720
    :array_1
    .array-data 8
        0x64
        0x3e8
        0x3e8
    .end array-data
.end method

.method public a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 775
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 776
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[NOTIPannel] ClearNoti. inbox No: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 781
    :cond_1
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/e/b/m;

    sget v3, Lcom/sec/chaton/chat/notification/a;->f:I

    invoke-direct {v1, v6, v3, v2}, Lcom/sec/chaton/e/b/m;-><init>(Lcom/sec/chaton/e/b/d;IZ)V

    invoke-static {v0, v2, v1}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 856
    :cond_2
    :goto_0
    return-void

    .line 785
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 788
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 789
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/e;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 790
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 788
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 793
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 794
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 795
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 798
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 799
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 800
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 801
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 799
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 804
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 805
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 806
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 809
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    .line 811
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-lez v0, :cond_a

    .line 812
    sget v0, Lcom/sec/chaton/chat/notification/a;->f:I

    invoke-direct {p0, v0, v2}, Lcom/sec/chaton/chat/notification/a;->b(IZ)Landroid/app/Notification;

    move-result-object v0

    .line 813
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 819
    :goto_3
    new-instance v0, Lcom/sec/chaton/sstream/a;

    invoke-direct {v0}, Lcom/sec/chaton/sstream/a;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v1}, Lcom/sec/chaton/sstream/a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 821
    const-string v0, "main_icon_badge_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 822
    const-string v0, "main_icon_badge_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 823
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(I)V

    .line 824
    invoke-static {}, Lcom/sec/chaton/util/am;->x()V

    .line 828
    :cond_8
    sget v0, Lcom/sec/chaton/chat/notification/a;->i:I

    if-eq p2, v0, :cond_2

    .line 834
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/c;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 835
    if-eqz v1, :cond_c

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_c

    .line 836
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 837
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 838
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "packageName"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 839
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 840
    sget v2, Lcom/sec/chaton/chat/notification/a;->h:I

    if-ne p2, v2, :cond_b

    .line 841
    const-string v2, "com.sec.chaton.chat.CHATROOM_CLOSE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 845
    :goto_5
    const-string v2, "inbox_no"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 846
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[B project] send broadcast, inboxNO : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 852
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v1, :cond_9

    .line 853
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 852
    :cond_9
    throw v0

    .line 815
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    goto/16 :goto_3

    .line 843
    :cond_b
    :try_start_2
    const-string v2, "com.sec.chaton.chat.MESSAGE_IS_READ"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 852
    :cond_c
    if-eqz v1, :cond_2

    .line 853
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 852
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_6
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/e/w;ILjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IIJLjava/lang/String;)V
    .locals 24

    .prologue
    .line 427
    .line 428
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x5

    if-le v3, v4, :cond_1a

    .line 429
    const/4 v3, 0x0

    const/4 v4, 0x5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 431
    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p1

    move-object/from16 v8, p6

    invoke-static/range {v3 .. v9}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 445
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v4, "Setting Notification"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/sec/chaton/chat/notification/a;->f(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 602
    :cond_0
    :goto_1
    return-void

    .line 453
    :cond_1
    const-wide/16 v3, 0x0

    cmp-long v3, p11, v3

    if-gtz v3, :cond_19

    .line 454
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 459
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/notification/a;->C:Landroid/app/KeyguardManager;

    invoke-virtual {v5}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 461
    :cond_2
    sget v5, Lcom/sec/chaton/chat/notification/a;->c:I

    .line 462
    const/4 v6, 0x0

    .line 466
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/sec/chaton/chat/notification/a;->f(I)Z

    move-result v23

    .line 467
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/chat/notification/a;->c()Z

    move-result v7

    .line 469
    if-eqz v23, :cond_a

    .line 470
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v5

    const/4 v8, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const/high16 v10, 0x10000000

    invoke-static {v5, v8, v9, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 472
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xb

    if-ge v5, v9, :cond_7

    .line 473
    new-instance v5, Landroid/app/Notification;

    const v9, 0x7f020166

    move-object/from16 v0, p2

    invoke-direct {v5, v9, v0, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 474
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const v9, 0x7f0b0007

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v5, v3, v4, v0, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    move-object v3, v5

    .line 491
    :goto_3
    if-eqz p10, :cond_9

    .line 492
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/app/Notification;)V

    .line 499
    :goto_4
    if-eqz v7, :cond_3

    .line 500
    const/4 v4, 0x0

    iput-object v4, v3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 501
    const/4 v4, 0x0

    iput-object v4, v3, Landroid/app/Notification;->vibrate:[J

    .line 503
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    const/4 v5, -0x1

    invoke-virtual {v4, v5, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    move v4, v6

    .line 573
    :goto_5
    sget-boolean v3, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v3, :cond_4

    .line 574
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isEnableNoti:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p10

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    :cond_4
    const/4 v3, 0x1

    move/from16 v0, p10

    if-eq v0, v3, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/chat/notification/a;->h()Z

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    .line 577
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/am;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 578
    sget-boolean v3, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v3, :cond_6

    .line 579
    const-string v3, "It is on call"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "alertoncall_mode"

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/chaton/chat/notification/a;->v:I

    .line 582
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/chaton/chat/notification/a;->v:I

    if-nez v3, :cond_15

    .line 583
    const-string v3, "[NOTI] ALERTONCALL_OFF"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    :goto_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    const-string v5, "sensor"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    .line 588
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    .line 589
    if-eqz v5, :cond_0

    .line 590
    if-eqz v23, :cond_16

    .line 591
    invoke-static/range {p2 .. p2}, Lcom/sec/chaton/chat/notification/ac;->a(Ljava/lang/String;)Lcom/sec/chaton/chat/notification/ac;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto/16 :goto_1

    .line 476
    :cond_7
    new-instance v5, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-direct {v5, v9}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 477
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const v10, 0x7f0b0007

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v9, 0x7f020166

    invoke-static {v4, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v3

    const v4, 0x7f020166

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 484
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v3, v4, :cond_8

    .line 485
    invoke-virtual {v5}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v3

    goto/16 :goto_3

    .line 487
    :cond_8
    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    goto/16 :goto_3

    .line 495
    :cond_9
    const/4 v4, 0x0

    iput-object v4, v3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 496
    const/4 v4, 0x0

    iput-object v4, v3, Landroid/app/Notification;->vibrate:[J

    goto/16 :goto_4

    .line 507
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/chat/notification/a;->g()Lcom/sec/chaton/chat/notification/d;

    move-result-object v3

    .line 508
    sget-object v4, Lcom/sec/chaton/chat/notification/d;->a:Lcom/sec/chaton/chat/notification/d;

    if-ne v3, v4, :cond_10

    .line 509
    sget-boolean v3, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v3, :cond_b

    .line 510
    const-string v3, "Mute alert. Do not alert"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_b
    sget v3, Lcom/sec/chaton/chat/notification/a;->e:I

    move/from16 v22, v3

    .line 519
    :goto_7
    invoke-static/range {p2 .. p2}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 520
    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 522
    :goto_8
    const-string v3, "\n"

    invoke-virtual {v6, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 524
    const-string v3, "\n+"

    const-string v5, " "

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 525
    new-instance v3, Lcom/sec/chaton/chat/notification/g;

    invoke-static/range {p11 .. p12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual/range {p6 .. p6}, Lcom/sec/chaton/e/w;->a()I

    move-result v11

    const-string v5, "mixed"

    const/4 v7, 0x0

    aget-object v4, v4, v7

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    const/16 v21, 0x1

    :goto_9
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p3

    move/from16 v10, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p13

    move/from16 v14, p14

    move-wide/from16 v15, p16

    move/from16 v17, p15

    move-object/from16 v18, p18

    move-wide/from16 v19, p4

    invoke-direct/range {v3 .. v21}, Lcom/sec/chaton/chat/notification/g;-><init>(Lcom/sec/chaton/chat/notification/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;IJILjava/lang/String;JZ)V

    .line 526
    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, p10

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/chat/notification/a;->a(ILcom/sec/chaton/chat/notification/g;Z)Z

    move-result v4

    .line 528
    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    .line 532
    new-instance v5, Lcom/sec/chaton/sstream/a;

    invoke-direct {v5}, Lcom/sec/chaton/sstream/a;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Lcom/sec/chaton/sstream/a;->a(Ljava/util/ArrayList;)Z

    .line 534
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-direct {v0, v1}, Lcom/sec/chaton/chat/notification/a;->a(Lcom/sec/chaton/e/w;)I

    move-result v13

    .line 536
    const/4 v5, 0x0

    .line 538
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/sec/chaton/e/c;->a:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 539
    if-eqz v5, :cond_13

    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_13

    .line 540
    :cond_c
    :goto_a
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_13

    .line 541
    const-string v7, "messageTypeFlag"

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 543
    const/4 v7, 0x0

    .line 544
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_d

    .line 545
    const/4 v7, 0x2

    invoke-static {v8, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    .line 548
    :cond_d
    if-eqz v7, :cond_e

    and-int v8, v13, v7

    if-ne v8, v13, :cond_c

    .line 549
    :cond_e
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 550
    new-instance v9, Landroid/net/Uri$Builder;

    invoke-direct {v9}, Landroid/net/Uri$Builder;-><init>()V

    const-string v10, "packageName"

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .line 551
    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 552
    const-string v9, "com.sec.chaton.chat.PUSH_RECEIVED"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 553
    const-string v9, "message_id"

    iget-object v10, v3, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 554
    const-string v9, "message_type"

    iget v10, v3, Lcom/sec/chaton/chat/notification/g;->g:I

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 555
    const-string v9, "inbox_no"

    iget-object v10, v3, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 556
    const-string v9, "sender_id"

    iget-object v10, v3, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 557
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[B project] send broadcast, type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v7, v9}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_a

    .line 563
    :catchall_0
    move-exception v3

    move-object v4, v5

    :goto_b
    if-eqz v4, :cond_f

    .line 564
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 563
    :cond_f
    throw v3

    .line 513
    :cond_10
    sget-object v4, Lcom/sec/chaton/chat/notification/d;->c:Lcom/sec/chaton/chat/notification/d;

    if-ne v3, v4, :cond_11

    .line 514
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/chat/notification/a;->p()V

    :cond_11
    move/from16 v22, v5

    goto/16 :goto_7

    .line 525
    :cond_12
    const/16 v21, 0x0

    goto/16 :goto_9

    .line 563
    :cond_13
    if-eqz v5, :cond_14

    .line 564
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_14
    move-object/from16 p2, v6

    .line 566
    goto/16 :goto_5

    .line 585
    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/chat/notification/a;->m()V

    goto/16 :goto_6

    .line 593
    :cond_16
    invoke-static {v4}, Lcom/sec/chaton/chat/notification/ac;->a(Z)Lcom/sec/chaton/chat/notification/ac;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto/16 :goto_1

    .line 597
    :cond_17
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v4}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 563
    :catchall_1
    move-exception v3

    move-object v4, v5

    goto :goto_b

    :cond_18
    move-object/from16 v6, p2

    goto/16 :goto_8

    :cond_19
    move-wide/from16 v3, p11

    goto/16 :goto_2

    :cond_1a
    move-object/from16 v7, p2

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;ZZ)V
    .locals 11

    .prologue
    const/high16 v10, 0x10000000

    const/high16 v9, 0x10000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 331
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->c()Z

    move-result v3

    .line 332
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/a;->k()Z

    move-result v4

    .line 333
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/a;->o()Z

    move-result v5

    .line 337
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v6, "access_control_enabled"

    const/4 v7, 0x0

    invoke-static {v0, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    .line 345
    :goto_0
    sget-boolean v6, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v6, :cond_0

    .line 346
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isEnableNoti: true isWebNoti:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isRecording:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isBlockingMode:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isCameraPreview:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isInteractionControl"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isPopupOpen"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->h()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_0
    iget-object v6, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v7, "Setting show blackscreen popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    if-nez v3, :cond_1

    if-nez v5, :cond_1

    if-nez v0, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    if-eqz p2, :cond_3

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->D:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->C:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/a;->l()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 364
    const-string v0, "keyguard off && foreground -> don\'t display popup"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :cond_3
    :goto_1
    return-void

    .line 340
    :catch_0
    move-exception v0

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "process for the Interaction Control "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/chaton/chat/notification/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v0, v2

    goto/16 :goto_0

    .line 370
    :cond_5
    if-eqz p2, :cond_6

    .line 371
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/chat/notification/ScreenWebNotification;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    const-string v1, "contents"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 374
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 375
    invoke-virtual {v0, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 376
    invoke-virtual {v0, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 377
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 379
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->D:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->h()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting is simple popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 380
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_7

    .line 381
    const-string v0, "Start screen notification Toast"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_7
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-lez v0, :cond_8

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    .line 387
    new-instance v2, Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 388
    new-instance v3, Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 389
    new-instance v1, Ljava/lang/Integer;

    iget v4, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    invoke-direct {v1, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 390
    new-instance v1, Ljava/lang/Boolean;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/notification/g;->o:Z

    invoke-direct {v1, v0}, Ljava/lang/Boolean;-><init>(Z)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 392
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->J:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 393
    new-instance v0, Lcom/sec/chaton/chat/notification/f;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/notification/f;-><init>(Lcom/sec/chaton/chat/notification/a;Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->J:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 396
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_3

    .line 397
    const-string v0, "[EXCEPTION] current unread count is zero. Do not display toast."

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 402
    :cond_9
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-class v4, Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 403
    const-string v3, "isUpdated"

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 404
    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/chaton/util/am;->h(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->D:Landroid/os/PowerManager;

    invoke-virtual {v3}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->C:Landroid/app/KeyguardManager;

    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 406
    :cond_a
    :goto_2
    sget-object v2, Lcom/sec/chaton/chat/notification/ScreenNotification2;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 407
    invoke-virtual {v0, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 408
    invoke-virtual {v0, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 410
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_b

    .line 411
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start screenNotification, isBlack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_b
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_c
    move v1, v2

    .line 404
    goto :goto_2
.end method

.method public a(Ljava/lang/Long;)Z
    .locals 5

    .prologue
    .line 1418
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    .line 1419
    iget-object v2, v0, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1420
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_1

    .line 1421
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Msg ID Exist, msgId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    :cond_1
    const/4 v0, 0x1

    .line 1426
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 220
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    .line 225
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)Z
    .locals 0

    .prologue
    .line 1629
    iput-boolean p1, p0, Lcom/sec/chaton/chat/notification/a;->E:Z

    return p1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->z:Landroid/app/NotificationManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 282
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 888
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/chat/notification/a;->a(ILcom/sec/chaton/chat/notification/g;Z)Z

    .line 889
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 230
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 231
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    :cond_0
    monitor-exit v1

    .line 235
    return-void

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(I)Landroid/app/Notification;
    .locals 9

    .prologue
    const v8, 0x7f020166

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1169
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v3, "OFF"

    const-string v4, "OFF"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1170
    iget-object v3, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v4, "Setting show receive message"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "ON"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 1172
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->e()Lcom/sec/chaton/chat/notification/h;

    move-result-object v4

    .line 1173
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->d()Landroid/content/Intent;

    move-result-object v3

    .line 1174
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, 0x10000000

    invoke-static {v5, v1, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 1177
    sget v3, Lcom/sec/chaton/chat/notification/a;->d:I

    if-eq p1, v3, :cond_1

    sget v3, Lcom/sec/chaton/chat/notification/a;->f:I

    if-ne p1, v3, :cond_3

    .line 1178
    :cond_1
    new-instance v2, Landroid/app/Notification;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {v2, v8, v3, v0, v1}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    move-object v0, v2

    .line 1192
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v4, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    iget-object v3, v4, Lcom/sec/chaton/chat/notification/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 1193
    return-object v0

    :cond_2
    move v0, v1

    .line 1170
    goto :goto_0

    .line 1180
    :cond_3
    const-string v3, ""

    .line 1181
    if-nez v0, :cond_4

    .line 1182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget v3, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v6, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v7, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/notification/g;->o:Z

    invoke-virtual {p0, v3, v6, v7, v0}, Lcom/sec/chaton/chat/notification/a;->a(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1190
    :goto_2
    new-instance v3, Landroid/app/Notification;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {v3, v8, v2, v0, v1}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    move-object v0, v3

    goto :goto_1

    .line 1184
    :cond_4
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v2, :cond_5

    .line 1185
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0b0048

    new-array v2, v2, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v1

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    .line 1187
    :cond_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0047

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_2
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 238
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 240
    monitor-exit v1

    .line 241
    return-void

    .line 240
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 740
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->w:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 745
    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v3, "Video Recording ON"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 770
    :goto_0
    return v1

    .line 750
    :cond_0
    :try_start_0
    const-class v2, Landroid/media/AudioManager;

    const-string v3, "isRecordActive"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 751
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 752
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAudioRecording - "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->C:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    .line 755
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KeyguardManager.inKeyguardRestrictedInputMode() - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3

    .line 766
    :goto_1
    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 767
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 770
    goto :goto_0

    .line 756
    :catch_0
    move-exception v0

    move v0, v1

    move v2, v1

    .line 757
    :goto_3
    const-string v3, "[NOTI]NoSuchMethodException - isRecordActive mehtod is not available"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 762
    :catch_1
    move-exception v0

    move v0, v1

    move v2, v1

    goto :goto_1

    :catch_2
    move-exception v0

    move v0, v1

    goto :goto_1

    :catch_3
    move-exception v3

    goto :goto_1

    .line 760
    :catch_4
    move-exception v0

    move v0, v1

    move v2, v1

    goto :goto_1

    :catch_5
    move-exception v0

    move v0, v1

    goto :goto_1

    :catch_6
    move-exception v3

    goto :goto_1

    .line 758
    :catch_7
    move-exception v0

    move v0, v1

    move v2, v1

    goto :goto_1

    :catch_8
    move-exception v0

    move v0, v1

    goto :goto_1

    :catch_9
    move-exception v3

    goto :goto_1

    .line 756
    :catch_a
    move-exception v0

    move v0, v1

    goto :goto_3

    :catch_b
    move-exception v3

    goto :goto_3

    :cond_1
    move v0, v1

    goto :goto_2
.end method

.method public d(I)Landroid/app/Notification;
    .locals 13

    .prologue
    const v11, 0x7f0b00b4

    const v10, 0x7f020163

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1197
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->e()Lcom/sec/chaton/chat/notification/h;

    move-result-object v5

    .line 1198
    new-instance v6, Landroid/app/Notification$Builder;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 1200
    const-string v0, ""

    .line 1201
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v1, "OFF"

    const-string v3, "OFF"

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1202
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting show receive message"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v1, v3, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v3, v4

    .line 1204
    :goto_0
    if-eqz v3, :cond_4

    .line 1206
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v4, :cond_3

    .line 1207
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0048

    new-array v7, v4, [Ljava/lang/Object;

    iget v8, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v0, v1, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1211
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1229
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/a;->d()Landroid/content/Intent;

    move-result-object v7

    .line 1230
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v8

    const/high16 v9, 0x10000000

    invoke-static {v8, v2, v7, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 1232
    iget-object v8, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    iget-object v9, v5, Lcom/sec/chaton/chat/notification/h;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    iget-object v9, v5, Lcom/sec/chaton/chat/notification/h;->c:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    move-result-object v1

    const v7, 0x7f020166

    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 1240
    sget v1, Lcom/sec/chaton/chat/notification/a;->d:I

    if-eq p1, v1, :cond_1

    sget v1, Lcom/sec/chaton/chat/notification/a;->f:I

    if-eq p1, v1, :cond_1

    .line 1241
    invoke-virtual {v6, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1244
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_8

    .line 1246
    invoke-virtual {v6}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    .line 1296
    :goto_3
    return-object v0

    :cond_2
    move v3, v2

    .line 1202
    goto :goto_0

    .line 1209
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0047

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1215
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget v7, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v8, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v9, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/notification/g;->o:Z

    invoke-virtual {p0, v7, v8, v9, v0}, Lcom/sec/chaton/chat/notification/a;->a(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1216
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 1217
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v7

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/sec/chaton/util/bt;->i(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1218
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v7

    if-eqz v7, :cond_10

    .line 1219
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_6

    .line 1220
    const-string v0, "can not get profile image bitmap"

    iget-object v7, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    :cond_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v7, 0x7f0201bb

    invoke-static {v0, v7}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_2

    .line 1225
    :cond_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_2

    .line 1249
    :cond_8
    new-instance v7, Landroid/app/Notification$InboxStyle;

    invoke-direct {v7, v6}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    .line 1252
    if-nez v3, :cond_c

    .line 1253
    const-string v0, ""

    .line 1255
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/en;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1265
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_a

    .line 1266
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_9

    .line 1267
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    .line 1269
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v7, v0}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    move v1, v2

    .line 1270
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 1271
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    .line 1272
    iget v3, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    iget-object v5, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/notification/g;->o:Z

    invoke-virtual {p0, v3, v5, v6, v0}, Lcom/sec/chaton/chat/notification/a;->a(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 1270
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1275
    :cond_a
    iget-object v0, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    invoke-virtual {v7, v0}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    move v1, v2

    .line 1276
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 1277
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    .line 1278
    iget-object v3, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-ne v3, v4, :cond_b

    .line 1279
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    .line 1281
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    iget-object v6, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    iget-object v8, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/notification/g;->o:Z

    invoke-virtual {p0, v5, v6, v8, v0}, Lcom/sec/chaton/chat/notification/a;->a(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1282
    invoke-virtual {v7, v0}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 1276
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1286
    :cond_c
    iget-object v0, v5, Lcom/sec/chaton/chat/notification/h;->b:Ljava/lang/String;

    invoke-virtual {v7, v0}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 1290
    :cond_d
    const-string v0, ""

    .line 1291
    iget v1, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    add-int/lit8 v1, v1, -0x7

    .line 1292
    if-lez v1, :cond_e

    .line 1293
    if-le v1, v4, :cond_f

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b01a9

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1295
    :cond_e
    :goto_6
    invoke-virtual {v7, v0}, Landroid/app/Notification$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 1296
    invoke-virtual {v7}, Landroid/app/Notification$InboxStyle;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_3

    .line 1293
    :cond_f
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01a8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_10
    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_2
.end method

.method public d()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1087
    .line 1088
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1089
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v1

    .line 1090
    const-string v0, "com.sec.chaton.action.NOTI_MESSAGE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1091
    const-string v0, "callChatList"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1092
    sget-object v2, Lcom/sec/chaton/chat/notification/c;->a:[I

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget v0, v0, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    move-object v0, v1

    .line 1118
    :goto_1
    return-object v0

    .line 1095
    :pswitch_0
    const-string v2, "inboxNO"

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1096
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1097
    const-string v0, "fromPush"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1098
    const-string v2, "buddyNO"

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1101
    :pswitch_1
    const-string v2, "inboxNO"

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1102
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1103
    const-string v0, "fromPush"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1104
    const-string v2, "buddyNO"

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1107
    :pswitch_2
    const-string v2, "inboxNO"

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1108
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1109
    const-string v0, "fromPush"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1110
    const-string v2, "buddyNO"

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 1114
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1115
    const-string v1, "com.sec.chaton.action.NOTI_MESSAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1116
    const-string v1, "callChatTab"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1092
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public d(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1432
    .line 1433
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1434
    const/4 v0, 0x1

    .line 1438
    :goto_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 1439
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IsInboxNO Exist:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1441
    :cond_0
    return v0

    .line 1436
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/sec/chaton/chat/notification/h;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1122
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v1, "OFF"

    const-string v4, "OFF"

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1123
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/a;->A:Lcom/sec/chaton/util/ab;

    const-string v4, "Setting show receive message"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v1, v3

    .line 1124
    :goto_0
    const-string v0, ""

    .line 1126
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 1127
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0b0047

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1132
    :goto_1
    new-instance v5, Lcom/sec/chaton/chat/notification/h;

    invoke-direct {v5}, Lcom/sec/chaton/chat/notification/h;-><init>()V

    .line 1134
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->d:Ljava/lang/Long;

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->c:Ljava/lang/Long;

    .line 1135
    iget-object v0, v5, Lcom/sec/chaton/chat/notification/h;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-gtz v0, :cond_1

    .line 1136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->c:Ljava/lang/Long;

    .line 1139
    :cond_1
    if-eqz v1, :cond_4

    .line 1140
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    .line 1141
    iput-object v4, v5, Lcom/sec/chaton/chat/notification/h;->b:Ljava/lang/String;

    move-object v0, v5

    .line 1164
    :goto_2
    return-object v0

    :cond_2
    move v1, v2

    .line 1123
    goto :goto_0

    .line 1129
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0b0048

    new-array v5, v3, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 1145
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v3, :cond_5

    .line 1147
    iput-object v4, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    .line 1148
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/a;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->b:Ljava/lang/String;

    :goto_3
    move-object v0, v5

    .line 1164
    goto :goto_2

    .line 1151
    :cond_5
    iget v0, p0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v3, :cond_6

    .line 1153
    iput-object v4, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    .line 1154
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/e;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/e;->b:Ljava/lang/String;

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->b:Ljava/lang/String;

    goto :goto_3

    .line 1157
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    .line 1158
    iget-object v0, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1159
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->a:Ljava/lang/String;

    .line 1161
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget v1, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/notification/g;->o:Z

    invoke-virtual {p0, v1, v3, v4, v0}, Lcom/sec/chaton/chat/notification/a;->a(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/sec/chaton/chat/notification/h;->b:Ljava/lang/String;

    goto :goto_3
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1301
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1302
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1304
    if-nez p1, :cond_0

    .line 1305
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/a;->j()V

    .line 1307
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 1311
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    .line 1313
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1314
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    .line 1316
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 1317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    .line 1319
    :cond_2
    return-void
.end method

.method public g()Lcom/sec/chaton/chat/notification/d;
    .locals 17

    .prologue
    .line 1540
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd,HH:mm"

    invoke-direct {v6, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1541
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute type"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1542
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->b:Lcom/sec/chaton/chat/notification/d;

    .line 1544
    if-nez v2, :cond_3

    .line 1545
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    .line 1546
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 1547
    sget-boolean v5, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v5, :cond_0

    .line 1548
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[isAlertMute] endTime :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " curretTime:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    :cond_0
    cmp-long v1, v3, v1

    if-gtz v1, :cond_2

    .line 1552
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->a:Lcom/sec/chaton/chat/notification/d;

    .line 1619
    :cond_1
    :goto_0
    return-object v1

    .line 1554
    :cond_2
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->c:Lcom/sec/chaton/chat/notification/d;

    goto :goto_0

    .line 1558
    :cond_3
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 1559
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1560
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "Setting mute hour end Long"

    const-wide/16 v7, 0x0

    invoke-virtual {v1, v4, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1561
    cmp-long v1, v2, v4

    if-gtz v1, :cond_4

    .line 1562
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->a:Lcom/sec/chaton/chat/notification/d;

    .line 1568
    :goto_1
    sget-boolean v7, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v7, :cond_1

    .line 1569
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1570
    const-string v8, ">current : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " >end : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1572
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[isAlertMute] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ret :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1566
    :cond_4
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->c:Lcom/sec/chaton/chat/notification/d;

    goto :goto_1

    .line 1575
    :cond_5
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1576
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v3

    .line 1577
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 1578
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    const-wide/16 v9, 0x0

    invoke-virtual {v1, v2, v9, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    .line 1580
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v9, "Setting mute repeat"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    const/4 v9, 0x1

    if-ne v5, v9, :cond_7

    .line 1581
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 1582
    invoke-virtual {v5, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1583
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 1584
    invoke-virtual {v9, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1585
    const/4 v10, 0x5

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    const/4 v11, 0x5

    invoke-virtual {v9, v11}, Ljava/util/Calendar;->get(I)I

    move-result v11

    if-ge v10, v11, :cond_7

    .line 1587
    const/4 v10, 0x5

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    const/4 v10, 0x5

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v5

    sub-int v5, v9, v5

    .line 1588
    const-wide/32 v9, 0x5265c00

    int-to-long v11, v5

    mul-long/2addr v9, v11

    add-long/2addr v3, v9

    .line 1589
    const-wide/32 v9, 0x5265c00

    int-to-long v11, v5

    mul-long/2addr v9, v11

    add-long/2addr v1, v9

    .line 1591
    sget-boolean v9, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v9, :cond_6

    .line 1592
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[isAlertMute] increase end time by "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "startTime:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", endTime:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v5, v9}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1595
    :cond_6
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v9, "Setting mute hour start Long"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1596
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v9, "Setting mute hour end Long"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_7
    move-wide v13, v1

    move-wide v15, v3

    move-wide v4, v15

    move-wide v2, v13

    .line 1601
    cmp-long v1, v4, v7

    if-gtz v1, :cond_8

    cmp-long v1, v7, v2

    if-gtz v1, :cond_8

    .line 1602
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->a:Lcom/sec/chaton/chat/notification/d;

    .line 1611
    :goto_2
    sget-boolean v9, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v9, :cond_1

    .line 1612
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1613
    const-string v10, ">start : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " >current : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " >end : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1616
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[isAlertMute] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ret:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/chaton/chat/notification/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1603
    :cond_8
    cmp-long v1, v2, v7

    if-gez v1, :cond_9

    .line 1606
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->c:Lcom/sec/chaton/chat/notification/d;

    goto :goto_2

    .line 1608
    :cond_9
    sget-object v1, Lcom/sec/chaton/chat/notification/d;->b:Lcom/sec/chaton/chat/notification/d;

    goto :goto_2
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 1625
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/a;->E:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1634
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    .line 1635
    const-string v0, "Set Type"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1637
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1638
    const-string v0, "Set Type"

    const-string v1, "ALL"

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1639
    const-string v1, "ALL"

    .line 1640
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xa

    if-le v0, v3, :cond_2

    .line 1641
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v3, "vibrator"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 1642
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1644
    :cond_0
    const-string v0, "Set Type"

    const-string v1, "MELODY"

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1645
    const-string v0, "MELODY"

    .line 1649
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
