.class public Lcom/sec/chaton/chat/ex;
.super Landroid/app/AlertDialog;
.source "EditChatTitleDialog.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/app/ProgressDialog;

.field c:Landroid/os/Handler;

.field private d:I

.field private e:Lcom/sec/chaton/chat/fc;

.field private f:Lcom/sec/chaton/widget/ClearableEditText;

.field private g:Landroid/content/Context;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/chaton/chat/ex;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ex;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/chat/fc;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 40
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/chaton/chat/ex;->d:I

    .line 74
    new-instance v0, Lcom/sec/chaton/chat/ey;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ey;-><init>(Lcom/sec/chaton/chat/ex;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ex;->c:Landroid/os/Handler;

    .line 203
    new-instance v0, Lcom/sec/chaton/chat/fb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/fb;-><init>(Lcom/sec/chaton/chat/ex;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ex;->l:Landroid/text/TextWatcher;

    .line 60
    iput-object p1, p0, Lcom/sec/chaton/chat/ex;->e:Lcom/sec/chaton/chat/fc;

    .line 61
    iput-object p2, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    .line 62
    iput-object p3, p0, Lcom/sec/chaton/chat/ex;->i:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Lcom/sec/chaton/chat/ex;->j:Ljava/lang/String;

    .line 64
    iput-object p5, p0, Lcom/sec/chaton/chat/ex;->k:Ljava/lang/String;

    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/chat/ex;->a()V

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ex;->setInverseBackgroundForced(Z)V

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    .line 70
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ex;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/chaton/chat/ex;->h:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, -0x2

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    const v4, 0x7f0b01aa

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ex;->setTitle(Ljava/lang/CharSequence;)V

    .line 134
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ex;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 135
    const v4, 0x7f03001b

    invoke-virtual {v0, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 136
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ex;->setView(Landroid/view/View;)V

    .line 138
    const v4, 0x7f070075

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    new-instance v6, Lcom/sec/chaton/util/w;

    iget-object v7, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    iget v8, p0, Lcom/sec/chaton/chat/ex;->d:I

    invoke-direct {v6, v7, v8}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v6, v4, v5

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v4, ""

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v4, p0, Lcom/sec/chaton/chat/ex;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v4, Lcom/sec/chaton/chat/ez;

    invoke-direct {v4, p0}, Lcom/sec/chaton/chat/ez;-><init>(Lcom/sec/chaton/chat/ex;)V

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ex;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 176
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_0

    .line 182
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    const v4, 0x7f0b0037

    invoke-virtual {v0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v4, Lcom/sec/chaton/chat/fa;

    invoke-direct {v4, p0}, Lcom/sec/chaton/chat/fa;-><init>(Lcom/sec/chaton/chat/ex;)V

    invoke-virtual {p0, v3, v0, v4}, Lcom/sec/chaton/chat/ex;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    const v3, 0x7f0b003a

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v0, v1

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v2, v3, v0}, Lcom/sec/chaton/chat/ex;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 201
    return-void

    :cond_0
    move v9, v3

    move v3, v2

    move v2, v9

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ex;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/chaton/chat/ex;->b()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ex;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 241
    const-string v1, "inbox_title"

    iget-object v2, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v2}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const-string v1, "inbox_title_fixed"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v1, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inbox_no=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/ex;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 246
    if-lez v0, :cond_1

    .line 247
    new-instance v0, Lcom/sec/widget/ai;

    iget-object v1, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/widget/ai;-><init>(Landroid/content/Context;)V

    .line 248
    const v1, 0x7f0b0089

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 249
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ex;->i:Ljava/lang/String;

    .line 256
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ex;->dismiss()V

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->e:Lcom/sec/chaton/chat/fc;

    iget-object v1, p0, Lcom/sec/chaton/chat/ex;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/chaton/chat/fc;->c(Ljava/lang/String;)V

    .line 258
    return-void

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->g:Landroid/content/Context;

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ex;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/16 v3, 0x1e

    const/4 v2, 0x0

    .line 106
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 109
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 110
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ex;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 116
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/ex;->setIcon(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->i:Ljava/lang/String;

    .line 119
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_0

    .line 120
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 126
    iget-object v1, p0, Lcom/sec/chaton/chat/ex;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/ClearableEditText;->setSelection(I)V

    .line 128
    return-void

    .line 113
    :cond_1
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ex;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public show()V
    .locals 0

    .prologue
    .line 263
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    .line 264
    return-void
.end method
