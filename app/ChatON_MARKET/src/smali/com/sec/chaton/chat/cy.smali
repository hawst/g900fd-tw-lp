.class Lcom/sec/chaton/chat/cy;
.super Landroid/os/AsyncTask;
.source "ChatFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/StringBuilder;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Z

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:[Ljava/io/File;

.field i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field j:I

.field k:Ljava/lang/String;

.field l:[B

.field final synthetic m:Lcom/sec/chaton/chat/ChatFragment;

.field private n:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 1

    .prologue
    .line 3973
    iput-object p1, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 3974
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->a:Ljava/lang/StringBuilder;

    .line 3982
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->i:Ljava/util/ArrayList;

    .line 3990
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->n:Ljava/lang/String;

    .line 3992
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->l:[B

    .line 4241
    return-void

    .line 3992
    nop

    :array_0
    .array-data 1
        -0x11t
        -0x45t
        -0x41t
    .end array-data
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 4571
    const-string v2, ""

    .line 4572
    const-string v0, ";"

    invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 4573
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 4574
    array-length v0, v4

    if-lez v0, :cond_a

    .line 4575
    sget-object v0, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    .line 4576
    array-length v6, v4

    move v1, v3

    :goto_0
    if-ge v1, v6, :cond_1

    aget-object v7, v4, v1

    .line 4577
    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 4578
    array-length v8, v7

    if-le v8, v11, :cond_0

    .line 4579
    aget-object v0, v7, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/aj;->a(I)Lcom/sec/chaton/e/aj;

    move-result-object v0

    .line 4581
    aget-object v7, v7, v11

    const/16 v8, 0xa

    const/16 v9, 0x20

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4576
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4585
    :cond_1
    const-string v1, ""

    .line 4586
    sget-object v4, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    if-ne v0, v4, :cond_4

    .line 4589
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4590
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b00e5

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4608
    :cond_2
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_b

    .line 4612
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-le v2, v10, :cond_9

    .line 4613
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    sget-object v2, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    if-ne v0, v2, :cond_8

    .line 4614
    new-array v0, v11, [Ljava/lang/Object;

    aput-object p2, v0, v3

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v5, v3, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v10

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4627
    :goto_2
    return-object v0

    .line 4592
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0198

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 4595
    :cond_4
    sget-object v4, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    if-ne v0, v4, :cond_5

    .line 4596
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b005e

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 4597
    :cond_5
    sget-object v4, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    if-ne v0, v4, :cond_6

    .line 4598
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b00a7

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 4599
    :cond_6
    sget-object v4, Lcom/sec/chaton/e/aj;->e:Lcom/sec/chaton/e/aj;

    if-ne v0, v4, :cond_7

    .line 4600
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0178

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 4603
    :cond_7
    sget-object v4, Lcom/sec/chaton/e/aj;->f:Lcom/sec/chaton/e/aj;

    if-ne v0, v4, :cond_2

    .line 4604
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b01be

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 4616
    :cond_8
    new-array v0, v10, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v5, v3, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 4619
    :cond_9
    new-array v0, v10, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b00b4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_a
    move-object v0, p3

    .line 4624
    goto :goto_2

    :cond_b
    move-object v0, v2

    goto :goto_2
.end method


# virtual methods
.method public a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 4553
    const-string v0, "\r\n"

    .line 4554
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4555
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 4557
    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->n:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->n:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 4559
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4562
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4563
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4565
    iput-object v2, p0, Lcom/sec/chaton/chat/cy;->n:Ljava/lang/String;

    .line 4567
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/File;)V
    .locals 7

    .prologue
    const v6, 0x7f0b0039

    const v5, 0x7f0b0037

    const/4 v4, 0x1

    .line 4101
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0xa00000

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 4102
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 4103
    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 4104
    const v1, 0x7f0b01ec

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 4106
    new-instance v1, Lcom/sec/chaton/chat/cz;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/chat/cz;-><init>(Lcom/sec/chaton/chat/cy;Ljava/io/File;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4112
    new-instance v1, Lcom/sec/chaton/chat/da;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/da;-><init>(Lcom/sec/chaton/chat/cy;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4118
    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    .line 4119
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 4141
    :goto_0
    return-void

    .line 4121
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 4122
    const v1, 0x7f0b024b

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 4123
    const v1, 0x7f0b024c

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 4124
    new-instance v1, Lcom/sec/chaton/chat/db;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/chat/db;-><init>(Lcom/sec/chaton/chat/cy;Ljava/io/File;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4131
    new-instance v1, Lcom/sec/chaton/chat/dc;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/dc;-><init>(Lcom/sec/chaton/chat/cy;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4138
    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    .line 4139
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method public a(Ljava/io/File;Ljava/io/File;)V
    .locals 10

    .prologue
    const v9, 0x7f0b0414

    const v8, 0x7f0b01ed

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 4146
    .line 4148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4149
    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    const v4, 0x7f0b0007

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->S(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4150
    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v7, :cond_0

    .line 4151
    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4153
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4154
    if-eqz p2, :cond_5

    .line 4155
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->i:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4156
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    .line 4157
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    new-instance v3, Lcom/sec/chaton/chat/df;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/df;-><init>(Lcom/sec/chaton/chat/cy;)V

    invoke-static {v0, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 4159
    invoke-virtual {p0, p2}, Lcom/sec/chaton/chat/cy;->c(Ljava/io/File;)J

    move-result-wide v3

    .line 4160
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v3, v5

    const-wide/32 v5, 0xa00000

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    .line 4161
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 4162
    const v1, 0x7f0b01ea

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 4163
    const v1, 0x7f0b01ec

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 4165
    const v1, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/chat/dd;

    invoke-direct {v3, p0, p1, v2}, Lcom/sec/chaton/chat/dd;-><init>(Lcom/sec/chaton/chat/cy;Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4194
    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/chat/de;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/de;-><init>(Lcom/sec/chaton/chat/cy;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 4200
    invoke-virtual {v0, v7}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    .line 4201
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 4239
    :cond_1
    :goto_0
    return-void

    :cond_2
    move v0, v1

    .line 4203
    :goto_1
    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 4204
    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 4205
    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->i:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    aget-object v4, v4, v0

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4203
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4208
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4209
    const-string v3, "plain/text"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 4210
    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4211
    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/chat/cy;->b:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v3, v8, v4}, Lcom/sec/chaton/chat/ChatFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4212
    const-string v2, "android.intent.extra.STREAM"

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 4215
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    const/16 v3, 0x17

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4216
    :catch_0
    move-exception v0

    .line 4217
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v9, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 4218
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 4219
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 4224
    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4225
    const-string v3, "plain/text"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 4226
    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4227
    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/chat/cy;->b:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v3, v8, v4}, Lcom/sec/chaton/chat/ChatFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4228
    const-string v2, "android.intent.extra.STREAM"

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4231
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    const/16 v3, 0x17

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 4232
    :catch_1
    move-exception v0

    .line 4233
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v9, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 4234
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 4235
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected a(Ljava/lang/Long;)V
    .locals 6

    .prologue
    const/high16 v5, 0xa00000

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 3997
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/cy;->d:Z

    .line 3998
    iget-boolean v0, p0, Lcom/sec/chaton/chat/cy;->d:Z

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->e:Ljava/lang/String;

    .line 3999
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/tmp"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->f:Ljava/lang/String;

    .line 4000
    iget-boolean v0, p0, Lcom/sec/chaton/chat/cy;->d:Z

    if-eqz v0, :cond_0

    .line 4001
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/ChatON/Chats/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->g:Ljava/lang/String;

    .line 4003
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, v2, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->k:Ljava/lang/String;

    .line 4007
    :try_start_0
    iget v0, p0, Lcom/sec/chaton/chat/cy;->j:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/chaton/chat/cy;->j:I

    if-ne v0, v4, :cond_7

    .line 4013
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->g:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4014
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4015
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 4018
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4020
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4021
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->l:[B

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 4022
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 4024
    iget v0, p0, Lcom/sec/chaton/chat/cy;->j:I

    if-ne v0, v4, :cond_3

    .line 4025
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/chat/cy;->k:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4026
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4027
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/cy;->b(Ljava/io/File;)V

    .line 4032
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b00e6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/chat/cy;->g:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/...)"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4086
    :cond_4
    :goto_1
    if-eqz v2, :cond_5

    .line 4087
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 4094
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4096
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 4097
    return-void

    .line 3998
    :cond_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 4035
    :cond_7
    :try_start_3
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->f:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4036
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_8

    .line 4037
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 4040
    :cond_8
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->b:Ljava/lang/String;

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4042
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4043
    :try_start_4
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->l:[B

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 4044
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 4045
    array-length v4, v0

    if-le v4, v5, :cond_b

    .line 4047
    const/4 v4, 0x0

    const/high16 v5, 0xa00000

    invoke-virtual {v2, v0, v4, v5}, Ljava/io/FileOutputStream;->write([BII)V

    .line 4052
    :goto_4
    iget v0, p0, Lcom/sec/chaton/chat/cy;->j:I

    const/4 v4, 0x1

    if-ne v0, v4, :cond_e

    .line 4065
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/sec/chaton/chat/cy;->a(Ljava/io/File;Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 4076
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 4077
    :goto_5
    :try_start_5
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_9

    .line 4078
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 4086
    :cond_9
    if-eqz v1, :cond_a

    .line 4087
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 4094
    :cond_a
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    goto :goto_3

    .line 4049
    :cond_b
    :try_start_7
    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 4080
    :catch_1
    move-exception v0

    .line 4081
    :goto_7
    :try_start_8
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_c

    .line 4082
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 4086
    :cond_c
    if-eqz v2, :cond_d

    .line 4087
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 4094
    :cond_d
    :goto_8
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    goto/16 :goto_3

    .line 4066
    :cond_e
    :try_start_a
    iget v0, p0, Lcom/sec/chaton/chat/cy;->j:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_10

    .line 4067
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/chaton/chat/cy;->k:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4068
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_11

    .line 4071
    :goto_9
    invoke-virtual {p0, v3, v1}, Lcom/sec/chaton/chat/cy;->a(Ljava/io/File;Ljava/io/File;)V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_1

    .line 4085
    :catchall_0
    move-exception v0

    .line 4086
    :goto_a
    if-eqz v2, :cond_f

    .line 4087
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    .line 4094
    :cond_f
    :goto_b
    iget-object v1, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4085
    throw v0

    .line 4072
    :cond_10
    :try_start_c
    iget v0, p0, Lcom/sec/chaton/chat/cy;->j:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 4073
    invoke-virtual {p0, v3}, Lcom/sec/chaton/chat/cy;->a(Ljava/io/File;)V
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_1

    .line 4089
    :catch_2
    move-exception v1

    .line 4090
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_f

    .line 4091
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_b

    .line 4089
    :catch_3
    move-exception v0

    .line 4090
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_a

    .line 4091
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_6

    .line 4089
    :catch_4
    move-exception v0

    .line 4090
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_d

    .line 4091
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_8

    .line 4089
    :catch_5
    move-exception v0

    .line 4090
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_5

    .line 4091
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 4085
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_a

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_a

    .line 4080
    :catch_6
    move-exception v0

    move-object v2, v1

    goto :goto_7

    .line 4076
    :catch_7
    move-exception v0

    goto/16 :goto_5

    :catch_8
    move-exception v0

    move-object v1, v2

    goto/16 :goto_5

    :cond_11
    move-object v1, v0

    goto :goto_9
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4355
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    aget-object v1, p1, v2

    aput-object v1, v0, v2

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 4356
    return-void
.end method

.method protected varargs b([Ljava/lang/Integer;)Ljava/lang/Long;
    .locals 13

    .prologue
    .line 4368
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/cy;->j:I

    .line 4369
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/e/v;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "message_inbox_no=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v6, v6, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v6, v4, v5

    const-string v5, "message_time , _id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 4371
    if-eqz v5, :cond_0

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 4372
    :cond_0
    const/4 v0, 0x0

    .line 4529
    :goto_0
    return-object v0

    .line 4376
    :cond_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    .line 4393
    const-string v0, ""

    .line 4395
    const-string v6, "\r\n"

    .line 4398
    :cond_2
    const-string v0, "message_time"

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 4399
    const-string v0, "message_content"

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4400
    const-string v2, "message_sender"

    invoke-interface {v5, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v5, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 4402
    const-string v3, "message_content_type"

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v7

    .line 4405
    const-string v3, "buddy_no"

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 4406
    const-string v3, "buddy_name"

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 4409
    const-string v3, "message_formatted"

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 4411
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {p0, v10, v11}, Lcom/sec/chaton/chat/cy;->a(J)Ljava/lang/String;

    move-result-object v10

    .line 4413
    const/4 v1, 0x1

    .line 4414
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v11, "chaton_id"

    const-string v12, ""

    invoke-virtual {v3, v11, v12}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 4415
    const/4 v1, 0x0

    .line 4417
    :cond_3
    if-eqz v1, :cond_6

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0094

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 4419
    :goto_1
    invoke-static {v7}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v2

    .line 4421
    sget-object v1, Lcom/sec/chaton/chat/cp;->a:[I

    invoke-virtual {v7}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v11

    aget v1, v1, v11

    packed-switch v1, :pswitch_data_0

    .line 4519
    :cond_4
    :goto_2
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4520
    sget-object v0, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    if-eq v7, v0, :cond_5

    .line 4521
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4523
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4525
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4527
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 4529
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    move-object v3, v4

    .line 4417
    goto :goto_1

    .line 4423
    :pswitch_1
    invoke-direct {p0, v8, v4, v0}, Lcom/sec/chaton/chat/cy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :pswitch_2
    move-object v2, v0

    .line 4427
    goto :goto_2

    .line 4462
    :pswitch_3
    const/4 v1, 0x0

    .line 4463
    if-eqz v0, :cond_a

    .line 4464
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 4467
    :goto_3
    if-eqz v4, :cond_4

    array-length v0, v4

    const/4 v1, 0x6

    if-le v0, v1, :cond_4

    .line 4468
    const/4 v1, 0x0

    .line 4469
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    .line 4470
    const/4 v0, 0x6

    :goto_4
    array-length v9, v4

    if-ge v0, v9, :cond_7

    .line 4471
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v11, v4, v0

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, "\n"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4470
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 4473
    :cond_7
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 4474
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 4476
    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 4477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_6
    move-object v2, v0

    .line 4479
    goto/16 :goto_2

    .line 4513
    :pswitch_4
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 4514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_8
    move-object v0, v2

    goto :goto_6

    :cond_9
    move-object v0, v1

    goto :goto_5

    :cond_a
    move-object v4, v1

    goto/16 :goto_3

    .line 4421
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public b(Ljava/io/File;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 4258
    .line 4263
    const/16 v0, 0x400

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 4264
    const-string v0, ""

    .line 4265
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 4266
    if-nez v7, :cond_1

    .line 4314
    :cond_0
    return-void

    .line 4269
    :cond_1
    const/4 v0, 0x0

    move v5, v0

    move-object v3, v1

    move-object v2, v1

    move-object v0, v1

    :goto_0
    array-length v4, v7

    if-ge v5, v4, :cond_0

    .line 4270
    aget-object v4, v7, v5

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 4272
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/chaton/chat/cy;->g:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v8, v7, v5

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 4274
    new-instance v4, Ljava/io/FileInputStream;

    aget-object v9, v7, v5

    invoke-direct {v4, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 4275
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 4276
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 4277
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 4280
    :goto_1
    invoke-virtual {v1, v6}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    const/4 v8, -0x1

    if-ne v3, v8, :cond_7

    .line 4294
    if-eqz v1, :cond_2

    .line 4295
    :try_start_3
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 4297
    :cond_2
    if-eqz v0, :cond_3

    .line 4298
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 4300
    :cond_3
    if-eqz v4, :cond_4

    .line 4301
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 4303
    :cond_4
    if-eqz v2, :cond_5

    .line 4304
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_5
    :goto_2
    move-object v3, v4

    .line 4269
    :cond_6
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v10, v2

    move-object v2, v3

    move-object v3, v10

    goto :goto_0

    .line 4284
    :cond_7
    :try_start_4
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 4285
    invoke-virtual {v0, v6}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 4286
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 4288
    :catch_0
    move-exception v3

    move-object v10, v3

    move-object v3, v4

    move-object v4, v10

    .line 4289
    :goto_4
    :try_start_5
    sget-boolean v8, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v8, :cond_8

    .line 4290
    sget-object v8, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v4, v8}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 4294
    :cond_8
    if-eqz v1, :cond_9

    .line 4295
    :try_start_6
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 4297
    :cond_9
    if-eqz v0, :cond_a

    .line 4298
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 4300
    :cond_a
    if-eqz v3, :cond_b

    .line 4301
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 4303
    :cond_b
    if-eqz v2, :cond_6

    .line 4304
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    .line 4306
    :catch_1
    move-exception v4

    .line 4307
    sget-boolean v8, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v8, :cond_6

    .line 4308
    sget-object v8, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v4, v8}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 4293
    :catchall_0
    move-exception v3

    move-object v10, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v10

    .line 4294
    :goto_5
    if-eqz v2, :cond_c

    .line 4295
    :try_start_7
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 4297
    :cond_c
    if-eqz v1, :cond_d

    .line 4298
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 4300
    :cond_d
    if-eqz v4, :cond_e

    .line 4301
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 4303
    :cond_e
    if-eqz v3, :cond_f

    .line 4304
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 4293
    :cond_f
    :goto_6
    throw v0

    .line 4306
    :catch_2
    move-exception v1

    .line 4307
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_f

    .line 4308
    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_6

    .line 4306
    :catch_3
    move-exception v3

    .line 4307
    sget-boolean v8, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v8, :cond_5

    .line 4308
    sget-object v8, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 4293
    :catchall_1
    move-exception v4

    move-object v10, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_5

    :catchall_2
    move-exception v4

    move-object v10, v4

    move-object v4, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_5

    :catchall_3
    move-exception v2

    move-object v10, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_5

    .line 4288
    :catch_4
    move-exception v4

    move-object v10, v3

    move-object v3, v2

    move-object v2, v10

    goto :goto_4

    :catch_5
    move-exception v2

    move-object v10, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v10

    goto :goto_4

    :cond_10
    move-object v10, v3

    move-object v3, v2

    move-object v2, v10

    goto/16 :goto_3
.end method

.method public c(Ljava/io/File;)J
    .locals 6

    .prologue
    const-wide/16 v1, 0x0

    .line 4533
    .line 4534
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 4535
    if-nez v3, :cond_1

    .line 4548
    :cond_0
    return-wide v1

    .line 4538
    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 4540
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4538
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4545
    :cond_2
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v1, v4

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3973
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/cy;->b([Ljava/lang/Integer;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 4361
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4362
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 4363
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3973
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/cy;->a(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    const v5, 0x7f0b0007

    .line 4319
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 4323
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4324
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->n:Ljava/lang/String;

    .line 4327
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 4328
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMddHHmm"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 4329
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v4, v5}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".txt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/chat/cy;->b:Ljava/lang/String;

    .line 4331
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMddHHmmss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4332
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v2, v5}, Lcom/sec/chaton/chat/ChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/cy;->c:Ljava/lang/String;

    .line 4348
    iget-object v0, p0, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->R(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 4349
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 4350
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3973
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/cy;->a([Ljava/lang/Integer;)V

    return-void
.end method
