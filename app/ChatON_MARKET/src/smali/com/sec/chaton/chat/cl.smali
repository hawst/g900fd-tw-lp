.class Lcom/sec/chaton/chat/cl;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 1362
    iput-object p1, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 1370
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1372
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1373
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setSelected(Z)V

    .line 1381
    :cond_0
    if-nez p2, :cond_1

    add-int/lit8 v0, p4, -0x1

    if-lez v0, :cond_1

    .line 1382
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->s(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1383
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->b(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 1386
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->u(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1387
    const-string v0, "onScroll - QUERY_MESSAGE_LOAD_MORE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1388
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/16 v1, 0xe

    iget-object v3, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->v(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/v;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v9, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v4, v4, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    aput-object v4, v6, v8

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v9}, Lcom/sec/chaton/chat/ChatFragment;->c(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 1401
    :cond_1
    :goto_0
    add-int/lit8 v0, p2, 0x1

    add-int/2addr v0, p3

    if-le v0, p4, :cond_4

    .line 1409
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->k()V

    .line 1419
    :cond_2
    :goto_1
    return-void

    .line 1395
    :cond_3
    const-string v0, "onScroll - request message from server"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1396
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->w(Lcom/sec/chaton/chat/ChatFragment;)V

    goto :goto_0

    .line 1412
    :cond_4
    if-eqz p3, :cond_2

    .line 1415
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v8, v0, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    .line 1416
    iget-object v0, p0, Lcom/sec/chaton/chat/cl;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    goto :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1365
    return-void
.end method
