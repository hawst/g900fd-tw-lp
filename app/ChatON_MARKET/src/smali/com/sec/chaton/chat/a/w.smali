.class public Lcom/sec/chaton/chat/a/w;
.super Lcom/sec/chaton/chat/a/u;
.source "LiveShareBubbleDrawer.java"

# interfaces
.implements Lcom/sec/chaton/chat/a/k;
.implements Lcom/sec/chaton/chat/a/m;


# static fields
.field public static final G:Ljava/lang/String;


# instance fields
.field H:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

.field I:Z

.field J:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/chaton/chat/a/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/a/w;->G:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/u;-><init>()V

    .line 20
    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->I:Z

    .line 21
    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->J:Z

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/u;->a(Z)V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/w;->H:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    .line 28
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/w;->I:Z

    .line 29
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/w;->J:Z

    .line 30
    return-void
.end method

.method protected b_()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    const/4 v6, 0x0

    .line 38
    invoke-super {p0}, Lcom/sec/chaton/chat/a/u;->b_()V

    .line 40
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->k()Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->m()Ljava/lang/String;

    move-result-object v4

    .line 47
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 48
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 50
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 53
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 58
    :cond_3
    iget v5, p0, Lcom/sec/chaton/chat/a/w;->i:F

    .line 62
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->al:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->am:Landroid/widget/TextView;

    .line 71
    :goto_1
    invoke-virtual {v2, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 73
    invoke-virtual {v1, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 74
    invoke-virtual {v0, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 76
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 67
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->ai:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aj:Landroid/widget/TextView;

    goto :goto_1
.end method

.method f()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/w;->H:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->H:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    if-eqz v0, :cond_0

    .line 89
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/w;->J:Z

    .line 90
    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/w;->I:Z

    .line 96
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->J:Z

    return v0

    .line 92
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->J:Z

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/chat/a/n;
    .locals 4

    .prologue
    .line 172
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/w;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/w;->p:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 177
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->k()Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return v0

    .line 182
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->m()Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 5

    .prologue
    const v3, 0x7f0b00b4

    .line 192
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->k()Ljava/lang/String;

    move-result-object v0

    .line 193
    if-nez v0, :cond_1

    .line 194
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "onCopy() - title is null"

    const-string v1, "LiveShareBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->m()Ljava/lang/String;

    move-result-object v1

    .line 200
    if-nez v1, :cond_3

    .line 201
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_2

    .line 202
    const-string v1, "onCopy() - name is null"

    const-string v2, "LiveShareBubbleDrawer"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 206
    :cond_3
    const-string v2, "%s\n%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 207
    return-object v0
.end method

.method k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->I:Z

    if-nez v0, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->f()Z

    .line 104
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->J:Z

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->H:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    .line 108
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->I:Z

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->f()Z

    .line 116
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/w;->J:Z

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->H:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->name:Ljava/lang/String;

    .line 120
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/u;->onClick(Landroid/view/View;)V

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ak:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ah:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/w;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/chat/a/w;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/w;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/w;->e:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/chat/fk;->e(ZLandroid/database/Cursor;)V

    goto :goto_0
.end method
