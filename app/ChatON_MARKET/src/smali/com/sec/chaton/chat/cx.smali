.class Lcom/sec/chaton/chat/cx;
.super Landroid/os/AsyncTask;
.source "ChatFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field b:Z

.field final synthetic c:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 1

    .prologue
    .line 13538
    iput-object p1, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 13539
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/cx;->a:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/c;)V
    .locals 0

    .prologue
    .line 13538
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/cx;-><init>(Lcom/sec/chaton/chat/ChatFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13564
    iget-boolean v0, p0, Lcom/sec/chaton/chat/cx;->b:Z

    if-eqz v0, :cond_2

    .line 13566
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ac(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 13567
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ac(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    .line 13572
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1, v0, v3, v4}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;ZZ)Ljava/io/File;

    move-result-object v0

    .line 13573
    if-eqz v0, :cond_0

    .line 13574
    iget-object v1, p0, Lcom/sec/chaton/chat/cx;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 13592
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 13569
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ac(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 13578
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ac(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    .line 13580
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 13581
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    .line 13586
    :goto_2
    iget-object v2, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2, v0, v3, v4}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;ZZ)Ljava/io/File;

    move-result-object v0

    .line 13587
    if-eqz v0, :cond_3

    .line 13588
    iget-object v2, p0, Lcom/sec/chaton/chat/cx;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 13583
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 13598
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 13599
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 13600
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 13601
    iget-object v2, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Ljava/io/File;)V

    .line 13603
    const-wide/16 v2, 0xa

    const/4 v0, 0x0

    :try_start_0
    invoke-static {v2, v3, v0}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 13604
    :catch_0
    move-exception v0

    .line 13606
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 13612
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/cx;->b:Z

    if-nez v0, :cond_1

    .line 13613
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 13616
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 13617
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 13619
    :cond_2
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13538
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/cx;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13538
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/cx;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 13545
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 13547
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->v()V

    .line 13549
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13550
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    .line 13551
    iput-boolean v2, p0, Lcom/sec/chaton/chat/cx;->b:Z

    .line 13552
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ac(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 13553
    iget-object v0, p0, Lcom/sec/chaton/chat/cx;->c:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v2}, Lcom/sec/chaton/chat/ChatFragment;->p(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 13557
    :cond_1
    return-void
.end method
