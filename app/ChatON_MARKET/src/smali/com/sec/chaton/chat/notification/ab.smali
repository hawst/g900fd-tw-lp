.class Lcom/sec/chaton/chat/notification/ab;
.super Landroid/content/BroadcastReceiver;
.source "ScreenWebNotification.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/notification/ScreenWebNotification;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/chat/notification/ScreenWebNotification;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/chaton/chat/notification/ab;->a:Lcom/sec/chaton/chat/notification/ScreenWebNotification;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/chat/notification/ScreenWebNotification;Lcom/sec/chaton/chat/notification/aa;)V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lcom/sec/chaton/chat/notification/ab;-><init>(Lcom/sec/chaton/chat/notification/ScreenWebNotification;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 193
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "BR - ACTION_SCREEN_ON"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ab;->a:Lcom/sec/chaton/chat/notification/ScreenWebNotification;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a(Lcom/sec/chaton/chat/notification/ScreenWebNotification;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 199
    const-string v0, "BR - ACTION_SCREEN_OFF"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ab;->a:Lcom/sec/chaton/chat/notification/ScreenWebNotification;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a(Lcom/sec/chaton/chat/notification/ScreenWebNotification;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ab;->a:Lcom/sec/chaton/chat/notification/ScreenWebNotification;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a()V

    goto :goto_0
.end method
