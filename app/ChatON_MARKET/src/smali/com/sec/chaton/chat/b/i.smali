.class public Lcom/sec/chaton/chat/b/i;
.super Ljava/lang/Object;
.source "TTSSpeakEngine.java"


# static fields
.field private static a:Landroid/speech/tts/TextToSpeech;

.field private static b:Z

.field private static c:Z

.field private static d:Z

.field private static e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/b/l;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Ljava/util/Timer;

.field private static g:Landroid/widget/ListView;

.field private static h:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field private static i:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->b:Z

    .line 41
    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->c:Z

    .line 42
    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->d:Z

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/chaton/chat/b/i;->e:Ljava/util/ArrayList;

    .line 45
    sput-object v1, Lcom/sec/chaton/chat/b/i;->f:Ljava/util/Timer;

    .line 51
    sput-object v1, Lcom/sec/chaton/chat/b/i;->g:Landroid/widget/ListView;

    .line 182
    new-instance v0, Lcom/sec/chaton/chat/b/j;

    invoke-direct {v0}, Lcom/sec/chaton/chat/b/j;-><init>()V

    sput-object v0, Lcom/sec/chaton/chat/b/i;->h:Landroid/speech/tts/TextToSpeech$OnInitListener;

    .line 310
    sput-object v1, Lcom/sec/chaton/chat/b/i;->i:Ljava/util/TimerTask;

    return-void
.end method

.method static synthetic a(Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    .prologue
    .line 18
    sput-object p0, Lcom/sec/chaton/chat/b/i;->f:Ljava/util/Timer;

    return-object p0
.end method

.method static synthetic a(Ljava/util/TimerTask;)Ljava/util/TimerTask;
    .locals 0

    .prologue
    .line 18
    sput-object p0, Lcom/sec/chaton/chat/b/i;->i:Ljava/util/TimerTask;

    return-object p0
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 278
    new-instance v0, Lcom/sec/chaton/chat/b/l;

    invoke-direct {v0}, Lcom/sec/chaton/chat/b/l;-><init>()V

    .line 279
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v1

    .line 280
    if-nez p2, :cond_1

    .line 281
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 282
    const-string v0, "addMessageForPlayback - message is null"

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    iput-wide p0, v0, Lcom/sec/chaton/chat/b/l;->a:J

    .line 287
    iput-object p2, v0, Lcom/sec/chaton/chat/b/l;->b:Ljava/lang/String;

    .line 288
    if-nez p3, :cond_2

    .line 289
    invoke-virtual {v1}, Lcom/sec/chaton/chat/b/c;->d()Lcom/sec/chaton/chat/b/a;

    move-result-object v1

    iget-object p3, v1, Lcom/sec/chaton/chat/b/a;->s:Ljava/lang/String;

    .line 291
    :cond_2
    iput-object p3, v0, Lcom/sec/chaton/chat/b/l;->c:Ljava/lang/String;

    .line 292
    sget-object v1, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 293
    sget-object v1, Lcom/sec/chaton/chat/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 294
    sget-object v1, Lcom/sec/chaton/chat/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 295
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_3

    .line 296
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addMessageForPlayback - add to messageQueue. isInitInProgress : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/chaton/chat/b/i;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isInitSuccess : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/chaton/chat/b/i;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TTSSpeakEngine"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_3
    sget-boolean v1, Lcom/sec/chaton/chat/b/i;->d:Z

    if-nez v1, :cond_5

    .line 300
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/chaton/chat/b/i;->c:Z

    .line 301
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_4

    .line 302
    const-string v1, "rebound TTSSpeakEngin()"

    const-string v2, "TTSSpeakEngine"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_4
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->b()V

    .line 306
    :cond_5
    sget-object v1, Lcom/sec/chaton/chat/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 54
    sput-object p0, Lcom/sec/chaton/chat/b/i;->g:Landroid/widget/ListView;

    .line 55
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 48
    sget-boolean v0, Lcom/sec/chaton/chat/b/i;->b:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/b/l;)Z
    .locals 1

    .prologue
    .line 18
    invoke-static {p0}, Lcom/sec/chaton/chat/b/i;->b(Lcom/sec/chaton/chat/b/l;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 354
    sget-object v1, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    if-nez v1, :cond_1

    .line 355
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 356
    const-string v1, "isLanguageAvailable - mTts is null"

    const-string v2, "TTSSpeakEngine"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_0
    :goto_0
    return v0

    .line 361
    :cond_1
    if-nez p0, :cond_2

    .line 362
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 363
    const-string v1, "isLanguageAvailable - language is null"

    const-string v2, "TTSSpeakEngine"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 368
    :cond_2
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/chaton/chat/b/c;->c(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 370
    if-nez v1, :cond_3

    .line 371
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 372
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isLanguageAvailable - locale for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TTSSpeakEngine"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 376
    :cond_3
    sget-object v2, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, v1}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    .line 378
    const/4 v2, -0x2

    if-ne v1, v2, :cond_4

    .line 379
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 380
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isLanguageAvailable - LANG_NOT_SUPPORTED("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TTSSpeakEngine"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 385
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 18
    sput-boolean p0, Lcom/sec/chaton/chat/b/i;->d:Z

    return p0
.end method

.method public static declared-synchronized b()V
    .locals 3

    .prologue
    .line 58
    const-class v1, Lcom/sec/chaton/chat/b/i;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initTTSSpeakEngine() - isInitInProgress : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/sec/chaton/chat/b/i;->c:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isInitSuccess : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/sec/chaton/chat/b/i;->d:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "TTSSpeakEngine"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    invoke-static {}, Lcom/sec/chaton/chat/b/n;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 69
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 66
    :cond_2
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/chat/b/i;->c:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/chaton/chat/b/i;->d:Z

    if-nez v0, :cond_1

    .line 67
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lcom/sec/chaton/chat/b/l;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 313
    const v1, 0x1b669

    .line 314
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v2

    .line 334
    sget-object v3, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    if-eqz v3, :cond_2

    .line 335
    iget-object v1, p0, Lcom/sec/chaton/chat/b/l;->c:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/chat/b/c;->c(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 336
    sget-object v2, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v1

    .line 337
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set language result code - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TTSSpeakEngine"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, -0x2

    if-ne v1, v2, :cond_1

    .line 339
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Lang data missing !!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/b/l;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result code - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TTSSpeakEngine"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :cond_1
    sget-object v1, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/sec/chaton/chat/b/l;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v1

    .line 342
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_2

    .line 343
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "speakMessage - result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TTSSpeakEngine"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_2
    if-nez v1, :cond_3

    .line 350
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Z)Z
    .locals 0

    .prologue
    .line 18
    sput-boolean p0, Lcom/sec/chaton/chat/b/i;->c:Z

    return p0
.end method

.method public static declared-synchronized c()V
    .locals 3

    .prologue
    .line 146
    const-class v1, Lcom/sec/chaton/chat/b/i;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "shutdownTTSSpeakEngine()"

    const-string v2, "TTSSpeakEngine"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/b/i;->f:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 154
    sget-object v0, Lcom/sec/chaton/chat/b/i;->f:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 157
    :cond_1
    sget-object v0, Lcom/sec/chaton/chat/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 160
    sget-object v0, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_4

    .line 162
    sget-object v0, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 164
    const-string v0, "mTts.stop()"

    const-string v2, "TTSSpeakEngine"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_2
    sget-object v0, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 168
    :cond_3
    sget-object v0, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 169
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 170
    const-string v0, "mTts.shutdown()"

    const-string v2, "TTSSpeakEngine"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_4
    :goto_0
    const/4 v0, 0x0

    :try_start_1
    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->b:Z

    .line 177
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->d:Z

    .line 178
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->c:Z

    .line 179
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/chat/b/i;->g:Landroid/widget/ListView;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180
    monitor-exit v1

    return-void

    .line 173
    :catch_0
    move-exception v0

    .line 174
    :try_start_2
    const-string v2, "TTSSpeakEngine"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/chaton/chat/b/i;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 18
    sget-boolean v0, Lcom/sec/chaton/chat/b/i;->d:Z

    return v0
.end method

.method static synthetic f()Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic g()Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/chaton/chat/b/i;->i:Ljava/util/TimerTask;

    return-object v0
.end method

.method static synthetic h()Ljava/util/Timer;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/chaton/chat/b/i;->f:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic i()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/chaton/chat/b/i;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic j()Z
    .locals 1

    .prologue
    .line 18
    sget-boolean v0, Lcom/sec/chaton/chat/b/i;->c:Z

    return v0
.end method

.method private static k()V
    .locals 4

    .prologue
    .line 73
    const/4 v0, 0x1

    :try_start_0
    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->c:Z

    .line 75
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "init()"

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/b/i;->h:Landroid/speech/tts/TextToSpeech$OnInitListener;

    const-string v3, "com.samsung.SMT"

    invoke-direct {v0, v1, v2, v3}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    .line 82
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mTts : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_1
    sget-object v0, Lcom/sec/chaton/chat/b/i;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->getEngines()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/speech/tts/TextToSpeech$EngineInfo;

    .line 88
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_3

    .line 89
    iget-object v2, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EngineInfo - info.name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TTSSpeakEngine"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_3
    iget-object v2, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v0, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.SMT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/chat/b/i;->b:Z

    .line 97
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "samsung engine exist : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/sec/chaton/chat/b/i;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_4
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_5

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mIsSamsungTTSEngineExist : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/sec/chaton/chat/b/i;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_5
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 134
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/chaton/chat/b/i;->c:Z

    .line 135
    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
