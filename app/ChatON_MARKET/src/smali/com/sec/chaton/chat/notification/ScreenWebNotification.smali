.class public Lcom/sec/chaton/chat/notification/ScreenWebNotification;
.super Landroid/support/v4/app/FragmentActivity;
.source "ScreenWebNotification.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Landroid/app/KeyguardManager;

.field private d:Landroid/os/PowerManager;

.field private e:I

.field private f:Ljava/lang/Runnable;

.field private g:Landroid/os/Handler;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    .line 36
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->e:I

    .line 38
    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->g:Landroid/os/Handler;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->o:Z

    .line 52
    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->p:Landroid/content/BroadcastReceiver;

    .line 190
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/notification/ScreenWebNotification;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->g:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 133
    :cond_0
    const-string v0, "[finishPopup]"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->finish()V

    .line 135
    invoke-virtual {p0, v2, v2}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->overridePendingTransition(II)V

    .line 136
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 172
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 188
    :goto_0
    return-void

    .line 175
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a()V

    goto :goto_0

    .line 180
    :pswitch_1
    invoke-static {p0, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 181
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 182
    const-string v1, "fromPush"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 183
    const-string v1, "Content"

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->startActivity(Landroid/content/Intent;)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a()V

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x7f0702d8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 56
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x280000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 61
    iput-object p0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->b:Landroid/content/Context;

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/sec/multiwindow/MultiWindowManager;->isMultiWindowServiceEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-static {p0}, Landroid/sec/multiwindow/MultiWindow;->createInstance(Landroid/app/Activity;)Landroid/sec/multiwindow/MultiWindow;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {v0}, Landroid/sec/multiwindow/MultiWindow;->normalWindow()Z

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->o:Z

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->b:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->c:Landroid/app/KeyguardManager;

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->b:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->d:Landroid/os/PowerManager;

    .line 74
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 75
    const v1, 0x7f0300db

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->d:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->c:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    :cond_1
    const/high16 v0, -0x1000000

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 93
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->setContentView(Landroid/view/View;)V

    .line 95
    const v0, 0x7f0703d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->h:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    const v0, 0x7f0703d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->i:Landroid/widget/TextView;

    .line 99
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "contents"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->l:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    const v0, 0x7f0702d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->j:Landroid/widget/Button;

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->j:Landroid/widget/Button;

    const v2, 0x7f0b01a4

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    const v0, 0x7f0702d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->k:Landroid/widget/Button;

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->k:Landroid/widget/Button;

    const v1, 0x7f0b0176

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onCreate] webContent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 114
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 115
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    new-instance v1, Lcom/sec/chaton/chat/notification/ab;

    invoke-direct {v1, p0, v5}, Lcom/sec/chaton/chat/notification/ab;-><init>(Lcom/sec/chaton/chat/notification/ScreenWebNotification;Lcom/sec/chaton/chat/notification/aa;)V

    iput-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->p:Landroid/content/BroadcastReceiver;

    .line 118
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    return-void

    .line 81
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 82
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->g:Landroid/os/Handler;

    .line 83
    new-instance v0, Lcom/sec/chaton/chat/notification/aa;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/aa;-><init>(Lcom/sec/chaton/chat/notification/ScreenWebNotification;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->f:Ljava/lang/Runnable;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->g:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->f:Ljava/lang/Runnable;

    iget v3, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->e:I

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 153
    const-string v0, "[onDestroy]"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 156
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->o:Z

    if-eqz v0, :cond_0

    .line 157
    invoke-static {p0}, Landroid/sec/multiwindow/MultiWindow;->createInstance(Landroid/app/Activity;)Landroid/sec/multiwindow/MultiWindow;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow()Z

    .line 160
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 124
    const-string v0, "contents"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->l:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onNewIntent] webContent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 140
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 141
    const-string v0, "onResume"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->n:Z

    if-nez v0, :cond_0

    .line 143
    iput-boolean v2, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->n:Z

    .line 146
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->m:Z

    if-nez v0, :cond_1

    .line 147
    iput-boolean v2, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->m:Z

    .line 149
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 164
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 165
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "[onStop]"

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ScreenWebNotification;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    return-void
.end method
