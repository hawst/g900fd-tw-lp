.class Lcom/sec/chaton/chat/bk;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 12038
    iput-object p1, p0, Lcom/sec/chaton/chat/bk;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 12042
    iget-object v0, p0, Lcom/sec/chaton/chat/bk;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aK(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_0

    .line 12043
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/bk;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSPlayerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 12044
    const-string v1, "AMS_FILE_PATH"

    iget-object v2, p0, Lcom/sec/chaton/chat/bk;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aL(Lcom/sec/chaton/chat/ChatFragment;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 12045
    const-string v1, "VIEWER_MODE"

    const/16 v2, 0x3ea

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 12046
    iget-object v1, p0, Lcom/sec/chaton/chat/bk;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->startActivity(Landroid/content/Intent;)V

    .line 12048
    :cond_0
    return-void
.end method
