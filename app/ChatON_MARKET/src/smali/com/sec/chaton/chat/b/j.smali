.class final Lcom/sec/chaton/chat/b/j;
.super Ljava/lang/Object;
.source "TTSSpeakEngine.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 186
    if-nez p1, :cond_3

    .line 187
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "listener - init success!"

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/chat/b/i;->a(Z)Z

    .line 192
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    invoke-static {v0}, Lcom/sec/chaton/chat/b/i;->a(Ljava/util/Timer;)Ljava/util/Timer;

    .line 194
    new-instance v0, Lcom/sec/chaton/chat/b/k;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/b/k;-><init>(Lcom/sec/chaton/chat/b/j;)V

    invoke-static {v0}, Lcom/sec/chaton/chat/b/i;->a(Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 216
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->h()Ljava/util/Timer;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/chat/b/i;->g()Ljava/util/TimerTask;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    const-wide/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;Ljava/util/Date;J)V

    .line 218
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->i()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 219
    invoke-static {}, Lcom/sec/chaton/chat/b/i;->i()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 227
    :cond_1
    :goto_0
    invoke-static {v5}, Lcom/sec/chaton/chat/b/i;->b(Z)Z

    .line 229
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "listener - status : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isInitInProgress : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/chat/b/i;->j()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isInitSuccess : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/chat/b/i;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_2
    return-void

    .line 222
    :cond_3
    invoke-static {v5}, Lcom/sec/chaton/chat/b/i;->a(Z)Z

    .line 223
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 224
    const-string v0, "listener - init fail!"

    const-string v1, "TTSSpeakEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
