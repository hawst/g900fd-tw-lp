.class Lcom/sec/chaton/chat/eu;
.super Landroid/os/Handler;
.source "DuringCallController.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/et;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/et;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v1, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 48
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 88
    :goto_0
    :pswitch_0
    return-void

    .line 51
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->a(Lcom/sec/chaton/chat/et;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0, v4}, Lcom/sec/chaton/chat/eu;->removeMessages(I)V

    .line 54
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/eu;->removeMessages(I)V

    .line 55
    invoke-virtual {p0, v4}, Lcom/sec/chaton/chat/eu;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v2, v0, v2}, Lcom/sec/chaton/chat/eu;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/eu;->sendMessage(Landroid/os/Message;)Z

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0, v4}, Lcom/sec/chaton/chat/et;->a(Lcom/sec/chaton/chat/et;Z)Z

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Lcom/sec/chaton/util/co;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget v0, p1, Landroid/os/Message;->arg1:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v2, v0, v2}, Lcom/sec/chaton/chat/eu;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/chat/eu;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 72
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_3

    .line 75
    iget v0, p1, Landroid/os/Message;->arg1:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v4, v0, v2}, Lcom/sec/chaton/chat/eu;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/chat/eu;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 72
    goto :goto_1

    .line 77
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->c(Lcom/sec/chaton/chat/et;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0}, Lcom/sec/chaton/chat/et;->b(Lcom/sec/chaton/chat/et;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/chat/eu;->a:Lcom/sec/chaton/chat/et;

    invoke-static {v0, v2}, Lcom/sec/chaton/chat/et;->a(Lcom/sec/chaton/chat/et;Z)Z

    goto/16 :goto_0

    .line 85
    :pswitch_4
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/eu;->removeMessages(I)V

    goto/16 :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method
