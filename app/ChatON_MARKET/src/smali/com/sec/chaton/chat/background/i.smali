.class public Lcom/sec/chaton/chat/background/i;
.super Ljava/lang/Object;
.source "Request.java"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:J

.field protected c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected d:Lcom/sec/chaton/chat/background/j;

.field protected e:Ljava/lang/String;

.field protected f:Ljava/lang/String;

.field protected g:I

.field protected h:I

.field protected i:J

.field protected j:Ljava/lang/String;

.field protected k:J

.field protected l:Ljava/lang/String;

.field protected m:Ljava/lang/String;

.field protected n:Ljava/lang/String;

.field protected o:Ljava/lang/String;

.field protected p:Lcom/sec/chaton/msgsend/k;

.field protected q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-wide v0, p0, Lcom/sec/chaton/chat/background/i;->b:J

    .line 73
    iput-wide v0, p0, Lcom/sec/chaton/chat/background/i;->i:J

    .line 74
    iput-wide v0, p0, Lcom/sec/chaton/chat/background/i;->k:J

    .line 75
    sget-object v0, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/i;->p:Lcom/sec/chaton/msgsend/k;

    .line 76
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/chat/background/h;
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/sec/chaton/chat/background/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/background/h;-><init>(Lcom/sec/chaton/chat/background/i;)V

    return-object v0
.end method

.method public a(I)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 103
    iput p1, p0, Lcom/sec/chaton/chat/background/i;->g:I

    .line 104
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/sec/chaton/chat/background/i;->b:J

    .line 85
    return-object p0
.end method

.method public a(Lcom/sec/chaton/msgsend/k;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->p:Lcom/sec/chaton/msgsend/k;

    .line 164
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->a:Ljava/lang/String;

    .line 80
    return-object p0
.end method

.method public a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/background/i;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/background/i;->c:Ljava/util/ArrayList;

    .line 94
    :cond_0
    :goto_0
    if-eqz p1, :cond_2

    .line 95
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 96
    iget-object v1, p0, Lcom/sec/chaton/chat/background/i;->c:Ljava/util/ArrayList;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/background/i;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/chat/background/i;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 99
    :cond_2
    return-object p0
.end method

.method public b(I)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 108
    iput p1, p0, Lcom/sec/chaton/chat/background/i;->h:I

    .line 109
    return-object p0
.end method

.method public b(J)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 128
    iput-wide p1, p0, Lcom/sec/chaton/chat/background/i;->i:J

    .line 129
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->e:Ljava/lang/String;

    .line 119
    return-object p0
.end method

.method public c(J)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/sec/chaton/chat/background/i;->k:J

    .line 134
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->f:Ljava/lang/String;

    .line 124
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->j:Ljava/lang/String;

    .line 139
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->l:Ljava/lang/String;

    .line 144
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->m:Ljava/lang/String;

    .line 149
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->n:Ljava/lang/String;

    .line 154
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->o:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/chaton/chat/background/i;->q:Ljava/lang/String;

    .line 169
    return-object p0
.end method
