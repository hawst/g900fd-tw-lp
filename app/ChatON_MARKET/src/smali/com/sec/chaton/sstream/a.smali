.class public Lcom/sec/chaton/sstream/a;
.super Ljava/lang/Object;
.source "SStream.java"


# static fields
.field public static final a:Ljava/lang/String;

.field protected static final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/sstream/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    .line 34
    const-string v0, "content://com.sec.chaton.provider/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/sstream/a;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method private a(Ljava/lang/String;I)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 174
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 175
    const-string v1, "inboxNO"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v1, "chatType"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v1, "chaton"

    const-string v2, "com.sec.chaton"

    const-string v3, "sstream"

    invoke-static {v1, v2, v3, v0}, Lcom/sec/chaton/sstream/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 258
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    .line 259
    invoke-virtual {v2, p0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 260
    invoke-virtual {v2, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 261
    invoke-virtual {v2, p2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 263
    if-eqz p3, :cond_1

    .line 264
    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 265
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 266
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 267
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 271
    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 232
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 233
    const-string v0, "removeAllStories()"

    sget-object v1, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    .line 242
    :try_start_0
    const-string v1, "com.sec.chaton"

    const-string v2, "samsung.personal"

    invoke-static {v0, v1, v2}, La/a/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_0
    return-void

    .line 243
    :catch_0
    move-exception v0

    .line 244
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/chat/notification/g;Z)Z
    .locals 16

    .prologue
    .line 97
    const/4 v14, 0x0

    .line 100
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v15

    .line 101
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 102
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 103
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203f6

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 104
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 105
    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 111
    new-instance v9, La/a/b/b;

    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/sstream/a;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v9, v3, v1, v2, v4}, La/a/b/b;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 114
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 115
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 116
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    .line 120
    :cond_0
    new-instance v8, La/a/b/a;

    const/4 v2, 0x0

    invoke-direct {v8, v1, v2}, La/a/b/a;-><init>(Ljava/lang/String;La/a/b/b;)V

    .line 122
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/sstream/a;->a()V

    .line 124
    const-string v2, ""

    .line 126
    move-object/from16 v0, p1

    iget v2, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    invoke-static {v2}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v2, v3, :cond_3

    .line 127
    const-string v1, ""

    .line 128
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 129
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    :goto_0
    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    const/high16 v2, 0x41f00000    # 30.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v15, v1, v2}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 135
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 141
    :goto_1
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v2, v0, Lcom/sec/chaton/chat/notification/g;->f:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/sstream/a;->a(Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 142
    new-instance v1, La/a/b/d;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "samsung.personal"

    const-string v4, "com.sec.chaton"

    const/4 v6, 0x0

    sget-object v7, La/a/b/e;->b:La/a/b/e;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/sec/chaton/chat/notification/g;->d:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    if-eqz p2, :cond_4

    const/4 v12, -0x1

    :goto_2
    const/4 v13, 0x0

    invoke-direct/range {v1 .. v13}, La/a/b/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;La/a/b/e;La/a/b/a;La/a/b/b;JILjava/lang/String;)V

    .line 154
    invoke-static {v15, v1}, La/a/b;->a(Landroid/content/Context;La/a/b/d;)V
    :try_end_0
    .catch La/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 155
    const/4 v1, 0x1

    .line 166
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_1

    .line 167
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The story item is updated to SSTREAM successfully!!! ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sentTime["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/g;->d:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_1
    :goto_3
    return v1

    .line 131
    :cond_2
    :try_start_1
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    goto :goto_0

    .line 137
    :cond_3
    move-object/from16 v0, p1

    iget v2, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    invoke-static {v2}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/sec/chaton/chat/notification/g;->o:Z

    invoke-static {v2, v3, v1, v4}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_1
    .catch La/a/a; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    goto/16 :goto_1

    .line 142
    :cond_4
    const/4 v12, 0x0

    goto :goto_2

    .line 156
    :catch_0
    move-exception v1

    .line 159
    invoke-virtual {v1}, La/a/a;->printStackTrace()V

    move v1, v14

    .line 160
    goto :goto_3

    .line 161
    :catch_1
    move-exception v1

    .line 162
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v1, v14

    .line 163
    goto :goto_3
.end method

.method private b()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 250
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 251
    const-string v0, "getIconUri()"

    sget-object v1, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_0
    const-string v0, "android.resource"

    const-string v1, "com.sec.chaton"

    const-string v2, "drawable/sstream_bg"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/sstream/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/ArrayList;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/notification/g;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 182
    .line 184
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v2, :cond_2

    move v1, v2

    .line 186
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 187
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 194
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "more then two chatrooms with unreadmessage ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_0
    return v2

    .line 186
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 275
    const/4 v0, 0x0

    .line 277
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    .line 283
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "sstream.app"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 297
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 298
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 299
    const-string v1, "SStream is disabled by user."

    sget-object v2, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 285
    :catch_0
    move-exception v1

    .line 286
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 287
    const-string v1, "Can\'t retrieve application enabled setting."

    sget-object v2, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 291
    :catch_1
    move-exception v1

    .line 292
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 303
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 304
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 305
    const-string v1, "SStream is disabled by developer."

    sget-object v2, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 311
    const-string v0, "SStream is available."

    sget-object v1, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/notification/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/sstream/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 209
    const-string v0, ":: removeStoryItem ::"

    sget-object v1, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/sstream/a;->a()V

    .line 214
    invoke-virtual {p0, p2}, Lcom/sec/chaton/sstream/a;->a(Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "addStoryItems() on removeStoryItem is failed."

    sget-object v1, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/notification/g;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 69
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 70
    const-string v0, ":: addStoryItems ::"

    sget-object v2, Lcom/sec/chaton/sstream/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/sstream/a;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 93
    :cond_1
    :goto_0
    return v1

    .line 82
    :cond_2
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/chaton/sstream/a;->b(Ljava/util/ArrayList;)Z

    move-result v2

    .line 88
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    invoke-direct {p0, v0, v2}, Lcom/sec/chaton/sstream/a;->a(Lcom/sec/chaton/chat/notification/g;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 93
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
