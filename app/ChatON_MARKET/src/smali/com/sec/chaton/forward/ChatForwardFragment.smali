.class public Lcom/sec/chaton/forward/ChatForwardFragment;
.super Landroid/support/v4/app/Fragment;
.source "ChatForwardFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/support/v4/app/FragmentManager;

.field b:Landroid/support/v4/app/FragmentTransaction;

.field c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

.field d:Lcom/sec/chaton/buddy/BuddyFragment;

.field e:Landroid/os/Bundle;

.field f:Landroid/os/Bundle;

.field g:I

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lcom/sec/chaton/widget/StateButton;

.field private o:Lcom/sec/chaton/widget/StateButton;

.field private p:Lcom/sec/chaton/widget/StateButton;

.field private q:Landroid/view/View;

.field private r:Lcom/sec/chaton/buddy/cy;

.field private s:Lcom/sec/chaton/msgbox/as;

.field private t:Landroid/view/MenuItem;

.field private u:Landroid/view/MenuItem;

.field private v:Z

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->a:Landroid/support/v4/app/FragmentManager;

    .line 50
    iput-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    .line 72
    iput-boolean v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->v:Z

    .line 73
    iput-boolean v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->w:Z

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/forward/ChatForwardFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->t:Landroid/view/MenuItem;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const v5, 0x7f07031b

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->a:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    .line 344
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->o:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/StateButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 347
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 350
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 355
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->c()V

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->o:Lcom/sec/chaton/widget/StateButton;

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->p:Lcom/sec/chaton/widget/StateButton;

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->o:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/StateButton;->setPressed(Z)V

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/StateButton;->setPressed(Z)V

    .line 362
    iget-boolean v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->w:Z

    if-nez v0, :cond_2

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    const-string v2, "MSGFRAGMENT"

    invoke-virtual {v0, v5, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 364
    iput-boolean v4, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->w:Z

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 368
    iput v3, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->g:I

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->s:Lcom/sec/chaton/msgbox/as;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/as;)V

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 392
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->p:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 394
    return-void

    .line 371
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/StateButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->o:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->d()V

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_5

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 378
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->p:Lcom/sec/chaton/widget/StateButton;

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->o:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/StateButton;->setPressed(Z)V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/StateButton;->setPressed(Z)V

    .line 381
    iget-boolean v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->v:Z

    if-nez v0, :cond_6

    .line 382
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    const-string v2, "BUDDYFRAGMENT"

    invoke-virtual {v0, v5, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 383
    iput-boolean v4, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->v:Z

    .line 385
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Z)V

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 388
    iput v4, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->g:I

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->r:Lcom/sec/chaton/buddy/cy;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/cy;)V

    .line 390
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->b:Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onAttach, MemoryAddress : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 398
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 403
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->p:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/StateButton;->getId()I

    move-result v0

    const v1, 0x7f070319

    if-eq v0, v1, :cond_0

    .line 404
    invoke-direct {p0, p1}, Lcom/sec/chaton/forward/ChatForwardFragment;->a(Landroid/view/View;)V

    goto :goto_0

    .line 408
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->p:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/StateButton;->getId()I

    move-result v0

    const v1, 0x7f07031a

    if-eq v0, v1, :cond_0

    .line 409
    invoke-direct {p0, p1}, Lcom/sec/chaton/forward/ChatForwardFragment;->a(Landroid/view/View;)V

    goto :goto_0

    .line 401
    nop

    :pswitch_data_0
    .packed-switch 0x7f070319
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->a:Landroid/support/v4/app/FragmentManager;

    .line 143
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    .line 144
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    .line 145
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/forward/ChatForwardFragment;->setHasOptionsMenu(Z)V

    .line 147
    new-instance v0, Lcom/sec/chaton/forward/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/forward/b;-><init>(Lcom/sec/chaton/forward/ChatForwardFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->r:Lcom/sec/chaton/buddy/cy;

    .line 157
    new-instance v0, Lcom/sec/chaton/forward/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/forward/c;-><init>(Lcom/sec/chaton/forward/ChatForwardFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->s:Lcom/sec/chaton/msgbox/as;

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->r:Lcom/sec/chaton/buddy/cy;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/cy;)V

    .line 169
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 84
    iget v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->g:I

    if-nez v0, :cond_0

    .line 87
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 88
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->t:Landroid/view/MenuItem;

    .line 89
    const v0, 0x7f0705a5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->u:Landroid/view/MenuItem;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->t:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    .line 180
    sget-object v0, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    invoke-virtual {v0}, Lcom/sec/chaton/e/r;->a()I

    move-result v0

    .line 181
    sget-object v4, Lcom/sec/chaton/w;->e:Lcom/sec/chaton/w;

    .line 183
    const/4 v5, -0x1

    .line 185
    if-eqz v6, :cond_13

    invoke-virtual {v6}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_13

    .line 186
    const-string v1, "content_type"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    const-string v1, "content_type"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->h:I

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward content_type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 190
    :cond_0
    const-string v1, "download_uri"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    const-string v1, "download_uri"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->i:Ljava/lang/String;

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward download_uri:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 194
    :cond_1
    const-string v1, "sub_content"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 195
    const-string v1, "sub_content"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->j:Ljava/lang/String;

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward sub_content:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 198
    :cond_2
    const-string v1, "forward_sender_name"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 199
    const-string v1, "forward_sender_name"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->k:Ljava/lang/String;

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward sender name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 202
    :cond_3
    const-string v1, "inboxNO"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 203
    const-string v1, "inboxNO"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->l:Ljava/lang/String;

    .line 204
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward mInboxNo:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 206
    :cond_4
    const-string v1, "chatType"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 207
    const-string v0, "chatType"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forward chatType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    :cond_5
    move v1, v0

    .line 211
    const-string v0, "is_forward_mode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 212
    const-string v0, "is_forward_mode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Forward isForward:"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    move v2, v0

    .line 218
    :goto_0
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 219
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/w;

    check-cast v0, Lcom/sec/chaton/w;

    .line 220
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Forward intentFrom:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 222
    :goto_1
    const-string v4, "null"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 223
    const-string v4, "null"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->m:Ljava/lang/String;

    .line 224
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Forward BuddyName:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->m:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 227
    :cond_6
    const-string v4, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 228
    const-string v4, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    move v5, v1

    move v1, v4

    move-object v4, v0

    .line 232
    :goto_2
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 233
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0165

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/chaton/forward/ChatForwardActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 236
    :cond_7
    const v0, 0x7f0300a9

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 238
    const v0, 0x7f070319

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/StateButton;

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/widget/StateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    const v0, 0x7f07031a

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/StateButton;

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->o:Lcom/sec/chaton/widget/StateButton;

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->o:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/widget/StateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    const v0, 0x7f07031b

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->q:Landroid/view/View;

    .line 247
    if-nez p3, :cond_b

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "content_type"

    iget v8, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->h:I

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "download_uri"

    iget-object v8, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "inboxNO"

    iget-object v8, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "sub_content"

    iget-object v8, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "forward_sender_name"

    iget-object v8, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "ACTIVITY_PURPOSE"

    const/16 v8, 0x8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    sget-object v7, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v0, v7, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->m:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "null"

    iget-object v8, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_8
    sget-object v0, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    invoke-virtual {v0}, Lcom/sec/chaton/e/r;->a()I

    move-result v0

    if-eq v5, v0, :cond_9

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v7, "chatType"

    invoke-virtual {v0, v7, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 265
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v5, "is_forward_mode"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 267
    if-lez v1, :cond_a

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->e:Landroid/os/Bundle;

    const-string v5, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 271
    :cond_a
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 274
    :cond_b
    if-nez p3, :cond_d

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v5, "content_type"

    iget v7, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->h:I

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v5, "download_uri"

    iget-object v7, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v5, "inboxNO"

    iget-object v7, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v5, "sub_content"

    iget-object v7, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v5, "forward_sender_name"

    iget-object v7, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v5, "mode"

    const/4 v7, 0x3

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    sget-object v5, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v4, "is_forward_mode"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 285
    if-lez v1, :cond_c

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->f:Landroid/os/Bundle;

    const-string v2, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 290
    :cond_c
    new-instance v0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-direct {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    .line 309
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->n:Lcom/sec/chaton/widget/StateButton;

    invoke-direct {p0, v0}, Lcom/sec/chaton/forward/ChatForwardFragment;->a(Landroid/view/View;)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 312
    return-object v6

    .line 293
    :cond_d
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BUDDYFRAGMENT"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    if-nez v0, :cond_e

    .line 295
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 301
    :goto_4
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "MSGFRAGMENT"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    if-nez v0, :cond_f

    .line 303
    new-instance v0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-direct {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    goto :goto_3

    .line 298
    :cond_e
    iput-boolean v9, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->v:Z

    goto :goto_4

    .line 306
    :cond_f
    iput-boolean v9, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->w:Z

    goto :goto_3

    :cond_10
    move-object v4, v0

    move v10, v5

    move v5, v1

    move v1, v10

    goto/16 :goto_2

    :cond_11
    move-object v0, v4

    goto/16 :goto_1

    :cond_12
    move v2, v3

    goto/16 :goto_0

    :cond_13
    move v1, v5

    move v2, v3

    move v5, v0

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 174
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 175
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 96
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a5

    if-ne v0, v1, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 100
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a6

    if-ne v0, v1, :cond_1

    .line 101
    iget v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->g:I

    if-ne v0, v3, :cond_4

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-le v0, v3, :cond_3

    .line 104
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    .line 105
    const/4 v1, 0x0

    const v2, 0x7f0b0175

    invoke-virtual {p0, v2}, Lcom/sec/chaton/forward/ChatForwardFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 106
    const v1, 0x7f0b0029

    invoke-virtual {p0, v1}, Lcom/sec/chaton/forward/ChatForwardFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 109
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 110
    new-instance v2, Lcom/sec/chaton/forward/a;

    invoke-direct {v2, p0}, Lcom/sec/chaton/forward/a;-><init>(Lcom/sec/chaton/forward/ChatForwardFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 122
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 132
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/sec/chaton/forward/ChatForwardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 136
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 125
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->d:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->e()V

    goto :goto_0

    .line 127
    :cond_4
    iget v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->g:I

    if-nez v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->c:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->e()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 323
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 324
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 317
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/forward/ChatForwardFragment;->p:Lcom/sec/chaton/widget/StateButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setPressed(Z)V

    .line 319
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 328
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 329
    return-void
.end method
