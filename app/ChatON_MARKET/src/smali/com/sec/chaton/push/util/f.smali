.class public final enum Lcom/sec/chaton/push/util/f;
.super Ljava/lang/Enum;
.source "NetworkUtil.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/push/util/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/push/util/f;

.field public static final enum b:Lcom/sec/chaton/push/util/f;

.field public static final enum c:Lcom/sec/chaton/push/util/f;

.field public static final enum d:Lcom/sec/chaton/push/util/f;

.field private static final synthetic e:[Lcom/sec/chaton/push/util/f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/sec/chaton/push/util/f;

    const-string v1, "NoActive"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/push/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    .line 24
    new-instance v0, Lcom/sec/chaton/push/util/f;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/push/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/util/f;->b:Lcom/sec/chaton/push/util/f;

    .line 25
    new-instance v0, Lcom/sec/chaton/push/util/f;

    const-string v1, "Mobile"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/push/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    .line 26
    new-instance v0, Lcom/sec/chaton/push/util/f;

    const-string v1, "ETC"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/push/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/util/f;->d:Lcom/sec/chaton/push/util/f;

    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/push/util/f;

    sget-object v1, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/push/util/f;->b:Lcom/sec/chaton/push/util/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/push/util/f;->d:Lcom/sec/chaton/push/util/f;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/push/util/f;->e:[Lcom/sec/chaton/push/util/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/sec/chaton/push/util/f;
    .locals 1

    .prologue
    .line 34
    packed-switch p0, :pswitch_data_0

    .line 53
    sget-object v0, Lcom/sec/chaton/push/util/f;->d:Lcom/sec/chaton/push/util/f;

    :goto_0
    return-object v0

    .line 36
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    goto :goto_0

    .line 40
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/push/util/f;->b:Lcom/sec/chaton/push/util/f;

    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    goto :goto_0

    .line 34
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/push/util/f;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/chaton/push/util/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/push/util/f;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/push/util/f;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/chaton/push/util/f;->e:[Lcom/sec/chaton/push/util/f;

    invoke-virtual {v0}, [Lcom/sec/chaton/push/util/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/push/util/f;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
