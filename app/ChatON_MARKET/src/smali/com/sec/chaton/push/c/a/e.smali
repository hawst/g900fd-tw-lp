.class public Lcom/sec/chaton/push/c/a/e;
.super Lcom/sec/chaton/push/c/a;
.source "PingMessageTask.java"

# interfaces
.implements Lcom/sec/chaton/push/c/d;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:Lcom/sec/chaton/push/b/a/d;

.field private d:Lcom/sec/chaton/push/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/chaton/push/c/a/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    const-wide/16 v0, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/push/c/a;-><init>(Landroid/content/Context;J)V

    .line 42
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/e;->c:Lcom/sec/chaton/push/b/a/d;

    .line 43
    return-void
.end method


# virtual methods
.method public a(ILcom/google/protobuf/GeneratedMessageLite;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 97
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    const-string v1, "PingMessageTask.onResponse(). ErrorCode: %d."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    if-nez p1, :cond_4

    .line 103
    if-nez p2, :cond_2

    .line 104
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_1

    .line 105
    sget-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    const-string v1, "Response message is null."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_1
    :goto_0
    return-void

    .line 111
    :cond_2
    check-cast p2, Lcom/sec/a/a/a/x;

    .line 113
    iget v0, p0, Lcom/sec/chaton/push/c/a/e;->b:I

    invoke-virtual {p2}, Lcom/sec/a/a/a/x;->d()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 114
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 115
    sget-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    const-string v1, "Request Async Id(%d) != Response Async Id(%d)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/push/c/a/e;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p2}, Lcom/sec/a/a/a/x;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/e;->d:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/e;->d:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0}, Lcom/sec/chaton/push/c/b;->a()V

    goto :goto_0

    .line 125
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_5

    .line 126
    sget-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    const-string v1, "Close connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_5
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/e;->c:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_6
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_7

    .line 137
    sget-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    const-string v1, "Execeute initialize."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_7
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/c/a/b;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 147
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/e;->d:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/e;->d:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, p1}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_6

    .line 132
    sget-object v1, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 141
    :catch_1
    move-exception v0

    .line 142
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_8

    .line 143
    sget-object v1, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public a(Lcom/sec/chaton/push/c/b;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/push/c/a/e;->d:Lcom/sec/chaton/push/c/b;

    .line 53
    return-void
.end method

.method public c()Lcom/sec/chaton/push/c/h;
    .locals 3

    .prologue
    .line 62
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    const-string v1, "PingMessageTask.onRequest()."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 68
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 69
    sget-object v0, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    const-string v1, "Initialize isn\'t completed. Execute initialize."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/c/a/b;

    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/e;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V

    .line 75
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :cond_2
    :goto_0
    const/4 v0, 0x0

    .line 92
    :goto_1
    return-object v0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 78
    sget-object v1, Lcom/sec/chaton/push/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 85
    :cond_3
    invoke-static {}, Lcom/sec/chaton/push/util/d;->a()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/push/c/a/e;->b:I

    .line 87
    invoke-static {}, Lcom/sec/a/a/a/aa;->newBuilder()Lcom/sec/a/a/a/ab;

    move-result-object v0

    .line 88
    iget v1, p0, Lcom/sec/chaton/push/c/a/e;->b:I

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/ab;->a(I)Lcom/sec/a/a/a/ab;

    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/a/a/a/ab;->a(J)Lcom/sec/a/a/a/ab;

    .line 90
    const/16 v1, 0x8

    invoke-static {v1}, Lcom/sec/chaton/push/k;->c(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/ab;->b(I)Lcom/sec/a/a/a/ab;

    .line 92
    new-instance v1, Lcom/sec/chaton/push/c/h;

    invoke-direct {v1}, Lcom/sec/chaton/push/c/h;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/push/c/a/e;->c:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/h;->a(Lcom/sec/chaton/push/b/a/a;)Lcom/sec/chaton/push/c/h;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/a/a/a/ab;->d()Lcom/sec/a/a/a/aa;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/push/c/h;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    goto :goto_1
.end method

.method public f_()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    const-class v0, Lcom/sec/a/a/a/x;

    return-object v0
.end method
