.class public Lcom/sec/chaton/push/util/e;
.super Ljava/lang/Object;
.source "NetworkUtil.java"


# static fields
.field private static a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->l()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/util/e;->a:Landroid/content/Context;

    return-void
.end method

.method public static a()Lcom/sec/chaton/push/util/f;
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/sec/chaton/push/util/e;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 60
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 62
    if-nez v0, :cond_0

    .line 63
    sget-object v0, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    .line 65
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/push/util/f;->a(I)Lcom/sec/chaton/push/util/f;

    move-result-object v0

    goto :goto_0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 76
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
