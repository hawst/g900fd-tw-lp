.class final enum Lcom/sec/chaton/push/b/b/g;
.super Ljava/lang/Enum;
.source "MessageDecoder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/push/b/b/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/push/b/b/g;

.field public static final enum b:Lcom/sec/chaton/push/b/b/g;

.field public static final enum c:Lcom/sec/chaton/push/b/b/g;

.field public static final enum d:Lcom/sec/chaton/push/b/b/g;

.field private static final synthetic e:[Lcom/sec/chaton/push/b/b/g;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/sec/chaton/push/b/b/g;

    const-string v1, "READ_RESERVED"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/push/b/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/b/b/g;->a:Lcom/sec/chaton/push/b/b/g;

    .line 29
    new-instance v0, Lcom/sec/chaton/push/b/b/g;

    const-string v1, "READ_TYPE"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/push/b/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/b/b/g;->b:Lcom/sec/chaton/push/b/b/g;

    .line 30
    new-instance v0, Lcom/sec/chaton/push/b/b/g;

    const-string v1, "READ_LENGTH"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/push/b/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/b/b/g;->c:Lcom/sec/chaton/push/b/b/g;

    .line 31
    new-instance v0, Lcom/sec/chaton/push/b/b/g;

    const-string v1, "READ_BODY"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/push/b/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/push/b/b/g;->d:Lcom/sec/chaton/push/b/b/g;

    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/push/b/b/g;

    sget-object v1, Lcom/sec/chaton/push/b/b/g;->a:Lcom/sec/chaton/push/b/b/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/push/b/b/g;->b:Lcom/sec/chaton/push/b/b/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/push/b/b/g;->c:Lcom/sec/chaton/push/b/b/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/push/b/b/g;->d:Lcom/sec/chaton/push/b/b/g;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/push/b/b/g;->e:[Lcom/sec/chaton/push/b/b/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/push/b/b/g;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/push/b/b/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/push/b/b/g;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/push/b/b/g;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/chaton/push/b/b/g;->e:[Lcom/sec/chaton/push/b/b/g;

    invoke-virtual {v0}, [Lcom/sec/chaton/push/b/b/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/push/b/b/g;

    return-object v0
.end method
