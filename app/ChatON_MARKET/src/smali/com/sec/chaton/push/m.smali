.class Lcom/sec/chaton/push/m;
.super Ljava/lang/Object;
.source "PushClientService.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/PushClientService;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/PushClientService;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/chaton/push/m;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/chaton/push/m;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/push/PushClientApplication;

    .line 136
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v1

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/e;->a(I)V

    .line 138
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/sec/chaton/push/m;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Cancel retry provisioning and initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->c()V

    .line 142
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->f()V

    .line 145
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/push/c/i;->a(Landroid/content/Context;Lcom/sec/chaton/push/c/k;)V

    .line 148
    :try_start_0
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/sec/chaton/push/m;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Close provisioning connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->k()Lcom/sec/chaton/push/b/a/c;

    move-result-object v1

    .line 154
    if-eqz v1, :cond_2

    .line 155
    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/a;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_1

    .line 162
    :cond_2
    :goto_0
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_3

    .line 163
    iget-object v1, p0, Lcom/sec/chaton/push/m;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Close push connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_4

    .line 169
    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/a;->b()V
    :try_end_1
    .catch Lcom/sec/chaton/push/a/a; {:try_start_1 .. :try_end_1} :catch_0

    .line 174
    :cond_4
    :goto_1
    return-void

    .line 171
    :catch_0
    move-exception v0

    goto :goto_1

    .line 157
    :catch_1
    move-exception v1

    goto :goto_0
.end method
