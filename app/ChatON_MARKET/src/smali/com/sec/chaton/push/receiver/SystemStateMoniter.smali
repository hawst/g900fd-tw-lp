.class public Lcom/sec/chaton/push/receiver/SystemStateMoniter;
.super Landroid/content/BroadcastReceiver;
.source "SystemStateMoniter.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/push/util/f;

.field private static c:Lcom/sec/chaton/push/receiver/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    .line 26
    sget-object v0, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    sput-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    return-void
.end method

.method public static declared-synchronized a()V
    .locals 3

    .prologue
    .line 49
    const-class v1, Lcom/sec/chaton/push/receiver/SystemStateMoniter;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/util/e;->a()Lcom/sec/chaton/push/util/f;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    .line 51
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    sget-object v2, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    if-ne v0, v2, :cond_1

    .line 52
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v2, "There isn\'t active network."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 56
    :cond_1
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 57
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    sget-object v2, Lcom/sec/chaton/push/util/f;->b:Lcom/sec/chaton/push/util/f;

    if-ne v0, v2, :cond_2

    .line 58
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v2, "ConnectType is WIFI."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 59
    :cond_2
    :try_start_2
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    sget-object v2, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v0, v2, :cond_3

    .line 60
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v2, "ConnectType is 3G."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_3
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v2, "ConnectType is is ETC."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized a(Lcom/sec/chaton/push/receiver/b;)V
    .locals 2

    .prologue
    .line 37
    const-class v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->c:Lcom/sec/chaton/push/receiver/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    monitor-exit v0

    return-void

    .line 37
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 70
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v1, "SystemStateMoniter.onReceive()."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 77
    const-string v0, "noConnectivity"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 78
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 80
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v2, :cond_1

    .line 81
    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v3, "NoConnectivity: %s, NetworkInfo: %s."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_1
    if-eqz v1, :cond_5

    .line 85
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 86
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v1, "No active internet."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_2
    const-class v1, Lcom/sec/chaton/push/receiver/SystemStateMoniter;

    monitor-enter v1

    .line 90
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    sput-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    .line 92
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->c:Lcom/sec/chaton/push/receiver/b;

    if-eqz v0, :cond_3

    .line 93
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->c:Lcom/sec/chaton/push/receiver/b;

    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    sget-object v3, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    invoke-interface {v0, v2, v3}, Lcom/sec/chaton/push/receiver/b;->a(Lcom/sec/chaton/push/util/f;Lcom/sec/chaton/push/util/f;)V

    .line 95
    :cond_3
    monitor-exit v1

    .line 136
    :cond_4
    :goto_0
    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 100
    :cond_5
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v1, v2, :cond_6

    .line 101
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_4

    .line 102
    sget-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v1, "This network state isn\'t connected. Ignore it."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_6
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/push/util/f;->a(I)Lcom/sec/chaton/push/util/f;

    move-result-object v0

    .line 110
    const-class v1, Lcom/sec/chaton/push/receiver/SystemStateMoniter;

    monitor-enter v1

    .line 111
    :try_start_1
    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    sget-object v3, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v2, v3, :cond_8

    .line 112
    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    if-ne v2, v0, :cond_7

    .line 113
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v2, :cond_7

    .line 114
    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v3, "CurrentConnectType(%s) == newConnectType(%s)."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_7
    :goto_1
    sput-object v0, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    .line 128
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 118
    :cond_8
    :try_start_2
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v2, :cond_9

    .line 119
    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a:Ljava/lang/String;

    const-string v3, "CurrentConnectType(%s) != newConnectType(%s)."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_9
    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->c:Lcom/sec/chaton/push/receiver/b;

    if-eqz v2, :cond_7

    .line 123
    sget-object v2, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->c:Lcom/sec/chaton/push/receiver/b;

    sget-object v3, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->b:Lcom/sec/chaton/push/util/f;

    invoke-interface {v2, v3, v0}, Lcom/sec/chaton/push/receiver/b;->a(Lcom/sec/chaton/push/util/f;Lcom/sec/chaton/push/util/f;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1
.end method
