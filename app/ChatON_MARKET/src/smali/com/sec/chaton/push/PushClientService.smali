.class public Lcom/sec/chaton/push/PushClientService;
.super Landroid/app/Service;
.source "PushClientService.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/sec/chaton/push/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 38
    const-class v0, Lcom/sec/chaton/push/PushClientService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    .line 187
    new-instance v0, Lcom/sec/chaton/push/n;

    invoke-direct {v0, p0}, Lcom/sec/chaton/push/n;-><init>(Lcom/sec/chaton/push/PushClientService;)V

    iput-object v0, p0, Lcom/sec/chaton/push/PushClientService;->b:Lcom/sec/chaton/push/f;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 180
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    const-string v1, "PushClientService.onBind()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->b:Lcom/sec/chaton/push/f;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 44
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    const-string v1, "PushClientService.onCreate()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 50
    invoke-virtual {p0}, Lcom/sec/chaton/push/PushClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 51
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 113
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    const-string v1, "PushClientService.onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 120
    invoke-virtual {p0}, Lcom/sec/chaton/push/PushClientService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/push/PushClientApplication;

    .line 121
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/PushClientApplication;->a(Z)V

    .line 123
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    const-string v1, "Stop HeartBeat."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_1
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d()Z

    .line 130
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/e;->b()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/m;

    invoke-direct {v1, p0}, Lcom/sec/chaton/push/m;-><init>(Lcom/sec/chaton/push/PushClientService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 176
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 55
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    const-string v3, "PushClientService.onStartCommand()"

    invoke-static {v0, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 61
    invoke-virtual {p0}, Lcom/sec/chaton/push/PushClientService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/push/PushClientApplication;

    .line 62
    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/PushClientApplication;->a(Z)V

    .line 65
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/push/k;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 66
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    const-string v2, "The registered application doesn\'t exist in push module. Don\'t try to execute initalize."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/push/PushClientService;->stopSelf()V

    move v0, v1

    .line 108
    :goto_0
    return v0

    .line 73
    :cond_2
    invoke-static {}, Lcom/sec/chaton/push/util/e;->a()Lcom/sec/chaton/push/util/f;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    if-ne v0, v3, :cond_4

    .line 74
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_3

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientService;->a:Ljava/lang/String;

    const-string v2, "The network isn\'t available. Don\'t try to execute initialize."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/push/PushClientService;->stopSelf()V

    move v0, v1

    .line 80
    goto :goto_0

    .line 84
    :cond_4
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/e;->b()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/l;

    invoke-direct {v1, p0}, Lcom/sec/chaton/push/l;-><init>(Lcom/sec/chaton/push/PushClientService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move v0, v2

    .line 108
    goto :goto_0
.end method
