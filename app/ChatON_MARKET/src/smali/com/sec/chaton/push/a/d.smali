.class Lcom/sec/chaton/push/a/d;
.super Ljava/lang/Object;
.source "ErrorHandler.java"

# interfaces
.implements Lcom/sec/chaton/push/b/b/a;


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/a/b;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/a/b;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sec/chaton/push/a/d;->a:Lcom/sec/chaton/push/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 311
    return-void
.end method

.method public a(ZLjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 287
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 288
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Push IChannelListener.onChannelDisconnected()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/c/e;->c()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 292
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 293
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Thread is MessageTaskExecuteThread, execute directly."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_1
    new-instance v0, Lcom/sec/chaton/push/a/j;

    invoke-direct {v0, p1, p2}, Lcom/sec/chaton/push/a/j;-><init>(ZLjava/lang/Throwable;)V

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/j;->run()V

    .line 306
    :goto_0
    return-void

    .line 300
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_3

    .line 301
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Thread isn\'t MessageTaskExecuteThread, post disconnect hander."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_3
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/e;->b()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/a/j;

    invoke-direct {v1, p1, p2}, Lcom/sec/chaton/push/a/j;-><init>(ZLjava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
