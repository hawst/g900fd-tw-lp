.class public Lcom/sec/chaton/push/PushClientApplication;
.super Lcom/sec/common/CommonApplication;
.source "PushClientApplication.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/push/PushClientApplication;


# instance fields
.field private c:Z

.field private d:Lcom/sec/chaton/push/b/a/d;

.field private e:Lcom/sec/chaton/push/b/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/chaton/push/PushClientApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/PushClientApplication;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/common/CommonApplication;-><init>()V

    return-void
.end method

.method public static h()Lcom/sec/chaton/push/PushClientApplication;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/chaton/push/PushClientApplication;->b:Lcom/sec/chaton/push/PushClientApplication;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/chaton/push/PushClientApplication;->c:Z

    .line 47
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/chaton/push/PushClientApplication;->c:Z

    return v0
.end method

.method public j()Lcom/sec/chaton/push/b/a/d;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientApplication;->d:Lcom/sec/chaton/push/b/a/d;

    return-object v0
.end method

.method public k()Lcom/sec/chaton/push/b/a/c;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/push/PushClientApplication;->e:Lcom/sec/chaton/push/b/a/c;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 67
    invoke-super {p0}, Lcom/sec/common/CommonApplication;->onCreate()V

    .line 69
    sput-object p0, Lcom/sec/chaton/push/PushClientApplication;->b:Lcom/sec/chaton/push/PushClientApplication;

    .line 70
    iput-boolean v4, p0, Lcom/sec/chaton/push/PushClientApplication;->c:Z

    .line 73
    invoke-static {p0}, Lcom/sec/chaton/push/util/g;->a(Landroid/content/Context;)V

    .line 75
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 76
    sget-object v0, Lcom/sec/chaton/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "PushClientApplication.onCreate()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_1

    .line 80
    sget-object v0, Lcom/sec/chaton/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "PushModule version: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "0.9.01.12"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_1
    invoke-static {p0}, Lcom/sec/chaton/push/util/i;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 85
    sget-object v0, Lcom/sec/chaton/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "Discovered public push client. disable in app push client."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_2
    invoke-static {p0, v4}, Lcom/sec/chaton/push/util/i;->a(Landroid/content/Context;Z)V

    .line 115
    :goto_0
    return-void

    .line 92
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_4

    .line 93
    sget-object v0, Lcom/sec/chaton/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "Couldn\'t discover public push client. enable in app push client."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_4
    invoke-static {p0, v4}, Lcom/sec/chaton/push/util/i;->b(Landroid/content/Context;Z)V

    .line 100
    new-instance v0, Lcom/sec/chaton/push/b/a/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/push/b/a/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/push/PushClientApplication;->e:Lcom/sec/chaton/push/b/a/c;

    .line 101
    new-instance v0, Lcom/sec/chaton/push/b/a/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/push/b/a/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/push/PushClientApplication;->d:Lcom/sec/chaton/push/b/a/d;

    .line 104
    invoke-static {}, Lcom/sec/chaton/push/util/AlarmTimer;->a()Lcom/sec/chaton/push/util/AlarmTimer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/push/PushClientApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/util/AlarmTimer;->a(Landroid/content/Context;)V

    .line 107
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a(Lcom/sec/chaton/push/receiver/b;)V

    .line 108
    invoke-static {}, Lcom/sec/chaton/push/receiver/SystemStateMoniter;->a()V

    .line 111
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    .line 114
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/c/a/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/push/c/a/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/c;)V

    goto :goto_0
.end method
