.class public Lcom/sec/chaton/push/c/h;
.super Ljava/lang/Object;
.source "MessageTaskDispatcher.java"


# instance fields
.field private a:Lcom/sec/chaton/push/b/a/a;

.field private b:Lcom/google/protobuf/GeneratedMessageLite;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/push/b/a/a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/push/c/h;->a:Lcom/sec/chaton/push/b/a/a;

    return-object v0
.end method

.method public a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/push/c/h;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/chaton/push/c/h;->b:Lcom/google/protobuf/GeneratedMessageLite;

    .line 54
    return-object p0
.end method

.method public a(Lcom/sec/chaton/push/b/a/a;)Lcom/sec/chaton/push/c/h;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/push/c/h;->a:Lcom/sec/chaton/push/b/a/a;

    .line 45
    return-object p0
.end method

.method public b()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/push/c/h;->b:Lcom/google/protobuf/GeneratedMessageLite;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    const-string v0, "TaskOperation(Connection: %s, Request: %s)."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/push/c/h;->a:Lcom/sec/chaton/push/b/a/a;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/push/c/h;->b:Lcom/google/protobuf/GeneratedMessageLite;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
