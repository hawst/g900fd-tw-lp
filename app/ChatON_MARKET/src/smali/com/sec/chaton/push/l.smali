.class Lcom/sec/chaton/push/l;
.super Ljava/lang/Object;
.source "PushClientService.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/PushClientService;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/PushClientService;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/chaton/push/l;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 88
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 89
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/push/l;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v0}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushModule hasn\'t been initialized. Execute initialize."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/c/a/b;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V

    .line 104
    :cond_1
    :goto_0
    return-void

    .line 95
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/push/l;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v0}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushModule has been initialized."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_1

    .line 101
    iget-object v1, p0, Lcom/sec/chaton/push/l;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
