.class Lcom/sec/chaton/push/receiver/a;
.super Ljava/lang/Object;
.source "AutoRestartReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/chaton/push/receiver/AutoRestartReceiver;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/receiver/AutoRestartReceiver;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/chaton/push/receiver/a;->b:Lcom/sec/chaton/push/receiver/AutoRestartReceiver;

    iput-object p2, p0, Lcom/sec/chaton/push/receiver/a;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_0

    .line 58
    invoke-static {}, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushModule is started but isn\'t initialized yet. Execute initialize."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/c/a/b;

    iget-object v2, p0, Lcom/sec/chaton/push/receiver/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :cond_1
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_1

    .line 65
    invoke-static {}, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
