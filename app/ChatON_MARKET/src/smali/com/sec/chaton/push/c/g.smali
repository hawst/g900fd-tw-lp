.class Lcom/sec/chaton/push/c/g;
.super Landroid/os/Handler;
.source "MessageTaskDispatcher.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/c/e;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/push/c/e;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    .line 380
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 381
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 385
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 387
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/push/c/a;

    .line 389
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 390
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Dispath message task. %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_0
    instance-of v1, v0, Lcom/sec/chaton/push/c/d;

    if-eqz v1, :cond_5

    .line 394
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 395
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "This message task needs response message."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/push/c/d;

    invoke-static {v2, v1}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;

    .line 408
    :goto_0
    invoke-virtual {v0}, Lcom/sec/chaton/push/c/a;->c()Lcom/sec/chaton/push/c/h;

    move-result-object v0

    .line 410
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_2

    .line 411
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TaskOperation is %s."

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_2
    if-nez v0, :cond_7

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0, v7}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;

    .line 439
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->b(Lcom/sec/chaton/push/c/e;)Lcom/sec/chaton/push/c/d;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 440
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_4

    .line 441
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Setting message response timeout countdown wait %d seconds."

    new-array v2, v6, [Ljava/lang/Object;

    const-wide/16 v3, 0xb4

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, -0x1

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->c(Lcom/sec/chaton/push/c/e;)V

    .line 459
    :goto_2
    return-void

    .line 400
    :cond_5
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_6

    .line 401
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "This message task doesn\'t need response message."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1, v7}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;

    goto :goto_0

    .line 419
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/push/c/h;->a()Lcom/sec/chaton/push/b/a/a;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/h;->b()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    if-nez v1, :cond_a

    .line 420
    :cond_8
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_9

    .line 421
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The TaskOperation is invaild.( ConnectionManager: %s. requestMessage: %s )"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/h;->a()Lcom/sec/chaton/push/b/a/a;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/h;->b()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0, v7}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;

    goto :goto_1

    .line 428
    :cond_a
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/push/c/h;->a()Lcom/sec/chaton/push/b/a/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/h;->b()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/push/b/a/a;->a(Lcom/google/protobuf/GeneratedMessageLite;)V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/k; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 429
    :catch_0
    move-exception v0

    .line 430
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1, v7}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;

    .line 432
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_3

    .line 433
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/k;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 451
    :cond_b
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_c

    .line 452
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Wake up MessageTaskDispatchThread."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->d(Lcom/sec/chaton/push/c/e;)Ljava/lang/Thread;

    move-result-object v1

    monitor-enter v1

    .line 456
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->d(Lcom/sec/chaton/push/c/e;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 457
    monitor-exit v1

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 462
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/protobuf/GeneratedMessageLite;

    .line 464
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 465
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Received response message. %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->e(Lcom/sec/chaton/push/c/e;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 469
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->e(Lcom/sec/chaton/push/c/e;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/push/c/c;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/c;->a(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 505
    :cond_1
    :goto_0
    return-void

    .line 474
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->b(Lcom/sec/chaton/push/c/e;)Lcom/sec/chaton/push/c/d;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->b(Lcom/sec/chaton/push/c/e;)Lcom/sec/chaton/push/c/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/chaton/push/c/d;->f_()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 475
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_3

    .line 476
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ResponseMessageTask.getHandleMessageClass() == %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 483
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->b(Lcom/sec/chaton/push/c/e;)Lcom/sec/chaton/push/c/d;

    move-result-object v1

    .line 484
    iget-object v2, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;

    .line 486
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1, v2, v0}, Lcom/sec/chaton/push/c/d;->a(ILcom/google/protobuf/GeneratedMessageLite;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->f(Lcom/sec/chaton/push/c/e;)V

    .line 492
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_4

    .line 493
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Wake up MessageTaskDispatchThread."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->d(Lcom/sec/chaton/push/c/e;)Ljava/lang/Thread;

    move-result-object v1

    monitor-enter v1

    .line 497
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->d(Lcom/sec/chaton/push/c/e;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 498
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 489
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->f(Lcom/sec/chaton/push/c/e;)V

    throw v0

    .line 500
    :cond_5
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v1, :cond_1

    .line 501
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ResponseMessageTask.getHandleMessageClass() != %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Throw away this response message."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private c(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 508
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 509
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Timeout."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->b(Lcom/sec/chaton/push/c/e;)Lcom/sec/chaton/push/c/d;

    move-result-object v0

    if-nez v0, :cond_2

    .line 513
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 514
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "The response message task is gone."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_1
    :goto_0
    return-void

    .line 521
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->b(Lcom/sec/chaton/push/c/e;)Lcom/sec/chaton/push/c/d;

    move-result-object v0

    .line 522
    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1, v2}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;

    .line 524
    const/4 v1, -0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/push/c/d;->a(ILcom/google/protobuf/GeneratedMessageLite;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->f(Lcom/sec/chaton/push/c/e;)V

    .line 530
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_3

    .line 531
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Wake up MessageTaskDispatchThread."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->d(Lcom/sec/chaton/push/c/e;)Ljava/lang/Thread;

    move-result-object v1

    monitor-enter v1

    .line 535
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->d(Lcom/sec/chaton/push/c/e;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 536
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 527
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->f(Lcom/sec/chaton/push/c/e;)V

    throw v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 541
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->g(Lcom/sec/chaton/push/c/e;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 544
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;)Landroid/os/Handler;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 545
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_0

    .line 546
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "This MessageTaskExecuteHandler is dead."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 586
    :goto_0
    return-void

    .line 553
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->c(Lcom/sec/chaton/push/c/e;)V

    .line 555
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 576
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_2

    .line 577
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Unknown message.what => %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 583
    :cond_2
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v0}, Lcom/sec/chaton/push/c/e;->f(Lcom/sec/chaton/push/c/e;)V

    .line 585
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 558
    :pswitch_0
    :try_start_3
    invoke-direct {p0, p1}, Lcom/sec/chaton/push/c/g;->a(Landroid/os/Message;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 583
    :catchall_1
    move-exception v0

    :try_start_4
    iget-object v2, p0, Lcom/sec/chaton/push/c/g;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v2}, Lcom/sec/chaton/push/c/e;->f(Lcom/sec/chaton/push/c/e;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 564
    :pswitch_1
    :try_start_5
    invoke-direct {p0, p1}, Lcom/sec/chaton/push/c/g;->b(Landroid/os/Message;)V

    goto :goto_1

    .line 570
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/push/c/g;->c(Landroid/os/Message;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 555
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
