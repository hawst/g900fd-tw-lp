.class Lcom/sec/chaton/push/c/f;
.super Ljava/lang/Thread;
.source "MessageTaskDispatcher.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/c/e;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/push/c/e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/sec/chaton/push/c/f;->a:Lcom/sec/chaton/push/c/e;

    .line 328
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 329
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 341
    :goto_0
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_0

    .line 342
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Wating for taking message task from MessageTaskQueue."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/i;->b()Lcom/sec/chaton/push/c/a;

    move-result-object v0

    .line 347
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 348
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Forward message task to MessageTaskExecuteHandler. task: %s. "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/push/c/f;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v1}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/push/c/f;->a:Lcom/sec/chaton/push/c/e;

    invoke-static {v2}, Lcom/sec/chaton/push/c/e;->a(Lcom/sec/chaton/push/c/e;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    .line 353
    if-nez v0, :cond_3

    .line 356
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_3

    .line 357
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Couldn\'t forward task to MessageTaskExecuteHandler. Is Message Task Execute Handler quit?"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Quit MessageTaskDispatcherThread."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_2
    :goto_1
    return-void

    .line 364
    :cond_3
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 367
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 369
    :catch_0
    move-exception v0

    .line 370
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 371
    invoke-static {}, Lcom/sec/chaton/push/c/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
