.class Lcom/sec/chaton/push/n;
.super Lcom/sec/chaton/push/f;
.source "PushClientService.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/PushClientService;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/PushClientService;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-direct {p0}, Lcom/sec/chaton/push/f;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 302
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v0}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushClientService.ackNotification()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_0
    if-nez p1, :cond_1

    .line 308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Notification id shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 312
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/c/a/c;

    iget-object v3, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-virtual {v3}, Lcom/sec/chaton/push/PushClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/sec/chaton/push/c/a/c;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :cond_2
    :goto_0
    return-void

    .line 316
    :catch_0
    move-exception v0

    .line 317
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 318
    iget-object v1, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/push/b;)V
    .locals 3

    .prologue
    .line 253
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v0}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushClientService.deregistration()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    if-nez p1, :cond_1

    .line 259
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Application id shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_1
    new-instance v0, Lcom/sec/chaton/push/c/a/a;

    iget-object v1, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-virtual {v1}, Lcom/sec/chaton/push/PushClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/push/c/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 263
    new-instance v1, Lcom/sec/chaton/push/p;

    invoke-direct {v1, p0, p2}, Lcom/sec/chaton/push/p;-><init>(Lcom/sec/chaton/push/n;Lcom/sec/chaton/push/b;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/a/a;->a(Lcom/sec/chaton/push/c/b;)V

    .line 292
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :cond_2
    :goto_0
    return-void

    .line 293
    :catch_0
    move-exception v0

    .line 294
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 295
    iget-object v1, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/push/h;)V
    .locals 3

    .prologue
    .line 190
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v0}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushClientService.registration()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_0
    if-nez p1, :cond_1

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Application id shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_1
    :try_start_0
    new-instance v0, Lcom/sec/chaton/push/c/a/h;

    iget-object v1, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-virtual {v1}, Lcom/sec/chaton/push/PushClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/push/c/a/h;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 201
    new-instance v1, Lcom/sec/chaton/push/o;

    invoke-direct {v1, p0, p2}, Lcom/sec/chaton/push/o;-><init>(Lcom/sec/chaton/push/n;Lcom/sec/chaton/push/h;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/a/h;->a(Lcom/sec/chaton/push/c/b;)V

    .line 229
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :cond_2
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v0

    .line 231
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 232
    iget-object v1, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 325
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 326
    iget-object v1, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PushClientService.isPushAvailable()"

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_0
    invoke-static {v0}, Lcom/sec/chaton/push/c/a/b;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330
    const/4 v0, 0x1

    .line 332
    :cond_1
    return v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 239
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v0}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushClientService.getRegId()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_0
    if-nez p1, :cond_1

    .line 245
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Application id shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/push/k;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 339
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v0}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PushClientService.getWifiPort()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    const/16 v0, 0x1467

    return v0
.end method
