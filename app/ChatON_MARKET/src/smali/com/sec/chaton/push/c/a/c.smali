.class public Lcom/sec/chaton/push/c/a/c;
.super Lcom/sec/chaton/push/c/a;
.source "NotiAcksMessageTask.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/push/b/a/d;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/chaton/push/c/a/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/a/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    const-wide/16 v0, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/push/c/a;-><init>(Landroid/content/Context;J)V

    .line 36
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/push/PushClientApplication;

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/c;->b:Lcom/sec/chaton/push/b/a/d;

    .line 37
    iput-object p2, p0, Lcom/sec/chaton/push/c/a/c;->c:Ljava/util/List;

    .line 38
    return-void
.end method


# virtual methods
.method public c()Lcom/sec/chaton/push/c/h;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 42
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 43
    sget-object v1, Lcom/sec/chaton/push/c/a/c;->a:Ljava/lang/String;

    const-string v2, "NotiAcksMessageTask.onRequest()."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 48
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 49
    sget-object v1, Lcom/sec/chaton/push/c/a/c;->a:Ljava/lang/String;

    const-string v2, "Initialize isn\'t completed. Execute initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/c/a/b;

    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/c;->a()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V

    .line 55
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_2
    :goto_0
    return-object v0

    .line 56
    :catch_0
    move-exception v1

    .line 57
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_2

    .line 58
    sget-object v2, Lcom/sec/chaton/push/c/a/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 65
    :cond_3
    invoke-static {}, Lcom/sec/a/a/a/o;->newBuilder()Lcom/sec/a/a/a/p;

    move-result-object v1

    .line 66
    invoke-static {v0}, Lcom/sec/chaton/push/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/p;->a(Ljava/lang/String;)Lcom/sec/a/a/a/p;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 69
    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/p;->b(Ljava/lang/String;)Lcom/sec/a/a/a/p;

    goto :goto_1

    .line 72
    :cond_4
    new-instance v0, Lcom/sec/chaton/push/c/h;

    invoke-direct {v0}, Lcom/sec/chaton/push/c/h;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/push/c/a/c;->b:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/c/h;->a(Lcom/sec/chaton/push/b/a/a;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/a/a/a/p;->d()Lcom/sec/a/a/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/h;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    goto :goto_0
.end method
