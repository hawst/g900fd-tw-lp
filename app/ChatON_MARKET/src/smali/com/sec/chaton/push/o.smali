.class Lcom/sec/chaton/push/o;
.super Ljava/lang/Object;
.source "PushClientService.java"

# interfaces
.implements Lcom/sec/chaton/push/c/b;


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/h;

.field final synthetic b:Lcom/sec/chaton/push/n;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/n;Lcom/sec/chaton/push/h;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/chaton/push/o;->b:Lcom/sec/chaton/push/n;

    iput-object p2, p0, Lcom/sec/chaton/push/o;->a:Lcom/sec/chaton/push/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/chaton/push/o;->a:Lcom/sec/chaton/push/h;

    if-eqz v0, :cond_0

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/o;->a:Lcom/sec/chaton/push/h;

    const-string v1, "Registration success"

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/chaton/push/k;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/push/h;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v0

    .line 208
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/sec/chaton/push/o;->b:Lcom/sec/chaton/push/n;

    iget-object v1, v1, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/chaton/push/o;->a:Lcom/sec/chaton/push/h;

    if-eqz v0, :cond_0

    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/o;->a:Lcom/sec/chaton/push/h;

    const-string v1, "Registration fail."

    invoke-interface {v0, p1, v1}, Lcom/sec/chaton/push/h;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 221
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/push/o;->b:Lcom/sec/chaton/push/n;

    iget-object v1, v1, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
