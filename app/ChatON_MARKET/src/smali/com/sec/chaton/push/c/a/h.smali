.class public Lcom/sec/chaton/push/c/a/h;
.super Lcom/sec/chaton/push/c/a;
.source "RegistrationMessageTask.java"

# interfaces
.implements Lcom/sec/chaton/push/c/d;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/chaton/push/b/a/d;

.field private e:Lcom/sec/chaton/push/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/chaton/push/c/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    const-wide/16 v0, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/push/c/a;-><init>(Landroid/content/Context;J)V

    .line 49
    iput-object p2, p0, Lcom/sec/chaton/push/c/a/h;->c:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/push/PushClientApplication;

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/h;->d:Lcom/sec/chaton/push/b/a/d;

    .line 51
    return-void
.end method


# virtual methods
.method public a(ILcom/google/protobuf/GeneratedMessageLite;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 106
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 107
    sget-object v0, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v1, "RegistrationMessageTask.onResponse(). ErrorCode: %d."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_0
    if-nez p1, :cond_e

    .line 112
    if-nez p2, :cond_2

    .line 113
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_1

    .line 114
    sget-object v0, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v1, "Response message is null."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_1
    :goto_0
    return-void

    .line 120
    :cond_2
    check-cast p2, Lcom/sec/a/a/a/aj;

    .line 121
    invoke-virtual {p2}, Lcom/sec/a/a/a/aj;->f()I

    move-result v0

    .line 123
    iget v1, p0, Lcom/sec/chaton/push/c/a/h;->b:I

    invoke-virtual {p2}, Lcom/sec/a/a/a/aj;->d()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 124
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 125
    sget-object v0, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v1, "Request Async Id(%d) != Response Async Id(%d)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/push/c/a/h;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p2}, Lcom/sec/a/a/a/aj;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_3
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_4

    .line 133
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/push/k;->b(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Lcom/sec/a/a/a/aj;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/k;->c(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 136
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 139
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/h;->a()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0}, Lcom/sec/chaton/push/c/b;->a()V

    goto :goto_0

    .line 145
    :cond_4
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_5

    .line 146
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Registration reply result code is error. ErrorCode: %d."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_5
    const/16 v1, 0xfa2

    if-ne v0, v1, :cond_7

    .line 150
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_6

    .line 151
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Server error: empty device token."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Close connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_6
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_3

    .line 161
    :goto_1
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 162
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->e()V

    .line 164
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 169
    :cond_7
    const/16 v1, 0xfa3

    if-ne v0, v1, :cond_9

    .line 170
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_8

    .line 171
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Server error: empty application id."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Do not anything."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_8
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_1
    .catch Lcom/sec/chaton/push/a/a; {:try_start_1 .. :try_end_1} :catch_2

    .line 181
    :goto_2
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->e()V

    .line 183
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 188
    :cond_9
    const/16 v1, 0xfa1

    if-ne v0, v1, :cond_b

    .line 189
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_a

    .line 190
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Server error: empty device token."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Close connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_a
    :try_start_2
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_2
    .catch Lcom/sec/chaton/push/a/a; {:try_start_2 .. :try_end_2} :catch_1

    .line 200
    :goto_3
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->e()V

    .line 202
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 203
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 207
    :cond_b
    const/16 v1, 0xfa7

    if-ne v0, v1, :cond_d

    .line 208
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_c

    .line 209
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Server error: registration failed."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Do not anything."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 214
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 219
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 225
    :cond_e
    const/4 v0, -0x1

    if-ne p1, v0, :cond_11

    .line 227
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_f

    .line 228
    sget-object v0, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v1, "Internal error code is timeout."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_f
    :try_start_3
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_10

    .line 233
    sget-object v0, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v1, "Disconnect connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/h;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_3
    .catch Lcom/sec/chaton/push/a/a; {:try_start_3 .. :try_end_3} :catch_0

    .line 241
    :goto_4
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    .line 244
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, p1}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 237
    :catch_0
    move-exception v0

    goto :goto_4

    .line 196
    :catch_1
    move-exception v1

    goto :goto_3

    .line 177
    :catch_2
    move-exception v1

    goto/16 :goto_2

    .line 157
    :catch_3
    move-exception v1

    goto/16 :goto_1
.end method

.method public a(Lcom/sec/chaton/push/c/b;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/push/c/a/h;->e:Lcom/sec/chaton/push/c/b;

    .line 66
    return-void
.end method

.method public c()Lcom/sec/chaton/push/c/h;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 70
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 71
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "RegistrationMessageTask.onRequest()."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 76
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 77
    sget-object v1, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    const-string v2, "Initialize isn\'t completed. Execute initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/c/a/b;

    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/h;->a()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V

    .line 83
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_2
    :goto_0
    return-object v0

    .line 84
    :catch_0
    move-exception v1

    .line 85
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_2

    .line 86
    sget-object v2, Lcom/sec/chaton/push/c/a/h;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 93
    :cond_3
    invoke-static {}, Lcom/sec/chaton/push/util/d;->a()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/push/c/a/h;->b:I

    .line 95
    invoke-static {}, Lcom/sec/a/a/a/am;->newBuilder()Lcom/sec/a/a/a/an;

    move-result-object v1

    .line 96
    iget v2, p0, Lcom/sec/chaton/push/c/a/h;->b:I

    invoke-virtual {v1, v2}, Lcom/sec/a/a/a/an;->a(I)Lcom/sec/a/a/a/an;

    .line 97
    invoke-static {v0}, Lcom/sec/chaton/push/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/an;->a(Ljava/lang/String;)Lcom/sec/a/a/a/an;

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/an;->b(Ljava/lang/String;)Lcom/sec/a/a/a/an;

    .line 99
    const-string v0, ""

    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/an;->c(Ljava/lang/String;)Lcom/sec/a/a/a/an;

    .line 101
    new-instance v0, Lcom/sec/chaton/push/c/h;

    invoke-direct {v0}, Lcom/sec/chaton/push/c/h;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/push/c/a/h;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/c/h;->a(Lcom/sec/chaton/push/b/a/a;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/a/a/a/an;->d()Lcom/sec/a/a/a/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/h;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    goto :goto_0
.end method

.method public f_()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    const-class v0, Lcom/sec/a/a/a/aj;

    return-object v0
.end method
