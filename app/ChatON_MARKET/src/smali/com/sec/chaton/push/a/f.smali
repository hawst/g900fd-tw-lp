.class Lcom/sec/chaton/push/a/f;
.super Ljava/lang/Object;
.source "ErrorHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcom/sec/chaton/push/util/f;

.field private b:Lcom/sec/chaton/push/util/f;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/push/util/f;Lcom/sec/chaton/push/util/f;)V
    .locals 0

    .prologue
    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    iput-object p1, p0, Lcom/sec/chaton/push/a/f;->a:Lcom/sec/chaton/push/util/f;

    .line 405
    iput-object p2, p0, Lcom/sec/chaton/push/a/f;->b:Lcom/sec/chaton/push/util/f;

    .line 406
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const-wide/16 v7, 0x5

    const/4 v6, -0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 410
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 411
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MessageController.onNetworkChanged()."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OldNetworkInfo: %s, NewNetworkInfo: %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/push/a/f;->a:Lcom/sec/chaton/push/util/f;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/sec/chaton/push/a/f;->b:Lcom/sec/chaton/push/util/f;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/util/AlarmTimer;->a()Lcom/sec/chaton/push/util/AlarmTimer;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "connectivityChanged"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/push/util/AlarmTimer;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 418
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 419
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Stop HeartBeat."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_1
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d()Z

    .line 424
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 425
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Try to disconnect provisioning connection and push connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->k()Lcom/sec/chaton/push/b/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/c;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_1

    .line 435
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_1
    .catch Lcom/sec/chaton/push/a/a; {:try_start_1 .. :try_end_1} :catch_0

    .line 440
    :goto_1
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/chaton/push/a/b;->a(I)V

    .line 442
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/chaton/push/c/e;->a(I)V

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/push/a/f;->b:Lcom/sec/chaton/push/util/f;

    sget-object v1, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    if-ne v0, v1, :cond_4

    .line 447
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_3

    .line 448
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No Active Internet. Stop push module after %d seconds."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :cond_3
    new-instance v5, Lcom/sec/chaton/push/a/g;

    invoke-direct {v5, p0}, Lcom/sec/chaton/push/a/g;-><init>(Lcom/sec/chaton/push/a/f;)V

    .line 470
    :goto_2
    invoke-static {}, Lcom/sec/chaton/push/util/AlarmTimer;->a()Lcom/sec/chaton/push/util/AlarmTimer;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "connectivityChanged"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/16 v6, 0x1388

    add-long/2addr v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/push/util/AlarmTimer;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/chaton/push/util/b;)V

    .line 471
    return-void

    .line 458
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_5

    .line 459
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Start push module after %d seconds."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_5
    new-instance v5, Lcom/sec/chaton/push/a/h;

    invoke-direct {v5, p0}, Lcom/sec/chaton/push/a/h;-><init>(Lcom/sec/chaton/push/a/f;)V

    goto :goto_2

    .line 436
    :catch_0
    move-exception v0

    goto :goto_1

    .line 430
    :catch_1
    move-exception v0

    goto :goto_0
.end method
