.class public Lcom/sec/chaton/push/receiver/AutoRestartReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AutoRestartReceiver.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 28
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_0

    .line 29
    sget-object v0, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;->a:Ljava/lang/String;

    const-string v1, "AutoRestartReceiver.onReceive(). Action: %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 35
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/chaton/push/k;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 36
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_1

    .line 37
    sget-object v0, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;->a:Ljava/lang/String;

    const-string v1, "Because a registered application doesn\'t exist in push module, don\'t start push service."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_1
    :goto_0
    return-void

    .line 44
    :cond_2
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/PushClientApplication;->i()Z

    move-result v1

    if-nez v1, :cond_4

    .line 45
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v1, :cond_3

    .line 46
    sget-object v1, Lcom/sec/chaton/push/receiver/AutoRestartReceiver;->a:Ljava/lang/String;

    const-string v2, "PushModule hasn\'t been started. Probably process had been killed by system or user. Start push service."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 53
    :cond_4
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/c/e;->b()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/receiver/a;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/push/receiver/a;-><init>(Lcom/sec/chaton/push/receiver/AutoRestartReceiver;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
