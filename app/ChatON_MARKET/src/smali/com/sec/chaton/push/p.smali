.class Lcom/sec/chaton/push/p;
.super Ljava/lang/Object;
.source "PushClientService.java"

# interfaces
.implements Lcom/sec/chaton/push/c/b;


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/b;

.field final synthetic b:Lcom/sec/chaton/push/n;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/n;Lcom/sec/chaton/push/b;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/chaton/push/p;->b:Lcom/sec/chaton/push/n;

    iput-object p2, p0, Lcom/sec/chaton/push/p;->a:Lcom/sec/chaton/push/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/chaton/push/p;->a:Lcom/sec/chaton/push/b;

    if-eqz v0, :cond_0

    .line 268
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/p;->a:Lcom/sec/chaton/push/b;

    const-string v1, "Deregistration success"

    invoke-interface {v0, v1}, Lcom/sec/chaton/push/b;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 269
    :catch_0
    move-exception v0

    .line 270
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/sec/chaton/push/p;->b:Lcom/sec/chaton/push/n;

    iget-object v1, v1, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/chaton/push/p;->a:Lcom/sec/chaton/push/b;

    if-eqz v0, :cond_0

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/p;->a:Lcom/sec/chaton/push/b;

    const-string v1, "Deregistration fail."

    invoke-interface {v0, p1, v1}, Lcom/sec/chaton/push/b;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 282
    :catch_0
    move-exception v0

    .line 283
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_0

    .line 284
    iget-object v1, p0, Lcom/sec/chaton/push/p;->b:Lcom/sec/chaton/push/n;

    iget-object v1, v1, Lcom/sec/chaton/push/n;->a:Lcom/sec/chaton/push/PushClientService;

    invoke-static {v1}, Lcom/sec/chaton/push/PushClientService;->a(Lcom/sec/chaton/push/PushClientService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
