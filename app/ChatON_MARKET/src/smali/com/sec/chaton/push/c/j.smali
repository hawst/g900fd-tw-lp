.class Lcom/sec/chaton/push/c/j;
.super Ljava/lang/Object;
.source "MessageTaskQueue.java"

# interfaces
.implements Lcom/sec/chaton/push/util/b;


# instance fields
.field final synthetic a:Lcom/sec/chaton/push/c/a;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/sec/chaton/push/c/i;


# direct methods
.method constructor <init>(Lcom/sec/chaton/push/c/i;Lcom/sec/chaton/push/c/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/chaton/push/c/j;->c:Lcom/sec/chaton/push/c/i;

    iput-object p2, p0, Lcom/sec/chaton/push/c/j;->a:Lcom/sec/chaton/push/c/a;

    iput-object p3, p0, Lcom/sec/chaton/push/c/j;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 71
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 72
    invoke-static {}, Lcom/sec/chaton/push/c/i;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Put delayed task to MessageTaskQueue. task: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/push/c/j;->a:Lcom/sec/chaton/push/c/a;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->e()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/j;->c:Lcom/sec/chaton/push/c/i;

    invoke-static {v0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/i;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/push/c/j;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 77
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/push/c/j;->c:Lcom/sec/chaton/push/c/i;

    iget-object v1, p0, Lcom/sec/chaton/push/c/j;->a:Lcom/sec/chaton/push/c/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 85
    :cond_1
    :goto_0
    return-void

    .line 77
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_1

    .line 82
    invoke-static {}, Lcom/sec/chaton/push/c/i;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
