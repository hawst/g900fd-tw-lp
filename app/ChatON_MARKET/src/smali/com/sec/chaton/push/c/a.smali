.class public abstract Lcom/sec/chaton/push/c/a;
.super Ljava/lang/Object;
.source "AbstractMessageTask.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/chaton/push/c/a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private final b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/chaton/push/c/a;->a:Landroid/content/Context;

    .line 28
    iput-wide p2, p0, Lcom/sec/chaton/push/c/a;->b:J

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/chaton/push/c/a;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 45
    iget-wide v0, p0, Lcom/sec/chaton/push/c/a;->b:J

    invoke-virtual {p1}, Lcom/sec/chaton/push/c/a;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 47
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    .line 48
    const/4 v0, -0x1

    .line 52
    :goto_0
    return v0

    .line 49
    :cond_0
    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 50
    const/4 v0, 0x0

    goto :goto_0

    .line 52
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/push/c/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/sec/chaton/push/c/a;->b:J

    return-wide v0
.end method

.method public abstract c()Lcom/sec/chaton/push/c/h;
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lcom/sec/chaton/push/c/a;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/push/c/a;->a(Lcom/sec/chaton/push/c/a;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 60
    const-string v0, "%s(Priority: %d)."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/sec/chaton/push/c/a;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
