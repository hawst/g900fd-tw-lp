.class final Lcom/sec/chaton/push/heartbeat/a;
.super Ljava/lang/Object;
.source "HeartBeat.java"

# interfaces
.implements Lcom/sec/chaton/push/c/b;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 62
    const-class v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v1

    .line 64
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    monitor-exit v1

    .line 96
    :goto_0
    return-void

    .line 68
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 69
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Success send ping."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h()Lcom/sec/chaton/push/util/f;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v0, v2, :cond_4

    .line 74
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 75
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Check ping interval expired."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_2
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_3

    .line 80
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Ping interval is expired. Reset fail count."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a(I)I

    .line 85
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 86
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/push/k;->d(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 87
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->k()I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/chaton/push/k;->a(Landroid/content/SharedPreferences$Editor;I)V

    .line 88
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 93
    :cond_4
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a(Z)V

    .line 94
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->l()V

    .line 95
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 100
    const-class v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v1

    .line 102
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    monitor-exit v1

    .line 131
    :goto_0
    return-void

    .line 106
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_1

    .line 107
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Fail send ping."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Ping error code is %d. Decrease ping interval."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_1
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h()Lcom/sec/chaton/push/util/f;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v0, v2, :cond_4

    .line 113
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 114
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Check ping interval expired."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_2
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 118
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_3

    .line 119
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Ping interval is expired."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_3
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 123
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/push/k;->d(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 124
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 128
    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a(Z)V

    .line 129
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->l()V

    .line 130
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
