.class public Lcom/sec/chaton/push/heartbeat/HeartBeat;
.super Landroid/content/BroadcastReceiver;
.source "HeartBeat.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Landroid/content/Context;

.field private static c:Z

.field private static d:Lcom/sec/chaton/push/util/f;

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:Lcom/sec/chaton/push/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    .line 56
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->b:Landroid/content/Context;

    .line 59
    new-instance v0, Lcom/sec/chaton/push/heartbeat/a;

    invoke-direct {v0}, Lcom/sec/chaton/push/heartbeat/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->l:Lcom/sec/chaton/push/c/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 162
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    return v0
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 34
    sput p0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    return p0
.end method

.method private static a(J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 407
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->b:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 409
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.push.HEARTBEAT_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 411
    sget-object v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->b:Landroid/content/Context;

    invoke-static {v2, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 413
    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    add-long/2addr v3, p0

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 414
    return-void
.end method

.method static synthetic a(Z)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p0}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->b(Z)V

    return-void
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 167
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    return v0
.end method

.method private static b(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 431
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 432
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "AdjustPingAlgorithm. isSuccess:%s."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_1

    .line 436
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "Current mFailCount:%d, mTopBaseCount:%d."

    new-array v2, v6, [Ljava/lang/Object;

    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_1
    if-eqz p0, :cond_6

    .line 441
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    if-nez v0, :cond_5

    .line 442
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    sget v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g:I

    if-ge v0, v1, :cond_2

    .line 443
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    .line 464
    :cond_2
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_3

    .line 465
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "New mFailCount: %d, mTopBaseCount: %d."

    new-array v2, v6, [Ljava/lang/Object;

    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_3
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    sget-object v1, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v0, v1, :cond_4

    .line 469
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->n()V

    .line 471
    :cond_4
    return-void

    .line 445
    :cond_5
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    if-ne v0, v4, :cond_2

    .line 446
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 447
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "adjustPingAlgorithm - fixed heartbeat interval."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 453
    :cond_6
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    if-nez v0, :cond_7

    .line 454
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    if-lez v0, :cond_2

    .line 455
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    .line 457
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    goto :goto_0

    .line 459
    :cond_7
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    if-ne v0, v4, :cond_2

    .line 460
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->m()V

    goto :goto_0
.end method

.method public static declared-synchronized c()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 173
    const-class v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v2

    :try_start_0
    sget-boolean v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->c:Z

    if-nez v3, :cond_6

    .line 174
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->c:Z

    .line 176
    invoke-static {}, Lcom/sec/chaton/push/util/e;->a()Lcom/sec/chaton/push/util/f;

    move-result-object v3

    sput-object v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    .line 192
    sget-object v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    sget-object v4, Lcom/sec/chaton/push/util/f;->a:Lcom/sec/chaton/push/util/f;

    if-ne v3, v4, :cond_1

    .line 193
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v3, :cond_3

    .line 194
    sget-object v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v3, "No active internet. Couldn\'t start heartbeat."

    invoke-static {v1, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :cond_0
    :goto_0
    monitor-exit v2

    return v0

    .line 199
    :cond_1
    :try_start_1
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    sget-object v3, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v0, v3, :cond_4

    .line 200
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 201
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v3, "HeartBeat is started with mobile. Load ping variables."

    invoke-static {v0, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_2
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->o()V

    .line 218
    :cond_3
    :goto_1
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->p()V

    move v0, v1

    .line 219
    goto :goto_0

    .line 210
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_5

    .line 211
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v3, "HeartBeat is started with not mobile. Initialize ping variables."

    invoke-static {v0, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_5
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->m()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 221
    :cond_6
    :try_start_2
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v1, :cond_0

    .line 222
    sget-object v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v3, "HeartBeat already was started."

    invoke-static {v1, v3}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized d()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 232
    const-class v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v1

    :try_start_0
    sget-boolean v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->c:Z

    if-eqz v2, :cond_1

    .line 233
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->c:Z

    .line 235
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    .line 241
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    const/4 v0, 0x1

    .line 249
    :cond_0
    :goto_0
    monitor-exit v1

    return v0

    .line 245
    :cond_1
    :try_start_1
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v2, :cond_0

    .line 246
    sget-object v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v3, "HeartBeat already was stopped."

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized e()V
    .locals 3

    .prologue
    .line 256
    const-class v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->c:Z

    if-eqz v0, :cond_1

    .line 257
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 259
    :cond_1
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_0

    .line 260
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v2, "HeartBeat already was stopped."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic f()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->c:Z

    return v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/push/util/f;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    return-object v0
.end method

.method static synthetic i()Z
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->r()Z

    move-result v0

    return v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    return v0
.end method

.method static synthetic l()V
    .locals 0

    .prologue
    .line 34
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->p()V

    return-void
.end method

.method private static declared-synchronized m()V
    .locals 3

    .prologue
    .line 268
    const-class v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    .line 269
    const/4 v0, 0x0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    .line 271
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    sget-object v2, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v0, v2, :cond_0

    .line 272
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/chaton/push/k;->d(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->j:I

    .line 273
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/chaton/push/k;->e(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->k:I

    .line 274
    const/16 v0, 0x18

    invoke-static {v0}, Lcom/sec/chaton/push/k;->f(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i:I

    .line 275
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/chaton/push/k;->g(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    .line 283
    :goto_0
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->k:I

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    div-int/2addr v0, v2

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    .line 284
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i:I

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    div-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit v1

    return-void

    .line 277
    :cond_0
    const/4 v0, 0x4

    :try_start_1
    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->j:I

    .line 278
    const/4 v0, 0x4

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->k:I

    .line 279
    const/16 v0, 0x18

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i:I

    .line 280
    const/4 v0, 0x4

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized n()V
    .locals 3

    .prologue
    .line 290
    const-class v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    sget-object v2, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-eq v0, v2, :cond_1

    .line 291
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_0

    .line 292
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v2, "Ping has been started with not mobile. don\'t save ping variables."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 306
    :cond_1
    :try_start_1
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    invoke-static {v0, v2}, Lcom/sec/chaton/push/k;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized o()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 312
    const-class v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    sget-object v2, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-eq v0, v2, :cond_1

    .line 313
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_0

    .line 314
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v2, "Ping has been started with not mobile. don\'t load ping variables."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 320
    :cond_1
    const/4 v0, 0x4

    :try_start_1
    invoke-static {v0}, Lcom/sec/chaton/push/k;->d(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->j:I

    .line 321
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/chaton/push/k;->e(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->k:I

    .line 322
    const/16 v0, 0x18

    invoke-static {v0}, Lcom/sec/chaton/push/k;->f(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i:I

    .line 323
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/chaton/push/k;->g(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    .line 325
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/push/k;->h(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    .line 327
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/sec/chaton/push/k;->i(I)I

    move-result v0

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    .line 329
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    if-ne v0, v3, :cond_2

    .line 330
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->k:I

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    div-int/2addr v0, v2

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    .line 333
    :cond_2
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i:I

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    div-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized p()V
    .locals 6

    .prologue
    .line 360
    const-class v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;

    monitor-enter v2

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 361
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "Rescheduling HeartBeat."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_0
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    add-int/lit8 v0, v0, 0x1

    sget v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    mul-int/2addr v0, v1

    int-to-long v0, v0

    .line 367
    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    if-nez v3, :cond_6

    .line 370
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 371
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "The HeartBeat is first time. Using mimimum interval value."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_1
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->j:I

    int-to-long v0, v0

    .line 387
    :cond_2
    :goto_0
    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i:I

    int-to-long v3, v3

    cmp-long v3, v0, v3

    if-lez v3, :cond_4

    .line 388
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_3

    .line 389
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "The scheduled heart beat interval is bigger than max heart interval."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_3
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->i:I

    int-to-long v0, v0

    .line 395
    :cond_4
    const-wide/32 v3, 0xea60

    mul-long/2addr v0, v3

    .line 397
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v3, :cond_5

    .line 398
    sget-object v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Rescheduling... heartbeat interval: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_5
    invoke-static {v0, v1}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    monitor-exit v2

    return-void

    .line 377
    :cond_6
    :try_start_1
    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 378
    sget v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I

    int-to-long v3, v3

    cmp-long v3, v0, v3

    if-lez v3, :cond_2

    .line 379
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_7

    .line 380
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "The HeartBeat is second time. Using increment value."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_7
    sget v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long v0, v0

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static q()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 420
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->b:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 422
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.push.HEARTBEAT_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423
    sget-object v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->b:Landroid/content/Context;

    invoke-static {v2, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 425
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 426
    return-void
.end method

.method private static r()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 476
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/chaton/push/k;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 478
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 479
    :cond_0
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 480
    sget-object v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v2, "The expire date is null, set expiredate."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :cond_1
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 484
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/k;->d(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 485
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 496
    :cond_2
    :goto_0
    return v0

    .line 489
    :cond_3
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v3, :cond_4

    .line 490
    sget-object v3, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v4, "Expire date: %s. Today: %s."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v0

    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->s()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_4
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 496
    goto :goto_0
.end method

.method private static s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 502
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 508
    invoke-static {v0}, Lcom/sec/chaton/push/k;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 509
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_0

    .line 510
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "Because a registered application doesn\'t exist in push module, stop HeartBeat."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d()Z

    .line 564
    :cond_1
    :goto_0
    return-void

    .line 519
    :cond_2
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->i()Z

    move-result v0

    if-nez v0, :cond_4

    .line 520
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_3

    .line 521
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "PushModule hasn\'t been started. Probably process had been killed by system. Start service."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :cond_3
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d()Z

    .line 526
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 531
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_6

    .line 532
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "Send ping by heart beat."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "[PushModule information]"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "======================"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "PushModule Version: 0.9.01.12"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PushServer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/chaton/push/k;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, -0x1

    invoke-static {v2}, Lcom/sec/chaton/push/k;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/chaton/push/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Application id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/chaton/push/k;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Registration id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/chaton/push/k;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeartBeat count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeartBeat max count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeartBeat fail count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d:Lcom/sec/chaton/push/util/f;

    sget-object v1, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v0, v1, :cond_5

    .line 546
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NetworkOperator: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/chaton/push/k;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 551
    :cond_5
    :goto_1
    sget-object v0, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    const-string v1, "======================"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    :cond_6
    new-instance v0, Lcom/sec/chaton/push/c/a/e;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/push/c/a/e;-><init>(Landroid/content/Context;)V

    .line 556
    sget-object v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;->l:Lcom/sec/chaton/push/c/b;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/a/e;->a(Lcom/sec/chaton/push/c/b;)V

    .line 558
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 559
    :catch_0
    move-exception v0

    .line 560
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_1

    .line 561
    sget-object v1, Lcom/sec/chaton/push/heartbeat/HeartBeat;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 548
    :catch_1
    move-exception v0

    goto :goto_1
.end method
