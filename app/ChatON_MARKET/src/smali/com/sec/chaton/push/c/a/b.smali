.class public Lcom/sec/chaton/push/c/a/b;
.super Lcom/sec/chaton/push/c/a;
.source "InitMessageTask.java"

# interfaces
.implements Lcom/sec/chaton/push/c/d;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Z


# instance fields
.field private c:I

.field private d:Lcom/sec/chaton/push/b/a/d;

.field private e:Lcom/sec/chaton/push/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/chaton/push/c/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 52
    const-wide/16 v0, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/push/c/a;-><init>(Landroid/content/Context;J)V

    .line 54
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    .line 56
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 431
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/push/a/b;->a(I)V

    .line 432
    return-void
.end method

.method public static declared-synchronized a(Z)Z
    .locals 2

    .prologue
    .line 439
    const-class v1, Lcom/sec/chaton/push/c/a/b;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/push/c/a/b;->b:Z

    if-eqz v0, :cond_0

    .line 440
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/push/b/a/d;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/push/c/a/b;->b:Z

    .line 445
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/push/c/a/b;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return v0

    .line 439
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized d()Z
    .locals 2

    .prologue
    .line 450
    const-class v0, Lcom/sec/chaton/push/c/a/b;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/sec/chaton/push/c/a/b;->a(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized e()V
    .locals 2

    .prologue
    .line 456
    const-class v0, Lcom/sec/chaton/push/c/a/b;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    sput-boolean v1, Lcom/sec/chaton/push/c/a/b;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    monitor-exit v0

    return-void

    .line 456
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized f()V
    .locals 2

    .prologue
    .line 462
    const-class v0, Lcom/sec/chaton/push/c/a/b;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/sec/chaton/push/c/a/b;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    monitor-exit v0

    return-void

    .line 462
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public a(ILcom/google/protobuf/GeneratedMessageLite;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/16 v5, -0x68

    .line 245
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "InitMessageTask.onResponse(). ErrorCode: %d."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_0
    if-nez p1, :cond_16

    .line 251
    if-nez p2, :cond_2

    .line 252
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_1

    .line 253
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Response message is null."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :cond_1
    :goto_0
    return-void

    .line 259
    :cond_2
    check-cast p2, Lcom/sec/a/a/a/i;

    .line 260
    invoke-virtual {p2}, Lcom/sec/a/a/a/i;->f()I

    move-result v0

    .line 262
    iget v1, p0, Lcom/sec/chaton/push/c/a/b;->c:I

    invoke-virtual {p2}, Lcom/sec/a/a/a/i;->d()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 263
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 264
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Request Async Id(%d) != Response Async Id(%d)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/push/c/a/b;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p2}, Lcom/sec/a/a/a/i;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_3
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_7

    .line 271
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->e()V

    .line 273
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->g()V

    .line 277
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 278
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_4

    .line 279
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Start HeartBeat."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_4
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->c()Z

    .line 289
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0}, Lcom/sec/chaton/push/c/b;->a()V

    goto :goto_0

    .line 284
    :cond_6
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_5

    .line 285
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "HeartBeat will not be started hence push service hadn\'t been started."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 293
    :cond_7
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_8

    .line 294
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Init reply result code is error. ErrorCode: %d."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_8
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->f()V

    .line 299
    invoke-direct {p0, v5}, Lcom/sec/chaton/push/c/a/b;->a(I)V

    .line 301
    const/16 v1, 0xfa2

    if-eq v0, v1, :cond_9

    const/16 v1, 0xfa6

    if-ne v0, v1, :cond_c

    .line 305
    :cond_9
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_a

    .line 306
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Disconnect connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_4

    .line 314
    :goto_2
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_b

    .line 315
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Execute reprovisioning."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_b
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 319
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v5}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 326
    :cond_c
    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_13

    .line 327
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_d

    .line 328
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Server error: internal server error."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_d
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->e()Lcom/sec/chaton/push/c/a/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->a:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_10

    .line 333
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_e

    .line 334
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Disconnect connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_1
    .catch Lcom/sec/chaton/push/a/a; {:try_start_1 .. :try_end_1} :catch_3

    .line 342
    :goto_3
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_f

    .line 343
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Retry initialize using secondary push server."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_f
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->d()V

    .line 347
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v5}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 354
    :cond_10
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->e()Lcom/sec/chaton/push/c/a/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->b:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_1

    .line 356
    :try_start_2
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_11

    .line 357
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Disconnect connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_2
    .catch Lcom/sec/chaton/push/a/a; {:try_start_2 .. :try_end_2} :catch_2

    .line 365
    :goto_4
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_12

    .line 366
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Execute re-provisioning."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :cond_12
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 370
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v5}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 378
    :cond_13
    const/16 v1, 0x7d3

    if-ne v0, v1, :cond_1

    .line 379
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_14

    .line 380
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Server error: interrupted."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_14
    :try_start_3
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_15

    .line 385
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Disconnect connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_3
    .catch Lcom/sec/chaton/push/a/a; {:try_start_3 .. :try_end_3} :catch_1

    .line 393
    :goto_5
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v5}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 401
    :cond_16
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->f()V

    .line 403
    invoke-direct {p0, v5}, Lcom/sec/chaton/push/c/a/b;->a(I)V

    .line 405
    const/4 v0, -0x1

    if-ne p1, v0, :cond_19

    .line 407
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_17

    .line 408
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Internal error code is timeout."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_17
    :try_start_4
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_18

    .line 413
    sget-object v0, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v1, "Disconnect connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_18
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_4
    .catch Lcom/sec/chaton/push/a/a; {:try_start_4 .. :try_end_4} :catch_0

    .line 421
    :goto_6
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    .line 424
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v5}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 417
    :catch_0
    move-exception v0

    goto :goto_6

    .line 389
    :catch_1
    move-exception v0

    goto :goto_5

    .line 361
    :catch_2
    move-exception v0

    goto/16 :goto_4

    .line 338
    :catch_3
    move-exception v0

    goto/16 :goto_3

    .line 310
    :catch_4
    move-exception v0

    goto/16 :goto_2
.end method

.method public c()Lcom/sec/chaton/push/c/h;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x2

    const/4 v8, 0x1

    const/16 v7, -0x68

    const/4 v0, 0x0

    .line 75
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 76
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "InitMessageTask.onRequest()."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->f()V

    .line 82
    invoke-static {}, Lcom/sec/chaton/push/util/e;->a()Lcom/sec/chaton/push/util/f;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/push/util/f;->c:Lcom/sec/chaton/push/util/f;

    if-ne v1, v2, :cond_1

    .line 89
    invoke-static {}, Lcom/sec/chaton/push/util/e;->b()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {v0}, Lcom/sec/chaton/push/k;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 92
    if-eqz v1, :cond_4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 107
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/b;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/push/c/a/f;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 108
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_2

    .line 109
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Provisioning isn\'t completed. Execute provisioning before initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/c/a/f;

    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/b;->a()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/chaton/push/c/a/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :cond_3
    :goto_1
    return-object v0

    .line 95
    :cond_4
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v3, :cond_5

    .line 96
    sget-object v3, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The network operator is changed. New("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "), Old("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Execute re-provisioning."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_5
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 102
    invoke-static {}, Lcom/sec/chaton/push/k;->c()V

    goto :goto_0

    .line 114
    :catch_0
    move-exception v1

    .line 115
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_3

    .line 116
    sget-object v2, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 124
    :cond_6
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 125
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v1, :cond_7

    .line 126
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Initialization already has been completed. Don\'t execute re-initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_3

    .line 130
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    const/16 v2, -0x66

    invoke-interface {v1, v2}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto :goto_1

    .line 138
    :cond_8
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_9

    .line 139
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Disconnect connection before initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 147
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/b;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/push/c/a/f;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/b;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/push/c/a/f;->c(Landroid/content/Context;)I

    move-result v2

    .line 150
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v3, :cond_a

    .line 151
    sget-object v3, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v4, "Connect to push server(%s:%s)."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_a
    :try_start_2
    iget-object v3, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v2, v4}, Lcom/sec/chaton/push/b/a/d;->a(Ljava/lang/String;IZ)Lorg/jboss/netty/channel/Channel;
    :try_end_2
    .catch Lcom/sec/chaton/push/a/k; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/sec/chaton/push/a/a; {:try_start_2 .. :try_end_2} :catch_2

    .line 234
    :goto_3
    invoke-static {}, Lcom/sec/chaton/push/util/d;->a()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/push/c/a/b;->c:I

    .line 236
    invoke-static {}, Lcom/sec/a/a/a/l;->newBuilder()Lcom/sec/a/a/a/m;

    move-result-object v1

    .line 237
    iget v2, p0, Lcom/sec/chaton/push/c/a/b;->c:I

    invoke-virtual {v1, v2}, Lcom/sec/a/a/a/m;->a(I)Lcom/sec/a/a/a/m;

    .line 238
    invoke-static {v0}, Lcom/sec/chaton/push/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/m;->a(Ljava/lang/String;)Lcom/sec/a/a/a/m;

    .line 240
    new-instance v0, Lcom/sec/chaton/push/c/h;

    invoke-direct {v0}, Lcom/sec/chaton/push/c/h;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/c/h;->a(Lcom/sec/chaton/push/b/a/a;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/a/a/a/m;->d()Lcom/sec/a/a/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/h;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    goto/16 :goto_1

    .line 156
    :catch_1
    move-exception v1

    .line 158
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_b

    .line 159
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "There isn\'t available network in device."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_b
    invoke-direct {p0, v7}, Lcom/sec/chaton/push/c/a/b;->a(I)V

    .line 164
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_3

    .line 165
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v9}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_1

    .line 169
    :catch_2
    move-exception v1

    .line 170
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_c

    .line 171
    sget-object v2, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v3, "Can\'t connect to push server."

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    :cond_c
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v1, :cond_d

    .line 175
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Switch push server address."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_d
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->d()V

    .line 180
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/b;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/push/c/a/f;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 181
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/b;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/push/c/a/f;->c(Landroid/content/Context;)I

    move-result v2

    .line 183
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v3, :cond_e

    .line 184
    sget-object v3, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v4, "Connect to push server(%s:%s)."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_e
    :try_start_3
    iget-object v3, p0, Lcom/sec/chaton/push/c/a/b;->d:Lcom/sec/chaton/push/b/a/d;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v2, v4}, Lcom/sec/chaton/push/b/a/d;->a(Ljava/lang/String;IZ)Lorg/jboss/netty/channel/Channel;
    :try_end_3
    .catch Lcom/sec/chaton/push/a/k; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/sec/chaton/push/a/a; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_3

    .line 189
    :catch_3
    move-exception v1

    .line 190
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_f

    .line 191
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "There isn\'t available network in device."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :cond_f
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->d()V

    .line 196
    invoke-direct {p0, v7}, Lcom/sec/chaton/push/c/a/b;->a(I)V

    .line 198
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_3

    .line 199
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v9}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_1

    .line 203
    :catch_4
    move-exception v1

    .line 204
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_10

    .line 205
    sget-object v2, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v3, "Can\'t connect to push server."

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208
    :cond_10
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->d()V

    .line 210
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_11

    .line 211
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Execute re-provisioning."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_11
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 217
    invoke-direct {p0, v7}, Lcom/sec/chaton/push/c/a/b;->a(I)V

    .line 219
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_12

    .line 220
    sget-object v1, Lcom/sec/chaton/push/c/a/b;->a:Ljava/lang/String;

    const-string v2, "Couldn\'t connect primary and secondary push server. execute re-provisioning."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_12
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->e()V

    .line 225
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_3

    .line 226
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/b;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v7}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_1

    .line 143
    :catch_5
    move-exception v1

    goto/16 :goto_2
.end method

.method public f_()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    const-class v0, Lcom/sec/a/a/a/i;

    return-object v0
.end method
