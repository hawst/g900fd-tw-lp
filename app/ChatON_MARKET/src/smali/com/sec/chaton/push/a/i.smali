.class Lcom/sec/chaton/push/a/i;
.super Ljava/lang/Object;
.source "ErrorHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(ZLjava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    iput-boolean p1, p0, Lcom/sec/chaton/push/a/i;->a:Z

    .line 324
    iput-object p2, p0, Lcom/sec/chaton/push/a/i;->b:Ljava/lang/Throwable;

    .line 325
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 329
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_0

    .line 330
    iget-boolean v0, p0, Lcom/sec/chaton/push/a/i;->a:Z

    if-eqz v0, :cond_3

    .line 331
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Prov. channel is disconnected unexpectedly by %s."

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/chaton/push/a/i;->b:Ljava/lang/Throwable;

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/sec/chaton/push/a/i;->a:Z

    if-eqz v0, :cond_1

    .line 338
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v0

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/e;->a(I)V

    .line 340
    :cond_1
    return-void

    .line 331
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/push/a/i;->b:Ljava/lang/Throwable;

    goto :goto_0

    .line 333
    :cond_3
    invoke-static {}, Lcom/sec/chaton/push/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Prov. channel is disconnected."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
