.class public Lcom/sec/chaton/push/c/a/f;
.super Lcom/sec/chaton/push/c/a;
.source "ProvMessageTask.java"

# interfaces
.implements Lcom/sec/chaton/push/c/d;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/push/c/a/g;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Lcom/sec/chaton/push/b/a/c;

.field private e:Lcom/sec/chaton/push/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/push/c/a/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    .line 56
    sget-object v0, Lcom/sec/chaton/push/c/a/g;->a:Lcom/sec/chaton/push/c/a/g;

    sput-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 63
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/push/c/a;-><init>(Landroid/content/Context;J)V

    .line 65
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->k()Lcom/sec/chaton/push/b/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/f;->d:Lcom/sec/chaton/push/b/a/c;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    .line 67
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 517
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/push/a/b;->a(I)V

    .line 518
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v8, -0x1

    .line 72
    invoke-static {v2}, Lcom/sec/chaton/push/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-static {v2}, Lcom/sec/chaton/push/k;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-static {v0}, Lcom/sec/chaton/push/k;->a(I)I

    move-result v3

    .line 78
    invoke-static {v8}, Lcom/sec/chaton/push/k;->d(I)I

    move-result v4

    .line 79
    invoke-static {v8}, Lcom/sec/chaton/push/k;->e(I)I

    move-result v5

    .line 80
    invoke-static {v8}, Lcom/sec/chaton/push/k;->f(I)I

    move-result v6

    .line 81
    invoke-static {v8}, Lcom/sec/chaton/push/k;->g(I)I

    move-result v7

    .line 83
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eq v4, v8, :cond_0

    if-eq v5, v8, :cond_0

    if-eq v6, v8, :cond_0

    if-ne v7, v8, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->a:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_0

    .line 96
    invoke-static {v2}, Lcom/sec/chaton/push/k;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    .line 97
    :cond_0
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->b:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_1

    .line 98
    invoke-static {v2}, Lcom/sec/chaton/push/k;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shouldn\'t reach this code."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static c(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 111
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->a:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_0

    .line 112
    invoke-static {v2}, Lcom/sec/chaton/push/k;->a(I)I

    move-result v0

    .line 114
    :goto_0
    return v0

    .line 113
    :cond_0
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->b:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_1

    .line 114
    invoke-static {v2}, Lcom/sec/chaton/push/k;->b(I)I

    move-result v0

    goto :goto_0

    .line 119
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Sholudn\'t reach this code."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static d()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 127
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->a:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_1

    .line 128
    sget-object v0, Lcom/sec/chaton/push/c/a/g;->b:Lcom/sec/chaton/push/c/a/g;

    sput-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    .line 130
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_0

    .line 131
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v1, "Change server to %s."

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->b:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_2

    .line 136
    sget-object v0, Lcom/sec/chaton/push/c/a/g;->a:Lcom/sec/chaton/push/c/a/g;

    sput-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    .line 138
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_0

    .line 139
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v1, "Change server to %s."

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Sholudn\'t reach this code."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static e()Lcom/sec/chaton/push/c/a/g;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->b:Lcom/sec/chaton/push/c/a/g;

    return-object v0
.end method


# virtual methods
.method public a(ILcom/google/protobuf/GeneratedMessageLite;)V
    .locals 16

    .prologue
    .line 324
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 325
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "ProvMessageTask.onResponse(). ErrorCode: %d."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_0
    :try_start_0
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 330
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Disconnect provisioning connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->d:Lcom/sec/chaton/push/b/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/c;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_4

    .line 338
    :goto_0
    if-nez p1, :cond_16

    .line 340
    if-nez p2, :cond_3

    .line 341
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 342
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Response message is null."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_2
    :goto_1
    return-void

    .line 348
    :cond_3
    check-cast p2, Lcom/sec/a/a/a/ad;

    .line 349
    invoke-virtual/range {p2 .. p2}, Lcom/sec/a/a/a/ad;->d()I

    move-result v1

    .line 351
    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_f

    .line 352
    invoke-virtual/range {p2 .. p2}, Lcom/sec/a/a/a/ad;->f()Ljava/lang/String;

    move-result-object v2

    .line 353
    invoke-virtual/range {p2 .. p2}, Lcom/sec/a/a/a/ad;->h()Ljava/lang/String;

    move-result-object v3

    .line 354
    invoke-virtual/range {p2 .. p2}, Lcom/sec/a/a/a/ad;->j()I

    move-result v4

    .line 355
    invoke-virtual/range {p2 .. p2}, Lcom/sec/a/a/a/ad;->l()Ljava/lang/String;

    move-result-object v5

    .line 356
    invoke-virtual/range {p2 .. p2}, Lcom/sec/a/a/a/ad;->n()I

    move-result v6

    .line 369
    invoke-virtual/range {p2 .. p2}, Lcom/sec/a/a/a/ad;->r()Ljava/lang/String;

    move-result-object v1

    .line 371
    const/4 v10, -0x1

    .line 372
    const/4 v9, -0x1

    .line 373
    const/4 v8, -0x1

    .line 374
    const/4 v7, -0x1

    .line 376
    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_e

    .line 379
    :try_start_1
    new-instance v11, Ljava/net/URI;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "http://dummy.dummy?"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v11, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 381
    const-string v1, "UTF-8"

    invoke-static {v11, v1}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 383
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    .line 384
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "ping_min"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 385
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v14, v7

    move v7, v8

    move v8, v9

    move v9, v1

    move v1, v14

    :goto_3
    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v1

    .line 391
    goto :goto_2

    .line 386
    :cond_4
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "ping_avg"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 387
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v9, v10

    move v14, v8

    move v8, v1

    move v1, v7

    move v7, v14

    goto :goto_3

    .line 388
    :cond_5
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "ping_max"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 389
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v8, v9

    move v9, v10

    move v14, v7

    move v7, v1

    move v1, v14

    goto :goto_3

    .line 390
    :cond_6
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "ping_inc"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1a

    .line 391
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    move v7, v8

    move v8, v9

    move v9, v10

    goto :goto_3

    :cond_7
    move v14, v7

    move v7, v10

    move v10, v14

    move v15, v9

    move v9, v8

    move v8, v15

    .line 405
    :goto_4
    const/4 v1, -0x1

    if-eq v7, v1, :cond_8

    const/4 v1, -0x1

    if-eq v8, v1, :cond_8

    const/4 v1, -0x1

    if-eq v9, v1, :cond_8

    const/4 v1, -0x1

    if-ne v10, v1, :cond_9

    .line 406
    :cond_8
    const/4 v7, 0x4

    .line 407
    const/4 v8, 0x4

    .line 408
    const/16 v9, 0x18

    .line 409
    const/4 v10, 0x4

    .line 412
    :cond_9
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_a

    .line 413
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v11, "[Provisioning push information]"

    invoke-static {v1, v11}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "\tMin Interval: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "\tAvg Interval: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "\tMax Interval: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "\tInc Interval: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_a
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->d()V

    .line 425
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->c:Ljava/lang/String;

    invoke-static/range {v1 .. v10}, Lcom/sec/chaton/push/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIIII)V

    .line 429
    :try_start_2
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_b

    .line 430
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Execute initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_b
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/c/a/b;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/push/c/a/f;->a()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 440
    :cond_c
    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_2

    .line 441
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1}, Lcom/sec/chaton/push/c/b;->a()V

    goto/16 :goto_1

    .line 394
    :catch_0
    move-exception v1

    .line 395
    sget-boolean v11, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v11, :cond_d

    .line 396
    sget-object v11, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_d
    move v14, v7

    move v7, v10

    move v10, v14

    move v15, v9

    move v9, v8

    move v8, v15

    .line 402
    goto/16 :goto_4

    .line 398
    :catch_1
    move-exception v1

    .line 399
    sget-boolean v11, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v11, :cond_e

    .line 400
    sget-object v11, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_e
    move v14, v7

    move v7, v10

    move v10, v14

    move v15, v9

    move v9, v8

    move v8, v15

    goto/16 :goto_4

    .line 434
    :catch_2
    move-exception v1

    .line 435
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_c

    .line 436
    sget-object v2, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 444
    :cond_f
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_10

    .line 445
    sget-object v2, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v3, "Prov reply result code is error. ErrorCode: %d."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :cond_10
    const/16 v2, 0xbb9

    if-eq v1, v2, :cond_11

    const/16 v2, 0xbb8

    if-ne v1, v2, :cond_13

    .line 451
    :cond_11
    const/16 v1, -0x67

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/chaton/push/c/a/f;->a(I)V

    .line 453
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_12

    .line 454
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    const/16 v2, -0x67

    invoke-interface {v1, v2}, Lcom/sec/chaton/push/c/b;->a(I)V

    .line 457
    :cond_12
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 458
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Restart device."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 465
    :cond_13
    const/16 v2, 0xbba

    if-ne v1, v2, :cond_15

    .line 467
    const/16 v1, -0x67

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/chaton/push/c/a/f;->a(I)V

    .line 469
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_14

    .line 470
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Execute reprovisioning."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    :cond_14
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 474
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->b()V

    .line 476
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_2

    .line 477
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    const/16 v2, -0x67

    invoke-interface {v1, v2}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_1

    .line 482
    :cond_15
    const/16 v1, -0x67

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/chaton/push/c/a/f;->a(I)V

    .line 484
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_2

    .line 485
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    const/16 v2, -0x67

    invoke-interface {v1, v2}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_1

    .line 490
    :cond_16
    const/16 v1, -0x67

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/chaton/push/c/a/f;->a(I)V

    .line 492
    const/4 v1, -0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_19

    .line 493
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_17

    .line 494
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Internal error code is timeout."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_17
    :try_start_3
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_18

    .line 499
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Disconnect connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->d:Lcom/sec/chaton/push/b/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/c;->b()V
    :try_end_3
    .catch Lcom/sec/chaton/push/a/a; {:try_start_3 .. :try_end_3} :catch_3

    .line 507
    :goto_6
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->b()V

    .line 510
    :cond_19
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_2

    .line 511
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    const/16 v2, -0x67

    invoke-interface {v1, v2}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_1

    .line 503
    :catch_3
    move-exception v1

    goto :goto_6

    .line 334
    :catch_4
    move-exception v1

    goto/16 :goto_0

    :cond_1a
    move v1, v7

    move v7, v8

    move v8, v9

    move v9, v10

    goto/16 :goto_3
.end method

.method public c()Lcom/sec/chaton/push/c/h;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v6, -0x2

    const/16 v4, -0x67

    const/4 v10, 0x3

    const/4 v5, 0x0

    .line 173
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 174
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v1, "ProvMessageTask.onRequest()."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->c()V

    .line 179
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/f;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/push/c/a/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 181
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v1, "Provisioning data already exists."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_2

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    const/16 v1, -0x64

    invoke-interface {v0, v1}, Lcom/sec/chaton/push/c/b;->a(I)V

    :cond_2
    move-object v0, v5

    .line 319
    :goto_0
    return-object v0

    .line 191
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_4

    .line 192
    sget-object v0, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v1, "Connect to provisioning server(gld.push.samsungosp.com:5223)."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->d:Lcom/sec/chaton/push/b/a/c;

    const-string v1, "gld.push.samsungosp.com"

    const/16 v2, 0x1467

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/push/b/a/c;->a(Ljava/lang/String;IZ)Lorg/jboss/netty/channel/Channel;
    :try_end_0
    .catch Lcom/sec/chaton/push/a/k; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 238
    invoke-static {}, Lcom/sec/a/a/a/ag;->newBuilder()Lcom/sec/a/a/a/ah;

    move-result-object v6

    .line 240
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/f;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 243
    invoke-static {v5}, Lcom/sec/chaton/push/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 246
    const-string v2, ""

    .line 247
    const-string v1, ""

    .line 249
    const-string v4, ""

    .line 250
    const-string v3, ""

    .line 253
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/chaton/push/c/a/f;->c:Ljava/lang/String;

    .line 258
    iget-object v8, p0, Lcom/sec/chaton/push/c/a/f;->c:Ljava/lang/String;

    if-eqz v8, :cond_10

    iget-object v8, p0, Lcom/sec/chaton/push/c/a/f;->c:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_10

    .line 260
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/f;->c:Ljava/lang/String;

    invoke-virtual {v1, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 261
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/f;->c:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 269
    :cond_5
    :goto_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v8

    const/4 v9, 0x5

    if-ne v8, v9, :cond_11

    .line 270
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_12

    .line 277
    invoke-virtual {v0, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 278
    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v4, v3

    move-object v3, v0

    .line 292
    :cond_6
    :goto_3
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 295
    const-string v8, "1"

    invoke-virtual {v6, v8}, Lcom/sec/a/a/a/ah;->a(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 296
    const-string v8, "00000000"

    invoke-virtual {v6, v8}, Lcom/sec/a/a/a/ah;->c(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 297
    invoke-virtual {v6, v7}, Lcom/sec/a/a/a/ah;->b(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 298
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "device.model="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "&sim.mcc="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "&sim.mnc="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "&net.mcc="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "&net.mnc="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/sec/a/a/a/ah;->e(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 300
    sget-boolean v8, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v8, :cond_7

    .line 301
    sget-object v8, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Device id: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    sget-object v7, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Network.MCC: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    sget-object v2, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Network.MNC: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SIM.MCC: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM.MNC: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device model: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_7
    invoke-static {v5}, Lcom/sec/chaton/push/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_9

    .line 312
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_8

    .line 313
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "This device has a device token(%s). Using it to provisioning."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v11

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_8
    invoke-virtual {v6, v0}, Lcom/sec/a/a/a/ah;->d(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 319
    :cond_9
    new-instance v0, Lcom/sec/chaton/push/c/h;

    invoke-direct {v0}, Lcom/sec/chaton/push/c/h;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/push/c/a/f;->d:Lcom/sec/chaton/push/b/a/c;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/h;->a(Lcom/sec/chaton/push/b/a/a;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/a/a/a/ah;->d()Lcom/sec/a/a/a/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/h;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    goto/16 :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_a

    .line 200
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Can\'t connect to provisioning server hence network isn\'t avaliable."

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 203
    :cond_a
    invoke-direct {p0, v4}, Lcom/sec/chaton/push/c/a/f;->a(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_b

    .line 206
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v6}, Lcom/sec/chaton/push/c/b;->a(I)V

    :cond_b
    move-object v0, v5

    .line 209
    goto/16 :goto_0

    .line 210
    :catch_1
    move-exception v0

    .line 211
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_c

    .line 212
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "Can\'t connect to provisioning server."

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 215
    :cond_c
    invoke-direct {p0, v4}, Lcom/sec/chaton/push/c/a/f;->a(I)V

    .line 217
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->b()V

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_d

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v6}, Lcom/sec/chaton/push/c/b;->a(I)V

    :cond_d
    move-object v0, v5

    .line 223
    goto/16 :goto_0

    .line 224
    :catch_2
    move-exception v0

    .line 225
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_e

    .line 226
    sget-object v1, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v2, "mConnectionManager isn\'t set up."

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 229
    :cond_e
    invoke-direct {p0, v4}, Lcom/sec/chaton/push/c/a/f;->a(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_f

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/f;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, v6}, Lcom/sec/chaton/push/c/b;->a(I)V

    :cond_f
    move-object v0, v5

    .line 234
    goto/16 :goto_0

    .line 263
    :cond_10
    sget-boolean v8, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v8, :cond_5

    .line 264
    sget-object v8, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v9, "Use empty network mcc and mnc because couldn\'t retrieve network information."

    invoke-static {v8, v9}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 281
    :cond_11
    sget-boolean v8, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v8, :cond_6

    .line 282
    sget-object v8, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    const-string v9, "Use empty mcc and mnc because couldn\'t retrieve sim information."

    invoke-static {v8, v9}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v8, Lcom/sec/chaton/push/c/a/f;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SIM State: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_12
    move-object v0, v3

    move-object v3, v4

    goto/16 :goto_2
.end method

.method public f_()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    const-class v0, Lcom/sec/a/a/a/ad;

    return-object v0
.end method
