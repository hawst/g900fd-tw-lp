.class public Lcom/sec/chaton/push/c/e;
.super Ljava/lang/Object;
.source "MessageTaskDispatcher.java"

# interfaces
.implements Lcom/sec/chaton/push/b/b/b;


# static fields
.field private static final a:Ljava/lang/String;

.field private static c:Lcom/sec/chaton/push/c/e;


# instance fields
.field private final b:Ljava/lang/Object;

.field private d:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private e:Ljava/lang/Thread;

.field private f:Landroid/os/HandlerThread;

.field private g:Landroid/os/Handler;

.field private h:Lcom/sec/chaton/push/c/d;

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">;",
            "Lcom/sec/chaton/push/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/push/c/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/push/c/e;->b:Ljava/lang/Object;

    .line 92
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/push/c/e;->i:Ljava/util/Map;

    .line 96
    invoke-direct {p0}, Lcom/sec/chaton/push/c/e;->e()V

    .line 97
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/push/c/e;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->g:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/push/c/e;Lcom/sec/chaton/push/c/d;)Lcom/sec/chaton/push/c/d;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/chaton/push/c/e;->h:Lcom/sec/chaton/push/c/d;

    return-object p1
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/push/c/e;
    .locals 2

    .prologue
    .line 100
    const-class v1, Lcom/sec/chaton/push/c/e;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/c/e;->c:Lcom/sec/chaton/push/c/e;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/sec/chaton/push/c/e;

    invoke-direct {v0}, Lcom/sec/chaton/push/c/e;-><init>()V

    sput-object v0, Lcom/sec/chaton/push/c/e;->c:Lcom/sec/chaton/push/c/e;

    .line 104
    :cond_0
    sget-object v0, Lcom/sec/chaton/push/c/e;->c:Lcom/sec/chaton/push/c/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/sec/chaton/push/c/e;)Lcom/sec/chaton/push/c/d;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->h:Lcom/sec/chaton/push/c/d;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/push/c/e;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/chaton/push/c/e;->f()V

    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/push/c/e;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->e:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/push/c/e;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->i:Ljava/util/Map;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 135
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 136
    sget-object v0, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    const-string v1, "MessageTaskDispatcher.prepare()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/push/c/e;->d:Z

    if-eqz v0, :cond_2

    .line 140
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 141
    sget-object v0, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    const-string v1, "It is already prepared."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_1
    :goto_0
    return-void

    .line 147
    :cond_2
    iput-boolean v2, p0, Lcom/sec/chaton/push/c/e;->d:Z

    .line 150
    new-instance v0, Lcom/sec/chaton/push/c/f;

    const-string v1, "MessageTaskDispatchThread"

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/push/c/f;-><init>(Lcom/sec/chaton/push/c/e;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/push/c/e;->e:Ljava/lang/Thread;

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 154
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MessageTaskExecuteThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/push/c/e;->f:Landroid/os/HandlerThread;

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 156
    new-instance v0, Lcom/sec/chaton/push/c/g;

    iget-object v1, p0, Lcom/sec/chaton/push/c/e;->f:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/push/c/g;-><init>(Lcom/sec/chaton/push/c/e;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/push/c/e;->g:Landroid/os/Handler;

    .line 159
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sget-object v1, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 255
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 256
    sget-object v0, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    const-string v1, "Acquire wake lock."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 264
    :cond_1
    monitor-exit v1

    .line 270
    :cond_2
    :goto_0
    return-void

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 265
    :catch_0
    move-exception v0

    .line 266
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 267
    sget-object v1, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/push/c/e;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/chaton/push/c/e;->g()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/push/c/e;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->b:Ljava/lang/Object;

    return-object v0
.end method

.method private g()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 273
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v2, :cond_0

    .line 274
    sget-object v2, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    const-string v3, "Release wake lock."

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :try_start_1
    iget-object v3, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_1

    .line 283
    iget-object v3, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 285
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v3, :cond_1

    .line 286
    sget-object v3, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    const-string v4, "Is all wake lock released? %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/chaton/push/c/e;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v7

    if-nez v7, :cond_3

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    :cond_1
    monitor-exit v2

    .line 296
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    .line 286
    goto :goto_0

    .line 289
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 291
    :catch_0
    move-exception v0

    .line 292
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_2

    .line 293
    sget-object v1, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public a(I)V
    .locals 5

    .prologue
    .line 226
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 227
    sget-object v0, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    const-string v1, "Cancel current message task. (reason internal error code: %d)."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/push/c/e;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 231
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->h:Lcom/sec/chaton/push/c/d;

    if-eqz v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->g:Landroid/os/Handler;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->h:Lcom/sec/chaton/push/c/d;

    .line 236
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/chaton/push/c/e;->h:Lcom/sec/chaton/push/c/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 238
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v0, p1, v2}, Lcom/sec/chaton/push/c/d;->a(ILcom/google/protobuf/GeneratedMessageLite;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    :try_start_2
    invoke-direct {p0}, Lcom/sec/chaton/push/c/e;->g()V

    .line 243
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 244
    sget-object v0, Lcom/sec/chaton/push/c/e;->a:Ljava/lang/String;

    const-string v2, "Wake up MessageTaskDispatchThread."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/push/c/e;->e:Ljava/lang/Thread;

    monitor-enter v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 248
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 249
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 251
    :cond_2
    :try_start_4
    monitor-exit v1

    .line 252
    return-void

    .line 240
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/sec/chaton/push/c/e;->g()V

    throw v0

    .line 251
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 249
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
.end method

.method public a(Lcom/sec/chaton/push/c/c;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->i:Ljava/util/Map;

    invoke-interface {p1}, Lcom/sec/chaton/push/c/c;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/push/c/e;->g:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 215
    return-void
.end method

.method public b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->g:Landroid/os/Handler;

    return-object v0
.end method

.method public c()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/push/c/e;->f:Landroid/os/HandlerThread;

    return-object v0
.end method
