.class public Lcom/sec/chaton/push/a/b;
.super Ljava/lang/Object;
.source "ErrorHandler.java"

# interfaces
.implements Lcom/sec/chaton/push/receiver/b;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/push/a/b;


# instance fields
.field private final c:Ljava/util/Random;

.field private final d:Ljava/lang/String;

.field private e:I

.field private final f:Ljava/lang/String;

.field private g:I

.field private final h:Ljava/lang/Object;

.field private final i:Ljava/lang/Object;

.field private j:Lcom/sec/chaton/push/b/b/a;

.field private k:Lcom/sec/chaton/push/b/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/chaton/push/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "provisioningRetryToken"

    iput-object v0, p0, Lcom/sec/chaton/push/a/b;->d:Ljava/lang/String;

    .line 52
    const-string v0, "initializeRetryToken"

    iput-object v0, p0, Lcom/sec/chaton/push/a/b;->f:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/push/a/b;->h:Ljava/lang/Object;

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/push/a/b;->i:Ljava/lang/Object;

    .line 254
    new-instance v0, Lcom/sec/chaton/push/a/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/push/a/c;-><init>(Lcom/sec/chaton/push/a/b;)V

    iput-object v0, p0, Lcom/sec/chaton/push/a/b;->j:Lcom/sec/chaton/push/b/b/a;

    .line 284
    new-instance v0, Lcom/sec/chaton/push/a/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/push/a/d;-><init>(Lcom/sec/chaton/push/a/b;)V

    iput-object v0, p0, Lcom/sec/chaton/push/a/b;->k:Lcom/sec/chaton/push/b/b/a;

    .line 64
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/sec/chaton/push/a/b;->c:Ljava/util/Random;

    .line 66
    iput v3, p0, Lcom/sec/chaton/push/a/b;->e:I

    .line 67
    iput v3, p0, Lcom/sec/chaton/push/a/b;->g:I

    .line 68
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/push/a/b;
    .locals 2

    .prologue
    .line 40
    const-class v1, Lcom/sec/chaton/push/a/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/a/b;->b:Lcom/sec/chaton/push/a/b;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/chaton/push/a/b;

    invoke-direct {v0}, Lcom/sec/chaton/push/a/b;-><init>()V

    sput-object v0, Lcom/sec/chaton/push/a/b;->b:Lcom/sec/chaton/push/a/b;

    .line 44
    :cond_0
    sget-object v0, Lcom/sec/chaton/push/a/b;->b:Lcom/sec/chaton/push/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(I)J
    .locals 4

    .prologue
    const/16 v3, 0x77

    .line 207
    .line 209
    const/16 v0, 0x14

    if-gt p1, v0, :cond_1

    .line 210
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 211
    sget-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v1, "Using short base retry interval."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/push/a/b;->c:Ljava/util/Random;

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    .line 236
    :goto_0
    return-wide v0

    .line 225
    :cond_1
    const-wide/32 v0, 0x493e0

    iget-object v2, p0, Lcom/sec/chaton/push/a/b;->c:Ljava/util/Random;

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    .line 476
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 477
    sget-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v1, "Cancel pending message task."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    .line 482
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 483
    sget-object v1, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v2, "Cancel task count: %d."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/i;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    :cond_1
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->c()V

    .line 487
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->f()V

    .line 489
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/a/e;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/push/a/e;-><init>(Lcom/sec/chaton/push/a/b;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/push/c/i;->a(Landroid/content/Context;Lcom/sec/chaton/push/c/k;)V

    .line 501
    return-void
.end method

.method public a(Lcom/sec/chaton/push/util/f;Lcom/sec/chaton/push/util/f;)V
    .locals 2

    .prologue
    .line 243
    invoke-static {}, Lcom/sec/chaton/push/c/e;->a()Lcom/sec/chaton/push/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/c/e;->b()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/push/a/f;

    invoke-direct {v1, p1, p2}, Lcom/sec/chaton/push/a/f;-><init>(Lcom/sec/chaton/push/util/f;Lcom/sec/chaton/push/util/f;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 244
    return-void
.end method

.method public b()V
    .locals 10

    .prologue
    const v2, 0x7fffffff

    .line 72
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    .line 74
    iget-object v6, p0, Lcom/sec/chaton/push/a/b;->h:Ljava/lang/Object;

    monitor-enter v6

    .line 75
    :try_start_0
    const-string v1, "provisioningRetryToken"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/i;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v1, "The retry provisioning task had been reserved in MessageTaskQueue."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    monitor-exit v6

    .line 104
    :goto_0
    return-void

    .line 83
    :cond_1
    iget v1, p0, Lcom/sec/chaton/push/a/b;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/push/a/b;->e:I

    .line 85
    iget v1, p0, Lcom/sec/chaton/push/a/b;->e:I

    invoke-direct {p0, v1}, Lcom/sec/chaton/push/a/b;->b(I)J

    move-result-wide v3

    .line 87
    iget v1, p0, Lcom/sec/chaton/push/a/b;->e:I

    if-lt v1, v2, :cond_3

    .line 88
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_2

    .line 89
    sget-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v1, "Provisioning retry count is over(max count: %d)."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x7fffffff

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_2
    monitor-exit v6

    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 95
    :cond_3
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v1, :cond_4

    .line 96
    sget-object v1, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v2, "Retry provisioning (retry count: %d) (interval: %d)."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/chaton/push/a/b;->e:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const-wide/16 v8, 0x3e8

    div-long v8, v3, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/push/a/b;->c()V

    .line 102
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "provisioningRetryToken"

    new-instance v5, Lcom/sec/chaton/push/c/a/f;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/sec/chaton/push/c/a/f;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/push/c/i;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/chaton/push/c/a;)V

    .line 103
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 107
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/sec/chaton/push/a/b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 110
    :try_start_0
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v2, :cond_0

    .line 111
    sget-object v2, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v3, "Cancel retry provisioning."

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    const-string v2, "provisioningRetryToken"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/c/i;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v2, :cond_1

    .line 116
    sget-object v2, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v3, "Because the retry provisioning task had been reserved in MessageTaskQueue, decrease retry count by 1."

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_1
    iget v2, p0, Lcom/sec/chaton/push/a/b;->e:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/chaton/push/a/b;->e:I

    .line 121
    iget v2, p0, Lcom/sec/chaton/push/a/b;->e:I

    if-gez v2, :cond_2

    .line 122
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/chaton/push/a/b;->e:I

    .line 127
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-string v3, "provisioningRetryToken"

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/push/c/i;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 128
    monitor-exit v1

    .line 129
    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 132
    iget-object v1, p0, Lcom/sec/chaton/push/a/b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 133
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/sec/chaton/push/a/b;->e:I

    .line 134
    monitor-exit v1

    .line 135
    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()V
    .locals 10

    .prologue
    const v2, 0x7fffffff

    .line 141
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    .line 143
    iget-object v6, p0, Lcom/sec/chaton/push/a/b;->i:Ljava/lang/Object;

    monitor-enter v6

    .line 144
    :try_start_0
    const-string v1, "initializeRetryToken"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/i;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_0

    .line 146
    sget-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v1, "The retry initailize task had been reserved in MessageTaskQueue."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    monitor-exit v6

    .line 173
    :goto_0
    return-void

    .line 152
    :cond_1
    iget v1, p0, Lcom/sec/chaton/push/a/b;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/push/a/b;->g:I

    .line 154
    iget v1, p0, Lcom/sec/chaton/push/a/b;->g:I

    invoke-direct {p0, v1}, Lcom/sec/chaton/push/a/b;->b(I)J

    move-result-wide v3

    .line 156
    iget v1, p0, Lcom/sec/chaton/push/a/b;->g:I

    if-lt v1, v2, :cond_3

    .line 157
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v0, :cond_2

    .line 158
    sget-object v0, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v1, "Initialize retry count is over(max count: %d)."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x7fffffff

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_2
    monitor-exit v6

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 164
    :cond_3
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v1, :cond_4

    .line 165
    sget-object v1, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v2, "Retry initialize (retry count: %d) (interval: %d)."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/chaton/push/a/b;->g:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const-wide/16 v8, 0x3e8

    div-long v8, v3, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/push/a/b;->f()V

    .line 171
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "initializeRetryToken"

    new-instance v5, Lcom/sec/chaton/push/c/a/b;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/push/c/i;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/chaton/push/c/a;)V

    .line 172
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 176
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lcom/sec/chaton/push/a/b;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 179
    :try_start_0
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v2, :cond_0

    .line 180
    sget-object v2, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v3, "Cancel retry initalize."

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :cond_0
    const-string v2, "initializeRetryToken"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/c/i;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 184
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->b:Z

    if-eqz v2, :cond_1

    .line 185
    sget-object v2, Lcom/sec/chaton/push/a/b;->a:Ljava/lang/String;

    const-string v3, "Because the retry initailize task had been reserved in MessageTaskQueue, decrease retry count by 1."

    invoke-static {v2, v3}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_1
    iget v2, p0, Lcom/sec/chaton/push/a/b;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/chaton/push/a/b;->g:I

    .line 190
    iget v2, p0, Lcom/sec/chaton/push/a/b;->g:I

    if-gez v2, :cond_2

    .line 191
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/chaton/push/a/b;->g:I

    .line 196
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-string v3, "initializeRetryToken"

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/push/c/i;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 197
    monitor-exit v1

    .line 198
    return-void

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 201
    iget-object v1, p0, Lcom/sec/chaton/push/a/b;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 202
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/sec/chaton/push/a/b;->g:I

    .line 203
    monitor-exit v1

    .line 204
    return-void

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public h()Lcom/sec/chaton/push/b/b/a;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/chaton/push/a/b;->j:Lcom/sec/chaton/push/b/b/a;

    return-object v0
.end method

.method public i()Lcom/sec/chaton/push/b/b/a;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/chaton/push/a/b;->k:Lcom/sec/chaton/push/b/b/a;

    return-object v0
.end method
