.class public Lcom/sec/chaton/push/k;
.super Ljava/lang/Object;
.source "PushClientPreference.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static final c:Ljava/lang/Object;

.field private static final d:Ljava/lang/Object;

.field private static e:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const-class v0, Lcom/sec/chaton/push/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/k;->a:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/chaton/push/k;->c:Ljava/lang/Object;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    .line 53
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.chaton.push.config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 134
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "primaryPort"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 62
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v1, "deviceId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 64
    sget-object v1, Lcom/sec/chaton/push/k;->a:Ljava/lang/String;

    const-string v2, "Saved IMEI: %s."

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    if-eqz v0, :cond_2

    .line 118
    :cond_1
    :goto_0
    return-object v0

    .line 71
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    .line 73
    const-string v0, "phone"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 74
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_3

    .line 77
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 78
    const-string v2, "deviceId"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 79
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 81
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_4

    .line 82
    sget-object v0, Lcom/sec/chaton/push/k;->a:Ljava/lang/String;

    const-string v2, "This device has no IMEI. Using MAC address instead of IMEI."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_4
    const-string v0, "wifi"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 86
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 88
    const-string v1, ":"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 89
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 91
    if-eqz v0, :cond_6

    .line 95
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    :goto_1
    const/16 v3, 0xe

    if-ge v1, v3, :cond_5

    .line 97
    const-string v3, "M"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 101
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 105
    const-string v2, "deviceId"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 106
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 108
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_1

    .line 109
    sget-object v1, Lcom/sec/chaton/push/k;->a:Ljava/lang/String;

    const-string v2, "MAC address is %s."

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 112
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v1, :cond_1

    .line 113
    sget-object v1, Lcom/sec/chaton/push/k;->a:Ljava/lang/String;

    const-string v2, "This Device has no Mac Address"

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static a(II)V
    .locals 3

    .prologue
    .line 488
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 489
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 490
    const-string v2, "heartbeatFailCount"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 491
    const-string v2, "heartbeatCount"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 492
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 493
    monitor-exit v1

    .line 494
    return-void

    .line 493
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 258
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 259
    :try_start_0
    const-string v0, "deviceToken"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 260
    monitor-exit v1

    .line 261
    return-void

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;I)V
    .locals 2

    .prologue
    .line 452
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 453
    :try_start_0
    const-string v0, "heartbeatFailCount"

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 454
    monitor-exit v1

    .line 455
    return-void

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 186
    const-string v0, "deviceId"

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 187
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIIII)V
    .locals 3

    .prologue
    .line 353
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 354
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 355
    const-string v2, "networkOperator"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 356
    const-string v2, "deviceToken"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 357
    const-string v2, "primaryIp"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 358
    const-string v2, "primaryPort"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 359
    const-string v2, "secondaryIp"

    invoke-interface {v0, v2, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 360
    const-string v2, "secondaryPort"

    invoke-interface {v0, v2, p5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 361
    const-string v2, "pingMinInterval"

    invoke-interface {v0, v2, p6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 362
    const-string v2, "pingAvgInterval"

    invoke-interface {v0, v2, p7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 363
    const-string v2, "pingMaxInterval"

    invoke-interface {v0, v2, p8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 364
    const-string v2, "pingIncInterval"

    invoke-interface {v0, v2, p9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 365
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 366
    monitor-exit v1

    .line 367
    return-void

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(I)I
    .locals 3

    .prologue
    .line 146
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 147
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "secondaryPort"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 123
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "deviceToken"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 322
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 323
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 324
    invoke-static {v0}, Lcom/sec/chaton/push/k;->l(Landroid/content/SharedPreferences$Editor;)V

    .line 325
    invoke-static {v0}, Lcom/sec/chaton/push/k;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 326
    invoke-static {v0}, Lcom/sec/chaton/push/k;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 327
    invoke-static {v0}, Lcom/sec/chaton/push/k;->c(Landroid/content/SharedPreferences$Editor;)V

    .line 328
    invoke-static {v0}, Lcom/sec/chaton/push/k;->d(Landroid/content/SharedPreferences$Editor;)V

    .line 329
    invoke-static {v0}, Lcom/sec/chaton/push/k;->e(Landroid/content/SharedPreferences$Editor;)V

    .line 334
    invoke-static {v0}, Lcom/sec/chaton/push/k;->f(Landroid/content/SharedPreferences$Editor;)V

    .line 335
    invoke-static {v0}, Lcom/sec/chaton/push/k;->g(Landroid/content/SharedPreferences$Editor;)V

    .line 336
    invoke-static {v0}, Lcom/sec/chaton/push/k;->h(Landroid/content/SharedPreferences$Editor;)V

    .line 337
    invoke-static {v0}, Lcom/sec/chaton/push/k;->i(Landroid/content/SharedPreferences$Editor;)V

    .line 339
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 340
    monitor-exit v1

    .line 341
    return-void

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 264
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 265
    :try_start_0
    const-string v0, "primaryIp"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 266
    monitor-exit v1

    .line 267
    return-void

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 374
    sget-object v1, Lcom/sec/chaton/push/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 375
    :try_start_0
    const-string v0, "appId"

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 376
    monitor-exit v1

    .line 377
    return-void

    .line 376
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(I)I
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 153
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "pingInterval"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "primaryIp"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c()V
    .locals 3

    .prologue
    .line 508
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 509
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 511
    const-string v2, "heartbeatFailCount"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 512
    const-string v2, "heartbeatCount"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 513
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 514
    monitor-exit v1

    .line 515
    return-void

    .line 514
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 270
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 271
    :try_start_0
    const-string v0, "primaryPort"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 272
    monitor-exit v1

    .line 273
    return-void

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 392
    sget-object v1, Lcom/sec/chaton/push/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 393
    :try_start_0
    const-string v0, "regId"

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 394
    monitor-exit v1

    .line 395
    return-void

    .line 394
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static d(I)I
    .locals 3

    .prologue
    .line 160
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 161
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "pingMinInterval"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 141
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "secondaryIp"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static d(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 276
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 277
    :try_start_0
    const-string v0, "secondaryIp"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 278
    monitor-exit v1

    .line 279
    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static d(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 416
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 417
    :try_start_0
    const-string v0, "expireDate"

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 418
    monitor-exit v1

    .line 419
    return-void

    .line 418
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static e(I)I
    .locals 3

    .prologue
    .line 166
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 167
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "pingAvgInterval"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 386
    sget-object v1, Lcom/sec/chaton/push/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 387
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "appId"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 388
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static e(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 282
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 283
    :try_start_0
    const-string v0, "secondaryPort"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 284
    monitor-exit v1

    .line 285
    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static f(I)I
    .locals 3

    .prologue
    .line 172
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 173
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "pingMaxInterval"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 404
    sget-object v1, Lcom/sec/chaton/push/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 405
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "regId"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 406
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static f(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 296
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 297
    :try_start_0
    const-string v0, "pingMinInterval"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 298
    monitor-exit v1

    .line 299
    return-void

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static g(I)I
    .locals 3

    .prologue
    .line 178
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 179
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "pingIncInterval"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 428
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 429
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "expireDate"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 430
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static g(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 302
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 303
    :try_start_0
    const-string v0, "pingAvgInterval"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 304
    monitor-exit v1

    .line 305
    return-void

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static h(I)I
    .locals 3

    .prologue
    .line 458
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 459
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "heartbeatFailCount"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 460
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static h(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 446
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 447
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "networkOperator"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static h(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 308
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 309
    :try_start_0
    const-string v0, "pingMaxInterval"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 310
    monitor-exit v1

    .line 311
    return-void

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static i(I)I
    .locals 3

    .prologue
    .line 476
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 477
    :try_start_0
    sget-object v0, Lcom/sec/chaton/push/k;->e:Landroid/content/SharedPreferences;

    const-string v2, "heartbeatCount"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 478
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static i(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 314
    sget-object v1, Lcom/sec/chaton/push/k;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 315
    :try_start_0
    const-string v0, "pingIncInterval"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 316
    monitor-exit v1

    .line 317
    return-void

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static j(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 380
    sget-object v1, Lcom/sec/chaton/push/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 381
    :try_start_0
    const-string v0, "appId"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 382
    monitor-exit v1

    .line 383
    return-void

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static k(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 398
    sget-object v1, Lcom/sec/chaton/push/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 399
    :try_start_0
    const-string v0, "regId"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 400
    monitor-exit v1

    .line 401
    return-void

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static l(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 440
    sget-object v1, Lcom/sec/chaton/push/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 441
    :try_start_0
    const-string v0, "networkOperator"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 442
    monitor-exit v1

    .line 443
    return-void

    .line 442
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
