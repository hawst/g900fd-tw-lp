.class public Lcom/sec/chaton/push/c/a/a;
.super Lcom/sec/chaton/push/c/a;
.source "DeregistrationMessageTask.java"

# interfaces
.implements Lcom/sec/chaton/push/c/d;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/chaton/push/b/a/d;

.field private e:Lcom/sec/chaton/push/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/chaton/push/c/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    const-wide/16 v0, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/push/c/a;-><init>(Landroid/content/Context;J)V

    .line 49
    iput-object p2, p0, Lcom/sec/chaton/push/c/a/a;->c:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/a;->d:Lcom/sec/chaton/push/b/a/d;

    .line 51
    return-void
.end method


# virtual methods
.method public a(ILcom/google/protobuf/GeneratedMessageLite;)V
    .locals 8

    .prologue
    const/16 v7, 0xfa9

    const/16 v6, 0xfa2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 120
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 121
    sget-object v0, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v1, "DeregistrationMessageTask.onResponse(). ErrorCode: %d."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    if-nez p1, :cond_13

    .line 126
    if-nez p2, :cond_2

    .line 127
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_1

    .line 128
    sget-object v0, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v1, "Response message is null."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_1
    :goto_0
    return-void

    .line 134
    :cond_2
    check-cast p2, Lcom/sec/a/a/a/c;

    .line 135
    invoke-virtual {p2}, Lcom/sec/a/a/a/c;->f()I

    move-result v0

    .line 137
    iget v1, p0, Lcom/sec/chaton/push/c/a/a;->b:I

    invoke-virtual {p2}, Lcom/sec/a/a/a/c;->d()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 138
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_1

    .line 139
    sget-object v0, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v1, "Request Async Id(%d) != Response Async Id(%d)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/push/c/a/a;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p2}, Lcom/sec/a/a/a/c;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_3
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_5

    .line 147
    invoke-static {}, Lcom/sec/chaton/push/k;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 148
    invoke-static {v0}, Lcom/sec/chaton/push/k;->j(Landroid/content/SharedPreferences$Editor;)V

    .line 149
    invoke-static {v0}, Lcom/sec/chaton/push/k;->k(Landroid/content/SharedPreferences$Editor;)V

    .line 150
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 155
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_4

    .line 156
    sget-object v0, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v1, "Close connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/a;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_3

    .line 165
    :goto_1
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->d()Z

    .line 168
    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/a;->a()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0}, Lcom/sec/chaton/push/c/b;->a()V

    goto :goto_0

    .line 174
    :cond_5
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_6

    .line 175
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Deregistration reply result code is error. ErrorCode: %d."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_6
    if-eq v0, v6, :cond_7

    if-ne v0, v7, :cond_b

    .line 179
    :cond_7
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_8

    .line 183
    if-ne v0, v6, :cond_a

    .line 184
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_8

    .line 186
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Server error: empty device token."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_8
    :goto_2
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_9

    .line 198
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Close connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_1
    .catch Lcom/sec/chaton/push/a/a; {:try_start_1 .. :try_end_1} :catch_2

    .line 206
    :goto_3
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 208
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->e()V

    .line 210
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 211
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 188
    :cond_a
    if-ne v0, v7, :cond_8

    .line 189
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_8

    .line 191
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Server error: wrong device token."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 215
    :cond_b
    const/16 v1, 0xfa4

    if-ne v0, v1, :cond_d

    .line 216
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_c

    .line 217
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Server error: empty application id."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Do not anything."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 226
    :cond_d
    const/16 v1, 0xfa1

    if-ne v0, v1, :cond_10

    .line 227
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_e

    .line 228
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Server error: invalid state."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Close connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_e
    :try_start_2
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_2
    .catch Lcom/sec/chaton/push/a/a; {:try_start_2 .. :try_end_2} :catch_1

    .line 238
    :goto_4
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_f

    .line 239
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Execute initialization."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_f
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/push/a/b;->e()V

    .line 244
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 249
    :cond_10
    const/16 v1, 0xfa8

    if-ne v0, v1, :cond_12

    .line 250
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_11

    .line 251
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Server error: invalid state."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Close connection."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_11
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 256
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 261
    :cond_12
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_1

    .line 262
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1, v0}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 267
    :cond_13
    const/4 v0, -0x1

    if-ne p1, v0, :cond_16

    .line 269
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_14

    .line 270
    sget-object v0, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v1, "Internal error code is timeout."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_14
    :try_start_3
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_15

    .line 275
    sget-object v0, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v1, "Disconnect connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/a;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_3
    .catch Lcom/sec/chaton/push/a/a; {:try_start_3 .. :try_end_3} :catch_0

    .line 283
    :goto_5
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    .line 286
    :cond_16
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v0, p1}, Lcom/sec/chaton/push/c/b;->a(I)V

    goto/16 :goto_0

    .line 279
    :catch_0
    move-exception v0

    goto :goto_5

    .line 234
    :catch_1
    move-exception v1

    goto :goto_4

    .line 202
    :catch_2
    move-exception v1

    goto/16 :goto_3

    .line 160
    :catch_3
    move-exception v0

    goto/16 :goto_1
.end method

.method public a(Lcom/sec/chaton/push/c/b;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    .line 66
    return-void
.end method

.method public c()Lcom/sec/chaton/push/c/h;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 70
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_0

    .line 71
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "DeregistrationMessageTask.onRequest()."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    invoke-static {v0}, Lcom/sec/chaton/push/k;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/sec/chaton/push/c/a/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 78
    :cond_1
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v1, :cond_2

    .line 79
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "The application(id:%s) had been deregistered. callback success to application without networking."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/push/c/a/a;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    if-eqz v1, :cond_3

    .line 83
    iget-object v1, p0, Lcom/sec/chaton/push/c/a/a;->e:Lcom/sec/chaton/push/c/b;

    invoke-interface {v1}, Lcom/sec/chaton/push/c/b;->a()V

    .line 115
    :cond_3
    :goto_0
    return-object v0

    .line 90
    :cond_4
    invoke-static {}, Lcom/sec/chaton/push/c/a/b;->d()Z

    move-result v1

    if-nez v1, :cond_6

    .line 91
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v1, :cond_5

    .line 92
    sget-object v1, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    const-string v2, "Initialize isn\'t completed. Execute initialize."

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_5
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/push/c/a/b;

    invoke-virtual {p0}, Lcom/sec/chaton/push/c/a/a;->a()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/chaton/push/c/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V

    .line 98
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v1

    .line 100
    sget-boolean v2, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v2, :cond_3

    .line 101
    sget-object v2, Lcom/sec/chaton/push/c/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 108
    :cond_6
    invoke-static {}, Lcom/sec/chaton/push/util/d;->a()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/push/c/a/a;->b:I

    .line 109
    invoke-static {}, Lcom/sec/a/a/a/f;->newBuilder()Lcom/sec/a/a/a/g;

    move-result-object v1

    .line 110
    iget v2, p0, Lcom/sec/chaton/push/c/a/a;->b:I

    invoke-virtual {v1, v2}, Lcom/sec/a/a/a/g;->a(I)Lcom/sec/a/a/a/g;

    .line 111
    invoke-static {v0}, Lcom/sec/chaton/push/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/a/a/a/g;->a(Ljava/lang/String;)Lcom/sec/a/a/a/g;

    .line 112
    invoke-static {v0}, Lcom/sec/chaton/push/k;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/g;->b(Ljava/lang/String;)Lcom/sec/a/a/a/g;

    .line 113
    const-string v0, ""

    invoke-virtual {v1, v0}, Lcom/sec/a/a/a/g;->c(Ljava/lang/String;)Lcom/sec/a/a/a/g;

    .line 115
    new-instance v0, Lcom/sec/chaton/push/c/h;

    invoke-direct {v0}, Lcom/sec/chaton/push/c/h;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/push/c/a/a;->d:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/c/h;->a(Lcom/sec/chaton/push/b/a/a;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/a/a/a/g;->d()Lcom/sec/a/a/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/push/c/h;->a(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/sec/chaton/push/c/h;

    move-result-object v0

    goto :goto_0
.end method

.method public f_()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    const-class v0, Lcom/sec/a/a/a/c;

    return-object v0
.end method
