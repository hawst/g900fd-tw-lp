.class public Lcom/sec/chaton/push/c/a/d;
.super Ljava/lang/Object;
.source "NotiGroupMessageTask.java"

# interfaces
.implements Lcom/sec/chaton/push/c/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/sec/chaton/push/b/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/chaton/push/c/a/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/sec/chaton/push/c/a/d;->b:Landroid/content/Context;

    .line 56
    invoke-static {}, Lcom/sec/chaton/push/PushClientApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/PushClientApplication;->j()Lcom/sec/chaton/push/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/push/c/a/d;->c:Lcom/sec/chaton/push/b/a/d;

    .line 57
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/sec/a/a/a/r;)V
    .locals 5

    .prologue
    .line 161
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.sec.chaton.push.NOTIFICATION_ACTION"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x0

    .line 165
    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->h()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 166
    const/4 v0, 0x1

    .line 169
    :cond_0
    const-string v2, "notificationId"

    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    const-string v2, "sender"

    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string v2, "msg"

    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v2, "appData"

    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v2, "timeStamp"

    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->r()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 174
    const-string v2, "sessionInfo"

    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    const-string v2, "connectionTerm"

    invoke-virtual {p2}, Lcom/sec/a/a/a/r;->t()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 176
    const-string v2, "ack"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 178
    const-string v0, "com.sec.chaton.push.BROADCAST_PUSH_MESSAGE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 182
    const-string v0, "#"

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 184
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_0

    .line 185
    sget-object v1, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v2, "ResultCode : %s."

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v5

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    sget-object v1, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v2, "Message : %s."

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v6

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_0
    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 190
    sparse-switch v0, :sswitch_data_0

    .line 252
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_1

    .line 253
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Unknown ResultCode."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 195
    :sswitch_1
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_2

    .line 196
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Server error: internal server error."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_2
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->e()Lcom/sec/chaton/push/c/a/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->a:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_6

    .line 200
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_3

    .line 201
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Close connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/d;->c:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/chaton/push/a/a; {:try_start_0 .. :try_end_0} :catch_1

    .line 210
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_4

    .line 211
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Change target server."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_4
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->d()V

    .line 215
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_5

    .line 216
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Execute initialize."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_5
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    goto :goto_0

    .line 222
    :cond_6
    invoke-static {}, Lcom/sec/chaton/push/c/a/f;->e()Lcom/sec/chaton/push/c/a/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/push/c/a/g;->b:Lcom/sec/chaton/push/c/a/g;

    if-ne v0, v1, :cond_1

    .line 223
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_7

    .line 224
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Close connection."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :cond_7
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/push/c/a/d;->c:Lcom/sec/chaton/push/b/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/push/b/a/d;->b()V
    :try_end_1
    .catch Lcom/sec/chaton/push/a/a; {:try_start_1 .. :try_end_1} :catch_0

    .line 234
    :goto_2
    invoke-static {}, Lcom/sec/chaton/push/k;->b()V

    .line 236
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v0, :cond_8

    .line 237
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Execute reprovisioning."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_8
    invoke-static {}, Lcom/sec/chaton/push/a/b;->a()Lcom/sec/chaton/push/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/push/a/b;->e()V

    goto :goto_0

    .line 229
    :catch_0
    move-exception v0

    goto :goto_2

    .line 206
    :catch_1
    move-exception v0

    goto :goto_1

    .line 190
    nop

    :sswitch_data_0
    .sparse-switch
        0x7d0 -> :sswitch_0
        0x7d1 -> :sswitch_0
        0xfa0 -> :sswitch_0
        0xfa5 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    const-class v0, Lcom/sec/a/a/a/u;

    return-object v0
.end method

.method public a(Lcom/google/protobuf/GeneratedMessageLite;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 66
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "NotiGroupMessageTask.onEvent()"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_1

    .line 71
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "Rescheduling HeartBeat."

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    invoke-static {}, Lcom/sec/chaton/push/heartbeat/HeartBeat;->e()V

    .line 75
    check-cast p1, Lcom/sec/a/a/a/u;

    .line 77
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_2

    .line 78
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "NotiGroup"

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "NotiElement count: %d."

    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/sec/a/a/a/u;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "=========================="

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/sec/chaton/push/util/d;->a(Lcom/google/protobuf/MessageLite;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v1, "=========================="

    invoke-static {v0, v1}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 92
    invoke-virtual {p1}, Lcom/sec/a/a/a/u;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/r;

    .line 93
    invoke-virtual {v0}, Lcom/sec/a/a/a/r;->j()I

    move-result v3

    .line 94
    invoke-virtual {v0}, Lcom/sec/a/a/a/r;->h()I

    move-result v4

    .line 95
    invoke-virtual {v0}, Lcom/sec/a/a/a/r;->f()Ljava/lang/String;

    move-result-object v5

    .line 96
    invoke-virtual {v0}, Lcom/sec/a/a/a/r;->d()Ljava/lang/String;

    move-result-object v6

    .line 98
    const/4 v7, 0x0

    invoke-static {v7}, Lcom/sec/chaton/push/k;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 100
    if-ne v3, v9, :cond_4

    .line 101
    iget-object v3, p0, Lcom/sec/chaton/push/c/a/d;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/a/a/a/r;->n()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/sec/chaton/push/c/a/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_4
    if-eqz v7, :cond_5

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 104
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->c:Z

    if-eqz v0, :cond_3

    .line 105
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v3, "The application id of notification isn\'t registered in device."

    invoke-static {v0, v3}, Lcom/sec/chaton/push/util/g;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_6
    if-ne v4, v9, :cond_9

    .line 112
    sget-boolean v4, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v4, :cond_7

    .line 113
    sget-object v4, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v5, "This notification needs acknowledgement by application level."

    invoke-static {v4, v5}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_7
    :goto_1
    if-eq v3, v8, :cond_c

    if-eq v3, v9, :cond_c

    .line 132
    sget-boolean v3, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v3, :cond_8

    .line 133
    sget-object v3, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v4, "Broadcast notification will be sent to application."

    invoke-static {v3, v4}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_8
    iget-object v3, p0, Lcom/sec/chaton/push/c/a/d;->b:Landroid/content/Context;

    invoke-direct {p0, v3, v0}, Lcom/sec/chaton/push/c/a/d;->a(Landroid/content/Context;Lcom/sec/a/a/a/r;)V

    goto :goto_0

    .line 117
    :cond_9
    if-ne v4, v8, :cond_b

    .line 118
    sget-boolean v4, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v4, :cond_a

    .line 119
    sget-object v4, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v5, "This notification needs acknowledgement by system level."

    invoke-static {v4, v5}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_a
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 125
    :cond_b
    if-nez v4, :cond_7

    .line 126
    sget-boolean v4, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v4, :cond_7

    .line 127
    sget-object v4, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v5, "This notification doesn\'t need acknowledgement"

    invoke-static {v4, v5}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 138
    :cond_c
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_3

    .line 139
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v3, "Broadcast notification will not be sent to application."

    invoke-static {v0, v3}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 145
    :cond_d
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_f

    .line 146
    sget-boolean v0, Lcom/sec/chaton/push/util/g;->a:Z

    if-eqz v0, :cond_e

    .line 147
    sget-object v0, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    const-string v2, "Send system level NotiAcks to push server."

    invoke-static {v0, v2}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_e
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/push/c/i;->a()Lcom/sec/chaton/push/c/i;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/push/c/a/c;

    iget-object v3, p0, Lcom/sec/chaton/push/c/a/d;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/sec/chaton/push/c/a/c;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Lcom/sec/chaton/push/c/i;->a(Lcom/sec/chaton/push/c/a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_f
    :goto_2
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    sget-boolean v1, Lcom/sec/chaton/push/util/g;->d:Z

    if-eqz v1, :cond_f

    .line 154
    sget-object v1, Lcom/sec/chaton/push/c/a/d;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
