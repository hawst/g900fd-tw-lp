.class Lcom/sec/chaton/msgbox/aq;
.super Landroid/os/Handler;
.source "MsgboxSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const v4, 0x7f0b002c

    const/4 v3, 0x0

    .line 590
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 594
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 595
    if-nez v0, :cond_2

    .line 596
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 597
    const-string v0, "resultEntry is null"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 601
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 604
    :sswitch_0
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_3

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/a/a/l;->c:Lcom/sec/chaton/a/a/l;

    if-ne v0, v1, :cond_5

    .line 606
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 607
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 610
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/e/r;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v3}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->o(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)J

    goto :goto_0

    .line 613
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/chaton/d/o;->b(Z)V

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->q(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    .line 619
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 626
    invoke-static {}, Lcom/sec/chaton/j/e;->a()Lcom/sec/chaton/j/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/e;->c()V

    .line 627
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->r(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    goto/16 :goto_0

    .line 632
    :sswitch_1
    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->q(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    .line 636
    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/chaton/d/o;->b(Z)V

    .line 642
    invoke-static {}, Lcom/sec/chaton/j/e;->a()Lcom/sec/chaton/j/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/e;->c()V

    .line 643
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_6

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/a/a/l;->e:Lcom/sec/chaton/a/a/l;

    if-ne v0, v1, :cond_7

    .line 644
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->s(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    .line 645
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 648
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->i()V

    .line 651
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->r(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    goto/16 :goto_0

    .line 652
    :catch_0
    move-exception v0

    .line 654
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 657
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->q(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 663
    :sswitch_2
    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->q(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    .line 664
    iget-object v1, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->r(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    .line 666
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_8

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/a/a/l;->e:Lcom/sec/chaton/a/a/l;

    if-ne v0, v1, :cond_a

    .line 667
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->clearChoices()V

    .line 669
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 670
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 673
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->t(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    goto/16 :goto_0

    .line 675
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/msgbox/aq;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 601
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0x19 -> :sswitch_2
    .end sparse-switch
.end method
