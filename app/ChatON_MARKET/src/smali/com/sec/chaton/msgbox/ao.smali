.class Lcom/sec/chaton/msgbox/ao;
.super Landroid/database/ContentObserver;
.source "MsgboxSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sec/chaton/msgbox/ao;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ao;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    if-eqz v0, :cond_0

    .line 485
    const-string v0, ""

    .line 486
    iget-object v3, p0, Lcom/sec/chaton/msgbox/ao;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v3}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->d(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ao;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->d(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 490
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ao;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->e(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/e/q;->c()Landroid/net/Uri;

    move-result-object v3

    new-array v6, v1, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v6, v5

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    :goto_1
    return-void

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ao;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->e(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/e/q;->b()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v4, v0

    goto :goto_0
.end method
