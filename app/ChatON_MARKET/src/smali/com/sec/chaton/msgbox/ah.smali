.class public Lcom/sec/chaton/msgbox/ah;
.super Lcom/sec/common/util/a;
.source "MsgboxSearchTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/util/a",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Landroid/database/MatrixCursor;",
        ">;"
    }
.end annotation


# static fields
.field static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/chaton/msgbox/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgbox/ah;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/common/util/a;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/chaton/msgbox/ah;->b([Ljava/lang/Object;)Landroid/database/MatrixCursor;

    move-result-object v0

    return-object v0
.end method

.method protected varargs b([Ljava/lang/Object;)Landroid/database/MatrixCursor;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 29
    aget-object v0, p1, v7

    check-cast v0, Ljava/util/ArrayList;

    .line 30
    aget-object v1, p1, v8

    check-cast v1, Ljava/lang/String;

    .line 32
    new-instance v3, Landroid/database/MatrixCursor;

    const/16 v2, 0x26

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "inbox_title"

    aput-object v4, v2, v7

    sget-object v4, Lcom/sec/chaton/msgbox/MsgboxFragment;->d:Ljava/lang/String;

    aput-object v4, v2, v8

    const/4 v4, 0x2

    const-string v5, "inbox_no"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "buddy_no"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "inbox_chat_type"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "inbox_valid"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "relation_buddy_no"

    aput-object v5, v2, v4

    const/4 v4, 0x7

    const-string v5, "buddy_show_phone_number"

    aput-object v5, v2, v4

    const/16 v4, 0x8

    const-string v5, "buddy_extra_info"

    aput-object v5, v2, v4

    const/16 v4, 0x9

    const-string v5, "buddy_msisdns"

    aput-object v5, v2, v4

    const/16 v4, 0xa

    const-string v5, "is_buddy"

    aput-object v5, v2, v4

    const/16 v4, 0xb

    const-string v5, "inbox_last_chat_type"

    aput-object v5, v2, v4

    const/16 v4, 0xc

    const-string v5, "inbox_session_id"

    aput-object v5, v2, v4

    const/16 v4, 0xd

    const-string v5, "inbox_last_msg_no"

    aput-object v5, v2, v4

    const/16 v4, 0xe

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/16 v4, 0xf

    const-string v5, "inbox_server_ip"

    aput-object v5, v2, v4

    const/16 v4, 0x10

    const-string v5, "inbox_server_port"

    aput-object v5, v2, v4

    const/16 v4, 0x11

    const-string v5, "inbox_title_fixed"

    aput-object v5, v2, v4

    const/16 v4, 0x12

    const-string v5, "inbox_last_msg_sender"

    aput-object v5, v2, v4

    const/16 v4, 0x13

    const-string v5, "inbox_last_temp_msg"

    aput-object v5, v2, v4

    const/16 v4, 0x14

    const-string v5, "buddy_name"

    aput-object v5, v2, v4

    const/16 v4, 0x15

    const-string v5, "inbox_trunk_unread_count"

    aput-object v5, v2, v4

    const/16 v4, 0x16

    const-string v5, "inbox_last_timestamp"

    aput-object v5, v2, v4

    const/16 v4, 0x17

    const-string v5, "inbox_last_time"

    aput-object v5, v2, v4

    const/16 v4, 0x18

    const-string v5, "inbox_enable_noti"

    aput-object v5, v2, v4

    const/16 v4, 0x19

    const-string v5, "inbox_unread_count"

    aput-object v5, v2, v4

    const/16 v4, 0x1a

    const-string v5, "inbox_is_entered"

    aput-object v5, v2, v4

    const/16 v4, 0x1b

    const-string v5, "lasst_session_merge_time"

    aput-object v5, v2, v4

    const/16 v4, 0x1c

    const-string v5, "inbox_participants"

    aput-object v5, v2, v4

    const/16 v4, 0x1d

    const-string v5, "inbox_last_tid"

    aput-object v5, v2, v4

    const/16 v4, 0x1e

    const-string v5, "inbox_translated"

    aput-object v5, v2, v4

    const/16 v4, 0x1f

    const-string v5, "inbox_last_message"

    aput-object v5, v2, v4

    const/16 v4, 0x20

    const-string v5, "inbox_is_new"

    aput-object v5, v2, v4

    const/16 v4, 0x21

    const-string v5, "profile_url"

    aput-object v5, v2, v4

    const/16 v4, 0x22

    const-string v5, "inbox_enable_translate"

    aput-object v5, v2, v4

    const/16 v4, 0x23

    const-string v5, "translate_outgoing_message"

    aput-object v5, v2, v4

    const/16 v4, 0x24

    const-string v5, "inbox_translate_my_language"

    aput-object v5, v2, v4

    const/16 v4, 0x25

    const-string v5, "inbox_translate_buddy_language"

    aput-object v5, v2, v4

    invoke-direct {v3, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 42
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 43
    aget-object v2, v0, v7

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 44
    aget-object v5, v0, v8

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 45
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 46
    const-string v2, ""

    .line 48
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 49
    const-string v5, ""

    .line 54
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_3
    return-object v3
.end method
