.class Lcom/sec/chaton/msgbox/t;
.super Lcom/sec/chaton/msgbox/e;
.source "MsgboxFragment.java"


# instance fields
.field final synthetic b:Landroid/database/Cursor;

.field final synthetic c:Lcom/sec/chaton/msgbox/MsgboxFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/util/concurrent/ExecutorService;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 2317
    iput-object p1, p0, Lcom/sec/chaton/msgbox/t;->c:Lcom/sec/chaton/msgbox/MsgboxFragment;

    iput-object p3, p0, Lcom/sec/chaton/msgbox/t;->b:Landroid/database/Cursor;

    invoke-direct {p0, p2}, Lcom/sec/chaton/msgbox/e;-><init>(Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2317
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/msgbox/t;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2321
    iget-object v0, p0, Lcom/sec/chaton/msgbox/t;->c:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2348
    :goto_0
    return-void

    .line 2324
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 2325
    const-string v0, "makeBackUpCursor get result"

    sget-object v1, Lcom/sec/chaton/msgbox/t;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2327
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/t;->c:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2330
    iget-object v0, p0, Lcom/sec/chaton/msgbox/t;->c:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->j(Lcom/sec/chaton/msgbox/MsgboxFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2331
    iget-object v0, p0, Lcom/sec/chaton/msgbox/t;->c:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->B(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/msgbox/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/t;->b:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 2332
    if-eqz v0, :cond_2

    .line 2333
    iget-object v1, p0, Lcom/sec/chaton/msgbox/t;->c:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    .line 2347
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/chaton/msgbox/e;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
