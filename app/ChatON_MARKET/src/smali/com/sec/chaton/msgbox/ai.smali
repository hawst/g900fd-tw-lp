.class Lcom/sec/chaton/msgbox/ai;
.super Ljava/lang/Object;
.source "MsgboxSelectionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const v4, 0x7f07057e

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->toggle()V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Z)Z

    move v0, v1

    .line 253
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/widget/FastScrollableListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 254
    iget-object v2, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v2

    invoke-virtual {v2, v0, v3}, Lcom/sec/widget/FastScrollableListView;->setItemChecked(IZ)V

    .line 253
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Z)Z

    move v0, v1

    .line 258
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/widget/FastScrollableListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 259
    iget-object v2, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/widget/FastScrollableListView;->setItemChecked(IZ)V

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemIds()[J

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->c(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 267
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->invalidateViews()V

    .line 269
    return-void

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ai;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->c(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method
