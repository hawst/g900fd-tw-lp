.class Lcom/sec/chaton/msgbox/i;
.super Ljava/lang/Object;
.source "MsgboxFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 1307
    iput-object p1, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    .line 1426
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1428
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->c(Lcom/sec/chaton/msgbox/MsgboxFragment;)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/sec/chaton/chat/notification/a;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;I)V

    .line 1430
    :cond_0
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1422
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1317
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->i(Lcom/sec/chaton/msgbox/MsgboxFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1318
    const-string v0, "[MSGBOX] onQueryComplete() mIsPause start close cursor"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    if-eqz p3, :cond_0

    .line 1320
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    .line 1323
    :cond_0
    const-string v0, "[MSGBOX] onQueryComplete() mIsPause end close cursor"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    if-ne p1, v3, :cond_1

    .line 1327
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z

    .line 1416
    :cond_1
    :goto_0
    return-void

    .line 1331
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1337
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z

    goto :goto_0

    .line 1340
    :cond_3
    packed-switch p1, :pswitch_data_0

    .line 1403
    if-eqz p3, :cond_4

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 1404
    :cond_4
    if-eqz p3, :cond_5

    .line 1405
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    .line 1411
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->r(Lcom/sec/chaton/msgbox/MsgboxFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1412
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, v2}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z

    goto :goto_0

    .line 1342
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->l(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1343
    const-string v0, "[MSGBOX] onQueryComplete() QUERY_MSGBOX"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    if-eqz p3, :cond_6

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 1346
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->m(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1347
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->n(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/widget/FastScrollableListView;->setVisibility(I)V

    .line 1350
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1351
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1352
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0202b7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1367
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    .line 1379
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->p(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    .line 1381
    if-eqz p3, :cond_5

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 1382
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;I)V

    goto :goto_1

    .line 1356
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->m(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1357
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->n(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableListView;->setVisibility(I)V

    .line 1360
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1361
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1362
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0202b6

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2

    .line 1386
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->q(Lcom/sec/chaton/msgbox/MsgboxFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1387
    if-eqz p3, :cond_9

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_a

    .line 1390
    :cond_9
    if-eqz p3, :cond_1

    .line 1391
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 1397
    :cond_a
    :goto_3
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1398
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->q(Lcom/sec/chaton/msgbox/MsgboxFragment;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, "participants_buddy_no"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1400
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/msgbox/i;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 1340
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 2

    .prologue
    .line 1312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 1313
    return-void
.end method
