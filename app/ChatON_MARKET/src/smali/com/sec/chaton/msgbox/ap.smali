.class Lcom/sec/chaton/msgbox/ap;
.super Ljava/lang/Object;
.source "MsgboxSelectionFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 584
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 578
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/16 v2, 0x8

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 507
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->f(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 508
    if-eqz p3, :cond_0

    .line 509
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 572
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 516
    packed-switch p1, :pswitch_data_0

    .line 566
    if-eqz p3, :cond_2

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 567
    :cond_2
    if-eqz p3, :cond_0

    .line 568
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 518
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->g(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 519
    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_6

    .line 520
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->h(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 521
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/widget/FastScrollableListView;->setVisibility(I)V

    .line 522
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/CheckedTextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 534
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->j(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/msgbox/a;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/chaton/msgbox/a;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 535
    if-eqz v1, :cond_5

    .line 536
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 540
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)[J

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)[J

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    .line 542
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)[J

    move-result-object v1

    array-length v1, v1

    iget-object v2, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/widget/FastScrollableListView;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_7

    .line 543
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/CheckedTextView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 544
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1, v7}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Z)Z

    .line 545
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableListView;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 546
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v1

    invoke-virtual {v1, v0, v7}, Lcom/sec/widget/FastScrollableListView;->setItemChecked(IZ)V

    .line 545
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 524
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->h(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 525
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/widget/FastScrollableListView;->setVisibility(I)V

    .line 527
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)I

    move-result v2

    if-eq v1, v2, :cond_4

    .line 528
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/CheckedTextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    goto :goto_1

    .line 549
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/CheckedTextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 550
    iget-object v1, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Z)Z

    move v1, v0

    .line 551
    :goto_3
    iget-object v2, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v2}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)[J

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_a

    move v2, v0

    .line 552
    :goto_4
    iget-object v3, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v3}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/widget/FastScrollableListView;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_8

    .line 553
    iget-object v3, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v3}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/widget/FastScrollableListView;->getItemIdAtPosition(I)J

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v5}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)[J

    move-result-object v5

    aget-wide v5, v5, v1

    cmp-long v3, v3, v5

    if-nez v3, :cond_9

    .line 554
    iget-object v3, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v3}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v3

    invoke-virtual {v3, v2, v7}, Lcom/sec/widget/FastScrollableListView;->setItemChecked(IZ)V

    .line 551
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 552
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 560
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->invalidateViews()V

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/msgbox/ap;->a:Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;[J)[J

    goto/16 :goto_0

    .line 516
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 2

    .prologue
    .line 502
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 503
    return-void
.end method
