.class Lcom/sec/chaton/msgbox/s;
.super Ljava/lang/Object;
.source "MsgboxFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 2252
    iput-object p1, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2302
    const-string v0, "afterTextChanged()"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2303
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2255
    const-string v0, "beforeTextChanged()"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 2261
    const-string v0, "onTextChanged() Start"

    const-string v1, "[SEARCH]"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262
    const-string v0, "onTextChanged()"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268
    iget-object v0, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2269
    iget-object v0, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->x(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    .line 2277
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2297
    :goto_1
    return-void

    .line 2271
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->y(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    goto :goto_0

    .line 2284
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->z(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2286
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 2287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTextChanged(is text)["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2288
    iget-object v1, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/msgbox/MsgboxFragment;->d(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z

    .line 2289
    iget-object v1, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/lang/String;)V

    .line 2296
    :goto_2
    const-string v0, "onTextChanged() End"

    const-string v1, "[SEARCH]"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2291
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTextChanged(clear text)["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2292
    iget-object v0, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->d(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z

    .line 2293
    iget-object v0, p0, Lcom/sec/chaton/msgbox/s;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->A(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    goto :goto_2
.end method
