.class public Lcom/sec/chaton/msgbox/MsgboxFragment;
.super Landroid/support/v4/app/ListFragment;
.source "MsgboxFragment.java"

# interfaces
.implements Lcom/coolots/sso/a/c;
.implements Lcom/sec/chaton/by;
.implements Lcom/sec/chaton/msgbox/au;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;


# instance fields
.field private A:I

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:Lcom/sec/chaton/d/o;

.field private E:Landroid/app/ProgressDialog;

.field private F:Lcom/sec/common/a/d;

.field private G:Landroid/view/View;

.field private H:Lcom/sec/chaton/widget/ImageTextViewGroup;

.field private I:Z

.field private J:Z

.field private K:I

.field private L:Lcom/sec/common/f/c;

.field private M:Lcom/sec/chaton/util/bt;

.field private N:Lcom/sec/chaton/chat/fh;

.field private O:Z

.field private P:Z

.field private Q:Landroid/view/View;

.field private R:Lcom/sec/chaton/widget/ClearableEditText;

.field private S:Z

.field private T:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private U:Landroid/widget/ImageButton;

.field private V:Landroid/widget/ImageButton;

.field private W:Landroid/view/View;

.field private X:Landroid/view/View;

.field private Y:Landroid/widget/RelativeLayout;

.field private final Z:Ljava/lang/String;

.field private aa:Landroid/widget/LinearLayout$LayoutParams;

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:Ljava/util/concurrent/ExecutorService;

.field private ag:Landroid/os/Handler;

.field private ah:Landroid/text/TextWatcher;

.field private ai:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

.field e:Landroid/database/ContentObserver;

.field f:Lcom/sec/chaton/e/a/v;

.field public g:Landroid/os/Handler;

.field h:Landroid/content/BroadcastReceiver;

.field i:Landroid/database/ContentObserver;

.field private final j:I

.field private k:Lcom/sec/widget/FastScrollableListView;

.field private l:Landroid/widget/LinearLayout;

.field private m:Landroid/widget/LinearLayout;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Lcom/sec/chaton/msgbox/a;

.field private r:Z

.field private s:Lcom/sec/chaton/e/a/u;

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:J

.field private x:Lcom/sec/chaton/e/r;

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    const-class v0, Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    .line 179
    const-string v0, "extra_search_title"

    sput-object v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->b:Ljava/lang/String;

    .line 180
    const-string v0, "extra_search_cursor"

    sput-object v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->c:Ljava/lang/String;

    .line 181
    const-string v0, "chiness_chat_title"

    sput-object v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 138
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 142
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->j:I

    .line 198
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->r:Z

    .line 214
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->B:Z

    .line 221
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->C:Ljava/lang/String;

    .line 228
    iput-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    .line 236
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->I:Z

    .line 237
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->J:Z

    .line 239
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    .line 256
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->O:Z

    .line 259
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->P:Z

    .line 266
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->S:Z

    .line 276
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Z:Ljava/lang/String;

    .line 280
    iput-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->aa:Landroid/widget/LinearLayout$LayoutParams;

    .line 283
    iput-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->af:Ljava/util/concurrent/ExecutorService;

    .line 1258
    new-instance v0, Lcom/sec/chaton/msgbox/h;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/msgbox/h;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->e:Landroid/database/ContentObserver;

    .line 1307
    new-instance v0, Lcom/sec/chaton/msgbox/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/i;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->f:Lcom/sec/chaton/e/a/v;

    .line 1433
    new-instance v0, Lcom/sec/chaton/msgbox/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/j;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ag:Landroid/os/Handler;

    .line 2065
    new-instance v0, Lcom/sec/chaton/msgbox/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/p;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->g:Landroid/os/Handler;

    .line 2252
    new-instance v0, Lcom/sec/chaton/msgbox/s;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/s;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ah:Landroid/text/TextWatcher;

    .line 2440
    new-instance v0, Lcom/sec/chaton/msgbox/w;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/w;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ai:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 2585
    new-instance v0, Lcom/sec/chaton/msgbox/x;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/x;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->h:Landroid/content/BroadcastReceiver;

    .line 2610
    new-instance v0, Lcom/sec/chaton/msgbox/y;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/msgbox/y;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->i:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->m()V

    return-void
.end method

.method static synthetic B(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/msgbox/a;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    return-object v0
.end method

.method private a(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 670
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x425c0000    # 55.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    const/high16 v2, 0x425c0000    # 55.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v0, p1, v1, v2}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 676
    :goto_0
    return-object v0

    .line 671
    :catch_0
    move-exception v0

    .line 672
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 673
    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 676
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/widget/ProfileImageView;
    .locals 6

    .prologue
    .line 682
    new-instance v1, Lcom/sec/chaton/widget/ProfileImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/sec/chaton/widget/ProfileImageView;-><init>(Landroid/content/Context;)V

    .line 683
    invoke-virtual {v1, v1, p4}, Lcom/sec/chaton/widget/ProfileImageView;->a(Landroid/view/View;Lcom/sec/chaton/e/r;)V

    .line 685
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne p4, v0, :cond_1

    .line 689
    if-eqz p1, :cond_0

    .line 690
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    invoke-virtual {v0, v1, p3}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 691
    invoke-virtual {v1, p3}, Lcom/sec/chaton/widget/ProfileImageView;->setBuddyNo(Ljava/lang/String;)V

    :cond_0
    :goto_0
    move-object v0, v1

    .line 740
    :goto_1
    return-object v0

    .line 696
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    .line 700
    invoke-static {p4}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 702
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_chat_profile.png_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 703
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 704
    invoke-direct {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/ProfileImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 706
    :catch_0
    move-exception v0

    .line 707
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    move-object v0, v1

    .line 708
    goto :goto_1

    .line 711
    :cond_2
    const-string v0, ""

    .line 712
    invoke-static {p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 713
    const-string v0, "&filename="

    invoke-virtual {p8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 714
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v0, v3

    .line 717
    :cond_3
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_chat_profile.png_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 720
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 721
    invoke-direct {p0, v3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/ProfileImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 732
    :catch_1
    move-exception v0

    .line 733
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    move-object v0, v1

    .line 734
    goto/16 :goto_1

    .line 724
    :cond_4
    :try_start_2
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 725
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p7}, Lcom/sec/chaton/e/a/f;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 726
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_group_profile.png_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 727
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 728
    invoke-direct {p0, v3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/ProfileImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(J)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1983
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 1986
    sub-long/2addr v3, p1

    const-wide/32 v5, 0x927c0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    move v0, v1

    .line 1990
    :goto_0
    iget-boolean v3, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->P:Z

    if-eqz v3, :cond_0

    .line 1991
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->O:Z

    .line 1992
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->l()V

    .line 1994
    :cond_0
    invoke-direct {p0, v0, v2}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZZ)V

    .line 1995
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 11

    .prologue
    .line 892
    const-string v0, "inbox_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 893
    const-string v0, "inbox_title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 894
    const-string v0, "inbox_session_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 895
    const-string v0, "inbox_chat_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v5

    .line 896
    const-string v0, "inbox_server_ip"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 897
    const-string v0, "inbox_server_port"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 898
    const-string v0, "inbox_title_fixed"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 899
    const-string v0, "buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 900
    const-string v0, "Y"

    const-string v2, "inbox_enable_noti"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 901
    const-string v0, "relation_buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 902
    invoke-static {v3, v5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    move-object v0, p0

    .line 904
    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 905
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->j()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;I)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->e(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 138
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct/range {p0 .. p10}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct/range {p0 .. p10}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;ZZ)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1070
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1072
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->j()V

    .line 1073
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077
    :goto_0
    return-void

    .line 1074
    :catch_0
    move-exception v0

    .line 1075
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 919
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 920
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p3}, Lcom/sec/chaton/e/z;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "participants_buddy_no"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 921
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 922
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 923
    const-string v2, "participants_buddy_no"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 926
    :cond_0
    if-eqz v1, :cond_1

    .line 927
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 930
    :cond_1
    const/4 v1, 0x0

    .line 931
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p10

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 935
    :goto_1
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/chat/ChatInfoActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 936
    const-string v1, "ACTIVITY_PURPOSE"

    const/4 v3, 0x6

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 937
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->j:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 938
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    invoke-virtual {v2, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 939
    const-string v1, "inboxNO"

    invoke-virtual {v2, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 940
    const-string v1, "chatType"

    invoke-virtual {p5}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 941
    const-string v1, "buddyNO"

    invoke-virtual {v2, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 942
    const-string v1, "inboxValid"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 943
    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 944
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    invoke-virtual {v2, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 945
    const-string v1, "sessionID"

    invoke-virtual {v2, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 946
    const-string v1, "inbox_server_ip"

    invoke-virtual {v2, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 947
    const-string v1, "inbox_server_port"

    move/from16 v0, p8

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 948
    const-string v1, "ACTIVITY_PURPOSE_ARG"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 949
    const-string v1, "inbox_title_fixed"

    move-object/from16 v0, p9

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 950
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 951
    const-string v1, "groupId"

    move-object/from16 v0, p10

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 954
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0, v2, v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 955
    return-void

    :cond_3
    move-object/from16 p10, v1

    goto :goto_1
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    .line 477
    .line 478
    if-eqz p1, :cond_1

    .line 479
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 480
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "do_not_show_max_chat_list_check_day"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    move-wide v2, v0

    .line 486
    :goto_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->J:Z

    .line 487
    cmp-long v4, v2, v5

    if-eqz v4, :cond_0

    .line 488
    sub-long/2addr v0, v2

    .line 489
    const-wide v2, 0x9a7ec800L

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 490
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->J:Z

    .line 493
    :cond_0
    return-void

    .line 482
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "do_not_show_max_chat_list_check_day"

    invoke-virtual {v0, v1, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 483
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 965
    if-eqz p1, :cond_0

    .line 966
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p2, v1}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)I

    .line 970
    :goto_0
    return-void

    .line 968
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)I

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 989
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SHORTCUT] CONTEXT_MENU_ADD_SHORTCUT start, Memory Address:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    const/4 v8, 0x0

    .line 995
    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p9

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v8, p9

    :cond_0
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p5

    move-object v6, p4

    move-object/from16 v7, p9

    move-object/from16 v9, p10

    .line 999
    invoke-direct/range {v1 .. v9}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/widget/ProfileImageView;

    move-result-object v7

    move-object v1, p0

    move-object v2, p2

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p8

    move/from16 v6, p7

    .line 1000
    invoke-direct/range {v1 .. v6}, Lcom/sec/chaton/msgbox/MsgboxFragment;->c(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 1002
    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-ne v0, v1, :cond_b

    .line 1005
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v1, p3, v0}, Lcom/sec/chaton/util/ac;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/e/r;)Landroid/content/Intent;

    move-result-object v4

    .line 1007
    const/4 v3, 0x0

    .line 1009
    :try_start_0
    invoke-virtual {v7}, Lcom/sec/chaton/widget/ProfileImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1010
    if-nez v3, :cond_1

    .line 1011
    :try_start_1
    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-ne v0, v1, :cond_5

    .line 1012
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0203e5

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1018
    :cond_1
    :goto_1
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v3, v1, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1019
    invoke-static {v1}, Lcom/sec/chaton/util/bt;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 1024
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1026
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1027
    if-eqz p1, :cond_9

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-ne v0, v1, :cond_9

    .line 1028
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1030
    :goto_3
    if-eqz p1, :cond_2

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-ne v0, v2, :cond_2

    .line 1031
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1033
    :cond_2
    if-eqz p1, :cond_3

    invoke-static/range {p5 .. p5}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1034
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1038
    :cond_3
    :goto_4
    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-ne v0, v2, :cond_6

    .line 1039
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v5, 0x1e

    if-le v2, v5, :cond_4

    .line 1040
    const/4 v2, 0x0

    const/16 v5, 0x1d

    invoke-virtual {v1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1041
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1051
    :cond_4
    :goto_5
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v2}, Lcom/sec/chaton/util/ac;->a(F)I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v5}, Lcom/sec/chaton/util/ac;->a(F)I

    move-result v5

    const/4 v6, 0x1

    invoke-static {v3, v2, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1052
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v4, v1, v2}, Lcom/sec/chaton/util/ac;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1053
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SHORTCUT] CONTEXT_MENU_ADD_SHORTCUT end, Memory Address:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    return-void

    .line 1014
    :cond_5
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0203e6

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v3

    goto/16 :goto_1

    .line 1020
    :catch_0
    move-exception v1

    move-object v10, v1

    move-object v1, v3

    move-object v3, v10

    .line 1021
    :goto_6
    sget-object v5, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1043
    :cond_6
    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    move-object/from16 v0, p5

    if-eq v0, v2, :cond_7

    invoke-static/range {p5 .. p5}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1044
    :cond_7
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v5, 0x1e

    if-le v2, v5, :cond_8

    .line 1045
    const/4 v2, 0x0

    const/16 v5, 0x1d

    invoke-virtual {v1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1046
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1048
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, p7, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 1020
    :catch_1
    move-exception v1

    move-object v10, v1

    move-object v1, v3

    move-object v3, v10

    goto :goto_6

    :cond_9
    move-object v1, v2

    goto/16 :goto_3

    :cond_a
    move-object v1, v2

    goto/16 :goto_4

    :cond_b
    move-object p3, p4

    goto/16 :goto_0
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    .line 1999
    const-string v0, "syncChatList()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2001
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2002
    const-string v0, "[MSGBOX] [Before mapping] Don\'t need to sync ChatListInfo"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2030
    :cond_0
    :goto_0
    return-void

    .line 2005
    :cond_1
    if-eqz p1, :cond_2

    .line 2006
    invoke-static {}, Lcom/sec/chaton/provider/a/a;->c()V

    .line 2008
    const-string v0, "[MSGBOX] Need to sync ChatListInfo"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2010
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "multidevice_chatlist_sync_last_call_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2016
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->g:Landroid/os/Handler;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "multidevice_chatlist_sync_last_time"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/chat/fh;->a(Landroid/os/Handler;J)Lcom/sec/chaton/chat/fh;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->N:Lcom/sec/chaton/chat/fh;

    .line 2018
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->N:Lcom/sec/chaton/chat/fh;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/fh;->a()V

    .line 2020
    if-eqz p2, :cond_0

    .line 2022
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2023
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 2027
    :cond_2
    const-string v0, "[MSGBOX] [After mapping] Don\'t need to sync ChatListInfo"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z
    .locals 0

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->O:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/util/bt;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->M:Lcom/sec/chaton/util/bt;

    return-object v0
.end method

.method private b(I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1538
    invoke-direct {p0, v8}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Z)V

    .line 1539
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->J:Z

    if-nez v0, :cond_1

    .line 1540
    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    .line 1541
    iput-boolean v7, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->J:Z

    .line 1543
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 1545
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 1546
    const v0, 0x7f0b005b

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 1548
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1549
    const v1, 0x7f0300bd

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1550
    const v0, 0x7f070357

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1551
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v4, 0x7f0b0316

    new-array v5, v7, [Ljava/lang/Object;

    const/16 v6, 0x1e

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1552
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1553
    const v1, 0x7f070356

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1555
    const v4, 0x7f0b0315

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1556
    invoke-virtual {v2, v7}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    .line 1557
    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    .line 1558
    invoke-virtual {v2, v7}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v3, 0x7f0b0037

    new-instance v4, Lcom/sec/chaton/msgbox/l;

    invoke-direct {v4, p0, v0}, Lcom/sec/chaton/msgbox/l;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v3, 0x7f0b0039

    new-instance v4, Lcom/sec/chaton/msgbox/k;

    invoke-direct {v4, p0, v0}, Lcom/sec/chaton/msgbox/k;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1577
    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    .line 1579
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1580
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1586
    :cond_1
    return-void
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 958
    const-string v0, "inbox_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 959
    const-string v1, "Y"

    const-string v2, "inbox_enable_noti"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 961
    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZLjava/lang/String;)V

    .line 962
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->f(Landroid/database/Cursor;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2355
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2380
    :cond_0
    :goto_0
    return-void

    .line 2361
    :cond_1
    new-instance v0, Lcom/sec/chaton/msgbox/u;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->af:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/msgbox/u;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/util/concurrent/ExecutorService;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/u;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;I)V
    .locals 10

    .prologue
    const/4 v1, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1081
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->s:Lcom/sec/chaton/e/a/u;

    invoke-static {p1}, Lcom/sec/chaton/e/z;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "participants_buddy_no"

    aput-object v5, v4, v9

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1086
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v9}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1106
    :goto_0
    return-void

    .line 1091
    :cond_0
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    if-ne v0, v1, :cond_2

    .line 1092
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msgbox_close_popup_show"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1094
    :goto_1
    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    .line 1095
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    .line 1097
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    move v0, v8

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z
    .locals 0

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->P:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/msgbox/MsgboxFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 744
    .line 745
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne p2, v0, :cond_1

    .line 746
    const-string v0, "Y"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 747
    if-eqz p1, :cond_0

    .line 762
    :cond_0
    :goto_0
    return-object p3

    .line 753
    :cond_1
    invoke-static {p2}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 754
    invoke-static {p4, p3}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 756
    :cond_2
    if-nez p5, :cond_0

    .line 757
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0155

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    goto :goto_0
.end method

.method private c(Landroid/database/Cursor;)V
    .locals 11

    .prologue
    .line 973
    const-string v0, "inbox_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 974
    const-string v0, "inbox_title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 975
    const-string v0, "inbox_chat_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v5

    .line 976
    const-string v0, "inbox_participants"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 977
    const-string v0, "buddy_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 978
    const-string v0, "buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 979
    const-string v0, "Y"

    const-string v1, "inbox_valid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 980
    const-string v0, "inbox_title_fixed"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 981
    const-string v0, "relation_buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 982
    const-string v0, "profile_url"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object v0, p0

    .line 984
    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Z)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    return-object v0
.end method

.method private d(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 1057
    const-string v0, "inbox_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1058
    const-string v0, "inbox_session_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1059
    const-string v0, "inbox_chat_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v3

    .line 1060
    const-string v0, "inbox_server_ip"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1061
    const-string v0, "inbox_server_port"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 1063
    invoke-static {v1, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    move-object v0, p0

    .line 1065
    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;I)V

    .line 1066
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->c(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)Z
    .locals 0

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->S:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/msgbox/MsgboxFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->v:Ljava/lang/String;

    return-object v0
.end method

.method private e(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 2248
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/b/e;->c()Landroid/os/Message;

    move-result-object v0

    .line 2249
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2250
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/b/e;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2251
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->d(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/msgbox/MsgboxFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->y:Ljava/lang/String;

    return-object v0
.end method

.method private f(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 2307
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 2350
    :cond_0
    :goto_0
    return-void

    .line 2310
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2317
    new-instance v0, Lcom/sec/chaton/msgbox/t;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->af:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/chaton/msgbox/t;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/util/concurrent/ExecutorService;Landroid/database/Cursor;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/t;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private g()I
    .locals 1

    .prologue
    .line 309
    const v0, 0x7f030127

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/msgbox/MsgboxFragment;)I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->z:I

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/msgbox/MsgboxFragment;)I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->A:I

    return v0
.end method

.method private h()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x8

    .line 1180
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1213
    :goto_0
    return-void

    .line 1185
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1187
    const-string v0, "setDisplayFirstSyncButton() mChatListSync is null. ERROR."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1191
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "multidevice_chatlist_sync_last_time"

    invoke-virtual {v0, v1, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    .line 1192
    cmp-long v0, v1, v5

    if-eqz v0, :cond_3

    .line 1194
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1195
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setVisibility(I)V

    .line 1197
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1205
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1206
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Y:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1207
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1208
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->X:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1212
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDisplayFirstSyncButton() lastChatListSyncCallTime["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1201
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1202
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private i()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1605
    new-instance v0, Lcom/sec/chaton/msgbox/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/m;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    .line 1612
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 1613
    if-eqz v2, :cond_0

    array-length v0, v2

    if-lez v0, :cond_0

    .line 1614
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1615
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v5

    .line 1616
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Delete File] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1614
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1625
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    .line 1627
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 1629
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0167

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1630
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/msgbox/MsgboxFragment;)Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->I:Z

    return v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 2048
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2052
    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v1, :cond_0

    .line 2053
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2059
    :cond_0
    :goto_0
    return-void

    .line 2055
    :catch_0
    move-exception v0

    .line 2056
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[MSGBOX] mSearchFieldText.setOnFocusChangeListener() Exception :: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/msgbox/MsgboxFragment;)Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->S:Z

    return v0
.end method

.method static synthetic k(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->s:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 2127
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 2128
    const v1, 0x7f0b03e3

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 2129
    const v1, 0x7f0b03e5

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 2130
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/msgbox/r;

    invoke-direct {v2, p0}, Lcom/sec/chaton/msgbox/r;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 2136
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 2137
    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->m:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private l()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2181
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->O:Z

    if-eqz v0, :cond_0

    .line 2182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->O:Z

    .line 2183
    const-string v0, "[MSGBOX] updateMsgbox()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2184
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->s:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x1

    invoke-static {}, Lcom/sec/chaton/e/q;->b()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2186
    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->l:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2383
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->b(Ljava/lang/String;)V

    .line 2384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->O:Z

    .line 2385
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->l()V

    .line 2386
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/widget/FastScrollableListView;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->k:Lcom/sec/widget/FastScrollableListView;

    return-object v0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 2541
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2542
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2543
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2544
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->e()V

    .line 2548
    :cond_0
    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 2551
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2552
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2553
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2554
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->f()V

    .line 2558
    :cond_0
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 2562
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2563
    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2564
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2565
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2566
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2568
    const-string v0, "date_format"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2569
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->i:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2571
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2572
    const-string v0, "registerListener for system event"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2574
    :cond_0
    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->h()V

    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/msgbox/MsgboxFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->t:Ljava/util/ArrayList;

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2577
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2578
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->i:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2580
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2581
    const-string v0, "unregisterListener for system event"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2583
    :cond_0
    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/msgbox/MsgboxFragment;)Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->P:Z

    return v0
.end method

.method static synthetic s(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/d/o;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->D:Lcom/sec/chaton/d/o;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ag:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->k()V

    return-void
.end method

.method static synthetic x(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o()V

    return-void
.end method

.method static synthetic y(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->n()V

    return-void
.end method

.method static synthetic z(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 624
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 625
    const-string v0, "inbox_is_new"

    const-string v1, "N"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->s:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x1

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 627
    return-void
.end method

.method public a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2522
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2523
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0702e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2524
    if-eqz v0, :cond_0

    .line 2525
    packed-switch p1, :pswitch_data_0

    .line 2537
    :cond_0
    :goto_0
    return-void

    .line 2527
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->aa:Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ae:I

    iget v3, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ab:I

    invoke-virtual {v1, v4, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2528
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->aa:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2531
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->aa:Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ae:I

    iget v3, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ac:I

    invoke-virtual {v1, v4, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2532
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->aa:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2525
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1894
    iput-object p1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->C:Ljava/lang/String;

    .line 1895
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/msgbox/a;->a(Ljava/lang/String;)V

    .line 1896
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->invalidateViews()V

    .line 1897
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 11

    .prologue
    .line 1634
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msgbox_close_popup_show"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1636
    if-nez v0, :cond_0

    .line 1637
    invoke-virtual/range {p0 .. p5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1679
    :goto_0
    return-void

    .line 1639
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v8

    .line 1640
    const v0, 0x7f0b000e

    invoke-virtual {v8, v0}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 1642
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1643
    const v1, 0x7f0300bd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1644
    const v0, 0x7f070357

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 1645
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 1646
    if-eqz v2, :cond_1

    .line 1647
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1650
    :cond_1
    const v0, 0x7f070356

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1654
    const v3, 0x7f0b0033

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1656
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    .line 1657
    invoke-virtual {v8, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    .line 1659
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v9

    const v10, 0x7f0b0037

    new-instance v0, Lcom/sec/chaton/msgbox/o;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/msgbox/o;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/widget/CheckBox;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v9, v10, v0}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/msgbox/n;

    invoke-direct {v2, p0}, Lcom/sec/chaton/msgbox/n;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1677
    invoke-virtual {v8}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    .line 1951
    const-string v0, "[MSGBOX] onTabSelected()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "multidevice_chatlist_sync_last_call_time"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1954
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 1955
    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(J)V

    .line 1960
    :cond_0
    invoke-static {p0, v4}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 1963
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1964
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1965
    sget-object v0, Lcom/sec/chaton/buddy/EmptyFragment;->c:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/buddy/EmptyFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1966
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v2, 0x7f070009

    const-class v3, Lcom/sec/chaton/buddy/EmptyFragment;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 1971
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    if-eqz v0, :cond_2

    .line 1972
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->a(Ljava/lang/String;)V

    .line 1976
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v4, :cond_3

    .line 1977
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00050001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1979
    :cond_3
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1684
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq p2, v0, :cond_2

    .line 1687
    invoke-static {p1, p2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->D:Lcom/sec/chaton/d/o;

    .line 1688
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->D:Lcom/sec/chaton/d/o;

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ag:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 1689
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->D:Lcom/sec/chaton/d/o;

    invoke-virtual {v0, p1, p4, p5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 1691
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->D:Lcom/sec/chaton/d/o;

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/d/o;->c(J)V

    .line 1692
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1693
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1696
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->D:Lcom/sec/chaton/d/o;

    invoke-virtual {v0, p2, p3, p1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)J

    .line 1698
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->D:Lcom/sec/chaton/d/o;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Z)V

    .line 1721
    :cond_1
    :goto_0
    return-void

    .line 1701
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->s:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inbox_no IN (\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1702
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->s:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "message_inbox_no=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1705
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/j/c/a;->a(Ljava/lang/String;)Z

    .line 1707
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/j/c/g;->a(Ljava/lang/String;)Z

    .line 1709
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->i()V

    .line 1712
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1713
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(Ljava/lang/String;)V

    .line 1715
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    .line 1716
    const v1, 0x7f0704a1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/sec/chaton/chat/EmptyChatFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2034
    const-string v0, "[MSGBOX] onTabUnSelected()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2036
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    .line 2037
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2039
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->j()V

    .line 2041
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 2042
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->r:Z

    if-eqz v0, :cond_1

    .line 2043
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a()V

    .line 2045
    :cond_1
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 2480
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    if-eqz v0, :cond_0

    .line 2481
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->a(Z)V

    .line 2482
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/a;->notifyDataSetChanged()V

    .line 2484
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Y:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 2485
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Y:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2487
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->X:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2488
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->X:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2490
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 2491
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2493
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(I)V

    .line 2494
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 2497
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    if-eqz v0, :cond_0

    .line 2498
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->a(Z)V

    .line 2499
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/a;->notifyDataSetChanged()V

    .line 2501
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Y:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 2502
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Y:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2504
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2505
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2507
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->X:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 2508
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->X:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2510
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(I)V

    .line 2511
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 2514
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->a(Ljava/lang/String;)V

    .line 2517
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 497
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 498
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 499
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 500
    const-string v1, "content_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 502
    iput-boolean v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->B:Z

    .line 509
    :cond_0
    const-string v1, "download_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 512
    :cond_1
    const-string v1, "sub_content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 515
    :cond_2
    const-string v1, "forward_sender_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 518
    :cond_3
    const-string v1, "inboxNO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 521
    :cond_4
    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 522
    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    .line 525
    :cond_5
    sget-object v1, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 531
    :cond_6
    const-string v1, "is_forward_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 535
    :cond_7
    invoke-static {p0, v2}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 538
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 540
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/msgbox/a;->a(I)V

    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 543
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 546
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_a

    .line 547
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/msgbox/a;->a(I)V

    .line 556
    :cond_9
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b035b

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    .line 559
    return-void

    .line 549
    :cond_a
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    .line 550
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/msgbox/a;->a(I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 1801
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1802
    packed-switch p1, :pswitch_data_0

    .line 1891
    :cond_0
    :goto_0
    return-void

    .line 1806
    :pswitch_0
    const-string v0, "inboxNO"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1807
    const-string v0, "buddyNO"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1808
    const-string v0, "chatType"

    invoke-virtual {p3, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 1809
    invoke-static {v8}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    .line 1810
    const-string v0, "receivers"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 1811
    const-string v0, "inbox_session_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1812
    const-string v0, "inbox_server_ip"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1813
    const-string v0, "inbox_server_port"

    invoke-virtual {p3, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 1815
    const-string v0, "isClosing"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 1816
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 1818
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->r:Z

    if-eqz v0, :cond_2

    .line 1819
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a()V

    .line 1821
    :cond_2
    if-eqz v9, :cond_0

    .line 1822
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1824
    array-length v3, v9

    move v0, v6

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v9, v0

    .line 1825
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1824
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1830
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 1831
    const-string v0, "callChatList"

    invoke-virtual {v3, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1834
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v0}, Lcom/sec/chaton/e/r;->a()I

    move-result v0

    if-ne v8, v0, :cond_4

    .line 1835
    const-string v0, "chatType"

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1836
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v3, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1837
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1844
    :goto_2
    const-string v0, "inboxValid"

    invoke-virtual {v3, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1845
    const-string v1, "receivers"

    new-array v0, v6, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1846
    invoke-virtual {p0, v3}, Lcom/sec/chaton/msgbox/MsgboxFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1839
    :cond_4
    const-string v0, "inboxNO"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1840
    const-string v0, "chatType"

    invoke-virtual {v3, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1841
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->m:Ljava/lang/String;

    invoke-virtual {v3, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 1802
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2428
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 2430
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1758
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1760
    invoke-virtual {p0, v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(I)V

    .line 1783
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    .line 1784
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->n()V

    .line 1790
    :cond_0
    :goto_0
    return-void

    .line 1785
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1786
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1787
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->o()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 287
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->t:Ljava/util/ArrayList;

    .line 289
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    .line 290
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->f:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->s:Lcom/sec/chaton/e/a/u;

    .line 294
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ab:I

    .line 296
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ac:I

    .line 297
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ad:I

    .line 298
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ae:I

    .line 299
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    iget v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ad:I

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->aa:Landroid/widget/LinearLayout$LayoutParams;

    .line 304
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->p()V

    .line 305
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->af:Ljava/util/concurrent/ExecutorService;

    .line 306
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 19

    .prologue
    .line 768
    invoke-super/range {p0 .. p3}, Landroid/support/v4/app/ListFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 769
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 889
    :cond_0
    :goto_0
    return-void

    .line 775
    :cond_1
    new-instance v18, Lcom/sec/chaton/b/a;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 777
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 778
    if-eqz p3, :cond_0

    .line 789
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    move-object/from16 v0, p3

    iget v3, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v2, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 791
    const-string v3, "inbox_no"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->u:Ljava/lang/String;

    .line 792
    const-string v3, "inbox_title"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 793
    const-string v3, "inbox_session_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->v:Ljava/lang/String;

    .line 794
    const-string v3, "lasst_session_merge_time"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->w:J

    .line 795
    const-string v3, "inbox_chat_type"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    .line 796
    const-string v3, "inbox_server_ip"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->y:Ljava/lang/String;

    .line 797
    const-string v3, "inbox_server_port"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->z:I

    .line 800
    const-string v3, "inbox_participants"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->A:I

    .line 801
    const-string v3, "buddy_name"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 802
    const-string v3, "inbox_title_fixed"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 803
    const-string v3, "profile_url"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 804
    const-string v3, "buddy_no"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 805
    const-string v3, "Y"

    const-string v5, "inbox_valid"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 806
    const-string v3, "Y"

    const-string v5, "inbox_enable_noti"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 808
    const-string v3, "relation_buddy_no"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 810
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->u:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->v:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    .line 811
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->u:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->w:J

    invoke-virtual {v2, v9, v10}, Lcom/sec/chaton/d/o;->a(J)V

    .line 813
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serverIP:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 814
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serverPort:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->z:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 817
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 818
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    .line 831
    :cond_2
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 832
    const/4 v2, 0x0

    .line 838
    :goto_2
    if-eqz v11, :cond_5

    .line 842
    if-nez v2, :cond_3

    .line 843
    const v2, 0x7f0b01a6

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v9

    new-instance v2, Lcom/sec/chaton/msgbox/ac;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/sec/chaton/msgbox/ac;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 854
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 855
    if-eqz v5, :cond_a

    .line 856
    const v2, 0x7f0b0293

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/msgbox/ad;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v5}, Lcom/sec/chaton/msgbox/ad;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 873
    :cond_4
    :goto_3
    const v2, 0x7f0b03da

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v9, Lcom/sec/chaton/msgbox/af;

    move-object/from16 v10, p0

    move-object v13, v4

    move-object v14, v6

    move-object v15, v7

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v17}, Lcom/sec/chaton/msgbox/af;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 881
    :cond_5
    const v2, 0x7f0b000e

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/msgbox/g;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/chaton/msgbox/g;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 820
    :cond_6
    if-eqz v11, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_7

    .line 821
    const v2, 0x7f0b0155

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    .line 823
    :cond_7
    if-eqz v11, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v2, v3, :cond_8

    .line 824
    const v2, 0x7f0b00b4

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    .line 826
    :cond_8
    if-eqz v11, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/msgbox/MsgboxFragment;->x:Lcom/sec/chaton/e/r;

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 827
    const v2, 0x7f0b0155

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    goto/16 :goto_1

    .line 834
    :cond_9
    const-string v2, "0999"

    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    goto/16 :goto_2

    .line 864
    :cond_a
    const v2, 0x7f0b0292

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/msgbox/ae;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v5}, Lcom/sec/chaton/msgbox/ae;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;Z)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2243
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2244
    const v0, 0x7f0f0009

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2245
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const v10, 0x7f0b03e3

    const v9, 0x7f07014c

    const/16 v6, 0x8

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 314
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->L:Lcom/sec/common/f/c;

    .line 315
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->M:Lcom/sec/chaton/util/bt;

    .line 317
    const v0, 0x7f0300bc

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 321
    const v0, 0x7f07034f

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Q:Landroid/view/View;

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Q:Landroid/view/View;

    const-string v1, "#fafafa"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Q:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v1, v8, [Landroid/text/InputFilter;

    new-instance v2, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/16 v4, 0x1e

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 326
    iput-boolean v5, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->S:Z

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ah:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 347
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Lcom/sec/chaton/msgbox/f;

    invoke-direct {v1, p0}, Lcom/sec/chaton/msgbox/f;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Q:Landroid/view/View;

    const v1, 0x7f0702d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    const v1, 0x7f0202b5

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0350

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 362
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 364
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/msgbox/q;

    invoke-direct {v1, p0}, Lcom/sec/chaton/msgbox/q;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 377
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Q:Landroid/view/View;

    const v1, 0x7f0702d8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    const v1, 0x7f0202b7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->V:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/msgbox/z;

    invoke-direct {v1, p0}, Lcom/sec/chaton/msgbox/z;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    const v0, 0x7f070353

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->l:Landroid/widget/LinearLayout;

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->l:Landroid/widget/LinearLayout;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->n:Landroid/widget/ImageView;

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->n:Landroid/widget/ImageView;

    const v1, 0x7f02034d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->o:Landroid/widget/TextView;

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->o:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 397
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->l:Landroid/widget/LinearLayout;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->p:Landroid/widget/TextView;

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->p:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0206

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 400
    const v0, 0x7f070350

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->m:Landroid/widget/LinearLayout;

    .line 401
    const v0, 0x102000a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/FastScrollableListView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->k:Lcom/sec/widget/FastScrollableListView;

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->k:Lcom/sec/widget/FastScrollableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->k:Lcom/sec/widget/FastScrollableListView;

    new-instance v1, Lcom/sec/chaton/msgbox/aa;

    invoke-direct {v1, p0}, Lcom/sec/chaton/msgbox/aa;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 419
    const v0, 0x7f070354

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    const v1, 0x7f0702da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ImageTextViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v10}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setText(I)V

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v1, 0x7f0203fc

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setImageResource(I)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setGravity(I)V

    .line 428
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {p0, v10}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v8}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setFocusable(Z)V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    new-instance v1, Lcom/sec/chaton/msgbox/ab;

    invoke-direct {v1, p0}, Lcom/sec/chaton/msgbox/ab;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 438
    new-instance v0, Lcom/sec/chaton/msgbox/a;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->g()I

    move-result v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->L:Lcom/sec/common/f/c;

    iget-object v6, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ai:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/msgbox/a;-><init>(Landroid/content/Context;Landroid/widget/ListView;ILandroid/database/Cursor;Lcom/sec/common/f/c;Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    .line 439
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/msgbox/a;->a(Lcom/sec/chaton/msgbox/au;)V

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->b(Ljava/lang/String;)V

    .line 441
    iput-boolean v8, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->O:Z

    .line 442
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v8, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 443
    iput-boolean v8, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->P:Z

    .line 454
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    const v0, 0x7f0702e8

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 464
    invoke-virtual {p0, v8}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(I)V

    .line 467
    const v0, 0x7f0700a7

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    .line 468
    const v0, 0x7f0700a8

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->X:Landroid/view/View;

    .line 469
    const v0, 0x7f070355

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->Y:Landroid/widget/RelativeLayout;

    .line 470
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->W:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->G:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 473
    :cond_0
    return-object v7

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1905
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroyView()V

    .line 1907
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1909
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1911
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->ah:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V

    .line 1913
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/a;->b()V

    .line 1914
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1918
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/a;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 1919
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1920
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1921
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1922
    const-string v0, "The Cursor of Adapter was cosed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1928
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1929
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->E:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1931
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1932
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->F:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 1935
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->L:Lcom/sec/common/f/c;

    if-eqz v0, :cond_3

    .line 1936
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->L:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 1939
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1940
    const-string v0, "[ChatTitleSearch] resetSearchCursor"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1941
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1942
    iput-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->T:Ljava/util/ArrayList;

    .line 1944
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->q()V

    .line 1945
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->af:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_5

    .line 1946
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->af:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 1948
    :cond_5
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 631
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->K:I

    packed-switch v0, :pswitch_data_0

    .line 665
    :goto_0
    return-void

    .line 633
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 634
    iget-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->r:Z

    if-eqz v1, :cond_0

    .line 635
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a()V

    .line 638
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v1, :cond_1

    .line 639
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 641
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->j()V

    .line 643
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/msgbox/ag;

    iget-boolean v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->B:Z

    invoke-interface {v1, v0, v2}, Lcom/sec/chaton/msgbox/ag;->a(Landroid/database/Cursor;Z)V

    goto :goto_0

    .line 646
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 648
    const-string v1, "inbox_no"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 649
    const-string v2, "inbox_session_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 650
    const-string v3, "inbox_chat_type"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v3

    .line 651
    const-string v4, "inbox_server_ip"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 652
    const-string v5, "inbox_server_port"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object v0, p0

    .line 654
    :try_start_0
    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/msgbox/MsgboxFragment;->b(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 655
    :catch_0
    move-exception v0

    .line 657
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 631
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2223
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 2224
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2237
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2232
    :pswitch_0
    const-string v1, "onOptionsItemSelected():msgbox_menu_sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2233
    invoke-direct {p0, v0, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->a(ZZ)V

    goto :goto_0

    .line 2224
    :pswitch_data_0
    .packed-switch 0x7f070580
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 597
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 598
    const-string v0, "[MSGBOX] onPause()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->I:Z

    .line 600
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->j()V

    .line 602
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->R:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 606
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const v2, 0x7f07057f

    .line 2201
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 2208
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2210
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "multidevice_chatlist_sync_last_time"

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2211
    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 2212
    const/4 v0, 0x1

    invoke-interface {p1, v2, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 2219
    :goto_0
    return-void

    .line 2214
    :cond_0
    invoke-interface {p1, v2, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0

    .line 2217
    :cond_1
    invoke-interface {p1, v2, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 2404
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 2405
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2406
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/msgbox/v;

    invoke-direct {v1, p0}, Lcom/sec/chaton/msgbox/v;-><init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2415
    :cond_0
    return-void
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 2

    .prologue
    .line 2420
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 2421
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->U:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2423
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 575
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 576
    iput-boolean v2, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->I:Z

    .line 577
    const-string v0, "[MSGBOX] onResume()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 579
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->d:Ljava/lang/String;

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->L:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 581
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->L:Lcom/sec/common/f/c;

    .line 582
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->L:Lcom/sec/common/f/c;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgbox/a;->a(Lcom/sec/common/f/c;)V

    .line 586
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 587
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxFragment;->q:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/msgbox/a;->a(I)V

    .line 588
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 591
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->l()V

    .line 593
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 611
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onStart()V

    .line 612
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->h()V

    .line 613
    const-string v0, "[MSGBOX] onStart()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 619
    const-string v0, "[MSGBOX] onStop()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onStop()V

    .line 621
    return-void
.end method
