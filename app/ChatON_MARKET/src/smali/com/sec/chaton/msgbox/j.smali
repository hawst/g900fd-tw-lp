.class Lcom/sec/chaton/msgbox/j;
.super Landroid/os/Handler;
.source "MsgboxFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 1433
    iput-object p1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1436
    iget-object v0, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1514
    :cond_0
    :goto_0
    return-void

    .line 1439
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 1440
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 1441
    if-nez v0, :cond_2

    .line 1442
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1443
    const-string v0, "resultEntry is null"

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1447
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1478
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->s(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1479
    iget-object v1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->s(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1482
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->t(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/common/a/d;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->t(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1483
    iget-object v1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->t(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->dismiss()V

    .line 1487
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->u(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/o;->b(Z)V

    .line 1494
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_5

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/a/a/l;->e:Lcom/sec/chaton/a/a/l;

    if-ne v0, v1, :cond_6

    .line 1495
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->u(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->v(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    .line 1506
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0167

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1508
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/msgbox/j;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b002c

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1447
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method
