.class public Lcom/sec/chaton/msgbox/a;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "MsgBoxListAdapter.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static f:I


# instance fields
.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/sec/chaton/e/a/v;

.field private d:Landroid/view/LayoutInflater;

.field private e:I

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/av;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z

.field private m:Ljava/text/DateFormat;

.field private n:Ljava/text/DateFormat;

.field private o:Lcom/sec/chaton/msgbox/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "inbox_participants"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "inbox_title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "inbox_unread_count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "inbox_last_time"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "inbox_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inbox_last_message"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "inbox_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "inbox_title_fixed"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/msgbox/a;->a:[Ljava/lang/String;

    .line 80
    sput v3, Lcom/sec/chaton/msgbox/a;->f:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 146
    sget-object v4, Lcom/sec/chaton/msgbox/a;->a:[Ljava/lang/String;

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 90
    iput-object v5, p0, Lcom/sec/chaton/msgbox/a;->i:Ljava/lang/String;

    .line 103
    iput-boolean v7, p0, Lcom/sec/chaton/msgbox/a;->k:Z

    .line 106
    iput-boolean v7, p0, Lcom/sec/chaton/msgbox/a;->l:Z

    .line 160
    new-instance v0, Lcom/sec/chaton/msgbox/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/b;-><init>(Lcom/sec/chaton/msgbox/a;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/a;->c:Lcom/sec/chaton/e/a/v;

    .line 147
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/a;->d:Landroid/view/LayoutInflater;

    .line 148
    iput p2, p0, Lcom/sec/chaton/msgbox/a;->e:I

    .line 150
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/a;->a()V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListView;ILandroid/database/Cursor;Lcom/sec/common/f/c;Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 120
    sget-object v4, Lcom/sec/chaton/msgbox/a;->a:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 90
    iput-object v5, p0, Lcom/sec/chaton/msgbox/a;->i:Ljava/lang/String;

    .line 103
    iput-boolean v7, p0, Lcom/sec/chaton/msgbox/a;->k:Z

    .line 106
    iput-boolean v7, p0, Lcom/sec/chaton/msgbox/a;->l:Z

    .line 160
    new-instance v0, Lcom/sec/chaton/msgbox/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/b;-><init>(Lcom/sec/chaton/msgbox/a;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/a;->c:Lcom/sec/chaton/e/a/v;

    .line 121
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/a;->d:Landroid/view/LayoutInflater;

    .line 122
    iput p3, p0, Lcom/sec/chaton/msgbox/a;->e:I

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/a;->b:Ljava/util/ArrayList;

    .line 140
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/a;->a()V

    .line 142
    sput v6, Lcom/sec/chaton/msgbox/a;->f:I

    .line 143
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 996
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Lcom/sec/common/util/i;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 997
    iget-object v0, p0, Lcom/sec/chaton/msgbox/a;->n:Ljava/text/DateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1000
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/a;->m:Ljava/text/DateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/msgbox/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/high16 v3, 0x4000000

    .line 233
    const-string v0, "0999"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 234
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    const-string v1, "specialuserid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const-string v1, "speicalusername"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 272
    :goto_0
    return-void

    .line 243
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 244
    const-string v1, "specialuserid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const-string v1, "speicalusername"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 248
    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 256
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 262
    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 265
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 266
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 269
    iget-object v1, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/a;->m:Ljava/text/DateFormat;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/a;->n:Ljava/text/DateFormat;

    .line 158
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 1243
    sput p1, Lcom/sec/chaton/msgbox/a;->f:I

    .line 1244
    return-void
.end method

.method public a(Lcom/sec/chaton/msgbox/au;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public a(Lcom/sec/chaton/msgbox/c;)V
    .locals 0

    .prologue
    .line 1236
    iput-object p1, p0, Lcom/sec/chaton/msgbox/a;->o:Lcom/sec/chaton/msgbox/c;

    .line 1237
    return-void
.end method

.method public a(Lcom/sec/common/f/c;)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/chaton/msgbox/a;->i:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1230
    iput-boolean p1, p0, Lcom/sec/chaton/msgbox/a;->k:Z

    .line 1231
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1174
    iput-object p1, p0, Lcom/sec/chaton/msgbox/a;->j:Ljava/lang/String;

    .line 1175
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 1248
    iput-boolean p1, p0, Lcom/sec/chaton/msgbox/a;->l:Z

    .line 1249
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 24

    .prologue
    .line 277
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    .line 282
    const-string v3, "inbox_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 283
    const-string v3, "inbox_chat_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 284
    const-string v3, "inbox_unread_count"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 286
    const-string v3, "inbox_trunk_unread_count"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 288
    const-string v3, "inbox_last_message"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 289
    const-string v3, "inbox_last_msg_sender"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 290
    const-string v3, "inbox_title"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 292
    const-string v3, "inbox_title_fixed"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 293
    const-string v3, "inbox_participants"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 294
    const-string v3, "inbox_last_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 295
    const-string v3, "profile_url"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 302
    const-string v3, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 304
    const-string v3, "relation_buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 305
    const/4 v3, 0x0

    .line 306
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3a

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3a

    .line 310
    :goto_0
    const-string v3, "Y"

    const-string v9, "inbox_enable_noti"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    .line 312
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/sec/chaton/msgbox/d;

    .line 341
    if-eqz v4, :cond_24

    .line 350
    const-string v3, ";"

    invoke-virtual {v4, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 354
    const/4 v3, 0x0

    .line 355
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_0

    .line 356
    const/4 v3, 0x2

    aget-object v3, v20, v3

    .line 362
    :cond_0
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_2

    .line 363
    const/16 v21, 0x0

    aget-object v21, v20, v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 364
    const/16 v22, 0x1

    aget-object v22, v20, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v22

    .line 366
    const-string v23, ""

    .line 367
    const-string v23, ""

    .line 369
    const/16 v23, 0x1

    move/from16 v0, v21

    move/from16 v1, v23

    if-eq v0, v1, :cond_1

    const/16 v23, 0x2

    move/from16 v0, v21

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    :cond_1
    sget-object v21, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_2

    .line 370
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v21, "chaton_id"

    const-string v23, ""

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 371
    const/4 v4, 0x0

    .line 372
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_39

    .line 373
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 374
    const-string v4, "mixed"

    const/4 v7, 0x0

    aget-object v3, v3, v7

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 376
    :goto_1
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;[Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 538
    :cond_2
    :goto_2
    if-nez v4, :cond_1e

    .line 539
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->b:Landroid/widget/TextView;

    sget-object v7, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 551
    :goto_3
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    if-eqz v3, :cond_4

    .line 552
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_20

    .line 553
    :cond_3
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 566
    :cond_4
    :goto_4
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    if-eqz v3, :cond_6

    .line 567
    if-eqz v12, :cond_5

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_22

    .line 568
    :cond_5
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 615
    :cond_6
    :goto_5
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 616
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->f:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 617
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->f:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 618
    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    if-ne v10, v3, :cond_2e

    .line 621
    const-string v3, "Y"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2c

    if-gtz v15, :cond_2c

    .line 622
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0b0155

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v7, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 628
    :goto_6
    const/16 v3, 0xa

    if-lt v15, v3, :cond_2d

    .line 629
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x5

    const/4 v12, 0x0

    invoke-virtual {v3, v4, v7, v11, v12}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 634
    :goto_7
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->f:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v7, v15, 0x1

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 672
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/a;->j:Ljava/lang/String;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/a;->j:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 674
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/util/at;->c(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_32

    .line 675
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/util/at;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/msgbox/a;->g:Ljava/util/ArrayList;

    .line 676
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/a;->g:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/msgbox/a;->j:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/at;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 677
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/chaton/msgbox/a;->h:Ljava/util/ArrayList;

    .line 678
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 679
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/chaton/util/at;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/chaton/msgbox/a;->g:Ljava/util/ArrayList;

    .line 680
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/msgbox/a;->h:Ljava/util/ArrayList;

    iget-object v7, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v11, ""

    invoke-virtual {v7, v3, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 682
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->b()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_7

    .line 683
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Landroid/text/Spannable;

    .line 684
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v7

    const v11, 0x7f08001a

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v4, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->b()I

    move-result v7

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->d()I

    move-result v11

    add-int/2addr v7, v11

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->c()I

    move-result v11

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->d()I

    move-result v12

    add-int/2addr v11, v12

    add-int/lit8 v11, v11, 0x1

    const/16 v12, 0x21

    invoke-interface {v3, v4, v7, v11, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 685
    iget-object v4, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 686
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->e()V

    .line 780
    :cond_7
    :goto_9
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->e:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 781
    const-wide/16 v3, 0x0

    cmp-long v3, v16, v3

    if-eqz v3, :cond_33

    .line 783
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->e:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/msgbox/a;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 796
    :goto_a
    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    if-ne v10, v3, :cond_34

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_34

    .line 797
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/chaton/msgbox/d;->g:Landroid/widget/ImageView;

    move-object/from16 v0, v18

    invoke-virtual {v3, v4, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 937
    :goto_b
    if-eqz v19, :cond_35

    .line 938
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->i:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 968
    :goto_c
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/chaton/msgbox/a;->l:Z

    if-nez v3, :cond_8

    .line 970
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/a;->i:Ljava/lang/String;

    if-eqz v3, :cond_37

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/a;->i:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_37

    .line 971
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/chaton/msgbox/a;->k:Z

    if-eqz v3, :cond_36

    .line 972
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->l:Landroid/view/View;

    const v4, 0x7f020413

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 976
    :goto_d
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->l:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 977
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->m:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 990
    :cond_8
    :goto_e
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 991
    return-void

    .line 378
    :cond_9
    invoke-static {v10}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    sget-object v7, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v4, v7, :cond_14

    .line 381
    move-object/from16 v0, v20

    array-length v4, v0

    const/4 v7, 0x4

    if-ge v4, v7, :cond_b

    .line 382
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0b00b4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 387
    :goto_f
    sget-object v7, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    move-object/from16 v0, v22

    if-ne v0, v7, :cond_d

    .line 390
    move-object/from16 v0, v20

    array-length v3, v0

    const/4 v7, 0x3

    if-ge v3, v7, :cond_c

    .line 391
    const-string v3, ""

    .line 403
    :cond_a
    :goto_10
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ": "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_2

    .line 384
    :cond_b
    const/4 v4, 0x3

    aget-object v4, v20, v4

    invoke-static {v4}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_f

    .line 393
    :cond_c
    const/4 v3, 0x2

    aget-object v3, v20, v3

    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 396
    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 397
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 398
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_10

    .line 405
    :cond_d
    sget-object v7, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    move-object/from16 v0, v22

    if-ne v0, v7, :cond_f

    .line 408
    move-object/from16 v0, v20

    array-length v3, v0

    const/4 v7, 0x3

    if-ge v3, v7, :cond_e

    .line 409
    const-string v3, ""

    .line 425
    :goto_11
    invoke-static {v3}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 426
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ": "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 427
    goto/16 :goto_2

    .line 411
    :cond_e
    const/4 v3, 0x2

    aget-object v3, v20, v3

    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_11

    .line 427
    :cond_f
    sget-object v7, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    move-object/from16 v0, v22

    if-ne v0, v7, :cond_12

    .line 428
    const-string v3, ""

    .line 430
    move-object/from16 v0, v20

    array-length v7, v0

    const/16 v21, 0x3

    move/from16 v0, v21

    if-ge v7, v0, :cond_10

    .line 431
    const-string v7, ""

    .line 436
    :goto_12
    :try_start_0
    new-instance v20, Lorg/json/JSONObject;

    move-object/from16 v0, v20

    invoke-direct {v0, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 437
    const-string v7, "push_message"

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 438
    const-string v20, "content_type"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 439
    const-string v20, "result"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 440
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    const v20, 0x7f0b022f

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 447
    :goto_13
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ":"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 449
    goto/16 :goto_2

    .line 433
    :cond_10
    const/4 v7, 0x2

    aget-object v7, v20, v7

    invoke-static {v7}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_12

    .line 442
    :cond_11
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    const v20, 0x7f0b022e

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_13

    .line 444
    :catch_0
    move-exception v7

    .line 445
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v7, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_13

    .line 450
    :cond_12
    const/4 v7, 0x0

    .line 451
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_13

    .line 452
    const-string v7, "\n"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 453
    const-string v20, "mixed"

    const/16 v21, 0x0

    aget-object v7, v7, v21

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 455
    :cond_13
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v20, ": "

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-static {v0, v3, v7}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 456
    goto/16 :goto_2

    .line 458
    :cond_14
    sget-object v4, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    move-object/from16 v0, v22

    if-ne v0, v4, :cond_17

    .line 461
    move-object/from16 v0, v20

    array-length v3, v0

    const/4 v4, 0x3

    if-ge v3, v4, :cond_16

    .line 462
    const-string v3, ""

    :cond_15
    :goto_14
    move-object v4, v3

    .line 474
    goto/16 :goto_2

    .line 464
    :cond_16
    const/4 v3, 0x2

    aget-object v3, v20, v3

    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 467
    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 468
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 469
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_14

    .line 475
    :cond_17
    sget-object v4, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    move-object/from16 v0, v22

    if-ne v0, v4, :cond_19

    .line 478
    move-object/from16 v0, v20

    array-length v3, v0

    const/4 v4, 0x3

    if-ge v3, v4, :cond_18

    .line 479
    const-string v3, ""

    .line 495
    :goto_15
    invoke-static {v3}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 497
    goto/16 :goto_2

    .line 481
    :cond_18
    const/4 v3, 0x2

    aget-object v3, v20, v3

    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_15

    .line 497
    :cond_19
    sget-object v4, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    move-object/from16 v0, v22

    if-ne v0, v4, :cond_1c

    .line 498
    const-string v3, ""

    .line 500
    move-object/from16 v0, v20

    array-length v4, v0

    const/4 v7, 0x3

    if-ge v4, v7, :cond_1a

    .line 501
    const-string v4, ""

    .line 506
    :goto_16
    :try_start_2
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 507
    const-string v4, "push_message"

    invoke-virtual {v7, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 508
    const-string v7, "content_type"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 509
    const-string v7, "result"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 510
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    const v7, 0x7f0b022f

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v3

    :goto_17
    move-object v4, v3

    .line 518
    goto/16 :goto_2

    .line 503
    :cond_1a
    const/4 v4, 0x2

    aget-object v4, v20, v4

    invoke-static {v4}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_16

    .line 512
    :cond_1b
    :try_start_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    const v7, 0x7f0b022e

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v3

    goto :goto_17

    .line 514
    :catch_1
    move-exception v4

    .line 515
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_17

    .line 519
    :cond_1c
    const/4 v4, 0x0

    .line 520
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1d

    .line 521
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 522
    const-string v7, "mixed"

    const/16 v20, 0x0

    aget-object v4, v4, v20

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 524
    :cond_1d
    move-object/from16 v0, v22

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_2

    .line 541
    :cond_1e
    const v3, 0x3f99999a    # 1.2f

    .line 542
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    .line 543
    iget v7, v7, Landroid/util/DisplayMetrics;->density:F

    const/high16 v20, 0x40000000    # 2.0f

    cmpl-float v7, v7, v20

    if-ltz v7, :cond_1f

    .line 544
    const/high16 v3, 0x3f800000    # 1.0f

    .line 546
    :cond_1f
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    iget-object v0, v9, Lcom/sec/chaton/msgbox/d;->b:Landroid/widget/TextView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getLineHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v3, v3, v20

    float-to-int v3, v3

    invoke-static {v7, v4, v3}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 548
    iget-object v4, v9, Lcom/sec/chaton/msgbox/d;->b:Landroid/widget/TextView;

    sget-object v7, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v4, v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto/16 :goto_3

    .line 555
    :cond_20
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 556
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_21

    .line 558
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    const v4, 0x7f0b005a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    .line 561
    :cond_21
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 570
    :cond_22
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 571
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_23

    .line 573
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    const v4, 0x7f0b005a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    .line 576
    :cond_23
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 581
    :cond_24
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->b:Landroid/widget/TextView;

    sget-object v7, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 583
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    if-eqz v3, :cond_26

    .line 584
    if-eqz v11, :cond_25

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_25

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_28

    .line 585
    :cond_25
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 597
    :cond_26
    :goto_18
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    if-eqz v3, :cond_6

    .line 598
    if-eqz v12, :cond_27

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_27

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2a

    .line 600
    :cond_27
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 587
    :cond_28
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_29

    .line 590
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    const v4, 0x7f0b005a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_18

    .line 593
    :cond_29
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_18

    .line 603
    :cond_2a
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 604
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_2b

    .line 606
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    const v4, 0x7f0b005a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    .line 610
    :cond_2b
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 624
    :cond_2c
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v13, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto/16 :goto_6

    .line 631
    :cond_2d
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v3, v4, v7, v11, v12}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_7

    .line 640
    :cond_2e
    invoke-static {v10}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 642
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-static {v14, v13, v15}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    sget-object v7, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 644
    const/16 v3, 0xa

    if-lt v15, v3, :cond_2f

    .line 645
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x5

    const/4 v12, 0x0

    invoke-virtual {v3, v4, v7, v11, v12}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 649
    :goto_19
    if-lez v15, :cond_30

    .line 651
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->f:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 647
    :cond_2f
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v3, v4, v7, v11, v12}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_19

    .line 654
    :cond_30
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->f:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 663
    :cond_31
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v13, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto/16 :goto_8

    .line 692
    :cond_32
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/msgbox/a;->j:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 693
    if-ltz v4, :cond_7

    .line 694
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/msgbox/a;->j:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/chaton/msgbox/a;->j:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v3

    .line 695
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Landroid/text/Spannable;

    .line 696
    new-instance v11, Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f08001a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-direct {v11, v12}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v12, 0x21

    invoke-interface {v3, v11, v4, v7, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 697
    iget-object v4, v9, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 786
    :cond_33
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->e:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 800
    :cond_34
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/msgbox/a;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/chaton/msgbox/d;->g:Landroid/widget/ImageView;

    invoke-static {v10}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v7

    invoke-virtual/range {v3 .. v8}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 940
    :cond_35
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->i:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_c

    .line 974
    :cond_36
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->l:Landroid/view/View;

    const v4, 0x7f020412

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_d

    .line 980
    :cond_37
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/chaton/msgbox/a;->k:Z

    if-eqz v3, :cond_38

    .line 981
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->m:Landroid/view/View;

    const v4, 0x7f020415

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 985
    :goto_1a
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->l:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 986
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->m:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_e

    .line 983
    :cond_38
    iget-object v3, v9, Lcom/sec/chaton/msgbox/d;->m:Landroid/view/View;

    const v4, 0x7f020414

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1a

    :cond_39
    move v3, v4

    goto/16 :goto_1

    :cond_3a
    move-object v6, v3

    goto/16 :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1082
    .line 1085
    iget-object v0, p0, Lcom/sec/chaton/msgbox/a;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/msgbox/a;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1087
    sget v0, Lcom/sec/chaton/msgbox/a;->f:I

    if-eqz v0, :cond_0

    .line 1090
    instance-of v0, v1, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1091
    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    sget v2, Lcom/sec/chaton/msgbox/a;->f:I

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 1096
    :cond_0
    new-instance v2, Lcom/sec/chaton/msgbox/d;

    invoke-direct {v2}, Lcom/sec/chaton/msgbox/d;-><init>()V

    .line 1098
    const v0, 0x7f0704b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->j:Landroid/widget/FrameLayout;

    .line 1099
    const v0, 0x7f0704b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->k:Landroid/widget/FrameLayout;

    .line 1100
    iget-object v0, p0, Lcom/sec/chaton/msgbox/a;->d:Landroid/view/LayoutInflater;

    const v3, 0x7f030134

    iget-object v4, v2, Lcom/sec/chaton/msgbox/d;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1101
    iget-object v3, p0, Lcom/sec/chaton/msgbox/a;->d:Landroid/view/LayoutInflater;

    const v4, 0x7f030133

    iget-object v5, v2, Lcom/sec/chaton/msgbox/d;->k:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1102
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1103
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1105
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->a:Landroid/widget/TextView;

    .line 1106
    const v0, 0x7f07014d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->b:Landroid/widget/TextView;

    .line 1107
    const v0, 0x7f0704ec

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->c:Landroid/widget/TextView;

    .line 1108
    const v0, 0x7f0704eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->d:Landroid/widget/TextView;

    .line 1109
    const v0, 0x7f07014b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->g:Landroid/widget/ImageView;

    .line 1114
    const v0, 0x7f0704e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->e:Landroid/widget/TextView;

    .line 1115
    const v0, 0x7f0704e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->f:Landroid/widget/TextView;

    .line 1116
    iput-object v1, v2, Lcom/sec/chaton/msgbox/d;->h:Landroid/view/View;

    .line 1117
    const v0, 0x7f0704e7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->i:Landroid/widget/ImageView;

    .line 1125
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1126
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    .line 1127
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 1128
    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    .line 1129
    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 1130
    const v0, 0x7f070016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->n:Landroid/widget/TableLayout;

    .line 1131
    iget-object v0, v2, Lcom/sec/chaton/msgbox/d;->n:Landroid/widget/TableLayout;

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/widget/TableLayout;->setPadding(IIII)V

    .line 1132
    iget-object v0, v2, Lcom/sec/chaton/msgbox/d;->h:Landroid/view/View;

    const v3, 0x7f02040e

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1133
    const v0, 0x7f0704b5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->l:Landroid/view/View;

    .line 1134
    const v0, 0x7f0702e4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/msgbox/d;->m:Landroid/view/View;

    .line 1138
    :cond_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1139
    return-object v1
.end method
