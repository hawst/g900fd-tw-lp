.class Lcom/sec/chaton/msgbox/w;
.super Ljava/lang/Object;
.source "MsgboxFragment.java"

# interfaces
.implements Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 2440
    iput-object p1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/view/View;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2443
    if-eqz p3, :cond_0

    .line 2444
    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2445
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 2446
    const-string v1, "buddy_no"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2448
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "0999"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2452
    if-nez p2, :cond_1

    .line 2453
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->c(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    .line 2475
    :cond_0
    :goto_0
    return-void

    .line 2454
    :cond_1
    if-ne p2, v3, :cond_2

    .line 2455
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->d(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto :goto_0

    .line 2456
    :cond_2
    if-ne p2, v4, :cond_0

    .line 2457
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->e(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto :goto_0

    .line 2464
    :cond_3
    if-nez p2, :cond_4

    .line 2465
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->f(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto :goto_0

    .line 2466
    :cond_4
    if-ne p2, v3, :cond_5

    .line 2467
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->c(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto :goto_0

    .line 2468
    :cond_5
    if-ne p2, v4, :cond_6

    .line 2469
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->d(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto :goto_0

    .line 2470
    :cond_6
    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    .line 2471
    iget-object v1, p0, Lcom/sec/chaton/msgbox/w;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->e(Lcom/sec/chaton/msgbox/MsgboxFragment;Landroid/database/Cursor;)V

    goto :goto_0
.end method
