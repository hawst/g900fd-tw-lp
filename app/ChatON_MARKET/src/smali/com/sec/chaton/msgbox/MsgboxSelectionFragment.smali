.class public Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;
.super Landroid/support/v4/app/ListFragment;
.source "MsgboxSelectionFragment.java"

# interfaces
.implements Lcom/sec/chaton/msgbox/au;
.implements Lcom/sec/chaton/msgbox/c;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/sec/chaton/d/o;

.field private C:Landroid/app/ProgressDialog;

.field private D:Lcom/sec/widget/DropPanelMenu;

.field private E:Z

.field private F:Z

.field private G:Landroid/view/Menu;

.field private H:[J

.field private I:I

.field private J:Lcom/sec/common/f/c;

.field private K:Lcom/sec/chaton/msgbox/as;

.field private L:Lcom/sec/chaton/w;

.field private M:Landroid/os/Handler;

.field b:Landroid/os/Bundle;

.field c:I

.field d:Landroid/database/ContentObserver;

.field e:Lcom/sec/chaton/e/a/v;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/LinearLayout;

.field private k:Lcom/sec/widget/FastScrollableListView;

.field private l:Lcom/sec/chaton/msgbox/a;

.field private m:Landroid/widget/CheckedTextView;

.field private n:Z

.field private o:Z

.field private p:Lcom/sec/chaton/e/a/u;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Lcom/sec/chaton/e/r;

.field private t:Ljava/lang/String;

.field private u:I

.field private v:I

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const-class v0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 132
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n:Z

    .line 154
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->z:Ljava/lang/String;

    .line 165
    iput-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->E:Z

    .line 171
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->I:I

    .line 177
    sget-object v0, Lcom/sec/chaton/w;->e:Lcom/sec/chaton/w;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->L:Lcom/sec/chaton/w;

    .line 180
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->c:I

    .line 479
    new-instance v0, Lcom/sec/chaton/msgbox/ao;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/msgbox/ao;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->d:Landroid/database/ContentObserver;

    .line 497
    new-instance v0, Lcom/sec/chaton/msgbox/ap;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/ap;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->e:Lcom/sec/chaton/e/a/v;

    .line 587
    new-instance v0, Lcom/sec/chaton/msgbox/aq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/msgbox/aq;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->M:Landroid/os/Handler;

    .line 1456
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1119
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1120
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object p1, v0

    .line 1125
    :cond_0
    return-object p1
.end method

.method private a(Landroid/widget/ListView;)V
    .locals 3

    .prologue
    .line 1334
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1335
    const/4 v0, 0x0

    .line 1336
    if-eqz p1, :cond_0

    .line 1337
    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    .line 1341
    :cond_0
    iget v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->c:I

    if-lez v1, :cond_2

    .line 1342
    iget v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->c:I

    invoke-virtual {p0, v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1346
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1347
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/forward/ChatForwardActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1349
    :cond_1
    return-void

    .line 1344
    :cond_2
    const v1, 0x7f0b013a

    invoke-virtual {p0, v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 769
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 770
    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/e/a/n;->l(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 771
    const-string v9, ""

    .line 772
    const-string v7, ""

    .line 773
    sget-object v5, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    .line 774
    const-string v3, ""

    .line 775
    const/4 v1, 0x0

    .line 778
    if-eqz v11, :cond_4

    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 780
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 781
    const-string v2, "inbox_no"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 782
    const-string v2, "inbox_session_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 783
    const-string v2, "inbox_chat_type"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v6

    .line 784
    const-string v2, "inbox_server_ip"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 785
    const-string v2, "inbox_server_port"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 786
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    sget-object v12, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v6, v12, :cond_3

    move v1, v2

    move-object v3, v6

    move-object v5, v10

    move-object v2, v4

    move-object v4, v8

    .line 797
    :goto_0
    if-eqz v11, :cond_1

    .line 798
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 801
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 802
    const/4 v6, 0x0

    invoke-direct {p0, p2, v6}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Ljava/util/ArrayList;Z)V

    .line 804
    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 805
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Ljava/util/ArrayList;Z)V

    .line 822
    :goto_1
    return-void

    .line 794
    :cond_3
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move-object v2, v3

    move-object v4, v7

    move-object v3, v5

    move-object v5, v9

    goto :goto_0

    .line 797
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_5

    .line 798
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 797
    :cond_5
    throw v1

    .line 808
    :cond_6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 809
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    .line 810
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/util/bk;->b()I

    move-result v1

    .line 812
    :cond_7
    invoke-static {v5, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->B:Lcom/sec/chaton/d/o;

    .line 813
    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->B:Lcom/sec/chaton/d/o;

    iget-object v4, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->M:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 814
    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->B:Lcom/sec/chaton/d/o;

    invoke-virtual {v3, v5, v2, v1}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 815
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->B:Lcom/sec/chaton/d/o;

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/o;->c(J)V

    .line 817
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_8

    .line 818
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 820
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->B:Lcom/sec/chaton/d/o;

    move-object/from16 v0, p4

    invoke-virtual {v1, p1, v0}, Lcom/sec/chaton/d/o;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)J

    .line 821
    invoke-static {}, Lcom/sec/chaton/j/e;->a()Lcom/sec/chaton/j/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/e;->b()V

    goto :goto_1
.end method

.method private a(Ljava/util/ArrayList;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 825
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 827
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 828
    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "inbox_no=?"

    new-array v5, v8, [Ljava/lang/String;

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 829
    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "message_inbox_no=?"

    new-array v5, v8, [Ljava/lang/String;

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 830
    sget-object v3, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "participants_inbox_no=?"

    new-array v5, v8, [Ljava/lang/String;

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 832
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/chaton/j/c/a;->a(Ljava/lang/String;)Z

    .line 833
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/chaton/j/c/g;->a(Ljava/lang/String;)Z

    .line 835
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_chat_profile.png_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 836
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 838
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 840
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v3

    .line 841
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 844
    :cond_1
    :try_start_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 845
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 856
    :cond_2
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 858
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/e/b/m;

    const/4 v2, 0x0

    sget v3, Lcom/sec/chaton/chat/notification/a;->d:I

    invoke-direct {v1, v2, v3, v7}, Lcom/sec/chaton/e/b/m;-><init>(Lcom/sec/chaton/e/b/d;IZ)V

    invoke-static {v0, v9, v1}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 860
    if-eqz p2, :cond_4

    .line 861
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k()V

    .line 862
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->clearChoices()V

    .line 864
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 865
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 868
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n()V

    .line 870
    :cond_4
    return-void

    .line 847
    :catch_0
    move-exception v0

    .line 848
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_2

    .line 849
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 851
    :catch_1
    move-exception v0

    .line 852
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_2

    .line 853
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->o:Z

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;[J)[J
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->H:[J

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/widget/FastScrollableListView;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->G:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private f()I
    .locals 1

    .prologue
    .line 217
    const v0, 0x7f030126

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->E:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->j:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 377
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 378
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/at;

    invoke-interface {v0}, Lcom/sec/chaton/msgbox/at;->c()V

    .line 384
    :cond_1
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 693
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_chat_profile.png_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 694
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 702
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    .line 704
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    .line 707
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->I:I

    return v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1007
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1009
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1010
    const-string v1, "closeMultipleChatRoom()"

    const-class v2, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    :cond_0
    const/4 v1, -0x3

    if-eq v1, v0, :cond_1

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 1014
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1029
    :goto_0
    return-void

    .line 1024
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->o:Z

    if-nez v0, :cond_3

    .line 1025
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b()V

    goto :goto_0

    .line 1027
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l()V

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/msgbox/a;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    return-object v0
.end method

.method private j()V
    .locals 15

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v2

    .line 1054
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1055
    const-string v0, ""

    .line 1057
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1058
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1059
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1061
    sget-object v0, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    .line 1065
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1067
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1072
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1073
    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1072
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1076
    :cond_1
    const-string v0, ""

    .line 1077
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1078
    invoke-static {}, Lcom/sec/chaton/a/ag;->newBuilder()Lcom/sec/chaton/a/ah;

    move-result-object v8

    .line 1079
    const-string v9, "inbox_chat_type"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v9

    .line 1080
    const-string v10, "inbox_session_id"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1081
    const-string v11, "lasst_session_merge_time"

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 1082
    const-string v13, "inbox_server_ip"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    iput-object v13, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->t:Ljava/lang/String;

    .line 1083
    const-string v13, "inbox_server_port"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    iput v13, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->u:I

    .line 1085
    sget-boolean v13, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v13, :cond_2

    .line 1086
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "endChatMultiple() sessionID["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    :cond_2
    const-string v13, "inbox_no"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1089
    invoke-interface {v5, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    sget-object v13, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v9, v13, :cond_3

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1092
    :cond_3
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1095
    :cond_4
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1097
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-static {v13, v0}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/sec/chaton/a/ah;->a(Ljava/lang/Iterable;)Lcom/sec/chaton/a/ah;

    .line 1099
    invoke-virtual {v9}, Lcom/sec/chaton/e/r;->a()I

    move-result v9

    invoke-static {v9}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/chaton/a/ah;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/ah;

    .line 1100
    invoke-virtual {v8, v10}, Lcom/sec/chaton/a/ah;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ah;

    .line 1101
    invoke-interface {v5, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1103
    invoke-virtual {v8, v11, v12}, Lcom/sec/chaton/a/ah;->a(J)Lcom/sec/chaton/a/ah;

    .line 1104
    invoke-virtual {v8}, Lcom/sec/chaton/a/ah;->d()Lcom/sec/chaton/a/ag;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105
    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "\'"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ","

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1107
    :cond_5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_6

    .line 1108
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1110
    :cond_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 1111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "endChatMultiple() sessionIDs["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1113
    :cond_7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v6, v0, v4}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1114
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1132
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)[J
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->H:[J

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1134
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1135
    const-string v0, "deleteMultipleCheckAll()"

    const-class v1, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 1140
    const v1, 0x7f0b02af

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 1142
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemIds()[J

    move-result-object v1

    array-length v1, v1

    if-le v1, v2, :cond_1

    .line 1143
    const v1, 0x7f0b0034

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 1149
    :goto_0
    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/msgbox/aj;

    invoke-direct {v3, p0}, Lcom/sec/chaton/msgbox/aj;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/msgbox/ar;

    invoke-direct {v3, p0}, Lcom/sec/chaton/msgbox/ar;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1163
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1167
    return-void

    .line 1146
    :cond_1
    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    goto :goto_0
.end method

.method static synthetic m(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->s:Lcom/sec/chaton/e/r;

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->K:Lcom/sec/chaton/msgbox/as;

    if-eqz v0, :cond_0

    .line 1403
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1404
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->K:Lcom/sec/chaton/msgbox/as;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/chaton/msgbox/as;->a(Z)V

    .line 1410
    :cond_0
    :goto_0
    return-void

    .line 1406
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->K:Lcom/sec/chaton/msgbox/as;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/chaton/msgbox/as;->a(Z)V

    goto :goto_0
.end method

.method static synthetic n(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->r:Ljava/lang/String;

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1444
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;

    if-eqz v0, :cond_0

    .line 1445
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;->e()V

    .line 1454
    :goto_0
    return-void

    .line 1446
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    if-eqz v0, :cond_1

    .line 1447
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/forward/ChatForwardActivity;->d()V

    goto :goto_0

    .line 1449
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_2

    .line 1450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ERROR!! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is attached on wrong activity."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic o(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Lcom/sec/chaton/d/o;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->B:Lcom/sec/chaton/d/o;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k()V

    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->M:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->h()V

    return-void
.end method

.method static synthetic t(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n()V

    return-void
.end method

.method static synthetic u(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->j()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 425
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 426
    const-string v0, "inbox_is_new"

    const-string v1, "N"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x1

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 428
    return-void
.end method

.method public a(Lcom/sec/chaton/msgbox/as;)V
    .locals 0

    .prologue
    .line 1416
    iput-object p1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->K:Lcom/sec/chaton/msgbox/as;

    .line 1417
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1172
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deleteMultiDialog() isShowPopup[removed], mMode["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 1181
    const v0, 0x7f0b000e

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 1183
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1184
    const v1, 0x7f0300bd

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1185
    const v0, 0x7f070357

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1186
    iget v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->I:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    .line 1187
    if-eqz v0, :cond_1

    .line 1188
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1190
    new-instance v1, Lcom/sec/chaton/msgbox/ak;

    invoke-direct {v1, p0}, Lcom/sec/chaton/msgbox/ak;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199
    :cond_1
    const v1, 0x7f070356

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1202
    iget-object v4, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v4}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemIds()[J

    move-result-object v4

    array-length v4, v4

    if-le v4, v5, :cond_2

    .line 1203
    const v4, 0x7f0b0034

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1208
    :goto_0
    invoke-virtual {v2, v5}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    .line 1209
    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    .line 1211
    invoke-virtual {v2, v5}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v3, 0x7f0b0037

    new-instance v4, Lcom/sec/chaton/msgbox/am;

    invoke-direct {v4, p0, v0}, Lcom/sec/chaton/msgbox/am;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/msgbox/al;

    invoke-direct {v3, p0}, Lcom/sec/chaton/msgbox/al;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1232
    invoke-virtual {v2}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1234
    return-void

    .line 1206
    :cond_2
    const v4, 0x7f0b0033

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    if-eqz v0, :cond_0

    .line 1367
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/widget/FastScrollableListView;->setItemChecked(IZ)V

    .line 1369
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m()V

    .line 1371
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 1373
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Landroid/widget/ListView;)V

    .line 1374
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1392
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 1393
    iget-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n:Z

    if-eqz v0, :cond_0

    .line 1394
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a()V

    .line 1397
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Landroid/widget/ListView;)V

    .line 1398
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 1420
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemPosition()I

    move-result v0

    .line 1421
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1422
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doFarwardAction updated checkedItemPosition["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    :cond_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 1426
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1427
    iget-boolean v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n:Z

    if-eqz v1, :cond_1

    .line 1428
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a()V

    .line 1430
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1431
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/chaton/forward/ChatForwardActivity;

    if-eqz v1, :cond_2

    .line 1432
    const-string v1, "isForwardSelected"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1433
    const-string v1, "content_type"

    iget v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->v:I

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1434
    const-string v1, "download_uri"

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->w:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    const-string v1, "sub_content"

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->x:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    const-string v1, "forward_sender_name"

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->A:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1437
    const-string v1, "is_forward_mode"

    iget-boolean v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->F:Z

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1439
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/msgbox/at;

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->L:Lcom/sec/chaton/w;

    invoke-interface {v1, v0, v2, v3}, Lcom/sec/chaton/msgbox/at;->a(Landroid/database/Cursor;Landroid/os/Bundle;Lcom/sec/chaton/w;)V

    .line 1441
    :cond_3
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 299
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 300
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 301
    iput-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b:Landroid/os/Bundle;

    .line 302
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 303
    const-string v0, "content_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    const-string v0, "content_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->v:I

    .line 306
    :cond_0
    const-string v0, "download_uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    const-string v0, "download_uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->w:Ljava/lang/String;

    .line 309
    :cond_1
    const-string v0, "sub_content"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 310
    const-string v0, "sub_content"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->x:Ljava/lang/String;

    .line 312
    :cond_2
    const-string v0, "inboxNO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 313
    const-string v0, "inboxNO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->y:Ljava/lang/String;

    .line 315
    :cond_3
    const-string v0, "forward_sender_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 316
    const-string v0, "forward_sender_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->A:Ljava/lang/String;

    .line 318
    :cond_4
    const-string v0, "mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 319
    const-string v0, "mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->I:I

    .line 321
    :cond_5
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 322
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/w;

    check-cast v0, Lcom/sec/chaton/w;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->L:Lcom/sec/chaton/w;

    .line 324
    :cond_6
    const-string v0, "is_forward_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 325
    const-string v0, "is_forward_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->F:Z

    .line 327
    :cond_7
    const-string v0, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 328
    const-string v0, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->c:I

    .line 332
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 334
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    if-eqz v0, :cond_d

    .line 335
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/msgbox/a;->a(I)V

    .line 338
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/msgbox/a;->b(Z)V

    .line 343
    :cond_9
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Landroid/widget/ListView;)V

    .line 350
    :goto_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/msgbox/a;->b(Z)V

    .line 356
    :cond_a
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 357
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 362
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->C:Landroid/app/ProgressDialog;

    .line 365
    const/4 v0, 0x3

    iget v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->I:I

    if-ne v0, v1, :cond_e

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m:Landroid/widget/CheckedTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 371
    :cond_b
    :goto_1
    if-eqz p1, :cond_c

    .line 372
    const-string v0, "checkArray"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->H:[J

    .line 374
    :cond_c
    return-void

    .line 345
    :cond_d
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/msgbox/a;->a(I)V

    goto :goto_0

    .line 367
    :cond_e
    const/4 v0, 0x4

    iget v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->I:I

    if-ne v0, v1, :cond_b

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m:Landroid/widget/CheckedTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 953
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 954
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 958
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 286
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 295
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 919
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 942
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 196
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 197
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->b:Landroid/os/Bundle;

    .line 198
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->e:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p:Lcom/sec/chaton/e/a/u;

    .line 204
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 1293
    iput-object p1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->G:Landroid/view/Menu;

    .line 1294
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1295
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 222
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->J:Lcom/sec/common/f/c;

    .line 224
    const v0, 0x7f0300be

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 226
    const v0, 0x7f070353

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i:Landroid/widget/LinearLayout;

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i:Landroid/widget/LinearLayout;

    const v2, 0x7f07014b

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->f:Landroid/widget/ImageView;

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->f:Landroid/widget/ImageView;

    const v2, 0x7f02034d

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i:Landroid/widget/LinearLayout;

    const v2, 0x7f07014c

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->g:Landroid/widget/TextView;

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b021d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i:Landroid/widget/LinearLayout;

    const v2, 0x7f07014d

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->h:Landroid/widget/TextView;

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0206

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 234
    const v0, 0x7f070350

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->j:Landroid/widget/LinearLayout;

    .line 235
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/FastScrollableListView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    .line 240
    new-instance v0, Lcom/sec/chaton/msgbox/a;

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->f()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/msgbox/a;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/msgbox/a;->a(Lcom/sec/chaton/msgbox/au;)V

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    iget-object v2, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/msgbox/a;->a(Lcom/sec/chaton/msgbox/c;)V

    .line 244
    const v0, 0x7f070358

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m:Landroid/widget/CheckedTextView;

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m:Landroid/widget/CheckedTextView;

    new-instance v2, Lcom/sec/chaton/msgbox/ai;

    invoke-direct {v2, p0}, Lcom/sec/chaton/msgbox/ai;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    new-instance v2, Lcom/sec/chaton/msgbox/an;

    invoke-direct {v2, p0}, Lcom/sec/chaton/msgbox/an;-><init>(Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;)V

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 279
    new-instance v0, Lcom/sec/widget/DropPanelMenu;

    invoke-direct {v0, p0}, Lcom/sec/widget/DropPanelMenu;-><init>(Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->D:Lcom/sec/widget/DropPanelMenu;

    .line 281
    return-object v1
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 967
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroyView()V

    .line 969
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    if-eqz v0, :cond_1

    .line 972
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/a;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 973
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 974
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 975
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 976
    const-string v0, "The Cursor of Adapter was closed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/msgbox/a;->a(Lcom/sec/chaton/msgbox/au;)V

    .line 982
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->l:Lcom/sec/chaton/msgbox/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/msgbox/a;->a(Lcom/sec/chaton/msgbox/c;)V

    .line 985
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 987
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->J:Lcom/sec/common/f/c;

    if-eqz v0, :cond_2

    .line 988
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->J:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 990
    :cond_2
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    const v1, 0x7f07057e

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1308
    invoke-super/range {p0 .. p5}, Landroid/support/v4/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 1310
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    if-eqz v0, :cond_1

    .line 1311
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->K:Lcom/sec/chaton/msgbox/as;

    if-eqz v0, :cond_0

    .line 1312
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->K:Lcom/sec/chaton/msgbox/as;

    invoke-interface {v0, v2}, Lcom/sec/chaton/msgbox/as;->a(Z)V

    .line 1313
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a(Landroid/widget/ListView;)V

    .line 1331
    :cond_0
    :goto_0
    return-void

    .line 1317
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemIds()[J

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1318
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->G:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1322
    :goto_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemIds()[J

    move-result-object v0

    array-length v0, v0

    invoke-virtual {p1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 1323
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 1324
    iput-boolean v2, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->o:Z

    goto :goto_0

    .line 1320
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->G:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 1326
    :cond_3
    iput-boolean v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->o:Z

    .line 1327
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1265
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 1267
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1269
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1270
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOptionsItemSelected() itemId["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 1287
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1276
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->i()V

    goto :goto_0

    .line 1281
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;

    if-eqz v0, :cond_1

    .line 1282
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->n()V

    goto :goto_0

    .line 1273
    :pswitch_data_0
    .packed-switch 0x7f07057d
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 416
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->E:Z

    .line 419
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 421
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->d:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 422
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1239
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1243
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->G:Landroid/view/Menu;

    const v1, 0x7f07057c

    invoke-interface {v0, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1245
    const v0, 0x7f07057d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1246
    if-eqz v0, :cond_0

    .line 1247
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1250
    :cond_0
    const v0, 0x7f07057e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1251
    if-eqz v0, :cond_1

    .line 1252
    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemIds()[J

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_2

    .line 1253
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1259
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->D:Lcom/sec/widget/DropPanelMenu;

    invoke-virtual {v0, v2}, Lcom/sec/widget/DropPanelMenu;->setItemChanged(Z)V

    .line 1260
    return-void

    .line 1255
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 388
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 389
    iput-boolean v5, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->E:Z

    .line 391
    invoke-static {p0, v1}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 392
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->g()V

    .line 394
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;

    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;->d()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02bb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/q;->b()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->d:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 410
    invoke-direct {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->m()V

    .line 412
    return-void

    .line 397
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    if-eqz v0, :cond_2

    .line 398
    const-string v0, ""

    .line 399
    iget-object v3, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->y:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->y:Ljava/lang/String;

    move-object v4, v0

    .line 402
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->p:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/q;->c()Landroid/net/Uri;

    move-result-object v3

    new-array v6, v1, [Ljava/lang/String;

    aput-object v4, v6, v5

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 404
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR!! "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is attached on wrong activity."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v4, v0

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 209
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 210
    const-string v0, "checkArray"

    iget-object v1, p0, Lcom/sec/chaton/msgbox/MsgboxSelectionFragment;->k:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableListView;->getCheckedItemIds()[J

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 214
    return-void
.end method
