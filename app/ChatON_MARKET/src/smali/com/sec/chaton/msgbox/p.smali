.class Lcom/sec/chaton/msgbox/p;
.super Landroid/os/Handler;
.source "MsgboxFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/msgbox/MsgboxFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/msgbox/MsgboxFragment;)V
    .locals 0

    .prologue
    .line 2065
    iput-object p1, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2068
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 2069
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 2122
    :cond_0
    :goto_0
    return-void

    .line 2072
    :pswitch_0
    const-string v1, "mChatListSyncHandler GetChatListInfo.GET_CHATLISTINFO"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2074
    iget-object v1, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->s(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2076
    iget-object v1, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->s(Lcom/sec/chaton/msgbox/MsgboxFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    move v1, v2

    .line 2079
    :goto_1
    iget-object v4, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v4}, Lcom/sec/chaton/msgbox/MsgboxFragment;->t(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/common/a/d;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v4}, Lcom/sec/chaton/msgbox/MsgboxFragment;->t(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/common/a/d;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2080
    iget-object v4, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v4}, Lcom/sec/chaton/msgbox/MsgboxFragment;->t(Lcom/sec/chaton/msgbox/MsgboxFragment;)Lcom/sec/common/a/d;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/common/a/d;->dismiss()V

    .line 2083
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v4, v5, :cond_4

    .line 2084
    const-string v0, "Success to sync ChatListInfo ... "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087
    invoke-static {}, Lcom/sec/chaton/provider/a/a;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2088
    invoke-static {}, Lcom/sec/chaton/provider/a/a;->e()V

    .line 2089
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v0

    const/4 v2, -0x1

    new-instance v4, Lcom/sec/chaton/e/b/m;

    sget v5, Lcom/sec/chaton/chat/notification/a;->d:I

    invoke-direct {v4, v6, v5, v3}, Lcom/sec/chaton/e/b/m;-><init>(Lcom/sec/chaton/e/b/d;IZ)V

    invoke-static {v0, v2, v4}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 2093
    :cond_2
    invoke-static {v6}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;)V

    .line 2095
    if-eqz v1, :cond_0

    .line 2097
    invoke-static {}, Lcom/sec/chaton/provider/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2099
    const-string v0, "GET_CHATLISTINFO : NO_CONTENT "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2100
    iget-object v0, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->w(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    goto/16 :goto_0

    .line 2102
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2103
    iget-object v0, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-static {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->p(Lcom/sec/chaton/msgbox/MsgboxFragment;)V

    goto/16 :goto_0

    .line 2107
    :cond_4
    if-eqz v1, :cond_5

    .line 2108
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/msgbox/p;->a:Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/msgbox/MsgboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0291

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2111
    :cond_5
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-ne v1, v2, :cond_0

    .line 2112
    const-string v1, "Fail to sync ChatListInfo ... "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ChatListInfo Fail code : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", message : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2114
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v3, 0x7f0e0000

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 2115
    const-string v1, "01000006"

    const-string v2, "1008"

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0

    :cond_6
    move v1, v3

    goto/16 :goto_1

    .line 2069
    nop

    :pswitch_data_0
    .packed-switch 0x8fd
        :pswitch_0
    .end packed-switch
.end method
