.class public Lcom/sec/chaton/PlusFragment;
.super Landroid/support/v4/app/Fragment;
.source "PlusFragment.java"

# interfaces
.implements Lcom/sec/chaton/by;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Z

.field c:Landroid/database/ContentObserver;

.field d:Lcom/sec/chaton/e/a/v;

.field private e:Lcom/sec/widget/ExpandableHeightGridView;

.field private f:Lcom/sec/widget/ExpandableHeightGridView;

.field private g:Lcom/sec/chaton/settings/moreapps/a;

.field private h:Lcom/sec/chaton/settings/moreapps/d;

.field private i:Lcom/sec/chaton/event/e;

.field private j:Z

.field private k:Landroid/widget/LinearLayout;

.field private l:Landroid/database/Cursor;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/aj;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/ak;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private p:Lcom/sec/common/f/c;

.field private q:Landroid/app/Activity;

.field private r:Lcom/sec/chaton/PlusFragment;

.field private s:Z

.field private t:Z

.field private u:Lcom/sec/chaton/e/a/u;

.field private v:Landroid/widget/AdapterView$OnItemClickListener;

.field private w:Landroid/widget/AdapterView$OnItemClickListener;

.field private x:Landroid/content/BroadcastReceiver;

.field private y:Landroid/content/BroadcastReceiver;

.field private z:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/chaton/PlusFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/PlusFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/PlusFragment;->j:Z

    .line 195
    new-instance v0, Lcom/sec/chaton/aa;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/aa;-><init>(Lcom/sec/chaton/PlusFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->c:Landroid/database/ContentObserver;

    .line 261
    new-instance v0, Lcom/sec/chaton/ab;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ab;-><init>(Lcom/sec/chaton/PlusFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->v:Landroid/widget/AdapterView$OnItemClickListener;

    .line 292
    new-instance v0, Lcom/sec/chaton/ac;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ac;-><init>(Lcom/sec/chaton/PlusFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->w:Landroid/widget/AdapterView$OnItemClickListener;

    .line 360
    new-instance v0, Lcom/sec/chaton/ad;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ad;-><init>(Lcom/sec/chaton/PlusFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->d:Lcom/sec/chaton/e/a/v;

    .line 495
    new-instance v0, Lcom/sec/chaton/ae;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ae;-><init>(Lcom/sec/chaton/PlusFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->x:Landroid/content/BroadcastReceiver;

    .line 501
    new-instance v0, Lcom/sec/chaton/af;

    invoke-direct {v0, p0}, Lcom/sec/chaton/af;-><init>(Lcom/sec/chaton/PlusFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->y:Landroid/content/BroadcastReceiver;

    .line 1019
    new-instance v0, Lcom/sec/chaton/ag;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ag;-><init>(Lcom/sec/chaton/PlusFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->z:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/PlusFragment;I)I
    .locals 0

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/chaton/PlusFragment;->o:I

    return p1
.end method

.method private a(Lcom/sec/chaton/ai;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 594
    .line 595
    sget-object v1, Lcom/sec/chaton/ah;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/ai;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 606
    :goto_0
    return v0

    .line 597
    :pswitch_0
    invoke-static {}, Lcom/sec/chaton/settings/downloads/av;->a()I

    move-result v0

    goto :goto_0

    .line 600
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "new_livepartner_count"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 603
    :pswitch_2
    invoke-static {}, Lcom/sec/chaton/event/f;->m()I

    move-result v0

    goto :goto_0

    .line 595
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/PlusFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/chaton/PlusFragment;->l:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->u:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/PlusFragment;Lcom/sec/chaton/settings/moreapps/a;)Lcom/sec/chaton/settings/moreapps/a;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/chaton/PlusFragment;->g:Lcom/sec/chaton/settings/moreapps/a;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/PlusFragment;Lcom/sec/common/f/c;)Lcom/sec/common/f/c;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/chaton/PlusFragment;->p:Lcom/sec/common/f/c;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/PlusFragment;Z)Z
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/chaton/PlusFragment;->t:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 245
    new-instance v0, Lcom/sec/chaton/ak;

    const v1, 0x7f020360

    const v2, 0x7f0b030d

    sget-object v3, Lcom/sec/chaton/ai;->a:Lcom/sec/chaton/ai;

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/sec/chaton/ak;-><init>(IIILcom/sec/chaton/ai;)V

    .line 246
    new-instance v1, Lcom/sec/chaton/ak;

    const v2, 0x7f020363

    const v3, 0x7f0b03c2

    sget-object v4, Lcom/sec/chaton/ai;->b:Lcom/sec/chaton/ai;

    invoke-direct {v1, v2, v3, v6, v4}, Lcom/sec/chaton/ak;-><init>(IIILcom/sec/chaton/ai;)V

    .line 247
    new-instance v2, Lcom/sec/chaton/ak;

    const v3, 0x7f020361

    const v4, 0x7f0b027a

    sget-object v5, Lcom/sec/chaton/ai;->c:Lcom/sec/chaton/ai;

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/sec/chaton/ak;-><init>(IIILcom/sec/chaton/ai;)V

    .line 252
    iget-object v3, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    invoke-static {}, Lcom/sec/chaton/event/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 550
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/ak;

    iget-object v1, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/ak;

    iget-object v1, v1, Lcom/sec/chaton/ak;->d:Lcom/sec/chaton/ai;

    invoke-direct {p0, v1}, Lcom/sec/chaton/PlusFragment;->a(Lcom/sec/chaton/ai;)I

    move-result v1

    iput v1, v0, Lcom/sec/chaton/ak;->c:I

    .line 550
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->h:Lcom/sec/chaton/settings/moreapps/d;

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->h:Lcom/sec/chaton/settings/moreapps/d;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/moreapps/d;->notifyDataSetChanged()V

    .line 558
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "plus_tab_badge"

    invoke-direct {p0}, Lcom/sec/chaton/PlusFragment;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 560
    new-instance v0, Landroid/content/Intent;

    const-string v1, "more_tab_badge_update"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 561
    iget-object v1, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 562
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/PlusFragment;)Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/chaton/PlusFragment;->t:Z

    return v0
.end method

.method private f()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 583
    move v1, v0

    move v2, v0

    .line 585
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/ak;

    iget v0, v0, Lcom/sec/chaton/ak;->c:I

    add-int/2addr v2, v0

    .line 585
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 589
    :cond_0
    return v2
.end method

.method static synthetic f(Lcom/sec/chaton/PlusFragment;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->p:Lcom/sec/common/f/c;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/PlusFragment;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->r:Lcom/sec/chaton/PlusFragment;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/settings/moreapps/a;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->g:Lcom/sec/chaton/settings/moreapps/a;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/PlusFragment;)Lcom/sec/widget/ExpandableHeightGridView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->e:Lcom/sec/widget/ExpandableHeightGridView;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->l:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/PlusFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->k:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/settings/moreapps/d;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->h:Lcom/sec/chaton/settings/moreapps/d;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/PlusFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/chaton/PlusFragment;->e()V

    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/event/e;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->i:Lcom/sec/chaton/event/e;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 3

    .prologue
    .line 490
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_event_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 491
    if-lez v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->i:Lcom/sec/chaton/event/e;

    invoke-virtual {v0}, Lcom/sec/chaton/event/e;->a()Lcom/sec/chaton/event/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 494
    :cond_0
    monitor-exit p0

    return-void

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 614
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->h()V

    .line 618
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "moreapps_polling_recieve"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/chaton/PlusFragment;->s:Z

    if-eqz v0, :cond_2

    .line 619
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-static {v0}, Lcom/sec/chaton/d/t;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/t;

    move-result-object v0

    .line 620
    invoke-virtual {v0}, Lcom/sec/chaton/d/t;->a()Lcom/sec/chaton/settings/moreapps/a/a;

    .line 621
    const-string v0, "Start mMoreAppTask"

    const-class v1, Lcom/sec/chaton/global/GlobalApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    iput-boolean v3, p0, Lcom/sec/chaton/PlusFragment;->s:Z

    .line 624
    const-string v0, "moreapps_polling_recieve"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626
    :cond_2
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 632
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 212
    iput-object p1, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    .line 214
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 539
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 541
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/PlusFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->h()V

    .line 544
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 115
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 117
    const v0, 0x7f0300d5

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 119
    iput-object p0, p0, Lcom/sec/chaton/PlusFragment;->r:Lcom/sec/chaton/PlusFragment;

    .line 121
    const v0, 0x7f0703bf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/ExpandableHeightGridView;

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->f:Lcom/sec/widget/ExpandableHeightGridView;

    .line 122
    const v0, 0x7f0703c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/ExpandableHeightGridView;

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->e:Lcom/sec/widget/ExpandableHeightGridView;

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->f:Lcom/sec/widget/ExpandableHeightGridView;

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->v:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Lcom/sec/widget/ExpandableHeightGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->e:Lcom/sec/widget/ExpandableHeightGridView;

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->w:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Lcom/sec/widget/ExpandableHeightGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 127
    new-instance v0, Lcom/sec/chaton/event/e;

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/PlusFragment;->z:Landroid/os/Handler;

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/event/e;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->i:Lcom/sec/chaton/event/e;

    .line 128
    const v0, 0x7f0703c1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->k:Landroid/widget/LinearLayout;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    .line 149
    new-instance v0, Lcom/sec/chaton/settings/moreapps/d;

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/PlusFragment;->n:Ljava/util/ArrayList;

    invoke-direct {v0, v2, p0, v3}, Lcom/sec/chaton/settings/moreapps/d;-><init>(Landroid/content/Context;Lcom/sec/chaton/PlusFragment;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->h:Lcom/sec/chaton/settings/moreapps/d;

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->f:Lcom/sec/widget/ExpandableHeightGridView;

    invoke-virtual {v0, v4}, Lcom/sec/widget/ExpandableHeightGridView;->setExpanded(Z)V

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->m:Ljava/util/ArrayList;

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->e:Lcom/sec/widget/ExpandableHeightGridView;

    invoke-virtual {v0, v4}, Lcom/sec/widget/ExpandableHeightGridView;->setExpanded(Z)V

    .line 169
    new-instance v0, Lcom/sec/chaton/event/e;

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/PlusFragment;->z:Landroid/os/Handler;

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/event/e;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->i:Lcom/sec/chaton/event/e;

    .line 174
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 175
    const-string v2, "more_event_update"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 176
    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/PlusFragment;->x:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 179
    invoke-direct {p0}, Lcom/sec/chaton/PlusFragment;->d()V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->f:Lcom/sec/widget/ExpandableHeightGridView;

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->h:Lcom/sec/chaton/settings/moreapps/d;

    invoke-virtual {v0, v2}, Lcom/sec/widget/ExpandableHeightGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/PlusFragment;->d:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->u:Lcom/sec/chaton/e/a/u;

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/e/av;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/chaton/PlusFragment;->c:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 187
    iput-boolean v5, p0, Lcom/sec/chaton/PlusFragment;->b:Z

    .line 189
    iput-boolean v4, p0, Lcom/sec/chaton/PlusFragment;->s:Z

    .line 191
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 478
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/PlusFragment;->b:Z

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->p:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->p:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/PlusFragment;->c:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 486
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 487
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    .line 221
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1079
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1080
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/PlusFragment;->j:Z

    .line 1081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPause() / isShow : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/PlusFragment;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/PlusFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/PlusFragment;->x:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1085
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 516
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 519
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/PlusFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->h()V

    .line 523
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/PlusFragment;->a()V

    .line 524
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 525
    const-string v1, "more_event_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 526
    iget-object v1, p0, Lcom/sec/chaton/PlusFragment;->q:Landroid/app/Activity;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/PlusFragment;->x:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 529
    invoke-direct {p0}, Lcom/sec/chaton/PlusFragment;->e()V

    .line 531
    return-void
.end method

.method public onStart()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 228
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 230
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 231
    const-string v0, "onStart - Redraw the list, "

    sget-object v1, Lcom/sec/chaton/PlusFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->u:Lcom/sec/chaton/e/a/u;

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/PlusFragment;->u:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x0

    sget-object v3, Lcom/sec/chaton/e/av;->a:Landroid/net/Uri;

    const-string v7, "priority"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_1
    return-void
.end method
