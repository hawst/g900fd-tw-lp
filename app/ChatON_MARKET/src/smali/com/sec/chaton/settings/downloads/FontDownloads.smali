.class public Lcom/sec/chaton/settings/downloads/FontDownloads;
.super Landroid/support/v4/app/Fragment;
.source "FontDownloads.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Lcom/sec/chaton/base/e;
.implements Lcom/sec/chaton/settings/downloads/br;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/ExpandableListView$OnGroupClickListener;",
        "Lcom/sec/chaton/base/e;",
        "Lcom/sec/chaton/settings/downloads/br;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/FontFilter;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/app/Activity;

.field private d:Landroid/widget/ExpandableListView;

.field private e:Landroid/app/ProgressDialog;

.field private f:Lcom/sec/chaton/d/k;

.field private g:Lcom/sec/chaton/d/a/bz;

.field private h:Lcom/sec/chaton/settings/downloads/bm;

.field private i:Lcom/sec/common/f/c;

.field private j:Landroid/view/Menu;

.field private k:Landroid/os/Handler;

.field private l:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 77
    new-instance v0, Lcom/sec/chaton/settings/downloads/bc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/bc;-><init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->k:Landroid/os/Handler;

    .line 104
    new-instance v0, Lcom/sec/chaton/settings/downloads/bd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/bd;-><init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->l:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/FontDownloads;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/FontDownloads;Lcom/sec/chaton/d/a/bz;)Lcom/sec/chaton/d/a/bz;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->g:Lcom/sec/chaton/d/a/bz;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/FontDownloads;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 280
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->a:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/chaton/e/a/m;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->a:Ljava/util/ArrayList;

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->a:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 345
    :goto_0
    return-void

    .line 288
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "setting_used_font_filter_id"

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 291
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 292
    if-lez v4, :cond_3

    .line 293
    add-int/lit8 v1, v4, 0x1

    new-array v5, v1, [Ljava/lang/CharSequence;

    .line 296
    const v1, 0x7f0b0256

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/FontDownloads;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    move v2, v0

    move v1, v0

    .line 297
    :goto_1
    if-ge v2, v4, :cond_2

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontFilter;

    .line 299
    add-int/lit8 v6, v2, 0x1

    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/FontFilter;->title:Ljava/lang/String;

    aput-object v7, v5, v6

    .line 300
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/FontFilter;->id:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 301
    add-int/lit8 v0, v2, 0x1

    .line 297
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 305
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0257

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/settings/downloads/be;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/be;-><init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V

    invoke-virtual {v0, v5, v1, v2}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 342
    :cond_3
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b(Z)V

    .line 343
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->a(Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/FontDownloads;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->f()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/FontDownloads;)Lcom/sec/chaton/settings/downloads/bm;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->f(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 390
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/bg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/bg;-><init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 399
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->a(Landroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/downloads/bh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/downloads/bh;-><init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 470
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 475
    :cond_1
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 481
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->e(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 489
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/bi;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/bi;-><init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 513
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 514
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->g()V

    .line 427
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 587
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    .line 588
    if-ge v0, v4, :cond_0

    .line 592
    :cond_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 593
    if-eqz p2, :cond_3

    .line 594
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 595
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download_font, onLoadFinished(), id/count/cursor : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :cond_1
    :goto_0
    if-ne v0, v4, :cond_4

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->d:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 605
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/bm;->setGroupCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    :cond_2
    :goto_1
    return-void

    .line 597
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_font, onLoadFinished(), id/count/cursor : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/... null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 606
    :catch_0
    move-exception v0

    .line 607
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 611
    :cond_4
    if-le v0, v4, :cond_2

    .line 613
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v1, v0, p2}, Lcom/sec/chaton/settings/downloads/bm;->setChildrenCursor(ILandroid/database/Cursor;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 614
    :catch_1
    move-exception v0

    .line 615
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 349
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 350
    const-string v1, "ITEM_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/FontDownloads;->startActivityForResult(Landroid/content/Intent;I)V

    .line 353
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 371
    .line 374
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 378
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/bf;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/settings/downloads/bf;-><init>(Lcom/sec/chaton/settings/downloads/FontDownloads;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 385
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705ac

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 95
    :cond_0
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 446
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->g()V

    .line 452
    const/4 v0, 0x1

    .line 456
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705ad

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 102
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 357
    packed-switch p1, :pswitch_data_0

    .line 366
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 367
    return-void

    .line 359
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    if-eqz v0, :cond_0

    .line 360
    const/16 v0, 0x7d0

    if-ne p2, v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bm;->notifyDataSetChanged()V

    goto :goto_0

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 192
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    .line 193
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 197
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 199
    new-instance v0, Lcom/sec/chaton/d/k;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->l:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/d/k;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->f:Lcom/sec/chaton/d/k;

    .line 200
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 533
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 534
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "download_font, onCreateLoader(), id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :cond_0
    if-ge p1, v2, :cond_2

    .line 582
    :cond_1
    :goto_0
    return-object v3

    .line 543
    :cond_2
    if-ne p1, v2, :cond_1

    .line 544
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_used_font_filter_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 547
    if-lez v0, :cond_3

    .line 548
    const-string v4, "filter_id=?"

    .line 549
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    .line 552
    :goto_1
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/e/at;->a:Landroid/net/Uri;

    const-string v6, "filter_title"

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v0

    goto :goto_0

    :cond_3
    move-object v5, v3

    move-object v4, v3

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 403
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->j:Landroid/view/Menu;

    .line 405
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    const v0, 0x7f0f0029

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 407
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 408
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 410
    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 415
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/FontDownloads;->a(Z)V

    .line 416
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b(Z)V

    .line 418
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 419
    return-void

    .line 413
    :cond_0
    const v0, 0x7f0f0028

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v4, 0x280

    const/16 v2, 0x78

    const/4 v7, 0x1

    .line 204
    const v0, 0x7f0300a8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 206
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->i:Lcom/sec/common/f/c;

    .line 208
    const v0, 0x7f070318

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->d:Landroid/widget/ExpandableListView;

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->d:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 210
    new-instance v0, Lcom/sec/chaton/settings/downloads/bm;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    iget-object v5, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->i:Lcom/sec/common/f/c;

    invoke-direct {v0, v1, v3, v5}, Lcom/sec/chaton/settings/downloads/bm;-><init>(Landroid/database/Cursor;Landroid/content/Context;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/bm;->a(Lcom/sec/chaton/settings/downloads/br;)V

    .line 213
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 215
    new-instance v0, Lcom/sec/chaton/settings/downloads/aw;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/aw;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->k:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/aw;->a(Landroid/os/Handler;)V

    .line 217
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/bm;->a(Lcom/sec/chaton/settings/downloads/aw;)V

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->d:Landroid/widget/ExpandableListView;

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setDescendantFocusability(I)V

    .line 220
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->e()V

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->d:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v7}, Landroid/widget/ExpandableListView;->setItemsCanFocus(Z)V

    .line 224
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 225
    const-string v0, "download_font, onCreateView(), request message control : font list"

    sget-object v1, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->f:Lcom/sec/chaton/d/k;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    move v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/k;->a(Lcom/sec/chaton/d/a/b;IIII)Lcom/sec/chaton/d/a/bz;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->g:Lcom/sec/chaton/d/a/bz;

    .line 229
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings/downloads/FontDownloads;->setHasOptionsMenu(Z)V

    .line 231
    return-object v6
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 236
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 237
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "download_font, onDestroyView()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->g:Lcom/sec/chaton/d/a/bz;

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->g:Lcom/sec/chaton/d/a/bz;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/bz;->c()V

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->i:Lcom/sec/common/f/c;

    if-eqz v0, :cond_2

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->i:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    if-eqz v0, :cond_4

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bm;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bm;->b()V

    .line 259
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bm;->c()V

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/downloads/bm;->a(Lcom/sec/chaton/settings/downloads/br;)V

    .line 263
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->f:Lcom/sec/chaton/d/k;

    if-eqz v0, :cond_5

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->f:Lcom/sec/chaton/d/k;

    invoke-virtual {v0}, Lcom/sec/chaton/d/k;->a()V

    .line 266
    :cond_5
    iput-object v2, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->l:Landroid/os/Handler;

    .line 267
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 271
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 272
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 273
    const-string v0, "download_font, onDetach()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->c:Landroid/app/Activity;

    .line 277
    return-void
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 1

    .prologue
    .line 518
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 61
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/FontDownloads;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 622
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    .line 624
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 625
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_font, onLoaderReset(), id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/FontDownloads;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :cond_0
    if-ge v0, v3, :cond_1

    .line 632
    :cond_1
    if-ne v0, v3, :cond_3

    .line 634
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/bm;->setGroupCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :cond_2
    :goto_0
    return-void

    .line 635
    :catch_0
    move-exception v0

    .line 636
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 638
    :cond_3
    if-le v0, v3, :cond_2

    .line 641
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDownloads;->h:Lcom/sec/chaton/settings/downloads/bm;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/downloads/bm;->setChildrenCursor(ILandroid/database/Cursor;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 642
    :catch_1
    move-exception v0

    .line 643
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 431
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 441
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 434
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->c()V

    goto :goto_0

    .line 438
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->d()V

    goto :goto_0

    .line 431
    :pswitch_data_0
    .packed-switch 0x7f0705ac
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
