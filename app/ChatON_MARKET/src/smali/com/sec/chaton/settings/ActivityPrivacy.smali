.class public Lcom/sec/chaton/settings/ActivityPrivacy;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "ActivityPrivacy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;


# instance fields
.field private A:Landroid/preference/Preference;

.field private B:Landroid/preference/Preference;

.field private C:Landroid/os/Handler;

.field a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/chaton/util/ab;

.field private d:Landroid/preference/CheckBoxPreference;

.field private e:Landroid/preference/CheckBoxPreference;

.field private f:Landroid/app/ProgressDialog;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Lcom/sec/chaton/d/h;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Landroid/preference/Preference;

.field private u:[Ljava/lang/String;

.field private v:I

.field private w:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/PrivacyList;",
            ">;"
        }
    .end annotation
.end field

.field private x:I

.field private y:Lcom/sec/common/a/d;

.field private z:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    .line 55
    const-class v0, Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->a:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->c:Lcom/sec/chaton/util/ab;

    .line 65
    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    .line 82
    const-class v0, Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    .line 84
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->u:[Ljava/lang/String;

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->v:I

    .line 91
    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->y:Lcom/sec/common/a/d;

    .line 649
    new-instance v0, Lcom/sec/chaton/settings/bz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/bz;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->C:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPrivacy;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->o:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPrivacy;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->y:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPrivacy;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->z:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPrivacy;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->w:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a()V
    .locals 11

    .prologue
    const v10, 0x7f0b0183

    const v9, 0x7f08001b

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x7f08003f

    .line 191
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 194
    const-string v0, "pref_item_phonenumber"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 196
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 202
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "show_phone_number_to_all"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-ne v0, v8, :cond_5

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_4

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 220
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings/bs;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bs;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 242
    const-string v0, "pref_item_show_profileimage"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    .line 243
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v8, :cond_8

    .line 244
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    .line 249
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings/bt;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bt;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 287
    const-string v0, "pref_item_samsung_account"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->p:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    .line 289
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 298
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "samsung_account_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-ne v0, v8, :cond_a

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->q:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 312
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings/bv;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bv;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 328
    const-string v0, "pref_item_birthday"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    .line 329
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->c()V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings/bw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bw;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 342
    const-string v0, "pref_item_poston"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 344
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 345
    invoke-virtual {p0, v10}, Lcom/sec/chaton/settings/ActivityPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 346
    new-instance v1, Lcom/sec/chaton/settings/bx;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bx;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->t:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings/by;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/by;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 389
    return-void

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v8}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 207
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_1

    .line 210
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_7

    .line 212
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_1

    .line 216
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_1

    .line 246
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_2

    .line 294
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v8}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_3

    .line 306
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_4
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPrivacy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 398
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 399
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 400
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 401
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const v6, 0x7f08003f

    const v5, 0x7f08001b

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 582
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    const-string v0, "phonenumber"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isEnabled()Z

    move-result v0

    if-ne v0, v3, :cond_3

    .line 586
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 588
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 593
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitPrivacyCheck/ phonenumber : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 590
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 591
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 594
    :cond_3
    const-string v0, "showprofileimage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 595
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 597
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->x:I

    .line 598
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 599
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    .line 606
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitPrivacyCheck/ showprofileimage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 602
    :cond_4
    iput v4, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->x:I

    .line 603
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 604
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto :goto_2

    .line 607
    :cond_5
    const-string v0, "emailsamsung"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isEnabled()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 608
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 609
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 610
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 615
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitPrivacyCheck/ emailsamsung : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 612
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 613
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 405
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 406
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 407
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, p4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v0, v2, v4, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 408
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, p5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v1, v2, v4, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 409
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v4

    const/4 v0, 0x1

    const-string v3, "\n"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    aput-object v1, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 410
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v7, 0x7f08001b

    const v6, 0x7f08003f

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_1

    .line 551
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 552
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 563
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v9, :cond_5

    .line 564
    iput v9, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->x:I

    .line 565
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    .line 571
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 573
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->q:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 579
    :goto_2
    return-void

    .line 554
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto :goto_0

    .line 557
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_4

    .line 558
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0

    .line 560
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_0

    .line 568
    :cond_5
    iput v8, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->x:I

    .line 569
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->B:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_1

    .line 575
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_2
.end method

.method private b(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 417
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 419
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 421
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 423
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/ActivityPrivacy;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->z:Ljava/lang/Boolean;

    return-object v0
.end method

.method private c()V
    .locals 9

    .prologue
    const v8, 0x7f0b008f

    const v7, 0x7f08003f

    const/4 v6, 0x2

    const/4 v5, 0x1

    const v4, 0x7f08001b

    .line 621
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 622
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "birthday_type"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 623
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 624
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 625
    const-string v0, "SHORT_HIDE"

    .line 627
    :cond_0
    const-string v2, "FULL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 628
    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 646
    :goto_0
    return-void

    .line 629
    :cond_1
    const-string v2, "SHORT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 630
    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 631
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, v0, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v0, v0, v6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 632
    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0

    .line 633
    :cond_2
    const-string v2, "FULL_HIDE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 634
    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0

    .line 635
    :cond_3
    const-string v2, "SHORT_HIDE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 636
    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 637
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, v0, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v0, v0, v6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 638
    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0

    .line 640
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0

    .line 644
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->A:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/ActivityPrivacy;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->x:I

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/ActivityPrivacy;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->y:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/ActivityPrivacy;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->u:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/ActivityPrivacy;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->v:I

    return v0
.end method

.method static synthetic i(Lcom/sec/chaton/settings/ActivityPrivacy;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b()V

    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->d:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->e:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/settings/ActivityPrivacy;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->w:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const v4, 0x7f0b01c4

    const v2, 0x7f0b01c3

    const v3, 0x7f08001b

    .line 479
    const-string v0, "changeSummary"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-static {}, Lcom/sec/chaton/util/p;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->t:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 482
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->t:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 487
    :goto_0
    return-void

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->t:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(I)V

    .line 485
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->t:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    .line 436
    const-string v1, "prePassword"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    new-array v2, v6, [Ljava/lang/String;

    move v1, v0

    .line 440
    :goto_0
    if-ge v1, v6, :cond_0

    .line 441
    const-string v3, ""

    aput-object v3, v2, v1

    .line 440
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 444
    :cond_0
    const-string v1, "PASSWORD_LOCK"

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 446
    const-string v3, "GET"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 447
    const-string v3, "GET preference"

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v3, "LOCK_STATE"

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 450
    const/4 v3, 0x1

    const-string v4, "PASSWORD"

    const-string v5, "0000"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    .line 452
    :goto_1
    if-ge v0, v6, :cond_2

    .line 453
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "data : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 457
    :cond_1
    const-string v0, "SET preference"

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "state : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pass : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 462
    invoke-static {p2}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 463
    invoke-static {p3}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 464
    const-string v4, "LOCK_STATE"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 465
    const-string v1, "PASSWORD"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 466
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 468
    :cond_2
    return-object v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 534
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 393
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 395
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 100
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-static {p0, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/br;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/br;-><init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 124
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->z:Ljava/lang/Boolean;

    .line 125
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->C:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->o:Lcom/sec/chaton/d/h;

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->o:Lcom/sec/chaton/d/h;

    const-string v1, "phonenumber|showprofileimage|emailsamsung"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->g(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->w:Ljava/util/ArrayList;

    .line 137
    const v0, 0x7f050010

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->addPreferencesFromResource(I)V

    .line 138
    iput-object p0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->b:Landroid/content/Context;

    .line 139
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->c:Lcom/sec/chaton/util/ab;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->c:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 141
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->p:Ljava/lang/String;

    .line 144
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->g:Ljava/lang/String;

    .line 145
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b018f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->h:Ljava/lang/String;

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0190

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->i:Ljava/lang/String;

    .line 147
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0147

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->k:Ljava/lang/String;

    .line 148
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0284

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->l:Ljava/lang/String;

    .line 149
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0256

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->m:Ljava/lang/String;

    .line 150
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->n:Ljava/lang/String;

    .line 151
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0275

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->q:Ljava/lang/String;

    .line 152
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0285

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->r:Ljava/lang/String;

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b018d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->j:Ljava/lang/String;

    .line 159
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    .line 162
    const-string v0, "pref_item_password_lock"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->t:Landroid/preference/Preference;

    .line 163
    const-string v0, "GET"

    const-string v1, ""

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->u:[Ljava/lang/String;

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->u:[Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->v:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;)V

    .line 168
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v0

    .line 170
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 510
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onDestroy()V

    .line 511
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 492
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onPause()V

    .line 493
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    return-void
.end method

.method protected onRestart()V
    .locals 3

    .prologue
    .line 517
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onRestart()V

    .line 518
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onRestart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->s:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const-string v0, "GET"

    const-string v1, ""

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->u:[Ljava/lang/String;

    .line 521
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->u:[Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/settings/ActivityPrivacy;->v:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Ljava/lang/String;)V

    .line 523
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 498
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onResume()V

    .line 499
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->c()V

    .line 500
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 187
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 176
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 177
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPrivacy;->finish()V

    .line 178
    const/4 v0, 0x1

    .line 180
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 529
    return-void
.end method
