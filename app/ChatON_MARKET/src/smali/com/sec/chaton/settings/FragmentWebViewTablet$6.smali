.class Lcom/sec/chaton/settings/FragmentWebViewTablet$6;
.super Landroid/os/Handler;
.source "FragmentWebViewTablet.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/FragmentWebViewTablet;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$6;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 531
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 532
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 533
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 534
    if-eqz v0, :cond_0

    .line 535
    const-string v0, "accept_time"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "server_time"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[NOTICE] Acccep time : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "accept_time"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v0, "notice"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$6;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    iget-object v0, v0, Lcom/sec/chaton/settings/FragmentWebViewTablet;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$6;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    # getter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->url:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$100(Lcom/sec/chaton/settings/FragmentWebViewTablet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Noti url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$6;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    # getter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->url:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$100(Lcom/sec/chaton/settings/FragmentWebViewTablet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-void
.end method
