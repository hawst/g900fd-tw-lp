.class Lcom/sec/chaton/settings/tellfriends/q;
.super Lcom/sec/chaton/settings/tellfriends/s;
.source "RenrenManager.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/m;

.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/m;)V
    .locals 1

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/q;->a:Lcom/sec/chaton/settings/tellfriends/m;

    .line 220
    const-string v0, "GetMyInfoTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/s;-><init>(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V

    .line 217
    const-string v0, "uid,name,tinyurl,mainurl,headurl"

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/q;->b:Ljava/lang/String;

    .line 221
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/m;Lcom/sec/chaton/settings/tellfriends/n;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/q;-><init>(Lcom/sec/chaton/settings/tellfriends/m;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 225
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/m;->f()Lcom/renren/android/Renren;

    move-result-object v1

    invoke-virtual {v1}, Lcom/renren/android/Renren;->e()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 226
    new-instance v1, Lcom/renren/android/c/f;

    invoke-direct {v1, v0}, Lcom/renren/android/c/f;-><init>([Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/q;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/renren/android/c/f;->a(Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x0

    .line 230
    :try_start_0
    invoke-virtual {v1}, Lcom/renren/android/c/f;->a()Landroid/os/Bundle;
    :try_end_0
    .catch Lcom/renren/android/f; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 235
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/q;->a(Landroid/os/Bundle;)I

    move-result v1

    .line 237
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 238
    new-instance v0, Lcom/renren/android/c/g;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/q;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/renren/android/c/g;-><init>(Ljava/lang/String;)V

    .line 239
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/renren/android/c/g;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 240
    invoke-virtual {v0}, Lcom/renren/android/c/g;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/renren/android/c/a;

    .line 241
    invoke-virtual {v0}, Lcom/renren/android/c/a;->a()J

    move-result-wide v2

    .line 242
    invoke-virtual {v0}, Lcom/renren/android/c/a;->b()Ljava/lang/String;

    move-result-object v4

    .line 243
    invoke-virtual {v0}, Lcom/renren/android/c/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 245
    iget-object v5, p0, Lcom/sec/chaton/settings/tellfriends/q;->a:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-static {v5}, Lcom/sec/chaton/settings/tellfriends/m;->a(Lcom/sec/chaton/settings/tellfriends/m;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/sec/chaton/settings/tellfriends/u;->b(Landroid/content/Context;Ljava/lang/String;)Z

    .line 246
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/q;->a:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/m;->a(Lcom/sec/chaton/settings/tellfriends/m;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/sec/chaton/settings/tellfriends/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 247
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/q;->a:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/m;->a(Lcom/sec/chaton/settings/tellfriends/m;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/chaton/settings/tellfriends/u;->c(Landroid/content/Context;Ljava/lang/String;)Z

    .line 251
    :cond_0
    return v1

    .line 231
    :catch_0
    move-exception v1

    .line 232
    invoke-virtual {v1}, Lcom/renren/android/f;->printStackTrace()V

    goto :goto_0
.end method
