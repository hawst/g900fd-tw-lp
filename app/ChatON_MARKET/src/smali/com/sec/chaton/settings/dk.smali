.class public final enum Lcom/sec/chaton/settings/dk;
.super Ljava/lang/Enum;
.source "FragmentChatView2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/dk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/dk;

.field public static final enum b:Lcom/sec/chaton/settings/dk;

.field public static final enum c:Lcom/sec/chaton/settings/dk;

.field public static final enum d:Lcom/sec/chaton/settings/dk;

.field public static final enum e:Lcom/sec/chaton/settings/dk;

.field public static final enum f:Lcom/sec/chaton/settings/dk;

.field private static final synthetic h:[Lcom/sec/chaton/settings/dk;


# instance fields
.field g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 48
    new-instance v0, Lcom/sec/chaton/settings/dk;

    const-string v1, "System"

    const-string v2, "sizeSystem"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/settings/dk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/settings/dk;->a:Lcom/sec/chaton/settings/dk;

    .line 49
    new-instance v0, Lcom/sec/chaton/settings/dk;

    const-string v1, "Tiny"

    const-string v2, "size50"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/settings/dk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/settings/dk;->b:Lcom/sec/chaton/settings/dk;

    .line 50
    new-instance v0, Lcom/sec/chaton/settings/dk;

    const-string v1, "Small"

    const-string v2, "size70"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/settings/dk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/settings/dk;->c:Lcom/sec/chaton/settings/dk;

    .line 51
    new-instance v0, Lcom/sec/chaton/settings/dk;

    const-string v1, "Normal"

    const-string v2, "size100"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/chaton/settings/dk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    .line 52
    new-instance v0, Lcom/sec/chaton/settings/dk;

    const-string v1, "Large"

    const-string v2, "size150"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/chaton/settings/dk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/settings/dk;->e:Lcom/sec/chaton/settings/dk;

    .line 53
    new-instance v0, Lcom/sec/chaton/settings/dk;

    const-string v1, "Huge"

    const/4 v2, 0x5

    const-string v3, "size200"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/dk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/settings/dk;->f:Lcom/sec/chaton/settings/dk;

    .line 47
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/chaton/settings/dk;

    sget-object v1, Lcom/sec/chaton/settings/dk;->a:Lcom/sec/chaton/settings/dk;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/settings/dk;->b:Lcom/sec/chaton/settings/dk;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/settings/dk;->c:Lcom/sec/chaton/settings/dk;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/settings/dk;->e:Lcom/sec/chaton/settings/dk;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/settings/dk;->f:Lcom/sec/chaton/settings/dk;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/settings/dk;->h:[Lcom/sec/chaton/settings/dk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput-object p3, p0, Lcom/sec/chaton/settings/dk;->g:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/settings/dk;
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/chaton/settings/dk;->values()[Lcom/sec/chaton/settings/dk;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/settings/dk;
    .locals 5

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/chaton/settings/dk;->values()[Lcom/sec/chaton/settings/dk;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 61
    invoke-virtual {v3}, Lcom/sec/chaton/settings/dk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 62
    return-object v3

    .line 60
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/dk;
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/chaton/settings/dk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/dk;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/dk;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/chaton/settings/dk;->h:[Lcom/sec/chaton/settings/dk;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/dk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/dk;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/settings/dk;->g:Ljava/lang/String;

    return-object v0
.end method
