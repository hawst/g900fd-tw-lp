.class Lcom/sec/chaton/settings/bv;
.super Ljava/lang/Object;
.source "ActivityPrivacy.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/sec/chaton/settings/bv;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 316
    if-eqz p2, :cond_0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/settings/bv;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Lcom/sec/chaton/settings/ActivityPrivacy;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/h;->d(Z)V

    .line 322
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/bv;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 323
    return v2

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/bv;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Lcom/sec/chaton/settings/ActivityPrivacy;)Lcom/sec/chaton/d/h;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->d(Z)V

    goto :goto_0
.end method
