.class Lcom/sec/chaton/settings/x;
.super Ljava/lang/Object;
.source "ActivityContactSyncFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/chaton/settings/x;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/chaton/settings/x;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/settings/x;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 213
    const-string v0, "support_contact_auto_sync"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/settings/x;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 219
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/x;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
