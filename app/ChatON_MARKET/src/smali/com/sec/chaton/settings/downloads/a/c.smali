.class final enum Lcom/sec/chaton/settings/downloads/a/c;
.super Ljava/lang/Enum;
.source "AmsItemInstallWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/downloads/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/downloads/a/c;

.field public static final enum b:Lcom/sec/chaton/settings/downloads/a/c;

.field private static final synthetic c:[Lcom/sec/chaton/settings/downloads/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/c;

    const-string v1, "INSTALL_BY_DOWNLOADING"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/c;->a:Lcom/sec/chaton/settings/downloads/a/c;

    .line 43
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/c;

    const-string v1, "INSTALL_BY_EXIST_FILE"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/settings/downloads/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/c;->b:Lcom/sec/chaton/settings/downloads/a/c;

    .line 41
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/settings/downloads/a/c;

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/c;->a:Lcom/sec/chaton/settings/downloads/a/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/c;->b:Lcom/sec/chaton/settings/downloads/a/c;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/c;->c:[Lcom/sec/chaton/settings/downloads/a/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/c;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/chaton/settings/downloads/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/c;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/downloads/a/c;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/chaton/settings/downloads/a/c;->c:[Lcom/sec/chaton/settings/downloads/a/c;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/downloads/a/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/downloads/a/c;

    return-object v0
.end method
