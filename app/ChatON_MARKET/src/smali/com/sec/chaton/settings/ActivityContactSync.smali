.class public Lcom/sec/chaton/settings/ActivityContactSync;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityContactSync.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-direct {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;-><init>()V

    return-object v0
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f0c00fc

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f0c00fb

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 37
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityContactSync;->a(Landroid/app/Activity;)V

    .line 38
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 31
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityContactSync;->a(Landroid/app/Activity;)V

    .line 32
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 20
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSync;->finish()V

    .line 22
    const/4 v0, 0x1

    .line 24
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
