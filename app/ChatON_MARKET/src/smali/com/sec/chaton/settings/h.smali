.class Lcom/sec/chaton/settings/h;
.super Ljava/lang/Object;
.source "AboutServiceFragment.java"

# interfaces
.implements Lcom/coolots/sso/a/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/g;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/g;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/sec/chaton/settings/h;->a:Lcom/sec/chaton/settings/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveUpdateVerion(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 690
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onReceiveUpdateVerion] ChatONV currentVersionName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",serverVersionName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",versionInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/settings/h;->a:Lcom/sec/chaton/settings/g;

    iget-object v0, v0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aput-object p1, v0, v1

    .line 694
    iget-object v0, p0, Lcom/sec/chaton/settings/h;->a:Lcom/sec/chaton/settings/g;

    iget-object v0, v0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aput-object p2, v0, v1

    .line 695
    const-string v0, "chatonv_update_version"

    invoke-static {v0, p2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    if-eqz p3, :cond_0

    .line 698
    iget-object v0, p0, Lcom/sec/chaton/settings/h;->a:Lcom/sec/chaton/settings/g;

    iget-object v0, v0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z

    .line 699
    iget-object v0, p0, Lcom/sec/chaton/settings/h;->a:Lcom/sec/chaton/settings/g;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/g;->notifyDataSetChanged()V

    .line 700
    const-string v0, "chatonVUpdateStatus"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 703
    if-eqz p3, :cond_0

    .line 704
    new-instance v0, Landroid/content/Intent;

    const-string v1, "more_tab_badge_update"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 705
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 709
    :cond_0
    return-void
.end method
