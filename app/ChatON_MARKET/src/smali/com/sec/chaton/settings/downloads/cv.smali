.class Lcom/sec/chaton/settings/downloads/cv;
.super Landroid/os/Handler;
.source "SoundDetail.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/SoundDetail;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/SoundDetail;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 203
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 205
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/v;

    .line 207
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 234
    :goto_0
    return-void

    .line 210
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 211
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PackageId: "

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    const-string v2, ". Status: "

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/SoundDetail;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->b(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Lcom/sec/chaton/settings/downloads/SoundDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 219
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->b(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Lcom/sec/chaton/settings/downloads/SoundDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 224
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-static {v0, v2}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Lcom/sec/chaton/settings/downloads/SoundDetail;Lcom/sec/chaton/settings/downloads/a/v;)Lcom/sec/chaton/settings/downloads/a/v;

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-virtual {v0, v3, v2, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->b(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Lcom/sec/chaton/settings/downloads/SoundDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 231
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/cv;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->c(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
