.class public Lcom/sec/chaton/settings/FragmentSkinChange3;
.super Landroid/support/v4/app/Fragment;
.source "FragmentSkinChange3.java"


# static fields
.field public static a:I

.field public static b:I

.field public static c:I


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Z

.field private E:Ljava/io/File;

.field private F:Ljava/io/File;

.field private G:Landroid/net/Uri;

.field private H:I

.field private I:I

.field private J:Landroid/graphics/drawable/BitmapDrawable;

.field private K:Landroid/view/View;

.field private L:Landroid/widget/Toast;

.field private M:Landroid/widget/Toast;

.field private N:Ljava/lang/String;

.field private O:Lcom/sec/common/f/c;

.field private P:Landroid/graphics/Bitmap;

.field private Q:Landroid/graphics/Bitmap;

.field private R:Landroid/graphics/drawable/BitmapDrawable;

.field private S:Z

.field d:Ljava/lang/String;

.field e:Landroid/widget/AdapterView$OnItemClickListener;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/LinearLayout;

.field private r:Lcom/sec/chaton/widget/StateButton;

.field private s:Lcom/sec/chaton/widget/StateButton;

.field private t:Lcom/sec/chaton/widget/StateButton;

.field private u:I

.field private v:Z

.field private w:Landroid/widget/GridView;

.field private x:Landroid/widget/GridView;

.field private y:Lcom/sec/chaton/settings/ei;

.field private z:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x5

    sput v0, Lcom/sec/chaton/settings/FragmentSkinChange3;->a:I

    .line 88
    const/4 v0, 0x6

    sput v0, Lcom/sec/chaton/settings/FragmentSkinChange3;->b:I

    .line 89
    const/4 v0, 0x7

    sput v0, Lcom/sec/chaton/settings/FragmentSkinChange3;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 82
    iput v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->f:I

    .line 83
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->g:I

    .line 84
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->h:I

    .line 85
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->i:I

    .line 86
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->j:I

    .line 104
    sget v0, Lcom/sec/chaton/settings/FragmentSkinChange3;->b:I

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    .line 114
    iput-boolean v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->C:Z

    .line 143
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->J:Landroid/graphics/drawable/BitmapDrawable;

    .line 146
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->L:Landroid/widget/Toast;

    .line 147
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    .line 148
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->N:Ljava/lang/String;

    .line 154
    const-string v0, "all_chat"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->d:Ljava/lang/String;

    .line 159
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->P:Landroid/graphics/Bitmap;

    .line 160
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->Q:Landroid/graphics/Bitmap;

    .line 161
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    .line 364
    new-instance v0, Lcom/sec/chaton/settings/ea;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/ea;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->e:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentSkinChange3;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentSkinChange3;I)I
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentSkinChange3;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentSkinChange3;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentSkinChange3;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Ljava/io/File;)V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 856
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 857
    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    .line 859
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->F:Ljava/io/File;

    .line 860
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->E:Ljava/io/File;

    const-string v2, "skin_myskin.png_"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->F:Ljava/io/File;

    .line 861
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->F:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 862
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->F:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    .line 863
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->F:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->N:Ljava/lang/String;

    .line 865
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 866
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->L:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 867
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->L:Landroid/widget/Toast;

    .line 869
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->L:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 949
    :cond_2
    :goto_0
    return-void

    .line 873
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-ne v0, v3, :cond_4

    .line 875
    const v0, 0x7f0d001d

    .line 879
    :goto_1
    const v1, 0x7f0b0204

    .line 880
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->D:Z

    .line 881
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/settings/ef;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/ef;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 927
    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/settings/eg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/eg;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 934
    new-instance v1, Lcom/sec/chaton/settings/eh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/eh;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/sec/common/a/a;

    .line 941
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 942
    iget-boolean v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->C:Z

    if-eqz v1, :cond_2

    .line 943
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 946
    :catch_0
    move-exception v0

    .line 947
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 877
    :cond_4
    const v0, 0x7f0d001e

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentSkinChange3;Z)Z
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->D:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/FragmentSkinChange3;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->L:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/FragmentSkinChange3;Z)Z
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->C:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/io/File;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->E:Ljava/io/File;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->L:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/FragmentSkinChange3;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->k()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 659
    iget-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    .line 661
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 662
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin_type"

    const-string v2, "pa"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    .line 663
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_send"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    .line 664
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_receive"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    .line 665
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "set_my_bg"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->B:Ljava/lang/String;

    .line 666
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 667
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->E:Ljava/io/File;

    const-string v2, "skin_myskin.png_"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 668
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    const-string v1, "skin_myskin.png_"

    if-ne v0, v1, :cond_0

    .line 669
    const-string v0, "skin_myskin.png_"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 670
    const-string v0, "ma"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    .line 671
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "set_my_bg"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin_type"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    const-string v0, "Y"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->B:Ljava/lang/String;

    .line 681
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 682
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 683
    const-string v0, "pa"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    .line 684
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin_type"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 689
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    .line 690
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_send"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 693
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    .line 694
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_receive"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityCreated] Skin id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Send Bubble: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Receive Bubble:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->g()V

    .line 702
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->h()V

    .line 705
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    const-string v1, "skin_myskin.png_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 706
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->k()V

    .line 711
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->p:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 712
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->g(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 713
    if-eqz v0, :cond_9

    .line 714
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 719
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->q:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_6

    .line 720
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 721
    if-eqz v0, :cond_a

    .line 722
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 727
    :cond_6
    :goto_3
    return-void

    .line 677
    :cond_7
    const-string v0, "N"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 708
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->e()V

    goto :goto_1

    .line 716
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->p:Landroid/widget/LinearLayout;

    const v1, 0x7f0200d5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_2

    .line 724
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->q:Landroid/widget/LinearLayout;

    const v1, 0x7f0200c6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_3
.end method

.method static synthetic j(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 824
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 825
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    .line 826
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "1. windows size=width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    return-object v0
.end method

.method private k()V
    .locals 4

    .prologue
    const/high16 v3, 0x426c0000    # 59.0f

    .line 833
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 834
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 835
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 837
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 838
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v1, v2, v0, v3}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;III)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->Q:Landroid/graphics/Bitmap;

    .line 839
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->Q:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 840
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->J:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 852
    :goto_0
    return-void

    .line 842
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->Q:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 845
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v1, v2, v0, v3}, Lcom/sec/chaton/util/ad;->b(Landroid/content/Context;III)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 846
    if-nez v0, :cond_2

    .line 847
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->J:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 849
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method static synthetic l(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->q:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->p:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/settings/FragmentSkinChange3;)Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    return v0
.end method

.method static synthetic o(Lcom/sec/chaton/settings/FragmentSkinChange3;)Lcom/sec/chaton/settings/ei;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 248
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "install"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v5, "install DESC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/ei;->changeCursor(Landroid/database/Cursor;)V

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ei;->notifyDataSetInvalidated()V

    .line 253
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 468
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 474
    :goto_0
    sget v0, Lcom/sec/chaton/settings/FragmentSkinChange3;->a:I

    if-ne p1, v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->s:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->r:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->t:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 478
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b()V

    .line 490
    :goto_1
    return-void

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    goto :goto_0

    .line 479
    :cond_1
    sget v0, Lcom/sec/chaton/settings/FragmentSkinChange3;->c:I

    if-ne p1, v0, :cond_2

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->t:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->s:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->r:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 483
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->c()V

    goto :goto_1

    .line 485
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->r:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->s:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->t:Lcom/sec/chaton/widget/StateButton;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setSelected(Z)V

    .line 488
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 284
    move v1, v2

    .line 285
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/ej;

    .line 288
    if-nez v0, :cond_0

    .line 321
    :goto_1
    return-void

    .line 291
    :cond_0
    iget v3, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    sget v4, Lcom/sec/chaton/settings/FragmentSkinChange3;->c:I

    if-ne v3, v4, :cond_2

    .line 292
    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 293
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 294
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_skin"

    invoke-virtual {v0, v3, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_skin_type"

    invoke-virtual {v0, v3, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 297
    iput-object p2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    .line 285
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 299
    :cond_1
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 302
    :cond_2
    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 303
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 304
    iget v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    sget v3, Lcom/sec/chaton/settings/FragmentSkinChange3;->a:I

    if-ne v0, v3, :cond_3

    .line 305
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_bubble_send"

    invoke-virtual {v0, v3, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    goto :goto_2

    .line 308
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_bubble_receive"

    invoke-virtual {v0, v3, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    goto :goto_2

    .line 312
    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 316
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    if-eqz v0, :cond_6

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 319
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00e6

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const v5, 0x7f0202d3

    const v1, 0x7f0202d1

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 493
    if-eqz p1, :cond_2

    .line 494
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 497
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 498
    iput-boolean v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    .line 526
    :goto_0
    return-void

    .line 501
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 506
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 507
    iput-boolean v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    goto :goto_0

    .line 504
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    const v1, 0x7f0202d2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    if-nez v0, :cond_3

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 514
    iput-boolean v3, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    goto :goto_0

    .line 517
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 518
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 522
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 523
    iput-boolean v3, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    goto :goto_0

    .line 520
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    const v1, 0x7f0202d4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 256
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "install"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v5, "install DESC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/ei;->changeCursor(Landroid/database/Cursor;)V

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ei;->notifyDataSetInvalidated()V

    .line 261
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 325
    move v1, v2

    .line 326
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/ej;

    .line 329
    if-nez v0, :cond_0

    .line 362
    :goto_1
    return-void

    .line 332
    :cond_0
    iget v3, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    sget v4, Lcom/sec/chaton/settings/FragmentSkinChange3;->c:I

    if-ne v3, v4, :cond_2

    .line 333
    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 334
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 335
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_skin"

    invoke-virtual {v0, v3, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_skin_type"

    invoke-virtual {v0, v3, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 338
    iput-object p2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    .line 326
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 340
    :cond_1
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 343
    :cond_2
    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 344
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 345
    iget v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    sget v3, Lcom/sec/chaton/settings/FragmentSkinChange3;->a:I

    if-ne v0, v3, :cond_3

    .line 346
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_bubble_send"

    invoke-virtual {v0, v3, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    goto :goto_2

    .line 349
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "setting_change_bubble_receive"

    invoke-virtual {v0, v3, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    goto :goto_2

    .line 353
    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 357
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    if-eqz v0, :cond_6

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 360
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00e6

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public c()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 264
    new-instance v6, Landroid/database/MatrixCursor;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v7

    const-string v1, "reference_id"

    aput-object v1, v0, v8

    const-string v1, "item_id"

    aput-object v1, v0, v9

    const/4 v1, 0x3

    const-string v3, "item_type"

    aput-object v3, v0, v1

    const/4 v1, 0x4

    const-string v3, "name"

    aput-object v3, v0, v1

    const/4 v1, 0x5

    const-string v3, "install"

    aput-object v3, v0, v1

    const/4 v1, 0x6

    const-string v3, "expiration_time"

    aput-object v3, v0, v1

    const/4 v1, 0x7

    const-string v3, "new"

    aput-object v3, v0, v1

    const/16 v1, 0x8

    const-string v3, "special"

    aput-object v3, v0, v1

    const/16 v1, 0x9

    const-string v3, "extras"

    aput-object v3, v0, v1

    invoke-direct {v6, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 267
    invoke-virtual {v6}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    const-string v1, "skin_add"

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->B:Ljava/lang/String;

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v6}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    const-string v1, "skin_myskin.png_"

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 272
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "install"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v5, "install DESC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 274
    new-array v1, v9, [Landroid/database/Cursor;

    aput-object v6, v1, v7

    aput-object v0, v1, v8

    .line 275
    new-instance v0, Landroid/database/MergeCursor;

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 277
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/ei;->changeCursor(Landroid/database/Cursor;)V

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ei;->notifyDataSetInvalidated()V

    .line 279
    return-void
.end method

.method public d()Z
    .locals 4

    .prologue
    .line 609
    const/4 v0, 0x0

    .line 612
    const-string v1, "Y"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/l;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 614
    new-instance v2, Ljava/io/File;

    const-string v3, "/skins/"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 615
    if-eqz v2, :cond_0

    .line 616
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    .line 617
    const/4 v0, 0x1

    .line 623
    :cond_0
    return v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 731
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->f(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/ck;

    move-result-object v0

    .line 732
    if-nez v0, :cond_0

    .line 733
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->f()V

    .line 743
    :goto_0
    return-void

    .line 735
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    const-string v2, "pa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 736
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/ck;->b:Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    .line 737
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 738
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 740
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/ck;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 746
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203e9

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    .line 747
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 748
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 749
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 752
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 753
    iget-boolean v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Z)V

    .line 754
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    iget v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/ei;->a(I)V

    .line 755
    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(I)V

    .line 757
    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 758
    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->d:Ljava/lang/String;

    .line 760
    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    const-string v1, "skin_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1021
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 1032
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    const-string v1, "bubble_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1033
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    .line 1044
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    const-string v1, "bubble_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1045
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    .line 1055
    :cond_2
    :goto_2
    return-void

    .line 1022
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    const-string v1, "skin_bg_02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1023
    const-string v0, "-2"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    goto :goto_0

    .line 1024
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    const-string v1, "skin_bg_03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1025
    const-string v0, "-3"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    goto :goto_0

    .line 1026
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    const-string v1, "skin_bg_04"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1027
    const-string v0, "-4"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    goto :goto_0

    .line 1028
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    const-string v1, "skin_bg_05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1029
    const-string v0, "-5"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    goto :goto_0

    .line 1034
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    const-string v1, "bubble_02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1035
    const-string v0, "-2"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    goto :goto_1

    .line 1036
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    const-string v1, "bubble_03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1037
    const-string v0, "-3"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    goto :goto_1

    .line 1038
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    const-string v1, "bubble_04"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1039
    const-string v0, "-4"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    goto :goto_1

    .line 1040
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    const-string v1, "bubble_05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1041
    const-string v0, "-5"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    goto/16 :goto_1

    .line 1046
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    const-string v1, "bubble_02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1047
    const-string v0, "-2"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    goto/16 :goto_2

    .line 1048
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    const-string v1, "bubble_03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1049
    const-string v0, "-3"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    goto/16 :goto_2

    .line 1050
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    const-string v1, "bubble_04"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1051
    const-string v0, "-4"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    goto/16 :goto_2

    .line 1052
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    const-string v1, "bubble_05"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1053
    const-string v0, "-5"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 645
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 646
    if-eqz p1, :cond_1

    .line 647
    const-string v0, "tab_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648
    const-string v0, "tab_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    .line 650
    :cond_0
    const-string v0, "tab_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 651
    const-string v0, "tab_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    .line 654
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->i()V

    .line 655
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 953
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 954
    packed-switch p1, :pswitch_data_0

    .line 1011
    :goto_0
    return-void

    .line 956
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 957
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2. windows size=width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 959
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 960
    const-string v1, "outputX"

    iget v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 961
    const-string v1, "outputY"

    iget v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 962
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 963
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 964
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 965
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/settings/FragmentSkinChange3;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 967
    :cond_0
    const-string v0, "Camera Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 972
    :pswitch_1
    if-nez p3, :cond_1

    .line 973
    const-string v0, "Gallery Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 976
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2. windows size=width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    .line 978
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 979
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 980
    const-string v1, "outputX"

    iget v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 981
    const-string v1, "outputY"

    iget v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 982
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 983
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 984
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 985
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/settings/FragmentSkinChange3;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 991
    :pswitch_2
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->P:Landroid/graphics/Bitmap;

    .line 992
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "temp_file_path"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/ad;->b(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    iget v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->P:Landroid/graphics/Bitmap;

    .line 993
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->P:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;)Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    .line 994
    const-string v1, "/skins/"

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->N:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/ad;->a(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 995
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->P:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 996
    const-string v0, "Y"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->B:Ljava/lang/String;

    .line 997
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "set_my_bg"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    const-string v0, "skin_myskin.png_"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 999
    const-string v0, "ma"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    .line 1000
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin"

    const-string v2, "skin_myskin.png_"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin_type"

    const-string v2, "ma"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00e6

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1003
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->c()V

    .line 1005
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1006
    :catch_0
    move-exception v0

    .line 1007
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 954
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 244
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 530
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 531
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 167
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 169
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 170
    const-string v0, "[onCreate]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 174
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/Application;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/skins/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->E:Ljava/io/File;

    .line 179
    :goto_0
    new-instance v0, Lcom/sec/chaton/settings/dz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/dz;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    .line 190
    const/4 v1, 0x5

    invoke-static {v1, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 192
    invoke-static {p0, v3}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    .line 194
    new-instance v1, Lcom/sec/common/f/c;

    invoke-direct {v1, v0}, Lcom/sec/common/f/c;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->O:Lcom/sec/common/f/c;

    .line 195
    sget v0, Lcom/sec/chaton/settings/FragmentSkinChange3;->b:I

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    .line 196
    iput-boolean v3, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    .line 198
    if-eqz p1, :cond_c

    .line 199
    const-string v0, "setting_change_skin"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    const-string v0, "setting_change_skin"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    .line 202
    :cond_1
    const-string v0, "setting_change_skin_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    const-string v0, "setting_change_skin_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    .line 205
    :cond_2
    const-string v0, "setting_change_bubble_send"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 206
    const-string v0, "setting_change_bubble_send"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    .line 208
    :cond_3
    const-string v0, "setting_change_bubble_receive"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 209
    const-string v0, "setting_change_bubble_receive"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    .line 211
    :cond_4
    const-string v0, "new_skin_filename"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 212
    const-string v0, "new_skin_filename"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->N:Ljava/lang/String;

    .line 214
    :cond_5
    const-string v0, "tab_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 215
    const-string v0, "tab_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    .line 217
    :cond_6
    const-string v0, "tab_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 218
    const-string v0, "tab_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    .line 220
    :cond_7
    const-string v0, "captureUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 221
    const-string v0, "captureUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    .line 224
    :cond_8
    const-string v0, "popup_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 225
    const-string v0, "popup_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->D:Z

    .line 234
    :cond_9
    :goto_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->D:Z

    if-ne v0, v3, :cond_a

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->E:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Ljava/io/File;)V

    .line 238
    :cond_a
    return-void

    .line 176
    :cond_b
    iput-object v4, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->E:Ljava/io/File;

    goto/16 :goto_0

    .line 229
    :cond_c
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_9

    .line 231
    const-string v1, "called_from_downloads"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->S:Z

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    .prologue
    const v2, 0x7f0705a7

    .line 780
    const v0, 0x7f0f002a

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 781
    iget-boolean v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->S:Z

    if-eqz v0, :cond_0

    .line 782
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 787
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 788
    return-void

    .line 784
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 785
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->b(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const v4, 0x7f070179

    .line 535
    const v0, 0x7f030073

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    .line 537
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f0702d5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    .line 538
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->z:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/settings/eb;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/eb;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546
    new-instance v0, Lcom/sec/chaton/settings/ei;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->O:Lcom/sec/common/f/c;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/ei;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f0702d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->e:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->w:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f0702d6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    .line 555
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->e:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 557
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->x:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->y:Lcom/sec/chaton/settings/ei;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 559
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f070256

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->A:Landroid/widget/LinearLayout;

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f0702d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/StateButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->r:Lcom/sec/chaton/widget/StateButton;

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->r:Lcom/sec/chaton/widget/StateButton;

    new-instance v1, Lcom/sec/chaton/settings/ec;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ec;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f0702d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/StateButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->s:Lcom/sec/chaton/widget/StateButton;

    .line 573
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->s:Lcom/sec/chaton/widget/StateButton;

    new-instance v1, Lcom/sec/chaton/settings/ed;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ed;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 583
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f0702d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/StateButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->t:Lcom/sec/chaton/widget/StateButton;

    .line 584
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->t:Lcom/sec/chaton/widget/StateButton;

    new-instance v1, Lcom/sec/chaton/settings/ee;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ee;-><init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/StateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 594
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f0702cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->o:Landroid/widget/ImageView;

    .line 596
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 597
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->p:Landroid/widget/LinearLayout;

    .line 599
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 600
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    const v1, 0x7f07043b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->q:Landroid/widget/LinearLayout;

    .line 603
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->setHasOptionsMenu(Z)V

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->K:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 812
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 813
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 814
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onDestroy] Skin id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Send Bubble: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Receive Bubble:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->O:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 818
    iput-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->P:Landroid/graphics/Bitmap;

    .line 819
    iput-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->Q:Landroid/graphics/Bitmap;

    .line 820
    iput-object v2, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->R:Landroid/graphics/drawable/BitmapDrawable;

    .line 821
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1016
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 1017
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 807
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 808
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 792
    const v0, 0x7f0705a7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 793
    iget-boolean v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->S:Z

    if-nez v1, :cond_0

    .line 794
    if-eqz v0, :cond_0

    .line 795
    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->e()I

    move-result v1

    if-lez v1, :cond_1

    .line 796
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;Z)V

    .line 802
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 803
    return-void

    .line 798
    :cond_1
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 770
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 771
    iget v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->I:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->H:I

    if-nez v0, :cond_1

    .line 772
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->j()V

    .line 774
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->i()V

    .line 775
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 629
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 630
    const-string v0, "setting_change_skin"

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    const-string v0, "setting_change_skin_type"

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const-string v0, "setting_change_bubble_send"

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    const-string v0, "setting_change_bubble_receive"

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const-string v0, "new_skin_filename"

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->N:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    const-string v0, "tab_type"

    iget v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->u:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 636
    const-string v0, "tab_state"

    iget-boolean v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 637
    const-string v0, "popup_status"

    iget-boolean v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 638
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 639
    const-string v0, "captureUri"

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentSkinChange3;->G:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 764
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 765
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->j()V

    .line 766
    return-void
.end method
