.class Lcom/sec/chaton/settings/v;
.super Ljava/lang/Object;
.source "ActivityContactSyncFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncNow onPreferenceChange "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    if-eqz p2, :cond_1

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    iput-boolean v2, v0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "contact_sim_sync"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 170
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->b(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 172
    :cond_0
    return v3

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    iput-boolean v3, v0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/settings/v;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "contact_sim_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
