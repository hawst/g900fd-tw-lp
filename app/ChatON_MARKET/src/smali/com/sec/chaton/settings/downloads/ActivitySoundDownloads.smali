.class public Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivitySoundDownloads.java"


# instance fields
.field private a:Lcom/sec/chaton/base/e;

.field private b:Lcom/sec/chaton/settings/downloads/SoundDownloads;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/sec/chaton/settings/downloads/SoundDownloads;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;->b:Lcom/sec/chaton/settings/downloads/SoundDownloads;

    .line 34
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;->b:Lcom/sec/chaton/settings/downloads/SoundDownloads;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;->a:Lcom/sec/chaton/base/e;

    .line 35
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;->b:Lcom/sec/chaton/settings/downloads/SoundDownloads;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 54
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 58
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;->a:Lcom/sec/chaton/base/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;->a:Lcom/sec/chaton/base/e;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/base/e;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 65
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 43
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 46
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 73
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 80
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 76
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;->b:Lcom/sec/chaton/settings/downloads/SoundDownloads;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a()V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
