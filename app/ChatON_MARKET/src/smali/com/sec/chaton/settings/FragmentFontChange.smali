.class public Lcom/sec/chaton/settings/FragmentFontChange;
.super Landroid/support/v4/app/Fragment;
.source "FragmentFontChange.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Ljava/lang/String;

.field private b:Landroid/widget/Toast;

.field private c:I

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:I

.field private g:I

.field private h:Landroid/view/View;

.field private i:Lcom/sec/common/f/c;

.field private j:Landroid/widget/ListView;

.field private k:Lcom/sec/chaton/io/entry/inner/FontType;

.field private l:Lcom/sec/chaton/settings/ActivityFontChange;

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/chaton/io/entry/inner/FontType;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/chaton/io/entry/inner/FontType;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/FontType;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field private r:Lcom/sec/chaton/settings/dp;

.field private s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->b:Landroid/widget/Toast;

    .line 101
    const-string v0, "all_chat"

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->a:Ljava/lang/String;

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->q:I

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->s:Ljava/util/HashMap;

    .line 189
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    .line 190
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->n:Ljava/util/Map;

    .line 191
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->o:Ljava/util/Map;

    .line 192
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentFontChange;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->n:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->o:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    .line 180
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->n:Ljava/util/Map;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->o:Ljava/util/Map;

    if-eqz v2, :cond_3

    .line 181
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontTitle()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->n:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontFileSize()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->o:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontType()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 186
    :cond_4
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/FragmentFontChange;)Lcom/sec/chaton/settings/ActivityFontChange;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->l:Lcom/sec/chaton/settings/ActivityFontChange;

    return-object v0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 249
    iput-object v8, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    .line 252
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v9}, Lcom/sec/chaton/io/entry/inner/FontType;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "install"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "install DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 258
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/io/entry/inner/FontType;

    const/4 v3, 0x4

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/io/entry/inner/FontType;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 267
    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 268
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->a()V

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->r:Lcom/sec/chaton/settings/dp;

    if-eqz v0, :cond_4

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->r:Lcom/sec/chaton/settings/dp;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/dp;->notifyDataSetChanged()V

    :cond_4
    move v2, v7

    move v1, v6

    .line 280
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    .line 282
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Default Font Typeface"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_a

    .line 283
    iput v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->q:I

    .line 284
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v3

    iput v3, p0, Lcom/sec/chaton/settings/FragmentFontChange;->c:I

    .line 285
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v3

    if-ne v3, v9, :cond_6

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    if-eqz v0, :cond_a

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v7, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    move v0, v1

    .line 280
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 263
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 264
    :goto_3
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 265
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 267
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_4
    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_5

    .line 268
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 267
    :cond_5
    throw v0

    .line 292
    :cond_6
    iget-object v3, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    if-eqz v3, :cond_a

    .line 294
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    invoke-virtual {v1, v2, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 295
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 297
    if-nez v0, :cond_7

    .line 299
    const-string v0, "Can not make font file"

    const-string v1, "FragmentFontChange"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    goto :goto_2

    .line 301
    :cond_7
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 302
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 303
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    move v0, v7

    goto :goto_2

    .line 310
    :cond_8
    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    if-eqz v0, :cond_9

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v7, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 315
    :cond_9
    return-void

    .line 267
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 263
    :catch_1
    move-exception v0

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_2
.end method

.method static synthetic c(Lcom/sec/chaton/settings/FragmentFontChange;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 422
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->f:I

    .line 423
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->g:I

    .line 425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "1. windows size=width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 489
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Default Font Typeface"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->c:I

    .line 490
    iget v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->c:I

    if-ne v0, v3, :cond_0

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 492
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 500
    :goto_0
    return-void

    .line 494
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 496
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 497
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 498
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 380
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 381
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 383
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 127
    check-cast p1, Lcom/sec/chaton/settings/ActivityFontChange;

    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->l:Lcom/sec/chaton/settings/ActivityFontChange;

    .line 128
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 196
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 198
    new-instance v0, Lcom/sec/chaton/settings/dn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/dn;-><init>(Lcom/sec/chaton/settings/FragmentFontChange;)V

    .line 208
    const/4 v1, 0x5

    invoke-static {v1, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 210
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    .line 211
    new-instance v1, Lcom/sec/common/f/c;

    invoke-direct {v1, v0}, Lcom/sec/common/f/c;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->i:Lcom/sec/common/f/c;

    .line 213
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->b()V

    .line 214
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 218
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 219
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 220
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f07043e

    const v4, 0x7f07043c

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 319
    const v0, 0x7f030072

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    .line 322
    :try_start_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203e9

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 323
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 324
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 330
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->a()V

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    .line 339
    :cond_1
    new-instance v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/FontType;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->k:Lcom/sec/chaton/io/entry/inner/FontType;

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 342
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->k:Lcom/sec/chaton/io/entry/inner/FontType;

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->m:Ljava/util/Map;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Default Font Typeface"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/io/entry/inner/FontType;->setFontTitle(Ljava/lang/String;)V

    .line 344
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    .line 345
    new-instance v0, Lcom/sec/chaton/settings/dp;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/dp;-><init>(Lcom/sec/chaton/settings/FragmentFontChange;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->r:Lcom/sec/chaton/settings/dp;

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->r:Lcom/sec/chaton/settings/dp;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v6, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    .line 354
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/FragmentFontChange;->k:Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v3}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 355
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 357
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 358
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 361
    :try_start_1
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 362
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 363
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 364
    :catch_0
    move-exception v0

    .line 365
    const-string v0, "Cannot make font from file"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 325
    :catch_1
    move-exception v0

    .line 327
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 371
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->j:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 373
    invoke-virtual {p0, v5}, Lcom/sec/chaton/settings/FragmentFontChange;->setHasOptionsMenu(Z)V

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->h:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 416
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->i:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 418
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 442
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 443
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 460
    iput p3, p0, Lcom/sec/chaton/settings/FragmentFontChange;->q:I

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 486
    :goto_0
    return-void

    .line 467
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->s:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 469
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 471
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 472
    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->s:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 482
    :catch_0
    move-exception v0

    .line 483
    const-string v0, "Cannot make font from file"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 478
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->s:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 479
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->s:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 224
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a6

    if-ne v0, v1, :cond_3

    .line 225
    iget v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->q:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 226
    iget v1, p0, Lcom/sec/chaton/settings/FragmentFontChange;->c:I

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/chaton/settings/FragmentFontChange;->q:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    if-ne v1, v0, :cond_2

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->b:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00f5

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->b:Landroid/widget/Toast;

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 242
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 232
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Typeface"

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/chaton/settings/FragmentFontChange;->q:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 234
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Name"

    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->p:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/chaton/settings/FragmentFontChange;->q:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontType;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 236
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/do;

    invoke-interface {v0}, Lcom/sec/chaton/settings/do;->c()V

    goto :goto_0

    .line 239
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a5

    if-ne v0, v1, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/do;

    invoke-interface {v0}, Lcom/sec/chaton/settings/do;->c()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 411
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 412
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 401
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 402
    iget v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/settings/FragmentFontChange;->f:I

    if-nez v0, :cond_1

    .line 403
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->c()V

    .line 405
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->b()V

    .line 406
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->d()V

    .line 407
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 430
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 431
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 395
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 396
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentFontChange;->c()V

    .line 397
    return-void
.end method
