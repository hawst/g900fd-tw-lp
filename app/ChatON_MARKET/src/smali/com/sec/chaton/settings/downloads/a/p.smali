.class public final Lcom/sec/chaton/settings/downloads/a/p;
.super Ljava/lang/Object;
.source "InstallWorkerManager.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/settings/downloads/a/p;


# instance fields
.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/a/l;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/settings/downloads/a/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/chaton/settings/downloads/a/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/p;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->d:Ljava/util/List;

    .line 52
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/settings/downloads/a/p;
    .locals 2

    .prologue
    .line 38
    const-class v1, Lcom/sec/chaton/settings/downloads/a/p;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/a/p;->b:Lcom/sec/chaton/settings/downloads/a/p;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/p;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/a/p;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/p;->b:Lcom/sec/chaton/settings/downloads/a/p;

    .line 42
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/a/p;->b:Lcom/sec/chaton/settings/downloads/a/p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 149
    if-eqz p1, :cond_0

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/l;

    return-object v0
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78
    if-nez p1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->b()Lcom/sec/chaton/e/ar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->h()Lcom/sec/common/util/g;

    move-result-object v0

    sget-object v1, Lcom/sec/common/util/g;->c:Lcom/sec/common/util/g;

    if-ne v0, v1, :cond_2

    .line 87
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "InstallAsyncWorker status("

    aput-object v1, v0, v2

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->h()Lcom/sec/common/util/g;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, ") is invalid."

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/p;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/l;

    .line 97
    if-nez v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/q;

    .line 109
    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/downloads/a/q;->a(Lcom/sec/chaton/settings/downloads/a/l;)V

    goto :goto_2

    .line 100
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 104
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/l;

    move-object p1, v0

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/q;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    return-void
.end method

.method public a(Lcom/sec/chaton/e/ar;)Z
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/l;

    .line 194
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->b()Lcom/sec/chaton/e/ar;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/chaton/e/ar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 200
    :pswitch_1
    const/4 v0, 0x1

    .line 205
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 195
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Lcom/sec/chaton/e/ar;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 214
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/l;

    .line 217
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->b()Lcom/sec/chaton/e/ar;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/chaton/e/ar;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 218
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 220
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 224
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 225
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/util/a;

    .line 227
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/common/util/a;->h()Lcom/sec/common/util/g;

    move-result-object v2

    sget-object v3, Lcom/sec/common/util/g;->c:Lcom/sec/common/util/g;

    if-eq v2, v3, :cond_2

    .line 228
    invoke-virtual {v0, v6}, Lcom/sec/common/util/a;->a(Z)Z

    goto :goto_1

    .line 231
    :cond_3
    return-void
.end method

.method public b(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 121
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->b()Lcom/sec/chaton/e/ar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/q;

    .line 126
    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/downloads/a/q;->c(Lcom/sec/chaton/settings/downloads/a/l;)V

    goto :goto_0

    .line 130
    :cond_0
    return-void
.end method

.method public b(Lcom/sec/chaton/settings/downloads/a/q;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method public c(Lcom/sec/chaton/e/ar;)V
    .locals 6

    .prologue
    .line 241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/l;

    .line 244
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->b()Lcom/sec/chaton/e/ar;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/chaton/e/ar;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 245
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    .line 246
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 247
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 253
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/a/p;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/l;

    .line 255
    if-eqz v0, :cond_2

    .line 256
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/l;->a()V

    goto :goto_1

    .line 260
    :cond_3
    return-void
.end method

.method public c(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/q;

    .line 134
    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/downloads/a/q;->b(Lcom/sec/chaton/settings/downloads/a/l;)V

    goto :goto_0

    .line 138
    :cond_0
    return-void
.end method
