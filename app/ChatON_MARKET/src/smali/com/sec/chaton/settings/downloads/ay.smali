.class final enum Lcom/sec/chaton/settings/downloads/ay;
.super Ljava/lang/Enum;
.source "DownloadableCounter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/downloads/ay;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/downloads/ay;

.field public static final enum b:Lcom/sec/chaton/settings/downloads/ay;

.field public static final enum c:Lcom/sec/chaton/settings/downloads/ay;

.field private static final synthetic d:[Lcom/sec/chaton/settings/downloads/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/sec/chaton/settings/downloads/ay;

    const-string v1, "STATUS_NOT_INSTALLED"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    .line 16
    new-instance v0, Lcom/sec/chaton/settings/downloads/ay;

    const-string v1, "STATUS_INSTALL_PROGRESSING"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/settings/downloads/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    .line 17
    new-instance v0, Lcom/sec/chaton/settings/downloads/ay;

    const-string v1, "STATUS_INSTALLED"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/settings/downloads/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/settings/downloads/ay;

    sget-object v1, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/chaton/settings/downloads/ay;->d:[Lcom/sec/chaton/settings/downloads/ay;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/ay;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/chaton/settings/downloads/ay;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/ay;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/downloads/ay;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/chaton/settings/downloads/ay;->d:[Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/downloads/ay;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/downloads/ay;

    return-object v0
.end method
