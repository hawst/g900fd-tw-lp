.class public abstract Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;
.super Landroid/support/v4/app/Fragment;
.source "SnsFriendsPickerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/settings/tellfriends/common/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/chaton/settings/tellfriends/aa;",
        ">",
        "Landroid/support/v4/app/Fragment;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/sec/chaton/settings/tellfriends/common/g;"
    }
.end annotation


# instance fields
.field public a:Lcom/sec/chaton/settings/tellfriends/common/d;

.field public b:Landroid/os/Handler;

.field private c:[J

.field private d:Landroid/app/Activity;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ListView;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/view/ViewStub;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/app/ProgressDialog;

.field private o:Lcom/sec/common/a/d;

.field private p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private q:Lcom/sec/chaton/settings/tellfriends/aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private r:Lcom/sec/chaton/d/h;

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/sec/chaton/settings/tellfriends/ad;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->c:[J

    .line 69
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    .line 70
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->o:Lcom/sec/common/a/d;

    .line 76
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->s:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->t:Ljava/util/Map;

    .line 412
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/k;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/common/k;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->u:Lcom/sec/chaton/settings/tellfriends/ad;

    .line 450
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/l;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/common/l;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b:Landroid/os/Handler;

    .line 85
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 155
    const v0, 0x7f030105

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 157
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->f:Landroid/widget/ListView;

    .line 158
    const v0, 0x7f070462

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e:Landroid/view/View;

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e:Landroid/view/View;

    const v2, 0x7f0702d8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->g:Landroid/widget/Button;

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->g:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e:Landroid/view/View;

    const v2, 0x7f0702d9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->h:Landroid/widget/Button;

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->h:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    const v0, 0x7f07014d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 168
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 170
    const v0, 0x7f070461

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->i:Landroid/view/ViewStub;

    .line 172
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/d;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->t:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v2, v5, v3}, Lcom/sec/chaton/settings/tellfriends/common/d;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Lcom/sec/chaton/settings/tellfriends/common/g;)V

    .line 174
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/i;

    const v2, 0x7f0202cf

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->f()I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/i;-><init>(II)V

    invoke-virtual {v0, v6}, Lcom/sec/chaton/settings/tellfriends/common/i;->a(Z)Lcom/sec/chaton/settings/tellfriends/common/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/i;->a()Lcom/sec/chaton/settings/tellfriends/common/h;

    move-result-object v0

    .line 176
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Lcom/sec/chaton/settings/tellfriends/common/h;)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->f:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 179
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->o()V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setTitle(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->notifyDataSetInvalidated()V

    .line 185
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->setHasOptionsMenu(Z)V

    .line 187
    return-object v1
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 397
    const/16 v0, 0x3e83

    if-ne p1, v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    const v1, 0x7f0b00a9

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 407
    :goto_0
    return-void

    .line 399
    :cond_0
    const/16 v0, 0x3e84

    if-ne p1, v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    const v1, 0x7f0b00aa

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 401
    :cond_1
    const/16 v0, 0x3e85

    if-ne p1, v0, :cond_2

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    const v1, 0x7f0b00ab

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 404
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    const v1, 0x7f0b00be

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(I)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 360
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/j;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/j;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method private a([J)V
    .locals 3

    .prologue
    .line 367
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 368
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/tellfriends/aa;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;Ljava/util/ArrayList;)[J
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Ljava/util/ArrayList;)[J

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;[J)[J
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->c:[J

    return-object p1
.end method

.method private a(Ljava/util/ArrayList;)[J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 389
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 390
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    aput-wide v3, v2, v1

    .line 389
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 393
    :cond_0
    return-object v2
.end method

.method private b()I
    .locals 4

    .prologue
    const v0, 0x7f0b01f4

    .line 191
    const/4 v1, 0x0

    .line 192
    sget-object v2, Lcom/sec/chaton/settings/tellfriends/common/m;->a:[I

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e()Lcom/sec/chaton/settings/tellfriends/common/n;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/settings/tellfriends/common/n;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 207
    :goto_0
    :pswitch_0
    return v0

    .line 194
    :pswitch_1
    const v0, 0x7f0b01f2

    .line 195
    goto :goto_0

    .line 197
    :pswitch_2
    const v0, 0x7f0b01f3

    .line 198
    goto :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;[J)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a([J)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 335
    const/4 v1, -0x3

    if-ne v1, v0, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Ljava/lang/String;)V

    .line 343
    :goto_0
    return-void

    .line 337
    :cond_0
    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 338
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 341
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0229

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c()I
    .locals 4

    .prologue
    const v0, 0x7f0b01ff

    .line 211
    const/4 v1, 0x0

    .line 212
    sget-object v2, Lcom/sec/chaton/settings/tellfriends/common/m;->a:[I

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e()Lcom/sec/chaton/settings/tellfriends/common/n;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/settings/tellfriends/common/n;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 227
    :goto_0
    :pswitch_0
    return v0

    .line 217
    :pswitch_1
    const v0, 0x7f0b0200

    .line 218
    goto :goto_0

    .line 220
    :pswitch_2
    const v0, 0x7f0b0201

    .line 221
    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Lcom/sec/chaton/settings/tellfriends/common/n;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e()Lcom/sec/chaton/settings/tellfriends/common/n;

    move-result-object v0

    return-object v0
.end method

.method private d()Lcom/sec/chaton/settings/tellfriends/aa;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->p:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/aj;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aj;-><init>(Landroid/app/Activity;)V

    .line 243
    :goto_0
    return-object v0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->p:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/an;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/an;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->p:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 238
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ap;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ap;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->p:Ljava/lang/Class;

    const-class v1, Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 240
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/al;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/al;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 243
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method private e()Lcom/sec/chaton/settings/tellfriends/common/n;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/aj;

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->b:Lcom/sec/chaton/settings/tellfriends/common/n;

    .line 257
    :goto_0
    return-object v0

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/an;

    if-eqz v0, :cond_1

    .line 250
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->c:Lcom/sec/chaton/settings/tellfriends/common/n;

    goto :goto_0

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/ap;

    if-eqz v0, :cond_2

    .line 252
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->d:Lcom/sec/chaton/settings/tellfriends/common/n;

    goto :goto_0

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    instance-of v0, v0, Lcom/sec/chaton/settings/tellfriends/al;

    if-eqz v0, :cond_3

    .line 254
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->e:Lcom/sec/chaton/settings/tellfriends/common/n;

    goto :goto_0

    .line 257
    :cond_3
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->a:Lcom/sec/chaton/settings/tellfriends/common/n;

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)[J
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->c:[J

    return-object v0
.end method

.method private f()I
    .locals 3

    .prologue
    .line 261
    const/4 v0, 0x0

    .line 262
    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/m;->a:[I

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e()Lcom/sec/chaton/settings/tellfriends/common/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/settings/tellfriends/common/n;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 277
    :goto_0
    return v0

    .line 264
    :pswitch_0
    const v0, 0x7f0203f0

    .line 265
    goto :goto_0

    .line 267
    :pswitch_1
    const v0, 0x7f0203f2

    .line 268
    goto :goto_0

    .line 270
    :pswitch_2
    const v0, 0x7f0203f3

    .line 271
    goto :goto_0

    .line 273
    :pswitch_3
    const v0, 0x7f0203f1

    goto :goto_0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->i()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->h()V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->u:Lcom/sec/chaton/settings/tellfriends/ad;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aa;->a(Lcom/sec/chaton/settings/tellfriends/ad;)V

    .line 284
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->k()V

    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Z)V

    .line 288
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->m()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    .line 295
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->r:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->o:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 323
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->o:Lcom/sec/common/a/d;

    .line 325
    :cond_0
    return-void
.end method

.method private k()V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->i()V

    .line 329
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->j()V

    .line 330
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l()V

    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->t:Ljava/util/Map;

    return-object v0
.end method

.method private l()V
    .locals 3

    .prologue
    const v2, 0x7f0b002a

    .line 347
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->k()V

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 349
    const/4 v1, -0x3

    if-ne v1, v0, :cond_0

    .line 350
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Ljava/lang/String;)V

    .line 356
    :goto_0
    return-void

    .line 351
    :cond_0
    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 352
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Z)V

    .line 364
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 372
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/m;->a:[I

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e()Lcom/sec/chaton/settings/tellfriends/common/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/common/n;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 382
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 374
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->a:Ljava/lang/String;

    goto :goto_0

    .line 376
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->b:Ljava/lang/String;

    goto :goto_0

    .line 378
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->c:Ljava/lang/String;

    goto :goto_0

    .line 380
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->d:Ljava/lang/String;

    goto :goto_0

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private o()V
    .locals 3

    .prologue
    .line 541
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->i:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->j:Landroid/view/View;

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->j:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->k:Landroid/widget/ImageView;

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->k:Landroid/widget/ImageView;

    const v1, 0x7f02034b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 545
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->j:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l:Landroid/widget/TextView;

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 547
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->j:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->m:Landroid/widget/TextView;

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ae

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->i:Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 551
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ai;)V
    .locals 4

    .prologue
    .line 528
    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/ai;->d()Ljava/lang/String;

    move-result-object v0

    .line 529
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->h()V

    .line 531
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0041

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 532
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 533
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->r:Lcom/sec/chaton/d/h;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 538
    :goto_0
    return-void

    .line 535
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->k()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 113
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    .line 114
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->p:Ljava/lang/Class;

    .line 108
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d()Lcom/sec/chaton/settings/tellfriends/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    .line 95
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->r:Lcom/sec/chaton/d/h;

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->g()V

    .line 101
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->k()V

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->q:Lcom/sec/chaton/settings/tellfriends/aa;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aa;->a()V

    .line 123
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 124
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    .line 130
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 146
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 148
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 136
    return-void
.end method
