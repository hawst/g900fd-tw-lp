.class Lcom/sec/chaton/settings/downloads/da;
.super Ljava/lang/Object;
.source "SoundDownloads.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/SoundDownloads;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/da;->a:Lcom/sec/chaton/settings/downloads/SoundDownloads;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 204
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 206
    if-nez v0, :cond_1

    .line 207
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 208
    const-string v0, "SoundDownloads.onItemLongClick cursor is null."

    invoke-static {}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    :goto_0
    return v1

    .line 214
    :cond_1
    const-string v3, "item_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 215
    const-string v4, "name"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 216
    const-string v5, "install"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 217
    if-nez v0, :cond_3

    move v0, v1

    .line 219
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 223
    if-eqz v0, :cond_2

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/da;->a:Lcom/sec/chaton/settings/downloads/SoundDownloads;

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a(Lcom/sec/chaton/settings/downloads/SoundDownloads;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v1, v2

    .line 227
    goto :goto_0

    :cond_3
    move v0, v2

    .line 217
    goto :goto_1
.end method
