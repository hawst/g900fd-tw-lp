.class public Lcom/sec/chaton/settings/downloads/FontDetail;
.super Landroid/support/v4/app/Fragment;
.source "FontDetail.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/common/f/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/sec/common/f/f;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private A:Lcom/sec/chaton/settings/downloads/a/i;

.field private B:Landroid/database/Cursor;

.field private C:Z

.field private D:Landroid/view/View;

.field private E:Landroid/view/Menu;

.field private F:Landroid/os/Handler;

.field a:Ljava/text/DateFormat;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Z

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Landroid/widget/ImageView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/ImageView;

.field private u:Landroid/widget/ImageView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/ProgressBar;

.field private x:Lcom/sec/chaton/d/k;

.field private y:Lcom/sec/common/f/c;

.field private z:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/FontDetail;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 80
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->g:J

    .line 114
    new-instance v0, Lcom/sec/chaton/settings/downloads/ba;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/ba;-><init>(Lcom/sec/chaton/settings/downloads/FontDetail;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->F:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/FontDetail;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->B:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/FontDetail;Lcom/sec/chaton/settings/downloads/a/i;)Lcom/sec/chaton/settings/downloads/a/i;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/16 v9, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 428
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 429
    const-string v0, "download_font, show detail, redraw()"

    sget-object v3, Lcom/sec/chaton/settings/downloads/FontDetail;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :cond_0
    const-wide/16 v3, 0x0

    .line 435
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 436
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 439
    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 440
    const-string v0, "extras"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 441
    const/4 v0, 0x0

    .line 444
    :try_start_0
    invoke-static {v4}, Lcom/sec/chaton/e/a/k;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/l;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 451
    :cond_1
    :goto_0
    iput-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->d:Ljava/lang/String;

    .line 452
    if-eqz v0, :cond_2

    .line 453
    iget-object v3, v0, Lcom/sec/chaton/e/a/l;->c:Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->e:Ljava/lang/String;

    .line 454
    iget-object v3, v0, Lcom/sec/chaton/e/a/l;->b:Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->f:Ljava/lang/String;

    .line 455
    iget-wide v3, v0, Lcom/sec/chaton/e/a/l;->d:J

    iput-wide v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->g:J

    .line 456
    iget-object v0, v0, Lcom/sec/chaton/e/a/l;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->k:Ljava/lang/String;

    .line 458
    :cond_2
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 459
    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->h:Z

    .line 460
    const-string v0, "new"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->i:I

    .line 461
    const-string v0, "special"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->j:I

    .line 465
    const-string v0, "expiration_time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 468
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->q:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->d:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 473
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->h:Z

    if-eqz v0, :cond_8

    .line 475
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    const v3, 0x7f0200ab

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    const v3, 0x7f0201cb

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 485
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v4, "KB) / "

    aput-object v4, v3, v10

    const v4, 0x7f0b022c

    invoke-virtual {p0, v4}, Lcom/sec/chaton/settings/downloads/FontDetail;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v11

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/bj;->b(Landroid/widget/TextView;)V

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    :goto_3
    iget v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->j:I

    if-eqz v0, :cond_10

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 573
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    const v1, 0x7f0b01dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 574
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    const-string v1, "#3eb1b9"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 584
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 585
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->f:Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->l:I

    iget v4, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->m:I

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;II)V

    .line 586
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->y:Lcom/sec/common/f/c;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->p:Landroid/widget/ImageView;

    invoke-virtual {v1, v3, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 589
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 590
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->e:Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->n:I

    iget v4, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->o:I

    invoke-direct {v0, v1, v3, v4, v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;IIZ)V

    .line 591
    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a(I)V

    .line 592
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->y:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 595
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->D:Landroid/view/View;

    const v1, 0x7f070317

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    .line 596
    return-void

    .line 445
    :catch_0
    move-exception v4

    .line 446
    sget-boolean v5, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v5, :cond_1

    .line 447
    sget-object v5, Lcom/sec/chaton/settings/downloads/FontDetail;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 459
    goto/16 :goto_1

    .line 483
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_2

    .line 492
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    if-nez v0, :cond_9

    .line 498
    new-instance v0, Ljava/sql/Date;

    invoke-direct {v0, v3, v4}, Ljava/sql/Date;-><init>(J)V

    .line 499
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 501
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    const v4, 0x7f0200ac

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 502
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 504
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, " ("

    aput-object v5, v4, v1

    iget-wide v5, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->g:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    const-string v5, "KB)"

    aput-object v5, v4, v10

    const-string v5, " / "

    aput-object v5, v4, v11

    const/4 v5, 0x4

    const-string v6, "~"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/bj;->a(Landroid/widget/TextView;)V

    .line 506
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 508
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v0

    const/4 v5, 0x5

    if-ne v0, v5, :cond_a

    .line 509
    new-instance v0, Ljava/sql/Date;

    invoke-direct {v0, v3, v4}, Ljava/sql/Date;-><init>(J)V

    .line 510
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 512
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    const v4, 0x7f0200ad

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 513
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 515
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, " ("

    aput-object v5, v4, v1

    iget-wide v5, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->g:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    const-string v5, "KB)"

    aput-object v5, v4, v10

    const-string v5, " / "

    aput-object v5, v4, v11

    const/4 v5, 0x4

    const-string v6, "~"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/bj;->a(Landroid/widget/TextView;)V

    .line 517
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 520
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_3

    .line 523
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 526
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-nez v0, :cond_b

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 530
    :cond_b
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_c

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    new-array v3, v11, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    const v4, 0x7f0b022a

    invoke-virtual {p0, v4}, Lcom/sec/chaton/settings/downloads/FontDetail;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v4, ")"

    aput-object v4, v3, v10

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/widget/TextView;)V

    .line 534
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 539
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 542
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 545
    :cond_d
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 547
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_e

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    new-array v3, v11, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v4, "KB)"

    aput-object v4, v3, v10

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/widget/TextView;)V

    .line 551
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 556
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 557
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 559
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_f

    .line 560
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    const v3, 0x7f0b022b

    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/downloads/FontDetail;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/bj;->d(Landroid/widget/TextView;)V

    .line 563
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 575
    :cond_10
    iget v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->i:I

    if-eqz v0, :cond_11

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 578
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    const v1, 0x7f0b01dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 579
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    const-string v1, "#e86d00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto/16 :goto_4

    .line 581
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 521
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/FontDetail;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/FontDetail;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/chaton/settings/downloads/FontDetail;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/FontDetail;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/high16 v4, 0x41a00000    # 20.0f

    .line 398
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 399
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 402
    instance-of v0, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-nez v0, :cond_1

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 406
    if-eqz v0, :cond_0

    .line 410
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 412
    if-eqz v0, :cond_0

    .line 415
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 418
    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float/2addr v0, v2

    invoke-static {v4}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 420
    float-to-int v0, v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->h:Z

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 284
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/bb;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/bb;-><init>(Lcom/sec/chaton/settings/downloads/FontDetail;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 352
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->B:Landroid/database/Cursor;

    .line 355
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->C:Z

    if-eqz v0, :cond_0

    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->C:Z

    .line 359
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/i;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->a(Landroid/os/Handler;)V

    .line 366
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Landroid/database/Cursor;)V

    .line 367
    return-void
.end method

.method public a(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 376
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    const v1, 0x7f02035d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 379
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 384
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    :goto_0
    return-void

    .line 300
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 311
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->h:Z

    if-eqz v0, :cond_2

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/bj;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 314
    if-eqz v0, :cond_1

    .line 315
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->g(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 316
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 339
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->B:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 318
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->h(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    if-nez v0, :cond_3

    .line 322
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->x:Lcom/sec/chaton/d/k;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/i;-><init>(Lcom/sec/chaton/d/k;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->a(Landroid/os/Handler;)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 325
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 326
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->x:Lcom/sec/chaton/d/k;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/i;-><init>(Lcom/sec/chaton/d/k;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->a(Landroid/os/Handler;)V

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 330
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 334
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->a(Z)Z

    goto :goto_1

    .line 300
    :pswitch_data_0
    .packed-switch 0x7f0701d2
        :pswitch_0
    .end packed-switch

    .line 330
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 389
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 391
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->c()V

    .line 392
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 154
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ITEM_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 347
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->c:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const v1, 0x7f0705ab

    .line 218
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 220
    const v0, 0x7f0f0027

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 222
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->E:Landroid/view/Menu;

    .line 224
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->h:Z

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->E:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 229
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->E:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 168
    const v0, 0x7f0300a7

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 170
    iput-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->D:Landroid/view/View;

    .line 172
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->a:Ljava/text/DateFormat;

    .line 174
    new-instance v0, Lcom/sec/chaton/d/k;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2, v5}, Lcom/sec/chaton/d/k;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->x:Lcom/sec/chaton/d/k;

    .line 175
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->y:Lcom/sec/common/f/c;

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->y:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0901d1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->l:I

    .line 180
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0901d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->m:I

    .line 182
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09027b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->n:I

    .line 183
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09027c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->o:I

    .line 185
    const v0, 0x7f0701d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->p:Landroid/widget/ImageView;

    .line 187
    const v0, 0x7f0701d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->q:Landroid/widget/TextView;

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->q:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 191
    const v0, 0x7f0701d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->r:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 195
    const v0, 0x7f070316

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->s:Landroid/widget/TextView;

    .line 199
    const v0, 0x7f0701d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    const v0, 0x7f070317

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->u:Landroid/widget/ImageView;

    .line 203
    const v0, 0x7f0701d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->v:Landroid/widget/TextView;

    .line 204
    const v0, 0x7f0701d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->w:Landroid/widget/ProgressBar;

    .line 207
    iput-boolean v3, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->C:Z

    .line 208
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 210
    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/downloads/FontDetail;->setHasOptionsMenu(Z)V

    .line 212
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 259
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->A:Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->b(Landroid/os/Handler;)V

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->z:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->z:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->z:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->y:Lcom/sec/common/f/c;

    if-eqz v0, :cond_2

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->y:Lcom/sec/common/f/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/FontDetail;->y:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 275
    :cond_2
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 70
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 372
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 233
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 239
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 236
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->a()V

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x7f0705ab
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 252
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 254
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->stopLoading()V

    .line 255
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 245
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 247
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->startLoading()V

    .line 248
    return-void
.end method
