.class Lcom/sec/chaton/settings/downloads/i;
.super Landroid/os/Handler;
.source "AmsItemDownloads.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 129
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x4b0

    if-ne v0, v3, :cond_2

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Lcom/sec/chaton/d/a/l;)Lcom/sec/chaton/d/a/l;

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    .line 141
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 143
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v3, :cond_b

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Z)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-virtual {v0, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 153
    :cond_2
    :goto_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x4b1

    if-ne v0, v3, :cond_3

    .line 154
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 156
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v3, v4}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Lcom/sec/chaton/d/a/j;)Lcom/sec/chaton/d/a/j;

    .line 158
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v3, v4, :cond_d

    .line 159
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/AmsItemDownloadEntry;

    .line 162
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/AmsItemDownloadEntry;->imageurl:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Ljava/lang/String;)Ljava/lang/String;

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 169
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v4}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->d(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 172
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v4}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v5}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->d(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v3, v6}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_3
    :goto_2
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_4

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 195
    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_7

    .line 196
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 197
    const-string v0, "download_ams, Download is done."

    invoke-static {}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/common/util/a/g;

    .line 201
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-virtual {v0}, Lcom/sec/common/util/a/g;->b()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Ljava/lang/String;)Ljava/lang/String;

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/d/e;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    if-ne v0, v3, :cond_e

    .line 205
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivityAmsItemBackgroundDetail;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    const-string v1, "filePath"

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    const-string v1, "amsItemId"

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->startActivity(Landroid/content/Intent;)V

    .line 236
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    .line 239
    :cond_7
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_9

    .line 240
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 241
    const-string v0, "download_ams, Download is canceled."

    invoke-static {}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    .line 247
    :cond_9
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 248
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_a

    .line 249
    const-string v0, "download_ams, Download is failed."

    invoke-static {}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    goto/16 :goto_0

    .line 147
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0b00b3

    invoke-static {v3}, Lcom/sec/chaton/settings/downloads/av;->a(I)I

    move-result v3

    invoke-static {v0, v3, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_c

    .line 175
    invoke-static {}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 178
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    goto/16 :goto_2

    .line 183
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    goto/16 :goto_2

    .line 209
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/d/e;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    if-ne v0, v3, :cond_6

    .line 210
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/app/Activity;

    move-result-object v0

    const-class v4, Lcom/sec/vip/amschaton/AMSPlayerActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 211
    const-string v0, "AMS_FILE_PATH"

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v4}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const-string v0, "VIEWER_MODE"

    const/16 v4, 0x3ea

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 213
    const-string v0, "AMS_DOWNLOAD_PREVIEW"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 214
    const-string v0, "AMS_FROM_DOWNLOADS"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 215
    const-string v0, "AMS_ITEM_ID"

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v4}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->l(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/settings/downloads/a;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v4}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/chaton/settings/downloads/a;->a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/inner/AmsItem;

    move-result-object v4

    .line 219
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v5}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/d/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-static {v6}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/a;

    .line 220
    if-eqz v0, :cond_10

    .line 221
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v0

    const/4 v5, 0x5

    if-ne v0, v5, :cond_10

    move v0, v1

    .line 227
    :goto_4
    if-eqz v4, :cond_f

    .line 228
    const-string v1, "AMS_FILE_SIZE"

    iget-object v5, v4, Lcom/sec/chaton/io/entry/inner/AmsItem;->filesize:Ljava/lang/Long;

    invoke-virtual {v3, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 229
    const-string v1, "AMS_EXPIRATION_DATE"

    iget-object v4, v4, Lcom/sec/chaton/io/entry/inner/AmsItem;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 230
    const-string v1, "AMS_IS_FAILED_ITEM"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 232
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/i;->a:Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-virtual {v0, v3, v2}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_3

    :cond_10
    move v0, v2

    goto :goto_4
.end method
