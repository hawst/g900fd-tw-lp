.class public Lcom/sec/chaton/settings/tellfriends/ax;
.super Landroid/os/AsyncTask;
.source "TwDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ltwitter4j/auth/AccessToken;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/ar;


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ltwitter4j/auth/AccessToken;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 382
    const-string v0, "Called. OauthAccessTokenTask - doInBackground()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 386
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/ar;->i(Lcom/sec/chaton/settings/tellfriends/ar;)Ltwitter4j/Twitter;

    move-result-object v1

    invoke-interface {v1, v0}, Ltwitter4j/Twitter;->getOAuthAccessToken(Ljava/lang/String;)Ltwitter4j/auth/AccessToken;
    :try_end_0
    .catch Ltwitter4j/TwitterException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 387
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/ar;->i(Lcom/sec/chaton/settings/tellfriends/ar;)Ltwitter4j/Twitter;

    move-result-object v1

    invoke-interface {v1, v0}, Ltwitter4j/Twitter;->setOAuthAccessToken(Ltwitter4j/auth/AccessToken;)V
    :try_end_1
    .catch Ltwitter4j/TwitterException; {:try_start_1 .. :try_end_1} :catch_1

    .line 393
    :goto_0
    return-object v0

    .line 388
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 389
    :goto_1
    invoke-virtual {v1}, Ltwitter4j/TwitterException;->printStackTrace()V

    .line 390
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/ar;->b(Lcom/sec/chaton/settings/tellfriends/ar;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-virtual {v1}, Ltwitter4j/TwitterException;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    const/16 v5, -0x3e9

    invoke-direct {v4, v1, v5, v2}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    .line 391
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/ar;->j(Lcom/sec/chaton/settings/tellfriends/ar;)V

    goto :goto_0

    .line 388
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method protected a(Ltwitter4j/auth/AccessToken;)V
    .locals 2

    .prologue
    .line 398
    const-string v0, "Called. OauthAccessTokenTask - onPostExecute()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->h(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->h(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->b(Lcom/sec/chaton/settings/tellfriends/ar;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Ljava/lang/Object;)V

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->dismiss()V

    .line 411
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 361
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ax;->a([Ljava/lang/String;)Ltwitter4j/auth/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 361
    check-cast p1, Ltwitter4j/auth/AccessToken;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ax;->a(Ltwitter4j/auth/AccessToken;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 364
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->d(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->d(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->h(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/util/Timer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ar;->a(Lcom/sec/chaton/settings/tellfriends/ar;Ljava/util/Timer;)Ljava/util/Timer;

    .line 377
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->h(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/aw;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/aw;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;Lcom/sec/chaton/settings/tellfriends/as;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 378
    return-void

    .line 371
    :cond_1
    const-string v0, "mTimer.cancel()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->h(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/tellfriends/ar;->a(Lcom/sec/chaton/settings/tellfriends/ar;Ljava/util/Timer;)Ljava/util/Timer;

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ax;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ar;->a(Lcom/sec/chaton/settings/tellfriends/ar;Ljava/util/Timer;)Ljava/util/Timer;

    goto :goto_0
.end method
