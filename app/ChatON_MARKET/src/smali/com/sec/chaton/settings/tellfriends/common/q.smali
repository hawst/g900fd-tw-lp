.class public Lcom/sec/chaton/settings/tellfriends/common/q;
.super Landroid/os/AsyncTask;
.source "SnsManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/chaton/settings/tellfriends/common/p;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/common/o;

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V
    .locals 1

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 62
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->d:Ljava/lang/Boolean;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->e:Ljava/util/Timer;

    .line 65
    iput p2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/q;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/q;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->d:Ljava/lang/Boolean;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 133
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/common/r;-><init>(Lcom/sec/chaton/settings/tellfriends/common/q;)V

    .line 142
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/q;->b()V

    .line 143
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    const-string v2, "startTimeOutCheckTask()"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/o;->c(Ljava/lang/String;)V

    .line 144
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->e:Ljava/util/Timer;

    .line 145
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->e:Ljava/util/Timer;

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 146
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->e:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    const-string v1, "cancelTimeOutCheckTask()"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/o;->c(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->e:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->e:Ljava/util/Timer;

    .line 154
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs a([Lcom/sec/chaton/settings/tellfriends/common/p;)Ljava/lang/Integer;
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 70
    .line 71
    const/4 v1, 0x0

    .line 73
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 74
    const/4 v2, -0x3

    if-eq v2, v0, :cond_0

    const/4 v2, -0x2

    if-ne v2, v0, :cond_1

    .line 75
    :cond_0
    const/16 v0, -0x3ee

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 77
    :cond_1
    const/4 v0, 0x0

    move v2, v3

    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_2

    .line 78
    aget-object v1, p1, v0

    .line 79
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Request:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Task : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\t["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/settings/tellfriends/common/o;->b(Ljava/lang/String;)V

    .line 80
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/q;->a()V

    .line 81
    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/common/p;->a()I

    move-result v2

    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/q;->b()V

    .line 84
    if-eq v2, v3, :cond_3

    .line 85
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\t["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v4, p1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/chaton/settings/tellfriends/common/o;->b(Ljava/lang/String;)V

    .line 89
    :cond_2
    if-nez v1, :cond_4

    .line 90
    const/16 v0, -0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 77
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 92
    :cond_4
    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/common/p;->a(Lcom/sec/chaton/settings/tellfriends/common/p;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->c:Ljava/lang/Object;

    .line 93
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/o;->a(Lcom/sec/chaton/settings/tellfriends/common/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 115
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, -0x3e8

    if-gt v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Error. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->c:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/o;->c(Ljava/lang/String;)V

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/o;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 121
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Done. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->c:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/o;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 59
    check-cast p1, [Lcom/sec/chaton/settings/tellfriends/common/p;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;->a([Lcom/sec/chaton/settings/tellfriends/common/p;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 5

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/q;->b()V

    .line 100
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->d:Ljava/lang/Boolean;

    monitor-enter v1

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    const-string v2, "onError()"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/tellfriends/common/o;->c(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/o;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    const/16 v3, -0x3e8

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 108
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->d:Ljava/lang/Boolean;

    .line 109
    monitor-exit v1

    .line 110
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Request:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Canceled."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/tellfriends/common/o;->b(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/o;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 59
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Start."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/o;->b(Ljava/lang/String;)V

    .line 126
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/q;->a()V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/q;->a:Lcom/sec/chaton/settings/tellfriends/common/o;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/o;->a(Lcom/sec/chaton/settings/tellfriends/common/o;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    return-void
.end method
