.class Lcom/sec/chaton/settings/s;
.super Ljava/lang/Object;
.source "ActivityChat.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityChat;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityChat;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/chaton/settings/s;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 481
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/settings/s;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityChat;->e(Lcom/sec/chaton/settings/ActivityChat;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 484
    packed-switch p3, :pswitch_data_0

    .line 515
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 516
    const-string v0, "Delete old failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 487
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/s;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityChat;->a(Lcom/sec/chaton/settings/ActivityChat;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0319

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 488
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 489
    const-string v0, "Delete old Chat Rooms"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 497
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/s;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityChat;->a(Lcom/sec/chaton/settings/ActivityChat;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b031b

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 498
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 499
    const-string v0, "No old Chatroom"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 506
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/s;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityChat;->a(Lcom/sec/chaton/settings/ActivityChat;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b031a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 507
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 508
    const-string v0, "No old Chatroom"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 484
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 470
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 464
    return-void
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 476
    return-void
.end method
