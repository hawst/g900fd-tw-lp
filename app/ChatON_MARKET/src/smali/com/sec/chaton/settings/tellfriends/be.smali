.class Lcom/sec/chaton/settings/tellfriends/be;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "TwitterManager.java"


# instance fields
.field a:Ltwitter4j/auth/AccessToken;

.field final synthetic b:Lcom/sec/chaton/settings/tellfriends/ba;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;Ltwitter4j/auth/AccessToken;)V
    .locals 1

    .prologue
    .line 331
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/be;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    .line 332
    const-string v0, "GetMyInfoTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 333
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/be;->a:Ltwitter4j/auth/AccessToken;

    .line 334
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;Ltwitter4j/auth/AccessToken;Lcom/sec/chaton/settings/tellfriends/bb;)V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/tellfriends/be;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;Ltwitter4j/auth/AccessToken;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 5

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/be;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    const-string v1, "GetMyInfoTask() : Get my name"

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->b(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V

    .line 339
    const/4 v0, 0x0

    .line 341
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->g()Ltwitter4j/Twitter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/be;->a:Ltwitter4j/auth/AccessToken;

    invoke-virtual {v2}, Ltwitter4j/auth/AccessToken;->getUserId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Ltwitter4j/Twitter;->showUser(J)Ltwitter4j/User;

    move-result-object v1

    .line 343
    if-eqz v1, :cond_0

    .line 344
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/be;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/be;->a:Ltwitter4j/auth/AccessToken;

    invoke-virtual {v3}, Ltwitter4j/auth/AccessToken;->getUserId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/settings/tellfriends/bj;->b(Landroid/content/Context;Ljava/lang/String;)Z

    .line 345
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/be;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-interface {v1}, Ltwitter4j/User;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/settings/tellfriends/bj;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 346
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/be;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-interface {v1}, Ltwitter4j/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/settings/tellfriends/bj;->c(Landroid/content/Context;Ljava/lang/String;)Z

    .line 349
    :cond_0
    new-instance v1, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/be;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/ba;->a(Lcom/sec/chaton/settings/tellfriends/ba;)Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 350
    sget-object v2, Lcom/sec/chaton/settings/tellfriends/y;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/be;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v3, v3, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/bj;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/be;->e:I
    :try_end_0
    .catch Ltwitter4j/TwitterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :goto_0
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/be;->f:Ljava/lang/Object;

    .line 360
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/be;->e:I

    return v0

    .line 352
    :catch_0
    move-exception v0

    .line 353
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->printStackTrace()V

    .line 354
    const/16 v1, -0x3e9

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/be;->e:I

    .line 355
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
