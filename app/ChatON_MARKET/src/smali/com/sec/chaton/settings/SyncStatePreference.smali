.class public Lcom/sec/chaton/settings/SyncStatePreference;
.super Landroid/preference/Preference;
.source "SyncStatePreference.java"


# instance fields
.field private a:Z

.field private b:Ljava/lang/Object;

.field private final c:Landroid/os/Handler;

.field private d:Ljava/lang/Runnable;

.field private e:Landroid/content/SyncStatusObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->a:Z

    .line 96
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->c:Landroid/os/Handler;

    .line 98
    new-instance v0, Lcom/sec/chaton/settings/fm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/fm;-><init>(Lcom/sec/chaton/settings/SyncStatePreference;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->d:Ljava/lang/Runnable;

    .line 105
    new-instance v0, Lcom/sec/chaton/settings/fn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/fn;-><init>(Lcom/sec/chaton/settings/SyncStatePreference;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->e:Landroid/content/SyncStatusObserver;

    .line 36
    const v0, 0x7f030143

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SyncStatePreference;->setWidgetLayoutResource(I)V

    .line 37
    new-instance v0, Lcom/sec/chaton/settings/fk;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/settings/fk;-><init>(Lcom/sec/chaton/settings/SyncStatePreference;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SyncStatePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/SyncStatePreference;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/settings/SyncStatePreference;->e()V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->a:Z

    if-eq v0, p1, :cond_0

    .line 87
    iput-boolean p1, p0, Lcom/sec/chaton/settings/SyncStatePreference;->a:Z

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SyncStatePreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 90
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SyncStatePreference;->setEnabled(Z)V

    .line 92
    :cond_0
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/SyncStatePreference;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->d:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/SyncStatePreference;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->c:Landroid/os/Handler;

    return-object v0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 127
    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSync()Landroid/content/SyncInfo;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    const-string v1, "com.sec.chaton"

    iget-object v0, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 134
    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v3

    .line 137
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    move v1, v0

    move v2, v0

    .line 139
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 140
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncInfo;

    iget-object v0, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const-string v4, "com.sec.chaton"

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncInfo;

    iget-object v0, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v2, 0x1

    .line 143
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSyncing return value = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "SyncStatePreference"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 146
    :cond_2
    return v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 152
    .line 154
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/sec/chaton/settings/SyncStatePreference;->d()Z

    move-result v0

    .line 159
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSyncStateUpdated "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SyncStatePreference"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/SyncStatePreference;->a(Z)V

    .line 161
    return-void

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/SyncStatePreference;->c()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SyncStatePreference;->b()V

    .line 114
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sec/chaton/settings/SyncStatePreference;->e:Landroid/content/SyncStatusObserver;

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->b:Ljava/lang/Object;

    .line 115
    invoke-direct {p0}, Lcom/sec/chaton/settings/SyncStatePreference;->e()V

    .line 116
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/settings/SyncStatePreference;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->b:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/SyncStatePreference;->b:Ljava/lang/Object;

    .line 124
    :cond_0
    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 59
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 60
    const v0, 0x7f070514

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 61
    const v1, 0x7f070515

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 62
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    .line 63
    iget-boolean v3, p0, Lcom/sec/chaton/settings/SyncStatePreference;->a:Z

    if-eqz v3, :cond_0

    .line 64
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 65
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 66
    new-instance v2, Lcom/sec/chaton/settings/fl;

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/settings/fl;-><init>(Lcom/sec/chaton/settings/SyncStatePreference;Landroid/graphics/drawable/AnimationDrawable;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 77
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 75
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto :goto_0
.end method
