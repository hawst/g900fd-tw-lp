.class public Lcom/sec/chaton/settings/downloads/av;
.super Ljava/lang/Object;
.source "DownloadHelper.java"


# direct methods
.method public static a()I
    .locals 2

    .prologue
    .line 104
    invoke-static {}, Lcom/sec/chaton/settings/downloads/u;->a()I

    move-result v0

    .line 105
    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->e()I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    sget-object v1, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    sget-object v1, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    sget-object v1, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    invoke-static {}, Lcom/sec/chaton/settings/downloads/dg;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    return v0
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 29
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 31
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 32
    :cond_0
    const p0, 0x7f0b0205

    .line 34
    :cond_1
    return p0
.end method

.method public static a(Landroid/content/Context;)Landroid/app/ProgressDialog;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 39
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 40
    const v1, 0x7f0b000d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 42
    return-object v0
.end method

.method public static b(Landroid/content/Context;)Lcom/sec/chaton/widget/c;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/sec/chaton/widget/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/c;-><init>(Landroid/content/Context;)V

    .line 47
    const v1, 0x7f0b0021

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setTitle(Ljava/lang/CharSequence;)V

    .line 48
    const v1, 0x7f0b01ce

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setMessage(Ljava/lang/CharSequence;)V

    .line 50
    return-object v0
.end method

.method public static c(Landroid/content/Context;)Lcom/sec/common/a/a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 56
    const v1, 0x7f0b016c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 57
    const v1, 0x7f0b01cf

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 58
    const v1, 0x7f0b0024

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 59
    const v1, 0x7f0b0039

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 61
    return-object v0
.end method

.method public static d(Landroid/content/Context;)Lcom/sec/common/a/a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01da

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0231

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;)Lcom/sec/common/a/a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0226

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b025a

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    return-object v0
.end method

.method public static f(Landroid/content/Context;)Lcom/sec/common/a/a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0258

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0259

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    return-object v0
.end method

.method public static g(Landroid/content/Context;)Landroid/widget/Toast;
    .locals 2

    .prologue
    .line 94
    const v0, 0x7f0b02a3

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    return-object v0
.end method

.method public static h(Landroid/content/Context;)Landroid/widget/Toast;
    .locals 2

    .prologue
    .line 98
    const v0, 0x7f0b02a4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    return-object v0
.end method
