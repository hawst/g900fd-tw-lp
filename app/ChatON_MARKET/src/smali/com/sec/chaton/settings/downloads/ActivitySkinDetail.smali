.class public Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivitySkinDetail.java"

# interfaces
.implements Lcom/sec/chaton/settings/downloads/cc;


# instance fields
.field private a:Lcom/sec/chaton/settings/downloads/SkinDetail;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 88
    const-string v0, "startPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 91
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;->startActivity(Landroid/content/Intent;)V

    .line 97
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/sec/chaton/settings/downloads/SkinDetail;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/SkinDetail;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    .line 47
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;->finish()V

    .line 103
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 81
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 85
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 69
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 72
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;->d()V

    .line 73
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 55
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 62
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 58
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;->finish()V

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
