.class public Lcom/sec/chaton/settings/downloads/az;
.super Ljava/lang/Object;
.source "EmbededSoundFileDataInfo.java"


# static fields
.field public static a:Ljava/lang/String;

.field private static t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/az;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:Ljava/lang/Integer;

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I

.field public final h:J

.field public final i:J

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Long;

.field public o:Ljava/lang/String;

.field public final p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field r:I

.field public s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/chaton/settings/downloads/az;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/az;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput v1, p0, Lcom/sec/chaton/settings/downloads/az;->b:I

    .line 48
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->c:Ljava/lang/Integer;

    .line 49
    iput v1, p0, Lcom/sec/chaton/settings/downloads/az;->d:I

    .line 50
    sget-object v0, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->e:Ljava/lang/String;

    .line 51
    iput v2, p0, Lcom/sec/chaton/settings/downloads/az;->f:I

    .line 52
    iput v2, p0, Lcom/sec/chaton/settings/downloads/az;->g:I

    .line 53
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/sec/chaton/settings/downloads/az;->h:J

    .line 55
    iput-wide v3, p0, Lcom/sec/chaton/settings/downloads/az;->i:J

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->j:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->k:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->l:Ljava/lang/String;

    .line 61
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->n:Ljava/lang/Long;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->p:Ljava/lang/String;

    .line 71
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/az;->m:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/az;->o:Ljava/lang/String;

    .line 73
    iput p3, p0, Lcom/sec/chaton/settings/downloads/az;->r:I

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->s:Ljava/lang/String;

    .line 76
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 78
    :try_start_0
    const-string v1, "filesize"

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/az;->n:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 79
    const-string v1, "samplefileurl"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 80
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/az;->q:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/az;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    sget-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    .line 32
    sget-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    const-string v1, "-1"

    new-instance v2, Lcom/sec/chaton/settings/downloads/az;

    const-string v3, "-1"

    const-string v4, "ChatON melody"

    const/high16 v5, 0x7f060000

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/settings/downloads/az;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    const-string v1, "-2"

    new-instance v2, Lcom/sec/chaton/settings/downloads/az;

    const-string v3, "-2"

    const-string v4, "ChatON original"

    const v5, 0x7f060001

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/settings/downloads/az;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    const-string v1, "-3"

    new-instance v2, Lcom/sec/chaton/settings/downloads/az;

    const-string v3, "-3"

    const-string v4, "Horn"

    const v5, 0x7f060003

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/settings/downloads/az;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    const-string v1, "-4"

    new-instance v2, Lcom/sec/chaton/settings/downloads/az;

    const-string v3, "-4"

    const-string v4, "Piano\'s ambience"

    const v5, 0x7f060004

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/settings/downloads/az;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    const-string v1, "-5"

    new-instance v2, Lcom/sec/chaton/settings/downloads/az;

    const-string v3, "-5"

    const-string v4, "Glass wiper"

    const v5, 0x7f060002

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/settings/downloads/az;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/az;->t:Ljava/util/Map;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 21
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    .line 22
    const/4 v0, 0x1

    .line 24
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()V
    .locals 3

    .prologue
    .line 88
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Ringtone"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Ringtone"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 92
    :cond_0
    return-void
.end method
