.class public Lcom/sec/chaton/settings/AboutNewNotice;
.super Landroid/preference/Preference;
.source "AboutNewNotice.java"


# instance fields
.field private a:Z

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/AboutNewNotice;->a:Z

    .line 16
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/AboutNewNotice;->b:I

    .line 23
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/chaton/settings/AboutNewNotice;->a:Z

    .line 77
    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutNewNotice;->notifyChanged()V

    .line 78
    return-void
.end method

.method public a(ZI)V
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/chaton/settings/AboutNewNotice;->a:Z

    .line 83
    iput p2, p0, Lcom/sec/chaton/settings/AboutNewNotice;->b:I

    .line 84
    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutNewNotice;->notifyChanged()V

    .line 85
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 31
    const v0, 0x7f07014d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 33
    iget-boolean v1, p0, Lcom/sec/chaton/settings/AboutNewNotice;->a:Z

    if-eqz v1, :cond_0

    .line 34
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 45
    iget v1, p0, Lcom/sec/chaton/settings/AboutNewNotice;->b:I

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cp;->a(Landroid/widget/TextView;I)V

    .line 67
    :goto_0
    return-void

    .line 48
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
