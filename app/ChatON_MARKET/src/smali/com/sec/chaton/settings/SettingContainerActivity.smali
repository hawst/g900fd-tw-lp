.class public Lcom/sec/chaton/settings/SettingContainerActivity;
.super Landroid/app/TabActivity;
.source "SettingContainerActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 229
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SettingContainerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 232
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/SettingContainerActivity;->startActivity(Landroid/content/Intent;)V

    .line 238
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-static {p0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 37
    const v1, 0x7f030146

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/SettingContainerActivity;->setContentView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SettingContainerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "contentId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 44
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    .line 53
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/SettingContainerActivity;->setTitle(I)V

    .line 61
    const v0, 0x1020012

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SettingContainerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    .line 62
    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 67
    sparse-switch v2, :sswitch_data_0

    .line 149
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 155
    :goto_1
    :try_start_0
    const-string v2, "content"

    invoke-virtual {v0, v2}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_2
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    .line 72
    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityManageBuddy;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 75
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityChat;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 78
    :sswitch_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 98
    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 101
    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityGeneral;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 104
    :sswitch_5
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityAlertType2;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 108
    :sswitch_6
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 115
    :sswitch_7
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityDownloads;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 120
    :sswitch_8
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 124
    :sswitch_9
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/userprofile/EditProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 130
    :sswitch_a
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 133
    :sswitch_b
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityMultiDeviceView;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 138
    :sswitch_c
    if-nez v1, :cond_1

    .line 139
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 141
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityContactSync;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 146
    :sswitch_d
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 156
    :catch_0
    move-exception v0

    .line 158
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x7f0b0005 -> :sswitch_d
        0x7f0b0011 -> :sswitch_2
        0x7f0b0018 -> :sswitch_5
        0x7f0b01b7 -> :sswitch_8
        0x7f0b01c5 -> :sswitch_6
        0x7f0b01e5 -> :sswitch_0
        0x7f0b01e6 -> :sswitch_1
        0x7f0b01e7 -> :sswitch_3
        0x7f0b0203 -> :sswitch_9
        0x7f0b0250 -> :sswitch_4
        0x7f0b0277 -> :sswitch_c
        0x7f0b0287 -> :sswitch_a
        0x7f0b0288 -> :sswitch_b
        0x7f0b030c -> :sswitch_7
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 223
    invoke-super {p0}, Landroid/app/TabActivity;->onResume()V

    .line 225
    invoke-direct {p0}, Lcom/sec/chaton/settings/SettingContainerActivity;->a()V

    .line 226
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 166
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SettingContainerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 206
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
