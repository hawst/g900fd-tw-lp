.class public Lcom/sec/chaton/settings/ActivitySettings;
.super Landroid/preference/PreferenceActivity;
.source "ActivitySettings.java"

# interfaces
.implements Lcom/sec/chaton/util/cn;


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/sec/chaton/settings/AboutNewNotice;

.field c:Lcom/sec/chaton/settings/SyncStatePreference;

.field d:Landroid/preference/PreferenceScreen;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/chaton/util/ab;

.field private g:Landroid/app/ProgressDialog;

.field private h:Landroid/app/ProgressDialog;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 40
    const-string v0, "Settings"

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->a:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/sec/chaton/settings/ActivitySettings;->f:Lcom/sec/chaton/util/ab;

    .line 59
    iput-object v1, p0, Lcom/sec/chaton/settings/ActivitySettings;->h:Landroid/app/ProgressDialog;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->i:Z

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivitySettings;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/chaton/settings/ActivitySettings;->h:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivitySettings;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->e:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    const-string v0, "pref_screen"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->d:Landroid/preference/PreferenceScreen;

    .line 111
    const-string v0, "pref_item_Buddies"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 112
    new-instance v3, Lcom/sec/chaton/settings/cb;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/cb;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 122
    const-string v0, "pref_item_Chats"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 123
    new-instance v3, Lcom/sec/chaton/settings/cc;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/cc;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 133
    const-string v0, "pref_item_Privacy"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 134
    new-instance v3, Lcom/sec/chaton/settings/cd;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/cd;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 144
    const-string v0, "pref_item_Noti"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 145
    new-instance v3, Lcom/sec/chaton/settings/ce;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/ce;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 155
    const-string v0, "pref_item_General"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v3, Lcom/sec/chaton/settings/cf;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/cf;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 167
    const-string v0, "pref_item_Downloads"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 168
    new-instance v3, Lcom/sec/chaton/settings/cg;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/cg;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "# notice in setting : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "notice"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ChatON"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "UpdateIsCritical"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "notice"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_2

    .line 185
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "UpdateIsCritical"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 191
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/settings/ActivitySettings;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "notice"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v3, v1, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 198
    :goto_1
    const-string v0, "pref_item_send_log"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 200
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/util/y;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_3

    .line 202
    new-instance v1, Lcom/sec/chaton/settings/ch;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ch;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 245
    :goto_2
    const-string v0, "pref_item_sync_now"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SyncStatePreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    .line 246
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0193

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivitySettings;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivitySettings;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    new-instance v1, Lcom/sec/chaton/settings/cj;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cj;-><init>(Lcom/sec/chaton/settings/ActivitySettings;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SyncStatePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 257
    return-void

    :cond_1
    move v0, v2

    .line 188
    goto :goto_0

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    goto :goto_1

    .line 239
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivitySettings;->d:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 266
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 267
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 268
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 269
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivitySettings;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/ActivitySettings;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->h:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 324
    const v0, 0x7f0c00fc

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 319
    const v0, 0x7f0c00fb

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 261
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 263
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 64
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 71
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 72
    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 73
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 84
    :cond_0
    const v0, 0x7f05000a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySettings;->addPreferencesFromResource(I)V

    .line 86
    iput-object p0, p0, Lcom/sec/chaton/settings/ActivitySettings;->e:Landroid/content/Context;

    .line 88
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->f:Lcom/sec/chaton/util/ab;

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->f:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    .line 99
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivitySettings;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 298
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->i:Z

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 308
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 274
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/SyncStatePreference;->b()V

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 284
    :cond_0
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 313
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onRestart()V

    .line 315
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 288
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivitySettings;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySettings;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/SyncStatePreference;->a()V

    .line 294
    return-void
.end method
