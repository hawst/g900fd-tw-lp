.class public Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;
.source "WeiboMessageFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/u;
.implements Lcom/sec/chaton/settings/tellfriends/common/v;


# instance fields
.field private b:Lcom/sec/chaton/settings/tellfriends/ap;

.field private c:Lcom/sec/chaton/settings/tellfriends/common/t;


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/ap;

    .line 23
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    .line 28
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/t;

    const-string v1, "280"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "KSC5601"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/t;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    .line 29
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    aput-object v2, v0, v1

    .line 30
    const-string v1, "280"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a(I)V

    .line 31
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a([Landroid/text/InputFilter;)V

    .line 32
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/v;)V

    .line 33
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/u;)V

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->d()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->d()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->d()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 83
    if-eqz p1, :cond_0

    .line 84
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/tellfriends/common/t;->a(Ljava/lang/String;)I

    move-result v0

    .line 86
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->b(I)V

    .line 88
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->c()V

    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/ap;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/cb;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/cb;-><init>(Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;)V

    invoke-virtual {v0, p2, v1}, Lcom/sec/chaton/settings/tellfriends/ap;->a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V

    .line 78
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ap;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ap;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/ap;

    .line 46
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->a()V

    .line 39
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onDestroy()V

    .line 40
    return-void
.end method
