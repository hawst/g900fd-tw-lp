.class Lcom/sec/chaton/settings/cz;
.super Ljava/lang/Object;
.source "CustomTimePicker.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field a:I

.field b:I

.field c:Z

.field final synthetic d:Lcom/sec/chaton/settings/co;

.field private final e:[C


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/co;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 413
    iput-object p1, p0, Lcom/sec/chaton/settings/cz;->d:Lcom/sec/chaton/settings/co;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409
    iput v1, p0, Lcom/sec/chaton/settings/cz;->a:I

    .line 410
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/chaton/settings/cz;->b:I

    .line 411
    iput-boolean v1, p0, Lcom/sec/chaton/settings/cz;->c:Z

    .line 416
    const/16 v0, 0x1e

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/chaton/settings/cz;->e:[C

    .line 414
    iput-boolean p2, p0, Lcom/sec/chaton/settings/cz;->c:Z

    .line 415
    return-void

    .line 416
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x660s
        0x661s
        0x662s
        0x663s
        0x664s
        0x665s
        0x666s
        0x667s
        0x668s
        0x669s
        0x6f0s
        0x6f1s
        0x6f2s
        0x6f3s
        0x6f4s
        0x6f5s
        0x6f6s
        0x6f7s
        0x6f8s
        0x6f9s
    .end array-data
.end method

.method private a(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 462
    move v1, p2

    :goto_0
    if-ge v1, p3, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/settings/cz;->e:[C

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/sec/chaton/settings/cz;->a([CC)Z

    move-result v0

    if-nez v0, :cond_2

    .line 468
    :cond_0
    if-ne v1, p3, :cond_3

    .line 470
    const/4 v0, 0x0

    .line 488
    :cond_1
    :goto_1
    return-object v0

    .line 462
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 473
    :cond_3
    sub-int v0, p3, p2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 475
    const-string v0, ""

    goto :goto_1

    .line 478
    :cond_4
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1, p2, p3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    .line 479
    sub-int v2, v1, p2

    .line 480
    sub-int v1, p3, p2

    .line 483
    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-lt v1, v2, :cond_1

    .line 484
    iget-object v3, p0, Lcom/sec/chaton/settings/cz;->e:[C

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/sec/chaton/settings/cz;->a([CC)Z

    move-result v3

    if-nez v3, :cond_5

    .line 485
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 483
    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_2
.end method

.method private a([CC)Z
    .locals 2

    .prologue
    .line 492
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 493
    aget-char v1, p1, v0

    if-ne v1, p2, :cond_0

    .line 494
    const/4 v0, 0x1

    .line 497
    :goto_1
    return v0

    .line 492
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 497
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 430
    invoke-direct/range {p0 .. p6}, Lcom/sec/chaton/settings/cz;->a(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 431
    if-nez v0, :cond_0

    .line 433
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 436
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-interface {p4, v2, p5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    invoke-interface {p4, p6, v2}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 437
    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 439
    const-string v0, ""

    .line 455
    :cond_1
    :goto_0
    return-object v0

    .line 442
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_3

    .line 443
    const-string v0, ""

    goto :goto_0

    .line 445
    :cond_3
    iget-boolean v2, p0, Lcom/sec/chaton/settings/cz;->c:Z

    if-nez v2, :cond_4

    .line 446
    const/16 v2, 0x17

    iput v2, p0, Lcom/sec/chaton/settings/cz;->b:I

    .line 450
    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 451
    iget v2, p0, Lcom/sec/chaton/settings/cz;->b:I

    if-le v1, v2, :cond_1

    .line 452
    const-string v0, ""

    goto :goto_0

    .line 448
    :cond_4
    const/16 v2, 0x3b

    iput v2, p0, Lcom/sec/chaton/settings/cz;->b:I

    goto :goto_1
.end method
