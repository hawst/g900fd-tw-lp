.class public Lcom/sec/chaton/settings/tellfriends/TwitterFriendsPickerActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "TwitterFriendsPickerActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/TwitterFriendsPickerFragment;

    invoke-direct {v0}, Lcom/sec/chaton/settings/tellfriends/TwitterFriendsPickerFragment;-><init>()V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 33
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 36
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 42
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 45
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 20
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterFriendsPickerActivity;->finish()V

    .line 22
    const/4 v0, 0x1

    .line 24
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
