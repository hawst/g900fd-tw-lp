.class public Lcom/sec/chaton/settings/downloads/q;
.super Ljava/lang/Object;
.source "AmsItemHelper.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/settings/downloads/s;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/settings/downloads/t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/chaton/settings/downloads/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/q;->a:Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/q;->b:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/q;->c:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    return-void
.end method

.method public static a(Lcom/sec/chaton/d/e;)I
    .locals 3

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 174
    sget-object v1, Lcom/sec/chaton/settings/downloads/r;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/d/e;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 188
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 176
    :pswitch_0
    const-string v0, "new_ams_item_background_count"

    goto :goto_0

    .line 180
    :pswitch_1
    const-string v0, "new_ams_item_stamp_count"

    goto :goto_0

    .line 184
    :pswitch_2
    const-string v0, "new_ams_item_template_count"

    goto :goto_0

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/sec/chaton/d/e;)Ljava/io/File;
    .locals 4

    .prologue
    .line 146
    invoke-static {p0}, Lcom/sec/common/util/l;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 147
    const/4 v0, 0x0

    .line 149
    sget-object v2, Lcom/sec/chaton/settings/downloads/r;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/d/e;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 164
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 168
    :cond_0
    return-object v0

    .line 151
    :pswitch_0
    new-instance v0, Ljava/io/File;

    const-string v2, "amsBackgroundItem"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :pswitch_1
    new-instance v0, Ljava/io/File;

    const-string v2, "amsStampItem"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :pswitch_2
    new-instance v0, Ljava/io/File;

    const-string v2, "amsTemplateItem"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 64
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/q;->a(Landroid/content/Context;Lcom/sec/chaton/d/e;)Ljava/io/File;

    move-result-object v0

    .line 65
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p3, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    .line 69
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 70
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download_ams, installItem, [renameResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], targetFile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/q;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_0
    if-nez v0, :cond_2

    .line 74
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t rename file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_1

    .line 96
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/settings/downloads/q;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    :cond_1
    throw v0

    .line 79
    :cond_2
    :try_start_1
    const-string v0, "com.sec.chaton.provider2"

    invoke-virtual {p1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-static {}, Lcom/sec/common/util/i;->a()J

    move-result-wide v2

    invoke-static {v1, p2, v2, v3}, Lcom/sec/chaton/e/a/j;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;J)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 86
    :try_start_2
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    if-ne p1, v0, :cond_3

    .line 87
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/Context;)Z

    .line 91
    :cond_3
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/s;

    .line 92
    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/settings/downloads/s;->b(Lcom/sec/chaton/d/e;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :catch_1
    move-exception v0

    .line 81
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 82
    :catch_2
    move-exception v0

    .line 83
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 101
    :cond_4
    return-void
.end method

.method public static a(Lcom/sec/chaton/settings/downloads/s;)V
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    :cond_0
    return-void
.end method

.method public static a(Lcom/sec/chaton/settings/downloads/t;)V
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 48
    invoke-static {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/io/File;

    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/q;->a(Landroid/content/Context;Lcom/sec/chaton/d/e;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Lcom/sec/chaton/settings/downloads/s;)V
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 199
    return-void
.end method

.method public static b(Lcom/sec/chaton/settings/downloads/t;)V
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 209
    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 106
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/q;->a(Landroid/content/Context;Lcom/sec/chaton/d/e;)Ljava/io/File;

    move-result-object v0

    .line 107
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 109
    const/4 v0, 0x0

    .line 110
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    const/4 v0, 0x1

    .line 112
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 115
    :cond_0
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_1

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download_ams, uninstallItem, [deleteExist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "], targetFile: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/q;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_1
    :try_start_1
    const-string v0, "com.sec.chaton.provider2"

    invoke-virtual {p1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/sec/chaton/e/a/j;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 128
    :try_start_2
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    if-ne p1, v0, :cond_2

    .line 129
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/Context;)Z

    .line 133
    :cond_2
    sget-object v0, Lcom/sec/chaton/settings/downloads/q;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/t;

    .line 134
    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/settings/downloads/t;->a(Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_3

    .line 138
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/settings/downloads/q;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 141
    :cond_3
    throw v0

    .line 122
    :catch_1
    move-exception v0

    .line 123
    :try_start_3
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 124
    :catch_2
    move-exception v0

    .line 125
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 143
    :cond_4
    return-void
.end method
