.class public Lcom/sec/chaton/settings/downloads/SoundDownloads;
.super Landroid/support/v4/app/Fragment;
.source "SoundDownloads.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/chaton/base/e;
.implements Lcom/sec/chaton/settings/downloads/do;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/sec/chaton/base/e;",
        "Lcom/sec/chaton/settings/downloads/do;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Landroid/widget/ListView;

.field private d:Landroid/app/ProgressDialog;

.field private e:Lcom/sec/chaton/d/bc;

.field private f:Lcom/sec/chaton/d/a/cm;

.field private g:Lcom/sec/chaton/settings/downloads/dl;

.field private h:Landroid/view/Menu;

.field private i:Landroid/os/Handler;

.field private j:Landroid/os/Handler;

.field private k:Landroid/widget/AdapterView$OnItemLongClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/sec/chaton/settings/downloads/SoundDownloads;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 76
    new-instance v0, Lcom/sec/chaton/settings/downloads/cy;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/cy;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->i:Landroid/os/Handler;

    .line 117
    new-instance v0, Lcom/sec/chaton/settings/downloads/cz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/cz;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->j:Landroid/os/Handler;

    .line 201
    new-instance v0, Lcom/sec/chaton/settings/downloads/da;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/da;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->k:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SoundDownloads;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SoundDownloads;Lcom/sec/chaton/d/a/cm;)Lcom/sec/chaton/d/a/cm;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->f:Lcom/sec/chaton/d/a/cm;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SoundDownloads;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->f()V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/SoundDownloads;)Lcom/sec/chaton/settings/downloads/dl;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 240
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->f:Lcom/sec/chaton/d/a/cm;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->f:Lcom/sec/chaton/d/a/cm;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/cm;->c()V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    if-eqz v0, :cond_2

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->d()V

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->b()V

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->c()V

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/dl;->a(Lcom/sec/chaton/settings/downloads/do;)V

    .line 266
    :cond_2
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0d0003

    new-instance v2, Lcom/sec/chaton/settings/downloads/dd;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/chaton/settings/downloads/dd;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/SoundDownloads;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->c:Landroid/widget/ListView;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->f(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 364
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/dc;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/dc;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 373
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->a(Landroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    .line 441
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/downloads/de;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/downloads/de;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 452
    :cond_1
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 458
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 464
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->e(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 466
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/df;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/df;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 490
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 491
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 408
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g()V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 300
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 301
    const-string v0, "download_sound, onLoadFinished()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/dl;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 305
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 318
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 319
    const-string v1, "ITEM_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    const-string v1, "SAMPLE_URL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->startActivityForResult(Landroid/content/Intent;I)V

    .line 323
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705a8

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->h:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->h:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->h:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 92
    :cond_0
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g()V

    .line 431
    const/4 v0, 0x1

    .line 435
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 341
    .line 344
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 359
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 352
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/db;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/settings/downloads/db;-><init>(Lcom/sec/chaton/settings/downloads/SoundDownloads;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 327
    packed-switch p1, :pswitch_data_0

    .line 336
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 337
    return-void

    .line 329
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    if-eqz v0, :cond_0

    .line 330
    const/16 v0, 0x7d0

    if-ne p2, v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->notifyDataSetChanged()V

    goto :goto_0

    .line 327
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 152
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    .line 153
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->j:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/sec/chaton/d/bc;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->e:Lcom/sec/chaton/d/bc;

    .line 160
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 277
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 278
    const-string v0, "download_sound, SoundDownloads.onCreateLoader()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "special"

    aput-object v1, v0, v3

    const-string v1, " DESC,"

    aput-object v1, v0, v5

    const-string v1, "item_id"

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const-string v2, " DESC"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 287
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "download_sound, order by : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v3

    const-string v1, " >= ?"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 293
    new-array v5, v5, [Ljava/lang/String;

    const-string v0, "0"

    aput-object v0, v5, v3

    .line 295
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 393
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->h:Landroid/view/Menu;

    .line 395
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    const v0, 0x7f0f002c

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 397
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 398
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 402
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a(Z)V

    .line 403
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 404
    return-void

    .line 400
    :cond_0
    const v0, 0x7f0f0024

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 164
    const v0, 0x7f030100

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 169
    const v0, 0x7f07045b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->c:Landroid/widget/ListView;

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->c:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->k:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 172
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 176
    :cond_0
    new-instance v0, Lcom/sec/chaton/settings/downloads/dl;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/settings/downloads/dl;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/dl;->a(Lcom/sec/chaton/settings/downloads/do;)V

    .line 178
    new-instance v0, Lcom/sec/chaton/settings/downloads/aw;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/aw;-><init>()V

    .line 179
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->i:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Landroid/os/Handler;)V

    .line 180
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/settings/downloads/dl;->a(Lcom/sec/chaton/settings/downloads/aw;)V

    .line 182
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->c:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 184
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->e()V

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->e:Lcom/sec/chaton/d/bc;

    sget-object v2, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    const-string v3, "mp3"

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/d/bc;->a(Lcom/sec/chaton/d/a/b;Ljava/lang/String;)Lcom/sec/chaton/d/a/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->f:Lcom/sec/chaton/d/a/cm;

    .line 189
    invoke-virtual {p0, v5}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->setHasOptionsMenu(Z)V

    .line 191
    return-object v1
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 196
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 198
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->c()V

    .line 199
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 270
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->b:Landroid/app/Activity;

    .line 273
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 578
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    if-nez v0, :cond_1

    .line 592
    :cond_0
    :goto_0
    return-void

    .line 582
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/dl;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 583
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/dl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 584
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 588
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 589
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 310
    const-string v0, "download_sound, onLoaderReset()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDownloads;->g:Lcom/sec/chaton/settings/downloads/dl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/dl;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 314
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 416
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 422
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 419
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SoundDownloads;->d()V

    goto :goto_0

    .line 416
    :pswitch_data_0
    .packed-switch 0x7f0705a8
        :pswitch_0
    .end packed-switch
.end method
