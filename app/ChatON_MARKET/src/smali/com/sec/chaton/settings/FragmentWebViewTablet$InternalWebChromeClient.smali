.class Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "FragmentWebViewTablet.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/FragmentWebViewTablet;)V
    .locals 0

    .prologue
    .line 764
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/FragmentWebViewTablet;Lcom/sec/chaton/settings/FragmentWebViewTablet$1;)V
    .locals 0

    .prologue
    .line 764
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;-><init>(Lcom/sec/chaton/settings/FragmentWebViewTablet;)V

    return-void
.end method


# virtual methods
.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 866
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebChromeClient;->onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    .line 876
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 897
    :goto_0
    return v4

    .line 880
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient$1;

    invoke-direct {v3, p0, p4}, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient$1;-><init>(Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;Landroid/webkit/JsResult;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    # setter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->mAlertDialog:Lcom/sec/common/a/d;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$202(Lcom/sec/chaton/settings/FragmentWebViewTablet;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 890
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    # getter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->mAlertDialog:Lcom/sec/common/a/d;
    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$200(Lcom/sec/chaton/settings/FragmentWebViewTablet;)Lcom/sec/common/a/d;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient$2;

    invoke-direct {v1, p0, p4}, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient$2;-><init>(Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;Landroid/webkit/JsResult;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 896
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    # getter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->mAlertDialog:Lcom/sec/common/a/d;
    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$200(Lcom/sec/chaton/settings/FragmentWebViewTablet;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->getActivity()Landroid/app/Activity;

    move-result-object v0

    mul-int/lit8 v1, p2, 0x64

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setProgress(I)V

    .line 771
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 775
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "failingUrl = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "desctiption = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    return-void
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 780
    const-string v0, "openFileChooser under SDK 10"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    # setter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->mUploadMessage:Landroid/webkit/ValueCallback;
    invoke-static {v0, p1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$302(Lcom/sec/chaton/settings/FragmentWebViewTablet;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 783
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 784
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 786
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 787
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 788
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 789
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 790
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 797
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    const-string v2, "File Chooser"

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 804
    :cond_1
    :goto_0
    return-void

    .line 798
    :catch_0
    move-exception v0

    .line 799
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 800
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 801
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 807
    const-string v0, "openFileChooser over SDK 11 and under SDK 15"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    # setter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->mUploadMessage:Landroid/webkit/ValueCallback;
    invoke-static {v0, p1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$302(Lcom/sec/chaton/settings/FragmentWebViewTablet;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 810
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 811
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 813
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 814
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 815
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 816
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 817
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 824
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    const-string v2, "File Chooser"

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 831
    :cond_1
    :goto_0
    return-void

    .line 825
    :catch_0
    move-exception v0

    .line 826
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 827
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 828
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 834
    const-string v0, "openFileChooser over SDK 16"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    # setter for: Lcom/sec/chaton/settings/FragmentWebViewTablet;->mUploadMessage:Landroid/webkit/ValueCallback;
    invoke-static {v0, p1}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->access$302(Lcom/sec/chaton/settings/FragmentWebViewTablet;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 837
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 838
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 853
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebViewTablet$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebViewTablet;

    const-string v2, "File Chooser"

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentWebViewTablet;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 854
    :catch_0
    move-exception v0

    .line 855
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 856
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 857
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
