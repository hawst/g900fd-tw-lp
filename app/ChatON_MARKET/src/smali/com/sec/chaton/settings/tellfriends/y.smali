.class public Lcom/sec/chaton/settings/tellfriends/y;
.super Ljava/lang/Object;
.source "SNSIDSet.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field private static k:Landroid/content/Context;

.field private static l:Lcom/sec/chaton/settings/tellfriends/y;


# instance fields
.field j:Landroid/os/Handler;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "facebook"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->a:Ljava/lang/String;

    .line 15
    const-string v0, "twitter"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->b:Ljava/lang/String;

    .line 16
    const-string v0, "weibo"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->c:Ljava/lang/String;

    .line 17
    const-string v0, "renren"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->d:Ljava/lang/String;

    .line 20
    const-string v0, "SNSIDSetClass"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->e:Ljava/lang/String;

    .line 23
    const-string v0, "FACEBOOK_ID_SET_STATUS"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->f:Ljava/lang/String;

    .line 24
    const-string v0, "TWITTER_ID_SET_STATUS"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->g:Ljava/lang/String;

    .line 25
    const-string v0, "WEIBO_ID_SET_STATUS"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->h:Ljava/lang/String;

    .line 26
    const-string v0, "RENREN_ID_SET_STATUS"

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/y;->m:Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/z;-><init>(Lcom/sec/chaton/settings/tellfriends/y;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/y;->j:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/chaton/settings/tellfriends/y;
    .locals 1

    .prologue
    .line 35
    sput-object p0, Lcom/sec/chaton/settings/tellfriends/y;->k:Landroid/content/Context;

    .line 38
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->l:Lcom/sec/chaton/settings/tellfriends/y;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/y;

    invoke-direct {v0}, Lcom/sec/chaton/settings/tellfriends/y;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/y;->l:Lcom/sec/chaton/settings/tellfriends/y;

    .line 41
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->l:Lcom/sec/chaton/settings/tellfriends/y;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/y;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/y;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 88
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/y;->k:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/y;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 92
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 93
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 94
    return-void
.end method
