.class public Lcom/sec/chaton/settings/downloads/SkinListView;
.super Landroid/support/v4/app/Fragment;
.source "SkinListView.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/chaton/base/e;
.implements Lcom/sec/chaton/settings/downloads/by;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/sec/chaton/base/e;",
        "Lcom/sec/chaton/settings/downloads/by;"
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Landroid/app/ProgressDialog;

.field private d:Landroid/widget/GridView;

.field private e:Lcom/sec/common/f/c;

.field private f:Lcom/sec/chaton/c/h;

.field private g:Lcom/sec/chaton/d/a/ca;

.field private h:Lcom/sec/chaton/settings/downloads/bw;

.field private i:Landroid/view/Menu;

.field private j:Lcom/sec/chaton/settings/downloads/cs;

.field private k:Landroid/os/Handler;

.field private l:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/sec/chaton/settings/downloads/SkinListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/SkinListView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 84
    new-instance v0, Lcom/sec/chaton/settings/downloads/cn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/cn;-><init>(Lcom/sec/chaton/settings/downloads/SkinListView;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->k:Landroid/os/Handler;

    .line 104
    new-instance v0, Lcom/sec/chaton/settings/downloads/co;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/co;-><init>(Lcom/sec/chaton/settings/downloads/SkinListView;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->l:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SkinListView;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->i:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SkinListView;Lcom/sec/chaton/d/a/ca;)Lcom/sec/chaton/d/a/ca;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->g:Lcom/sec/chaton/d/a/ca;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/SkinListView;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->f(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 203
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/cp;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/cp;-><init>(Lcom/sec/chaton/settings/downloads/SkinListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 220
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->a(Landroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/downloads/cq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/downloads/cq;-><init>(Lcom/sec/chaton/settings/downloads/SkinListView;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 339
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/SkinListView;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SkinListView;->d()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/SkinListView;)Lcom/sec/chaton/settings/downloads/bw;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 345
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->e(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 364
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/cr;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/cr;-><init>(Lcom/sec/chaton/settings/downloads/SkinListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 387
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->j:Lcom/sec/chaton/settings/downloads/cs;

    invoke-interface {v0}, Lcom/sec/chaton/settings/downloads/cs;->c()V

    .line 438
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/bw;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 302
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 410
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 411
    const-string v1, "ITEM_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 413
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/SkinListView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 414
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705a8

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->i:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 102
    :cond_0
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SkinListView;->e()V

    .line 354
    const/4 v0, 0x1

    .line 358
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 418
    packed-switch p1, :pswitch_data_0

    .line 430
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 431
    return-void

    .line 420
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    if-eqz v0, :cond_0

    .line 421
    const/16 v0, 0x7d0

    if-ne p2, v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bw;->notifyDataSetChanged()V

    .line 423
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/ActivityBgBubbleChange;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 424
    const-string v1, "called_from_downloads"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 425
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/SkinListView;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 148
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 150
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    .line 151
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 162
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 164
    new-instance v0, Lcom/sec/chaton/c/h;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->l:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/c/h;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->f:Lcom/sec/chaton/c/h;

    .line 165
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 285
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "special"

    aput-object v1, v0, v3

    const-string v1, " DESC,"

    aput-object v1, v0, v5

    const-string v1, "new"

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const-string v2, " DESC,"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, " DESC"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 293
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v3

    const-string v1, " >= ?"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 294
    new-array v5, v5, [Ljava/lang/String;

    const-string v0, "0"

    aput-object v0, v5, v3

    .line 296
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 225
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->i:Landroid/view/Menu;

    .line 227
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    const v0, 0x7f0f002b

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 229
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 230
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 234
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/SkinListView;->a(Z)V

    .line 236
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 237
    return-void

    .line 232
    :cond_0
    const v0, 0x7f0f0024

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 169
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 171
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->e:Lcom/sec/common/f/c;

    .line 173
    const v0, 0x7f030108

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 175
    const v0, 0x7f070465

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->d:Landroid/widget/GridView;

    .line 176
    new-instance v0, Lcom/sec/chaton/settings/downloads/bw;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->e:Lcom/sec/common/f/c;

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/sec/chaton/settings/downloads/bw;-><init>(Landroid/content/Context;Landroid/database/Cursor;ZLcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/bw;->a(Lcom/sec/chaton/settings/downloads/by;)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/settings/downloads/cs;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->j:Lcom/sec/chaton/settings/downloads/cs;

    .line 181
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 183
    new-instance v0, Lcom/sec/chaton/settings/downloads/aw;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/aw;-><init>()V

    .line 184
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->k:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/aw;->a(Landroid/os/Handler;)V

    .line 185
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/bw;->a(Lcom/sec/chaton/settings/downloads/aw;)V

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->d:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 188
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->d:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 191
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SkinListView;->c()V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->f:Lcom/sec/chaton/c/h;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->b()I

    move-result v2

    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->b()I

    move-result v3

    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->c()I

    move-result v4

    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->d()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/c/h;->a(Lcom/sec/chaton/d/a/b;IIII)Lcom/sec/chaton/d/a/ca;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->g:Lcom/sec/chaton/d/a/ca;

    .line 195
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/SkinListView;->setHasOptionsMenu(Z)V

    .line 197
    return-object v6
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 260
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 262
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->g:Lcom/sec/chaton/d/a/ca;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->g:Lcom/sec/chaton/d/a/ca;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/ca;->c()V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->e:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->e:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    if-eqz v0, :cond_3

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bw;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bw;->b()V

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bw;->c()V

    .line 280
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SkinListView;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 281
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    .line 158
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    if-nez v0, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/bw;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 397
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 401
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 402
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "download_skin, Clicked item id: "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/SkinListView;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/SkinListView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/SkinListView;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/bw;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 307
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 241
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 255
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 244
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->h:Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SkinListView;->e()V

    goto :goto_0

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 252
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SkinListView;->b()V

    goto :goto_0

    .line 241
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0705a8 -> :sswitch_1
    .end sparse-switch
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->d:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinListView;->d:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 144
    :cond_0
    return-void
.end method
