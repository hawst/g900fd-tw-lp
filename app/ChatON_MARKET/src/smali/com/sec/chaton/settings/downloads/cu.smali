.class Lcom/sec/chaton/settings/downloads/cu;
.super Landroid/os/Handler;
.source "SoundDetail.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/SoundDetail;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/SoundDetail;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/cu;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/multimedia/audio/d;

    .line 101
    if-nez v0, :cond_1

    .line 102
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "download_sound, PlaySoundHandler(), task is null"

    invoke-static {}, Lcom/sec/chaton/settings/downloads/SoundDetail;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/f;->a(I)Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v1

    .line 109
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_2

    .line 110
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sound event (what/task status/ task canceled) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->isCancelled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/SoundDetail;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cu;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/ImageView;Lcom/sec/chaton/multimedia/audio/f;)Z

    .line 113
    iget v0, p1, Landroid/os/Message;->what:I

    sget-object v1, Lcom/sec/chaton/multimedia/audio/f;->c:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/audio/f;->a()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cu;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a()V

    .line 117
    :cond_3
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
