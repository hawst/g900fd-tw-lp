.class public Lcom/sec/chaton/settings/downloads/a/l;
.super Lcom/sec/common/util/a;
.source "InstallAsyncWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "DownloadResult:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/sec/common/util/a",
        "<TParams;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static g:Lcom/sec/chaton/settings/downloads/a/o;

.field private static final h:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private e:Lcom/sec/chaton/e/ar;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-class v0, Lcom/sec/chaton/settings/downloads/a/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    .line 48
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/o;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/downloads/a/o;-><init>(Lcom/sec/chaton/settings/downloads/a/m;)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/l;->g:Lcom/sec/chaton/settings/downloads/a/o;

    .line 51
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/l;->h:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/e/ar;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    sget-object v0, Lcom/sec/chaton/settings/downloads/a/l;->h:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/sec/common/util/a;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    .line 64
    iput v1, p0, Lcom/sec/chaton/settings/downloads/a/l;->c:I

    .line 65
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->d:Ljava/lang/Object;

    .line 67
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/a/l;->e:Lcom/sec/chaton/e/ar;

    .line 68
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/a/l;->f:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 219
    invoke-static {v0, p1, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 221
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/a/l;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->a(I)V

    return-void
.end method


# virtual methods
.method protected varargs a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDownloadResult;[TParams;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 214
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->c([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 83
    return-void
.end method

.method public a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x4

    .line 161
    invoke-super {p0, p1}, Lcom/sec/common/util/a;->a(Ljava/lang/Object;)V

    .line 163
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "InstallAsyncWorker.onPostExecute()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    iput v2, p0, Lcom/sec/chaton/settings/downloads/a/l;->c:I

    .line 170
    invoke-direct {p0, v2}, Lcom/sec/chaton/settings/downloads/a/l;->a(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 174
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/settings/downloads/a/l;)V

    .line 181
    :goto_0
    return-void

    .line 176
    :cond_1
    iput v3, p0, Lcom/sec/chaton/settings/downloads/a/l;->c:I

    .line 177
    invoke-direct {p0, v3}, Lcom/sec/chaton/settings/downloads/a/l;->a(I)V

    .line 179
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/a/p;->c(Lcom/sec/chaton/settings/downloads/a/l;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 5

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/sec/common/util/a;->b(Ljava/lang/Object;)V

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 205
    const/4 v2, 0x6

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 207
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public b()Lcom/sec/chaton/e/ar;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->e:Lcom/sec/chaton/e/ar;

    return-object v0
.end method

.method protected varargs b([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TDownloadResult;"
        }
    .end annotation

    .prologue
    .line 210
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method protected synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected varargs c([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 127
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "InstallAsyncWorker.onDownloading()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->c:I

    .line 133
    sget-object v0, Lcom/sec/chaton/settings/downloads/a/l;->g:Lcom/sec/chaton/settings/downloads/a/o;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 134
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 136
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 137
    const-string v1, "InstallAsyncWorker.onInstalling()"

    sget-object v2, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_1
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/chaton/settings/downloads/a/l;->c:I

    .line 142
    sget-object v1, Lcom/sec/chaton/settings/downloads/a/l;->g:Lcom/sec/chaton/settings/downloads/a/o;

    const/4 v2, 0x2

    invoke-static {v1, v2, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 143
    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sec/chaton/settings/downloads/a/n; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_2

    .line 146
    const-string v0, "InstallAsyncWorker is interrupted."

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 150
    :catch_1
    move-exception v0

    .line 151
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 152
    sget-object v1, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 155
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->c:I

    return v0
.end method

.method protected e()V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0}, Lcom/sec/common/util/a;->e()V

    .line 116
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "InstallAsyncWorker.onPreExecute()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/settings/downloads/a/l;)V

    .line 122
    return-void
.end method

.method protected f()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 185
    invoke-super {p0}, Lcom/sec/common/util/a;->f()V

    .line 187
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "InstallAsyncWorker.onCancelled()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    iput v2, p0, Lcom/sec/chaton/settings/downloads/a/l;->c:I

    .line 193
    invoke-direct {p0, v2}, Lcom/sec/chaton/settings/downloads/a/l;->a(I)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 197
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/settings/downloads/a/l;)V

    .line 198
    return-void
.end method
