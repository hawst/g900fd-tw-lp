.class public Lcom/sec/chaton/settings/moreapps/a;
.super Landroid/widget/ArrayAdapter;
.source "MoreAppsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/aj;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/sec/common/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/chaton/settings/moreapps/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/moreapps/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/chaton/PlusFragment;Ljava/util/ArrayList;Lcom/sec/common/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/chaton/PlusFragment;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/aj;",
            ">;",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 45
    iput-object p1, p0, Lcom/sec/chaton/settings/moreapps/a;->b:Landroid/content/Context;

    .line 47
    iput-object p4, p0, Lcom/sec/chaton/settings/moreapps/a;->d:Lcom/sec/common/f/c;

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/a;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/settings/moreapps/a;->c:Landroid/view/LayoutInflater;

    .line 51
    return-void
.end method

.method private a(Lcom/sec/chaton/settings/moreapps/b;Lcom/sec/chaton/aj;)V
    .locals 4

    .prologue
    .line 103
    .line 106
    :try_start_0
    iget-object v0, p2, Lcom/sec/chaton/aj;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    new-instance v0, Lcom/sec/chaton/settings/moreapps/c;

    iget-object v1, p2, Lcom/sec/chaton/aj;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/moreapps/a;->b:Landroid/content/Context;

    iget-object v3, p2, Lcom/sec/chaton/aj;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/sec/chaton/settings/moreapps/c;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/settings/moreapps/b;)V

    .line 116
    iget-object v1, p0, Lcom/sec/chaton/settings/moreapps/a;->d:Lcom/sec/common/f/c;

    iget-object v2, p1, Lcom/sec/chaton/settings/moreapps/b;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 118
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    const-string v1, "MoreAppsAdapter.ExtractFileNameFromUrl"

    sget-object v2, Lcom/sec/chaton/settings/moreapps/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    sget-object v1, Lcom/sec/chaton/settings/moreapps/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 58
    if-nez p2, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/a;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0300bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 60
    new-instance v1, Lcom/sec/chaton/settings/moreapps/b;

    invoke-direct {v1}, Lcom/sec/chaton/settings/moreapps/b;-><init>()V

    .line 61
    const v0, 0x7f07034b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/settings/moreapps/b;->a:Landroid/widget/ImageView;

    .line 62
    const v0, 0x7f07034d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/settings/moreapps/b;->b:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f07034c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/settings/moreapps/b;->c:Landroid/widget/ImageView;

    .line 64
    const v0, 0x7f07034a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v1, Lcom/sec/chaton/settings/moreapps/b;->d:Landroid/widget/FrameLayout;

    .line 66
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 71
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/moreapps/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/aj;

    .line 72
    iget-object v2, v1, Lcom/sec/chaton/settings/moreapps/b;->b:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/aj;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/settings/moreapps/a;->a(Lcom/sec/chaton/settings/moreapps/b;Lcom/sec/chaton/aj;)V

    .line 78
    return-object p2

    .line 68
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/moreapps/b;

    move-object v1, v0

    goto :goto_0
.end method
