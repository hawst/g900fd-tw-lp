.class public Lcom/sec/chaton/settings/RingtonePreference2;
.super Landroid/preference/Preference;
.source "RingtonePreference2.java"


# instance fields
.field a:Landroid/content/DialogInterface$OnClickListener;

.field private b:Landroid/media/RingtoneManager;

.field private c:Lcom/sec/common/a/d;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/eq;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/multimedia/audio/a;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/sec/chaton/settings/eq;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Lcom/sec/chaton/settings/eq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 249
    new-instance v0, Lcom/sec/chaton/settings/ev;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/ev;-><init>(Lcom/sec/chaton/settings/RingtonePreference2;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->a:Landroid/content/DialogInterface$OnClickListener;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 249
    new-instance v0, Lcom/sec/chaton/settings/ev;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/ev;-><init>(Lcom/sec/chaton/settings/RingtonePreference2;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->a:Landroid/content/DialogInterface$OnClickListener;

    .line 61
    invoke-direct {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->d()V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 249
    new-instance v0, Lcom/sec/chaton/settings/ev;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/ev;-><init>(Lcom/sec/chaton/settings/RingtonePreference2;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->a:Landroid/content/DialogInterface$OnClickListener;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/RingtonePreference2;Lcom/sec/chaton/multimedia/audio/a;)Lcom/sec/chaton/multimedia/audio/a;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->e:Lcom/sec/chaton/multimedia/audio/a;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/RingtonePreference2;Lcom/sec/chaton/settings/eq;)Lcom/sec/chaton/settings/eq;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->h:Lcom/sec/chaton/settings/eq;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->c:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/RingtonePreference2;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/RingtonePreference2;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/settings/eq;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->h:Lcom/sec/chaton/settings/eq;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/RingtonePreference2;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/RingtonePreference2;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->f()Landroid/net/Uri;

    move-result-object v0

    .line 80
    if-nez v0, :cond_0

    move v0, v1

    .line 88
    :goto_0
    return v0

    .line 83
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/settings/RingtonePreference2;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/eq;

    .line 84
    if-eqz v0, :cond_1

    .line 85
    invoke-virtual {v0}, Lcom/sec/chaton/settings/eq;->e()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/RingtonePreference2;)Landroid/media/RingtoneManager;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/multimedia/audio/a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->e:Lcom/sec/chaton/multimedia/audio/a;

    return-object v0
.end method

.method private d()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Landroid/media/RingtoneManager;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    invoke-virtual {v0, v11}, Landroid/media/RingtoneManager;->setType(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    invoke-virtual {v0, v10}, Landroid/media/RingtoneManager;->setIncludeDrm(Z)V

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    invoke-virtual {v0, v6}, Landroid/media/RingtoneManager;->setStopPreviousRingtone(Z)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->f:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 112
    :goto_1
    iput v6, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/chaton/settings/eq;

    iget v2, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    sget-object v3, Lcom/sec/chaton/settings/er;->a:Lcom/sec/chaton/settings/er;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b01a5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v9}, Lcom/sec/chaton/settings/eq;-><init>(ILcom/sec/chaton/settings/er;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    invoke-virtual {v0}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 118
    if-eqz v8, :cond_3

    move v5, v6

    .line 120
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    invoke-virtual {v0, v5}, Landroid/media/RingtoneManager;->getRingtoneUri(I)Landroid/net/Uri;

    move-result-object v4

    .line 123
    new-instance v0, Lcom/sec/chaton/settings/eq;

    iget v1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    sget-object v2, Lcom/sec/chaton/settings/er;->a:Lcom/sec/chaton/settings/er;

    add-int/lit8 v7, v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/eq;-><init>(ILcom/sec/chaton/settings/er;Ljava/lang/String;Landroid/net/Uri;I)V

    .line 124
    iget-object v1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v7

    .line 126
    goto :goto_2

    .line 103
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    goto :goto_0

    .line 109
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->f:Ljava/util/HashMap;

    goto :goto_1

    .line 130
    :cond_3
    new-array v0, v11, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v6

    const-string v1, "< \'0\'"

    aput-object v1, v0, v10

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 131
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "name"

    move-object v2, v9

    move-object v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 132
    if-eqz v7, :cond_5

    .line 134
    :goto_3
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    const-string v0, "name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 136
    const-string v0, "item_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/dg;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    .line 139
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 141
    new-instance v0, Lcom/sec/chaton/settings/eq;

    iget v1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    sget-object v2, Lcom/sec/chaton/settings/er;->b:Lcom/sec/chaton/settings/er;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/eq;-><init>(ILcom/sec/chaton/settings/er;Ljava/lang/String;Landroid/net/Uri;I)V

    .line 142
    iget-object v1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 146
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 152
    :cond_5
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v6

    const-string v1, "> \'0\' AND "

    aput-object v1, v0, v10

    const-string v1, "install"

    aput-object v1, v0, v11

    const/4 v1, 0x3

    const-string v2, "!= \'0\'"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "name"

    move-object v2, v9

    move-object v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_7

    .line 157
    :goto_4
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 158
    const-string v0, "name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 159
    const-string v2, "item_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 161
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 163
    new-instance v3, Lcom/sec/chaton/settings/eq;

    iget v4, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/sec/chaton/settings/RingtonePreference2;->g:I

    sget-object v5, Lcom/sec/chaton/settings/er;->c:Lcom/sec/chaton/settings/er;

    invoke-direct {v3, v4, v5, v0, v2}, Lcom/sec/chaton/settings/eq;-><init>(ILcom/sec/chaton/settings/er;Ljava/lang/String;Landroid/net/Uri;)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    .line 168
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 181
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/CharSequence;

    .line 182
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_8

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/eq;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/eq;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    .line 182
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 187
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    .line 190
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 191
    const v1, 0x7f03014d

    invoke-virtual {v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 192
    const v1, 0x7f07054a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 193
    new-instance v4, Lcom/sec/chaton/settings/es;

    invoke-direct {v4, p0}, Lcom/sec/chaton/settings/es;-><init>(Lcom/sec/chaton/settings/RingtonePreference2;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    invoke-virtual {v3, v0}, Lcom/sec/common/a/a;->a(Landroid/view/View;)Lcom/sec/common/a/a;

    .line 204
    invoke-direct {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->c()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/settings/RingtonePreference2;->a:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v2, v0, v1}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 206
    const v0, 0x104000a

    new-instance v1, Lcom/sec/chaton/settings/et;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/et;-><init>(Lcom/sec/chaton/settings/RingtonePreference2;)V

    invoke-virtual {v3, v0, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 224
    const/high16 v0, 0x1040000

    invoke-virtual {v3, v0, v9}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->c:Lcom/sec/common/a/d;

    if-eqz v0, :cond_9

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 229
    :cond_9
    invoke-virtual {v3}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->c:Lcom/sec/common/a/d;

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->c:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/settings/eu;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/eu;-><init>(Lcom/sec/chaton/settings/RingtonePreference2;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 247
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 311
    new-instance v0, Lcom/sec/chaton/multimedia/audio/a;

    new-instance v1, Lcom/sec/chaton/settings/ew;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ew;-><init>(Lcom/sec/chaton/settings/RingtonePreference2;)V

    invoke-direct {v0, v1}, Lcom/sec/chaton/multimedia/audio/a;-><init>(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->e:Lcom/sec/chaton/multimedia/audio/a;

    .line 323
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/RingtonePreference2;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->g()V

    return-void
.end method

.method private f()Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 326
    .line 327
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Ringtone"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 329
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 330
    const-string v2, "Silent"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 345
    :cond_0
    :goto_0
    return-object v0

    .line 334
    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 338
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    .line 340
    if-eqz v0, :cond_0

    .line 341
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Ringtone"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/RingtonePreference2;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 368
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 369
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 371
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/settings/RingtonePreference2;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->e()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    invoke-virtual {v0}, Landroid/media/RingtoneManager;->stopPreviousRingtone()V

    .line 352
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->b:Landroid/media/RingtoneManager;

    invoke-virtual {v0}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 358
    if-eqz v0, :cond_0

    .line 361
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 364
    :cond_0
    return-void
.end method

.method protected onClick()V
    .locals 3

    .prologue
    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->h:Lcom/sec/chaton/settings/eq;

    .line 71
    invoke-direct {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->d()V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->b()Landroid/widget/ListView;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/settings/RingtonePreference2;->c()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/settings/RingtonePreference2;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 74
    return-void
.end method
