.class Lcom/sec/chaton/settings/downloads/ca;
.super Landroid/os/Handler;
.source "SkinDetail.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/SkinDetail;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/SkinDetail;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 118
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/r;

    .line 120
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 147
    :goto_0
    return-void

    .line 123
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 124
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "PackageId: "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    const-string v2, ". Status: "

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/SkinDetail;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Lcom/sec/chaton/settings/downloads/SkinDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Lcom/sec/chaton/settings/downloads/SkinDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 132
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Lcom/sec/chaton/settings/downloads/SkinDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Lcom/sec/chaton/settings/downloads/SkinDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 137
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Lcom/sec/chaton/settings/downloads/SkinDetail;Lcom/sec/chaton/settings/downloads/a/r;)Lcom/sec/chaton/settings/downloads/a/r;

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Lcom/sec/chaton/settings/downloads/SkinDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Lcom/sec/chaton/settings/downloads/SkinDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 144
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ca;->a:Lcom/sec/chaton/settings/downloads/SkinDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->b(Lcom/sec/chaton/settings/downloads/SkinDetail;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
