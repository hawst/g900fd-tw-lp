.class Lcom/sec/chaton/settings/d;
.super Landroid/os/Handler;
.source "AboutServiceFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/AboutServiceFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const v9, 0x7f07033c

    const v8, 0x7f07033b

    const v7, 0x7f07033a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 286
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 334
    :pswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_b

    .line 335
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 336
    if-eqz v0, :cond_0

    .line 337
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 340
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "3.3.51"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 341
    :cond_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/am;->s()V

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->f(Lcom/sec/chaton/settings/AboutServiceFragment;)V

    goto :goto_0

    .line 288
    :pswitch_2
    const-string v1, "2"

    .line 289
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 291
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_6

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 292
    const-string v0, "spp_update_is"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->b(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 306
    :goto_1
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v3

    sget v4, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v3, v4

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v3

    sget v4, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v3, v4

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v3

    sget v4, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v3, v4

    .line 315
    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 316
    const-string v0, "spp_latest_ver"

    iget-object v2, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    sget v2, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v0, v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;I)V

    goto/16 :goto_0

    .line 301
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->b(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_1

    .line 319
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 325
    :cond_6
    const-string v0, "There is no update of SPPPushClient."

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 350
    :cond_7
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->critical:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->critical:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 352
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v1, v1, Lcom/sec/chaton/settings/AboutServiceFragment;->g:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/bj;->c()V

    .line 353
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 354
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1, v6}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z

    .line 358
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->b(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 361
    if-eqz v2, :cond_9

    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 363
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v3, v5

    .line 364
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v3, v5

    .line 365
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aput-object v1, v3, v5

    .line 366
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v1

    aget-object v1, v1, v5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v3, v3, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v1

    aget-object v2, v1, v5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v1, v1, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v1, v1, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    aget-object v1, v1, v5

    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v1

    aget-object v1, v1, v5

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 369
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1, v5}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;I)V

    .line 374
    :cond_9
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateTargetVersion"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateIsCritical"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->critical:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 376
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateIsNormal"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 377
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateUrl"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->downloadurl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "SamsungappsUrl"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->samsungappsurl:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "more_tab_badge_update"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 367
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/settings/d;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v1, v1, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    aget-object v1, v1, v5

    goto :goto_3

    .line 388
    :cond_b
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 390
    const-string v0, "network fail to get version"

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 286
    :pswitch_data_0
    .packed-switch 0x450
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
