.class public Lcom/sec/chaton/settings/ActivityChat;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "ActivityChat.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/sec/chaton/e/a/v;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/chaton/util/ab;

.field private e:Z

.field private f:Landroid/app/ProgressDialog;

.field private g:Landroid/preference/CheckBoxPreference;

.field private h:Lcom/sec/chaton/e/a/u;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    .line 51
    const-string v0, "Settings"

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->a:Ljava/lang/String;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    .line 458
    new-instance v0, Lcom/sec/chaton/settings/s;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/s;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->b:Lcom/sec/chaton/e/a/v;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityChat;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const v7, 0x7f08003f

    const/4 v6, 0x1

    const v5, 0x7f08001b

    const/4 v4, 0x0

    .line 135
    const-string v0, "pref_item_change_bubble_background_style"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 136
    new-instance v1, Lcom/sec/chaton/settings/j;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/j;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 184
    const-string v0, "pref_item_font_style"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 186
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityChat;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/chaton/settings/ActivityChat;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 188
    new-instance v1, Lcom/sec/chaton/settings/k;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/k;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 199
    const-string v0, "pref_item_font_size"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 201
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityChat;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/chaton/settings/ActivityChat;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 203
    new-instance v1, Lcom/sec/chaton/settings/l;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/l;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 214
    const-string v0, "pref_item_enter_key"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 216
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting enter key"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v6, :cond_0

    .line 217
    iput-boolean v6, p0, Lcom/sec/chaton/settings/ActivityChat;->e:Z

    .line 218
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting enter key"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 223
    :goto_0
    iget-boolean v1, p0, Lcom/sec/chaton/settings/ActivityChat;->e:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 224
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/chaton/settings/ActivityChat;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 226
    new-instance v1, Lcom/sec/chaton/settings/m;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/m;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 250
    const-string v0, "pref_item_push_to_talk_key"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 251
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting push to talk"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 252
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting push to talk"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 253
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 254
    const v1, 0x7f0b0212

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityChat;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/chaton/settings/ActivityChat;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 256
    new-instance v1, Lcom/sec/chaton/settings/n;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/n;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 279
    const-string v0, "pref_item_auto_resend"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->g:Landroid/preference/CheckBoxPreference;

    .line 280
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 282
    if-eqz v0, :cond_1

    .line 283
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityChat;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 288
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->g:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings/o;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/o;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 340
    const-string v0, "pref_item_delete_old_chat_rooms"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 341
    new-instance v1, Lcom/sec/chaton/settings/p;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/p;-><init>(Lcom/sec/chaton/settings/ActivityChat;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 375
    return-void

    .line 220
    :cond_0
    iput-boolean v4, p0, Lcom/sec/chaton/settings/ActivityChat;->e:Z

    .line 221
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting enter key"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 285
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityChat;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityChat;Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/settings/ActivityChat;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 432
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 433
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 434
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 435
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityChat;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/chaton/settings/ActivityChat;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityChat;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 449
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityChat;->c:Landroid/content/Context;

    const v3, 0x7f0b0313

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->h:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x1

    invoke-static {}, Lcom/sec/chaton/e/g;->a()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 456
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/ActivityChat;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->g:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 529
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 531
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Typeface"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 533
    if-eq v1, v4, :cond_0

    .line 535
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Default Font Name"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 536
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Name"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 545
    :cond_0
    :goto_0
    return-object v0

    .line 538
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/e/a/k;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 539
    if-eqz v1, :cond_2

    .line 540
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 542
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Name"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 4

    .prologue
    const v3, 0x7f0b017c

    .line 549
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Default Font Size"

    sget-object v2, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/dk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 551
    invoke-static {v0}, Lcom/sec/chaton/settings/dk;->a(Ljava/lang/String;)Lcom/sec/chaton/settings/dk;

    move-result-object v0

    .line 552
    invoke-virtual {v0}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v0

    .line 554
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 556
    sget-object v1, Lcom/sec/chaton/settings/dk;->a:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 557
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 569
    :goto_0
    return-object v0

    .line 558
    :cond_0
    sget-object v1, Lcom/sec/chaton/settings/dk;->b:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 559
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 560
    :cond_1
    sget-object v1, Lcom/sec/chaton/settings/dk;->c:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 561
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 562
    :cond_2
    sget-object v1, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 563
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 564
    :cond_3
    sget-object v1, Lcom/sec/chaton/settings/dk;->e:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 565
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 567
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/ActivityChat;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityChat;->b()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/ActivityChat;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 379
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 381
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iput-object p0, p0, Lcom/sec/chaton/settings/ActivityChat;->c:Landroid/content/Context;

    .line 96
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityChat;->b:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->h:Lcom/sec/chaton/e/a/u;

    .line 104
    const v0, 0x7f050006

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityChat;->addPreferencesFromResource(I)V

    .line 106
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->d:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    .line 117
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityChat;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 405
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onDestroy()V

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityChat;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 409
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 386
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onPause()V

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 415
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onRestart()V

    .line 417
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 393
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onResume()V

    .line 394
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityChat;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :goto_0
    return-void

    .line 397
    :catch_0
    move-exception v0

    .line 398
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 125
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityChat;->finish()V

    .line 127
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 423
    return-void
.end method
