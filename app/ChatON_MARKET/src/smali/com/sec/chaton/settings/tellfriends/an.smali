.class public Lcom/sec/chaton/settings/tellfriends/an;
.super Lcom/sec/chaton/settings/tellfriends/aa;
.source "SnsHelperTwitter.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/settings/tellfriends/ba;

.field private c:Lcom/sec/chaton/settings/tellfriends/af;

.field private d:Lcom/sec/chaton/settings/tellfriends/ag;

.field private e:Lcom/sec/chaton/settings/tellfriends/ad;

.field private f:Lcom/sec/chaton/settings/tellfriends/ah;

.field private g:Lcom/sec/chaton/settings/tellfriends/ac;

.field private h:Lcom/sec/chaton/settings/tellfriends/ae;

.field private i:Lcom/sec/chaton/settings/tellfriends/common/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/an;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/aa;-><init>()V

    .line 35
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ao;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/ao;-><init>(Lcom/sec/chaton/settings/tellfriends/an;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->i:Lcom/sec/chaton/settings/tellfriends/common/c;

    .line 28
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/an;->i:Lcom/sec/chaton/settings/tellfriends/common/c;

    invoke-direct {v0, p1, v1}, Lcom/sec/chaton/settings/tellfriends/ba;-><init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ad;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->e:Lcom/sec/chaton/settings/tellfriends/ad;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ac;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->g:Lcom/sec/chaton/settings/tellfriends/ac;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ae;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->h:Lcom/sec/chaton/settings/tellfriends/ae;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ah;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->f:Lcom/sec/chaton/settings/tellfriends/ah;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ag;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->d:Lcom/sec/chaton/settings/tellfriends/ag;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/af;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->c:Lcom/sec/chaton/settings/tellfriends/af;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/an;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->c()Ljava/lang/String;

    move-result-object v0

    .line 223
    if-eqz p1, :cond_0

    .line 224
    const-string v1, "_normal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 225
    const-string v1, "_normal"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 229
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->e()V

    .line 33
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ac;)V
    .locals 2

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/an;->g:Lcom/sec/chaton/settings/tellfriends/ac;

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    const/16 v1, 0x2ee6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->c(I)V

    .line 173
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ad;)V
    .locals 2

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/an;->e:Lcom/sec/chaton/settings/tellfriends/ad;

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    const/16 v1, 0x2ee1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->b(I)V

    .line 167
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/af;)V
    .locals 2

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/an;->c:Lcom/sec/chaton/settings/tellfriends/af;

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    const/16 v1, 0x2ee4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->a(I)V

    .line 203
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V
    .locals 3

    .prologue
    .line 182
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/an;->f:Lcom/sec/chaton/settings/tellfriends/ah;

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    const/16 v1, 0x2ee2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->a()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/an;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->f()Z

    move-result v0

    return v0
.end method
