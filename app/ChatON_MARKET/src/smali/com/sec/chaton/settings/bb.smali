.class Lcom/sec/chaton/settings/bb;
.super Ljava/lang/Object;
.source "ActivityNoti.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityNoti;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    const v8, 0x7f0b001f

    const/4 v3, 0x5

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 406
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 407
    packed-switch p2, :pswitch_data_0

    .line 438
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    iget-object v0, v0, Lcom/sec/chaton/settings/ActivityNoti;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 439
    return-void

    .line 409
    :pswitch_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 410
    const-wide/32 v2, 0x36ee80

    add-long/2addr v2, v0

    .line 411
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Setting mute hour start Long"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 412
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour end Long"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 413
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v8, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->h(Lcom/sec/chaton/settings/ActivityNoti;)V

    goto :goto_0

    .line 418
    :pswitch_1
    invoke-virtual {v6, v3, v7}, Ljava/util/Calendar;->add(II)V

    .line 419
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 420
    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v6, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x7

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 421
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 422
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 423
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v8, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->h(Lcom/sec/chaton/settings/ActivityNoti;)V

    goto/16 :goto_0

    .line 428
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->i(Lcom/sec/chaton/settings/ActivityNoti;)V

    goto/16 :goto_0

    .line 431
    :pswitch_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 432
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour start Long"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 433
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour end Long"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 434
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute repeat"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/settings/bb;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->h(Lcom/sec/chaton/settings/ActivityNoti;)V

    goto/16 :goto_0

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
