.class public abstract Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;
.super Landroid/support/v4/app/Fragment;
.source "SnsSubMenuFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/sec/chaton/settings/tellfriends/common/g;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field protected a:Landroid/widget/LinearLayout;

.field protected b:Landroid/widget/LinearLayout;

.field public c:Lcom/sec/chaton/settings/tellfriends/common/d;

.field protected d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;"
        }
    .end annotation
.end field

.field protected e:Z

.field protected f:Lcom/sec/chaton/settings/tellfriends/ad;

.field private h:Landroid/app/Activity;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/app/ProgressDialog;

.field private q:Lcom/sec/common/a/d;

.field private r:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/d;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->d:Ljava/util/ArrayList;

    .line 297
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/y;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/common/y;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->f:Lcom/sec/chaton/settings/tellfriends/ad;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->h:Landroid/app/Activity;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 240
    const-string v0, "showProgressDialog() \t- null. create and show"

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->h:Landroid/app/Activity;

    const/4 v1, 0x0

    const v2, 0x7f0b000d

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/w;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/w;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 264
    :goto_0
    return-void

    .line 261
    :cond_0
    const-string v0, "showProgressDialog() \t- not null. show"

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    .line 193
    if-ltz v0, :cond_0

    .line 194
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 195
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/d;

    const/4 v3, 0x5

    invoke-virtual {v2, v1, v0, v3}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(III)V

    .line 197
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->h:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/common/x;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/x;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 291
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 216
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 219
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 221
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    .line 224
    :cond_0
    return-void
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->q:Lcom/sec/common/a/d;

    .line 236
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 212
    iput-boolean p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->e:Z

    .line 213
    return-void
.end method

.method public abstract b()V
.end method

.method public e()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->m:Landroid/widget/TextView;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->e:Z

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i()V

    .line 228
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l()V

    .line 229
    return-void
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b(Z)V

    .line 268
    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    .line 271
    .line 272
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismissProgressDialog() \t- mProgressDialog : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". isSowing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->p:Landroid/app/ProgressDialog;

    .line 277
    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c(Z)V

    .line 295
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 74
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->h:Landroid/app/Activity;

    .line 75
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x8

    const v9, 0x7f07014c

    const v8, 0x7f0702d7

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 80
    const v0, 0x7f0300fe

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    .line 82
    iput-boolean v6, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->e:Z

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    const v1, 0x7f07029d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 86
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 87
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 88
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 89
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    const v5, 0x7f0202dc

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 90
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f02029a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    const v1, 0x7f070457

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 98
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 99
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 100
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 101
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    const v5, 0x7f0202dc

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 102
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f02029a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    const v1, 0x7f070459

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    const v1, 0x7f07045a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b:Landroid/widget/LinearLayout;

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    const v1, 0x7f070456

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a:Landroid/widget/LinearLayout;

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->k:Landroid/widget/TextView;

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->k:Landroid/widget/TextView;

    const v1, 0x7f0b000d

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->m:Landroid/widget/TextView;

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    const v1, 0x7f070458

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->n:Landroid/widget/LinearLayout;

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->o:Landroid/widget/TextView;

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->o:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b01a3

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (0)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setClickable(Z)V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 133
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/d;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->h:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->d:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/settings/tellfriends/common/d;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/d;

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Lcom/sec/chaton/settings/tellfriends/common/g;)V

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->notifyDataSetInvalidated()V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 143
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b()V

    .line 144
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->setHasOptionsMenu(Z)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a()V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->g()V

    .line 161
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 162
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->h:Landroid/app/Activity;

    .line 154
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 155
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 166
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 167
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->i()V

    .line 168
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 173
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->d()V

    .line 174
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 178
    if-nez p2, :cond_0

    .line 180
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c()V

    .line 184
    :cond_0
    return-void
.end method
