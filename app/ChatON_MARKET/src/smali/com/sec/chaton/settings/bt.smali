.class Lcom/sec/chaton/settings/bt;
.super Ljava/lang/Object;
.source "ActivityPrivacy.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->c(Lcom/sec/chaton/settings/ActivityPrivacy;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->d(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 282
    :goto_0
    return v5

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    iget-object v1, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    const v3, 0x7f0b0282

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings/ActivityPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings/ActivityPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    const v4, 0x7f0b0147

    invoke-virtual {v3, v4}, Lcom/sec/chaton/settings/ActivityPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    const v4, 0x7f0b0256

    invoke-virtual {v3, v4}, Lcom/sec/chaton/settings/ActivityPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/sec/chaton/settings/bt;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v3}, Lcom/sec/chaton/settings/ActivityPrivacy;->e(Lcom/sec/chaton/settings/ActivityPrivacy;)I

    move-result v3

    new-instance v4, Lcom/sec/chaton/settings/bu;

    invoke-direct {v4, p0}, Lcom/sec/chaton/settings/bu;-><init>(Lcom/sec/chaton/settings/bt;)V

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Lcom/sec/chaton/settings/ActivityPrivacy;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto :goto_0
.end method
