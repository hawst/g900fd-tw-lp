.class Lcom/sec/chaton/settings/tellfriends/common/y;
.super Ljava/lang/Object;
.source "SnsSubMenuFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/ad;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 354
    :goto_0
    return-void

    .line 348
    :cond_0
    const-string v0, "onCancelled get friend list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a(Z)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 302
    const-string v0, "onError get friend list"

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 314
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->g()V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->j()V

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a(Z)V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 341
    :goto_0
    return-void

    .line 321
    :cond_0
    invoke-static {p1}, Lcom/sec/chaton/settings/tellfriends/aa;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 322
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v1, v1, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c(Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    const v3, 0x7f0b01a3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 332
    :cond_2
    const-string v0, "friends.size() == 0 || friends == null"

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 340
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->a(Z)V

    goto :goto_0

    .line 336
    :cond_3
    const-string v0, "friends.size() != 0 || friends != null"

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/y;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->notifyDataSetChanged()V

    goto :goto_1
.end method
