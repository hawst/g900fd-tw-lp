.class Lcom/sec/chaton/settings/tellfriends/bs;
.super Landroid/webkit/WebViewClient;
.source "WeiboDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/bl;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/bl;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/bl;Lcom/sec/chaton/settings/tellfriends/bm;)V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bs;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "On Page Finished URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Ljava/lang/String;)V

    .line 260
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->c(Lcom/sec/chaton/settings/tellfriends/bl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->d(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->e(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->f(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 271
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageStarted URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->c(Lcom/sec/chaton/settings/tellfriends/bl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->d(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 300
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 301
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 323
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->b(Lcom/sec/chaton/settings/tellfriends/bl;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-direct {v1, p3, p2, p4}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->dismiss()V

    .line 327
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "shouldOverrideUrlLoading URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Ljava/lang/String;)V

    .line 306
    const-string v0, "chaton://WeiboSubMenuActivity"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->g(Lcom/sec/chaton/settings/tellfriends/bl;)Lcom/sec/chaton/settings/tellfriends/br;

    move-result-object v0

    if-nez v0, :cond_0

    .line 310
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    new-instance v0, Lcom/sec/chaton/settings/tellfriends/br;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-direct {v0, v2}, Lcom/sec/chaton/settings/tellfriends/br;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;)V

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/tellfriends/br;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/br;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Lcom/sec/chaton/settings/tellfriends/bl;Lcom/sec/chaton/settings/tellfriends/br;)Lcom/sec/chaton/settings/tellfriends/br;

    .line 318
    :cond_0
    :goto_0
    return v4

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bs;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
