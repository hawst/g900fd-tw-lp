.class public Lcom/sec/chaton/settings/tellfriends/br;
.super Landroid/os/AsyncTask;
.source "WeiboDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lweibo4android/http/AccessToken;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/bl;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/bl;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Lweibo4android/http/AccessToken;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 334
    const-string v0, "Called. GetAccessTokenTask - doInBackground()"

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Ljava/lang/String;)V

    .line 335
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 336
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 340
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/bl;->h(Lcom/sec/chaton/settings/tellfriends/bl;)Lweibo4android/http/RequestToken;

    move-result-object v1

    const-string v3, "oauth_verifier"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lweibo4android/http/RequestToken;->getAccessToken(Ljava/lang/String;)Lweibo4android/http/AccessToken;
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 341
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/bl;->i(Lcom/sec/chaton/settings/tellfriends/bl;)Lweibo4android/Weibo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lweibo4android/Weibo;->setOAuthAccessToken(Lweibo4android/http/AccessToken;)V
    :try_end_1
    .catch Lweibo4android/WeiboException; {:try_start_1 .. :try_end_1} :catch_1

    .line 348
    :goto_0
    return-object v0

    .line 342
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 343
    :goto_1
    invoke-virtual {v1}, Lweibo4android/WeiboException;->printStackTrace()V

    .line 344
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/bl;->b(Lcom/sec/chaton/settings/tellfriends/bl;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-virtual {v1}, Lweibo4android/WeiboException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/16 v5, -0x3ed

    invoke-direct {v4, v1, v5, v2}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    .line 345
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/bl;->dismiss()V

    goto :goto_0

    .line 342
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method protected a(Lweibo4android/http/AccessToken;)V
    .locals 1

    .prologue
    .line 353
    const-string v0, "Called. GetAccessTokenTask - onPostExecute()"

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->j(Lcom/sec/chaton/settings/tellfriends/bl;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->j(Lcom/sec/chaton/settings/tellfriends/bl;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->b(Lcom/sec/chaton/settings/tellfriends/bl;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Ljava/lang/Object;)V

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->dismiss()V

    .line 362
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 330
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/br;->a([Ljava/lang/String;)Lweibo4android/http/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 330
    check-cast p1, Lweibo4android/http/AccessToken;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/br;->a(Lweibo4android/http/AccessToken;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->d(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->d(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->j(Lcom/sec/chaton/settings/tellfriends/bl;)Ljava/util/Timer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Lcom/sec/chaton/settings/tellfriends/bl;Ljava/util/Timer;)Ljava/util/Timer;

    .line 379
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->j(Lcom/sec/chaton/settings/tellfriends/bl;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/bq;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/bq;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;Lcom/sec/chaton/settings/tellfriends/bm;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 380
    return-void

    .line 373
    :cond_1
    const-string v0, "mTimer.cancel()"

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Ljava/lang/String;)V

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->j(Lcom/sec/chaton/settings/tellfriends/bl;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Lcom/sec/chaton/settings/tellfriends/bl;Ljava/util/Timer;)Ljava/util/Timer;

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/br;->a:Lcom/sec/chaton/settings/tellfriends/bl;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bl;->a(Lcom/sec/chaton/settings/tellfriends/bl;Ljava/util/Timer;)Ljava/util/Timer;

    goto :goto_0
.end method
