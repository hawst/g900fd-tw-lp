.class public Lcom/sec/chaton/settings/ActivityManageAccounts;
.super Landroid/preference/PreferenceActivity;
.source "ActivityManageAccounts.java"


# instance fields
.field private a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Landroid/preference/Preference;

.field private f:Landroid/app/ProgressDialog;

.field private g:Ljava/lang/String;

.field private h:Landroid/content/Context;

.field private i:Lcom/sec/chaton/settings/tellfriends/aj;

.field private j:Lcom/sec/chaton/settings/tellfriends/an;

.field private k:Lcom/sec/chaton/settings/tellfriends/ap;

.field private l:Lcom/sec/chaton/settings/tellfriends/al;

.field private m:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private n:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private o:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private p:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private q:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 41
    const-class v0, Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->a:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->b:I

    .line 44
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->c:I

    .line 48
    const/16 v0, 0x7f99

    iput v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->d:I

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->f:Landroid/app/ProgressDialog;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityManageAccounts;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->h:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 184
    const-string v0, "pref_item_facebook"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->m:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 185
    const-string v0, "pref_item_twitter"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->n:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 186
    const-string v0, "pref_item_weibo"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->o:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 187
    const-string v0, "pref_item_renren"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->p:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 188
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->m:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 189
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->n:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 190
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->o:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 191
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->p:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 416
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 588
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    const v1, 0x7f0b0092

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    const v1, 0x7f030142

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setWidgetLayoutResource(I)V

    .line 591
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 592
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 609
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 612
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 614
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 615
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityManageAccounts;->startActivity(Landroid/content/Intent;)V

    .line 617
    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 470
    sparse-switch p1, :sswitch_data_0

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 474
    :sswitch_0
    const/4 v0, 0x0

    .line 476
    if-eqz p3, :cond_1

    .line 478
    const-string v0, "country_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 482
    :cond_1
    if-ne p2, v1, :cond_2

    .line 496
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "samsung_account_email"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/settings/ActivityManageAccounts;->a(Ljava/lang/String;)V

    .line 501
    if-eqz v0, :cond_0

    .line 503
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->h:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    goto :goto_0

    .line 509
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 517
    :sswitch_1
    if-ne p2, v1, :cond_3

    .line 520
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 521
    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->a(Ljava/lang/String;)V

    .line 523
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "email : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 528
    :cond_3
    const-string v0, "SIGNING_SSO : result is ERROR"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 533
    :sswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->i:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/aj;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 470
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x7f99 -> :sswitch_2
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_FROM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v1, "TWITTER_SUB_MENU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 169
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0x7f0b0092

    const/4 v3, 0x1

    .line 75
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    invoke-static {p0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 79
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->setTheme(I)V

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const v0, 0x7f05000d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->addPreferencesFromResource(I)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 95
    const-string v0, "pref_item_samsung_account"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setTitle(I)V

    .line 97
    iput-object p0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->h:Landroid/content/Context;

    .line 99
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->g:Ljava/lang/String;

    .line 123
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    const v1, 0x7f030142

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setWidgetLayoutResource(I)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(I)V

    .line 156
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->a()V

    .line 158
    return-void

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->e:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings/an;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/an;-><init>(Lcom/sec/chaton/settings/ActivityManageAccounts;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->i:Lcom/sec/chaton/settings/tellfriends/aj;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->i:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a()V

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->j:Lcom/sec/chaton/settings/tellfriends/an;

    if-eqz v0, :cond_1

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->j:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->a()V

    .line 660
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->k:Lcom/sec/chaton/settings/tellfriends/ap;

    if-eqz v0, :cond_2

    .line 661
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->k:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->a()V

    .line 663
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->l:Lcom/sec/chaton/settings/tellfriends/al;

    if-eqz v0, :cond_3

    .line 664
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->l:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->a()V

    .line 667
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 671
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->q:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    .line 672
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->q:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 674
    :cond_5
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 675
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 802
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 803
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->setResult(I)V

    .line 804
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->finish()V

    .line 805
    const/4 v0, 0x1

    .line 807
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 174
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 175
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->setResult(I)V

    .line 176
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->finish()V

    .line 177
    const/4 v0, 0x1

    .line 179
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 641
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 642
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 627
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 628
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_FROM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 632
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v1, "TWITTER_SUB_MENU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 636
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->b()V

    .line 637
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 621
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 647
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 648
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStop, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityManageAccounts;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 596
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 601
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityManageAccounts;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 606
    :goto_0
    return-void

    .line 603
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
