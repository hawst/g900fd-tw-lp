.class public Lcom/sec/chaton/settings/ActivityFontChange;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityFontChange.java"

# interfaces
.implements Lcom/sec/chaton/settings/do;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/chaton/settings/FragmentFontChange;

    invoke-direct {v0}, Lcom/sec/chaton/settings/FragmentFontChange;-><init>()V

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityFontChange;->finish()V

    .line 74
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 47
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityFontChange;->a(Landroid/app/Activity;)V

    .line 50
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 38
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityFontChange;->a(Landroid/app/Activity;)V

    .line 41
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 60
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityFontChange;->finish()V

    .line 68
    :goto_0
    return v0

    .line 63
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a7

    if-ne v1, v2, :cond_1

    .line 64
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/downloads/ActivityFontDownloads;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityFontChange;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 68
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
