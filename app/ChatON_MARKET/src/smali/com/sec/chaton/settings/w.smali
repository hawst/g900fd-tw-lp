.class Lcom/sec/chaton/settings/w;
.super Ljava/lang/Object;
.source "ActivityContactSyncFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    iget-boolean v0, v0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    if-nez v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    iput-boolean v3, v0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "contact_sim_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->b(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 191
    return v3

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    iput-boolean v2, v0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/settings/w;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "contact_sim_sync"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
