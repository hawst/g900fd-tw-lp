.class public Lcom/sec/chaton/settings/SnsSignInOutPreference;
.super Landroid/preference/Preference;
.source "SnsSignInOutPreference.java"


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/chaton/settings/ff;

.field c:Lcom/sec/chaton/settings/fe;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    sget-object v0, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    iput-object v0, p0, Lcom/sec/chaton/settings/SnsSignInOutPreference;->c:Lcom/sec/chaton/settings/fe;

    .line 36
    iput-object p1, p0, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/settings/fe;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/chaton/settings/SnsSignInOutPreference;->c:Lcom/sec/chaton/settings/fe;

    return-object v0
.end method

.method public a(Lcom/sec/chaton/settings/fe;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/settings/SnsSignInOutPreference;->c:Lcom/sec/chaton/settings/fe;

    if-eq v0, p1, :cond_0

    .line 101
    iput-object p1, p0, Lcom/sec/chaton/settings/SnsSignInOutPreference;->c:Lcom/sec/chaton/settings/fe;

    .line 103
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->notifyChanged()V

    .line 107
    :cond_0
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 43
    const v0, 0x7f070512

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 45
    const v1, 0x7f070513

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 47
    new-instance v2, Lcom/sec/chaton/settings/fb;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/fb;-><init>(Lcom/sec/chaton/settings/SnsSignInOutPreference;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    new-instance v2, Lcom/sec/chaton/settings/fc;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/fc;-><init>(Lcom/sec/chaton/settings/SnsSignInOutPreference;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    sget-object v2, Lcom/sec/chaton/settings/fd;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a()Lcom/sec/chaton/settings/fe;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/settings/fe;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 93
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 95
    return-void

    .line 73
    :pswitch_0
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 75
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 81
    :pswitch_1
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 83
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
