.class Lcom/sec/chaton/settings/f;
.super Ljava/lang/Object;
.source "AboutServiceFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/sec/chaton/settings/AboutServiceFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/AboutServiceFragment;I)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    iput p2, p0, Lcom/sec/chaton/settings/f;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 594
    :cond_0
    :goto_0
    return-void

    .line 555
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->b(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z

    .line 556
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {}, Lcom/sec/chaton/util/cb;->c()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z

    .line 558
    iget v0, p0, Lcom/sec/chaton/settings/f;->a:I

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    if-ne v0, v1, :cond_4

    .line 559
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 560
    invoke-static {v2}, Lcom/sec/chaton/util/cb;->c(I)V

    goto :goto_0

    .line 561
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->k(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 562
    invoke-static {v2}, Lcom/sec/chaton/util/cb;->a(I)V

    goto :goto_0

    .line 565
    :cond_3
    invoke-static {v2}, Lcom/sec/chaton/util/cb;->e(I)V

    goto :goto_0

    .line 569
    :cond_4
    iget v0, p0, Lcom/sec/chaton/settings/f;->a:I

    if-nez v0, :cond_7

    .line 570
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->k(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v1, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v3}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0041

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->g:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->c()V

    goto :goto_0

    .line 573
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 574
    iget v0, p0, Lcom/sec/chaton/settings/f;->a:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(I)V

    goto :goto_0

    .line 576
    :cond_6
    iget v0, p0, Lcom/sec/chaton/settings/f;->a:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->e(I)V

    goto/16 :goto_0

    .line 579
    :cond_7
    iget v0, p0, Lcom/sec/chaton/settings/f;->a:I

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    if-ne v0, v1, :cond_0

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->k(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 581
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/ChatONVUpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 582
    const-string v1, "isCritical"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 583
    const-string v1, "isFromHome"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 585
    iget-object v1, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 586
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->k(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 587
    invoke-static {v3}, Lcom/sec/chaton/util/cb;->a(I)V

    goto/16 :goto_0

    .line 588
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->k(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/settings/f;->b:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 589
    invoke-static {v3}, Lcom/sec/chaton/util/cb;->c(I)V

    goto/16 :goto_0

    .line 591
    :cond_a
    invoke-static {v3}, Lcom/sec/chaton/util/cb;->e(I)V

    goto/16 :goto_0
.end method
