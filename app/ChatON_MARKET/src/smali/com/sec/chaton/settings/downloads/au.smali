.class final enum Lcom/sec/chaton/settings/downloads/au;
.super Ljava/lang/Enum;
.source "AniconPackageDownloads.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/downloads/au;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/downloads/au;

.field public static final enum b:Lcom/sec/chaton/settings/downloads/au;

.field public static final enum c:Lcom/sec/chaton/settings/downloads/au;

.field public static final enum d:Lcom/sec/chaton/settings/downloads/au;

.field public static final enum e:Lcom/sec/chaton/settings/downloads/au;

.field private static final synthetic g:[Lcom/sec/chaton/settings/downloads/au;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    new-instance v0, Lcom/sec/chaton/settings/downloads/au;

    const-string v1, "INVALID_SORT"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/settings/downloads/au;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/au;->a:Lcom/sec/chaton/settings/downloads/au;

    .line 78
    new-instance v0, Lcom/sec/chaton/settings/downloads/au;

    const-string v1, "SORT_BY_TIME"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/chaton/settings/downloads/au;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/au;->b:Lcom/sec/chaton/settings/downloads/au;

    .line 80
    new-instance v0, Lcom/sec/chaton/settings/downloads/au;

    const-string v1, "SORT_BY_TITLE"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/settings/downloads/au;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/au;->c:Lcom/sec/chaton/settings/downloads/au;

    .line 81
    new-instance v0, Lcom/sec/chaton/settings/downloads/au;

    const-string v1, "SORT_BY_CHARACTER"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/settings/downloads/au;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/au;->d:Lcom/sec/chaton/settings/downloads/au;

    .line 82
    new-instance v0, Lcom/sec/chaton/settings/downloads/au;

    const-string v1, "SORT_BY_POPULARITY"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/chaton/settings/downloads/au;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/au;->e:Lcom/sec/chaton/settings/downloads/au;

    .line 76
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/settings/downloads/au;

    sget-object v1, Lcom/sec/chaton/settings/downloads/au;->a:Lcom/sec/chaton/settings/downloads/au;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/settings/downloads/au;->b:Lcom/sec/chaton/settings/downloads/au;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/settings/downloads/au;->c:Lcom/sec/chaton/settings/downloads/au;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/settings/downloads/au;->d:Lcom/sec/chaton/settings/downloads/au;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/settings/downloads/au;->e:Lcom/sec/chaton/settings/downloads/au;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/chaton/settings/downloads/au;->g:[Lcom/sec/chaton/settings/downloads/au;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 87
    iput p3, p0, Lcom/sec/chaton/settings/downloads/au;->f:I

    .line 88
    return-void
.end method

.method static a(I)Lcom/sec/chaton/settings/downloads/au;
    .locals 1

    .prologue
    .line 95
    packed-switch p0, :pswitch_data_0

    .line 105
    sget-object v0, Lcom/sec/chaton/settings/downloads/au;->a:Lcom/sec/chaton/settings/downloads/au;

    :goto_0
    return-object v0

    .line 97
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/au;->b:Lcom/sec/chaton/settings/downloads/au;

    goto :goto_0

    .line 99
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/settings/downloads/au;->c:Lcom/sec/chaton/settings/downloads/au;

    goto :goto_0

    .line 101
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/settings/downloads/au;->d:Lcom/sec/chaton/settings/downloads/au;

    goto :goto_0

    .line 103
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/settings/downloads/au;->e:Lcom/sec/chaton/settings/downloads/au;

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/au;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/sec/chaton/settings/downloads/au;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/au;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/downloads/au;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/sec/chaton/settings/downloads/au;->g:[Lcom/sec/chaton/settings/downloads/au;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/downloads/au;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/downloads/au;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/chaton/settings/downloads/au;->f:I

    return v0
.end method
