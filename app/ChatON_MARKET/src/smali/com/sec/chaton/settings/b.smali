.class Lcom/sec/chaton/settings/b;
.super Ljava/lang/Object;
.source "AboutServiceFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/AboutServiceFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/chaton/settings/b;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/b;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityWebView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 180
    const-string v1, "PARAM_MENU"

    const-string v2, "PRIVACY_POLICY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-string v1, "FROM_ABOUT_CHATON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 182
    iget-object v1, p0, Lcom/sec/chaton/settings/b;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->startActivity(Landroid/content/Intent;)V

    .line 183
    return-void
.end method
