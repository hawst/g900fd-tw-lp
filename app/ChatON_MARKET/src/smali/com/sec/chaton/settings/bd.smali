.class Lcom/sec/chaton/settings/bd;
.super Landroid/widget/BaseAdapter;
.source "ActivityNoti.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityNoti;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 631
    iput-object p1, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->e(Lcom/sec/chaton/settings/ActivityNoti;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->j(Lcom/sec/chaton/settings/ActivityNoti;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v1}, Lcom/sec/chaton/settings/ActivityNoti;->e(Lcom/sec/chaton/settings/ActivityNoti;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 645
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 650
    .line 651
    iget-object v0, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030123

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 652
    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 654
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 655
    const v2, 0x7f07014d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 656
    iget-object v3, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v3}, Lcom/sec/chaton/settings/ActivityNoti;->e(Lcom/sec/chaton/settings/ActivityNoti;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->j(Lcom/sec/chaton/settings/ActivityNoti;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/bd;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v3}, Lcom/sec/chaton/settings/ActivityNoti;->e(Lcom/sec/chaton/settings/ActivityNoti;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 659
    return-object v1
.end method
