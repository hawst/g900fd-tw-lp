.class Lcom/sec/chaton/settings/dq;
.super Landroid/os/Handler;
.source "FragmentMultiDeviceView.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/FragmentMultiDeviceView;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const v4, 0x7f0b002a

    const/4 v3, 0x0

    .line 155
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 157
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 160
    :sswitch_0
    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 164
    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 165
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;

    .line 167
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->mapping:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 168
    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->MappingInfo:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 169
    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->url:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;Ljava/lang/String;)Ljava/lang/String;

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "webURL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mMappingInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->e(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->d(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Lcom/sec/chaton/settings/dr;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 173
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->f(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 181
    :sswitch_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_3

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->g(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Lcom/sec/chaton/d/w;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->f(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 184
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 187
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/dq;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->f(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 157
    nop

    :sswitch_data_0
    .sparse-switch
        0x19d -> :sswitch_0
        0x7d7 -> :sswitch_1
    .end sparse-switch
.end method
