.class public Lcom/sec/chaton/settings/tellfriends/ba;
.super Lcom/sec/chaton/settings/tellfriends/common/o;
.source "TwitterManager.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static e:Ltwitter4j/Twitter;

.field private static f:Ltwitter4j/auth/RequestToken;


# instance fields
.field private g:Lcom/sec/chaton/settings/tellfriends/bd;

.field private h:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->a:Ljava/lang/String;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/chaton/c/b;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?oauth_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/settings/tellfriends/common/o;-><init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;Ljava/lang/String;)V

    .line 434
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/bb;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->h:Landroid/os/Handler;

    .line 43
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ltwitter4j/TwitterFactory;

    invoke-direct {v0}, Ltwitter4j/TwitterFactory;-><init>()V

    invoke-virtual {v0}, Ltwitter4j/TwitterFactory;->getInstance()Ltwitter4j/Twitter;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    .line 45
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/tellfriends/bj;->a(Ltwitter4j/Twitter;Landroid/content/Context;)Z

    .line 47
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/ba;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->h:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Ltwitter4j/Twitter;)Ltwitter4j/Twitter;
    .locals 0

    .prologue
    .line 32
    sput-object p0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    return-object p0
.end method

.method static synthetic a(Ltwitter4j/auth/RequestToken;)Ltwitter4j/auth/RequestToken;
    .locals 0

    .prologue
    .line 32
    sput-object p0, Lcom/sec/chaton/settings/tellfriends/ba;->f:Ltwitter4j/auth/RequestToken;

    return-object p0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-static {p0}, Lcom/sec/chaton/settings/tellfriends/bj;->a(Landroid/content/Context;)V

    .line 76
    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    .line 77
    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->f:Ltwitter4j/auth/RequestToken;

    .line 78
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g()Ltwitter4j/Twitter;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/ba;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic h()Ltwitter4j/auth/RequestToken;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->f:Ltwitter4j/auth/RequestToken;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 51
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ltwitter4j/TwitterFactory;

    invoke-direct {v0}, Ltwitter4j/TwitterFactory;-><init>()V

    invoke-virtual {v0}, Ltwitter4j/TwitterFactory;->getInstance()Ltwitter4j/Twitter;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    .line 53
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bj;->a(Ltwitter4j/Twitter;Landroid/content/Context;)Z

    .line 56
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->f:Ltwitter4j/auth/RequestToken;

    if-nez v0, :cond_1

    .line 57
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/tellfriends/bf;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;Lcom/sec/chaton/settings/tellfriends/bb;)V

    .line 58
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    const/16 v2, 0x2ee5

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_1
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ba;->f:Ltwitter4j/auth/RequestToken;

    invoke-virtual {v0}, Ltwitter4j/auth/RequestToken;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/ba;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 129
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p3, v1}, Lcom/sec/chaton/settings/tellfriends/bh;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/bb;)V

    .line 130
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 131
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 86
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ar;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/settings/tellfriends/ba;->e:Ltwitter4j/Twitter;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/chaton/settings/tellfriends/ba;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/settings/tellfriends/bg;

    const/16 v5, 0x2ee4

    invoke-direct {v4, p0, v5}, Lcom/sec/chaton/settings/tellfriends/bg;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;I)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/settings/tellfriends/ar;-><init>(Landroid/content/Context;Ltwitter4j/Twitter;Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;)V

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->show()V

    .line 87
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bj;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bj;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->g:Lcom/sec/chaton/settings/tellfriends/bd;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bd;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/tellfriends/bd;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;Lcom/sec/chaton/settings/tellfriends/bb;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->g:Lcom/sec/chaton/settings/tellfriends/bd;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->g:Lcom/sec/chaton/settings/tellfriends/bd;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bd;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/ba;->g:Lcom/sec/chaton/settings/tellfriends/bd;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 113
    :cond_1
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bj;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(I)V
    .locals 4

    .prologue
    .line 117
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/tellfriends/bc;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;Lcom/sec/chaton/settings/tellfriends/bb;)V

    .line 118
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 119
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/bj;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->g:Lcom/sec/chaton/settings/tellfriends/bd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ba;->g:Lcom/sec/chaton/settings/tellfriends/bd;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
