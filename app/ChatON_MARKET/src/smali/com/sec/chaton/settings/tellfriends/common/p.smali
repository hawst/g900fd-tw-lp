.class public abstract Lcom/sec/chaton/settings/tellfriends/common/p;
.super Ljava/lang/Object;
.source "SnsManager.java"


# instance fields
.field private a:Ljava/lang/String;

.field protected e:I

.field protected f:Ljava/lang/Object;

.field final synthetic g:Lcom/sec/chaton/settings/tellfriends/common/o;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->g:Lcom/sec/chaton/settings/tellfriends/common/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->e:I

    .line 161
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->f:Ljava/lang/Object;

    .line 164
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->a:Ljava/lang/String;

    .line 165
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->f:Ljava/lang/Object;

    .line 166
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/p;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/p;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->f:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
