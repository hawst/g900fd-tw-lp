.class public Lcom/sec/chaton/settings/ei;
.super Landroid/support/v4/widget/CursorAdapter;
.source "FragmentSkinChange3Adapter.java"


# static fields
.field private static f:Landroid/graphics/drawable/BitmapDrawable;


# instance fields
.field a:Landroid/view/LayoutInflater;

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Lcom/sec/common/f/c;

.field private e:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/sec/common/f/c;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 46
    iput-object p1, p0, Lcom/sec/chaton/settings/ei;->b:Landroid/content/Context;

    .line 47
    iput-object p3, p0, Lcom/sec/chaton/settings/ei;->d:Lcom/sec/common/f/c;

    .line 48
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/settings/ei;->a:Landroid/view/LayoutInflater;

    .line 50
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/Application;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/skins/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ei;->e:Ljava/io/File;

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    iput-object v3, p0, Lcom/sec/chaton/settings/ei;->e:Ljava/io/File;

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/graphics/drawable/BitmapDrawable;
    .locals 5

    .prologue
    const v4, 0x7f0201ca

    .line 64
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/ei;->e:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 65
    sget-object v0, Lcom/sec/chaton/settings/ei;->f:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/chaton/settings/ei;->b:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings/ei;->e:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "skin_myskin.png_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    sput-object v0, Lcom/sec/chaton/settings/ei;->f:Landroid/graphics/drawable/BitmapDrawable;

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/ei;->f:Landroid/graphics/drawable/BitmapDrawable;

    .line 75
    :goto_0
    return-object v0

    .line 70
    :cond_1
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/chaton/settings/ei;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201ca

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 75
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/chaton/settings/ei;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/chaton/settings/ei;->c:I

    .line 60
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 83
    iget v0, p0, Lcom/sec/chaton/settings/ei;->c:I

    sget v1, Lcom/sec/chaton/settings/FragmentSkinChange3;->b:I

    if-ne v0, v1, :cond_2

    .line 84
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/ej;

    .line 85
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 86
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 87
    const-string v1, "item_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    .line 88
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "setting_change_bubble_receive"

    const-string v3, "-1"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 94
    :goto_0
    new-instance v1, Lcom/sec/chaton/settings/cl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recv_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/ei;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/cl;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 95
    iget-object v2, p0, Lcom/sec/chaton/settings/ei;->d:Lcom/sec/common/f/c;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 152
    :cond_0
    :goto_1
    return-void

    .line 92
    :cond_1
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 96
    :cond_2
    iget v0, p0, Lcom/sec/chaton/settings/ei;->c:I

    sget v1, Lcom/sec/chaton/settings/FragmentSkinChange3;->a:I

    if-ne v0, v1, :cond_4

    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/ej;

    .line 98
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 100
    const-string v1, "item_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    .line 101
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "setting_change_bubble_send"

    const-string v3, "-1"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 102
    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    :goto_2
    new-instance v1, Lcom/sec/chaton/settings/cm;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "send_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/ei;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/cm;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 108
    iget-object v2, p0, Lcom/sec/chaton/settings/ei;->d:Lcom/sec/common/f/c;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_1

    .line 105
    :cond_3
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 109
    :cond_4
    iget v0, p0, Lcom/sec/chaton/settings/ei;->c:I

    sget v1, Lcom/sec/chaton/settings/FragmentSkinChange3;->c:I

    if-ne v0, v1, :cond_0

    .line 110
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/ej;

    .line 111
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 112
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 113
    const-string v1, "item_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    .line 114
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    const-string v2, "skin_add"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 115
    iget-object v1, p0, Lcom/sec/chaton/settings/ei;->d:Lcom/sec/common/f/c;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 116
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    const v2, 0x7f0203d6

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 117
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 118
    const-string v1, "ma"

    iput-object v1, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    .line 119
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 121
    :cond_5
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    const-string v2, "skin_myskin.png_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 122
    iget-object v1, p0, Lcom/sec/chaton/settings/ei;->d:Lcom/sec/common/f/c;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 123
    const-string v1, "ma"

    iput-object v1, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    .line 124
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ei;->a()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 125
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 126
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/chaton/settings/ei;->f:Landroid/graphics/drawable/BitmapDrawable;

    .line 140
    :goto_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "setting_change_skin"

    const-string v3, "-1"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 141
    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 142
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 130
    :cond_6
    :try_start_0
    const-string v1, "extras"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/ab;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ac;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Lcom/sec/chaton/e/a/ac;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_4
    new-instance v1, Lcom/sec/chaton/settings/ck;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bg_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/ei;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/ck;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 136
    iget-object v2, p0, Lcom/sec/chaton/settings/ei;->d:Lcom/sec/common/f/c;

    iget-object v3, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    invoke-virtual {v2, v3, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 137
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_3

    .line 132
    :catch_0
    move-exception v1

    .line 133
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 144
    :cond_7
    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 159
    new-instance v1, Lcom/sec/chaton/settings/ej;

    invoke-direct {v1}, Lcom/sec/chaton/settings/ej;-><init>()V

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/settings/ei;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030050

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 161
    const v0, 0x7f0701ee

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/settings/ej;->f:Landroid/widget/ImageView;

    .line 162
    const v0, 0x7f0701f0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/settings/ej;->g:Landroid/widget/ImageView;

    .line 163
    const v0, 0x7f0701ed

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/sec/chaton/settings/ej;->d:Landroid/widget/LinearLayout;

    .line 164
    const v0, 0x7f0701ef

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/sec/chaton/settings/ej;->e:Landroid/widget/LinearLayout;

    .line 165
    const v0, 0x7f0701f1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/settings/ej;->h:Landroid/widget/ImageView;

    .line 166
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 167
    return-object v2
.end method
