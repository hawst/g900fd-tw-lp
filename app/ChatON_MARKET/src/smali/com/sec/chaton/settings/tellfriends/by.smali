.class Lcom/sec/chaton/settings/tellfriends/by;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "WeiboManager.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/bt;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;)V
    .locals 1

    .prologue
    .line 329
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/by;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    .line 330
    const-string v0, "GetRequestTokenTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 331
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;Lcom/sec/chaton/settings/tellfriends/bu;)V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/by;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/16 v3, -0x3e8

    .line 335
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/by;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    const-string v2, "GetRequestTokenTask() : Get Request Token"

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/tellfriends/bt;->b(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V

    .line 340
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/bt;->f()Lweibo4android/Weibo;

    move-result-object v1

    const-string v2, "chaton://WeiboSubMenuActivity"

    invoke-virtual {v1, v2}, Lweibo4android/Weibo;->getOAuthRequestToken(Ljava/lang/String;)Lweibo4android/http/RequestToken;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/bt;->b(Lweibo4android/http/RequestToken;)Lweibo4android/http/RequestToken;

    .line 342
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/bt;->g()Lweibo4android/http/RequestToken;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/bt;->g()Lweibo4android/http/RequestToken;

    move-result-object v1

    invoke-virtual {v1}, Lweibo4android/http/RequestToken;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/by;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    const-string v2, "Incorrect Request Token!"

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/tellfriends/bt;->c(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V

    .line 344
    const/16 v1, -0x3e8

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/by;->e:I

    .line 345
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/by;->f:Ljava/lang/Object;
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 360
    :goto_0
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/by;->f:Ljava/lang/Object;

    .line 361
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/by;->e:I

    return v0

    .line 347
    :cond_1
    const/4 v0, -0x1

    :try_start_1
    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/by;->e:I

    .line 348
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/bt;->g()Lweibo4android/http/RequestToken;

    move-result-object v0

    invoke-virtual {v0}, Lweibo4android/http/RequestToken;->getToken()Ljava/lang/String;
    :try_end_1
    .catch Lweibo4android/WeiboException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 350
    :catch_0
    move-exception v0

    .line 351
    :try_start_2
    invoke-virtual {v0}, Lweibo4android/WeiboException;->printStackTrace()V

    .line 352
    const/16 v1, -0x3ed

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/by;->e:I

    .line 353
    invoke-virtual {v0}, Lweibo4android/WeiboException;->getMessage()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_0

    .line 355
    :catch_1
    move-exception v0

    .line 356
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 357
    iput v3, p0, Lcom/sec/chaton/settings/tellfriends/by;->e:I

    .line 358
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
