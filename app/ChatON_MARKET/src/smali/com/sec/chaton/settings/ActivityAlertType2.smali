.class public Lcom/sec/chaton/settings/ActivityAlertType2;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "ActivityAlertType2.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private a:Lcom/sec/chaton/util/ab;

.field private b:Landroid/preference/CheckBoxPreference;

.field private c:Landroid/preference/CheckBoxPreference;

.field private d:Landroid/preference/CheckBoxPreference;

.field private e:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 103
    invoke-static {p0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/chat/notification/a;->i()Ljava/lang/String;

    move-result-object v2

    .line 107
    const-string v3, "ALL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    move v2, v1

    .line 125
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 126
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->b:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v3, "LED Indicator"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 129
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityAlertType2;->a(Ljava/lang/String;Landroid/preference/Preference;Z)V

    .line 130
    return-void

    .line 111
    :cond_0
    const-string v3, "MELODY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    .line 114
    goto :goto_0

    .line 115
    :cond_1
    const-string v3, "VIBRATION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    move v0, v1

    .line 118
    goto :goto_0

    :cond_2
    move v2, v0

    .line 122
    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 255
    if-eqz p1, :cond_1

    .line 256
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 257
    if-nez p3, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 260
    :cond_0
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 261
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 262
    invoke-virtual {p2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 264
    :cond_1
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v1, "Ringtone title"

    const v2, 0x7f0b01a5

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/ActivityAlertType2;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v1, "Ringtone"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 176
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 177
    const-string v0, "content://"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_6

    .line 180
    invoke-virtual {v0, p0}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_0
    :goto_0
    return-object v0

    .line 182
    :cond_1
    const-string v0, "android.resource://"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 183
    invoke-static {}, Lcom/sec/chaton/settings/downloads/az;->a()Ljava/util/Map;

    move-result-object v0

    .line 184
    const/4 v3, 0x0

    .line 185
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 186
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/az;

    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/az;->s:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 187
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/az;

    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/az;->o:Ljava/lang/String;

    .line 188
    const/4 v0, 0x1

    move v1, v0

    move-object v0, v2

    .line 192
    :goto_1
    if-nez v1, :cond_0

    .line 193
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 196
    :cond_3
    const-string v0, "file://"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 197
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x7

    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    .line 199
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->c()Ljava/lang/String;

    move-result-object v2

    :cond_4
    move-object v0, v2

    .line 201
    goto :goto_0

    .line 203
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-object v0, v2

    goto :goto_0

    :cond_7
    move v1, v3

    move-object v0, v2

    goto :goto_1
.end method

.method private c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 212
    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    .line 213
    const/4 v0, 0x0

    .line 215
    if-eqz v1, :cond_0

    .line 216
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v3, "Ringtone"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-static {p0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    .line 219
    if-eqz v1, :cond_0

    .line 220
    invoke-virtual {v1, p0}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v2, "Ringtone title"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityAlertType2;->addPreferencesFromResource(I)V

    .line 56
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    .line 58
    const-string v0, "pref_item_led"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityAlertType2;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->b:Landroid/preference/CheckBoxPreference;

    .line 59
    const-string v0, "pref_item_sound"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityAlertType2;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->c:Landroid/preference/CheckBoxPreference;

    .line 60
    const-string v0, "pref_item_vibration"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityAlertType2;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    .line 61
    const-string v0, "pref_item_sound_type"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityAlertType2;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    .line 63
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    .line 64
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 65
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->b:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 76
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    check-cast v0, Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->b()V

    .line 248
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onDestroy()V

    .line 249
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 233
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onPause()V

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    check-cast v0, Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->a()V

    .line 240
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->c:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_2

    move-object v0, p2

    .line 140
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 141
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/chaton/settings/ActivityAlertType2;->a(Ljava/lang/String;Landroid/preference/Preference;Z)V

    move v4, v1

    move v1, v0

    move v0, v4

    .line 146
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->c:Landroid/preference/CheckBoxPreference;

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_6

    .line 147
    :cond_0
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "ALL"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_1
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->d:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_a

    move-object v0, p2

    .line 143
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v1, v2

    goto :goto_0

    .line 149
    :cond_3
    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "MELODY"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 151
    :cond_4
    if-nez v1, :cond_5

    if-eqz v0, :cond_5

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "VIBRATION"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 154
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "OFF"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 156
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    if-ne p1, v0, :cond_9

    .line 157
    check-cast p2, Ljava/lang/String;

    .line 158
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_8

    .line 159
    :cond_7
    const-string p2, "Silent"

    .line 161
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v2, "Ringtone"

    invoke-virtual {v0, v2, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 163
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->e:Landroid/preference/Preference;

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/chaton/settings/ActivityAlertType2;->a(Ljava/lang/String;Landroid/preference/Preference;Z)V

    goto :goto_1

    .line 164
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->b:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityAlertType2;->a:Lcom/sec/chaton/util/ab;

    const-string v1, "LED Indicator"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    :cond_a
    move v0, v1

    move v1, v2

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onResume()V

    .line 81
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->a()V

    .line 82
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 87
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onStart()V

    .line 88
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 93
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityAlertType2;->finish()V

    .line 95
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
