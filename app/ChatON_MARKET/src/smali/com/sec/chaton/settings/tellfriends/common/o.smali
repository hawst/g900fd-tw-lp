.class public abstract Lcom/sec/chaton/settings/tellfriends/common/o;
.super Ljava/lang/Object;
.source "SnsManager.java"


# instance fields
.field private a:Ljava/lang/String;

.field public b:Landroid/app/Activity;

.field public c:Lcom/sec/chaton/settings/tellfriends/common/c;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/common/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->d:Ljava/util/ArrayList;

    .line 24
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->b:Landroid/app/Activity;

    .line 25
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    .line 26
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->a:Ljava/lang/String;

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/o;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->d:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method protected b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "killAllTask() \t- BlockingTaskListCount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\tBlockingTaskList : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/o;->b(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/common/q;

    .line 52
    if-eqz v0, :cond_0

    .line 53
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->cancel(Z)Z

    goto :goto_0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 57
    return-void
.end method
