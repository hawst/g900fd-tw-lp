.class public Lcom/sec/chaton/settings/ActivityBgBubbleChange;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityBgBubbleChange.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-direct {v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;-><init>()V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 56
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityBgBubbleChange;->a(Landroid/app/Activity;)V

    .line 59
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f0b0242

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityBgBubbleChange;->setTitle(I)V

    .line 21
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 34
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityBgBubbleChange;->a(Landroid/app/Activity;)V

    .line 37
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 41
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityBgBubbleChange;->finish()V

    .line 43
    const/4 v0, 0x1

    .line 48
    :goto_0
    return v0

    .line 44
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a7

    if-ne v0, v1, :cond_1

    .line 45
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivitySkinDownloads;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityBgBubbleChange;->startActivity(Landroid/content/Intent;)V

    .line 48
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
