.class public Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;
.source "FacebookSubMenuFragment.java"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private h:Landroid/view/View;

.field private i:Landroid/app/Activity;

.field private j:Lcom/sec/chaton/settings/tellfriends/aj;

.field private k:Lcom/sec/chaton/settings/tellfriends/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;-><init>()V

    .line 159
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/d;-><init>(Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->k:Lcom/sec/chaton/settings/tellfriends/ah;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;)Lcom/sec/chaton/settings/tellfriends/aj;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->i:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->a(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->c()V

    .line 102
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->h()V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/c;-><init>(Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aj;->a(Lcom/sec/chaton/settings/tellfriends/af;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ai;)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->k:Lcom/sec/chaton/settings/tellfriends/ah;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/aj;->a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V

    .line 121
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->e()Landroid/widget/TextView;

    move-result-object v0

    .line 107
    const v1, 0x7f0b01f8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 108
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->f:Lcom/sec/chaton/settings/tellfriends/ad;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aj;->a(Lcom/sec/chaton/settings/tellfriends/ad;)V

    .line 116
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 142
    const/16 v0, 0xbb9

    if-ne p1, v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/aj;->a(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-direct {v0, p1}, Lcom/sec/chaton/settings/tellfriends/aj;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    .line 30
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onAttach(Landroid/app/Activity;)V

    .line 31
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->i:Landroid/app/Activity;

    .line 32
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 136
    :goto_0
    return-void

    .line 127
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->i:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 128
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 131
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->i:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/tellfriends/FacebookMessageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    const-string v1, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 125
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f07029d -> :sswitch_1
        0x7f070457 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->h:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->h:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a()V

    .line 62
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->g()V

    .line 63
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDestroy()V

    .line 64
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDetach()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->i:Landroid/app/Activity;

    .line 39
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onPause()V

    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookSubMenuFragment;->i()V

    .line 57
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 156
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onScroll(Landroid/widget/AbsListView;III)V

    .line 157
    return-void
.end method
