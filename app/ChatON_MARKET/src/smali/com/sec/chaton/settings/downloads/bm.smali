.class public Lcom/sec/chaton/settings/downloads/bm;
.super Landroid/widget/CursorTreeAdapter;
.source "FontListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/settings/downloads/a/q;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/settings/downloads/aw;

.field private c:I

.field private d:I

.field private e:Lcom/sec/common/f/c;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/bs;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/sec/chaton/d/k;

.field private h:Lcom/sec/chaton/settings/downloads/br;

.field private i:Landroid/os/Handler;

.field private j:Landroid/view/View$OnLongClickListener;

.field private k:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/chaton/settings/downloads/bm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;Lcom/sec/common/f/c;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Landroid/widget/CursorTreeAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 55
    new-instance v0, Lcom/sec/chaton/settings/downloads/bn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/bn;-><init>(Lcom/sec/chaton/settings/downloads/bm;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->i:Landroid/os/Handler;

    .line 351
    new-instance v0, Lcom/sec/chaton/settings/downloads/bo;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/bo;-><init>(Lcom/sec/chaton/settings/downloads/bm;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->j:Landroid/view/View$OnLongClickListener;

    .line 374
    new-instance v0, Lcom/sec/chaton/settings/downloads/bp;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/bp;-><init>(Lcom/sec/chaton/settings/downloads/bm;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->k:Landroid/view/View$OnClickListener;

    .line 99
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/bm;->c:I

    .line 100
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/bm;->d:I

    .line 102
    iput-object p3, p0, Lcom/sec/chaton/settings/downloads/bm;->e:Lcom/sec/common/f/c;

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->f:Ljava/util/HashMap;

    .line 105
    new-instance v0, Lcom/sec/chaton/d/k;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/sec/chaton/d/k;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->g:Lcom/sec/chaton/d/k;

    .line 106
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/bm;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/bm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/bm;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->h:Lcom/sec/chaton/settings/downloads/br;

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->h:Lcom/sec/chaton/settings/downloads/br;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/settings/downloads/br;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 436
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/i;

    .line 438
    if-nez v0, :cond_1

    .line 439
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bm;->g:Lcom/sec/chaton/d/k;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/i;-><init>(Lcom/sec/chaton/d/k;Ljava/lang/String;)V

    .line 440
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 442
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/i;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bm;->g:Lcom/sec/chaton/d/k;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/i;-><init>(Lcom/sec/chaton/d/k;Ljava/lang/String;)V

    .line 443
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_0

    .line 444
    :cond_2
    if-eqz p2, :cond_0

    .line 445
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 449
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 450
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_font, download all - requestInstall : cancel(), id/status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/i;->a(Z)Z

    goto :goto_0

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/bm;)Lcom/sec/chaton/settings/downloads/br;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->h:Lcom/sec/chaton/settings/downloads/br;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(I)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 119
    .line 122
    if-lez p1, :cond_3

    .line 123
    const-string v3, "reference_id=?"

    .line 124
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 127
    :goto_0
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "special"

    aput-object v1, v0, v6

    const-string v1, " DESC,"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string v5, "item_id"

    aput-object v5, v0, v1

    const/4 v1, 0x3

    const-string v5, " DESC"

    aput-object v5, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 132
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "download_font, getChildrenCursor(), key/value : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "(null)"

    :goto_1
    sget-object v1, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 139
    return-object v0

    .line 134
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "(null)"

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v1, v4, v6

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " orderyBy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v4, v2

    move-object v3, v2

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;I)Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 584
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onChangedChildCursor(), filter id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_0
    if-nez p1, :cond_2

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 590
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->a()V

    .line 626
    :cond_1
    :goto_0
    return-object p1

    .line 593
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 594
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 596
    const-string v0, "item_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 597
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 599
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 600
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 601
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 603
    :goto_2
    if-eqz v0, :cond_4

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v4, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 601
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .line 609
    :cond_4
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v4, v3}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/i;

    .line 610
    if-nez v0, :cond_5

    .line 611
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v4, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 616
    :cond_5
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v3, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Lcom/sec/chaton/settings/downloads/a/l;)Z

    goto :goto_1

    .line 623
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->b()V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bm;->notifyDataSetChanged()V

    .line 638
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 644
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/aw;)V
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    .line 631
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/br;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/bm;->h:Lcom/sec/chaton/settings/downloads/br;

    .line 413
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 421
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 428
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/e/ar;)V

    .line 429
    return-void
.end method

.method public b(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 649
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 650
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 657
    :cond_0
    return-void
.end method

.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 15

    .prologue
    .line 209
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/bs;

    .line 210
    if-nez p3, :cond_1

    .line 211
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 212
    const-string v1, "download_font, bindChildView(), cursor is (null)"

    sget-object v2, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    const-string v2, "extras"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 218
    const/4 v2, 0x0

    .line 222
    :try_start_0
    invoke-static {v3}, Lcom/sec/chaton/e/a/k;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/l;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 230
    :cond_2
    :goto_1
    const-string v3, "item_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 231
    iput-object v7, v1, Lcom/sec/chaton/settings/downloads/bs;->a:Ljava/lang/String;

    .line 232
    const-string v3, "name"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 233
    iput-object v8, v1, Lcom/sec/chaton/settings/downloads/bs;->i:Ljava/lang/String;

    .line 234
    const/4 v5, 0x0

    .line 235
    const-wide/16 v3, 0x0

    .line 236
    if-eqz v2, :cond_3

    .line 237
    iget-object v4, v2, Lcom/sec/chaton/e/a/l;->b:Ljava/lang/String;

    .line 239
    iget-wide v2, v2, Lcom/sec/chaton/e/a/l;->d:J

    move-object v5, v4

    move-wide v13, v2

    move-wide v3, v13

    .line 241
    :cond_3
    const-string v2, "install"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x0

    move v6, v2

    .line 242
    :goto_2
    iput-boolean v6, v1, Lcom/sec/chaton/settings/downloads/bs;->j:Z

    .line 243
    const-string v2, "new"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 244
    const-string v2, "special"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 247
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/bm;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 248
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/bm;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v2

    sget-object v11, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2, v11, v7}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/settings/downloads/a/i;

    .line 253
    if-eqz v2, :cond_4

    .line 254
    iget-object v11, p0, Lcom/sec/chaton/settings/downloads/bm;->i:Landroid/os/Handler;

    invoke-virtual {v2, v11}, Lcom/sec/chaton/settings/downloads/a/i;->a(Landroid/os/Handler;)V

    .line 257
    :cond_4
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    iput v11, v1, Lcom/sec/chaton/settings/downloads/bs;->b:I

    .line 258
    iget-object v11, v1, Lcom/sec/chaton/settings/downloads/bs;->e:Landroid/widget/TextView;

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v8, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    iget-object v8, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    const/16 v11, 0x8

    invoke-virtual {v8, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 262
    if-eqz v6, :cond_7

    .line 265
    invoke-static {v7}, Lcom/sec/chaton/settings/downloads/bj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 266
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const v6, 0x7f0201ce

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 267
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 274
    :goto_3
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v11, 0x3e8

    div-long/2addr v3, v11

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x1

    const-string v4, "KB / "

    aput-object v4, v6, v3

    const/4 v3, 0x2

    const v4, 0x7f0b01d7

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v6}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/bj;->b(Landroid/widget/TextView;)V

    .line 331
    :goto_4
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 333
    if-eqz v10, :cond_c

    .line 334
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 336
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    const v3, 0x7f0b01dd

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 337
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    const-string v3, "#3eb1b9"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 347
    :goto_5
    new-instance v2, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    iget v3, p0, Lcom/sec/chaton/settings/downloads/bm;->c:I

    iget v4, p0, Lcom/sec/chaton/settings/downloads/bm;->d:I

    invoke-direct {v2, v5, v3, v4}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;II)V

    .line 348
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/bm;->e:Lcom/sec/common/f/c;

    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/bs;->c:Landroid/widget/ImageView;

    invoke-virtual {v3, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 223
    :catch_0
    move-exception v3

    .line 224
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_2

    .line 225
    sget-object v4, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 241
    :cond_5
    const/4 v2, 0x1

    move v6, v2

    goto/16 :goto_2

    .line 270
    :cond_6
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const v6, 0x7f0201cd

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 271
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_3

    .line 277
    :cond_7
    if-nez v2, :cond_8

    .line 278
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const v6, 0x7f0201d6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 279
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 280
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 281
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v11, 0x3e8

    div-long/2addr v3, v11

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x1

    const-string v4, "KB"

    aput-object v4, v6, v3

    invoke-static {v6}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/bj;->a(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 285
    :cond_8
    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_9

    .line 286
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const v6, 0x7f0201d9

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 287
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 288
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v11, 0x3e8

    div-long/2addr v3, v11

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x1

    const-string v4, "KB"

    aput-object v4, v6, v3

    invoke-static {v6}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/bj;->a(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 292
    :cond_9
    iget-object v6, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 294
    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/i;->d()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_4

    .line 296
    :pswitch_0
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const v3, 0x7f0201d2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 299
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v2

    if-nez v2, :cond_a

    .line 300
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 303
    :cond_a
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v6, " ("

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const v6, 0x7f0b022a

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-string v6, ")"

    aput-object v6, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 308
    :pswitch_1
    iget-object v6, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const v7, 0x7f0201d2

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 311
    iget-object v6, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v6}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 312
    iget-object v6, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 314
    :cond_b
    iget-object v6, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/i;->g()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v6, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 316
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v11, 0x3e8

    div-long/2addr v3, v11

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x1

    const-string v4, "KB"

    aput-object v4, v6, v3

    invoke-static {v6}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 321
    :pswitch_2
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 322
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 324
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    const v3, 0x7f0b022b

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/bj;->d(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 338
    :cond_c
    if-eqz v9, :cond_d

    .line 339
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 341
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    const v3, 0x7f0b01dc

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 342
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    const-string v3, "#e86d00"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto/16 :goto_5

    .line 344
    :cond_d
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 294
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/bq;

    .line 176
    const-string v1, "filter_title"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 177
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, v0, Lcom/sec/chaton/settings/downloads/bq;->a:I

    .line 178
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/bq;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 432
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->c(Lcom/sec/chaton/e/ar;)V

    .line 433
    return-void
.end method

.method public c(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bm;->notifyDataSetChanged()V

    .line 664
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 665
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 668
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 675
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 665
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, -0x1

    .line 465
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 466
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bm;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 467
    invoke-interface {v3, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 468
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 469
    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/downloads/bm;->getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v4

    .line 470
    if-eqz v4, :cond_0

    .line 474
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 475
    const-string v0, "filter_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 476
    const-string v5, "filter_title"

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 477
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 478
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "download_font, download all, (LOOP. GROUP - start) filter_id/count/title: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_1
    invoke-interface {v4, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 482
    :cond_2
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 483
    const-string v0, "item_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 484
    const-string v0, "name"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 485
    const-string v0, "install"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 486
    :goto_2
    sget-boolean v7, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v7, :cond_3

    .line 487
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "download_font, download all, (LOOP. CHILD) id/name/install : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    :cond_3
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    if-nez v0, :cond_2

    .line 492
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 485
    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    .line 496
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 497
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "download_font, download all, (LOOP. GROUP - end) current list added : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    :cond_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 502
    :cond_7
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 503
    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/bm;->a(Ljava/lang/String;Z)V

    goto :goto_3

    .line 505
    :cond_8
    return-void
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 111
    const-string v0, "filter_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 112
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/bm;->a(I)Landroid/database/Cursor;

    move-result-object v1

    .line 113
    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/settings/downloads/bm;->a(Landroid/database/Cursor;I)Landroid/database/Cursor;

    .line 114
    return-object v1
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 150
    if-eqz p4, :cond_0

    instance-of v0, p4, Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    move-object v0, p4

    .line 151
    check-cast v0, Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 153
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/CursorTreeAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 184
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 186
    const v1, 0x7f03004a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->j:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 191
    new-instance v2, Lcom/sec/chaton/settings/downloads/bs;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/settings/downloads/bs;-><init>(Lcom/sec/chaton/settings/downloads/bm;Lcom/sec/chaton/settings/downloads/bn;)V

    .line 192
    const v0, 0x7f0701dc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bs;->c:Landroid/widget/ImageView;

    .line 193
    const v0, 0x7f0701df

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bs;->e:Landroid/widget/TextView;

    .line 195
    const v0, 0x7f0701e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bs;->d:Landroid/widget/TextView;

    .line 196
    const v0, 0x7f0701de

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    .line 197
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/bs;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    const v0, 0x7f0701dd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bs;->g:Landroid/widget/TextView;

    .line 200
    const v0, 0x7f0701e0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bs;->h:Landroid/widget/ProgressBar;

    .line 202
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 204
    return-object v1
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 159
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 161
    const v1, 0x7f03004b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 164
    new-instance v2, Lcom/sec/chaton/settings/downloads/bq;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/settings/downloads/bq;-><init>(Lcom/sec/chaton/settings/downloads/bm;Lcom/sec/chaton/settings/downloads/bn;)V

    .line 165
    const v0, 0x7f0701e2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bq;->b:Landroid/widget/TextView;

    .line 168
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 169
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 509
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/bs;

    .line 525
    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/bs;->a:Ljava/lang/String;

    .line 526
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/sec/chaton/e/a/k;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 527
    if-nez v1, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 530
    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 531
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v1, v5, v7

    if-nez v1, :cond_2

    move v1, v2

    .line 533
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 537
    if-eqz v1, :cond_4

    .line 539
    invoke-static {v4, v0}, Lcom/sec/chaton/settings/downloads/bj;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 540
    if-eqz v0, :cond_3

    .line 541
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->g(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bm;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 531
    goto :goto_1

    .line 544
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->h(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 547
    :cond_4
    invoke-direct {p0, v4, v3}, Lcom/sec/chaton/settings/downloads/bm;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public setGroupCursor(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 713
    if-nez p1, :cond_2

    .line 714
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 715
    const-string v0, "group cursor. position/total : (null)"

    sget-object v1, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 719
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bm;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->a()V

    .line 734
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/CursorTreeAdapter;->setGroupCursor(Landroid/database/Cursor;)V

    .line 735
    return-void

    .line 722
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "group cursor. position/total : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/bm;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    :cond_3
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 727
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 728
    const-string v0, "filter_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 729
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/bm;->a(I)Landroid/database/Cursor;

    move-result-object v1

    .line 730
    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/settings/downloads/bm;->a(Landroid/database/Cursor;I)Landroid/database/Cursor;

    goto :goto_0
.end method
