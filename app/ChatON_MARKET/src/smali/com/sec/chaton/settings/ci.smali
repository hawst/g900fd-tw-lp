.class Lcom/sec/chaton/settings/ci;
.super Landroid/os/Handler;
.source "ActivitySettings.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ch;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ch;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/chaton/settings/ci;->a:Lcom/sec/chaton/settings/ch;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/chaton/settings/ci;->a:Lcom/sec/chaton/settings/ch;

    iget-object v0, v0, Lcom/sec/chaton/settings/ch;->a:Lcom/sec/chaton/settings/ActivitySettings;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivitySettings;->b(Lcom/sec/chaton/settings/ActivitySettings;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 215
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_0

    .line 216
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 220
    sget-object v1, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 222
    const/4 v0, 0x0

    .line 226
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/ey;->a(Landroid/content/Context;Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/ci;->a:Lcom/sec/chaton/settings/ch;

    iget-object v0, v0, Lcom/sec/chaton/settings/ch;->a:Lcom/sec/chaton/settings/ActivitySettings;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivitySettings;->c(Lcom/sec/chaton/settings/ActivitySettings;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ci;->a:Lcom/sec/chaton/settings/ch;

    iget-object v0, v0, Lcom/sec/chaton/settings/ch;->a:Lcom/sec/chaton/settings/ActivitySettings;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivitySettings;->c(Lcom/sec/chaton/settings/ActivitySettings;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/settings/ci;->a:Lcom/sec/chaton/settings/ch;

    iget-object v0, v0, Lcom/sec/chaton/settings/ch;->a:Lcom/sec/chaton/settings/ActivitySettings;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivitySettings;->c(Lcom/sec/chaton/settings/ActivitySettings;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method
