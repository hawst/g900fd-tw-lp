.class public Lcom/sec/chaton/settings/tellfriends/bl;
.super Landroid/app/Dialog;
.source "WeiboDialog.java"


# static fields
.field static final a:[F

.field static final b:[F

.field static final c:Landroid/widget/FrameLayout$LayoutParams;

.field private static final f:Ljava/lang/String;


# instance fields
.field public d:Landroid/content/Context;

.field e:Landroid/os/Handler;

.field private g:Lweibo4android/Weibo;

.field private h:Lweibo4android/http/RequestToken;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/sec/chaton/settings/tellfriends/common/b;

.field private l:Landroid/app/ProgressDialog;

.field private m:Landroid/webkit/WebView;

.field private n:Landroid/widget/FrameLayout;

.field private o:Landroid/widget/ImageView;

.field private p:Z

.field private q:Ljava/util/Timer;

.field private r:Lcom/sec/chaton/settings/tellfriends/br;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, -0x1

    .line 46
    const-class v0, Lcom/sec/chaton/settings/tellfriends/bl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bl;->f:Ljava/lang/String;

    .line 49
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bl;->a:[F

    .line 50
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bl;->b:[F

    .line 52
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bl;->c:Landroid/widget/FrameLayout$LayoutParams;

    return-void

    .line 49
    :array_0
    .array-data 4
        0x41f00000    # 30.0f
        0x42700000    # 60.0f
    .end array-data

    .line 50
    :array_1
    .array-data 4
        0x41f00000    # 30.0f
        0x42700000    # 60.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lweibo4android/Weibo;Lweibo4android/http/RequestToken;Lcom/sec/chaton/settings/tellfriends/common/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    const v0, 0x1030010

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 63
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->j:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->p:Z

    .line 75
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->q:Ljava/util/Timer;

    .line 76
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->r:Lcom/sec/chaton/settings/tellfriends/br;

    .line 421
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bp;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/bp;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->e:Landroid/os/Handler;

    .line 82
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->d:Landroid/content/Context;

    .line 83
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/bl;->h:Lweibo4android/http/RequestToken;

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lweibo4android/http/RequestToken;->getAuthenticationURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&from=chaton"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->i:Ljava/lang/String;

    .line 85
    iput-object p4, p0, Lcom/sec/chaton/settings/tellfriends/bl;->k:Lcom/sec/chaton/settings/tellfriends/common/b;

    .line 86
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/bl;->g:Lweibo4android/Weibo;

    .line 95
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/bl;Lcom/sec/chaton/settings/tellfriends/br;)Lcom/sec/chaton/settings/tellfriends/br;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->r:Lcom/sec/chaton/settings/tellfriends/br;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/bl;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->q:Ljava/util/Timer;

    return-object p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 192
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->o:Landroid/widget/ImageView;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->o:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/bo;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/bo;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->o:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 208
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 211
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 212
    new-instance v1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    .line 213
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 215
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 216
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 217
    invoke-virtual {v1}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    .line 219
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->requestFocus(I)Z

    .line 220
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 221
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bl;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    sget-object v2, Lcom/sec/chaton/settings/tellfriends/bl;->c:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 225
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/bs;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/settings/tellfriends/bs;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;Lcom/sec/chaton/settings/tellfriends/bm;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 227
    invoke-virtual {v0, p1, p1, p1, p1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 228
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 229
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 230
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-static {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/bl;)Lcom/sec/chaton/settings/tellfriends/common/b;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->k:Lcom/sec/chaton/settings/tellfriends/common/b;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 414
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bl;->f:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/bl;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->p:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/bl;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->o:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/bl;)Lcom/sec/chaton/settings/tellfriends/br;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->r:Lcom/sec/chaton/settings/tellfriends/br;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/bl;)Lweibo4android/http/RequestToken;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->h:Lweibo4android/http/RequestToken;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/settings/tellfriends/bl;)Lweibo4android/Weibo;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->g:Lweibo4android/Weibo;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/settings/tellfriends/bl;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->q:Ljava/util/Timer;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->m:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 155
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->p:Z

    if-nez v0, :cond_2

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 159
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 161
    :cond_2
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->p:Z

    .line 188
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 189
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x1

    .line 99
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 101
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/bm;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/bl;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 109
    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/tellfriends/bl;->requestWindowFeature(I)Z

    .line 111
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b000d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->l:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/bn;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/bn;-><init>(Lcom/sec/chaton/settings/tellfriends/bl;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 123
    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/tellfriends/bl;->requestWindowFeature(I)Z

    .line 125
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    .line 127
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/bl;->a()V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 130
    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/bl;->a(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bl;->o:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/tellfriends/bl;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->p:Z

    .line 182
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 183
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    .line 168
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 172
    if-eqz p1, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bl;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    .line 176
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Dialog;->onWindowFocusChanged(Z)V

    .line 177
    return-void
.end method
