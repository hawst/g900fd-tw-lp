.class public Lcom/sec/chaton/settings/ck;
.super Lcom/sec/common/f/a;
.source "BgDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 31
    iput-object p2, p0, Lcom/sec/chaton/settings/ck;->a:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 57
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .line 58
    if-nez p1, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/settings/ck;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ck;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 62
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ck;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 37
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ck;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 42
    iget-object v1, p0, Lcom/sec/chaton/settings/ck;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/cd;->d(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ck;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 68
    if-eqz v0, :cond_0

    .line 69
    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/graphics/drawable/Drawable;)V

    .line 71
    :cond_0
    return-void
.end method
