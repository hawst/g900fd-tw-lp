.class Lcom/sec/chaton/settings/downloads/aw;
.super Ljava/lang/Object;
.source "DownloadableCounter.java"


# static fields
.field static final a:Ljava/lang/String;

.field public static b:I

.field public static c:I

.field public static d:I


# instance fields
.field e:Landroid/os/Handler;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/ay;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/aw;->a:Ljava/lang/String;

    .line 20
    const/16 v0, 0x1f40

    sput v0, Lcom/sec/chaton/settings/downloads/aw;->b:I

    .line 21
    const/4 v0, 0x0

    sput v0, Lcom/sec/chaton/settings/downloads/aw;->c:I

    .line 22
    const/4 v0, 0x1

    sput v0, Lcom/sec/chaton/settings/downloads/aw;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/aw;->f:Ljava/util/HashMap;

    .line 29
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/aw;->f:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 114
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 115
    const-string v0, "setStatus(), mItems is (null)"

    sget-object v1, Lcom/sec/chaton/settings/downloads/aw;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    :goto_0
    return v2

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/aw;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/ay;

    .line 124
    if-nez v0, :cond_4

    move v0, v1

    .line 134
    :goto_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 142
    :cond_2
    if-eqz p3, :cond_3

    if-eqz v0, :cond_3

    .line 143
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/aw;->b()V

    :cond_3
    move v2, v0

    .line 146
    goto :goto_0

    .line 129
    :cond_4
    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/ay;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 130
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 32
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 33
    const-string v0, "clear()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/aw;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/aw;->f:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/aw;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/aw;->e:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/l;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49
    if-nez p1, :cond_0

    move v0, v1

    .line 77
    :goto_0
    return v0

    .line 53
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    .line 54
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 77
    :goto_1
    invoke-direct {p0, v2, v0, v1}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;Z)Z

    move-result v0

    goto :goto_0

    .line 60
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    goto :goto_1

    .line 65
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    goto :goto_1

    .line 69
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    goto :goto_1

    .line 55
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/chaton/settings/downloads/ay;)Z
    .locals 2

    .prologue
    .line 178
    sget-object v0, Lcom/sec/chaton/settings/downloads/ax;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/ay;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 185
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 180
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 5

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/aw;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/aw;->c()Z

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/aw;->e:Landroid/os/Handler;

    sget v2, Lcom/sec/chaton/settings/downloads/aw;->b:I

    if-eqz v0, :cond_1

    sget v0, Lcom/sec/chaton/settings/downloads/aw;->d:I

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v0, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 154
    :cond_0
    return-void

    .line 152
    :cond_1
    sget v0, Lcom/sec/chaton/settings/downloads/aw;->c:I

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 157
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "hasDownloadable()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/aw;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/aw;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 162
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Lcom/sec/chaton/settings/downloads/ay;)Z

    move-result v0

    .line 168
    if-eqz v0, :cond_1

    .line 169
    const/4 v0, 0x1

    .line 174
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
