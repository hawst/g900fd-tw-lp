.class public Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;
.source "TwitterMessageFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/u;
.implements Lcom/sec/chaton/settings/tellfriends/common/v;


# instance fields
.field private b:Lcom/sec/chaton/settings/tellfriends/an;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;-><init>()V

    .line 23
    const-string v0, "140"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a(I)V

    .line 24
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/v;)V

    .line 25
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/u;)V

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->d()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->d()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->d()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 84
    if-nez p1, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->b(I)V

    .line 87
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->c()V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/an;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/bi;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/bi;-><init>(Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;)V

    invoke-virtual {v0, p2, v1}, Lcom/sec/chaton/settings/tellfriends/an;->a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V

    .line 80
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 39
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/an;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/an;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/an;

    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->a()V

    .line 44
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onDestroy()V

    .line 45
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onResume()V

    .line 51
    return-void
.end method
