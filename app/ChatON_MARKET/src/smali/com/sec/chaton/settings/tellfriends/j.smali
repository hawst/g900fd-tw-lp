.class Lcom/sec/chaton/settings/tellfriends/j;
.super Landroid/webkit/WebViewClient;
.source "RenrenDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/e;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/e;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/e;Lcom/sec/chaton/settings/tellfriends/f;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/j;-><init>(Lcom/sec/chaton/settings/tellfriends/e;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 175
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "On Page Finished URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->c(Lcom/sec/chaton/settings/tellfriends/e;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->d(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->e(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->a(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->f(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 187
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageStarted URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->c(Lcom/sec/chaton/settings/tellfriends/e;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->d(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 193
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 197
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-direct {v1, p3, p2, p4}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/e;->dismiss()V

    .line 200
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v5, -0x3ef

    const/4 v1, 0x1

    .line 204
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shouldOverrideUrlLoading URL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    sget-object v2, Lcom/sec/chaton/c/b;->q:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 207
    invoke-static {p2}, Lcom/renren/android/g;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 208
    const-string v3, "error"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 209
    if-eqz v3, :cond_3

    .line 210
    const-string v0, "access_denied"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/common/b;->a()V

    .line 223
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/e;->dismiss()V

    move v0, v1

    .line 260
    :cond_0
    :goto_1
    return v0

    .line 212
    :cond_1
    const-string v0, "login_denied"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/common/a;

    const-string v3, "login_denied"

    invoke-direct {v2, v3, v5, p2}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    goto :goto_0

    .line 215
    :cond_2
    const-string v0, "error_description"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    const-string v3, "error_uri"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 217
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-direct {v4, v0, v5, v2}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    goto :goto_0

    .line 220
    :cond_3
    const-string v3, "access_token"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 221
    new-instance v3, Lcom/sec/chaton/settings/tellfriends/k;

    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/settings/tellfriends/k;-><init>(Lcom/sec/chaton/settings/tellfriends/e;Lcom/sec/chaton/settings/tellfriends/f;)V

    new-array v4, v1, [Ljava/lang/String;

    aput-object v2, v4, v0

    invoke-virtual {v3, v4}, Lcom/sec/chaton/settings/tellfriends/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/k;

    goto :goto_0

    .line 226
    :cond_4
    sget-object v2, Lcom/sec/chaton/c/b;->u:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 227
    invoke-static {p2}, Lcom/renren/android/g;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 228
    const-string v2, "error"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 229
    if-eqz v2, :cond_7

    .line 230
    const-string v3, "access_denied"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/common/b;->a()V

    .line 258
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 260
    goto/16 :goto_1

    .line 233
    :cond_6
    const-string v2, "error_description"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 234
    const-string v3, "error_uri"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-direct {v4, v2, v5, v0}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    goto :goto_2

    .line 238
    :cond_7
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/tellfriends/e;->dismiss()V

    .line 239
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Ljava/lang/Object;)V

    move v0, v1

    .line 240
    goto/16 :goto_1

    .line 243
    :cond_8
    const-string v2, "http://graph.renren.com/login_deny/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 246
    const-string v2, "display"

    invoke-virtual {p2, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 248
    const-string v0, "rrconnect://cancel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/common/b;->a()V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/e;->dismiss()V

    move v0, v1

    .line 251
    goto/16 :goto_1

    .line 252
    :cond_9
    const-string v0, "rrconnect://success"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 253
    invoke-static {p2}, Lcom/renren/android/g;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 254
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/tellfriends/e;->dismiss()V

    .line 255
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/j;->a:Lcom/sec/chaton/settings/tellfriends/e;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/e;->b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Ljava/lang/Object;)V

    move v0, v1

    .line 256
    goto/16 :goto_1
.end method
