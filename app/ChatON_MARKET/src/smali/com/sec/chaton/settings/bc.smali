.class Lcom/sec/chaton/settings/bc;
.super Ljava/lang/Object;
.source "ActivityNoti.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityNoti;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    iget-object v0, v0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->a()Lcom/sec/chaton/settings/cn;

    move-result-object v0

    .line 457
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->a()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    .line 458
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->c()V

    .line 468
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 469
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 470
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute type"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute repeat"

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    iget-object v0, v0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    iget-object v0, v0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    iget-object v0, v0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/co;->setDismissMessage(Landroid/os/Message;)V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    iget-object v0, v0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->dismiss()V

    .line 478
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b001f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/settings/bc;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->h(Lcom/sec/chaton/settings/ActivityNoti;)V

    .line 480
    return-void

    .line 459
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->b()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 460
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_3

    .line 461
    const-string v1, "end time is expired. set to next day"

    const-string v2, "Mute Manually"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->d()V

    .line 464
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->c()V

    goto/16 :goto_0
.end method
