.class public Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "DeleteAccountPreference.java"


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(III)Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;
    .locals 3

    .prologue
    .line 135
    new-instance v0, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;

    invoke-direct {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;-><init>()V

    .line 136
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 137
    const-string v2, "title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 138
    const-string v2, "message"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 139
    const-string v2, "okButton"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 142
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 153
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->b:I

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "okButton"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->c:I

    .line 155
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->a:I

    .line 156
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 163
    iget v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->a:I

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->b:I

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->c:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    .line 165
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
