.class public Lcom/sec/chaton/settings/ActivityPasswordLockSet;
.super Landroid/app/Activity;
.source "ActivityPasswordLockSet.java"

# interfaces
.implements Lcom/coolots/sso/a/c;
.implements Lcom/sec/chaton/util/cn;


# instance fields
.field private A:I

.field private B:Lcom/sec/common/a/d;

.field private C:Z

.field private D:Lcom/coolots/sso/a/a;

.field private E:Landroid/content/Intent;

.field private F:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private G:Lcom/sec/chaton/util/ar;

.field a:Landroid/os/Handler;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[Landroid/widget/ImageView;

.field private f:[I

.field private g:[I

.field private h:Ljava/lang/String;

.field private i:[Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/content/Context;

.field private v:Landroid/app/ProgressDialog;

.field private w:I

.field private x:I

.field private final y:I

.field private z:Landroid/os/CountDownTimer;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b:I

    .line 55
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "imei"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c:Ljava/lang/String;

    .line 56
    const-class v0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    .line 57
    new-array v0, v4, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e:[Landroid/widget/ImageView;

    .line 59
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    .line 60
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->g:[I

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    .line 64
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    .line 65
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 66
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    .line 67
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    .line 72
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->v:Landroid/app/ProgressDialog;

    .line 82
    iput v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->y:I

    .line 1357
    new-instance v0, Lcom/sec/chaton/settings/bl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/bl;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->G:Lcom/sec/chaton/util/ar;

    .line 1410
    new-instance v0, Lcom/sec/chaton/settings/bm;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/bm;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a:Landroid/os/Handler;

    return-void

    .line 59
    nop

    :array_0
    .array-data 4
        0x7f0703b5
        0x7f0703b1
        0x7f0703b4
        0x7f0703b2
        0x7f0703b7
        0x7f0703b9
        0x7f0703b8
        0x7f0703ba
        0x7f0703bc
        0x7f0703bb
        0x7f0703b6
    .end array-data

    .line 60
    :array_1
    .array-data 4
        0x7f0703ac
        0x7f0703ad
        0x7f0703ae
        0x7f0703af
    .end array-data
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPasswordLockSet;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->v:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityPasswordLockSet;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->C:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->D:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1340
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)Lcom/sec/chaton/util/ar;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->G:Lcom/sec/chaton/util/ar;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 825
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->z:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->z:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 828
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->A:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 829
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)Lcom/sec/chaton/d/a;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->F:Lcom/sec/chaton/d/a;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 923
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 924
    const-string v0, "passlock_clearDescription"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    :cond_0
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 927
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 928
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 1102
    const-string v0, "Restore"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    .line 1105
    if-eqz v0, :cond_5

    .line 1107
    check-cast v0, Ljava/util/HashMap;

    .line 1109
    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    const-string v1, "INPUT_PASSWORD[0]"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v3, v4

    .line 1110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "INPUT_PASSWORD[0] :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    const-string v1, "INPUT_PASSWORD[1]"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v3, v2

    .line 1113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "INPUT_PASSWORD[1] :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    const-string v1, "inputUserPassword"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    .line 1116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "inputUserPassword :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    const-string v1, "password_index"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1119
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1120
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    .line 1121
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "password_index : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    const-string v1, "keypad_input"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1124
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1125
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 1126
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keypad_input : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1128
    const-string v1, "descriptionbackup"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1129
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1130
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->o:I

    .line 1131
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->o:I

    if-eqz v1, :cond_0

    .line 1132
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->o:I

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 1135
    :cond_0
    const-string v1, "titlebackup"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1136
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1137
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->x:I

    .line 1138
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->x:I

    if-eqz v1, :cond_1

    .line 1139
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->x:I

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e(I)V

    .line 1142
    :cond_1
    const-string v1, "error_count"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1143
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1144
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    .line 1145
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error_count : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    const-string v1, "error_count_temp"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1148
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1149
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 1150
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error_count_temp : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    const-string v1, "error_number_count"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1154
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1155
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 1156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error_number_count : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    const-string v1, "hintbackup"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1160
    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->q:Ljava/lang/String;

    .line 1161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restore error_count"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    const-string v1, "config_changed"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1166
    if-eqz v1, :cond_2

    const-string v3, "yes"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1167
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->B:Lcom/sec/common/a/d;

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 1168
    iput-boolean v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->C:Z

    .line 1172
    :cond_2
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    const/4 v3, 0x5

    if-lt v1, v3, :cond_3

    .line 1177
    const-string v1, ""

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->q:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1178
    const-string v1, "Hint null"

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1185
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    const v3, 0x7f0b029b

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(I)V

    .line 1195
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    new-instance v3, Lcom/sec/chaton/settings/bj;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/bj;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1206
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backupmap : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1208
    :goto_1
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    if-gt v0, v1, :cond_5

    .line 1209
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b(I)V

    .line 1208
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1180
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->q:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1212
    :cond_5
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c()V

    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1219
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    .line 1220
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 1222
    const-string v0, "PASSWORD_LOCK"

    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1223
    const-string v1, "PASSWORD_HINT"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->q:Ljava/lang/String;

    .line 1225
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    .line 1229
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 1230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error_count_temp : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1233
    const-string v0, "Hint null"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1237
    :goto_0
    const v0, 0x7f0b01bf

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 1242
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1243
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    const v1, 0x7f0b029b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1247
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/settings/bk;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bk;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1262
    :goto_1
    return-void

    .line 1235
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->q:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1258
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1259
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private f(I)V
    .locals 6

    .prologue
    .line 802
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    aget v0, v0, p1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 803
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    aget v0, v0, p1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 805
    iput p1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->A:I

    .line 807
    new-instance v0, Lcom/sec/chaton/settings/bh;

    const-wide/16 v2, 0x12c

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/bh;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;JJ)V

    invoke-virtual {v0}, Lcom/sec/chaton/settings/bh;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->z:Landroid/os/CountDownTimer;

    .line 822
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->B:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->v:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 837
    const-string v0, "initialize_password"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 839
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v0

    .line 838
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 842
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    .line 843
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 793
    const-string v0, "start_vibrator"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 795
    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 796
    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 797
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 944
    const-string v0, "passlock_hint_setText"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    const v0, 0x7f0703b0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 946
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 947
    const-string v1, "#f16623"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 948
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 992
    const-string v0, "prePassword"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    new-array v2, v4, [Ljava/lang/String;

    move v0, v1

    .line 995
    :goto_0
    if-ge v0, v4, :cond_0

    .line 996
    const-string v3, ""

    aput-object v3, v2, v0

    .line 995
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 999
    :cond_0
    const-string v0, "PASSWORD_LOCK"

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1005
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    .line 1007
    const-string v4, "GET"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1008
    const-string v3, "GET preference"

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    const-string v3, "LOCK_STATE"

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1010
    const/4 v1, 0x1

    const-string v3, "PASSWORD"

    const-string v4, "0000"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 1037
    :goto_1
    return-object v2

    .line 1013
    :cond_1
    const-string v4, "SET preference"

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "state : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "original pass : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1019
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1020
    invoke-static {p2}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1021
    invoke-static {v4}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1022
    const-string v7, "LOCK_STATE"

    invoke-interface {v0, v7, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1023
    const-string v5, "PASSWORD"

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1025
    const-string v5, "PASSWORD_HINT"

    const-string v6, ""

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1026
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1027
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "final pass : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "editor : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    const-string v0, "OFF"

    invoke-virtual {v3, v0, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const-string v0, "OFF"

    const-string v4, "default"

    invoke-virtual {v3, v0, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1033
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LOCK_STATE : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 1035
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    goto/16 :goto_1
.end method

.method public b()V
    .locals 4

    .prologue
    .line 896
    const-string v0, "passlock_defaultImage"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 899
    new-instance v1, Lcom/sec/chaton/settings/bi;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bi;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 909
    return-void
.end method

.method public b(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 849
    const-string v0, "passlock_setImage"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 866
    :goto_0
    if-gt v0, p1, :cond_0

    .line 867
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 866
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 877
    const-string v0, "passlock_backImage"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 890
    return-void
.end method

.method public clickkeypad(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    const v7, 0x7f0b01c2

    const/16 v6, 0x1f4

    const/4 v1, 0x0

    .line 298
    const-string v0, "clickkeypad"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_4

    .line 308
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    move v0, v1

    .line 309
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 310
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    aget v3, v3, v0

    if-ne v2, v3, :cond_5

    .line 311
    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    .line 316
    :cond_0
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f(I)V

    .line 322
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 323
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v5, v3, v4

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    .line 324
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INPUT_PASSWORD : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b(I)V

    .line 347
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 352
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MODE : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "keypad_input : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 357
    const-string v0, "GET"

    const-string v2, ""

    const-string v3, ""

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 359
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v2, v2, v3

    invoke-static {v2}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 361
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 363
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_1

    .line 364
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inputUserPasswordMD5 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " inputUserPasswordMD5new : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " savedData : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_1
    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "PRIVACY"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 377
    iget v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v4, v0, v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 379
    :cond_2
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 380
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 382
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 385
    :cond_3
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 387
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 388
    invoke-virtual {p0, v9}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->setResult(I)V

    .line 389
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    .line 516
    :cond_4
    :goto_1
    return-void

    .line 309
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 391
    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 393
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->startActivity(Landroid/content/Intent;)V

    .line 394
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    goto :goto_1

    .line 398
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 399
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 400
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v2, "#5f5f5f"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 402
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 403
    invoke-virtual {p0, v8}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 404
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 405
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f()V

    .line 406
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto :goto_1

    .line 417
    :cond_8
    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "RECEIVER"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "HOME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 419
    :cond_9
    iget v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v4, v0, v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 421
    :cond_a
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 422
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 424
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 426
    :cond_b
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/util/p;->a(Z)V

    .line 427
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 428
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->E:Landroid/content/Intent;

    invoke-virtual {p0, v9, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->setResult(ILandroid/content/Intent;)V

    .line 430
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 431
    const-string v1, "password_lock_finish"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 432
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 434
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    goto/16 :goto_1

    .line 436
    :cond_c
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 437
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 438
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 439
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v2, "#5f5f5f"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 440
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 441
    invoke-virtual {p0, v8}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 442
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 443
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f()V

    .line 444
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto/16 :goto_1

    .line 446
    :cond_d
    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "CHANGE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 448
    iget v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v4, v0, v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 450
    :cond_e
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 451
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 453
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 456
    :cond_f
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 457
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    .line 458
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 459
    const-string v1, "MODE"

    const-string v2, "SET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 460
    invoke-virtual {p0, v0, v9}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 462
    :cond_10
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 463
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 464
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v2, "#5f5f5f"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 466
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 467
    invoke-virtual {p0, v8}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 468
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 469
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f()V

    .line 470
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto/16 :goto_1

    .line 483
    :cond_11
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    .line 484
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_14

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v2, v2, -0x2

    aget-object v0, v0, v2

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 486
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 487
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 489
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 492
    :cond_12
    const-string v0, "SET"

    const-string v1, "ON"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    goto/16 :goto_1

    .line 494
    :cond_13
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 495
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 496
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    .line 497
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 498
    const v0, 0x7f0b01c8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e(I)V

    .line 499
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v1, "#5f5f5f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 500
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 501
    invoke-virtual {p0, v8}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 502
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto/16 :goto_1

    .line 506
    :cond_14
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 507
    const v0, 0x7f0b01c9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e(I)V

    .line 510
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    .line 511
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d()V

    goto/16 :goto_1
.end method

.method public clickkeypad_back(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 251
    const-string v0, "clickkeypad_back"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    if-lez v0, :cond_1

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INPUT_PASSWORD : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v2, v2, v3

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    .line 257
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c(I)V

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto :goto_0
.end method

.method public clickkeypad_cancel(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 276
    const-string v0, "clickkeypad_cancel"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    invoke-static {v2}, Lcom/sec/chaton/util/p;->a(Z)V

    .line 280
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v1, "HOME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v1, "RECEIVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/TabActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 283
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 284
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    const-string v1, "finish"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 287
    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->startActivityForResult(Landroid/content/Intent;I)V

    .line 292
    :goto_0
    return-void

    .line 289
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    goto :goto_0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 915
    const-string v0, "passlock_setText"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 917
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 918
    iput p1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->o:I

    .line 919
    return-void
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 933
    const-string v0, "passlock_title_setText"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    const v0, 0x7f070344

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 935
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 936
    iput p1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->x:I

    .line 937
    return-void
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 1435
    const v0, 0x7f0c00fd

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 1430
    const v0, 0x7f0c00fd

    return v0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 956
    const-string v0, "onBackPressed"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    invoke-static {v3}, Lcom/sec/chaton/util/p;->a(Z)V

    .line 960
    invoke-static {v2}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 961
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v1, "HOME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v1, "RECEIVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 962
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBackPressed MODE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/TabActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 964
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 965
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 966
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 967
    const-string v1, "finish"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 968
    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->startActivityForResult(Landroid/content/Intent;I)V

    .line 976
    :goto_0
    return-void

    .line 970
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 972
    invoke-static {v2}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 974
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v5, 0x7f0b02bc

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 95
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, SUNFFIX: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const v0, 0x7f0300d3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->setContentView(I)V

    .line 103
    const v0, 0x7f0703b3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    .line 104
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    .line 105
    iput-object p0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    .line 107
    iput-boolean v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->C:Z

    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->E:Landroid/content/Intent;

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->E:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MODE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_0
    invoke-static {v2}, Lcom/sec/chaton/util/p;->a(Z)V

    .line 119
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->F:Lcom/sec/chaton/d/a;

    .line 123
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    move v1, v2

    .line 127
    :goto_1
    const/4 v0, 0x4

    if-ge v1, v0, :cond_0

    .line 128
    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->g:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v3, v1

    .line 127
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 113
    :catch_0
    move-exception v0

    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v1, "SET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135
    invoke-static {v2}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 136
    const v0, 0x7f0b01c8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e(I)V

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(I)V

    .line 152
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0297

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->D:Lcom/coolots/sso/a/a;

    .line 157
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->D:Lcom/coolots/sso/a/a;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->D:Lcom/coolots/sso/a/a;

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    invoke-virtual {v1, v2, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 160
    const-string v1, "onCreate:mChatonV.setListener()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 164
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b029b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/settings/bf;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/bf;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b00dc

    new-instance v3, Lcom/sec/chaton/settings/be;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/be;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 196
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->B:Lcom/sec/common/a/d;

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->B:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/settings/bg;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/bg;-><init>(Lcom/sec/chaton/settings/ActivityPasswordLockSet;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 209
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e()V

    .line 211
    return-void

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v1, "CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->s:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(I)V

    .line 142
    invoke-static {v4}, Lcom/sec/chaton/util/p;->c(Z)V

    goto/16 :goto_2

    .line 144
    :cond_3
    invoke-static {v4}, Lcom/sec/chaton/util/p;->c(Z)V

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 1314
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1316
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->v:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1317
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1321
    :cond_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1323
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->D:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->u:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 1324
    const-string v0, "onDestroy:mChatonV.setListener(null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x7

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 530
    .line 532
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v6, 0x43

    if-ne v0, v6, :cond_1

    move v0, v1

    .line 535
    :goto_0
    const/16 v6, 0xa

    if-ge v0, v6, :cond_0

    .line 536
    iget-object v6, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    aget v6, v6, v0

    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->clearFocus()V

    .line 535
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 539
    :cond_0
    const-string v0, "clickkeypad_back"

    iget-object v6, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    if-lez v0, :cond_13

    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "INPUT_PASSWORD : "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v7, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v6, v6, v7

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v6, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    iget-object v7, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v8, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v7, v7, v8

    iget v8, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {v7, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v6

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    iget v6, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    .line 546
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c(I)V

    .line 559
    :cond_1
    :goto_1
    const/16 v0, 0x21

    if-eq p1, v0, :cond_2

    const/16 v0, 0x2e

    if-eq p1, v0, :cond_2

    const/16 v0, 0x30

    if-eq p1, v0, :cond_2

    const/16 v0, 0x20

    if-eq p1, v0, :cond_2

    const/16 v0, 0x22

    if-eq p1, v0, :cond_2

    const/16 v0, 0x23

    if-eq p1, v0, :cond_2

    const/16 v0, 0x1e

    if-eq p1, v0, :cond_2

    const/16 v0, 0x1f

    if-eq p1, v0, :cond_2

    const/16 v0, 0x32

    if-eq p1, v0, :cond_2

    if-eq p1, v4, :cond_2

    const/16 v0, 0x10

    if-eq p1, v0, :cond_2

    const/16 v0, 0xf

    if-eq p1, v0, :cond_2

    const/16 v0, 0xe

    if-eq p1, v0, :cond_2

    const/16 v0, 0xd

    if-eq p1, v0, :cond_2

    const/16 v0, 0xc

    if-eq p1, v0, :cond_2

    const/16 v0, 0xb

    if-eq p1, v0, :cond_2

    const/16 v0, 0xa

    if-eq p1, v0, :cond_2

    const/16 v0, 0x9

    if-eq p1, v0, :cond_2

    if-ne p1, v5, :cond_16

    .line 567
    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 570
    :cond_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v6, 0x21

    if-ne v0, v6, :cond_25

    .line 571
    const/4 v0, 0x1

    .line 574
    :goto_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x2e

    if-ne v6, v7, :cond_4

    move v0, v2

    .line 577
    :cond_4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x30

    if-ne v6, v7, :cond_5

    .line 578
    const/4 v0, 0x3

    .line 580
    :cond_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x20

    if-ne v6, v7, :cond_6

    move v0, v3

    .line 583
    :cond_6
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x22

    if-ne v6, v7, :cond_7

    .line 584
    const/4 v0, 0x5

    .line 587
    :cond_7
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x23

    if-ne v6, v7, :cond_8

    .line 588
    const/4 v0, 0x6

    .line 591
    :cond_8
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x1f

    if-ne v6, v7, :cond_9

    move v0, v4

    .line 594
    :cond_9
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x32

    if-ne v6, v7, :cond_a

    move v0, v5

    .line 598
    :cond_a
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x1e

    if-ne v6, v7, :cond_b

    .line 599
    const/16 v0, 0x9

    .line 601
    :cond_b
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    if-ne v6, v5, :cond_c

    .line 603
    const/4 v0, 0x1

    .line 605
    :cond_c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x9

    if-ne v6, v7, :cond_d

    move v0, v2

    .line 611
    :cond_d
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0xa

    if-ne v6, v7, :cond_e

    .line 613
    const/4 v0, 0x3

    .line 617
    :cond_e
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0xb

    if-ne v6, v7, :cond_f

    move v0, v3

    .line 621
    :cond_f
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0xc

    if-ne v6, v7, :cond_10

    .line 623
    const/4 v0, 0x5

    .line 625
    :cond_10
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0xd

    if-ne v6, v7, :cond_11

    .line 626
    const/4 v0, 0x6

    .line 628
    :cond_11
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0xe

    if-ne v6, v7, :cond_24

    .line 632
    :goto_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v6, 0xf

    if-ne v0, v6, :cond_23

    .line 636
    :goto_4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v4, 0x10

    if-ne v0, v4, :cond_12

    .line 638
    const/16 v5, 0x9

    .line 641
    :cond_12
    invoke-direct {p0, v5}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f(I)V

    .line 643
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    if-eq v0, v3, :cond_16

    .line 645
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    .line 647
    iput v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    .line 649
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 651
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v6, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v7, v5, v6

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v6

    .line 652
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    .line 654
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INPUT_PASSWORD : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b(I)V

    .line 658
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 660
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    if-ne v0, v3, :cond_16

    move v0, v1

    .line 661
    :goto_5
    const/16 v3, 0xa

    if-ge v0, v3, :cond_14

    .line 662
    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f:[I

    aget v3, v3, v0

    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->clearFocus()V

    .line 661
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 554
    :cond_13
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    if-nez v0, :cond_1

    .line 555
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto/16 :goto_1

    .line 665
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MODE : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keypad_input : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    new-array v0, v2, [Ljava/lang/String;

    .line 669
    const-string v0, "GET"

    const-string v3, ""

    const-string v4, ""

    invoke-virtual {p0, v0, v3, v4}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 671
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v5, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 673
    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "PRIVACY"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 674
    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 675
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 676
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 678
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 681
    :cond_15
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 682
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 684
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->startActivity(Landroid/content/Intent;)V

    .line 685
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    .line 785
    :cond_16
    :goto_6
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 687
    :cond_17
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 688
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 689
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 690
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v2, "#5f5f5f"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 691
    const v0, 0x7f0b01c2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 692
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 693
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 694
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f()V

    .line 695
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto :goto_6

    .line 699
    :cond_18
    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "RECEIVER"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_19

    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "HOME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 700
    :cond_19
    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 701
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 702
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 704
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 706
    :cond_1a
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/util/p;->a(Z)V

    .line 707
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 708
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    goto :goto_6

    .line 710
    :cond_1b
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 711
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 712
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 713
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v2, "#5f5f5f"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 714
    const v0, 0x7f0b01c2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 715
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 716
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 717
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f()V

    .line 718
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto/16 :goto_6

    .line 720
    :cond_1c
    iget-object v4, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v5, "CHANGE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 722
    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->p:I

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 723
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 724
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 726
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 729
    :cond_1d
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    .line 730
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    .line 731
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 732
    const-string v1, "MODE"

    const-string v2, "SET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 733
    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_6

    .line 735
    :cond_1e
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 736
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 737
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 738
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v2, "#5f5f5f"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 739
    const v0, 0x7f0b01c2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 740
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 741
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    .line 742
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->f()V

    .line 743
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto/16 :goto_6

    .line 748
    :cond_1f
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    .line 749
    iget v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    if-ne v0, v2, :cond_22

    .line 750
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v2, v2, -0x2

    aget-object v0, v0, v2

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 751
    invoke-static {v1}, Lcom/sec/chaton/util/p;->c(Z)V

    .line 752
    invoke-static {}, Lcom/sec/chaton/util/p;->b()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 754
    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 757
    :cond_20
    const-string v0, "SET"

    const-string v1, "ON"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    goto/16 :goto_6

    .line 759
    :cond_21
    const v0, 0x7f0703b1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 760
    invoke-virtual {v0}, Landroid/widget/Button;->clearFocus()V

    .line 762
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a()V

    .line 763
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 764
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    .line 765
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(I)V

    .line 766
    const v0, 0x7f0b01c8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e(I)V

    .line 767
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->t:Landroid/widget/TextView;

    const-string v1, "#5f5f5f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 768
    const v0, 0x7f0b01c2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d(I)V

    .line 769
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->a(Ljava/lang/String;)V

    .line 770
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    goto/16 :goto_6

    .line 774
    :cond_22
    iput v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    .line 775
    const v0, 0x7f0703b1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 776
    invoke-virtual {v0}, Landroid/widget/Button;->clearFocus()V

    .line 777
    const v0, 0x7f0b01c9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->e(I)V

    .line 778
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->b()V

    .line 779
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d()V

    goto/16 :goto_6

    :cond_23
    move v5, v4

    goto/16 :goto_4

    :cond_24
    move v4, v0

    goto/16 :goto_3

    :cond_25
    move v0, v1

    goto/16 :goto_2
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 219
    const-string v0, "[LIFE] onNewIntent"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1302
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1304
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 3

    .prologue
    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveRemoveAccount"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->F:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->G:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->G:Lcom/sec/chaton/util/ar;

    const/16 v1, 0x3ea

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ar;->a(II)V

    .line 243
    return-void
.end method

.method public onRestart()V
    .locals 2

    .prologue
    .line 1296
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 1297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onRestart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1298
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1290
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1292
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1043
    const-string v0, "onRetainNonConfigurationInstance"

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1047
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "backupmapclear : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    const-string v1, "INPUT_PASSWORD[0]"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1050
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INPUT_PASSWORD[0] :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    const-string v1, "INPUT_PASSWORD[1]"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1053
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INPUT_PASSWORD[1] :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->i:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    const-string v1, "inputUserPassword"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1056
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inputUserPassword :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1059
    const-string v2, "password_index"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1060
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "password_index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->l:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1063
    const-string v2, "keypad_input"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keypad_input : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->o:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1067
    const-string v2, "descriptionbackup"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->x:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1070
    const-string v2, "titlebackup"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->q:Ljava/lang/String;

    .line 1073
    const-string v2, "hintbackup"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1075
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->m:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1076
    const-string v2, "error_count"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error_count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->w:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1080
    const-string v2, "error_count_temp"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1081
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error_count_temp : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    iget v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1085
    const-string v2, "error_number_count"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error_number_count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    iget-boolean v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->C:Z

    if-eqz v1, :cond_0

    .line 1091
    const-string v1, "config_changed"

    const-string v2, "yes"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1092
    const-string v1, "config is changing"

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    :cond_0
    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 1284
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 1285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1308
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 1309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStop, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1310
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->h:Ljava/lang/String;

    const-string v1, "SET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 1350
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockSet;->finish()V

    .line 1352
    :cond_0
    return-void
.end method
