.class Lcom/sec/chaton/settings/ew;
.super Ljava/lang/Object;
.source "RingtonePreference2.java"

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/RingtonePreference2;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/RingtonePreference2;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/sec/chaton/settings/ew;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 7

    .prologue
    const/4 v2, 0x5

    const/4 v4, 0x0

    .line 314
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 316
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-lez v1, :cond_0

    .line 317
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 318
    invoke-virtual {p1}, Landroid/media/SoundPool;->autoPause()V

    .line 319
    int-to-float v2, v1

    int-to-float v3, v0

    div-float/2addr v2, v3

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v3, v1, v0

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p1

    move v1, p2

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 321
    :cond_0
    return-void
.end method
