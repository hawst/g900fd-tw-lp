.class public Lcom/sec/chaton/settings/downloads/a/a;
.super Lcom/sec/chaton/settings/downloads/a/l;
.source "AmsItemInstallWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/chaton/settings/downloads/a/l",
        "<",
        "Ljava/lang/Void;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final g:Landroid/os/Handler;


# instance fields
.field private b:Lcom/sec/chaton/d/d;

.field private c:Lcom/sec/chaton/d/e;

.field private e:Ljava/lang/String;

.field private f:Lcom/sec/chaton/settings/downloads/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/chaton/settings/downloads/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    .line 46
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/b;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/a/b;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/a;->g:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/d/d;Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p2}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/sec/chaton/settings/downloads/a/l;-><init>(Lcom/sec/chaton/e/ar;Ljava/lang/String;)V

    .line 62
    sget-object v0, Lcom/sec/chaton/settings/downloads/a/c;->a:Lcom/sec/chaton/settings/downloads/a/c;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a/a;->f:Lcom/sec/chaton/settings/downloads/a/c;

    .line 63
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/a/a;->c:Lcom/sec/chaton/d/e;

    .line 65
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/a/a;->b:Lcom/sec/chaton/d/d;

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p2}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/sec/chaton/settings/downloads/a/l;-><init>(Lcom/sec/chaton/e/ar;Ljava/lang/String;)V

    .line 70
    sget-object v0, Lcom/sec/chaton/settings/downloads/a/c;->b:Lcom/sec/chaton/settings/downloads/a/c;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a/a;->f:Lcom/sec/chaton/settings/downloads/a/c;

    .line 72
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/a/a;->c:Lcom/sec/chaton/d/e;

    .line 74
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/a/a;->e:Ljava/lang/String;

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/a/a;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/a;->e([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 79
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->b([Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/a;->f:Lcom/sec/chaton/settings/downloads/a/c;

    sget-object v1, Lcom/sec/chaton/settings/downloads/a/c;->b:Lcom/sec/chaton/settings/downloads/a/c;

    if-ne v0, v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/a;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a/a;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 106
    :goto_0
    return-object v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/a;->b:Lcom/sec/chaton/d/d;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/a/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/d;->a(Lcom/sec/chaton/d/a/b;Lcom/sec/chaton/d/e;Ljava/lang/String;)Lcom/sec/chaton/d/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/j;->b()Lcom/sec/chaton/a/a/f;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_6

    .line 90
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/AmsItemDownloadEntry;

    .line 92
    const/4 v2, 0x0

    .line 96
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    .line 97
    new-instance v1, Ljava/io/File;

    iget-object v4, v0, Lcom/sec/chaton/io/entry/AmsItemDownloadEntry;->imageurl:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 100
    :try_start_1
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/settings/downloads/a/a;->g:Landroid/os/Handler;

    iget-object v4, v0, Lcom/sec/chaton/io/entry/AmsItemDownloadEntry;->imageurl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1, p0}, Lcom/sec/common/util/a/a;->b(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;

    .line 102
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_1

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download_ams, onDownloading, 1.imageurl : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/sec/chaton/io/entry/AmsItemDownloadEntry;->imageurl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_ams, onDownloading, 2.tmpFile: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    move-object v0, v1

    .line 106
    goto/16 :goto_0

    .line 107
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 108
    :goto_1
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_2

    .line 109
    sget-object v2, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 112
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 116
    :cond_3
    new-instance v1, Lcom/sec/chaton/settings/downloads/a/n;

    invoke-direct {v1, v0}, Lcom/sec/chaton/settings/downloads/a/n;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 117
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 118
    :goto_2
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_4

    .line 119
    sget-object v2, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 122
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 123
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 126
    :cond_5
    throw v0

    .line 129
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_7

    .line 130
    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "Http result code is error. result code: "

    aput-object v2, v1, v4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_7
    new-instance v1, Lcom/sec/chaton/settings/downloads/a/n;

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "Http result code is error. result code: "

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/sec/chaton/settings/downloads/a/n;-><init>(Ljava/lang/String;)V

    throw v1

    .line 117
    :catch_2
    move-exception v0

    goto :goto_2

    .line 107
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method protected varargs a(Ljava/io/File;[Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 139
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/a/l;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Boolean;

    .line 143
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, Lcom/sec/chaton/settings/downloads/q;->a(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;Ljava/io/File;)V

    .line 145
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 153
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    .line 155
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 156
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download_ams, delete temp file (result/filePath) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_0
    return-object v0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 148
    sget-object v1, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 151
    :cond_1
    new-instance v1, Lcom/sec/chaton/settings/downloads/a/n;

    invoke-direct {v1, v0}, Lcom/sec/chaton/settings/downloads/a/n;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 154
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    .line 155
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_2

    .line 156
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download_ams, delete temp file (result/filePath) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/a/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_2
    throw v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Ljava/io/File;

    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/a/a;->a(Ljava/io/File;[Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/a;->a([Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
