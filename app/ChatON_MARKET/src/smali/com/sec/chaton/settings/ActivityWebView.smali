.class public Lcom/sec/chaton/settings/ActivityWebView;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityWebView.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    const v2, 0x7f0b0025

    .line 31
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 32
    const/4 v0, 0x0

    .line 34
    if-eqz v1, :cond_0

    .line 35
    const-string v0, "PARAM_MENU"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    :cond_0
    if-eqz v0, :cond_1

    .line 40
    const-string v1, "TERMS_AND_CONDITION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 41
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/ActivityWebView;->setTitle(I)V

    .line 82
    :cond_1
    :goto_0
    new-instance v0, Lcom/sec/chaton/settings/FragmentWebView;

    invoke-direct {v0}, Lcom/sec/chaton/settings/FragmentWebView;-><init>()V

    return-object v0

    .line 43
    :cond_2
    const-string v1, "PRIVACY_POLICY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 44
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/ActivityWebView;->setTitle(I)V

    goto :goto_0

    .line 46
    :cond_3
    const-string v1, "Help"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 47
    const v0, 0x7f0b0013

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityWebView;->setTitle(I)V

    goto :goto_0

    .line 49
    :cond_4
    const-string v1, "ACCEPT_USE_DATA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 50
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/ActivityWebView;->setTitle(I)V

    goto :goto_0

    .line 52
    :cond_5
    const-string v1, "Noti"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 54
    const v0, 0x7f0b0419

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityWebView;->setTitle(I)V

    goto :goto_0

    .line 56
    :cond_6
    const-string v1, "VOC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 57
    const v0, 0x7f0b01ae

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityWebView;->setTitle(I)V

    goto :goto_0

    .line 59
    :cond_7
    const-string v1, "License"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    const v0, 0x7f0b00e3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityWebView;->setTitle(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 111
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityWebView;->a(Landroid/app/Activity;)V

    .line 114
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 100
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityWebView;->a(Landroid/app/Activity;)V

    .line 103
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 88
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityWebView;->finish()V

    .line 90
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
