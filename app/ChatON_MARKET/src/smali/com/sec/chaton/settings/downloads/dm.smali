.class Lcom/sec/chaton/settings/downloads/dm;
.super Landroid/os/Handler;
.source "SoundListAdapter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/dl;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/dl;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/dm;->a:Lcom/sec/chaton/settings/downloads/dl;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/multimedia/audio/d;

    .line 69
    if-nez v0, :cond_1

    .line 70
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 71
    const-string v0, "download_sound, PlaySoundHandler(), task is null"

    invoke-static {}, Lcom/sec/chaton/settings/downloads/dl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/f;->a(I)Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v1

    .line 77
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_2

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sound event (what/task status/ task canceled) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->isCancelled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/settings/downloads/dl;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/dm;->a:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/settings/downloads/dl;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/audio/f;)Z

    .line 81
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->c:Lcom/sec/chaton/multimedia/audio/f;

    if-ne v1, v0, :cond_3

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dm;->a:Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/dl;->f()V

    .line 84
    :cond_3
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
