.class Lcom/sec/chaton/settings/tellfriends/common/l;
.super Landroid/os/Handler;
.source "SnsFriendsPickerFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const v3, 0x7f0b00be

    const/4 v4, 0x0

    .line 453
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 454
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 523
    :goto_0
    return-void

    .line 456
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->j(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Lcom/sec/chaton/d/h;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->i(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)[J

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;[J)V

    goto :goto_0

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->k(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    goto :goto_0

    .line 463
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_5

    .line 464
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 465
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetSnsFriendsList;

    .line 466
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetSnsFriendsList;->sns:Ljava/util/ArrayList;

    .line 468
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 470
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 471
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/GetSnsFriends;

    .line 472
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 473
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/GetSnsFriends;->snsid:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 474
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/GetSnsFriends;->snsid:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/sec/chaton/settings/tellfriends/ai;->a(Ljava/lang/String;)V

    .line 475
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v4}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/Map;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/GetSnsFriends;->userid:Ljava/lang/String;

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 481
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/d;->addAll(Ljava/util/Collection;)V

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->notifyDataSetChanged()V

    .line 489
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->f(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    goto/16 :goto_0

    .line 487
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->h(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    goto :goto_2

    .line 492
    :sswitch_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_8

    .line 493
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 495
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBuddyList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 496
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 497
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->l(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/Map;

    move-result-object v2

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 498
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->remove(Ljava/lang/Object;)V

    goto :goto_3

    .line 502
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b008b

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 503
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_9

    .line 504
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;I)V

    goto/16 :goto_0

    .line 506
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 507
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 511
    :sswitch_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_a

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a:Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->clear()V

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b01fb

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 514
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_b

    .line 515
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;I)V

    goto/16 :goto_0

    .line 517
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 518
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/l;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 454
    :sswitch_data_0
    .sparse-switch
        0x12f -> :sswitch_2
        0x137 -> :sswitch_3
        0x139 -> :sswitch_0
        0x13a -> :sswitch_1
    .end sparse-switch
.end method
