.class public Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;
.source "TwitterSubMenuFragment.java"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private h:Landroid/app/Activity;

.field private i:Landroid/view/View;

.field private j:Lcom/sec/chaton/settings/tellfriends/an;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;)Lcom/sec/chaton/settings/tellfriends/an;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->a(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->c()V

    .line 111
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h()V

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/bk;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/bk;-><init>(Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/an;->a(Lcom/sec/chaton/settings/tellfriends/af;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ai;)V
    .locals 3

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/tellfriends/TwitterMessageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    const-string v1, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/ai;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    .line 134
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->e()Landroid/widget/TextView;

    move-result-object v0

    .line 116
    const v1, 0x7f0b01fc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 117
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 125
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->a(Z)V

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->f:Lcom/sec/chaton/settings/tellfriends/ad;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/an;->a(Lcom/sec/chaton/settings/tellfriends/ad;)V

    .line 127
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 156
    const/16 v0, 0xbb9

    if-ne p1, v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 164
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/an;

    invoke-direct {v0, p1}, Lcom/sec/chaton/settings/tellfriends/an;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    .line 37
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onAttach(Landroid/app/Activity;)V

    .line 38
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h:Landroid/app/Activity;

    .line 39
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 150
    :goto_0
    return-void

    .line 140
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 141
    const-string v1, "INTENT_FROM"

    const-string v2, "TWITTER_SUB_MENU"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 145
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/tellfriends/TwitterMessageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    const-string v1, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 138
    :sswitch_data_0
    .sparse-switch
        0x7f07029d -> :sswitch_0
        0x7f070457 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->i:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->i:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->a()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->g()V

    .line 70
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDestroy()V

    .line 71
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDetach()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->h:Landroid/app/Activity;

    .line 45
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onPause()V

    .line 63
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->i()V

    .line 64
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 168
    sub-int v0, p4, p3

    .line 170
    if-lt p2, v0, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->a(Z)V

    .line 175
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuFragment;->c()V

    .line 177
    :cond_0
    return-void
.end method
