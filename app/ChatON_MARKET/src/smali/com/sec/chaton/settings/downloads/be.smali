.class Lcom/sec/chaton/settings/downloads/be;
.super Ljava/lang/Object;
.source "FontDownloads.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/FontDownloads;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/be;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 308
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_font : clicked. choice filter : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_0
    if-gtz p2, :cond_2

    .line 314
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "setting_used_font_filter_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v0, v1

    .line 320
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/be;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/FontDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 321
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/be;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/FontDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/be;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v2, v5, v1, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 322
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 323
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 324
    const-string v2, "download_font, showSelectFilterPopup(), dismiss. restartLoader()\n - selected filter id :  %s\n - selected fileter title : %s"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-nez v0, :cond_3

    const-string v1, "(all selected)"

    :goto_1
    aput-object v1, v3, v4

    const/4 v1, 0x1

    if-nez v0, :cond_4

    const-string v0, "(all selected)"

    :goto_2
    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 326
    invoke-static {}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_1
    return-void

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/be;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/FontDownloads;->a:Ljava/util/ArrayList;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/FontFilter;

    .line 317
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "setting_used_font_filter_id"

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/FontFilter;->id:Ljava/lang/Integer;

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 324
    :cond_3
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/FontFilter;->id:Ljava/lang/Integer;

    goto :goto_1

    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/FontFilter;->title:Ljava/lang/String;

    goto :goto_2
.end method
