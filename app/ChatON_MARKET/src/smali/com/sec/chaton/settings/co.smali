.class public Lcom/sec/chaton/settings/co;
.super Landroid/app/AlertDialog;
.source "CustomTimePicker.java"


# instance fields
.field private final a:I

.field private final b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/widget/ImageButton;

.field private h:Landroid/widget/ImageButton;

.field private i:Landroid/widget/ImageButton;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/ImageButton;

.field private l:Landroid/widget/ImageButton;

.field private m:Landroid/widget/ImageButton;

.field private n:Landroid/widget/ImageButton;

.field private o:Landroid/widget/EditText;

.field private p:Landroid/widget/EditText;

.field private q:Landroid/widget/EditText;

.field private r:Landroid/widget/EditText;

.field private s:Landroid/widget/CheckBox;

.field private t:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 26
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/chaton/settings/co;->a:I

    .line 27
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/chaton/settings/co;->b:I

    .line 296
    new-instance v0, Lcom/sec/chaton/settings/cq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/cq;-><init>(Lcom/sec/chaton/settings/co;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    .line 53
    new-instance v1, Landroid/widget/ScrollView;

    invoke-direct {v1, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 55
    const v3, 0x7f03000e

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 57
    invoke-virtual {v1, v6}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    .line 58
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/settings/co;->setView(Landroid/view/View;IIII)V

    .line 59
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/co;->setIcon(I)V

    .line 61
    const v0, 0x7f070050

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 62
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 64
    const v0, 0x7f070051

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->g:Landroid/widget/ImageButton;

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->g:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    const v0, 0x7f070053

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->h:Landroid/widget/ImageButton;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->h:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v0, 0x7f070057

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->i:Landroid/widget/ImageButton;

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->i:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v0, 0x7f070059

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->j:Landroid/widget/ImageButton;

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->j:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    const v0, 0x7f070054

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->k:Landroid/widget/ImageButton;

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->k:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v0, 0x7f070056

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->l:Landroid/widget/ImageButton;

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->l:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v0, 0x7f07005a

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->m:Landroid/widget/ImageButton;

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->m:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v0, 0x7f07005c

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->n:Landroid/widget/ImageButton;

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->n:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/settings/co;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    const v0, 0x7f070052

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    iget v1, p0, Lcom/sec/chaton/settings/co;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 92
    const v0, 0x7f070055

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    .line 93
    const v0, 0x7f070058

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    iget v1, p0, Lcom/sec/chaton/settings/co;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 96
    const v0, 0x7f07005b

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    .line 97
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->b()V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    new-array v1, v7, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/settings/cz;

    invoke-direct {v3, p0, v2}, Lcom/sec/chaton/settings/cz;-><init>(Lcom/sec/chaton/settings/co;Z)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/cp;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cp;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/cr;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cr;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    new-array v1, v7, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/settings/cz;

    invoke-direct {v3, p0, v7}, Lcom/sec/chaton/settings/cz;-><init>(Lcom/sec/chaton/settings/co;Z)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/cs;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cs;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/ct;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ct;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    new-array v1, v7, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/settings/cz;

    invoke-direct {v3, p0, v2}, Lcom/sec/chaton/settings/cz;-><init>(Lcom/sec/chaton/settings/co;Z)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/cu;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cu;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/cv;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cv;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    new-array v1, v7, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/settings/cz;

    invoke-direct {v3, p0, v7}, Lcom/sec/chaton/settings/cz;-><init>(Lcom/sec/chaton/settings/co;Z)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/cw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cw;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/settings/cx;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cx;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 240
    const v0, 0x7f07005d

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/settings/co;->s:Landroid/widget/CheckBox;

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->s:Landroid/widget/CheckBox;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "Setting mute repeat"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->s:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/chaton/settings/cy;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/cy;-><init>(Lcom/sec/chaton/settings/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/co;I)I
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/chaton/settings/co;->c:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/co;I)I
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/chaton/settings/co;->e:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    return-object v0
.end method

.method private b()V
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 253
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour start Long"

    invoke-virtual {v0, v1, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 254
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Setting mute hour end Long"

    invoke-virtual {v2, v3, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 256
    new-instance v4, Lcom/sec/chaton/settings/cn;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/sec/chaton/settings/cn;-><init>(JJ)V

    .line 258
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 259
    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->f()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/co;->c:I

    .line 260
    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->g()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/co;->e:I

    .line 261
    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->h()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/co;->d:I

    .line 262
    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->i()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/co;->f:I

    .line 270
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    const-string v1, "%02d"

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/settings/co;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    const-string v1, "%02d"

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/settings/co;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    const-string v1, "%02d"

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/settings/co;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    const-string v1, "%02d"

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/settings/co;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 276
    return-void

    .line 264
    :cond_0
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/chaton/settings/co;->c:I

    .line 265
    iput v5, p0, Lcom/sec/chaton/settings/co;->e:I

    .line 266
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/chaton/settings/co;->d:I

    .line 267
    iput v5, p0, Lcom/sec/chaton/settings/co;->f:I

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/co;I)I
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/chaton/settings/co;->d:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 334
    iget v0, p0, Lcom/sec/chaton/settings/co;->d:I

    if-nez v0, :cond_0

    .line 335
    iget v0, p0, Lcom/sec/chaton/settings/co;->d:I

    add-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/sec/chaton/settings/co;->d:I

    .line 337
    :cond_0
    iget v0, p0, Lcom/sec/chaton/settings/co;->d:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/sec/chaton/settings/co;->d:I

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 339
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/co;I)I
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/chaton/settings/co;->f:I

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    .line 342
    iget v0, p0, Lcom/sec/chaton/settings/co;->d:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/sec/chaton/settings/co;->d:I

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->p:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 344
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/co;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/chaton/settings/co;->c:I

    return v0
.end method

.method private e()V
    .locals 5

    .prologue
    .line 347
    iget v0, p0, Lcom/sec/chaton/settings/co;->c:I

    if-nez v0, :cond_0

    .line 348
    iget v0, p0, Lcom/sec/chaton/settings/co;->c:I

    add-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/sec/chaton/settings/co;->c:I

    .line 350
    :cond_0
    iget v0, p0, Lcom/sec/chaton/settings/co;->c:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/sec/chaton/settings/co;->c:I

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 352
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/settings/co;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/chaton/settings/co;->e:I

    return v0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 355
    iget v0, p0, Lcom/sec/chaton/settings/co;->c:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/sec/chaton/settings/co;->c:I

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->o:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 357
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/settings/co;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/chaton/settings/co;->d:I

    return v0
.end method

.method private g()V
    .locals 5

    .prologue
    .line 359
    iget v0, p0, Lcom/sec/chaton/settings/co;->f:I

    if-nez v0, :cond_0

    .line 360
    iget v0, p0, Lcom/sec/chaton/settings/co;->f:I

    add-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lcom/sec/chaton/settings/co;->f:I

    .line 362
    :cond_0
    iget v0, p0, Lcom/sec/chaton/settings/co;->f:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lcom/sec/chaton/settings/co;->f:I

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 364
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/co;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/chaton/settings/co;->f:I

    return v0
.end method

.method private h()V
    .locals 5

    .prologue
    .line 367
    iget v0, p0, Lcom/sec/chaton/settings/co;->f:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lcom/sec/chaton/settings/co;->f:I

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->r:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 369
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    .line 372
    iget v0, p0, Lcom/sec/chaton/settings/co;->e:I

    if-nez v0, :cond_0

    .line 373
    iget v0, p0, Lcom/sec/chaton/settings/co;->e:I

    add-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lcom/sec/chaton/settings/co;->e:I

    .line 375
    :cond_0
    iget v0, p0, Lcom/sec/chaton/settings/co;->e:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lcom/sec/chaton/settings/co;->e:I

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 377
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->f()V

    return-void
.end method

.method private j()V
    .locals 5

    .prologue
    .line 380
    iget v0, p0, Lcom/sec/chaton/settings/co;->e:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lcom/sec/chaton/settings/co;->e:I

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/settings/co;->q:Landroid/widget/EditText;

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/settings/co;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 382
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->e()V

    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->d()V

    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->c()V

    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->j()V

    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->i()V

    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->h()V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings/co;->g()V

    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/settings/cn;
    .locals 6

    .prologue
    .line 385
    new-instance v0, Lcom/sec/chaton/settings/cn;

    iget v1, p0, Lcom/sec/chaton/settings/co;->c:I

    iget v2, p0, Lcom/sec/chaton/settings/co;->e:I

    iget v3, p0, Lcom/sec/chaton/settings/co;->d:I

    iget v4, p0, Lcom/sec/chaton/settings/co;->f:I

    iget-object v5, p0, Lcom/sec/chaton/settings/co;->s:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/cn;-><init>(IIIIZ)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 280
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 281
    iget v0, p0, Lcom/sec/chaton/settings/co;->c:I

    iget v1, p0, Lcom/sec/chaton/settings/co;->d:I

    if-ne v0, v1, :cond_0

    .line 282
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/co;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 286
    :goto_0
    return-void

    .line 284
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/co;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public show()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 290
    invoke-virtual {p0}, Lcom/sec/chaton/settings/co;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/co;->setTitle(Ljava/lang/CharSequence;)V

    .line 291
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/co;->setCanceledOnTouchOutside(Z)V

    .line 292
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/co;->setIcon(I)V

    .line 293
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    .line 294
    return-void
.end method
