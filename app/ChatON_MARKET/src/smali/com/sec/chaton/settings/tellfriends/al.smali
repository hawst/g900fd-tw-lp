.class public Lcom/sec/chaton/settings/tellfriends/al;
.super Lcom/sec/chaton/settings/tellfriends/aa;
.source "SnsHelperRenren.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/settings/tellfriends/m;

.field private c:Lcom/sec/chaton/settings/tellfriends/af;

.field private d:Lcom/sec/chaton/settings/tellfriends/ad;

.field private e:Lcom/sec/chaton/settings/tellfriends/ah;

.field private f:Lcom/sec/chaton/settings/tellfriends/common/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/al;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/aa;-><init>()V

    .line 23
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/am;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/am;-><init>(Lcom/sec/chaton/settings/tellfriends/al;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->f:Lcom/sec/chaton/settings/tellfriends/common/c;

    .line 149
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/m;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/al;->f:Lcom/sec/chaton/settings/tellfriends/common/c;

    invoke-direct {v0, p1, v1}, Lcom/sec/chaton/settings/tellfriends/m;-><init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    .line 150
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/al;)Lcom/sec/chaton/settings/tellfriends/ad;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->d:Lcom/sec/chaton/settings/tellfriends/ad;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/al;)Lcom/sec/chaton/settings/tellfriends/ah;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->e:Lcom/sec/chaton/settings/tellfriends/ah;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/al;)Lcom/sec/chaton/settings/tellfriends/af;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->c:Lcom/sec/chaton/settings/tellfriends/af;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/al;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/m;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/m;->e()V

    .line 211
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ac;)V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ad;)V
    .locals 2

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/al;->d:Lcom/sec/chaton/settings/tellfriends/ad;

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    const/16 v1, 0x2ee1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/m;->b(I)V

    .line 156
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/af;)V
    .locals 2

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/al;->c:Lcom/sec/chaton/settings/tellfriends/af;

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    const/16 v1, 0x2ee4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/m;->a(I)V

    .line 189
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V
    .locals 3

    .prologue
    .line 166
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/al;->e:Lcom/sec/chaton/settings/tellfriends/ah;

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    const/16 v1, 0x2ee2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/chaton/settings/tellfriends/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 172
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/al;->e:Lcom/sec/chaton/settings/tellfriends/ah;

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    const/16 v1, 0x2ee2

    invoke-virtual {v0, v1, p1, p3}, Lcom/sec/chaton/settings/tellfriends/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/m;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/m;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/al;->b:Lcom/sec/chaton/settings/tellfriends/m;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/m;->a()Z

    move-result v0

    return v0
.end method
