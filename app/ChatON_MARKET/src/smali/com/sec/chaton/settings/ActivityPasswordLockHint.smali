.class public Lcom/sec/chaton/settings/ActivityPasswordLockHint;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityPasswordLockHint.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-direct {v0}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;-><init>()V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 36
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockHint;->a(Landroid/app/Activity;)V

    .line 39
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 27
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-static {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockHint;->a(Landroid/app/Activity;)V

    .line 30
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 45
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockHint;->finish()V

    .line 47
    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 55
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityPasswordLockHint;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 95
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
