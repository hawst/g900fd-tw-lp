.class public Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;
.super Landroid/support/v4/app/Fragment;
.source "DeleteAccountPreference.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# instance fields
.field a:Landroid/os/Handler;

.field public b:Landroid/os/Handler;

.field private c:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private d:Lcom/sec/chaton/d/ap;

.field private e:Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

.field private f:Lcom/sec/chaton/d/a/ak;

.field private g:Lcom/coolots/sso/a/a;

.field private h:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 229
    new-instance v0, Lcom/sec/chaton/settings/dd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/dd;-><init>(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->a:Landroid/os/Handler;

    .line 271
    new-instance v0, Lcom/sec/chaton/settings/de;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/de;-><init>(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->f:Lcom/sec/chaton/d/a/ak;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->e:Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/d/ap;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->d:Lcom/sec/chaton/d/ap;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/d/a/ak;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->f:Lcom/sec/chaton/d/a/ak;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 187
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 190
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->setRetainInstance(Z)V

    .line 191
    new-instance v0, Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    invoke-direct {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->e:Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->e:Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;->setCancelable(Z)V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->b:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->d:Lcom/sec/chaton/d/ap;

    .line 194
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->c:Lcom/sec/chaton/d/a;

    .line 196
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    .line 197
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->g:Lcom/coolots/sso/a/a;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 203
    const-string v0, "onCreate:mChatonV.setListener()"

    const-class v1, Lcom/sec/chaton/settings/DeleteAccountPreference;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->c(Landroid/content/Context;)I

    .line 209
    const-string v0, "onCreate:mChatonV.removeAccountInDevice()"

    const-class v1, Lcom/sec/chaton/settings/DeleteAccountPreference;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->e:Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 215
    return-void

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->c:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 220
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 222
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->h:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 225
    const-string v0, "onCreate:mChatonV.setListener(null)"

    const-class v1, Lcom/sec/chaton/settings/DeleteAccountPreference;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 339
    return-void
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 2

    .prologue
    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveRemoveAccount"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/settings/DeleteAccountPreference;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    if-eqz p1, :cond_1

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->c:Lcom/sec/chaton/d/a;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->c:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->f:Lcom/sec/chaton/d/a/ak;

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->b:Landroid/os/Handler;

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
