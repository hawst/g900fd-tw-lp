.class Lcom/sec/chaton/settings/at;
.super Ljava/lang/Object;
.source "ActivityNoti.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityNoti;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/chaton/settings/at;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 214
    if-eqz p2, :cond_0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/settings/at;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/ActivityNoti;->c(Lcom/sec/chaton/settings/ActivityNoti;Z)Z

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings/at;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->a(Lcom/sec/chaton/settings/ActivityNoti;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting show blackscreen popup"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/settings/at;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/ActivityNoti;->d(Lcom/sec/chaton/settings/ActivityNoti;Z)V

    .line 223
    :goto_0
    return v3

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/at;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/ActivityNoti;->c(Lcom/sec/chaton/settings/ActivityNoti;Z)Z

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings/at;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->a(Lcom/sec/chaton/settings/ActivityNoti;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting show blackscreen popup"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/settings/at;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/ActivityNoti;->d(Lcom/sec/chaton/settings/ActivityNoti;Z)V

    goto :goto_0
.end method
