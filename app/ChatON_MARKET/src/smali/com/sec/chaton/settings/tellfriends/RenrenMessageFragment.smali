.class public Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;
.source "RenrenMessageFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/u;
.implements Lcom/sec/chaton/settings/tellfriends/common/v;


# instance fields
.field private b:Lcom/sec/chaton/settings/tellfriends/al;

.field private c:Lcom/sec/chaton/settings/tellfriends/common/t;


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/al;

    .line 24
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    .line 29
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/t;

    const-string v1, "480"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "KSC5601"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/t;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    aput-object v2, v0, v1

    .line 31
    const-string v1, "480"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a(I)V

    .line 32
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a([Landroid/text/InputFilter;)V

    .line 33
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/v;)V

    .line 34
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/u;)V

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->d()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->d()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->d()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 94
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/t;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/tellfriends/common/t;->a(Ljava/lang/String;)I

    move-result v0

    .line 96
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->b(I)V

    .line 98
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->c()V

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/al;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/t;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/t;-><init>(Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;)V

    invoke-virtual {v0, p2, v1}, Lcom/sec/chaton/settings/tellfriends/al;->a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V

    .line 89
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 48
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/al;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/al;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/al;

    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->a()V

    .line 53
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onDestroy()V

    .line 54
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onResume()V

    .line 60
    return-void
.end method
