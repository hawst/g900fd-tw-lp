.class Lcom/sec/chaton/settings/tellfriends/s;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "RenrenManager.java"


# instance fields
.field protected c:Ljava/lang/String;

.field final synthetic d:Lcom/sec/chaton/settings/tellfriends/m;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/s;->d:Lcom/sec/chaton/settings/tellfriends/m;

    .line 174
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/s;->c:Ljava/lang/String;

    .line 175
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Landroid/os/Bundle;)I
    .locals 6

    .prologue
    const/16 v1, -0x3e8

    .line 183
    .line 184
    const/4 v0, -0x1

    .line 187
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/m;->f()Lcom/renren/android/Renren;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/renren/android/Renren;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 189
    if-eqz v2, :cond_0

    .line 190
    const-string v3, "json"

    invoke-static {v2, v3}, Lcom/renren/android/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/renren/android/f; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    move-object v1, v2

    .line 206
    :goto_1
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/s;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/s;->f:Ljava/lang/Object;

    .line 207
    return v0

    .line 192
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/s;->d:Lcom/sec/chaton/settings/tellfriends/m;

    const-string v3, "null response"

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/tellfriends/m;->g(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/renren/android/f; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    .line 193
    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/s;->d:Lcom/sec/chaton/settings/tellfriends/m;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "runtime exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/tellfriends/m;->h(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V

    .line 199
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->printStackTrace()V

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 205
    goto :goto_1

    .line 201
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 202
    const/16 v0, -0x3ef

    .line 203
    invoke-virtual {v2}, Lcom/renren/android/f;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-virtual {v2}, Lcom/renren/android/f;->printStackTrace()V

    goto :goto_1
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/s;->c:Ljava/lang/String;

    return-object v0
.end method
