.class public Lcom/sec/chaton/settings/tellfriends/ar;
.super Landroid/app/Dialog;
.source "TwDialog.java"


# static fields
.field static final a:[F

.field static final b:[F

.field static final c:Landroid/widget/FrameLayout$LayoutParams;


# instance fields
.field public d:Landroid/content/Context;

.field e:Lcom/sec/chaton/settings/tellfriends/ax;

.field f:Landroid/os/Handler;

.field private g:Ltwitter4j/Twitter;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/chaton/settings/tellfriends/common/b;

.field private k:Landroid/app/ProgressDialog;

.field private l:Landroid/webkit/WebView;

.field private m:Landroid/widget/FrameLayout;

.field private n:Landroid/widget/ImageView;

.field private o:Z

.field private p:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, -0x1

    .line 52
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ar;->a:[F

    .line 53
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ar;->b:[F

    .line 55
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ar;->c:Landroid/widget/FrameLayout$LayoutParams;

    return-void

    .line 52
    :array_0
    .array-data 4
        0x41f00000    # 30.0f
        0x42700000    # 60.0f
    .end array-data

    .line 53
    :array_1
    .array-data 4
        0x41f00000    # 30.0f
        0x42700000    # 60.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ltwitter4j/Twitter;Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    const v0, 0x1030010

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 65
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->i:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->o:Z

    .line 75
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->p:Ljava/util/Timer;

    .line 76
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->e:Lcom/sec/chaton/settings/tellfriends/ax;

    .line 430
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/av;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->f:Landroid/os/Handler;

    .line 80
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->d:Landroid/content/Context;

    .line 81
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/ar;->h:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/sec/chaton/settings/tellfriends/ar;->j:Lcom/sec/chaton/settings/tellfriends/common/b;

    .line 83
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/ar;->g:Ltwitter4j/Twitter;

    .line 92
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/ar;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->p:Ljava/util/Timer;

    return-object p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 170
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->n:Landroid/widget/ImageView;

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->n:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/au;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/au;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->n:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 186
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 189
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 190
    new-instance v1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    .line 191
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 193
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 194
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 195
    invoke-virtual {v1}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->requestFocus(I)Z

    .line 198
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 199
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 200
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/ay;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/tellfriends/ay;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;)V

    const-string v3, "PinCode"

    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/ar;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    sget-object v2, Lcom/sec/chaton/settings/tellfriends/ar;->c:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 204
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/az;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/settings/tellfriends/az;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;Lcom/sec/chaton/settings/tellfriends/as;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 206
    invoke-virtual {v0, p1, p1, p1, p1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 207
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 208
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 209
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/ar;)Lcom/sec/chaton/settings/tellfriends/common/b;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->j:Lcom/sec/chaton/settings/tellfriends/common/b;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 426
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/ar;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 427
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->dismiss()V

    .line 428
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/ar;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->o:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->m:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->p:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/settings/tellfriends/ar;)Ltwitter4j/Twitter;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->g:Ltwitter4j/Twitter;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/settings/tellfriends/ar;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->b()V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->l:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 149
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->o:Z

    if-nez v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 153
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 155
    :cond_2
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->o:Z

    .line 166
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 167
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 96
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 98
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/as;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/as;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/ar;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b000d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->k:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/at;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/at;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 119
    invoke-virtual {p0, v5}, Lcom/sec/chaton/settings/tellfriends/ar;->requestWindowFeature(I)Z

    .line 120
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->m:Landroid/widget/FrameLayout;

    .line 122
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/ar;->a()V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 125
    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/ar;->a(I)V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->m:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ar;->n:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->m:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/tellfriends/ar;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 142
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/ar;->o:Z

    .line 160
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 161
    return-void
.end method
