.class public Lcom/sec/chaton/settings/ActivityNoti;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "ActivityNoti.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/lang/String;

.field b:Landroid/preference/CheckBoxPreference;

.field c:Landroid/preference/CheckBoxPreference;

.field d:Landroid/preference/CheckBoxPreference;

.field e:Landroid/preference/CheckBoxPreference;

.field f:Landroid/app/AlarmManager;

.field g:Lcom/sec/common/a/d;

.field h:Lcom/sec/chaton/settings/co;

.field i:Landroid/content/DialogInterface$OnClickListener;

.field j:Landroid/content/DialogInterface$OnClickListener;

.field private k:Landroid/content/Context;

.field private l:Lcom/sec/chaton/util/ab;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:[Ljava/lang/String;

.field private u:Landroid/preference/Preference;

.field private v:Landroid/preference/Preference;

.field private w:Landroid/preference/Preference;

.field private x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private y:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    .line 55
    const-string v0, "Settings"

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->a:Ljava/lang/String;

    .line 68
    iput-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    .line 72
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->m:Z

    .line 73
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    .line 74
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->o:Z

    .line 75
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    .line 76
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->q:I

    .line 91
    iput-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->g:Lcom/sec/common/a/d;

    .line 92
    iput-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    .line 403
    new-instance v0, Lcom/sec/chaton/settings/bb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/bb;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->i:Landroid/content/DialogInterface$OnClickListener;

    .line 449
    new-instance v0, Lcom/sec/chaton/settings/bc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/bc;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->j:Landroid/content/DialogInterface$OnClickListener;

    .line 631
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityNoti;I)I
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/chaton/settings/ActivityNoti;->r:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityNoti;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method private a(Ljava/lang/Long;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 484
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 485
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    const v6, 0x7f08001b

    const v5, 0x7f08003f

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 155
    const-string v0, "pref_item_push_notification"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->e:Landroid/preference/CheckBoxPreference;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting Notification"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 157
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting Notification"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->e:Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0194

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->e:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings/as;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/as;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 196
    const-string v0, "pref_item_blackscreen_popup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->c:Landroid/preference/CheckBoxPreference;

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show blackscreen popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 199
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show blackscreen popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 205
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->c:Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 206
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0174

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->c:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings/at;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/at;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 227
    const-string v0, "pref_item_popup_type"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->w:Landroid/preference/Preference;

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting is simple popup"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting is simple popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 235
    :goto_2
    if-eqz v0, :cond_4

    .line 236
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b032c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->w:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 237
    iput v4, p0, Lcom/sec/chaton/settings/ActivityNoti;->s:I

    .line 242
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->w:Landroid/preference/Preference;

    new-instance v2, Lcom/sec/chaton/settings/au;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/au;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 288
    const-string v0, "pref_item_alert_group_chat"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->d:Landroid/preference/CheckBoxPreference;

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting alert_new_groupchat"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_5

    .line 290
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->o:Z

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting alert_new_groupchat"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 296
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->d:Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->o:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 297
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b01ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->d:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings/ax;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/ax;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    const-string v0, "pref_item_received_message"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->b:Landroid/preference/CheckBoxPreference;

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show receive message"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_6

    .line 329
    iput-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->m:Z

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show receive message"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 335
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->b:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->m:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->b:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->b:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings/ay;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ay;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 366
    const-string v0, "pref_item_type"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->u:Landroid/preference/Preference;

    .line 368
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->a(Z)V

    .line 369
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    if-eqz v0, :cond_0

    .line 370
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->b(Z)V

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->u:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings/az;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/az;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 385
    const-string v0, "pref_item_mute"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->v:Landroid/preference/Preference;

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/a;->g()Lcom/sec/chaton/chat/notification/d;

    .line 387
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityNoti;->c()V

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->v:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings/ba;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ba;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 400
    return-void

    .line 160
    :cond_1
    iput-boolean v4, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting Notification"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 202
    :cond_2
    iput-boolean v4, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show blackscreen popup"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    .line 232
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting is simple popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    move v0, v1

    .line 233
    goto/16 :goto_2

    .line 239
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b032d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->w:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 240
    iput v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->s:I

    goto/16 :goto_3

    .line 293
    :cond_5
    iput-boolean v4, p0, Lcom/sec/chaton/settings/ActivityNoti;->o:Z

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting alert_new_groupchat"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_4

    .line 332
    :cond_6
    iput-boolean v4, p0, Lcom/sec/chaton/settings/ActivityNoti;->m:Z

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v1, "Setting show receive message"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_5
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityNoti;Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 560
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 561
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 562
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 563
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 524
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/a;->i()Ljava/lang/String;

    move-result-object v0

    .line 526
    const-string v1, "ALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->t:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 536
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->u:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 545
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->b:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 547
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    if-eqz v0, :cond_0

    .line 548
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/ActivityNoti;->b(Z)V

    .line 551
    :cond_0
    return-void

    .line 528
    :cond_1
    const-string v1, "MELODY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 529
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->t:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 530
    :cond_2
    const-string v1, "VIBRATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->t:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    goto :goto_0

    .line 534
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->t:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityNoti;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityNoti;I)I
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/chaton/settings/ActivityNoti;->s:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 443
    new-instance v0, Lcom/sec/chaton/settings/co;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/co;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    const/4 v1, -0x2

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/ActivityNoti;->j:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/settings/co;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 445
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    const v3, 0x7f0b0039

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/chaton/settings/co;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->h:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->show()V

    .line 447
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityNoti;Z)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/ActivityNoti;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->w:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->w:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 557
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/ActivityNoti;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->s:I

    return v0
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const-wide/16 v4, 0x0

    .line 489
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->q:I

    .line 490
    const-string v0, ""

    .line 492
    iget v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->q:I

    packed-switch v1, :pswitch_data_0

    .line 515
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityNoti;->v:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 517
    return-void

    .line 494
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ~ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/settings/ActivityNoti;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 498
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour start Long"

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 499
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Setting mute hour end Long"

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 500
    new-instance v4, Lcom/sec/chaton/settings/cn;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/sec/chaton/settings/cn;-><init>(JJ)V

    .line 502
    const-string v0, "%02d:%02d ~ %02d:%02d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->h()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->i()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 505
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 508
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 509
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 510
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 511
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 512
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute repeat"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic c(Lcom/sec/chaton/settings/ActivityNoti;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/settings/ActivityNoti;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->r:I

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/ActivityNoti;Z)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/ActivityNoti;->b(Z)V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/ActivityNoti;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/chaton/settings/ActivityNoti;->o:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/settings/ActivityNoti;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->y:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->w:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/ActivityNoti;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/chaton/settings/ActivityNoti;->m:Z

    return p1
.end method

.method static synthetic g(Lcom/sec/chaton/settings/ActivityNoti;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->q:I

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityNoti;->c()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityNoti;->b()V

    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/settings/ActivityNoti;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->x:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 618
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 567
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 569
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x7f0b032d

    const v5, 0x7f0b032c

    const/4 v4, 0x0

    .line 102
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "gotoAlert"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 115
    :cond_0
    const v0, 0x7f05000e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->addPreferencesFromResource(I)V

    .line 117
    iput-object p0, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    .line 119
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->l:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->f:Landroid/app/AlarmManager;

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->t:[Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->x:Ljava/util/Map;

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->x:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b032f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->x:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0330

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->y:[Ljava/lang/String;

    .line 140
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    .line 141
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 597
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onDestroy()V

    .line 598
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 574
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onPause()V

    .line 575
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 604
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onRestart()V

    .line 606
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 582
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onResume()V

    .line 583
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityNoti;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 589
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->a(Z)V

    .line 590
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->p:Z

    if-eqz v0, :cond_0

    .line 591
    iget-boolean v0, p0, Lcom/sec/chaton/settings/ActivityNoti;->n:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/ActivityNoti;->b(Z)V

    .line 593
    :cond_0
    return-void

    .line 586
    :catch_0
    move-exception v0

    .line 587
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 145
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityNoti;->finish()V

    .line 147
    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 612
    return-void
.end method
