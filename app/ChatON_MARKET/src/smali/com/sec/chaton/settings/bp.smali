.class Lcom/sec/chaton/settings/bp;
.super Ljava/lang/Object;
.source "ActivityPasswordLockView.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityPasswordLockView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityPasswordLockView;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/chaton/settings/bp;->a:Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 141
    const-string v0, "passwordLock select ok"

    iget-object v1, p0, Lcom/sec/chaton/settings/bp;->a:Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-static {v1}, Lcom/sec/chaton/settings/ActivityPasswordLockView;->a(Lcom/sec/chaton/settings/ActivityPasswordLockView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "now password state : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/bp;->a:Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-static {v1}, Lcom/sec/chaton/settings/ActivityPasswordLockView;->b(Lcom/sec/chaton/settings/ActivityPasswordLockView;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/bp;->a:Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-static {v2}, Lcom/sec/chaton/settings/ActivityPasswordLockView;->c(Lcom/sec/chaton/settings/ActivityPasswordLockView;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/bp;->a:Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-static {v1}, Lcom/sec/chaton/settings/ActivityPasswordLockView;->a(Lcom/sec/chaton/settings/ActivityPasswordLockView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/bp;->a:Lcom/sec/chaton/settings/ActivityPasswordLockView;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    const-string v1, "MODE"

    const-string v2, "CHANGE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    iget-object v1, p0, Lcom/sec/chaton/settings/bp;->a:Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/ActivityPasswordLockView;->startActivity(Landroid/content/Intent;)V

    .line 153
    const/4 v0, 0x0

    return v0
.end method
