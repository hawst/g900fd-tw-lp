.class public Lcom/sec/chaton/settings/PostONHideFragment;
.super Landroid/support/v4/app/Fragment;
.source "PostONHideFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/en;


# static fields
.field static a:Landroid/app/ProgressDialog;


# instance fields
.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/widget/ListView;

.field private d:Lcom/sec/chaton/d/v;

.field private e:Lcom/sec/chaton/settings/ek;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field private p:Z

.field private q:Lcom/sec/chaton/settings/PostONHideActivity;

.field private r:Landroid/view/MenuItem;

.field private s:Landroid/view/MenuItem;

.field private t:Landroid/view/ViewStub;

.field private final u:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->j:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->k:Ljava/util/ArrayList;

    .line 366
    new-instance v0, Lcom/sec/chaton/settings/ep;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/ep;-><init>(Lcom/sec/chaton/settings/PostONHideFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->u:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/PostONHideFragment;)Lcom/sec/chaton/d/v;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->d:Lcom/sec/chaton/d/v;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->t:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->i:Landroid/view/View;

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->i:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 178
    iget-object v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->i:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 179
    iget-object v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->i:Landroid/view/View;

    const v3, 0x7f07014d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 180
    const v3, 0x7f02034e

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 181
    const v0, 0x7f0b002a

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 182
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :cond_0
    return-void
.end method

.method private a(Lcom/sec/chaton/a/a/f;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 446
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_3

    .line 447
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONBlindList;

    .line 448
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONBlindList;->blind:Ljava/util/ArrayList;

    .line 449
    iget-object v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 450
    iget-object v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 452
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;

    .line 454
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/d;->h(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 455
    iget-object v3, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Add blindedBuddies: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 460
    new-array v3, v2, [Ljava/lang/String;

    .line 461
    :goto_1
    if-ge v1, v2, :cond_2

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 461
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 464
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/chaton/settings/PostONHideFragment;->a([Ljava/lang/String;)V

    .line 473
    :goto_2
    return-void

    .line 466
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 467
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 468
    invoke-direct {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->a()V

    .line 469
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 470
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 471
    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/sec/chaton/settings/PostONHideFragment;Lcom/sec/chaton/a/a/f;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/PostONHideFragment;->a(Lcom/sec/chaton/a/a/f;)V

    return-void
.end method

.method private a([Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v1, 0x0

    .line 399
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 401
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 402
    new-instance v3, Lcom/sec/chaton/block/aa;

    aget-object v4, p1, v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    aget-object v6, p1, v0

    invoke-static {v5, v6}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/chaton/block/aa;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    .line 410
    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0b0182

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 412
    iget-object v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/PostONHideActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 434
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->k:Ljava/util/ArrayList;

    .line 436
    invoke-direct {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->b()V

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v8, :cond_3

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 439
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->l:Ljava/lang/Boolean;

    .line 440
    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0141

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/settings/PostONHideFragment;->m:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 442
    :cond_3
    return-void

    .line 425
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_5

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 430
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 483
    new-instance v0, Lcom/sec/chaton/settings/ek;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/ek;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->e:Lcom/sec/chaton/settings/ek;

    .line 484
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->e:Lcom/sec/chaton/settings/ek;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/ek;->a(Lcom/sec/chaton/settings/en;)V

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->e:Lcom/sec/chaton/settings/ek;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 486
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const v4, 0x7f0b0182

    const/4 v0, -0x1

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 245
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 246
    packed-switch p1, :pswitch_data_0

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 248
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 249
    const-string v0, "Get blind list from user"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "blindlist"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 252
    array-length v0, v2

    iput v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 254
    iget-object v3, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    invoke-virtual {v3}, Lcom/sec/chaton/settings/PostONHideActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 261
    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 262
    iget-object v5, p0, Lcom/sec/chaton/settings/PostONHideFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 264
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->d:Lcom/sec/chaton/d/v;

    iget-object v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->k:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/chaton/settings/PostONHideFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/d/v;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 265
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->l:Ljava/lang/Boolean;

    .line 266
    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0041

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v6}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 272
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 273
    const-string v0, "Remove multiple blinded buddy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/PostONHideActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/chaton/settings/PostONHideFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->d:Lcom/sec/chaton/d/v;

    invoke-virtual {v0}, Lcom/sec/chaton/d/v;->b()V

    .line 282
    sget-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 283
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 284
    const-string v0, "Start getInteractionBlindList Sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->l:Ljava/lang/Boolean;

    .line 288
    const-string v0, "block_buddy_result"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/PostONHideFragment;->a([Ljava/lang/String;)V

    .line 289
    iput-boolean v6, p0, Lcom/sec/chaton/settings/PostONHideFragment;->p:Z

    goto/16 :goto_0

    .line 246
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 101
    check-cast p1, Lcom/sec/chaton/settings/PostONHideActivity;

    iput-object p1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    .line 102
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/PostONHideActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    const v1, 0x7f0b0182

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/PostONHideFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 83
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 87
    new-instance v0, Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->u:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/v;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->d:Lcom/sec/chaton/d/v;

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->d:Lcom/sec/chaton/d/v;

    invoke-virtual {v0}, Lcom/sec/chaton/d/v;->b()V

    .line 89
    const-string v0, "Start getInteractionBlindList Sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->l:Ljava/lang/Boolean;

    .line 91
    const v0, 0x7f0b00b4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/PostONHideFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->m:Ljava/lang/String;

    .line 92
    iput-boolean v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->p:Z

    .line 95
    iput v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    .line 96
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 187
    const v0, 0x7f0f0006

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 188
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 189
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 119
    const v0, 0x7f0300dc

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/PostONHideActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b0182

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/PostONHideFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 125
    const v0, 0x7f0703da

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->c:Landroid/widget/ListView;

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->c:Landroid/widget/ListView;

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDescendantFocusability(I)V

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->o:Ljava/util/ArrayList;

    .line 130
    const v0, 0x7f0703db

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->f:Landroid/widget/LinearLayout;

    .line 131
    const v0, 0x7f0703dc

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->h:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->h:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 133
    iget-object v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->h:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 134
    iget-object v2, p0, Lcom/sec/chaton/settings/PostONHideFragment;->h:Landroid/view/View;

    const v4, 0x7f07014d

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 135
    const v4, 0x7f02034b

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 136
    const v0, 0x7f0b0155

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 137
    const v0, 0x7f0b0185

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 140
    const v0, 0x7f0703dd

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->g:Landroid/widget/LinearLayout;

    .line 141
    const v0, 0x7f0703de

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->t:Landroid/view/ViewStub;

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 153
    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    sput-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    .line 154
    sget-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/eo;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/eo;-><init>(Lcom/sec/chaton/settings/PostONHideFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 169
    sget-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 170
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 171
    return-object v3
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 359
    sget-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 360
    sget-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 362
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 363
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    .line 108
    return-void
.end method

.method public onItemClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 490
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 491
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 492
    sget-object v1, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 493
    iput-object p1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->m:Ljava/lang/String;

    .line 494
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->l:Ljava/lang/Boolean;

    .line 495
    iget-object v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->d:Lcom/sec/chaton/d/v;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/d/v;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 496
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostON Buddy Show: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 218
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070577

    if-ne v1, v2, :cond_0

    .line 219
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 220
    const-string v2, "BUDDY_SORT_STYLE"

    const/16 v3, 0xb

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 221
    const-string v2, "ACTIVITY_PURPOSE"

    const/16 v3, 0x9

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 222
    const-string v2, "ACTIVITY_PURPOSE_ARG"

    iget-object v3, p0, Lcom/sec/chaton/settings/PostONHideFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 223
    const-string v2, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 224
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/settings/PostONHideFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 226
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070578

    if-ne v1, v2, :cond_1

    .line 227
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/poston/PostONHideListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 229
    const-string v3, "block_buddy_list"

    iget-object v4, p0, Lcom/sec/chaton/settings/PostONHideFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 230
    const-string v3, "block_buddy_type"

    const-string v4, "post_on"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 233
    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/settings/PostONHideFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 236
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_2

    .line 237
    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 240
    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 479
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 480
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 194
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 195
    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    .line 196
    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->s:Landroid/view/MenuItem;

    .line 197
    const v0, 0x7f070578

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    .line 198
    const v0, 0x7f070577

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->s:Landroid/view/MenuItem;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->s:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v0

    .line 202
    if-gtz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->s:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 207
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 214
    :cond_1
    return-void

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->s:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 341
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/PostONHideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0b0182

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 344
    iget-object v1, p0, Lcom/sec/chaton/settings/PostONHideFragment;->q:Lcom/sec/chaton/settings/PostONHideActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/PostONHideActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 346
    iget-boolean v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->p:Z

    if-eqz v0, :cond_0

    .line 347
    iput-boolean v3, p0, Lcom/sec/chaton/settings/PostONHideFragment;->p:Z

    .line 355
    :goto_0
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->d:Lcom/sec/chaton/d/v;

    invoke-virtual {v0}, Lcom/sec/chaton/d/v;->b()V

    .line 350
    sget-object v0, Lcom/sec/chaton/settings/PostONHideFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 351
    const-string v0, "Start getInteractionBlindList Sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/PostONHideFragment;->l:Ljava/lang/Boolean;

    goto :goto_0
.end method
