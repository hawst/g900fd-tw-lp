.class public abstract Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;
.super Landroid/support/v4/app/Fragment;
.source "SnsMessageFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field protected a:Landroid/app/Activity;

.field private b:Lcom/sec/chaton/settings/tellfriends/common/u;

.field private c:Lcom/sec/chaton/settings/tellfriends/common/v;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/app/ProgressDialog;

.field private h:[Landroid/text/InputFilter;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Landroid/widget/Toast;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    .line 49
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->h:[Landroid/text/InputFilter;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->i:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->j:Ljava/lang/String;

    .line 53
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->k:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->m:Landroid/widget/Toast;

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    if-eqz p1, :cond_0

    .line 64
    const-string v0, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->i:Ljava/lang/String;

    .line 65
    const-string v0, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendID"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->j:Ljava/lang/String;

    .line 67
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 231
    const-string v0, "showProgressDialog() \t- null. create and show"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    const v2, 0x7f0b016f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/s;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/s;-><init>(Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 255
    :goto_0
    return-void

    .line 252
    :cond_0
    const-string v0, "showProgressDialog() \t- not null. show"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b029f

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "www.ChatON.com/invite.html"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->i:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 182
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->k:Ljava/lang/String;

    .line 193
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->h:[Landroid/text/InputFilter;

    if-eqz v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->h:[Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 203
    :goto_1
    return-void

    .line 185
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->k:Ljava/lang/String;

    goto :goto_0

    .line 190
    :cond_1
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->k:Ljava/lang/String;

    goto :goto_0

    .line 200
    :cond_2
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->l:I

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    .line 201
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    new-array v2, v5, [Landroid/text/InputFilter;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_1
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(I)V
    .locals 0

    .prologue
    .line 210
    iput p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->l:I

    .line 211
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/u;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/common/u;

    .line 280
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/v;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/v;

    .line 292
    return-void
.end method

.method protected a([Landroid/text/InputFilter;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->h:[Landroid/text/InputFilter;

    .line 207
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/v;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/v;

    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/tellfriends/common/v;->a(Landroid/text/Editable;)V

    .line 171
    :cond_0
    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected b(I)V
    .locals 5

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->e:Landroid/widget/TextView;

    const-string v1, "(%d/%d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->l:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->a(Z)V

    .line 259
    return-void
.end method

.method protected d()V
    .locals 3

    .prologue
    .line 262
    .line 263
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismissProgressDialog() \t- mProgressDialog : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". isSowing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->g:Landroid/app/ProgressDialog;

    .line 268
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 100
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 117
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->a:Landroid/app/Activity;

    .line 118
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 88
    if-eqz p1, :cond_0

    .line 93
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->a(Landroid/os/Bundle;)V

    .line 94
    return-void

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 139
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 140
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 141
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 71
    const v0, 0x7f0300fc

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->d:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->d:Landroid/view/View;

    const v1, 0x7f07023e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->e:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->d:Landroid/view/View;

    const v1, 0x7f07023d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->f:Landroid/widget/EditText;

    .line 76
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->e()V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->b(I)V

    .line 79
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->setHasOptionsMenu(Z)V

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 122
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 123
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->a:Landroid/app/Activity;

    .line 129
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 145
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a6

    if-ne v0, v1, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/common/u;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/common/u;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    const/4 v0, 0x1

    .line 156
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 135
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 108
    const-string v0, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendID"

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/v;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->c:Lcom/sec/chaton/settings/tellfriends/common/v;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/v;->a(Landroid/text/Editable;)V

    .line 164
    :cond_0
    return-void
.end method
