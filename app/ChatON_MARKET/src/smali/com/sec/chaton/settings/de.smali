.class Lcom/sec/chaton/settings/de;
.super Landroid/os/Handler;
.source "DeleteAccountPreference.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/chaton/settings/de;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 275
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 276
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 293
    :pswitch_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/de;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->c(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/d/a/ak;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/de;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->c(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/d/a/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/ak;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    const-string v0, "ChatON deregistration is success."

    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string v0, "delete_account"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/settings/de;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/settings/de;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/global/GlobalApplication;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/de;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 312
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/de;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->a(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 314
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 317
    :cond_2
    const-string v0, "ChatON deregistration is fail."

    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0b016c

    const v2, 0x7f0b002d

    const v3, 0x7f0b0037

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->a(III)Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;

    move-result-object v1

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_0
    .end packed-switch
.end method
