.class public Lcom/sec/chaton/settings/DeregisterFragment;
.super Landroid/support/v4/app/Fragment;
.source "DeregisterFragment.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# static fields
.field private static final l:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/util/ar;

.field public b:Landroid/os/Handler;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/CheckBox;

.field private e:Landroid/widget/Button;

.field private f:Landroid/app/ProgressDialog;

.field private g:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private h:Lcom/sec/chaton/d/a/ak;

.field private i:Lcom/sec/chaton/d/ap;

.field private j:Lcom/sec/common/a/d;

.field private k:Lcom/coolots/sso/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/chaton/settings2/SettingActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/DeregisterFragment;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->f:Landroid/app/ProgressDialog;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->j:Lcom/sec/common/a/d;

    .line 138
    new-instance v0, Lcom/sec/chaton/settings/dh;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/dh;-><init>(Lcom/sec/chaton/settings/DeregisterFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->a:Lcom/sec/chaton/util/ar;

    .line 170
    new-instance v0, Lcom/sec/chaton/settings/di;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/di;-><init>(Lcom/sec/chaton/settings/DeregisterFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/DeregisterFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/chaton/settings/DeregisterFragment;->f:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/DeregisterFragment;Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/a;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/chaton/settings/DeregisterFragment;->k:Lcom/coolots/sso/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/DeregisterFragment;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/chaton/settings/DeregisterFragment;->h:Lcom/sec/chaton/d/a/ak;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/chaton/settings/DeregisterFragment;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/DeregisterFragment;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->b()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/DeregisterFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->f:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->e:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->e:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/DeregisterFragment;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->k:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 168
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/DeregisterFragment;)Lcom/sec/chaton/d/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->g:Lcom/sec/chaton/d/a;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->e()V

    .line 206
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 207
    const v1, 0x7f0b029b

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b002d

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/dj;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/dj;-><init>(Lcom/sec/chaton/settings/DeregisterFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->j:Lcom/sec/common/a/d;

    .line 217
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->j:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->j:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 246
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/DeregisterFragment;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->c()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/settings/DeregisterFragment;)Lcom/sec/chaton/d/ap;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->i:Lcom/sec/chaton/d/ap;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/DeregisterFragment;)Lcom/sec/chaton/d/a/ak;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->h:Lcom/sec/chaton/d/a/ak;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/DeregisterFragment;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->d()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->b:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->i:Lcom/sec/chaton/d/ap;

    .line 63
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->g:Lcom/sec/chaton/d/a;

    .line 64
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 68
    const v0, 0x7f03009a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 70
    const v0, 0x7f0702f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->c:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0702f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->d:Landroid/widget/CheckBox;

    .line 73
    const v0, 0x7f0702f8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->e:Landroid/widget/Button;

    .line 75
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    .line 77
    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->d:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/chaton/settings/df;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/df;-><init>(Lcom/sec/chaton/settings/DeregisterFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->e:Landroid/widget/Button;

    new-instance v2, Lcom/sec/chaton/settings/dg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/dg;-><init>(Lcom/sec/chaton/settings/DeregisterFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    return-object v1
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 223
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 225
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->k:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->k:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 229
    const-string v0, "onDestroy:mChatonV.setListener(null)"

    sget-object v1, Lcom/sec/chaton/settings/DeregisterFragment;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->c()V

    .line 234
    invoke-direct {p0}, Lcom/sec/chaton/settings/DeregisterFragment;->e()V

    .line 235
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 2

    .prologue
    .line 257
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveRemoveAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/DeregisterFragment;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->g:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/DeregisterFragment;->a:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/settings/DeregisterFragment;->a:Lcom/sec/chaton/util/ar;

    const/16 v1, 0x7530

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ar;->a(I)V

    .line 273
    return-void
.end method
