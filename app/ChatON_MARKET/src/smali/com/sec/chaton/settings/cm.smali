.class public Lcom/sec/chaton/settings/cm;
.super Lcom/sec/common/f/a;
.source "BubbleSendDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 27
    iput-object p2, p0, Lcom/sec/chaton/settings/cm;->a:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 57
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .line 59
    if-eqz p1, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/settings/cm;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/cm;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0201ca

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/chaton/settings/cm;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 42
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 47
    iget-object v1, p0, Lcom/sec/chaton/settings/cm;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/cm;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/cd;->g(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/chaton/settings/cm;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 70
    if-eqz v0, :cond_0

    .line 71
    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/graphics/drawable/Drawable;)V

    .line 73
    :cond_0
    return-void
.end method
