.class public Lcom/sec/chaton/settings/AboutServiceFragment;
.super Landroid/support/v4/app/Fragment;
.source "AboutServiceFragment.java"


# static fields
.field public static d:I

.field public static e:I

.field private static final o:Ljava/lang/String;


# instance fields
.field a:[Ljava/lang/String;

.field b:[Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field public f:Landroid/os/Handler;

.field public g:Landroid/os/Handler;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ListView;

.field private l:Lcom/sec/chaton/settings/g;

.field private m:Landroid/content/Context;

.field private n:I

.field private p:Lcom/sec/chaton/d/a/ct;

.field private q:Lcom/coolots/sso/a/a;

.field private r:Z

.field private s:[Landroid/widget/TextView;

.field private t:[Landroid/widget/TextView;

.field private u:[Landroid/widget/Button;

.field private v:Z

.field private w:Z

.field private x:Landroid/app/ProgressDialog;

.field private y:Lcom/sec/chaton/io/entry/GetVersionNotice;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/AboutServiceFragment;->o:Ljava/lang/String;

    .line 100
    const/4 v0, 0x1

    sput v0, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    .line 101
    const/4 v0, 0x2

    sput v0, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 81
    iput-object v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->l:Lcom/sec/chaton/settings/g;

    .line 105
    iput-object v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->q:Lcom/coolots/sso/a/a;

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->r:Z

    .line 115
    iput-object v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->x:Landroid/app/ProgressDialog;

    .line 279
    new-instance v0, Lcom/sec/chaton/settings/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/d;-><init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->f:Landroid/os/Handler;

    .line 402
    new-instance v0, Lcom/sec/chaton/settings/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/e;-><init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->g:Landroid/os/Handler;

    .line 599
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/AboutServiceFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->x:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->m:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/AboutServiceFragment;Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/a;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->q:Lcom/coolots/sso/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/AboutServiceFragment;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->y:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/chaton/settings/AboutServiceFragment;->o:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->u:[Landroid/widget/Button;

    aget-object v0, v0, p1

    new-instance v1, Lcom/sec/chaton/settings/f;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/f;-><init>(Lcom/sec/chaton/settings/AboutServiceFragment;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/AboutServiceFragment;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->z:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/AboutServiceFragment;I)I
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->n:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->k:Landroid/widget/ListView;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 264
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v2, v0, v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateTargetVersion"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 270
    return-void

    .line 265
    :catch_0
    move-exception v0

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v5

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->v:Z

    return p1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 274
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.ACTION_DISMISS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 276
    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "more_tab_badge_update"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 277
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->w:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->s:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/AboutServiceFragment;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->r:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->t:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->u:[Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/AboutServiceFragment;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->c()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->x:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/AboutServiceFragment;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->z:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/chaton/settings/AboutServiceFragment;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->y:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->w:Z

    return v0
.end method

.method static synthetic k(Lcom/sec/chaton/settings/AboutServiceFragment;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->v:Z

    return v0
.end method

.method static synthetic l(Lcom/sec/chaton/settings/AboutServiceFragment;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->q:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/settings/AboutServiceFragment;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->n:I

    return v0
.end method

.method static synthetic n(Lcom/sec/chaton/settings/AboutServiceFragment;)I
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->n:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->n:I

    return v0
.end method

.method static synthetic o(Lcom/sec/chaton/settings/AboutServiceFragment;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->r:Z

    return v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 536
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 538
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 234
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 235
    const-string v0, "onConfigurationChagne"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 218
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 220
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    .line 221
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    .line 223
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->s:[Landroid/widget/TextView;

    .line 224
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->t:[Landroid/widget/TextView;

    .line 225
    new-array v0, v1, [Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->u:[Landroid/widget/Button;

    .line 227
    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->m:Landroid/content/Context;

    .line 229
    invoke-direct {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->b()V

    .line 230
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 122
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "onCreateView"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_0
    const v0, 0x7f0300ed

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 128
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->k:Landroid/widget/ListView;

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->k:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 130
    const v0, 0x7f030053

    invoke-virtual {p1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 137
    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    move-object v0, v1

    .line 138
    check-cast v0, Landroid/view/ViewGroup;

    const/high16 v3, 0x40000

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 139
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->k:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v5, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 144
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateUrl"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->c:Ljava/lang/String;

    .line 147
    new-instance v0, Lcom/sec/chaton/settings/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/g;-><init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->l:Lcom/sec/chaton/settings/g;

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->k:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->l:Lcom/sec/chaton/settings/g;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 152
    const v0, 0x7f070208

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->h:Landroid/widget/TextView;

    .line 153
    const v0, 0x7f070209

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->i:Landroid/widget/TextView;

    .line 154
    const v0, 0x7f07020a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->j:Landroid/widget/TextView;

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0026

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/j;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0302

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/j;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b00e3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/j;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->h:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/settings/a;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/a;-><init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->i:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/settings/b;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/b;-><init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->j:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/settings/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/c;-><init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    if-nez p3, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->f:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->p:Lcom/sec/chaton/d/a/ct;

    .line 207
    :cond_1
    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x15

    if-ge v0, v1, :cond_2

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->f:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/as;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/as;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->m:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/as;->a(Landroid/content/Context;)V

    .line 212
    :cond_2
    return-object v2
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 244
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 246
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDestroyView, mGetVersionNoticeTask : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->p:Lcom/sec/chaton/d/a/ct;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->q:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->q:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->m:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/d;)V

    .line 253
    :cond_1
    const/4 v0, 0x1

    sput v0, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    .line 254
    const/4 v0, 0x2

    sput v0, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->x:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/settings/AboutServiceFragment;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 259
    :cond_2
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 543
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 545
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 461
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 462
    return-void
.end method
