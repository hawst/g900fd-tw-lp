.class public Lcom/sec/chaton/settings/downloads/cd;
.super Ljava/lang/Object;
.source "SkinHelper.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/io/FilenameFilter;

.field private static final g:Ljava/io/FilenameFilter;

.field private static final h:Ljava/io/FilenameFilter;

.field private static final i:Ljava/io/FilenameFilter;

.field private static final j:Ljava/io/FilenameFilter;

.field private static final k:Ljava/io/FilenameFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    const-class v0, Lcom/sec/chaton/settings/downloads/cd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    .line 85
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    const-string v1, "-1"

    const v2, 0x7f020426

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    const-string v1, "-2"

    const v2, 0x7f020427

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    const-string v1, "-3"

    const v2, 0x7f020428

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    const-string v1, "-4"

    const v2, 0x7f020429

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    const-string v1, "-5"

    const v2, 0x7f02042a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->c:Ljava/util/Map;

    .line 92
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->c:Ljava/util/Map;

    const-string v1, "-1"

    const v2, 0x7f0203e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->c:Ljava/util/Map;

    const-string v1, "-2"

    const v2, 0x7f0203ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->c:Ljava/util/Map;

    const-string v1, "-3"

    const v2, 0x7f0203eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->c:Ljava/util/Map;

    const-string v1, "-4"

    const v2, 0x7f0203ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->c:Ljava/util/Map;

    const-string v1, "-5"

    const v2, 0x7f0203ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->d:Ljava/util/Map;

    .line 99
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->d:Ljava/util/Map;

    const-string v1, "-1"

    const v2, 0x7f0202ef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->d:Ljava/util/Map;

    const-string v1, "-2"

    const v2, 0x7f0202f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->d:Ljava/util/Map;

    const-string v1, "-3"

    const v2, 0x7f0202f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->d:Ljava/util/Map;

    const-string v1, "-4"

    const v2, 0x7f0202f2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->d:Ljava/util/Map;

    const-string v1, "-5"

    const v2, 0x7f0202f3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->e:Ljava/util/Map;

    .line 106
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->e:Ljava/util/Map;

    const-string v1, "-1"

    const v2, 0x7f0202ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->e:Ljava/util/Map;

    const-string v1, "-2"

    const v2, 0x7f0202eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->e:Ljava/util/Map;

    const-string v1, "-3"

    const v2, 0x7f0202ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->e:Ljava/util/Map;

    const-string v1, "-4"

    const v2, 0x7f0202ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->e:Ljava/util/Map;

    const-string v1, "-5"

    const v2, 0x7f0202ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    new-instance v0, Lcom/sec/chaton/settings/downloads/ce;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/ce;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->f:Ljava/io/FilenameFilter;

    .line 132
    new-instance v0, Lcom/sec/chaton/settings/downloads/cf;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/cf;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->g:Ljava/io/FilenameFilter;

    .line 151
    new-instance v0, Lcom/sec/chaton/settings/downloads/cg;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/cg;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->h:Ljava/io/FilenameFilter;

    .line 170
    new-instance v0, Lcom/sec/chaton/settings/downloads/ch;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/ch;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->i:Ljava/io/FilenameFilter;

    .line 189
    new-instance v0, Lcom/sec/chaton/settings/downloads/ci;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/ci;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->j:Ljava/io/FilenameFilter;

    .line 208
    new-instance v0, Lcom/sec/chaton/settings/downloads/cj;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/cj;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/cd;->k:Ljava/io/FilenameFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807
    return-void
.end method

.method public static a()I
    .locals 8

    .prologue
    const/16 v5, 0x140

    const/16 v1, 0xf0

    const/16 v7, 0x500

    const/16 v6, 0x320

    const/16 v0, 0x96

    .line 689
    const/4 v2, 0x0

    .line 690
    invoke-static {}, Lcom/sec/common/util/i;->e()I

    move-result v3

    .line 691
    invoke-static {}, Lcom/sec/common/util/i;->f()I

    move-result v4

    .line 694
    if-ne v3, v1, :cond_1

    if-ne v4, v5, :cond_1

    .line 695
    const/16 v1, 0x38

    .line 711
    :cond_0
    :goto_0
    if-nez v1, :cond_8

    .line 715
    :goto_1
    return v0

    .line 696
    :cond_1
    if-ne v3, v5, :cond_2

    const/16 v5, 0x1e0

    if-ne v4, v5, :cond_2

    .line 697
    const/16 v1, 0x4b

    goto :goto_0

    .line 698
    :cond_2
    const/16 v5, 0x1e0

    if-ne v3, v5, :cond_3

    if-ne v4, v6, :cond_3

    .line 699
    const/16 v1, 0x70

    goto :goto_0

    .line 700
    :cond_3
    if-ne v3, v6, :cond_4

    if-ne v4, v7, :cond_4

    move v1, v0

    .line 701
    goto :goto_0

    .line 702
    :cond_4
    const/16 v5, 0x2d0

    if-ne v3, v5, :cond_5

    if-ne v4, v7, :cond_5

    move v1, v0

    .line 703
    goto :goto_0

    .line 704
    :cond_5
    if-ne v3, v7, :cond_6

    if-ne v4, v6, :cond_6

    .line 705
    const/16 v1, 0x78

    goto :goto_0

    .line 706
    :cond_6
    const/16 v5, 0xa00

    if-ne v3, v5, :cond_7

    const/16 v3, 0x640

    if-eq v4, v3, :cond_0

    :cond_7
    move v1, v2

    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;[Ljava/io/File;)Landroid/graphics/drawable/Drawable;
    .locals 12

    .prologue
    .line 418
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 419
    :cond_0
    const/4 v0, 0x0

    .line 477
    :goto_0
    return-object v0

    .line 422
    :cond_1
    new-instance v7, Lcom/sec/chaton/settings/downloads/cm;

    const/4 v0, 0x0

    invoke-direct {v7, v0}, Lcom/sec/chaton/settings/downloads/cm;-><init>(Lcom/sec/chaton/settings/downloads/ce;)V

    .line 423
    const/4 v6, 0x0

    .line 425
    array-length v10, p2

    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v10, :cond_8

    aget-object v1, p2, v9

    .line 427
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 428
    const/4 v0, 0x0

    .line 433
    :try_start_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 435
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    :try_start_1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    invoke-static {v8, v4, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 438
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    .line 440
    invoke-virtual {v7, v2}, Lcom/sec/chaton/settings/downloads/cm;->a(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 444
    if-eqz v8, :cond_2

    .line 446
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 454
    :cond_2
    :goto_2
    const-string v1, "_"

    invoke-virtual {v11, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 457
    const/4 v2, 0x4

    :try_start_3
    aget-object v2, v1, v2

    const-string v3, "no"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_4

    move-result v2

    if-eqz v2, :cond_5

    :goto_3
    move-object v6, v0

    .line 425
    :cond_3
    :goto_4
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 444
    :catchall_0
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    :goto_5
    if-eqz v8, :cond_4

    .line 446
    :try_start_4
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 444
    :cond_4
    :goto_6
    throw v0

    .line 462
    :cond_5
    const/4 v2, 0x4

    :try_start_5
    aget-object v2, v1, v2

    const-string v3, "ps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 463
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x10100a7

    aput v3, v1, v2

    invoke-virtual {v7, v1, v0}, Lcom/sec/chaton/settings/downloads/cm;->addState([ILandroid/graphics/drawable/Drawable;)V

    move-object v0, v6

    goto :goto_3

    .line 464
    :cond_6
    const/4 v2, 0x4

    aget-object v1, v1, v2

    const-string v2, "fo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 465
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x101009c

    aput v3, v1, v2

    invoke-virtual {v7, v1, v0}, Lcom/sec/chaton/settings/downloads/cm;->addState([ILandroid/graphics/drawable/Drawable;)V
    :try_end_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_7
    move-object v0, v6

    goto :goto_3

    .line 472
    :cond_8
    if-eqz v6, :cond_9

    .line 473
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    invoke-virtual {v7, v0, v6}, Lcom/sec/chaton/settings/downloads/cm;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_9
    move-object v0, v7

    .line 477
    goto/16 :goto_0

    .line 441
    :catch_0
    move-exception v1

    .line 444
    :goto_7
    if-eqz v0, :cond_3

    .line 446
    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_4

    .line 447
    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_2

    .line 467
    :catch_4
    move-exception v0

    goto :goto_4

    .line 444
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 441
    :catch_5
    move-exception v0

    move-object v0, v8

    goto :goto_7
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 228
    invoke-static {p0}, Lcom/sec/common/util/l;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 230
    new-instance v1, Ljava/io/File;

    const-string v2, "skin"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 233
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 237
    :cond_0
    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 544
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 545
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 546
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_skin, installSkin, id/install : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_0
    invoke-static {p0, p2, v0}, Lcom/sec/common/util/r;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 553
    invoke-static {p0, p3, v0}, Lcom/sec/common/util/r;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3

    .line 557
    :try_start_1
    const-string v0, "com.sec.chaton.provider2"

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {}, Lcom/sec/common/util/i;->a()J

    move-result-wide v2

    invoke-static {v1, p1, v2, v3}, Lcom/sec/chaton/e/a/j;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;J)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3

    .line 592
    return-void

    .line 558
    :catch_0
    move-exception v0

    .line 559
    :try_start_2
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3

    .line 563
    :catch_1
    move-exception v0

    .line 564
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_1

    .line 565
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 568
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 569
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    .line 572
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 573
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 576
    :cond_3
    throw v0

    .line 560
    :catch_2
    move-exception v0

    .line 561
    :try_start_3
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3

    .line 577
    :catch_3
    move-exception v0

    .line 578
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_4

    .line 579
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 582
    :cond_4
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 583
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    .line 586
    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 587
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 590
    :cond_6
    throw v0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 521
    instance-of v0, p0, Lcom/sec/chaton/settings/downloads/cl;

    if-eqz v0, :cond_1

    .line 522
    check-cast p0, Lcom/sec/chaton/settings/downloads/cl;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/cl;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 524
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 525
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 540
    :cond_0
    :goto_0
    return-void

    .line 529
    :cond_1
    instance-of v0, p0, Lcom/sec/chaton/settings/downloads/cm;

    if-eqz v0, :cond_0

    .line 530
    check-cast p0, Lcom/sec/chaton/settings/downloads/cm;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/cm;->a()Ljava/util/List;

    move-result-object v1

    .line 532
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 533
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 534
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 538
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 257
    :try_start_0
    sget-object v1, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    const/4 v0, 0x1

    .line 262
    :cond_0
    :goto_0
    return v0

    .line 261
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 859
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/sec/chaton/settings/downloads/cd;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 860
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 861
    const-string v1, "download_skin, apply(), not valid"

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :cond_0
    :goto_0
    return v0

    .line 867
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "setting_change_skin"

    invoke-virtual {v2, v3, p0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "setting_change_skin_type"

    invoke-virtual {v2, v3, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "setting_change_bubble_send"

    invoke-virtual {v2, v3, p0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "setting_change_bubble_receive"

    invoke-virtual {v2, v3, p0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_2

    .line 872
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "download_skin, apply(), itemId/type : "

    aput-object v3, v2, v0

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    const/4 v0, 0x2

    const-string v3, "/"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v0, v1

    .line 874
    goto :goto_0
.end method

.method public static b()I
    .locals 9

    .prologue
    const/16 v8, 0x1e0

    const/16 v7, 0x140

    const/16 v6, 0x500

    const/16 v5, 0x320

    const/16 v0, 0xe0

    .line 719
    const/4 v1, 0x0

    .line 720
    invoke-static {}, Lcom/sec/common/util/i;->e()I

    move-result v2

    .line 721
    invoke-static {}, Lcom/sec/common/util/i;->f()I

    move-result v3

    .line 724
    const/16 v4, 0xf0

    if-ne v2, v4, :cond_1

    if-ne v3, v7, :cond_1

    .line 725
    const/16 v1, 0x54

    .line 741
    :cond_0
    :goto_0
    if-nez v1, :cond_7

    .line 745
    :goto_1
    return v0

    .line 726
    :cond_1
    if-ne v2, v7, :cond_2

    if-ne v3, v8, :cond_2

    .line 727
    const/16 v1, 0x70

    goto :goto_0

    .line 728
    :cond_2
    if-ne v2, v8, :cond_3

    if-ne v3, v5, :cond_3

    .line 729
    const/16 v1, 0xa8

    goto :goto_0

    .line 730
    :cond_3
    if-ne v2, v5, :cond_4

    if-ne v3, v6, :cond_4

    move v1, v0

    .line 731
    goto :goto_0

    .line 732
    :cond_4
    const/16 v4, 0x2d0

    if-ne v2, v4, :cond_5

    if-ne v3, v6, :cond_5

    move v1, v0

    .line 733
    goto :goto_0

    .line 734
    :cond_5
    if-ne v2, v6, :cond_6

    if-ne v3, v5, :cond_6

    .line 735
    const/16 v1, 0xaa

    goto :goto_0

    .line 736
    :cond_6
    const/16 v4, 0xa00

    if-ne v2, v4, :cond_0

    const/16 v2, 0x640

    if-ne v3, v2, :cond_0

    .line 737
    const/16 v1, 0x154

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Skin;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 621
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 624
    new-instance v1, Lcom/sec/chaton/io/entry/inner/Skin;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/inner/Skin;-><init>()V

    .line 625
    const-string v2, "-1"

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    .line 626
    const-string v2, "pa"

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    .line 627
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    .line 628
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    .line 629
    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "android.resource"

    aput-object v3, v2, v7

    const-string v3, "://"

    aput-object v3, v2, v8

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    const-string v3, "/drawable/default_preview_01"

    aput-object v3, v2, v10

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    .line 631
    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    .line 634
    new-instance v2, Lcom/sec/chaton/io/entry/inner/Skin;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/Skin;-><init>()V

    .line 635
    const-string v3, "-2"

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    .line 636
    const-string v3, "pa"

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    .line 637
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    .line 638
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    .line 639
    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "android.resource"

    aput-object v4, v3, v7

    const-string v4, "://"

    aput-object v4, v3, v8

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    const-string v4, "/drawable/default_preview_02"

    aput-object v4, v3, v10

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    .line 640
    const-wide v3, 0x7fffffffffffffffL

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    .line 643
    new-instance v3, Lcom/sec/chaton/io/entry/inner/Skin;

    invoke-direct {v3}, Lcom/sec/chaton/io/entry/inner/Skin;-><init>()V

    .line 644
    const-string v4, "-3"

    iput-object v4, v3, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    .line 645
    const-string v4, "pa"

    iput-object v4, v3, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    .line 646
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    .line 647
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    .line 648
    new-array v4, v6, [Ljava/lang/Object;

    const-string v5, "android.resource"

    aput-object v5, v4, v7

    const-string v5, "://"

    aput-object v5, v4, v8

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "/drawable/default_preview_03"

    aput-object v5, v4, v10

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    .line 649
    const-wide v4, 0x7fffffffffffffffL

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    .line 652
    new-instance v4, Lcom/sec/chaton/io/entry/inner/Skin;

    invoke-direct {v4}, Lcom/sec/chaton/io/entry/inner/Skin;-><init>()V

    .line 653
    const-string v5, "-4"

    iput-object v5, v4, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    .line 654
    const-string v5, "pa"

    iput-object v5, v4, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    .line 655
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    .line 656
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    .line 657
    new-array v5, v6, [Ljava/lang/Object;

    const-string v6, "android.resource"

    aput-object v6, v5, v7

    const-string v6, "://"

    aput-object v6, v5, v8

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    const-string v6, "/drawable/default_preview_04"

    aput-object v6, v5, v10

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    .line 658
    const-wide v5, 0x7fffffffffffffffL

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    .line 660
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 661
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 663
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 665
    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 245
    invoke-static {p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v0

    .line 249
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->k(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->h(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    .line 252
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 824
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "setting_change_skin"

    invoke-virtual {v1, v2, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 826
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "setting_change_bubble_send"

    invoke-virtual {v2, v3, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 827
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "setting_change_bubble_receive"

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 829
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 830
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 831
    const-string v1, "isAppliedItem(), request parameter could be (null)"

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    :cond_0
    :goto_0
    return v0

    .line 836
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    .line 837
    :cond_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 838
    const-string v1, "isAppliedItem(), saved item could be (null)"

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 843
    :cond_3
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_4

    .line 844
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isAppliedItem(), saved/new : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    :cond_4
    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 851
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 749
    const/16 v0, 0x280

    return v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 268
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->f:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    return-object v0

    .line 271
    :catch_0
    move-exception v1

    goto :goto_0

    .line 269
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static d()I
    .locals 1

    .prologue
    .line 753
    const/16 v0, 0x280

    return v0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 277
    invoke-static {p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 278
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 280
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 296
    :cond_0
    :goto_0
    return-object v0

    .line 283
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 285
    if-eqz v1, :cond_0

    .line 292
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    :try_start_1
    new-instance v1, Lcom/sec/chaton/settings/downloads/cl;

    invoke-direct {v1, v2}, Lcom/sec/chaton/settings/downloads/cl;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 298
    if-eqz v2, :cond_2

    .line 300
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_2
    :goto_1
    move-object v0, v1

    .line 294
    goto :goto_0

    .line 298
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_3

    .line 300
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 298
    :cond_3
    :goto_3
    throw v0

    .line 295
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 298
    :goto_4
    if-eqz v1, :cond_0

    .line 300
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 301
    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_1

    .line 298
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 295
    :catch_4
    move-exception v1

    move-object v1, v2

    goto :goto_4
.end method

.method public static e()I
    .locals 3

    .prologue
    .line 757
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_skin_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 332
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->i:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :goto_0
    return-object v0

    .line 335
    :catch_0
    move-exception v1

    goto :goto_0

    .line 333
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/ck;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 370
    new-instance v1, Lcom/sec/chaton/settings/downloads/ck;

    invoke-direct {v1}, Lcom/sec/chaton/settings/downloads/ck;-><init>()V

    .line 372
    invoke-static {p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 373
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 375
    const-string v2, "pa"

    iput-object v2, v1, Lcom/sec/chaton/settings/downloads/ck;->a:Ljava/lang/String;

    .line 376
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/chaton/settings/downloads/ck;->b:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 414
    :cond_0
    :goto_0
    return-object v0

    .line 381
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 383
    if-eqz v3, :cond_0

    .line 388
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/util/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 391
    :try_start_0
    const-string v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 393
    const/4 v4, 0x3

    aget-object v2, v2, v4

    const-string v4, "ma"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 394
    const-string v2, "ma"

    iput-object v2, v1, Lcom/sec/chaton/settings/downloads/ck;->a:Ljava/lang/String;

    .line 399
    :goto_1
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    :try_start_1
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/chaton/settings/downloads/ck;->b:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 405
    if-eqz v2, :cond_2

    .line 407
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_2
    :goto_2
    move-object v0, v1

    .line 414
    goto :goto_0

    .line 396
    :cond_3
    :try_start_3
    const-string v2, "pa"

    iput-object v2, v1, Lcom/sec/chaton/settings/downloads/ck;->a:Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 402
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 405
    :goto_3
    if-eqz v1, :cond_0

    .line 407
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 408
    :catch_1
    move-exception v1

    goto :goto_0

    .line 405
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    if-eqz v2, :cond_4

    .line 407
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 405
    :cond_4
    :goto_5
    throw v0

    .line 408
    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_2

    .line 405
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 402
    :catch_4
    move-exception v1

    move-object v1, v2

    goto :goto_3
.end method

.method public static f()V
    .locals 2

    .prologue
    .line 882
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 883
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_skin_type"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 884
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_send"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 885
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_change_bubble_receive"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 886
    return-void
.end method

.method public static g(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 491
    invoke-static {p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 494
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 497
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->k(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/io/File;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 502
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->j:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 506
    :goto_0
    return-object v0

    .line 505
    :catch_0
    move-exception v1

    goto :goto_0

    .line 503
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 511
    invoke-static {p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    sget-object v0, Lcom/sec/chaton/settings/downloads/cd;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 514
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 517
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->h(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/io/File;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static j(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 596
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 597
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 598
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_skin, uninstallSkin, id/uninstall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :cond_0
    invoke-static {v0}, Lcom/sec/common/util/l;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 605
    :try_start_1
    const-string v0, "com.sec.chaton.provider2"

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v1, p1}, Lcom/sec/chaton/e/a/j;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 618
    return-void

    .line 606
    :catch_0
    move-exception v0

    .line 607
    :try_start_2
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 611
    :catch_1
    move-exception v0

    .line 612
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_1

    .line 613
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 616
    :cond_1
    throw v0

    .line 608
    :catch_2
    move-exception v0

    .line 609
    :try_start_3
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
.end method

.method private static k(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 482
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/cd;->k:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 486
    :goto_0
    return-object v0

    .line 485
    :catch_0
    move-exception v1

    goto :goto_0

    .line 483
    :catch_1
    move-exception v1

    goto :goto_0
.end method
