.class Lcom/sec/chaton/settings/dh;
.super Lcom/sec/chaton/util/ar;
.source "DeregisterFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/DeregisterFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/DeregisterFragment;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/chaton/settings/dh;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-direct {p0}, Lcom/sec/chaton/util/ar;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/chaton/settings/dh;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 148
    const-string v0, "Push deregistration is success. execute chaton deregistration."

    invoke-static {}, Lcom/sec/chaton/settings/DeregisterFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/settings/dh;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->e(Lcom/sec/chaton/settings/DeregisterFragment;)V

    .line 156
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01e2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 150
    :cond_1
    const-string v0, "Push deregistration is fail."

    invoke-static {}, Lcom/sec/chaton/settings/DeregisterFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/dh;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    iget-object v1, p0, Lcom/sec/chaton/settings/dh;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/DeregisterFragment;->f(Lcom/sec/chaton/settings/DeregisterFragment;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/ap;->a()Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/DeregisterFragment;->a(Lcom/sec/chaton/settings/DeregisterFragment;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;

    goto :goto_0
.end method
