.class Lcom/sec/chaton/settings/di;
.super Landroid/os/Handler;
.source "DeregisterFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/DeregisterFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/DeregisterFragment;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 201
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 178
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 180
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/DeregisterFragment;->e(Lcom/sec/chaton/settings/DeregisterFragment;)V

    .line 181
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->g(Lcom/sec/chaton/settings/DeregisterFragment;)Lcom/sec/chaton/d/a/ak;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->g(Lcom/sec/chaton/settings/DeregisterFragment;)Lcom/sec/chaton/d/a/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/ak;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    const-string v0, "ChatON deregistration is success."

    invoke-static {}, Lcom/sec/chaton/settings/DeregisterFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 187
    const-string v1, "com.sec.chaton.action.USER_DEREGISTRATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/DeregisterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b029c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/global/GlobalApplication;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/di;->a:Lcom/sec/chaton/settings/DeregisterFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/DeregisterFragment;->h(Lcom/sec/chaton/settings/DeregisterFragment;)V

    .line 196
    const-string v0, "ChatON deregistration is fail."

    invoke-static {}, Lcom/sec/chaton/settings/DeregisterFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_0
    .end packed-switch
.end method
