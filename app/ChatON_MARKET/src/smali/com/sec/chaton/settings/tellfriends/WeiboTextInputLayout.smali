.class public Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;
.super Ljava/lang/Object;
.source "WeiboTextInputLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/text/TextWatcher;

.field private b:Landroid/content/Context;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/EditText;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Landroid/widget/Toast;

.field private h:Lcom/sec/chaton/settings/tellfriends/cf;

.field private i:Lcom/sec/chaton/settings/tellfriends/cg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/EditText;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->c:Landroid/widget/TextView;

    .line 42
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->d:Landroid/widget/EditText;

    .line 43
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->e:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->f:I

    .line 45
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->g:Landroid/widget/Toast;

    .line 53
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->h:Lcom/sec/chaton/settings/tellfriends/cf;

    .line 54
    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->i:Lcom/sec/chaton/settings/tellfriends/cg;

    .line 59
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ce;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/ce;-><init>(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->a:Landroid/text/TextWatcher;

    .line 83
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->b:Landroid/content/Context;

    .line 84
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->d:Landroid/widget/EditText;

    .line 85
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->a(Landroid/content/Context;I)V

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->g:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)Lcom/sec/chaton/settings/tellfriends/cf;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->h:Lcom/sec/chaton/settings/tellfriends/cf;

    return-object v0
.end method

.method private a(Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->d:Landroid/widget/EditText;

    const v1, 0x10000006

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 92
    check-cast p1, Landroid/app/Activity;

    const v0, 0x7f07023e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->c:Landroid/widget/TextView;

    .line 93
    if-lez p2, :cond_0

    .line 94
    iput p2, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->f:I

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->a:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->c:Landroid/widget/TextView;

    const-string v1, "(%d/%d)"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->d:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 101
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/cg;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->e:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/tellfriends/cg;-><init>(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->i:Lcom/sec/chaton/settings/tellfriends/cg;

    .line 102
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/cf;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->f:I

    const-string v2, "KSC5601"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/cf;-><init>(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->h:Lcom/sec/chaton/settings/tellfriends/cf;

    .line 103
    new-array v0, v6, [Landroid/text/InputFilter;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->i:Lcom/sec/chaton/settings/tellfriends/cg;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->h:Lcom/sec/chaton/settings/tellfriends/cf;

    aput-object v1, v0, v5

    .line 104
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 105
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->f:I

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->g:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method
