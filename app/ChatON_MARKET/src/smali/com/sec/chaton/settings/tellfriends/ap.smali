.class public Lcom/sec/chaton/settings/tellfriends/ap;
.super Lcom/sec/chaton/settings/tellfriends/aa;
.source "SnsHelperWeibo.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/settings/tellfriends/bt;

.field private c:Lcom/sec/chaton/settings/tellfriends/ag;

.field private d:Lcom/sec/chaton/settings/tellfriends/af;

.field private e:Lcom/sec/chaton/settings/tellfriends/ad;

.field private f:Lcom/sec/chaton/settings/tellfriends/ah;

.field private g:Lcom/sec/chaton/settings/tellfriends/ac;

.field private h:Lcom/sec/chaton/settings/tellfriends/ae;

.field private i:Lcom/sec/chaton/settings/tellfriends/common/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/ap;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/aa;-><init>()V

    .line 35
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/aq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/aq;-><init>(Lcom/sec/chaton/settings/tellfriends/ap;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->i:Lcom/sec/chaton/settings/tellfriends/common/c;

    .line 28
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ap;->i:Lcom/sec/chaton/settings/tellfriends/common/c;

    invoke-direct {v0, p1, v1}, Lcom/sec/chaton/settings/tellfriends/bt;-><init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ad;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->e:Lcom/sec/chaton/settings/tellfriends/ad;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ac;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->g:Lcom/sec/chaton/settings/tellfriends/ac;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ae;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->h:Lcom/sec/chaton/settings/tellfriends/ae;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ah;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->f:Lcom/sec/chaton/settings/tellfriends/ah;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ag;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->c:Lcom/sec/chaton/settings/tellfriends/ag;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/ap;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/af;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->d:Lcom/sec/chaton/settings/tellfriends/af;

    return-object v0
.end method


# virtual methods
.method public a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bt;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bt;->e()V

    .line 33
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ac;)V
    .locals 2

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ap;->g:Lcom/sec/chaton/settings/tellfriends/ac;

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    const/16 v1, 0x2ee6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bt;->c(I)V

    .line 171
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ad;)V
    .locals 2

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ap;->e:Lcom/sec/chaton/settings/tellfriends/ad;

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    const/16 v1, 0x2ee1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bt;->b(I)V

    .line 165
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/af;)V
    .locals 2

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ap;->d:Lcom/sec/chaton/settings/tellfriends/af;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    const/16 v1, 0x2ee4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bt;->a(I)V

    .line 201
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V
    .locals 3

    .prologue
    .line 180
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/ap;->f:Lcom/sec/chaton/settings/tellfriends/ah;

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    const/16 v1, 0x2ee2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/chaton/settings/tellfriends/bt;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bt;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bt;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ap;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bt;->a()Z

    move-result v0

    return v0
.end method
