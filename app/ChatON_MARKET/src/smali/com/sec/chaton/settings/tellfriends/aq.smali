.class Lcom/sec/chaton/settings/tellfriends/aq;
.super Ljava/lang/Object;
.source "SnsHelperWeibo.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/c;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/ap;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/ap;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v0, -0x1

    .line 39
    packed-switch p1, :pswitch_data_0

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 41
    :pswitch_0
    if-ne p2, v0, :cond_3

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    instance-of v0, p3, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 44
    check-cast p3, Ljava/util/ArrayList;

    .line 45
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 47
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lweibo4android/User;

    .line 49
    new-instance v3, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lweibo4android/User;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lweibo4android/User;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lweibo4android/User;->getScreenName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lweibo4android/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, Lweibo4android/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/chaton/settings/tellfriends/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const-string v0, ""

    goto :goto_2

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->a(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ad;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 56
    :cond_3
    if-nez p2, :cond_4

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->a(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ad;->a()V

    goto/16 :goto_0

    .line 60
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->a(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ad;->a(I)V

    goto/16 :goto_0

    .line 65
    :pswitch_1
    if-ne p2, v0, :cond_5

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS_ID \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->b(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ac;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/sec/chaton/settings/tellfriends/ac;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 69
    :cond_5
    if-nez p2, :cond_6

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS_ID \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->b(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ac;->a()V

    goto/16 :goto_0

    .line 73
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS_ID \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->b(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ac;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ac;->a(I)V

    goto/16 :goto_0

    .line 78
    :pswitch_2
    if-ne p2, v0, :cond_9

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_USER_INFO \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    instance-of v0, p3, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 81
    check-cast p3, Ljava/util/ArrayList;

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 84
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lweibo4android/User;

    .line 85
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "REQUEST_GET_USER_INFO \tLong id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lweibo4android/User;->getId()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " int id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lweibo4android/User;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    new-instance v3, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-virtual {v0}, Lweibo4android/User;->getId()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lweibo4android/User;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lweibo4android/User;->getScreenName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lweibo4android/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-virtual {v0}, Lweibo4android/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/chaton/settings/tellfriends/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    const-string v0, ""

    goto :goto_4

    .line 91
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->c(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ae;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ae;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 94
    :cond_9
    if-nez p2, :cond_a

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_USER_INFO \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->c(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ae;->a()V

    goto/16 :goto_0

    .line 98
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_USER_INFO \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->c(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ae;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ae;->a(I)V

    goto/16 :goto_0

    .line 103
    :pswitch_3
    if-ne p2, v0, :cond_b

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->d(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ah;->a()V

    goto/16 :goto_0

    .line 106
    :cond_b
    if-nez p2, :cond_c

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->d(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ah;->b()V

    goto/16 :goto_0

    .line 110
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->d(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ah;->a(I)V

    goto/16 :goto_0

    .line 115
    :pswitch_4
    if-ne p2, v0, :cond_d

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->e(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onComplete()V

    goto/16 :goto_0

    .line 118
    :cond_d
    if-nez p2, :cond_e

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->e(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onCancelled()V

    goto/16 :goto_0

    .line 122
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->e(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onError()V

    goto/16 :goto_0

    .line 127
    :pswitch_5
    if-ne p2, v0, :cond_f

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->f(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onComplete()V

    goto/16 :goto_0

    .line 130
    :cond_f
    if-nez p2, :cond_10

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->f(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onCancelled()V

    goto/16 :goto_0

    .line 134
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->f(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/af;->onError(I)V

    goto/16 :goto_0

    .line 139
    :pswitch_6
    if-ne p2, v0, :cond_11

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_PREPARE_LOGIN \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/ap;->f(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ap;->a(Lcom/sec/chaton/settings/tellfriends/af;)V

    goto/16 :goto_0

    .line 142
    :cond_11
    if-nez p2, :cond_12

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_PREPARE_LOGIN \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->f(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onCancelled()V

    goto/16 :goto_0

    .line 146
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_PREPARE_LOGIN \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ap;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aq;->a:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->f(Lcom/sec/chaton/settings/tellfriends/ap;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/af;->onError(I)V

    goto/16 :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x2ee1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
