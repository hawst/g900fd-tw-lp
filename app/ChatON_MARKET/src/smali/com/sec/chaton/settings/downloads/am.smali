.class Lcom/sec/chaton/settings/downloads/am;
.super Landroid/os/Handler;
.source "AniconPackageDownloads.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 140
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3ea

    if-ne v0, v1, :cond_0

    .line 148
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 150
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-static {v1, v4}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;Lcom/sec/chaton/d/a/cd;)Lcom/sec/chaton/d/a/cd;

    .line 152
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->c(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    .line 154
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->e(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Lcom/sec/chaton/settings/downloads/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b(Z)V

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a(Z)V

    goto :goto_0

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00b3

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/av;->a(I)I

    move-result v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/am;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
