.class public Lcom/sec/chaton/settings/downloads/dl;
.super Landroid/support/v4/widget/CursorAdapter;
.source "SoundListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/settings/downloads/a/q;


# static fields
.field static final a:Ljava/lang/String;

.field private static final d:Ljava/lang/String;


# instance fields
.field b:Lcom/sec/chaton/settings/downloads/aw;

.field c:Lcom/sec/chaton/settings/downloads/bt;

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/dp;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/sec/chaton/d/bc;

.field private g:Lcom/sec/chaton/settings/downloads/do;

.field private h:Landroid/os/Handler;

.field private i:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    .line 55
    const-class v0, Lcom/sec/chaton/settings/downloads/dl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/dl;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 2

    .prologue
    .line 261
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 65
    new-instance v0, Lcom/sec/chaton/settings/downloads/dm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/dm;-><init>(Lcom/sec/chaton/settings/downloads/dl;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->h:Landroid/os/Handler;

    .line 144
    new-instance v0, Lcom/sec/chaton/settings/downloads/dn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/dn;-><init>(Lcom/sec/chaton/settings/downloads/dl;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->i:Landroid/os/Handler;

    .line 263
    sget-object v0, Lcom/sec/chaton/settings/downloads/dl;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->h:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/bt;->a(Ljava/lang/String;Landroid/os/Handler;)Lcom/sec/chaton/settings/downloads/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->c:Lcom/sec/chaton/settings/downloads/bt;

    .line 264
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->e:Ljava/util/HashMap;

    .line 266
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/chaton/d/bc;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->f:Lcom/sec/chaton/d/bc;

    .line 267
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/dl;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->c:Lcom/sec/chaton/settings/downloads/bt;

    if-nez v0, :cond_0

    .line 687
    :goto_0
    return-void

    .line 685
    :cond_0
    new-instance v0, Lcom/sec/chaton/multimedia/audio/d;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/chaton/multimedia/audio/d;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 686
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->c:Lcom/sec/chaton/settings/downloads/bt;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/bt;->b(Lcom/sec/chaton/multimedia/audio/d;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 499
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/v;

    .line 501
    if-nez v0, :cond_1

    .line 502
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->f:Lcom/sec/chaton/d/bc;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/v;-><init>(Lcom/sec/chaton/d/bc;Ljava/lang/String;)V

    .line 503
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 505
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->f:Lcom/sec/chaton/d/bc;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/v;-><init>(Lcom/sec/chaton/d/bc;Ljava/lang/String;)V

    .line 506
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_0

    .line 507
    :cond_2
    if-eqz p2, :cond_0

    .line 508
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 512
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 513
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_sound, download all - requestInstall : cancel(), id/status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->a(Z)Z

    goto :goto_0

    .line 508
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->c:Lcom/sec/chaton/settings/downloads/bt;

    if-nez v0, :cond_0

    .line 696
    :goto_0
    return-void

    .line 694
    :cond_0
    new-instance v0, Lcom/sec/chaton/multimedia/audio/d;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/sec/chaton/multimedia/audio/d;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Z)V

    .line 695
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->c:Lcom/sec/chaton/settings/downloads/bt;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/bt;->b(Lcom/sec/chaton/multimedia/audio/d;)Z

    goto :goto_0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 457
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 458
    const-string v1, "item_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 448
    if-nez p1, :cond_0

    .line 449
    const/4 v0, 0x0

    .line 452
    :goto_0
    return-object v0

    .line 451
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/dp;

    .line 452
    iget v0, v0, Lcom/sec/chaton/settings/downloads/dp;->a:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/dl;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/dp;

    .line 481
    if-nez v0, :cond_0

    .line 495
    :goto_0
    return-object v1

    .line 485
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 486
    iget v0, v0, Lcom/sec/chaton/settings/downloads/dp;->a:I

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 489
    :try_start_0
    const-string v0, "extras"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 490
    invoke-static {v0}, Lcom/sec/chaton/e/a/ad;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ae;

    move-result-object v0

    .line 491
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/ae;->b()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 495
    goto :goto_0

    .line 492
    :catch_0
    move-exception v0

    .line 493
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v0, v1

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 701
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->notifyDataSetChanged()V

    .line 702
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 708
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/aw;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    .line 230
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/do;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/dl;->g:Lcom/sec/chaton/settings/downloads/do;

    .line 226
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/multimedia/audio/f;)Z
    .locals 1

    .prologue
    .line 187
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 188
    :cond_0
    const/4 v0, 0x0

    .line 193
    :goto_0
    return v0

    .line 192
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->notifyDataSetChanged()V

    .line 193
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 245
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/e/ar;)V

    .line 246
    return-void
.end method

.method public b(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 713
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 714
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 721
    :cond_0
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 625
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 626
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 627
    const-string v0, "download_sound, requestPlaySample(), itemid is (empty)"

    sget-object v1, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 633
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 636
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 638
    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/net/URI;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    .line 654
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 655
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 656
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "download_sound, requestPlaySample(), play installed file : "

    aput-object v2, v1, v4

    aput-object p1, v1, v3

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/downloads/dl;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 639
    :catch_0
    move-exception v0

    .line 640
    :try_start_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 641
    sget-object v1, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 643
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->f()V
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 646
    :catch_1
    move-exception v0

    .line 647
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_4

    .line 648
    sget-object v1, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 650
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->f()V

    goto :goto_0

    .line 662
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 663
    new-array v0, v2, [Ljava/lang/Object;

    const-string v1, "download_sound, requestPlaySample(), play download file : "

    aput-object v1, v0, v4

    aput-object p1, v0, v3

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_6
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/dl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 666
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v3, :cond_8

    .line 667
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 668
    const-string v0, "download_sound, requestPlaySample(), sample url is (empty)"

    sget-object v1, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->f()V

    goto/16 :goto_0

    .line 673
    :cond_8
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/downloads/dl;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 18

    .prologue
    .line 299
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/dp;

    .line 300
    const-string v2, "extras"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 301
    const/4 v2, 0x0

    .line 305
    :try_start_0
    invoke-static {v3}, Lcom/sec/chaton/e/a/ad;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ae;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 312
    :cond_0
    :goto_0
    const-string v3, "item_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 313
    const-string v3, "name"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 314
    const-wide/16 v3, 0x0

    .line 315
    if-eqz v2, :cond_1

    .line 316
    invoke-virtual {v2}, Lcom/sec/chaton/e/a/ae;->a()J

    move-result-wide v2

    move-wide v3, v2

    .line 318
    :cond_1
    const-string v2, "install"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x0

    move v5, v2

    .line 319
    :goto_1
    const-string v2, "new"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 320
    const-string v2, "special"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 322
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_2

    .line 323
    const-string v2, "down_rank"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 324
    const-string v2, "data1"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 325
    const-string v12, "data2"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 326
    const-string v13, "data3"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 327
    const-string v14, "download_sound, bindView() : [id:%s] [install:%s] [special:%d] [new:%d] [downrank:%03d] [group:%s-%s-%s] [volume:%d] [piece:%s] [title:%s] "

    const/16 v15, 0xb

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v15, v16

    const/4 v10, 0x5

    aput-object v2, v15, v10

    const/4 v2, 0x6

    aput-object v12, v15, v2

    const/4 v2, 0x7

    aput-object v13, v15, v2

    const/16 v2, 0x8

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v15, v2

    const/16 v2, 0x9

    const-string v10, "(n/a)"

    aput-object v10, v15, v2

    const/16 v2, 0xa

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v15, v2

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 332
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v10, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v2, v10}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/dl;->e:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/dl;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v2

    sget-object v10, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2, v10, v7}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/settings/downloads/a/v;

    .line 342
    if-eqz v2, :cond_3

    .line 343
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/settings/downloads/dl;->i:Landroid/os/Handler;

    invoke-virtual {v2, v10}, Lcom/sec/chaton/settings/downloads/a/v;->a(Landroid/os/Handler;)V

    .line 346
    :cond_3
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    iput v10, v1, Lcom/sec/chaton/settings/downloads/dp;->a:I

    .line 347
    iput-object v7, v1, Lcom/sec/chaton/settings/downloads/dp;->b:Ljava/lang/String;

    .line 348
    iget-object v10, v1, Lcom/sec/chaton/settings/downloads/dp;->d:Landroid/widget/TextView;

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    iget-object v6, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 350
    iget-object v6, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 353
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/settings/downloads/dl;->c:Lcom/sec/chaton/settings/downloads/bt;

    invoke-virtual {v6, v7}, Lcom/sec/chaton/settings/downloads/bt;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v6

    .line 354
    if-nez v6, :cond_4

    .line 355
    sget-object v6, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    .line 357
    :cond_4
    iget-object v10, v1, Lcom/sec/chaton/settings/downloads/dp;->c:Landroid/widget/ImageView;

    invoke-static {v10, v6}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/ImageView;Lcom/sec/chaton/multimedia/audio/f;)Z

    .line 362
    if-eqz v5, :cond_7

    .line 365
    invoke-static {v7}, Lcom/sec/chaton/settings/downloads/dg;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 366
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const v5, 0x7f0201ce

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 367
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 374
    :goto_2
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, " ("

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-wide/16 v10, 0x3e8

    div-long/2addr v3, v10

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x2

    const-string v4, "KB) / "

    aput-object v4, v5, v3

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/dl;->mContext:Landroid/content/Context;

    const v6, 0x7f0b01d7

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v3

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/dg;->b(Landroid/widget/TextView;)V

    .line 429
    :goto_3
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 430
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 432
    if-eqz v9, :cond_c

    .line 433
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 435
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    const v3, 0x7f0b01dd

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 436
    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    const-string v2, "#3eb1b9"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 445
    :goto_4
    return-void

    .line 306
    :catch_0
    move-exception v3

    .line 307
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_0

    .line 308
    sget-object v4, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    :cond_5
    const/4 v2, 0x1

    move v5, v2

    goto/16 :goto_1

    .line 370
    :cond_6
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const v5, 0x7f0201cd

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 371
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_2

    .line 377
    :cond_7
    if-nez v2, :cond_8

    .line 378
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const v5, 0x7f0201d6

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 379
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 380
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 381
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, " ("

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-wide/16 v10, 0x3e8

    div-long/2addr v3, v10

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x2

    const-string v4, "KB)"

    aput-object v4, v5, v3

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 383
    :cond_8
    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_9

    .line 384
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const v5, 0x7f0201d9

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 385
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 386
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, " ("

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-wide/16 v10, 0x3e8

    div-long/2addr v3, v10

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x2

    const-string v4, "KB)"

    aput-object v4, v5, v3

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 390
    :cond_9
    iget-object v5, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 392
    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto/16 :goto_3

    .line 394
    :pswitch_0
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const v3, 0x7f0201d2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 397
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v2

    if-nez v2, :cond_a

    .line 398
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 401
    :cond_a
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, " ("

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/settings/downloads/dl;->mContext:Landroid/content/Context;

    const v6, 0x7f0b022a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, ")"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 406
    :pswitch_1
    iget-object v5, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const v6, 0x7f0201d2

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 409
    iget-object v5, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 410
    iget-object v5, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 412
    :cond_b
    iget-object v5, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/v;->g()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 414
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, " ("

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-wide/16 v10, 0x3e8

    div-long/2addr v3, v10

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x2

    const-string v4, "KB)"

    aput-object v4, v5, v3

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 419
    :pswitch_2
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 420
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 422
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/settings/downloads/dl;->mContext:Landroid/content/Context;

    const v4, 0x7f0b022b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/dg;->d(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 437
    :cond_c
    if-eqz v8, :cond_d

    .line 438
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 440
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    const v3, 0x7f0b01dc

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 441
    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    const-string v2, "#e86d00"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto/16 :goto_4

    .line 443
    :cond_d
    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 249
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->c(Lcom/sec/chaton/e/ar;)V

    .line 250
    return-void
.end method

.method public c(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 726
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->notifyDataSetChanged()V

    .line 727
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 728
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 736
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 743
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 728
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method d()V
    .locals 2

    .prologue
    .line 270
    sget-object v0, Lcom/sec/chaton/settings/downloads/dl;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->h:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/bt;->b(Ljava/lang/String;Landroid/os/Handler;)V

    .line 271
    return-void
.end method

.method public e()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 529
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 530
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 531
    const/4 v0, -0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 532
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 533
    const-string v0, "item_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 534
    const-string v0, "name"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 535
    const-string v0, "install"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 536
    :goto_1
    sget-boolean v6, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v6, :cond_1

    .line 537
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "download_sound, download all, add into list,  id/name/install : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    if-nez v0, :cond_0

    .line 542
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 535
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 546
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 547
    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/dl;->a(Ljava/lang/String;Z)V

    goto :goto_2

    .line 549
    :cond_4
    return-void
.end method

.method f()V
    .locals 3

    .prologue
    .line 678
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0229

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 679
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 275
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 276
    const v1, 0x7f030049

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 279
    new-instance v2, Lcom/sec/chaton/settings/downloads/dp;

    invoke-direct {v2}, Lcom/sec/chaton/settings/downloads/dp;-><init>()V

    .line 280
    const v0, 0x7f0701d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->c:Landroid/widget/ImageView;

    .line 281
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    const v0, 0x7f0701d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->d:Landroid/widget/TextView;

    .line 284
    const v0, 0x7f0701db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->e:Landroid/widget/TextView;

    .line 285
    const v0, 0x7f0701d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    .line 286
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    const v0, 0x7f0701d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->g:Landroid/widget/TextView;

    .line 290
    const v0, 0x7f0701da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/dp;->h:Landroid/widget/ProgressBar;

    .line 292
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 293
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 553
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 595
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 556
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/dp;

    .line 561
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->g:Lcom/sec/chaton/settings/downloads/do;

    if-eqz v1, :cond_0

    .line 565
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/dp;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/dl;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 570
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/dp;

    .line 571
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 572
    iget v0, v0, Lcom/sec/chaton/settings/downloads/dp;->a:I

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 573
    const-string v0, "item_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 574
    const-string v0, "name"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 575
    const-string v0, "install"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 577
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 581
    if-eqz v0, :cond_3

    .line 582
    invoke-static {v3, v4}, Lcom/sec/chaton/settings/downloads/dg;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 583
    if-eqz v0, :cond_2

    .line 584
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->g(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 585
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/dl;->notifyDataSetChanged()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 575
    goto :goto_1

    .line 587
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->h(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 590
    :cond_3
    invoke-direct {p0, v3, v1}, Lcom/sec/chaton/settings/downloads/dl;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 553
    :pswitch_data_0
    .packed-switch 0x7f0701d6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 97
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 98
    const-string v0, "swapCursor(), newCursor : "

    sget-object v1, Lcom/sec/chaton/settings/downloads/dl;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    if-nez p1, :cond_2

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->a()V

    .line 135
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 107
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 108
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 109
    const-string v0, "item_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 110
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 112
    :goto_2
    if-eqz v0, :cond_4

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 110
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .line 118
    :cond_4
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/v;

    .line 119
    if-nez v0, :cond_5

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 125
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Lcom/sec/chaton/settings/downloads/a/l;)Z

    goto :goto_1

    .line 132
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/dl;->b:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->b()V

    goto :goto_0
.end method
