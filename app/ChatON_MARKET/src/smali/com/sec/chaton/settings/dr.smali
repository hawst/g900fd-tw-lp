.class public Lcom/sec/chaton/settings/dr;
.super Landroid/widget/BaseAdapter;
.source "FragmentMultiDeviceView.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:[Landroid/widget/Button;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)V
    .locals 1

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 246
    invoke-static {p1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->f(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/dr;->b:Landroid/view/LayoutInflater;

    .line 248
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 262
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f0b03df

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 269
    .line 273
    if-nez p2, :cond_0

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030123

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 277
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/dr;->getCount()I

    move-result v0

    new-array v0, v0, [Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings/dr;->e:[Landroid/widget/Button;

    .line 279
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/dr;->c:Landroid/widget/TextView;

    .line 280
    const v0, 0x7f07014d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/settings/dr;->e:[Landroid/widget/Button;

    const v0, 0x7f0702d7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, p1

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->e:[Landroid/widget/Button;

    aget-object v0, v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->e:[Landroid/widget/Button;

    aget-object v0, v0, p1

    const v1, 0x7f0b0304

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->model:Ljava/lang/String;

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->isAsker:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->isAsker:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v1, v0

    .line 294
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->phoneNumber:Ljava/lang/String;

    .line 297
    iget-object v4, p0, Lcom/sec/chaton/settings/dr;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    if-eqz v3, :cond_4

    const-string v4, "web"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->e:[Landroid/widget/Button;

    aget-object v0, v0, p1

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 327
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->c:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->chatonid:Ljava/lang/String;

    .line 334
    iget-object v1, p0, Lcom/sec/chaton/settings/dr;->e:[Landroid/widget/Button;

    aget-object v1, v1, p1

    new-instance v2, Lcom/sec/chaton/settings/ds;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/settings/ds;-><init>(Lcom/sec/chaton/settings/dr;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    :cond_3
    return-object p2

    .line 308
    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 309
    if-ne v1, v5, :cond_5

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 311
    iget-object v1, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 313
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 316
    :cond_6
    if-ne v1, v5, :cond_1

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/settings/dr;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings/dr;->a:Lcom/sec/chaton/settings/FragmentMultiDeviceView;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    move v1, v2

    goto/16 :goto_0
.end method
