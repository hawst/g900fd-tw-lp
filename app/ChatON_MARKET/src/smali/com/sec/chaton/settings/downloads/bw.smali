.class public Lcom/sec/chaton/settings/downloads/bw;
.super Landroid/support/v4/widget/CursorAdapter;
.source "SkinAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/settings/downloads/a/q;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/settings/downloads/aw;

.field private c:Lcom/sec/common/f/c;

.field private d:Lcom/sec/chaton/c/h;

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/bz;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/sec/chaton/settings/downloads/by;

.field private g:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/sec/chaton/settings/downloads/bw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/bw;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ZLcom/sec/common/f/c;)V
    .locals 3

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 64
    new-instance v0, Lcom/sec/chaton/settings/downloads/bx;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/bx;-><init>(Lcom/sec/chaton/settings/downloads/bw;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->g:Landroid/os/Handler;

    .line 117
    iput-object p4, p0, Lcom/sec/chaton/settings/downloads/bw;->c:Lcom/sec/common/f/c;

    .line 118
    new-instance v0, Lcom/sec/chaton/c/h;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/c/h;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->d:Lcom/sec/chaton/c/h;

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->e:Ljava/util/HashMap;

    .line 120
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/bw;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 298
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/r;

    .line 300
    if-nez v0, :cond_1

    .line 301
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->d:Lcom/sec/chaton/c/h;

    invoke-direct {v0, v1, p1, p2}, Lcom/sec/chaton/settings/downloads/a/r;-><init>(Lcom/sec/chaton/c/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 304
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->d:Lcom/sec/chaton/c/h;

    invoke-direct {v0, v1, p1, p2}, Lcom/sec/chaton/settings/downloads/a/r;-><init>(Lcom/sec/chaton/c/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_0

    .line 306
    :cond_2
    if-eqz p3, :cond_0

    .line 307
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 311
    :pswitch_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->a(Z)Z

    goto :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/chaton/settings/downloads/bw;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    if-eqz p1, :cond_0

    .line 153
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/bz;

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bw;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 155
    iget v0, v0, Lcom/sec/chaton/settings/downloads/bz;->a:I

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 156
    const-string v0, "item_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    :cond_0
    return-object v0
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bw;->notifyDataSetChanged()V

    .line 470
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 476
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/aw;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    .line 464
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/by;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/bw;->f:Lcom/sec/chaton/settings/downloads/by;

    .line 148
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 128
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 135
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/e/ar;)V

    .line 136
    return-void
.end method

.method public b(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 481
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 489
    :cond_0
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 184
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/bz;

    .line 186
    const-string v1, "item_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 187
    const-string v1, "install"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_5

    move v4, v3

    .line 188
    :goto_0
    const-string v1, "new"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 189
    const-string v1, "special"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 192
    const-string v1, "extras"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/ab;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ac;

    move-result-object v8

    .line 195
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iput v1, v0, Lcom/sec/chaton/settings/downloads/bz;->a:I

    .line 198
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 199
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->e:Ljava/util/HashMap;

    invoke-virtual {v1, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v1

    sget-object v9, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v1, v9, v5}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/a/r;

    .line 204
    if-eqz v1, :cond_0

    .line 205
    iget-object v9, p0, Lcom/sec/chaton/settings/downloads/bw;->g:Landroid/os/Handler;

    invoke-virtual {v1, v9}, Lcom/sec/chaton/settings/downloads/a/r;->a(Landroid/os/Handler;)V

    .line 209
    :cond_0
    new-instance v9, Lcom/sec/chaton/settings/downloads/ct;

    invoke-virtual {v8}, Lcom/sec/chaton/e/a/ac;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Lcom/sec/chaton/settings/downloads/ct;-><init>(Ljava/lang/String;)V

    .line 210
    const v8, 0x7f0201ca

    invoke-virtual {v9, v8}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a(I)V

    .line 211
    iget-object v8, p0, Lcom/sec/chaton/settings/downloads/bw;->c:Lcom/sec/common/f/c;

    iget-object v10, v0, Lcom/sec/chaton/settings/downloads/bz;->c:Landroid/widget/ImageView;

    invoke-virtual {v8, v10, v9}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 213
    iget-object v8, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 214
    iget-object v8, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 215
    iget-object v8, v0, Lcom/sec/chaton/settings/downloads/bz;->f:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    if-eqz v4, :cond_8

    .line 221
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const v4, 0x7f0200ab

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 222
    invoke-static {v5}, Lcom/sec/chaton/settings/downloads/cd;->b(Ljava/lang/String;)Z

    move-result v1

    .line 223
    if-eqz v1, :cond_6

    .line 224
    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const v5, 0x7f0201cb

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 225
    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 231
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 232
    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    if-nez v1, :cond_7

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 234
    :cond_1
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 275
    :cond_2
    :goto_3
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 276
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 277
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 279
    :cond_3
    if-eqz v7, :cond_c

    .line 280
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->b:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 282
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/bz;->b:Landroid/widget/ImageView;

    const v1, 0x7f0201e0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 295
    :cond_4
    :goto_4
    return-void

    :cond_5
    move v4, v2

    .line 187
    goto/16 :goto_0

    .line 228
    :cond_6
    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setEnabled(Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 290
    :catch_0
    move-exception v0

    .line 291
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_4

    .line 292
    sget-object v1, Lcom/sec/chaton/settings/downloads/bw;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    move v1, v3

    .line 232
    goto :goto_2

    .line 236
    :cond_8
    if-nez v1, :cond_9

    .line 237
    :try_start_1
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const v2, 0x7f0200ac

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 238
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_3

    .line 239
    :cond_9
    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_a

    .line 240
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const v2, 0x7f0200ad

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 241
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_3

    .line 244
    :cond_a
    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_3

    .line 246
    :pswitch_0
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const v2, 0x7f0200ae

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 247
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 250
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v1

    if-nez v1, :cond_2

    .line 251
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto/16 :goto_3

    .line 256
    :pswitch_1
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 257
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 260
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 261
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 263
    :cond_b
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/a/r;->g()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_3

    .line 267
    :pswitch_2
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 268
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 269
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->f:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 283
    :cond_c
    if-eqz v6, :cond_d

    .line 284
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bz;->b:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 286
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/bz;->b:Landroid/widget/ImageView;

    const v1, 0x7f0201da

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 288
    :cond_d
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/bz;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    .line 244
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 139
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->c(Lcom/sec/chaton/e/ar;)V

    .line 140
    return-void
.end method

.method public c(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bw;->notifyDataSetChanged()V

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 496
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 513
    :cond_0
    :goto_0
    return-void

    .line 499
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 506
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 496
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 324
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 325
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bw;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    .line 326
    const/4 v0, -0x1

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 327
    :cond_0
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 328
    const-string v0, "item_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 329
    const-string v0, "install"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 330
    :goto_1
    const-string v6, "extras"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/chaton/e/a/ab;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ac;

    move-result-object v6

    .line 331
    invoke-virtual {v6}, Lcom/sec/chaton/e/a/ac;->b()Ljava/lang/String;

    move-result-object v7

    .line 332
    sget-boolean v8, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v8, :cond_1

    .line 333
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "download_skin, download all, add into list,  id/backgroudType/install : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/chaton/settings/downloads/bw;->b:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    if-nez v0, :cond_0

    .line 338
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    aput-object v5, v0, v1

    invoke-virtual {v6}, Lcom/sec/chaton/e/a/ac;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v0, v2

    .line 329
    goto :goto_1

    .line 342
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 343
    aget-object v4, v0, v1

    aget-object v0, v0, v2

    invoke-direct {p0, v4, v0, v1}, Lcom/sec/chaton/settings/downloads/bw;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 345
    :cond_4
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 163
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030051

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 165
    new-instance v2, Lcom/sec/chaton/settings/downloads/bz;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/bz;-><init>(Lcom/sec/chaton/settings/downloads/bw;)V

    .line 166
    const v0, 0x7f0701f3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bz;->b:Landroid/widget/ImageView;

    .line 167
    const v0, 0x7f0701f2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bz;->c:Landroid/widget/ImageView;

    .line 168
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/bz;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    :cond_0
    const v0, 0x7f0701f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bz;->e:Landroid/widget/ProgressBar;

    .line 172
    const v0, 0x7f0701f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    .line 173
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/bz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    const v0, 0x7f0701f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/bz;->f:Landroid/widget/TextView;

    .line 176
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 178
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 349
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v2, 0x7f0701f2

    if-ne v0, v2, :cond_1

    .line 350
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/bw;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->f:Lcom/sec/chaton/settings/downloads/by;

    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 353
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->f:Lcom/sec/chaton/settings/downloads/by;

    invoke-interface {v1, v0}, Lcom/sec/chaton/settings/downloads/by;->a(Ljava/lang/String;)V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/bz;

    .line 360
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bw;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 361
    iget v0, v0, Lcom/sec/chaton/settings/downloads/bz;->a:I

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 363
    const-string v0, "item_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 364
    const-string v0, "install"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 365
    :goto_1
    const-string v1, "extras"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/ab;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ac;

    move-result-object v1

    .line 367
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 371
    if-eqz v0, :cond_4

    .line 372
    invoke-virtual {v1}, Lcom/sec/chaton/e/a/ac;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/chaton/settings/downloads/cd;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 373
    if-eqz v0, :cond_3

    .line 374
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->g(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 375
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bw;->notifyDataSetChanged()V

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->f:Lcom/sec/chaton/settings/downloads/by;

    invoke-interface {v0}, Lcom/sec/chaton/settings/downloads/by;->a()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 385
    :catch_0
    move-exception v0

    .line 386
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 387
    sget-object v1, Lcom/sec/chaton/settings/downloads/bw;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 364
    goto :goto_1

    .line 380
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->h(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 383
    :cond_4
    invoke-virtual {v1}, Lcom/sec/chaton/e/a/ac;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v3, v0, v1}, Lcom/sec/chaton/settings/downloads/bw;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 421
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 422
    const-string v0, "swapCursor(), newCursor : "

    sget-object v1, Lcom/sec/chaton/settings/downloads/bw;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    :cond_0
    if-nez p1, :cond_2

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->a()V

    .line 459
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 430
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 431
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 432
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 433
    const-string v0, "item_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 434
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 436
    :goto_2
    if-eqz v0, :cond_4

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 434
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .line 442
    :cond_4
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/r;

    .line 443
    if-nez v0, :cond_5

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 449
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Lcom/sec/chaton/settings/downloads/a/l;)Z

    goto :goto_1

    .line 456
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bw;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->b()V

    goto :goto_0
.end method
