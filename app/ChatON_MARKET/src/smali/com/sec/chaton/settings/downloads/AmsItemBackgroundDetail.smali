.class public Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;
.super Landroid/support/v4/app/Fragment;
.source "AmsItemBackgroundDetail.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Ljava/text/DateFormat;

.field private c:Landroid/app/Activity;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:J

.field private k:Z

.field private l:Lcom/sec/chaton/settings/downloads/a/a;

.field private m:Landroid/graphics/Bitmap;

.field private n:Landroid/database/Cursor;

.field private o:Z

.field private p:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 79
    new-instance v0, Lcom/sec/chaton/settings/downloads/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/e;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->p:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->n:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;Lcom/sec/chaton/settings/downloads/a/a;)Lcom/sec/chaton/settings/downloads/a/a;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 374
    const-wide/16 v3, 0x0

    .line 377
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 378
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 380
    const-string v0, "extras"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 382
    :try_start_0
    new-instance v3, Lcom/sec/chaton/io/entry/inner/AmsItem;

    invoke-direct {v3}, Lcom/sec/chaton/io/entry/inner/AmsItem;-><init>()V

    .line 383
    invoke-static {v3, v0}, Lcom/sec/chaton/e/a/a;->a(Lcom/sec/chaton/io/entry/inner/AmsItem;Ljava/lang/String;)V

    .line 384
    iget-object v0, v3, Lcom/sec/chaton/io/entry/inner/AmsItem;->filesize:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->j:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :cond_0
    :goto_0
    const-string v0, "expiration_time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 393
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->k:Z

    .line 396
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 399
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->k:Z

    if-eqz v0, :cond_3

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    const v3, 0x7f0200af

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    new-array v3, v11, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->j:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v2

    const-string v1, "KB) / "

    aput-object v1, v3, v9

    const v1, 0x7f0b022c

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v10

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/widget/TextView;)V

    .line 507
    :goto_2
    return-void

    .line 385
    :catch_0
    move-exception v0

    .line 386
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_0

    .line 387
    sget-object v3, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 393
    goto :goto_1

    .line 408
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    if-nez v0, :cond_4

    .line 415
    new-instance v0, Ljava/sql/Date;

    invoke-direct {v0, v3, v4}, Ljava/sql/Date;-><init>(J)V

    .line 416
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 418
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    const v4, 0x7f0200ac

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 421
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, " ("

    aput-object v5, v4, v1

    iget-wide v5, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->j:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v2

    const-string v1, "KB)"

    aput-object v1, v4, v9

    const-string v1, " / "

    aput-object v1, v4, v10

    const-string v1, "~"

    aput-object v1, v4, v11

    const/4 v1, 0x5

    aput-object v0, v4, v1

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto :goto_2

    .line 425
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v0

    const/4 v5, 0x5

    if-ne v0, v5, :cond_5

    .line 426
    new-instance v0, Ljava/sql/Date;

    invoke-direct {v0, v3, v4}, Ljava/sql/Date;-><init>(J)V

    .line 427
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 429
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    const v4, 0x7f0200ad

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 432
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, " ("

    aput-object v5, v4, v1

    iget-wide v5, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->j:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v2

    const-string v1, "KB)"

    aput-object v1, v4, v9

    const-string v1, " / "

    aput-object v1, v4, v10

    const-string v1, "~"

    aput-object v1, v4, v11

    const/4 v1, 0x5

    aput-object v0, v4, v1

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto/16 :goto_2

    .line 438
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 440
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    new-array v3, v10, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    const v1, 0x7f0b022a

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    const-string v1, ")"

    aput-object v1, v3, v9

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_2

    .line 454
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    new-array v3, v10, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->j:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v2

    const-string v1, "KB)"

    aput-object v1, v3, v9

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_2

    .line 469
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    const v1, 0x7f0b022b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->d(Landroid/widget/TextView;)V

    goto/16 :goto_2

    .line 438
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->c:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 351
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->n:Landroid/database/Cursor;

    .line 354
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->o:Z

    if-eqz v0, :cond_0

    .line 355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->o:Z

    .line 358
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/a;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->a(Landroid/os/Handler;)V

    .line 365
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Landroid/database/Cursor;)V

    .line 366
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 126
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->c:Landroid/app/Activity;

    .line 127
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 285
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 288
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->k:Z

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 295
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/f;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/f;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    if-nez v0, :cond_2

    .line 304
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->i:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->h:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/a/a;-><init>(Ljava/lang/String;Lcom/sec/chaton/d/e;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->a(Landroid/os/Handler;)V

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    .line 321
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->n:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 308
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->i:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->h:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/a/a;-><init>(Ljava/lang/String;Lcom/sec/chaton/d/e;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->a(Landroid/os/Handler;)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 312
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 316
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->a(Z)Z

    goto :goto_1

    .line 285
    :pswitch_data_0
    .packed-switch 0x7f070261
        :pswitch_0
    .end packed-switch

    .line 312
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "filePath"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->i:Ljava/lang/String;

    .line 202
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "amsItemId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->h:Ljava/lang/String;

    .line 206
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a:Ljava/text/DateFormat;

    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->o:Z

    .line 209
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 210
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 343
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v2

    const-string v1, "=?"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 344
    new-array v5, v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->h:Ljava/lang/String;

    aput-object v0, v5, v2

    .line 346
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->c:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 214
    const v0, 0x7f03005b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 218
    const v0, 0x7f070262

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->e:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 222
    const v0, 0x7f070261

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    const v0, 0x7f070263

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->d:Landroid/widget/ImageView;

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->m:Landroid/graphics/Bitmap;

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->d:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 229
    const v0, 0x7f07025f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->g:Landroid/widget/ImageView;

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->g:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 232
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->l:Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->b(Landroid/os/Handler;)V

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 143
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 144
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 237
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 244
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 192
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->c:Landroid/app/Activity;

    .line 195
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 53
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 371
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->stopLoading()V

    .line 154
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 155
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->startLoading()V

    .line 165
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 166
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 176
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 177
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 188
    return-void
.end method
