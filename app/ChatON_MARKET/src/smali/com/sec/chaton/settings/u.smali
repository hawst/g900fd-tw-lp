.class Lcom/sec/chaton/settings/u;
.super Ljava/lang/Object;
.source "ActivityContactSyncFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/chaton/settings/u;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/settings/u;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .line 140
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/u;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 143
    return v1

    :cond_1
    move v0, v1

    .line 140
    goto :goto_0
.end method
