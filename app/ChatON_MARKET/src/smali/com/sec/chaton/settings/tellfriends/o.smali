.class Lcom/sec/chaton/settings/tellfriends/o;
.super Ljava/lang/Object;
.source "RenrenManager.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/b;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/sec/chaton/settings/tellfriends/m;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/m;I)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    iput p2, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Post Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Canceled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/m;->f(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/m;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 156
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/a;)V
    .locals 4

    .prologue
    const/16 v3, -0x3ea

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Post Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Webview Error. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/m;->d(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->a()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/m;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->a()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/m;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Post Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Done."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/m;->e(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/o;->b:Lcom/sec/chaton/settings/tellfriends/m;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/m;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/o;->a:I

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2, p1}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 150
    return-void
.end method
