.class public Lcom/sec/chaton/settings/downloads/bt;
.super Ljava/lang/Object;
.source "PlaySoundTaskManager.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/audio/g;


# static fields
.field static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/bv;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/String;


# instance fields
.field b:Ljava/lang/String;

.field c:Z

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/multimedia/audio/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/chaton/settings/downloads/bt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/bt;->e:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/bt;->c:Z

    .line 95
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/bt;->b:Ljava/lang/String;

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->d:Ljava/util/ArrayList;

    .line 98
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/bu;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/bt;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static a(Ljava/lang/String;Landroid/os/Handler;)Lcom/sec/chaton/settings/downloads/bt;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/sec/chaton/settings/downloads/bt;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/bt;->a:Ljava/util/HashMap;

    .line 49
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/bt;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/bv;

    .line 50
    if-nez v0, :cond_1

    .line 51
    new-instance v0, Lcom/sec/chaton/settings/downloads/bv;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/bv;-><init>(Ljava/lang/String;)V

    .line 52
    sget-object v1, Lcom/sec/chaton/settings/downloads/bt;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bv;->a()V

    .line 56
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/bv;->a:Lcom/sec/chaton/settings/downloads/bt;

    .line 58
    invoke-virtual {v0, p1}, Lcom/sec/chaton/settings/downloads/bt;->a(Landroid/os/Handler;)V

    .line 59
    return-object v0
.end method

.method static b(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 63
    sget-object v0, Lcom/sec/chaton/settings/downloads/bt;->a:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    sget-object v0, Lcom/sec/chaton/settings/downloads/bt;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/bv;

    .line 68
    if-eqz v0, :cond_0

    .line 72
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/bv;->a:Lcom/sec/chaton/settings/downloads/bt;

    .line 73
    invoke-virtual {v1, p1}, Lcom/sec/chaton/settings/downloads/bt;->b(Landroid/os/Handler;)Z

    .line 75
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bv;->b()V

    .line 76
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/bt;->b()V

    .line 78
    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/bt;->a()V

    .line 79
    sget-object v0, Lcom/sec/chaton/settings/downloads/bt;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 121
    return-void
.end method

.method a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 101
    if-nez p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/multimedia/audio/d;)V
    .locals 3

    .prologue
    .line 250
    sget-object v0, Lcom/sec/chaton/settings/downloads/bu;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/d;->a()Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/audio/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 257
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 258
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/d;->a()Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/audio/f;->a()I

    move-result v2

    invoke-static {v0, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 259
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStatusChanged(), managerKey/taskKey/status : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/bt;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/d;->a()Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/settings/downloads/bt;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 253
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/bt;->c(Lcom/sec/chaton/multimedia/audio/d;)Z

    goto :goto_0

    .line 263
    :cond_1
    return-void

    .line 250
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/d;

    .line 175
    if-nez v0, :cond_0

    move v0, v1

    .line 187
    :goto_0
    return v0

    .line 179
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 180
    goto :goto_0

    .line 183
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v2, :cond_2

    .line 184
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 187
    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/d;

    .line 158
    if-eqz v0, :cond_0

    .line 159
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/audio/d;->cancel(Z)Z

    goto :goto_0

    .line 171
    :cond_1
    return-void
.end method

.method b(Landroid/os/Handler;)Z
    .locals 1

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 116
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method b(Lcom/sec/chaton/multimedia/audio/d;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 124
    if-nez p1, :cond_0

    move v0, v1

    .line 153
    :goto_0
    return v0

    .line 128
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/d;->b()Ljava/lang/String;

    move-result-object v2

    .line 129
    if-nez v2, :cond_1

    move v0, v1

    .line 130
    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/downloads/bt;->a(Ljava/lang/String;)Z

    move-result v0

    .line 134
    if-eqz v0, :cond_2

    .line 135
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/downloads/bt;->b(Ljava/lang/String;)Z

    move v0, v1

    .line 136
    goto :goto_0

    .line 139
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/bt;->c:Z

    if-nez v0, :cond_3

    .line 140
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/bt;->b()V

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/d;

    .line 144
    if-eqz v0, :cond_4

    .line 145
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/bt;->c(Lcom/sec/chaton/multimedia/audio/d;)Z

    .line 149
    :cond_4
    invoke-virtual {p1, p0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/g;)V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    new-array v0, v1, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/chaton/multimedia/audio/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 153
    const/4 v0, 0x1

    goto :goto_0
.end method

.method b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/d;

    .line 192
    if-nez v0, :cond_0

    move v0, v1

    .line 201
    :goto_0
    return v0

    .line 196
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    sget-object v4, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v3, v4, :cond_1

    .line 197
    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/audio/d;->cancel(Z)Z

    move v0, v2

    .line 198
    goto :goto_0

    :cond_1
    move v0, v1

    .line 201
    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/audio/f;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 244
    :goto_0
    return-object v0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/d;

    .line 240
    if-nez v0, :cond_1

    move-object v0, v1

    .line 241
    goto :goto_0

    .line 244
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/d;->a()Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v0

    goto :goto_0
.end method

.method c(Lcom/sec/chaton/multimedia/audio/d;)Z
    .locals 2

    .prologue
    .line 205
    if-nez p1, :cond_0

    .line 206
    const/4 v0, 0x0

    .line 214
    :goto_0
    return v0

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bt;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/d;->c()V

    .line 214
    const/4 v0, 0x1

    goto :goto_0
.end method
