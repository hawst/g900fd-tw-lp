.class Lcom/sec/chaton/settings/ag;
.super Ljava/lang/Object;
.source "ActivityDownloads.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityDownloads;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityDownloads;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/sec/chaton/settings/ag;->a:Lcom/sec/chaton/settings/ActivityDownloads;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 361
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    const/4 v0, 0x0

    .line 370
    :goto_0
    return v0

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ag;->a:Lcom/sec/chaton/settings/ActivityDownloads;

    sget-object v1, Lcom/sec/chaton/settings/ai;->a:Lcom/sec/chaton/settings/ai;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/ActivityDownloads;->a(Lcom/sec/chaton/settings/ActivityDownloads;Lcom/sec/chaton/settings/ai;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/ag;->a:Lcom/sec/chaton/settings/ActivityDownloads;

    invoke-static {v1}, Lcom/sec/chaton/settings/ActivityDownloads;->b(Lcom/sec/chaton/settings/ActivityDownloads;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivitySoundDownloads;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 367
    iget-object v1, p0, Lcom/sec/chaton/settings/ag;->a:Lcom/sec/chaton/settings/ActivityDownloads;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->startActivity(Landroid/content/Intent;)V

    .line 370
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
