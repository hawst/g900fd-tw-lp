.class public Lcom/sec/chaton/settings/moreapps/c;
.super Lcom/sec/common/f/a;
.source "MoreDispatcherTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Landroid/net/Uri;

.field private c:Landroid/content/Context;

.field private d:Landroid/widget/ImageView;

.field private e:Ljava/lang/String;

.field private i:Landroid/widget/ImageView;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/chaton/settings/moreapps/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/moreapps/c;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/settings/moreapps/b;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 42
    iput-object p2, p0, Lcom/sec/chaton/settings/moreapps/c;->c:Landroid/content/Context;

    .line 43
    iget-object v0, p4, Lcom/sec/chaton/settings/moreapps/b;->a:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/moreapps/c;->d:Landroid/widget/ImageView;

    .line 44
    iput-object p3, p0, Lcom/sec/chaton/settings/moreapps/c;->e:Ljava/lang/String;

    .line 45
    iget-object v0, p4, Lcom/sec/chaton/settings/moreapps/b;->c:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/moreapps/c;->i:Landroid/widget/ImageView;

    .line 46
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 50
    iget-object v1, p0, Lcom/sec/chaton/settings/moreapps/c;->d:Landroid/widget/ImageView;

    .line 52
    invoke-virtual {p0}, Lcom/sec/chaton/settings/moreapps/c;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0203cb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 53
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 55
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 56
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 59
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 5

    .prologue
    .line 154
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/moreapps/c;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    if-eqz p1, :cond_6

    .line 161
    :try_start_0
    instance-of v1, p1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 163
    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 165
    const-string v1, " This bitmap is already recycled. "

    const-class v2, Lcom/sec/chaton/settings/moreapps/c;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    :goto_0
    return-void

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/settings/moreapps/c;->d:Landroid/widget/ImageView;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 177
    :cond_3
    :goto_1
    iget-boolean v1, p0, Lcom/sec/chaton/settings/moreapps/c;->j:Z

    if-eqz v1, :cond_5

    .line 178
    iget-object v1, p0, Lcom/sec/chaton/settings/moreapps/c;->i:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v1

    .line 189
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 190
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 193
    :catchall_0
    move-exception v1

    throw v1

    .line 173
    :cond_4
    :try_start_2
    instance-of v1, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 174
    iget-object v1, p0, Lcom/sec/chaton/settings/moreapps/c;->d:Landroid/widget/ImageView;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 191
    :catch_1
    move-exception v1

    .line 192
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 180
    :cond_5
    :try_start_4
    iget-object v1, p0, Lcom/sec/chaton/settings/moreapps/c;->i:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 185
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/settings/moreapps/c;->d:Landroid/widget/ImageView;

    const v2, 0x7f020440

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/c;->d:Landroid/widget/ImageView;

    .line 66
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 68
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 70
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/chaton/settings/moreapps/c;->a(Ljava/util/concurrent/Callable;J)V

    .line 71
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 77
    .line 79
    invoke-virtual {p0}, Lcom/sec/chaton/settings/moreapps/c;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 147
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/c;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/settings/moreapps/c;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/moreapps/c;->j:Z

    .line 91
    :goto_1
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/moreapps/c;->k()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/settings/moreapps/c;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 92
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/moreapps/c;->a:Landroid/net/Uri;

    .line 96
    :try_start_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/settings/moreapps/c;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    if-eqz v2, :cond_4

    .line 110
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/c;->a:Landroid/net/Uri;

    invoke-static {v0}, Lcom/sec/chaton/util/ad;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_1
    iput-boolean v3, p0, Lcom/sec/chaton/settings/moreapps/c;->j:Z

    goto :goto_1

    .line 98
    :catch_0
    move-exception v0

    .line 99
    :try_start_2
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_2

    .line 100
    sget-object v2, Lcom/sec/chaton/settings/moreapps/c;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    move-object v0, v1

    .line 103
    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    throw v0

    .line 115
    :catch_1
    move-exception v0

    .line 117
    :try_start_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 118
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v1

    .line 119
    goto :goto_0

    .line 121
    :catch_2
    move-exception v0

    .line 122
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v1

    .line 123
    goto/16 :goto_0

    .line 124
    :catchall_1
    move-exception v0

    throw v0

    .line 129
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/c;->a:Landroid/net/Uri;

    invoke-static {v0}, Lcom/sec/chaton/util/ad;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v0

    goto/16 :goto_0

    .line 132
    :catch_3
    move-exception v0

    .line 134
    :try_start_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 135
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v1

    .line 136
    goto/16 :goto_0

    .line 138
    :catch_4
    move-exception v0

    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v0, v1

    .line 140
    goto/16 :goto_0

    .line 141
    :catchall_2
    move-exception v0

    throw v0

    :cond_4
    move-object v0, v1

    .line 147
    goto/16 :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/chaton/settings/moreapps/c;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/sec/chaton/settings/moreapps/c;->g()Ljava/lang/Object;

    move-result-object v1

    .line 207
    if-eqz v1, :cond_0

    .line 208
    instance-of v0, v1, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 209
    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 215
    :cond_0
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method
