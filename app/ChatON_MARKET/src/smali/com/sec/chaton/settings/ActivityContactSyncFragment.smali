.class public Lcom/sec/chaton/settings/ActivityContactSyncFragment;
.super Lcom/sec/widget/PreferenceListFragment;
.source "ActivityContactSyncFragment.java"


# instance fields
.field a:Z

.field private b:Ljava/lang/String;

.field private c:Lcom/sec/chaton/settings/SyncStatePreference;

.field private d:Landroid/preference/CheckBoxPreference;

.field private e:Lcom/sec/chaton/util/ab;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/widget/PreferenceListFragment;-><init>()V

    .line 35
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->b:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->e:Lcom/sec/chaton/util/ab;

    .line 44
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->e:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method private a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    const-string v1, "support_contact_auto_sync"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    new-instance v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 112
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->e:Lcom/sec/chaton/util/ab;

    const-string v3, "auto_contact_sync"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 114
    invoke-virtual {v1, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 118
    :goto_0
    new-instance v2, Lcom/sec/chaton/settings/t;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/t;-><init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 134
    new-instance v2, Lcom/sec/chaton/settings/u;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/u;-><init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 146
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_0
    new-instance v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d:Landroid/preference/CheckBoxPreference;

    .line 150
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0281

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d:Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 154
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings/v;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/v;-><init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 175
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings/w;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/w;-><init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 194
    const-string v1, "for_wifi_only_device"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_1
    new-instance v1, Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/SyncStatePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    .line 199
    const-string v1, "support_contact_auto_sync"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 200
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    const v2, 0x7f0b03a9

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/SyncStatePreference;->setTitle(I)V

    .line 201
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/SyncStatePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 207
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    new-instance v2, Lcom/sec/chaton/settings/x;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/x;-><init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/SyncStatePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    new-instance v2, Lcom/sec/chaton/settings/y;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/y;-><init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/SyncStatePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 238
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    return-object v0

    .line 116
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_0

    .line 203
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    const v2, 0x7f0b0277

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/SyncStatePreference;->setTitle(I)V

    .line 204
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/SyncStatePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->d:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting Sync TimeInMillis"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 246
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    .line 247
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 249
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0244

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private c()Ljava/lang/String;
    .locals 6

    .prologue
    const v5, 0x7f0b0193

    .line 259
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting Sync TimeInMillis"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 261
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_0
    return-object v0

    .line 263
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 267
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0244

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 76
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 77
    invoke-super {p0, p1}, Lcom/sec/widget/PreferenceListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 78
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/sec/widget/PreferenceListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Lcom/sec/widget/PreferenceListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 83
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->e:Lcom/sec/chaton/util/ab;

    .line 60
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->e:Lcom/sec/chaton/util/ab;

    const-string v1, "contact_sim_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 61
    iput-boolean v4, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->e:Lcom/sec/chaton/util/ab;

    const-string v1, "contact_sim_sync"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 67
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->f:Ljava/util/List;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->f:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a(Ljava/util/List;)V

    .line 69
    invoke-super {p0, p1, p2, p3}, Lcom/sec/widget/PreferenceListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 64
    :cond_0
    iput-boolean v3, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a:Z

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->e:Lcom/sec/chaton/util/ab;

    const-string v1, "contact_sim_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/sec/widget/PreferenceListFragment;->onPause()V

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/SyncStatePreference;->b()V

    .line 105
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Lcom/sec/widget/PreferenceListFragment;->onResume()V

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->c:Lcom/sec/chaton/settings/SyncStatePreference;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/SyncStatePreference;->a()V

    .line 97
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 88
    invoke-super {p0}, Lcom/sec/widget/PreferenceListFragment;->onStop()V

    .line 89
    return-void
.end method
