.class public Lcom/sec/chaton/settings/downloads/a/x;
.super Landroid/os/AsyncTask;
.source "SoundUninstallWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Lcom/sec/chaton/widget/m;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/chaton/settings/downloads/a/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/a/x;->b:Landroid/app/Activity;

    .line 33
    iput-boolean p2, p0, Lcom/sec/chaton/settings/downloads/a/x;->d:Z

    .line 34
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 50
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a/x;->b:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 51
    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Lcom/sec/chaton/settings/downloads/dg;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 56
    sget-object v1, Lcom/sec/chaton/settings/downloads/a/x;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->c:Lcom/sec/chaton/widget/m;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->c:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->dismiss()V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->b:Landroid/app/Activity;

    const v1, 0x7f0b0228

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a/x;->b:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 75
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->d:Z

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 78
    :cond_1
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/x;->a([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/x;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 40
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->b:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->c:Lcom/sec/chaton/widget/m;

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->c:Lcom/sec/chaton/widget/m;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a/x;->b:Landroid/app/Activity;

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/m;->setMessage(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/x;->c:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->show()V

    .line 43
    return-void
.end method
