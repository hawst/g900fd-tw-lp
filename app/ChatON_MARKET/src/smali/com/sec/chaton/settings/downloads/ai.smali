.class Lcom/sec/chaton/settings/downloads/ai;
.super Landroid/os/Handler;
.source "AniconPackageDetail.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 168
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 177
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 179
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_2

    .line 181
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/ViewPackageEntry;

    .line 182
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ViewPackageEntry;->_package:Lcom/sec/chaton/io/entry/inner/Package;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Package;->id:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Ljava/lang/String;)Ljava/lang/String;

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 186
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ai;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/ActivityAniconPackageDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ActivityAniconPackageDetail;->c()V

    goto :goto_0
.end method
