.class Lcom/sec/chaton/settings/ev;
.super Ljava/lang/Object;
.source "RingtonePreference2.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/RingtonePreference2;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/RingtonePreference2;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->e(Lcom/sec/chaton/settings/RingtonePreference2;)V

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->c(Lcom/sec/chaton/settings/RingtonePreference2;)Landroid/media/RingtoneManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/RingtoneManager;->stopPreviousRingtone()V

    .line 258
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/RingtonePreference2;->a(Lcom/sec/chaton/settings/RingtonePreference2;Lcom/sec/chaton/settings/eq;)Lcom/sec/chaton/settings/eq;

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->f(Lcom/sec/chaton/settings/RingtonePreference2;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 265
    iget-object v1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->f(Lcom/sec/chaton/settings/RingtonePreference2;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/eq;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/RingtonePreference2;->a(Lcom/sec/chaton/settings/RingtonePreference2;Lcom/sec/chaton/settings/eq;)Lcom/sec/chaton/settings/eq;

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->b(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/settings/eq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 268
    sget-object v0, Lcom/sec/chaton/settings/ex;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v1}, Lcom/sec/chaton/settings/RingtonePreference2;->b(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/settings/eq;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/settings/eq;->a()Lcom/sec/chaton/settings/er;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/settings/er;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 270
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->b(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/settings/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/eq;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->c(Lcom/sec/chaton/settings/RingtonePreference2;)Landroid/media/RingtoneManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v1}, Lcom/sec/chaton/settings/RingtonePreference2;->b(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/settings/eq;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/settings/eq;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(I)Landroid/media/Ringtone;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0}, Landroid/media/Ringtone;->play()V

    goto :goto_0

    .line 278
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->d(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/multimedia/audio/a;

    move-result-object v0

    if-nez v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->g(Lcom/sec/chaton/settings/RingtonePreference2;)V

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->b(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/settings/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/eq;->d()I

    move-result v0

    .line 282
    if-lez v0, :cond_0

    .line 283
    iget-object v1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v1}, Lcom/sec/chaton/settings/RingtonePreference2;->d(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/multimedia/audio/a;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/multimedia/audio/a;->a(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 284
    iget-object v1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v1}, Lcom/sec/chaton/settings/RingtonePreference2;->d(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/multimedia/audio/a;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/audio/a;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 289
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->d(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/multimedia/audio/a;

    move-result-object v0

    if-nez v0, :cond_3

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->g(Lcom/sec/chaton/settings/RingtonePreference2;)V

    .line 293
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->b(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/settings/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/eq;->c()Landroid/net/Uri;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_0

    .line 295
    iget-object v1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v1}, Lcom/sec/chaton/settings/RingtonePreference2;->d(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/multimedia/audio/a;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/multimedia/audio/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 296
    iget-object v1, p0, Lcom/sec/chaton/settings/ev;->a:Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-static {v1}, Lcom/sec/chaton/settings/RingtonePreference2;->d(Lcom/sec/chaton/settings/RingtonePreference2;)Lcom/sec/chaton/multimedia/audio/a;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/audio/a;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 268
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
