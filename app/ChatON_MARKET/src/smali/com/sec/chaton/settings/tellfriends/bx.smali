.class Lcom/sec/chaton/settings/tellfriends/bx;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "WeiboManager.java"


# instance fields
.field a:Lweibo4android/http/AccessToken;

.field final synthetic b:Lcom/sec/chaton/settings/tellfriends/bt;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;Lweibo4android/http/AccessToken;)V
    .locals 1

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bx;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    .line 297
    const-string v0, "GetMyInfoTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 298
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/bx;->a:Lweibo4android/http/AccessToken;

    .line 299
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;Lweibo4android/http/AccessToken;Lcom/sec/chaton/settings/tellfriends/bu;)V
    .locals 0

    .prologue
    .line 293
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/tellfriends/bx;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;Lweibo4android/http/AccessToken;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 5

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bx;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    const-string v1, "GetMyInfoTask() : Get my name"

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bt;->a(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V

    .line 304
    const/4 v0, 0x0

    .line 306
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/bt;->f()Lweibo4android/Weibo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bx;->a:Lweibo4android/http/AccessToken;

    invoke-virtual {v2}, Lweibo4android/http/AccessToken;->getUserId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lweibo4android/Weibo;->showUser(Ljava/lang/String;)Lweibo4android/User;

    move-result-object v1

    .line 308
    if-eqz v1, :cond_0

    .line 309
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bx;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/bx;->a:Lweibo4android/http/AccessToken;

    invoke-virtual {v3}, Lweibo4android/http/AccessToken;->getUserId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/settings/tellfriends/cc;->b(Landroid/content/Context;Ljava/lang/String;)Z

    .line 310
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bx;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Lweibo4android/User;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/settings/tellfriends/cc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 311
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bx;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Lweibo4android/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/settings/tellfriends/cc;->c(Landroid/content/Context;Ljava/lang/String;)Z

    .line 314
    :cond_0
    new-instance v1, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bx;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/bt;->a(Lcom/sec/chaton/settings/tellfriends/bt;)Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 315
    sget-object v2, Lcom/sec/chaton/settings/tellfriends/y;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/bx;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v3, v3, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/cc;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bx;->e:I
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :goto_0
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bx;->f:Ljava/lang/Object;

    .line 324
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bx;->e:I

    return v0

    .line 317
    :catch_0
    move-exception v0

    .line 318
    invoke-virtual {v0}, Lweibo4android/WeiboException;->printStackTrace()V

    .line 319
    const/16 v1, -0x3ed

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bx;->e:I

    .line 320
    invoke-virtual {v0}, Lweibo4android/WeiboException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
