.class public Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;
.source "FacebookMessageFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/u;
.implements Lcom/sec/chaton/settings/tellfriends/common/v;


# instance fields
.field private b:Lcom/sec/chaton/settings/tellfriends/aj;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;-><init>()V

    .line 23
    const-string v0, "140"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a(I)V

    .line 24
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/v;)V

    .line 25
    invoke-virtual {p0, p0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/u;)V

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->d()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->d()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->d()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 87
    if-nez p1, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->b(I)V

    .line 90
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->c()V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/aj;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/b;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/tellfriends/b;-><init>(Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;)V

    invoke-virtual {v0, v1, v2, p2}, Lcom/sec/chaton/settings/tellfriends/aj;->a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 39
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/aj;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/tellfriends/aj;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/aj;

    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/FacebookMessageFragment;->b:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a()V

    .line 44
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onDestroy()V

    .line 45
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->onResume()V

    .line 51
    return-void
.end method
