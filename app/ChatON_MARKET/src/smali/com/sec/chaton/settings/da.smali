.class Lcom/sec/chaton/settings/da;
.super Ljava/lang/Object;
.source "DeleteAccountPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/DeleteAccountPreference;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/DeleteAccountPreference;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/chaton/settings/da;->a:Lcom/sec/chaton/settings/DeleteAccountPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 77
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 81
    :cond_0
    new-instance v0, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteAMSAlertDialog;

    invoke-direct {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteAMSAlertDialog;-><init>()V

    .line 84
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "delete_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteAMSAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v0

    .line 88
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
