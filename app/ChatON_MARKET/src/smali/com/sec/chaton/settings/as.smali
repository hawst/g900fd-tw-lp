.class Lcom/sec/chaton/settings/as;
.super Ljava/lang/Object;
.source "ActivityNoti.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityNoti;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/chaton/settings/as;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/high16 v5, 0x7f0e0000

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    if-eqz p2, :cond_1

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/settings/as;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/ActivityNoti;->a(Lcom/sec/chaton/settings/ActivityNoti;Z)Z

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings/as;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->a(Lcom/sec/chaton/settings/ActivityNoti;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting Notification"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/settings/as;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;Z)V

    .line 176
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 177
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_0
    :goto_0
    return v3

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/as;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/ActivityNoti;->a(Lcom/sec/chaton/settings/ActivityNoti;Z)Z

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/settings/as;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->a(Lcom/sec/chaton/settings/ActivityNoti;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting Notification"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/as;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;Z)V

    .line 185
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 186
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
