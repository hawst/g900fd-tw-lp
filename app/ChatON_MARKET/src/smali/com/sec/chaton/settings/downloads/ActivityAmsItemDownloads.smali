.class public Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityAmsItemDownloads.java"


# instance fields
.field private a:Lcom/sec/chaton/base/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;->a:Lcom/sec/chaton/base/e;

    .line 28
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;->a:Lcom/sec/chaton/base/e;

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 49
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 53
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;->a:Lcom/sec/chaton/base/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;->a:Lcom/sec/chaton/base/e;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/base/e;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 60
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 37
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 41
    :cond_0
    return-void
.end method
