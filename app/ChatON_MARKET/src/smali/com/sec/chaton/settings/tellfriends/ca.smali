.class public Lcom/sec/chaton/settings/tellfriends/ca;
.super Ljava/lang/Object;
.source "WeiboManager.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/b;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/bt;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;I)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369
    iput p2, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    .line 370
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Canceled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bt;->f(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/bt;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 401
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/a;)V
    .locals 4

    .prologue
    const/16 v3, -0x3ea

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Webview Error. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bt;->e(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V

    .line 388
    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->a()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/bt;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->a()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 394
    :goto_0
    return-void

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/bt;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Done."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/bt;->d(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V

    .line 376
    instance-of v0, p1, Lweibo4android/http/AccessToken;

    if-eqz v0, :cond_0

    .line 377
    check-cast p1, Lweibo4android/http/AccessToken;

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {p1, v0}, Lcom/sec/chaton/settings/tellfriends/cc;->a(Lweibo4android/http/AccessToken;Landroid/content/Context;)Z

    .line 379
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bx;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/chaton/settings/tellfriends/bx;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;Lweibo4android/http/AccessToken;Lcom/sec/chaton/settings/tellfriends/bu;)V

    .line 380
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/ca;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    iget v3, p0, Lcom/sec/chaton/settings/tellfriends/ca;->b:I

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 382
    :cond_0
    return-void
.end method
