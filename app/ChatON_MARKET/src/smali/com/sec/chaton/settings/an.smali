.class Lcom/sec/chaton/settings/an;
.super Ljava/lang/Object;
.source "ActivityManageAccounts.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityManageAccounts;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityManageAccounts;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/chaton/settings/an;->a:Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 144
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/an;->a:Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-static {v1}, Lcom/sec/chaton/settings/ActivityManageAccounts;->a(Lcom/sec/chaton/settings/ActivityManageAccounts;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 146
    const-string v1, "isSyncContacts"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 147
    iget-object v1, p0, Lcom/sec/chaton/settings/an;->a:Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-virtual {v1, v0, v3}, Lcom/sec/chaton/settings/ActivityManageAccounts;->startActivityForResult(Landroid/content/Intent;I)V

    .line 150
    const/4 v0, 0x0

    return v0
.end method
