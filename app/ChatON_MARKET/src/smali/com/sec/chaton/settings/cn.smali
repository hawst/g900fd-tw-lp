.class public Lcom/sec/chaton/settings/cn;
.super Ljava/lang/Object;
.source "CustomTimeData.java"


# instance fields
.field private final a:J

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:J

.field private g:J

.field private h:Z


# direct methods
.method public constructor <init>(IIIIZ)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/sec/chaton/settings/cn;->a:J

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/cn;->h:Z

    .line 18
    iput p1, p0, Lcom/sec/chaton/settings/cn;->b:I

    .line 19
    iput p2, p0, Lcom/sec/chaton/settings/cn;->c:I

    .line 20
    iput p3, p0, Lcom/sec/chaton/settings/cn;->d:I

    .line 21
    iput p4, p0, Lcom/sec/chaton/settings/cn;->e:I

    .line 22
    iput-boolean p5, p0, Lcom/sec/chaton/settings/cn;->h:Z

    .line 23
    invoke-direct {p0}, Lcom/sec/chaton/settings/cn;->k()V

    .line 24
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/sec/chaton/settings/cn;->a:J

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/cn;->h:Z

    .line 27
    iput-wide p1, p0, Lcom/sec/chaton/settings/cn;->f:J

    .line 28
    iput-wide p3, p0, Lcom/sec/chaton/settings/cn;->g:J

    .line 29
    invoke-direct {p0}, Lcom/sec/chaton/settings/cn;->j()V

    .line 30
    return-void
.end method

.method private j()V
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/16 v3, 0xb

    .line 33
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 34
    iget-wide v1, p0, Lcom/sec/chaton/settings/cn;->f:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 35
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/settings/cn;->b:I

    .line 36
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/settings/cn;->c:I

    .line 38
    iget-wide v1, p0, Lcom/sec/chaton/settings/cn;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 39
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/settings/cn;->d:I

    .line 40
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/cn;->e:I

    .line 41
    return-void
.end method

.method private k()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 44
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 46
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget v4, p0, Lcom/sec/chaton/settings/cn;->b:I

    iget v5, p0, Lcom/sec/chaton/settings/cn;->c:I

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 47
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/chaton/settings/cn;->f:J

    .line 49
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget v4, p0, Lcom/sec/chaton/settings/cn;->d:I

    iget v5, p0, Lcom/sec/chaton/settings/cn;->e:I

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 50
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/settings/cn;->g:J

    .line 51
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/chaton/settings/cn;->f:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/sec/chaton/settings/cn;->g:J

    return-wide v0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/sec/chaton/settings/cn;->g:J

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/chaton/settings/cn;->g:J

    .line 63
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/sec/chaton/settings/cn;->f:J

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/chaton/settings/cn;->f:J

    .line 67
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/chaton/settings/cn;->h:Z

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/chaton/settings/cn;->b:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/chaton/settings/cn;->c:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/chaton/settings/cn;->d:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/chaton/settings/cn;->e:I

    return v0
.end method
