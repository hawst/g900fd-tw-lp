.class public Lcom/sec/chaton/settings/downloads/ct;
.super Lcom/sec/chaton/multimedia/emoticon/anicon/n;
.source "SkinPreviewDispatcherTask.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/ct;->n()V

    .line 29
    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f0201ca

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/ct;->a(I)V

    .line 39
    return-void
.end method


# virtual methods
.method public c()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ct;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 45
    const-string v1, "android.resource"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ct;->k()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 49
    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 53
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->c()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
