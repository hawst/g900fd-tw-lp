.class Lcom/sec/chaton/settings/fi;
.super Ljava/lang/Object;
.source "SuggestionsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/SuggestionsActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/SuggestionsActivity;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/high16 v5, 0x7f0e0000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 298
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 299
    const/4 v3, -0x3

    if-eq v3, v2, :cond_0

    const/4 v3, -0x2

    if-ne v3, v2, :cond_1

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 326
    :goto_0
    return v0

    .line 303
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 305
    if-eqz p2, :cond_3

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 306
    iget-object v2, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v2, v1}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;Z)Z

    .line 307
    iget-object v2, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v2}, Lcom/sec/chaton/settings/SuggestionsActivity;->d(Lcom/sec/chaton/settings/SuggestionsActivity;)Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "exclude_me"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 309
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-ne v2, v0, :cond_2

    .line 310
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v2

    const-string v3, "00100003"

    const-string v4, "00000005"

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v2}, Lcom/sec/chaton/settings/SuggestionsActivity;->e(Lcom/sec/chaton/settings/SuggestionsActivity;)Lcom/sec/chaton/d/h;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/chaton/d/h;->f(Z)V

    .line 314
    iget-object v1, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v1}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 316
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v2, v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;Z)Z

    .line 317
    iget-object v2, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v2}, Lcom/sec/chaton/settings/SuggestionsActivity;->d(Lcom/sec/chaton/settings/SuggestionsActivity;)Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "exclude_me"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 319
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-ne v1, v0, :cond_4

    .line 320
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    const-string v2, "00100003"

    const-string v3, "00000006"

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v1}, Lcom/sec/chaton/settings/SuggestionsActivity;->e(Lcom/sec/chaton/settings/SuggestionsActivity;)Lcom/sec/chaton/d/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/h;->f(Z)V

    .line 324
    iget-object v1, p0, Lcom/sec/chaton/settings/fi;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v1}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto/16 :goto_0
.end method
