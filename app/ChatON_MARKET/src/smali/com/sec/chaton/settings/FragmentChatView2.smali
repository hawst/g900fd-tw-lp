.class public Lcom/sec/chaton/settings/FragmentChatView2;
.super Landroid/support/v4/app/Fragment;
.source "FragmentChatView2.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/chaton/settings/dk;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/chaton/settings/dk;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/app/Activity;

.field private d:Lcom/sec/chaton/settings/dk;

.field private e:Landroid/widget/ListView;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->f:I

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    .line 120
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentChatView2;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->c:Landroid/app/Activity;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->a:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f0b01e0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->a:Lcom/sec/chaton/settings/dk;

    iget-object v3, p0, Lcom/sec/chaton/settings/FragmentChatView2;->c:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/chaton/util/am;->b(Landroid/content/Context;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->b:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f0b017a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->b:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f090135

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->c:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f0b017b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->c:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f090136

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f0b017c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f090137

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->e:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f0b017d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->e:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f090138

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->f:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f0b017e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    sget-object v2, Lcom/sec/chaton/settings/dk;->f:Lcom/sec/chaton/settings/dk;

    const v3, 0x7f090139

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/FragmentChatView2;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/FragmentChatView2;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->b:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 142
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentChatView2;->c:Landroid/app/Activity;

    .line 143
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 148
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 149
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 150
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 124
    invoke-direct {p0}, Lcom/sec/chaton/settings/FragmentChatView2;->a()V

    .line 126
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Default Font Size"

    sget-object v2, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/dk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/dk;->a(Ljava/lang/String;)Lcom/sec/chaton/settings/dk;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->d:Lcom/sec/chaton/settings/dk;

    .line 128
    const v0, 0x7f0300e9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 130
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->e:Landroid/widget/ListView;

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->e:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/settings/dm;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/dm;-><init>(Lcom/sec/chaton/settings/FragmentChatView2;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->e:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/settings/FragmentChatView2;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/dk;->ordinal()I

    move-result v2

    invoke-virtual {v0, v2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 135
    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/FragmentChatView2;->setHasOptionsMenu(Z)V

    .line 137
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 168
    iput p3, p0, Lcom/sec/chaton/settings/FragmentChatView2;->f:I

    .line 169
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 154
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705a6

    if-ne v0, v1, :cond_1

    .line 155
    iget v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 156
    iget v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->f:I

    invoke-static {v0}, Lcom/sec/chaton/settings/dk;->a(I)Lcom/sec/chaton/settings/dk;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentChatView2;->d:Lcom/sec/chaton/settings/dk;

    .line 157
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Default Font Size"

    iget v2, p0, Lcom/sec/chaton/settings/FragmentChatView2;->f:I

    invoke-static {v2}, Lcom/sec/chaton/settings/dk;->a(I)Lcom/sec/chaton/settings/dk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/settings/dk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentChatView2;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00e6

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentChatView2;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/dl;

    invoke-interface {v0}, Lcom/sec/chaton/settings/dl;->c()V

    .line 163
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
