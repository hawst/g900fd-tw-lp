.class Lcom/sec/chaton/settings/downloads/ae;
.super Landroid/os/Handler;
.source "AniconPackageAdapter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/ad;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/ad;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ae;->a:Lcom/sec/chaton/settings/downloads/ad;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 64
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/e;

    .line 65
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ae;->a:Lcom/sec/chaton/settings/downloads/ad;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/ad;->a(Lcom/sec/chaton/settings/downloads/ad;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/af;

    .line 67
    if-nez v1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 71
    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 74
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 75
    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "PackageId: "

    aput-object v2, v1, v3

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    const-string v0, ". Status: "

    aput-object v0, v1, v5

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/ad;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ae;->a:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ad;->notifyDataSetChanged()V

    goto :goto_0

    .line 86
    :pswitch_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 87
    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "PackageId: "

    aput-object v2, v1, v3

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    const-string v0, ". Status: "

    aput-object v0, v1, v5

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/ad;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ae;->a:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ad;->notifyDataSetChanged()V

    goto :goto_0

    .line 95
    :pswitch_2
    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
