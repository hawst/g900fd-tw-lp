.class final enum Lcom/sec/chaton/settings/ai;
.super Ljava/lang/Enum;
.source "ActivityDownloads.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/ai;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/ai;

.field public static final enum b:Lcom/sec/chaton/settings/ai;

.field private static final synthetic c:[Lcom/sec/chaton/settings/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    new-instance v0, Lcom/sec/chaton/settings/ai;

    const-string v1, "External"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/ai;->a:Lcom/sec/chaton/settings/ai;

    .line 76
    new-instance v0, Lcom/sec/chaton/settings/ai;

    const-string v1, "Internal"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/settings/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/ai;->b:Lcom/sec/chaton/settings/ai;

    .line 74
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/settings/ai;

    sget-object v1, Lcom/sec/chaton/settings/ai;->a:Lcom/sec/chaton/settings/ai;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/settings/ai;->b:Lcom/sec/chaton/settings/ai;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/settings/ai;->c:[Lcom/sec/chaton/settings/ai;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/ai;
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/sec/chaton/settings/ai;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/ai;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/ai;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/chaton/settings/ai;->c:[Lcom/sec/chaton/settings/ai;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/ai;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/ai;

    return-object v0
.end method
