.class Lcom/sec/chaton/settings/y;
.super Ljava/lang/Object;
.source "ActivityContactSyncFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/chaton/settings/y;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 226
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 228
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/y;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b015f

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 235
    :goto_0
    return v3

    .line 232
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_request_sync_in_chaton"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 233
    invoke-static {v4}, Lcom/sec/chaton/account/i;->a(Z)V

    goto :goto_0
.end method
