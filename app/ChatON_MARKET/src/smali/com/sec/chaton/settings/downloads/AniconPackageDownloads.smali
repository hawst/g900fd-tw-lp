.class public Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;
.super Landroid/support/v4/app/Fragment;
.source "AniconPackageDownloads.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/sec/chaton/base/e;
.implements Lcom/sec/chaton/settings/downloads/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/sec/chaton/base/e;",
        "Lcom/sec/chaton/settings/downloads/ag;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Landroid/widget/ListView;

.field private d:Landroid/app/ProgressDialog;

.field private e:Lcom/sec/chaton/d/j;

.field private f:Lcom/sec/chaton/d/a/cd;

.field private g:Lcom/sec/chaton/settings/downloads/ad;

.field private h:Lcom/sec/common/f/c;

.field private i:Landroid/view/Menu;

.field private j:Landroid/os/Handler;

.field private k:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 110
    new-instance v0, Lcom/sec/chaton/settings/downloads/al;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/al;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->j:Landroid/os/Handler;

    .line 137
    new-instance v0, Lcom/sec/chaton/settings/downloads/am;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/am;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->k:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;Lcom/sec/chaton/d/a/cd;)Lcom/sec/chaton/d/a/cd;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->f:Lcom/sec/chaton/d/a/cd;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 387
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_anicon_sortby"

    sget-object v2, Lcom/sec/chaton/settings/downloads/au;->b:Lcom/sec/chaton/settings/downloads/au;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/au;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 388
    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/au;->a(I)Lcom/sec/chaton/settings/downloads/au;

    move-result-object v0

    .line 389
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0251

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0d001c

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/au;->a()I

    move-result v0

    new-instance v3, Lcom/sec/chaton/settings/downloads/ap;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/downloads/ap;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/common/a/a;->a(IILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 410
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->f(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 415
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/aq;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/aq;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 424
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->e()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Lcom/sec/chaton/settings/downloads/ad;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 483
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->a(Landroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    .line 484
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/downloads/ar;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/downloads/ar;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 497
    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->c:Landroid/widget/ListView;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 503
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 509
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->e(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 511
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/as;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/as;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 535
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 536
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 344
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 345
    const-string v0, "anicon_list, AniconPackageListView.onLoadFinished()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/ad;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 349
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 362
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivityAniconPackageDetail;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 363
    const-string v1, "ANICON_PACKAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 365
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->startActivity(Landroid/content/Intent;)V

    .line 366
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 370
    .line 373
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 377
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/ao;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/settings/downloads/ao;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 384
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705a9

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 128
    :cond_0
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ad;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->f()V

    .line 474
    const/4 v0, 0x1

    .line 478
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705aa

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 135
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 170
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 172
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    .line 173
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->k:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/sec/chaton/d/j;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->e:Lcom/sec/chaton/d/j;

    .line 180
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 290
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 291
    const-string v0, "anicon_list, AniconPackageListView.onCreateLoader()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_anicon_sortby"

    sget-object v2, Lcom/sec/chaton/settings/downloads/au;->b:Lcom/sec/chaton/settings/downloads/au;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/au;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 301
    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/au;->a(I)Lcom/sec/chaton/settings/downloads/au;

    move-result-object v0

    .line 302
    sget-object v1, Lcom/sec/chaton/settings/downloads/at;->a:[I

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/au;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 305
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "special"

    aput-object v1, v0, v4

    const-string v1, " DESC,"

    aput-object v1, v0, v5

    const-string v1, "new"

    aput-object v1, v0, v6

    const-string v1, " DESC,"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, " DESC"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 335
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "anicon_list, order by : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_1
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 313
    :pswitch_0
    new-array v0, v7, [Ljava/lang/Object;

    const-string v1, "special"

    aput-object v1, v0, v4

    const-string v1, " DESC,"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 320
    :pswitch_1
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "special"

    aput-object v1, v0, v4

    const-string v1, " DESC,"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data3"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 327
    :pswitch_2
    new-array v0, v7, [Ljava/lang/Object;

    const-string v1, "special"

    aput-object v1, v0, v4

    const-string v1, " DESC,"

    aput-object v1, v0, v5

    const-string v1, "down_rank"

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 428
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->i:Landroid/view/Menu;

    .line 430
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    const v0, 0x7f0f0026

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 432
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 433
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 437
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a(Z)V

    .line 438
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b(Z)V

    .line 440
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 441
    return-void

    .line 435
    :cond_0
    const v0, 0x7f0f0025

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 184
    const v0, 0x7f0300d2

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 186
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->h:Lcom/sec/common/f/c;

    .line 188
    const v0, 0x7f0703ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->c:Landroid/widget/ListView;

    .line 189
    new-instance v0, Lcom/sec/chaton/settings/downloads/ad;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->h:Lcom/sec/common/f/c;

    invoke-direct {v0, v2, v3, v5, v4}, Lcom/sec/chaton/settings/downloads/ad;-><init>(Landroid/content/Context;Landroid/database/Cursor;ZLcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/ad;->a(Lcom/sec/chaton/settings/downloads/ag;)V

    .line 191
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->c:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 194
    new-instance v0, Lcom/sec/chaton/settings/downloads/aw;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/aw;-><init>()V

    .line 195
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->j:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Landroid/os/Handler;)V

    .line 196
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/settings/downloads/ad;->a(Lcom/sec/chaton/settings/downloads/aw;)V

    .line 198
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->setHasOptionsMenu(Z)V

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->c:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/settings/downloads/an;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/an;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 246
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->d()V

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->e:Lcom/sec/chaton/d/j;

    sget-object v2, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    const-string v3, "0"

    const-string v4, "zip"

    const/16 v5, 0xf0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/chaton/d/j;->a(Lcom/sec/chaton/d/a/b;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/chaton/d/a/cd;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->f:Lcom/sec/chaton/d/a/cd;

    .line 251
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 256
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 258
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 260
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->f:Lcom/sec/chaton/d/a/cd;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->f:Lcom/sec/chaton/d/a/cd;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/cd;->c()V

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->h:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->h:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    if-eqz v0, :cond_3

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ad;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ad;->b()V

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ad;->c()V

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/ad;->a(Lcom/sec/chaton/settings/downloads/ag;)V

    .line 279
    :cond_3
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 283
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    .line 286
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 61
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 353
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 354
    const-string v0, "anicon_list, AniconPackageListView.onLoaderReset()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/ad;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 358
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 445
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 464
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 448
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->g:Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/ad;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->f()V

    goto :goto_0

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 456
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->b()V

    goto :goto_0

    .line 460
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->c()V

    goto :goto_0

    .line 445
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0705a9 -> :sswitch_1
        0x7f0705aa -> :sswitch_2
    .end sparse-switch
.end method
