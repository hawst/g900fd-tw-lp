.class public Lcom/sec/chaton/settings/downloads/AniconPackageDetail;
.super Landroid/support/v4/app/Fragment;
.source "AniconPackageDetail.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/common/f/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/sec/common/f/f;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Landroid/view/View;

.field private C:Landroid/view/Menu;

.field private D:Landroid/app/ProgressDialog;

.field private E:Landroid/os/Handler;

.field private F:Landroid/os/Handler;

.field a:Ljava/text/DateFormat;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/ImageView;

.field private s:Landroid/widget/ImageView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/ProgressBar;

.field private v:Lcom/sec/chaton/d/j;

.field private w:Lcom/sec/common/f/c;

.field private x:Ljava/io/File;

.field private y:Lcom/sec/chaton/settings/downloads/a/e;

.field private z:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/sec/chaton/settings/downloads/ActivityAniconPackageDetail;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 88
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->g:J

    .line 127
    new-instance v0, Lcom/sec/chaton/settings/downloads/ah;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/ah;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->E:Landroid/os/Handler;

    .line 165
    new-instance v0, Lcom/sec/chaton/settings/downloads/ai;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/ai;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->F:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->z:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Lcom/sec/chaton/settings/downloads/a/e;)Lcom/sec/chaton/settings/downloads/a/e;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 10

    .prologue
    .line 487
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 488
    const-wide/16 v0, 0x0

    .line 490
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 491
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 492
    const-string v0, "cursor is closed"

    sget-object v1, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    if-eqz p1, :cond_e

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_e

    .line 499
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 502
    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 503
    const-string v0, "extras"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 504
    const/4 v0, 0x0

    .line 507
    :try_start_0
    invoke-static {v2}, Lcom/sec/chaton/e/a/b;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/c;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 514
    :cond_2
    :goto_1
    iput-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->d:Ljava/lang/String;

    .line 515
    const-string v1, "install"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_2
    iput-boolean v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->h:Z

    .line 516
    const-string v1, "new"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->i:I

    .line 517
    const-string v1, "special"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->j:I

    .line 518
    const-string v1, "expiration_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 519
    if-eqz v0, :cond_3

    .line 520
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/c;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->e:Ljava/lang/String;

    .line 521
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/c;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->f:Ljava/lang/String;

    .line 522
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/c;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 523
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/c;->d()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->g:J

    :cond_3
    move-wide v8, v1

    move-wide v0, v8

    move-object v2, v3

    .line 527
    :goto_3
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->p:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 529
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 530
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 532
    iget-boolean v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->h:Z

    if-eqz v3, :cond_7

    .line 533
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 535
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 538
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v3, " ("

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->g:J

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "KB) / "

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const v3, 0x7f0b022c

    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/widget/TextView;)V

    .line 606
    :goto_4
    iget v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->j:I

    if-eqz v0, :cond_c

    .line 607
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    const v1, 0x7f0b01dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    const-string v1, "#3eb1b9"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 620
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 621
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->f:Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->k:I

    iget v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->l:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;II)V

    .line 622
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->w:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 625
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 626
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->e:Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->m:I

    iget v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->n:I

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;IIZ)V

    .line 627
    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a(I)V

    .line 628
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->w:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 631
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->B:Landroid/view/View;

    const v1, 0x7f070317

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    goto/16 :goto_0

    .line 508
    :catch_0
    move-exception v2

    .line 509
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_2

    .line 510
    sget-object v4, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->b:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 515
    :cond_6
    const/4 v1, 0x1

    goto/16 :goto_2

    .line 541
    :cond_7
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    if-nez v3, :cond_8

    .line 548
    new-instance v3, Ljava/sql/Date;

    invoke-direct {v3, v0, v1}, Ljava/sql/Date;-><init>(J)V

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 551
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const v3, 0x7f0200ac

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 552
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 554
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    const-string v4, " ("

    aput-object v4, v3, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const-string v4, "KB)"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string v4, " / "

    aput-object v4, v3, v2

    const/4 v2, 0x5

    const-string v4, "~"

    aput-object v4, v3, v2

    const/4 v2, 0x6

    aput-object v0, v3, v2

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 556
    :cond_8
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    invoke-virtual {v3}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_9

    .line 557
    new-instance v3, Ljava/sql/Date;

    invoke-direct {v3, v0, v1}, Ljava/sql/Date;-><init>(J)V

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 560
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const v3, 0x7f0200ad

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 561
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 563
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    const-string v4, " ("

    aput-object v4, v3, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const-string v4, "KB)"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string v4, " / "

    aput-object v4, v3, v2

    const/4 v2, 0x5

    const-string v4, "~"

    aput-object v4, v3, v2

    const/4 v2, 0x6

    aput-object v0, v3, v2

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 564
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 566
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 567
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_4

    .line 569
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const v1, 0x7f0200ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-nez v0, :cond_a

    .line 573
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 576
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v3, " ("

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0b022a

    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, ")"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 578
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 582
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const v1, 0x7f0200ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 585
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 586
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 588
    :cond_b
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v3, " ("

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->g:J

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "KB)"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 596
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 597
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 599
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    const v1, 0x7f0b022b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 600
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->d(Landroid/widget/TextView;)V

    goto/16 :goto_4

    .line 611
    :cond_c
    iget v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->i:I

    if-eqz v0, :cond_d

    .line 612
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 614
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    const v1, 0x7f0b01dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 615
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    const-string v1, "#e86d00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto/16 :goto_5

    .line 617
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_e
    move-object v2, v3

    goto/16 :goto_3

    .line 567
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/high16 v4, 0x41a00000    # 20.0f

    .line 461
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 462
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 465
    if-nez v2, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 471
    if-eqz v2, :cond_0

    .line 474
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 475
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 477
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v4}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 479
    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 481
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->h:Z

    if-eqz v0, :cond_0

    .line 350
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 351
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/ak;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/ak;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 415
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->z:Landroid/database/Cursor;

    .line 418
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->A:Z

    if-eqz v0, :cond_0

    .line 419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->A:Z

    .line 422
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/e;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->a(Landroid/os/Handler;)V

    .line 429
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Landroid/database/Cursor;)V

    .line 430
    return-void
.end method

.method public a(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 439
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    const v1, 0x7f02035d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 442
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 447
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    :goto_0
    return-void

    .line 367
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 378
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->h:Z

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 399
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->z:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 381
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    if-nez v0, :cond_2

    .line 382
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->v:Lcom/sec/chaton/d/j;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/e;-><init>(Lcom/sec/chaton/d/j;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    .line 383
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->a(Landroid/os/Handler;)V

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 386
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->v:Lcom/sec/chaton/d/j;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/e;-><init>(Lcom/sec/chaton/d/j;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->a(Landroid/os/Handler;)V

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 390
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 394
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->a(Z)Z

    goto :goto_1

    .line 367
    :pswitch_data_0
    .packed-switch 0x7f0701d2
        :pswitch_0
    .end packed-switch

    .line 390
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 452
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 454
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c()V

    .line 455
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 203
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ANICON_PACKAGE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 407
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v2

    const-string v1, "=?"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 408
    new-array v5, v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    aput-object v0, v5, v2

    .line 410
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const v1, 0x7f0705ab

    .line 277
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 279
    const v0, 0x7f0f0027

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 281
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->C:Landroid/view/Menu;

    .line 282
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->h:Z

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->C:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 288
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->C:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 214
    const v0, 0x7f0300d1

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 216
    iput-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->B:Landroid/view/View;

    .line 218
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a:Ljava/text/DateFormat;

    .line 220
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->F:Landroid/os/Handler;

    invoke-static {v0, v2}, Lcom/sec/chaton/d/j;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->v:Lcom/sec/chaton/d/j;

    .line 221
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->w:Lcom/sec/common/f/c;

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->w:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0901d1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->k:I

    .line 226
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0901d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->l:I

    .line 228
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->m:I

    .line 229
    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->n:I

    .line 231
    const v0, 0x7f0701d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->o:Landroid/widget/ImageView;

    .line 233
    const v0, 0x7f0701d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->p:Landroid/widget/TextView;

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->p:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 237
    const v0, 0x7f0701d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->q:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 241
    const v0, 0x7f0701d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    const v0, 0x7f070317

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->s:Landroid/widget/ImageView;

    .line 245
    const v0, 0x7f0701d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->t:Landroid/widget/TextView;

    .line 246
    const v0, 0x7f0701d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->u:Landroid/widget/ProgressBar;

    .line 249
    iput-boolean v5, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->A:Z

    .line 251
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b00b8

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/sec/chaton/settings/downloads/aj;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/aj;-><init>(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->v:Lcom/sec/chaton/d/j;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->c:Ljava/lang/String;

    const-string v3, "1"

    const/16 v4, 0xf0

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/chaton/d/j;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/chaton/d/a/ep;

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 270
    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->setHasOptionsMenu(Z)V

    .line 272
    return-object v1

    .line 266
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 322
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->y:Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->b(Landroid/os/Handler;)V

    .line 328
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->x:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->x:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->x:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->w:Lcom/sec/common/f/c;

    if-eqz v0, :cond_2

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->w:Lcom/sec/common/f/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->w:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 339
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 342
    :cond_3
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 79
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 435
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 292
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 298
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 295
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a()V

    goto :goto_0

    .line 292
    :pswitch_data_0
    .packed-switch 0x7f0705ab
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 313
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 315
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->stopLoading()V

    .line 318
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 304
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 306
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->startLoading()V

    .line 309
    :cond_0
    return-void
.end method
