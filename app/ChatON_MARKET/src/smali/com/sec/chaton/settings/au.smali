.class Lcom/sec/chaton/settings/au;
.super Ljava/lang/Object;
.source "ActivityNoti.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityNoti;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityNoti;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300da

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 246
    const v0, 0x7f0703d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 247
    new-instance v2, Lcom/sec/chaton/settings/bd;

    iget-object v3, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {v2, v3}, Lcom/sec/chaton/settings/bd;-><init>(Lcom/sec/chaton/settings/ActivityNoti;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 248
    iget-object v2, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v2}, Lcom/sec/chaton/settings/ActivityNoti;->c(Lcom/sec/chaton/settings/ActivityNoti;)I

    move-result v2

    invoke-virtual {v0, v2, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 249
    new-instance v2, Lcom/sec/chaton/settings/av;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/av;-><init>(Lcom/sec/chaton/settings/au;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    iget-object v2, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    invoke-static {v2}, Lcom/sec/chaton/settings/ActivityNoti;->b(Lcom/sec/chaton/settings/ActivityNoti;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b032a

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings/ActivityNoti;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/settings/aw;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/aw;-><init>(Lcom/sec/chaton/settings/au;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/au;->a:Lcom/sec/chaton/settings/ActivityNoti;

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings/ActivityNoti;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/settings/ActivityNoti;->g:Lcom/sec/common/a/d;

    .line 282
    return v4
.end method
