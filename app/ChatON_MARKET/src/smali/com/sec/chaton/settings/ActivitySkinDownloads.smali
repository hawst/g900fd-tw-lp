.class public Lcom/sec/chaton/settings/ActivitySkinDownloads;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivitySkinDownloads.java"

# interfaces
.implements Lcom/sec/chaton/settings/downloads/cs;


# instance fields
.field private a:Lcom/sec/chaton/base/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/sec/chaton/settings/downloads/SkinListView;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/SkinListView;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivitySkinDownloads;->a:Lcom/sec/chaton/base/e;

    .line 30
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySkinDownloads;->a:Lcom/sec/chaton/base/e;

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/settings/ActivityBgBubbleChange;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    const-string v1, "called_from_downloads"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivitySkinDownloads;->startActivity(Landroid/content/Intent;)V

    .line 69
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 52
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySkinDownloads;->a:Lcom/sec/chaton/base/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivitySkinDownloads;->a:Lcom/sec/chaton/base/e;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/base/e;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 59
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 37
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 40
    :cond_0
    return-void
.end method
