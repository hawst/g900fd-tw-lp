.class public Lcom/sec/chaton/settings/downloads/ActivityFontDetail;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityFontDetail.java"


# instance fields
.field private a:Lcom/sec/chaton/settings/downloads/FontDetail;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 79
    const-string v0, "startPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 82
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;->startActivity(Landroid/content/Intent;)V

    .line 88
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/FontDetail;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 72
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 76
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 60
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 63
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;->c()V

    .line 64
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 46
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 49
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ActivityFontDetail;->finish()V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
