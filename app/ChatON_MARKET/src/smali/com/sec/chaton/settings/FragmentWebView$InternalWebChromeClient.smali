.class Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "FragmentWebView.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings/FragmentWebView;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/FragmentWebView;)V
    .locals 0

    .prologue
    .line 837
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/FragmentWebView;Lcom/sec/chaton/settings/FragmentWebView$1;)V
    .locals 0

    .prologue
    .line 837
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;-><init>(Lcom/sec/chaton/settings/FragmentWebView;)V

    return-void
.end method


# virtual methods
.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 943
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebChromeClient;->onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    .line 953
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/FragmentWebView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 977
    :goto_0
    return v3

    .line 957
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/FragmentWebView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 958
    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 959
    invoke-virtual {v0, p3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 960
    const v1, 0x104000a

    new-instance v2, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient$1;

    invoke-direct {v2, p0, p4}, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient$1;-><init>(Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;Landroid/webkit/JsResult;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 967
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 968
    new-instance v1, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient$2;

    invoke-direct {v1, p0, p4}, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient$2;-><init>(Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;Landroid/webkit/JsResult;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 975
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/FragmentWebView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 842
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/FragmentWebView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    mul-int/lit8 v1, p2, 0x64

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setProgress(I)V

    .line 844
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 849
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "failingUrl = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "desctiption = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    return-void
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 855
    const-string v0, "openFileChooser under SDK 10"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    # setter for: Lcom/sec/chaton/settings/FragmentWebView;->mUploadMessage:Landroid/webkit/ValueCallback;
    invoke-static {v0, p1}, Lcom/sec/chaton/settings/FragmentWebView;->access$302(Lcom/sec/chaton/settings/FragmentWebView;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 858
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 859
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 861
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentWebView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 862
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 863
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 864
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 865
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 872
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentWebView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 879
    :cond_1
    :goto_0
    return-void

    .line 873
    :catch_0
    move-exception v0

    .line 874
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 875
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 876
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 883
    const-string v0, "openFileChooser over SDK 11 and under SDK 15"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    # setter for: Lcom/sec/chaton/settings/FragmentWebView;->mUploadMessage:Landroid/webkit/ValueCallback;
    invoke-static {v0, p1}, Lcom/sec/chaton/settings/FragmentWebView;->access$302(Lcom/sec/chaton/settings/FragmentWebView;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 886
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 887
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 889
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentWebView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 890
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 891
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 892
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 893
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 900
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentWebView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 907
    :cond_1
    :goto_0
    return-void

    .line 901
    :catch_0
    move-exception v0

    .line 902
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 903
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 904
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 911
    const-string v0, "openFileChooser over SDK 16"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    # setter for: Lcom/sec/chaton/settings/FragmentWebView;->mUploadMessage:Landroid/webkit/ValueCallback;
    invoke-static {v0, p1}, Lcom/sec/chaton/settings/FragmentWebView;->access$302(Lcom/sec/chaton/settings/FragmentWebView;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 914
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 915
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 930
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentWebView$InternalWebChromeClient;->this$0:Lcom/sec/chaton/settings/FragmentWebView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentWebView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 931
    :catch_0
    move-exception v0

    .line 932
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 933
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 934
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
