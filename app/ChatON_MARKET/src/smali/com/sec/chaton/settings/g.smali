.class public Lcom/sec/chaton/settings/g;
.super Landroid/widget/BaseAdapter;
.source "AboutServiceFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/AboutServiceFragment;

.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/coolots/sso/a/d;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 609
    iput-object p1, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 604
    iput v1, p0, Lcom/sec/chaton/settings/g;->e:I

    .line 605
    iput-boolean v0, p0, Lcom/sec/chaton/settings/g;->f:Z

    .line 606
    iput-boolean v0, p0, Lcom/sec/chaton/settings/g;->g:Z

    .line 607
    iput-boolean v0, p0, Lcom/sec/chaton/settings/g;->h:Z

    .line 687
    new-instance v0, Lcom/sec/chaton/settings/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/h;-><init>(Lcom/sec/chaton/settings/g;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/g;->i:Lcom/coolots/sso/a/d;

    .line 610
    invoke-static {p1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/g;->b:Landroid/view/LayoutInflater;

    .line 612
    invoke-static {p1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iput-boolean v1, p0, Lcom/sec/chaton/settings/g;->g:Z

    .line 614
    iget v0, p0, Lcom/sec/chaton/settings/g;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/g;->e:I

    .line 616
    :cond_0
    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 617
    iput-boolean v1, p0, Lcom/sec/chaton/settings/g;->h:Z

    .line 618
    iget v0, p0, Lcom/sec/chaton/settings/g;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/g;->e:I

    .line 620
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->g:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->h:Z

    if-ne v0, v1, :cond_2

    .line 621
    sput v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    .line 622
    const/4 v0, -0x1

    sput v0, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    .line 629
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/settings/g;->a()V

    .line 630
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 634
    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->g:Z

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    invoke-static {}, Lcom/sec/chaton/util/cb;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/cb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 637
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/a;

    .line 638
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->l(Lcom/sec/chaton/settings/AboutServiceFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 639
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->l(Lcom/sec/chaton/settings/AboutServiceFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->i:Lcom/coolots/sso/a/d;

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/d;)V

    .line 640
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->l(Lcom/sec/chaton/settings/AboutServiceFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->n(Landroid/content/Context;)I

    .line 649
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->h:Z

    if-eqz v0, :cond_4

    .line 650
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    const-string v2, "com.sec.spp.push"

    invoke-static {v2}, Lcom/sec/chaton/util/cb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 653
    :try_start_0
    const-string v0, "content://com.sec.spp.provider/version_info"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 655
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 657
    if-eqz v0, :cond_6

    .line 658
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 659
    iget-object v1, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v1, v1, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v2, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    const-string v3, "current_version"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 660
    iget-object v1, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v1, v1, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    sget v2, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    const-string v3, "latest_version"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 661
    const-string v1, "need_update"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/settings/g;->f:Z

    .line 663
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 664
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SPP update status] currentVer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " latestVer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " needUpdate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/settings/g;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/g;->notifyDataSetChanged()V

    .line 668
    const-string v1, "spp_update_is"

    iget-boolean v2, p0, Lcom/sec/chaton/settings/g;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 669
    iget-boolean v1, p0, Lcom/sec/chaton/settings/g;->f:Z

    if-eqz v1, :cond_2

    .line 670
    iget-object v1, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "more_tab_badge_update"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 672
    :cond_2
    const-string v1, "spp_latest_ver"

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 685
    :cond_4
    :goto_1
    return-void

    .line 642
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 643
    const-string v0, "chatonv update is NOT available"

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 677
    :cond_6
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 678
    const-string v0, "cursor is null"

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 681
    :catch_0
    move-exception v0

    .line 682
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 815
    if-nez p1, :cond_2

    .line 816
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;I)V

    .line 817
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 818
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 839
    :cond_0
    :goto_0
    return-void

    .line 820
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 822
    :cond_2
    sget v0, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    if-ne p1, v0, :cond_5

    .line 823
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;I)V

    .line 825
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->o(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chatonVUpdateStatus"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_4

    .line 827
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 829
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 831
    :cond_5
    sget v0, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    if-ne p1, v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;I)V

    .line 833
    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->f:Z

    if-nez v0, :cond_6

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "spp_update_is"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 834
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 836
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 796
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->c:Landroid/widget/ImageView;

    const v1, 0x7f0203d7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 797
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->d:Landroid/widget/TextView;

    const v1, 0x7f0b03d4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 798
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aget-object v0, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 799
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aget-object v1, v0, v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aget-object v0, v0, v3

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 801
    sget v0, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/g;->a(I)V

    .line 802
    return-void

    .line 799
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "chatonv_update_version"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "chatonv_update_version"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->d:I

    aget-object v0, v0, v3

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 805
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->c:Landroid/widget/ImageView;

    const v1, 0x7f0203de

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 806
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->d:Landroid/widget/TextView;

    const v1, 0x7f0b029a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 807
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v0, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 808
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v1, v0, v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "spp_latest_ver"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "spp_latest_ver"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 810
    sget v0, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/g;->a(I)V

    .line 811
    return-void

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings/AboutServiceFragment;->e:I

    aget-object v0, v0, v3

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 716
    iget v0, p0, Lcom/sec/chaton/settings/g;->e:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 723
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 729
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 736
    if-nez p2, :cond_0

    .line 737
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0300b6

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 740
    :cond_0
    const v0, 0x7f070338

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/g;->c:Landroid/widget/ImageView;

    .line 742
    const v0, 0x7f070339

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/g;->d:Landroid/widget/TextView;

    .line 744
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v1

    const v0, 0x7f07033a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, p1

    .line 745
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v1

    const v0, 0x7f07033b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, p1

    .line 747
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->e(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/Button;

    move-result-object v1

    const v0, 0x7f07033c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, p1

    .line 749
    packed-switch p1, :pswitch_data_0

    .line 791
    :cond_1
    :goto_0
    return-object p2

    .line 751
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->c:Landroid/widget/ImageView;

    const v1, 0x7f0203d7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 752
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->d:Landroid/widget/TextView;

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 753
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/AboutServiceFragment;->b(Lcom/sec/chaton/settings/AboutServiceFragment;I)I

    .line 754
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->c:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 755
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 756
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->c:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/settings/i;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/i;-><init>(Lcom/sec/chaton/settings/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 770
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->c(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    aget-object v0, v0, v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v2, v2, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 771
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    aget-object v1, v0, v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->b:[Ljava/lang/String;

    aget-object v0, v0, v4

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 772
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->d(Lcom/sec/chaton/settings/AboutServiceFragment;)[Landroid/widget/TextView;

    move-result-object v0

    aget-object v0, v0, v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 773
    invoke-direct {p0, v4}, Lcom/sec/chaton/settings/g;->a(I)V

    goto/16 :goto_0

    .line 771
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/g;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    iget-object v0, v0, Lcom/sec/chaton/settings/AboutServiceFragment;->a:[Ljava/lang/String;

    aget-object v0, v0, v4

    goto :goto_1

    .line 777
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->g:Z

    if-eqz v0, :cond_3

    .line 778
    invoke-direct {p0}, Lcom/sec/chaton/settings/g;->b()V

    goto/16 :goto_0

    .line 779
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->h:Z

    if-eqz v0, :cond_1

    .line 780
    invoke-direct {p0}, Lcom/sec/chaton/settings/g;->c()V

    goto/16 :goto_0

    .line 785
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/chaton/settings/g;->h:Z

    if-eqz v0, :cond_1

    .line 786
    invoke-direct {p0}, Lcom/sec/chaton/settings/g;->c()V

    goto/16 :goto_0

    .line 749
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
