.class public Lcom/sec/chaton/settings/downloads/ad;
.super Landroid/support/v4/widget/CursorAdapter;
.source "AniconPackageAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/settings/downloads/a/q;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/settings/downloads/aw;

.field private c:I

.field private d:I

.field private e:Lcom/sec/common/f/c;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/af;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/sec/chaton/d/j;

.field private h:Lcom/sec/chaton/settings/downloads/ag;

.field private i:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/chaton/settings/downloads/ad;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/ad;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ZLcom/sec/common/f/c;)V
    .locals 2

    .prologue
    .line 159
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 59
    new-instance v0, Lcom/sec/chaton/settings/downloads/ae;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/ae;-><init>(Lcom/sec/chaton/settings/downloads/ad;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->i:Landroid/os/Handler;

    .line 161
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/ad;->c:I

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/ad;->d:I

    .line 164
    iput-object p4, p0, Lcom/sec/chaton/settings/downloads/ad;->e:Lcom/sec/common/f/c;

    .line 165
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->f:Ljava/util/HashMap;

    .line 167
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/chaton/d/j;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->g:Lcom/sec/chaton/d/j;

    .line 168
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/ad;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->h:Lcom/sec/chaton/settings/downloads/ag;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->h:Lcom/sec/chaton/settings/downloads/ag;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/settings/downloads/ag;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 329
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/e;

    .line 331
    if-nez v0, :cond_1

    .line 332
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ad;->g:Lcom/sec/chaton/d/j;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/e;-><init>(Lcom/sec/chaton/d/j;Ljava/lang/String;)V

    .line 333
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 335
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ad;->g:Lcom/sec/chaton/d/j;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/e;-><init>(Lcom/sec/chaton/d/j;Ljava/lang/String;)V

    .line 336
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_0

    .line 338
    :cond_2
    if-eqz p2, :cond_0

    .line 339
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 343
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 344
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_anicon, download all - requestInstall : cancel(), id/status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ad;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/e;->a(Z)Z

    goto :goto_0

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/chaton/settings/downloads/ad;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    if-eqz p1, :cond_0

    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/af;

    .line 115
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ad;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 116
    iget v0, v0, Lcom/sec/chaton/settings/downloads/af;->a:I

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 117
    const-string v0, "item_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    :cond_0
    return-object v0
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ad;->notifyDataSetChanged()V

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 470
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/ag;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ad;->h:Lcom/sec/chaton/settings/downloads/ag;

    .line 128
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/aw;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    .line 458
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 136
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 143
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/e/ar;)V

    .line 144
    return-void
.end method

.method public b(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 475
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 483
    :cond_0
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 20

    .prologue
    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/af;

    .line 196
    const-string v2, "extras"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 197
    const/4 v2, 0x0

    .line 201
    :try_start_0
    invoke-static {v3}, Lcom/sec/chaton/e/a/b;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/c;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 208
    :cond_0
    :goto_0
    const-string v3, "item_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 209
    const-string v3, "name"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 210
    const-string v3, "install"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x0

    .line 211
    :goto_1
    const-string v4, "new"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 212
    const-string v4, "special"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 214
    const/4 v7, 0x0

    .line 215
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 216
    const-wide/16 v4, 0x0

    .line 217
    if-eqz v2, :cond_1

    .line 218
    invoke-virtual {v2}, Lcom/sec/chaton/e/a/c;->b()Ljava/lang/String;

    move-result-object v7

    .line 219
    invoke-virtual {v2}, Lcom/sec/chaton/e/a/c;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 220
    invoke-virtual {v2}, Lcom/sec/chaton/e/a/c;->d()J

    move-result-wide v4

    .line 223
    :cond_1
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_2

    .line 224
    const-string v2, "down_rank"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 225
    const-string v2, "data1"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 226
    const-string v14, "data2"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 227
    const-string v15, "data3"

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 228
    const-string v16, "anicon_list, bindView() : [id:%s] [installed:%s] [special:%d] [new:%d] [downrank:%03d] [group:%s-%s-%s] [volume:%d] [piece:%s] [title:%s] "

    const/16 v17, 0xb

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x3

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x4

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v17, v18

    const/4 v12, 0x5

    aput-object v2, v17, v12

    const/4 v2, 0x6

    aput-object v14, v17, v2

    const/4 v2, 0x7

    aput-object v15, v17, v2

    const/16 v2, 0x8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v17, v2

    const/16 v2, 0x9

    aput-object v6, v17, v2

    const/16 v12, 0xa

    if-nez v9, :cond_5

    const-string v2, "(null)"

    :goto_2
    aput-object v2, v17, v12

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 232
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v12, Lcom/sec/chaton/settings/downloads/ad;->b:Ljava/lang/String;

    invoke-static {v2, v12}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/ad;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/ad;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v2

    sget-object v12, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2, v12, v8}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/settings/downloads/a/e;

    .line 242
    if-eqz v2, :cond_3

    .line 243
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/settings/downloads/ad;->i:Landroid/os/Handler;

    invoke-virtual {v2, v8}, Lcom/sec/chaton/settings/downloads/a/e;->a(Landroid/os/Handler;)V

    .line 246
    :cond_3
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    iput v8, v1, Lcom/sec/chaton/settings/downloads/af;->a:I

    .line 247
    iget-object v8, v1, Lcom/sec/chaton/settings/downloads/af;->c:Landroid/widget/TextView;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v8, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    iget-object v8, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 251
    if-eqz v3, :cond_6

    .line 252
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    const v3, 0x7f0200af

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 254
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v3, v8

    const/4 v6, 0x1

    const-string v8, " ("

    aput-object v8, v3, v6

    const/4 v6, 0x2

    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x3

    const-string v5, "KB) / "

    aput-object v5, v3, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/settings/downloads/ad;->mContext:Landroid/content/Context;

    const v6, 0x7f0b01d7

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/widget/TextView;)V

    .line 310
    :goto_3
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 312
    if-eqz v11, :cond_b

    .line 313
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->f:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 315
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->f:Landroid/widget/ImageView;

    const v3, 0x7f0201e2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 323
    :goto_4
    new-instance v2, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/chaton/settings/downloads/ad;->c:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/chaton/settings/downloads/ad;->d:I

    invoke-direct {v2, v7, v3, v4}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;II)V

    .line 324
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/settings/downloads/ad;->e:Lcom/sec/common/f/c;

    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/af;->b:Landroid/widget/ImageView;

    invoke-virtual {v3, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 326
    return-void

    .line 202
    :catch_0
    move-exception v3

    .line 203
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_0

    .line 204
    sget-object v4, Lcom/sec/chaton/settings/downloads/ad;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 210
    :cond_4
    const/4 v3, 0x1

    goto/16 :goto_1

    .line 228
    :cond_5
    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 257
    :cond_6
    if-nez v2, :cond_7

    .line 258
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    const v3, 0x7f0200ac

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 259
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 260
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v3, v8

    const/4 v6, 0x1

    const-string v8, " ("

    aput-object v8, v3, v6

    const/4 v6, 0x2

    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x3

    const-string v5, "KB)"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto :goto_3

    .line 264
    :cond_7
    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v3

    const/4 v8, 0x5

    if-ne v3, v8, :cond_8

    .line 265
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    const v3, 0x7f0200ad

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 266
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 267
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v3, v8

    const/4 v6, 0x1

    const-string v8, " ("

    aput-object v8, v3, v6

    const/4 v6, 0x2

    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x3

    const-string v5, "KB)"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 271
    :cond_8
    iget-object v3, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 273
    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/e;->d()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_3

    .line 275
    :pswitch_0
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 278
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v2

    if-nez v2, :cond_9

    .line 279
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 282
    :cond_9
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const-string v5, " ("

    aput-object v5, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/settings/downloads/ad;->mContext:Landroid/content/Context;

    const v6, 0x7f0b022a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, ")"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 287
    :pswitch_1
    iget-object v3, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    const v8, 0x7f0200ae

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 290
    iget-object v3, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 291
    iget-object v3, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 293
    :cond_a
    iget-object v3, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/a/e;->g()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 295
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v3, v8

    const/4 v6, 0x1

    const-string v8, " ("

    aput-object v8, v3, v6

    const/4 v6, 0x2

    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x3

    const-string v5, "KB)"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 300
    :pswitch_2
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 303
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/settings/downloads/ad;->mContext:Landroid/content/Context;

    const v4, 0x7f0b022b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->d(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 316
    :cond_b
    if-eqz v10, :cond_c

    .line 317
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->f:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 319
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->f:Landroid/widget/ImageView;

    const v3, 0x7f0201e1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 321
    :cond_c
    iget-object v2, v1, Lcom/sec/chaton/settings/downloads/af;->f:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 273
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 147
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->c(Lcom/sec/chaton/e/ar;)V

    .line 148
    return-void
.end method

.method public c(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 488
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ad;->notifyDataSetChanged()V

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 493
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 500
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 490
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 361
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 362
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ad;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 363
    const/4 v0, -0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 364
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365
    const-string v0, "item_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 366
    const-string v0, "name"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 367
    const-string v0, "install"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 368
    :goto_1
    sget-boolean v6, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v6, :cond_1

    if-eqz v5, :cond_1

    .line 369
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "download_anicon, download all, add into list,  id/name/install : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/settings/downloads/ad;->b:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    if-nez v0, :cond_0

    .line 374
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 367
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 378
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 379
    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/ad;->a(Ljava/lang/String;Z)V

    goto :goto_2

    .line 381
    :cond_4
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 173
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 174
    const v1, 0x7f030048

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 176
    new-instance v2, Lcom/sec/chaton/settings/downloads/af;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/settings/downloads/af;-><init>(Lcom/sec/chaton/settings/downloads/ad;Lcom/sec/chaton/settings/downloads/ae;)V

    .line 177
    const v0, 0x7f0701d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/af;->b:Landroid/widget/ImageView;

    .line 178
    const v0, 0x7f0701d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/af;->c:Landroid/widget/TextView;

    .line 180
    const v0, 0x7f0701d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/af;->d:Landroid/widget/TextView;

    .line 181
    const v0, 0x7f0701d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    .line 182
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/af;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    const v0, 0x7f0701d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/af;->f:Landroid/widget/ImageView;

    .line 186
    const v0, 0x7f0701d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/af;->g:Landroid/widget/ProgressBar;

    .line 188
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 189
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 385
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/af;

    .line 387
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ad;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 388
    iget v0, v0, Lcom/sec/chaton/settings/downloads/af;->a:I

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 390
    const-string v0, "item_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 392
    const-string v0, "name"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 393
    const-string v0, "install"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 395
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 404
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 393
    goto :goto_0

    .line 399
    :cond_1
    if-eqz v0, :cond_2

    .line 400
    invoke-direct {p0, v3, v4}, Lcom/sec/chaton/settings/downloads/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 402
    :cond_2
    invoke-direct {p0, v3, v1}, Lcom/sec/chaton/settings/downloads/ad;->a(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 415
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 416
    const-string v0, "swapCursor(), newCursor : "

    sget-object v1, Lcom/sec/chaton/settings/downloads/ad;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_0
    if-nez p1, :cond_2

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->a()V

    .line 453
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 424
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 425
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 426
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 427
    const-string v0, "item_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 428
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 430
    :goto_2
    if-eqz v0, :cond_4

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 428
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .line 436
    :cond_4
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/e;

    .line 437
    if-nez v0, :cond_5

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 443
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Lcom/sec/chaton/settings/downloads/a/l;)Z

    goto :goto_1

    .line 450
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ad;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->b()V

    goto :goto_0
.end method
