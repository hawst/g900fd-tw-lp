.class Lcom/sec/chaton/settings/dm;
.super Landroid/widget/BaseAdapter;
.source "FragmentChatView2.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/FragmentChatView2;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/FragmentChatView2;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/settings/dm;->a:Lcom/sec/chaton/settings/FragmentChatView2;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 177
    invoke-static {}, Lcom/sec/chaton/settings/dk;->values()[Lcom/sec/chaton/settings/dk;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 182
    invoke-static {}, Lcom/sec/chaton/settings/dk;->values()[Lcom/sec/chaton/settings/dk;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 187
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 192
    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings/dm;->a:Lcom/sec/chaton/settings/FragmentChatView2;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentChatView2;->a(Lcom/sec/chaton/settings/FragmentChatView2;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03012b

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 194
    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 196
    const v0, 0x7f0704be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    .line 197
    const v2, 0x7f0704c0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 199
    invoke-static {p1}, Lcom/sec/chaton/settings/dk;->a(I)Lcom/sec/chaton/settings/dk;

    move-result-object v4

    .line 200
    iget-object v3, p0, Lcom/sec/chaton/settings/dm;->a:Lcom/sec/chaton/settings/FragmentChatView2;

    invoke-static {v3}, Lcom/sec/chaton/settings/FragmentChatView2;->b(Lcom/sec/chaton/settings/FragmentChatView2;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v3, p0, Lcom/sec/chaton/settings/dm;->a:Lcom/sec/chaton/settings/FragmentChatView2;

    invoke-static {v3}, Lcom/sec/chaton/settings/FragmentChatView2;->c(Lcom/sec/chaton/settings/FragmentChatView2;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 203
    if-nez p1, :cond_0

    .line 204
    invoke-virtual {v0}, Landroid/widget/TableLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 205
    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 206
    invoke-virtual {v0, v2}, Landroid/widget/TableLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 208
    const v2, 0x7f0704bf

    invoke-virtual {v0, v2}, Landroid/widget/TableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 209
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 212
    :cond_0
    return-object v1
.end method
