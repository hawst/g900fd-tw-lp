.class public Lcom/sec/chaton/settings/BasePreferenceActivity;
.super Lcom/sec/common/actionbar/ActionBarPreferenceActivity;
.source "BasePreferenceActivity.java"

# interfaces
.implements Lcom/sec/chaton/util/cn;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/chaton/settings/BasePreferenceActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/BasePreferenceActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/common/actionbar/ActionBarPreferenceActivity;-><init>()V

    return-void
.end method

.method private showPasswordLockActivity()V
    .locals 3

    .prologue
    .line 81
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "showPasswordLockActivity"

    sget-object v1, Lcom/sec/chaton/settings/BasePreferenceActivity;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 86
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->startActivity(Landroid/content/Intent;)V

    .line 91
    :cond_1
    return-void
.end method


# virtual methods
.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 100
    const v0, 0x7f0c00fc

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 95
    const v0, 0x7f0c00fb

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 26
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 28
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 33
    invoke-virtual {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->getListView()Landroid/widget/ListView;

    move-result-object v3

    .line 34
    invoke-virtual {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 35
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 37
    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 38
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 40
    instance-of v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 41
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 42
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v1, v4, :cond_0

    move-object v1, v2

    .line 43
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 44
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 49
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 51
    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 55
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/common/actionbar/ActionBarPreferenceActivity;->onResume()V

    .line 61
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->showPasswordLockActivity()V

    .line 62
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 66
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "onUserLeaveHint"

    sget-object v1, Lcom/sec/chaton/settings/BasePreferenceActivity;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
