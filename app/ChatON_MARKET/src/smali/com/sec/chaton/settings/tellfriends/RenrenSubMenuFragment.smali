.class public Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;
.source "RenrenSubMenuFragment.java"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private h:Landroid/app/Activity;

.field private i:Landroid/view/View;

.field private j:Lcom/sec/chaton/settings/tellfriends/al;

.field private k:Lcom/sec/chaton/settings/tellfriends/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;-><init>()V

    .line 164
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/x;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/x;-><init>(Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->k:Lcom/sec/chaton/settings/tellfriends/ah;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;)Lcom/sec/chaton/settings/tellfriends/al;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->h:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->a(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->c()V

    .line 154
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->h()V

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/v;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/v;-><init>(Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/al;->a(Lcom/sec/chaton/settings/tellfriends/af;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ai;)V
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->k:Lcom/sec/chaton/settings/tellfriends/ah;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/al;->a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;Ljava/lang/String;)V

    .line 101
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->e()Landroid/widget/TextView;

    move-result-object v0

    .line 106
    const v1, 0x7f0b01fd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 107
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->f:Lcom/sec/chaton/settings/tellfriends/ad;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/al;->a(Lcom/sec/chaton/settings/tellfriends/ad;)V

    .line 162
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 70
    const/16 v0, 0xbb9

    if-ne p1, v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 78
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/al;

    invoke-direct {v0, p1}, Lcom/sec/chaton/settings/tellfriends/al;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    .line 43
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onAttach(Landroid/app/Activity;)V

    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->h:Landroid/app/Activity;

    .line 45
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 93
    :goto_0
    return-void

    .line 84
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->h:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 88
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->h:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/tellfriends/RenrenMessageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    const-string v1, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 82
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f07029d -> :sswitch_0
        0x7f070457 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->i:Landroid/view/View;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->i:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->a()V

    .line 62
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->g()V

    .line 63
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDestroy()V

    .line 64
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDetach()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->h:Landroid/app/Activity;

    .line 51
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onPause()V

    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuFragment;->i()V

    .line 57
    return-void
.end method
