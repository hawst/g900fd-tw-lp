.class Lcom/sec/chaton/settings/downloads/ah;
.super Landroid/os/Handler;
.source "AniconPackageDetail.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 132
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/e;

    .line 134
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 161
    :goto_0
    return-void

    .line 137
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 138
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "PackageId: "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    const-string v2, ". Status: "

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 146
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 151
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Lcom/sec/chaton/settings/downloads/a/e;)Lcom/sec/chaton/settings/downloads/a/e;

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->a(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 158
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ah;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AniconPackageDetail;->b(Lcom/sec/chaton/settings/downloads/AniconPackageDetail;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/e;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
