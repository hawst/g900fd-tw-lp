.class public Lcom/sec/chaton/settings/DeleteAccountPreference;
.super Landroid/preference/Preference;
.source "DeleteAccountPreference.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:Landroid/support/v4/app/FragmentManager;

.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/chaton/settings/DeleteAccountPreference;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/DeleteAccountPreference;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a()Landroid/support/v4/app/FragmentManager;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/chaton/settings/DeleteAccountPreference;->a:Landroid/support/v4/app/FragmentManager;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/chaton/settings/DeleteAccountPreference;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 72
    const v0, 0x7f07050a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 73
    new-instance v1, Lcom/sec/chaton/settings/da;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/da;-><init>(Lcom/sec/chaton/settings/DeleteAccountPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-void
.end method
