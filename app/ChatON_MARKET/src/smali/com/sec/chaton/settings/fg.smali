.class Lcom/sec/chaton/settings/fg;
.super Landroid/os/Handler;
.source "SuggestionsActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/SuggestionsActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/SuggestionsActivity;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 126
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 128
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 129
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_4

    .line 130
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPrivacyList;

    .line 131
    if-eqz v0, :cond_0

    .line 134
    iget-object v2, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPrivacyList;->privacy:Ljava/util/ArrayList;

    invoke-static {v2, v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Lcom/sec/chaton/settings/SuggestionsActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 135
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->c(Lcom/sec/chaton/settings/SuggestionsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 136
    iget-object v2, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->c(Lcom/sec/chaton/settings/SuggestionsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->c(Lcom/sec/chaton/settings/SuggestionsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->_value:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Lcom/sec/chaton/settings/SuggestionsActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 143
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 146
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-static {v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/fg;->a:Lcom/sec/chaton/settings/SuggestionsActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x142
        :pswitch_0
    .end packed-switch
.end method
