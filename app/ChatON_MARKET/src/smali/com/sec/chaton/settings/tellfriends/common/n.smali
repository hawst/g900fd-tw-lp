.class public final enum Lcom/sec/chaton/settings/tellfriends/common/n;
.super Ljava/lang/Enum;
.source "SnsFriendsPickerFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/tellfriends/common/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/tellfriends/common/n;

.field public static final enum b:Lcom/sec/chaton/settings/tellfriends/common/n;

.field public static final enum c:Lcom/sec/chaton/settings/tellfriends/common/n;

.field public static final enum d:Lcom/sec/chaton/settings/tellfriends/common/n;

.field public static final enum e:Lcom/sec/chaton/settings/tellfriends/common/n;

.field private static final synthetic f:[Lcom/sec/chaton/settings/tellfriends/common/n;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/n;

    const-string v1, "NONE_TYPE"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->a:Lcom/sec/chaton/settings/tellfriends/common/n;

    .line 49
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/n;

    const-string v1, "FACEBOOK_TYPE"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/settings/tellfriends/common/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->b:Lcom/sec/chaton/settings/tellfriends/common/n;

    .line 50
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/n;

    const-string v1, "TWITTER_TYPE"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/settings/tellfriends/common/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->c:Lcom/sec/chaton/settings/tellfriends/common/n;

    .line 51
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/n;

    const-string v1, "WEIBO_TYPE"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/settings/tellfriends/common/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->d:Lcom/sec/chaton/settings/tellfriends/common/n;

    .line 52
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/common/n;

    const-string v1, "RENREN_TYPE"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/settings/tellfriends/common/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->e:Lcom/sec/chaton/settings/tellfriends/common/n;

    .line 47
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/settings/tellfriends/common/n;

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/n;->a:Lcom/sec/chaton/settings/tellfriends/common/n;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/n;->b:Lcom/sec/chaton/settings/tellfriends/common/n;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/n;->c:Lcom/sec/chaton/settings/tellfriends/common/n;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/n;->d:Lcom/sec/chaton/settings/tellfriends/common/n;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/common/n;->e:Lcom/sec/chaton/settings/tellfriends/common/n;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->f:[Lcom/sec/chaton/settings/tellfriends/common/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/tellfriends/common/n;
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/chaton/settings/tellfriends/common/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/common/n;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/tellfriends/common/n;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/n;->f:[Lcom/sec/chaton/settings/tellfriends/common/n;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/tellfriends/common/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/tellfriends/common/n;

    return-object v0
.end method
