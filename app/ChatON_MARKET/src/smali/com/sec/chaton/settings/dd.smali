.class Lcom/sec/chaton/settings/dd;
.super Landroid/os/Handler;
.source "DeleteAccountPreference.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/chaton/settings/dd;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 233
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 234
    const-string v0, "Push deregistration is success. execute chaton deregistration."

    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/settings/dd;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/dd;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->a(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 241
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 243
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01e2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 268
    :goto_0
    return-void

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/dd;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    iget-object v1, p0, Lcom/sec/chaton/settings/dd;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->b(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/ap;->a()Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->a(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;

    goto :goto_0

    .line 250
    :cond_2
    const-string v0, "Push deregistration is fail."

    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/settings/dd;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 253
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/dd;->a:Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;->a(Lcom/sec/chaton/settings/DeleteAccountPreference$DeleteWorkFragment;)Lcom/sec/chaton/settings/DeleteAccountPreference$ProgressDialogFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 254
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 258
    :cond_3
    invoke-static {}, Lcom/sec/chaton/settings/DeleteAccountPreference;->a()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0b016c

    const v2, 0x7f0b002d

    const v3, 0x7f0b0037

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;->a(III)Lcom/sec/chaton/settings/DeleteAccountPreference$ErrorDialogFragment;

    move-result-object v1

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method
