.class public Lcom/sec/chaton/settings/downloads/u;
.super Ljava/lang/Object;
.source "AniconHelper.java"


# static fields
.field public static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/util/Random;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/settings/downloads/ab;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/settings/downloads/ac;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/io/FilenameFilter;

.field private static final g:Ljava/io/FilenameFilter;

.field private static final h:Ljava/io/FilenameFilter;

.field private static final i:Ljava/io/FilenameFilter;

.field private static final j:Ljava/io/FilenameFilter;

.field private static k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    const-class v0, Lcom/sec/chaton/settings/downloads/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    .line 82
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->c:Ljava/util/Random;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->d:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->e:Ljava/util/List;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    .line 92
    new-instance v0, Lcom/sec/chaton/settings/downloads/v;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/v;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->f:Ljava/io/FilenameFilter;

    .line 111
    new-instance v0, Lcom/sec/chaton/settings/downloads/w;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/w;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->g:Ljava/io/FilenameFilter;

    .line 121
    new-instance v0, Lcom/sec/chaton/settings/downloads/x;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/x;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->h:Ljava/io/FilenameFilter;

    .line 131
    new-instance v0, Lcom/sec/chaton/settings/downloads/y;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/y;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->i:Ljava/io/FilenameFilter;

    .line 143
    new-instance v0, Lcom/sec/chaton/settings/downloads/aa;

    const-string v1, "0"

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/downloads/aa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/u;->j:Ljava/io/FilenameFilter;

    .line 958
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 960
    new-array v1, v4, [Ljava/lang/Class;

    const-class v2, [B

    aput-object v2, v1, v3

    .line 963
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "vibrateImmVibe"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 964
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/settings/downloads/u;->k:Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 968
    :goto_0
    return-void

    .line 965
    :catch_0
    move-exception v0

    .line 966
    sput-boolean v3, Lcom/sec/chaton/settings/downloads/u;->k:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1029
    return-void
.end method

.method public static a()I
    .locals 3

    .prologue
    .line 831
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_anicon_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/drawable/AnimationDrawable;
    .locals 2

    .prologue
    .line 326
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    const/4 v0, 0x0

    .line 332
    :goto_0
    return-object v0

    .line 330
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 332
    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->f:Ljava/io/FilenameFilter;

    invoke-static {p0, v0, v1, p2, p3}, Lcom/sec/chaton/util/b;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/FilenameFilter;II)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/util/d;",
            ">;)",
            "Landroid/graphics/drawable/AnimationDrawable;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 336
    new-instance v1, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    .line 338
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/d;

    .line 339
    invoke-virtual {v0}, Lcom/sec/chaton/util/d;->b()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/util/d;->a()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    .line 342
    :cond_0
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/AnimationDrawable;->selectDrawable(I)Z

    .line 343
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    .line 345
    return-object v1
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 768
    invoke-static {p0}, Lcom/sec/common/util/l;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 770
    new-instance v1, Ljava/io/File;

    const-string v2, "anicon"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 773
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 774
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 777
    :cond_0
    return-object v1
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 666
    const-class v3, Lcom/sec/chaton/settings/downloads/u;

    monitor-enter v3

    .line 667
    const/4 v2, 0x0

    .line 671
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 672
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 673
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v4, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    const-string v7, " is already cached."

    aput-object v7, v5, v6

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 703
    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 704
    invoke-static {v2}, Lcom/sec/common/util/l;->a(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 708
    :cond_1
    :goto_0
    monitor-exit v3

    return-object v0

    .line 680
    :cond_2
    :try_start_2
    new-instance v2, Ljava/io/File;

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/settings/downloads/u;->c:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 683
    :try_start_3
    invoke-static {p0, p2, v2}, Lcom/sec/common/util/r;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 686
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    .line 689
    :try_start_4
    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 690
    new-instance v1, Ljava/io/IOException;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Can\'t rename directory."

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 692
    :catch_0
    move-exception v1

    move-object v8, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v8

    .line 693
    :goto_1
    :try_start_5
    sget-object v4, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v4, v4, Lcom/sec/common/c/a;->e:Z

    if-eqz v4, :cond_3

    .line 694
    sget-object v4, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v5, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 697
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 698
    invoke-static {v2}, Lcom/sec/common/util/l;->a(Ljava/io/File;)V

    .line 701
    :cond_4
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 703
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_5

    :try_start_6
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 704
    invoke-static {v1}, Lcom/sec/common/util/l;->a(Ljava/io/File;)V

    .line 703
    :cond_5
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 666
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    .line 703
    :cond_6
    if-eqz v2, :cond_1

    :try_start_7
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 704
    invoke-static {v2}, Lcom/sec/common/util/l;->a(Ljava/io/File;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 703
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 692
    :catch_1
    move-exception v0

    move-object v2, v1

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 805
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 524
    const-class v7, Lcom/sec/chaton/settings/downloads/u;

    monitor-enter v7

    :try_start_0
    invoke-static {p0, p1, p3}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 527
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 528
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 529
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_anicon, installPackage, unzip from : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download_anicon, installPackage, unzip to : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    :cond_0
    invoke-static {p0, p2, v0}, Lcom/sec/common/util/r;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536
    const/4 v0, 0x2

    :try_start_1
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "package_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "=?"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 537
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 539
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "anicon_id"

    aput-object v8, v2, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    .line 541
    if-eqz v1, :cond_4

    .line 542
    :cond_1
    :goto_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 543
    const-string v0, "anicon_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 546
    :try_start_3
    invoke-static {p0, v0}, Lcom/sec/chaton/settings/downloads/u;->j(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 547
    :catch_0
    move-exception v0

    .line 548
    :try_start_4
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_1

    .line 549
    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 556
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_2

    .line 557
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 556
    :cond_2
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 574
    :catch_1
    move-exception v0

    .line 575
    :try_start_6
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_3

    .line 576
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 579
    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 524
    :catchall_1
    move-exception v0

    monitor-exit v7

    throw v0

    .line 556
    :cond_4
    if-eqz v1, :cond_5

    .line 557
    :try_start_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 563
    :cond_5
    :try_start_8
    const-string v0, "com.sec.chaton.provider2"

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {}, Lcom/sec/common/util/i;->a()J

    move-result-wide v2

    invoke-static {v1, p1, v2, v3}, Lcom/sec/chaton/e/a/j;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;J)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 571
    :try_start_9
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/ab;

    .line 572
    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/downloads/ab;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 564
    :catch_2
    move-exception v0

    .line 565
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 566
    :catch_3
    move-exception v0

    .line 567
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 581
    :cond_6
    monitor-exit v7

    return-void

    .line 556
    :catchall_2
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public static a(Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 976
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 978
    const/4 v1, 0x0

    const v2, 0x7f09022a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 979
    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 980
    return-void
.end method

.method public static a(Lcom/sec/chaton/settings/downloads/ab;)V
    .locals 1

    .prologue
    .line 1004
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1005
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    :cond_0
    return-void
.end method

.method public static a(Lcom/sec/chaton/settings/downloads/ac;)V
    .locals 1

    .prologue
    .line 1014
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->e:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1015
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->e:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1017
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 308
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 310
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 318
    :cond_0
    :goto_0
    return v0

    .line 314
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v1, v1

    if-eqz v1, :cond_0

    .line 318
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 785
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 791
    :cond_0
    :goto_0
    return v0

    .line 788
    :cond_1
    const-string v1, "file/download/anicon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 789
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 2

    .prologue
    .line 322
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v1

    invoke-static {p0, p1, v0, v1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    .locals 8

    .prologue
    .line 721
    const-class v2, Lcom/sec/chaton/settings/downloads/u;

    monitor-enter v2

    const/4 v1, 0x0

    .line 724
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, ".panel"

    aput-object v6, v4, v5

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 726
    :try_start_1
    invoke-virtual {p2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    .line 727
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_0

    .line 728
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download_anicon, savePackagePanelImage, rename result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download_anicon, savePackagePanelImage, from cache : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download_anicon, savePackagePanelImage, to storage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :cond_0
    if-nez v1, :cond_1

    .line 734
    new-instance v1, Ljava/io/IOException;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Can\'t rename anicon panel file."

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 736
    :catch_0
    move-exception v1

    .line 737
    :goto_0
    :try_start_2
    sget-object v3, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v3, v3, Lcom/sec/common/c/a;->e:Z

    if-eqz v3, :cond_1

    .line 738
    sget-object v3, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v4, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 742
    :cond_1
    monitor-exit v2

    return-object v0

    .line 721
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 736
    :catch_1
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/util/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 355
    :goto_0
    return-object v0

    .line 353
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 355
    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->f:Ljava/io/FilenameFilter;

    invoke-static {p0, v0, v1, p2, p3}, Lcom/sec/chaton/util/b;->b(Landroid/content/Context;Ljava/io/File;Ljava/io/FilenameFilter;II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 983
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 985
    const/4 v1, 0x0

    const v2, 0x7f09022a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 986
    const v1, 0x7f08004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 987
    return-void
.end method

.method public static b(Lcom/sec/chaton/settings/downloads/ab;)V
    .locals 1

    .prologue
    .line 1010
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1011
    return-void
.end method

.method public static b(Lcom/sec/chaton/settings/downloads/ac;)V
    .locals 1

    .prologue
    .line 1020
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->e:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1021
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 971
    sget-boolean v0, Lcom/sec/chaton/settings/downloads/u;->k:Z

    return v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 795
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 797
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 798
    :cond_0
    const/4 v0, 0x0

    .line 801
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 359
    const/4 v1, 0x0

    .line 361
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 364
    if-eqz v0, :cond_0

    .line 366
    :try_start_0
    invoke-static {p0, v0, p2, p3}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 367
    const/16 v0, 0xa0

    invoke-virtual {v2, v0}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 369
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 370
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    :goto_0
    return-object v0

    .line 374
    :catch_0
    move-exception v0

    .line 375
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 376
    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 386
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 389
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-object v0

    .line 394
    :cond_1
    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->i:Ljava/io/FilenameFilter;

    invoke-virtual {v2, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 398
    if-eqz v1, :cond_2

    array-length v3, v1

    if-nez v3, :cond_3

    .line 399
    :cond_2
    new-array v1, v6, [Ljava/io/File;

    .line 402
    const/4 v3, 0x0

    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->j(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    aput-object v4, v1, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    array-length v3, v1

    if-nez v3, :cond_5

    .line 412
    :cond_4
    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->j:Ljava/io/FilenameFilter;

    invoke-virtual {v2, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 414
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    array-length v2, v1

    if-eqz v2, :cond_5

    .line 415
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "AniconId: "

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    const/4 v3, 0x2

    const-string v4, ", Can\'t find thumbnail image using alternative image(first frame image)."

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_5
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eqz v2, :cond_0

    .line 424
    aget-object v0, v1, v5

    goto :goto_0

    .line 403
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 404
    goto :goto_1
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 809
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 819
    :cond_0
    :goto_0
    return-object v0

    .line 813
    :cond_1
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 815
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 819
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 990
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 992
    const/4 v1, 0x0

    const v2, 0x7f09022a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 993
    const v1, 0x7f080013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 994
    return-void
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/drawable/BitmapDrawable;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 498
    const/4 v1, 0x0

    .line 500
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const-string v4, ".panel"

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 502
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 504
    :try_start_0
    invoke-static {p0, v0, p2, p3}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 505
    const/16 v0, 0xa0

    invoke-virtual {v2, v0}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 506
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 507
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    :goto_0
    return-object v0

    .line 511
    :catch_0
    move-exception v0

    .line 512
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 513
    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 431
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 434
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-object v0

    .line 439
    :cond_1
    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->h:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 441
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eqz v2, :cond_0

    .line 445
    const/4 v0, 0x0

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method public static d(Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 997
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 999
    const/4 v1, 0x0

    const v2, 0x7f09022a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1000
    const v1, 0x7f080013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1001
    return-void
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 452
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 453
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 455
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-object v0

    .line 460
    :cond_1
    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->g:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 462
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eqz v2, :cond_0

    .line 466
    const/4 v0, 0x0

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method public static declared-synchronized f(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 584
    const-class v10, Lcom/sec/chaton/settings/downloads/u;

    monitor-enter v10

    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 585
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Uninstall anicon package. "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    :cond_0
    sget-object v0, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {v0}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 595
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v8

    .line 597
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 598
    const-string v0, "expiration_time"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 600
    invoke-static {}, Lcom/sec/common/util/i;->a()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    move v9, v7

    .line 608
    :cond_1
    :goto_0
    if-eqz v8, :cond_2

    .line 609
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 614
    :cond_2
    :try_start_4
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->g(Landroid/content/Context;Ljava/lang/String;)V

    .line 617
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "anicon_id"

    aput-object v5, v4, v0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "package_id"

    aput-object v6, v0, v5

    const/4 v5, 0x1

    const-string v6, "=?"

    aput-object v6, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v6, v0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 619
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 620
    const-string v0, "anicon_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 622
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_3

    .line 623
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Delete anicon. "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    :cond_3
    invoke-static {p0, v0}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/l;->a(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 652
    :catch_0
    move-exception v0

    .line 653
    :try_start_5
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_4

    .line 654
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 657
    :cond_4
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 659
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_5

    .line 660
    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 659
    :cond_5
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 584
    :catchall_1
    move-exception v0

    monitor-exit v10

    throw v0

    .line 608
    :catchall_2
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_6

    .line 609
    :try_start_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 608
    :cond_6
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 630
    :cond_7
    if-eqz v9, :cond_9

    .line 631
    :try_start_8
    const-string v0, "com.sec.chaton.provider2"

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;

    .line 633
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 634
    const-string v0, "Anicon package is expired. Delete from database."

    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 647
    :cond_8
    :goto_3
    :try_start_9
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/ac;

    .line 648
    invoke-interface {v0, p1}, Lcom/sec/chaton/settings/downloads/ac;->b(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    .line 638
    :cond_9
    :try_start_a
    const-string v0, "com.sec.chaton.provider2"

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {v1, p1}, Lcom/sec/chaton/e/a/j;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_3

    .line 640
    :catch_1
    move-exception v0

    .line 641
    :try_start_b
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 642
    :catch_2
    move-exception v0

    .line 643
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 659
    :cond_a
    if-eqz v8, :cond_b

    .line 660
    :try_start_c
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 651
    :cond_b
    monitor-exit v10

    return v9

    .line 608
    :catchall_3
    move-exception v0

    move-object v1, v8

    goto :goto_2

    :cond_c
    move v9, v7

    goto/16 :goto_0
.end method

.method public static declared-synchronized g(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 753
    const-class v2, Lcom/sec/chaton/settings/downloads/u;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    const-string v7, ".panel"

    aput-object v7, v5, v6

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 756
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 758
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 761
    :goto_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 762
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download_anicon, removePackagePanelImage, [deleteExist: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], panelImageDir : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 765
    :cond_0
    monitor-exit v2

    return-void

    .line 753
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 781
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 918
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 919
    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->j:Ljava/io/FilenameFilter;

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 921
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 922
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    .line 952
    :goto_0
    return-object v0

    .line 925
    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 926
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 928
    if-eqz v0, :cond_2

    array-length v1, v0

    if-gt v1, v2, :cond_3

    .line 930
    :cond_2
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 933
    :cond_3
    aget-object v1, v0, v2

    .line 934
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/z;->a()I

    move-result v0

    .line 936
    if-nez v1, :cond_4

    .line 937
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 940
    :cond_4
    const-string v2, "h"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/sec/chaton/settings/downloads/u;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 941
    sget-object v2, Lcom/sec/chaton/settings/downloads/z;->b:Lcom/sec/chaton/settings/downloads/z;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/z;->a()I

    move-result v2

    add-int/2addr v0, v2

    .line 944
    :cond_5
    const-string v2, "s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 945
    sget-object v2, Lcom/sec/chaton/settings/downloads/z;->c:Lcom/sec/chaton/settings/downloads/z;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/downloads/z;->a()I

    move-result v2

    add-int/2addr v0, v2

    .line 948
    :cond_6
    const-string v2, "m"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 949
    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->d:Lcom/sec/chaton/settings/downloads/z;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/z;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 952
    :cond_7
    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/z;->a(I)Lcom/sec/chaton/settings/downloads/z;

    move-result-object v0

    goto :goto_0
.end method

.method private static j(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 6

    .prologue
    const/4 v3, 0x6

    const/4 v5, 0x0

    .line 839
    invoke-static {p0, p1}, Lcom/sec/chaton/settings/downloads/u;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 840
    sget-object v1, Lcom/sec/chaton/settings/downloads/u;->j:Ljava/io/FilenameFilter;

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 842
    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    .line 843
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t find first frame file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 846
    :cond_1
    aget-object v1, v1, v5

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 847
    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 849
    if-eqz v1, :cond_2

    array-length v2, v1

    if-gt v2, v3, :cond_3

    .line 850
    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t shortcut index."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 853
    :cond_3
    aget-object v1, v1, v3

    .line 855
    new-instance v2, Lcom/sec/chaton/settings/downloads/aa;

    invoke-direct {v2, v1}, Lcom/sec/chaton/settings/downloads/aa;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 856
    if-eqz v1, :cond_4

    array-length v2, v1

    if-nez v2, :cond_5

    .line 857
    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t find target frame file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 860
    :cond_5
    aget-object v2, v1, v5

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".png"

    const-string v4, ".th.png"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 861
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 863
    aget-object v0, v1, v5

    invoke-virtual {v0, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 864
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t rename shortcut file name."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 867
    :cond_6
    return-object v3
.end method
