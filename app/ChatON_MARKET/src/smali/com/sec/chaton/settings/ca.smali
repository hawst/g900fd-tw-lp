.class Lcom/sec/chaton/settings/ca;
.super Ljava/lang/Object;
.source "ActivityRecommendation.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityRecommendation;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityRecommendation;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/chaton/settings/ca;->a:Lcom/sec/chaton/settings/ActivityRecommendation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    if-eqz p2, :cond_0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings/ca;->a:Lcom/sec/chaton/settings/ActivityRecommendation;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/ActivityRecommendation;->a(Lcom/sec/chaton/settings/ActivityRecommendation;Z)Z

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/settings/ca;->a:Lcom/sec/chaton/settings/ActivityRecommendation;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityRecommendation;->a(Lcom/sec/chaton/settings/ActivityRecommendation;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_normal"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 113
    :goto_0
    return v3

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ca;->a:Lcom/sec/chaton/settings/ActivityRecommendation;

    invoke-static {v0, v2}, Lcom/sec/chaton/settings/ActivityRecommendation;->a(Lcom/sec/chaton/settings/ActivityRecommendation;Z)Z

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/settings/ca;->a:Lcom/sec/chaton/settings/ActivityRecommendation;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityRecommendation;->a(Lcom/sec/chaton/settings/ActivityRecommendation;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_normal"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
