.class Lcom/sec/chaton/settings/o;
.super Ljava/lang/Object;
.source "ActivityChat.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityChat;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityChat;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const v4, 0x7f08001b

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityChat;->c(Lcom/sec/chaton/settings/ActivityChat;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    if-nez v0, :cond_0

    .line 292
    const/4 v0, 0x0

    .line 303
    :goto_0
    return v0

    .line 295
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 296
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_1

    .line 297
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->c()Z

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    iget-object v1, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-static {v2}, Lcom/sec/chaton/settings/ActivityChat;->c(Lcom/sec/chaton/settings/ActivityChat;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-virtual {v3}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/settings/ActivityChat;->a(Lcom/sec/chaton/settings/ActivityChat;Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 303
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 300
    :cond_1
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->d()Z

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    iget-object v1, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-static {v2}, Lcom/sec/chaton/settings/ActivityChat;->c(Lcom/sec/chaton/settings/ActivityChat;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings/o;->a:Lcom/sec/chaton/settings/ActivityChat;

    invoke-virtual {v3}, Lcom/sec/chaton/settings/ActivityChat;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/settings/ActivityChat;->a(Lcom/sec/chaton/settings/ActivityChat;Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_1
.end method
