.class public Lcom/sec/chaton/settings/downloads/SoundDetail;
.super Landroid/support/v4/app/Fragment;
.source "SoundDetail.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Ljava/text/DateFormat;

.field b:Lcom/sec/chaton/settings/downloads/bt;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Z

.field private i:I

.field private j:I

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/ProgressBar;

.field private q:Lcom/sec/chaton/d/bc;

.field private r:Lcom/sec/chaton/settings/downloads/a/v;

.field private s:Landroid/database/Cursor;

.field private t:Z

.field private u:Landroid/view/Menu;

.field private v:Landroid/os/Handler;

.field private w:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->g:J

    .line 97
    new-instance v0, Lcom/sec/chaton/settings/downloads/cu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/cu;-><init>(Lcom/sec/chaton/settings/downloads/SoundDetail;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->v:Landroid/os/Handler;

    .line 200
    new-instance v0, Lcom/sec/chaton/settings/downloads/cv;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/cv;-><init>(Lcom/sec/chaton/settings/downloads/SoundDetail;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->w:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->k:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SoundDetail;Lcom/sec/chaton/settings/downloads/a/v;)Lcom/sec/chaton/settings/downloads/a/v;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v6, 0x3

    const/4 v9, 0x2

    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 464
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 465
    const-string v0, "download_sound, show detail, redraw()"

    sget-object v3, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    const-wide/16 v3, 0x0

    .line 471
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 472
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 475
    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 476
    const-string v0, "extras"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 477
    const/4 v0, 0x0

    .line 480
    :try_start_0
    invoke-static {v4}, Lcom/sec/chaton/e/a/ad;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ae;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 487
    :cond_1
    :goto_0
    iput-object v3, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->f:Ljava/lang/String;

    .line 488
    if-eqz v0, :cond_2

    .line 491
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/ae;->a()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->g:J

    .line 494
    :cond_2
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 495
    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->h:Z

    .line 496
    const-string v0, "new"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->i:I

    .line 497
    const-string v0, "special"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->j:I

    .line 501
    const-string v0, "expiration_time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 504
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->l:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->f:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 507
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 509
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->h:Z

    if-eqz v0, :cond_6

    .line 511
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    const v3, 0x7f0200ab

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 514
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 515
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 521
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 524
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v2, "KB)"

    aput-object v2, v3, v9

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 525
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->b(Landroid/widget/TextView;)V

    .line 602
    :goto_3
    iget v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->j:I

    if-eqz v0, :cond_b

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 605
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    const v1, 0x7f0b01dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 606
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    const-string v1, "#3eb1b9"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 628
    :goto_4
    return-void

    .line 481
    :catch_0
    move-exception v4

    .line 482
    sget-boolean v5, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v5, :cond_1

    .line 483
    sget-object v5, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 495
    goto/16 :goto_1

    .line 518
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_2

    .line 529
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    if-nez v0, :cond_7

    .line 535
    new-instance v0, Ljava/sql/Date;

    invoke-direct {v0, v3, v4}, Ljava/sql/Date;-><init>(J)V

    .line 536
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 538
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    const v3, 0x7f0200ac

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 539
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v2, "KB)"

    aput-object v2, v3, v9

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 542
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/TextView;)V

    goto :goto_3

    .line 545
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v0

    const/4 v5, 0x5

    if-ne v0, v5, :cond_8

    .line 546
    new-instance v0, Ljava/sql/Date;

    invoke-direct {v0, v3, v4}, Ljava/sql/Date;-><init>(J)V

    .line 547
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->a:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    const v3, 0x7f0200ad

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v2, "KB)"

    aput-object v2, v3, v9

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 557
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_3

    .line 560
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 563
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-nez v0, :cond_9

    .line 564
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 567
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    const v4, 0x7f0b022a

    invoke-virtual {p0, v4}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v2, ")"

    aput-object v2, v3, v9

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 575
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 578
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 579
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 581
    :cond_a
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 583
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->g:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v2, "KB)"

    aput-object v2, v3, v9

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 590
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 591
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 593
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    const v2, 0x7f0b022b

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 594
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/dg;->d(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 607
    :cond_b
    iget v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->i:I

    if-eqz v0, :cond_c

    .line 608
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    const v1, 0x7f0b01dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 611
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    const-string v1, "#e86d00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto/16 :goto_4

    .line 613
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 558
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SoundDetail;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->b:Lcom/sec/chaton/settings/downloads/bt;

    if-nez v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    new-instance v0, Lcom/sec/chaton/multimedia/audio/d;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/chaton/multimedia/audio/d;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 188
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->b:Lcom/sec/chaton/settings/downloads/bt;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/bt;->b(Lcom/sec/chaton/multimedia/audio/d;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->s:Landroid/database/Cursor;

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->b:Lcom/sec/chaton/settings/downloads/bt;

    if-nez v0, :cond_0

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    new-instance v0, Lcom/sec/chaton/multimedia/audio/d;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/sec/chaton/multimedia/audio/d;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Z)V

    .line 197
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->b:Lcom/sec/chaton/settings/downloads/bt;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/bt;->b(Lcom/sec/chaton/multimedia/audio/d;)Z

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/SoundDetail;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/SoundDetail;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 180
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0229

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 181
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 428
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->s:Landroid/database/Cursor;

    .line 431
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->t:Z

    if-eqz v0, :cond_0

    .line 432
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->t:Z

    .line 435
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/v;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->a(Landroid/os/Handler;)V

    .line 442
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Landroid/database/Cursor;)V

    .line 443
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 122
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "download_sound, requestPlaySample()"

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 129
    const-string v0, "download_sound, requestPlaySample(), itemid is (empty)"

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_1
    :goto_0
    return-void

    .line 135
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/downloads/dg;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 138
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 140
    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/net/URI;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    .line 157
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 158
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 159
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "download_sound, requestPlaySample(), play installed file : "

    aput-object v2, v1, v4

    aput-object p1, v1, v3

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    :try_start_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_4

    .line 143
    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 145
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a()V
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 148
    :catch_1
    move-exception v0

    .line 149
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_5

    .line 150
    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 152
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a()V

    goto :goto_0

    .line 165
    :cond_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 166
    new-array v0, v2, [Ljava/lang/Object;

    const-string v1, "download_sound, requestPlaySample(), play download file : "

    aput-object v1, v0, v4

    aput-object p1, v0, v3

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v3, :cond_9

    .line 169
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 170
    const-string v0, "download_sound, requestPlaySample(), sample url is (empty)"

    sget-object v1, Lcom/sec/chaton/settings/downloads/SoundDetail;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a()V

    goto/16 :goto_0

    .line 175
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->e:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->h:Z

    if-eqz v0, :cond_0

    .line 357
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 358
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/cx;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/cx;-><init>(Lcom/sec/chaton/settings/downloads/SoundDetail;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    :goto_0
    return-void

    .line 374
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 385
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->h:Z

    if-eqz v0, :cond_2

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/dg;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 387
    if-eqz v0, :cond_1

    .line 388
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->g(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 389
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 412
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->s:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 391
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->h(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 394
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    if-nez v0, :cond_3

    .line 395
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->q:Lcom/sec/chaton/d/bc;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/v;-><init>(Lcom/sec/chaton/d/bc;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->a(Landroid/os/Handler;)V

    .line 397
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 398
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 399
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->q:Lcom/sec/chaton/d/bc;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/v;-><init>(Lcom/sec/chaton/d/bc;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->a(Landroid/os/Handler;)V

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 403
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/v;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 407
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->a(Z)Z

    goto :goto_1

    .line 374
    :pswitch_data_0
    .packed-switch 0x7f0701d2
        :pswitch_0
    .end packed-switch

    .line 403
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 239
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 242
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ITEM_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    .line 245
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SAMPLE_URL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->e:Ljava/lang/String;

    .line 246
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 420
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v2

    const-string v1, "=?"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 421
    new-array v5, v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    aput-object v0, v5, v2

    .line 423
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const v1, 0x7f0705ab

    .line 300
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 302
    const v0, 0x7f0f0027

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 304
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->u:Landroid/view/Menu;

    .line 305
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->h:Z

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->u:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 310
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->u:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 252
    const v0, 0x7f0300ff

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 254
    sget-object v0, Lcom/sec/chaton/settings/downloads/dl;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->v:Landroid/os/Handler;

    invoke-static {v0, v2}, Lcom/sec/chaton/settings/downloads/bt;->a(Ljava/lang/String;Landroid/os/Handler;)Lcom/sec/chaton/settings/downloads/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->b:Lcom/sec/chaton/settings/downloads/bt;

    .line 255
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->a:Ljava/text/DateFormat;

    .line 256
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v5}, Lcom/sec/chaton/d/bc;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->q:Lcom/sec/chaton/d/bc;

    .line 258
    const v0, 0x7f0701d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->k:Landroid/widget/ImageView;

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->k:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/chaton/settings/downloads/cw;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/cw;-><init>(Lcom/sec/chaton/settings/downloads/SoundDetail;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->b:Lcom/sec/chaton/settings/downloads/bt;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/downloads/bt;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/audio/f;

    move-result-object v0

    .line 267
    if-nez v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->k:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    invoke-static {v0, v2}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/ImageView;Lcom/sec/chaton/multimedia/audio/f;)Z

    .line 274
    :goto_0
    const v0, 0x7f0701d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->l:Landroid/widget/TextView;

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->l:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 278
    const v0, 0x7f0701d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->m:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 282
    const v0, 0x7f0701d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    const v0, 0x7f0701d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->o:Landroid/widget/TextView;

    .line 287
    const v0, 0x7f0701d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->p:Landroid/widget/ProgressBar;

    .line 290
    iput-boolean v3, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->t:Z

    .line 291
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 293
    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/downloads/SoundDetail;->setHasOptionsMenu(Z)V

    .line 295
    return-object v1

    .line 271
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->k:Landroid/widget/ImageView;

    invoke-static {v2, v0}, Lcom/sec/chaton/settings/downloads/dg;->a(Landroid/widget/ImageView;Lcom/sec/chaton/multimedia/audio/f;)Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 340
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->r:Lcom/sec/chaton/settings/downloads/a/v;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/v;->b(Landroid/os/Handler;)V

    .line 346
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/dl;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SoundDetail;->v:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/bt;->b(Ljava/lang/String;Landroid/os/Handler;)V

    .line 348
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 349
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 66
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/SoundDetail;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 448
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 314
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 320
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 317
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->b()V

    goto :goto_0

    .line 314
    :pswitch_data_0
    .packed-switch 0x7f0705ab
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 333
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 335
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->stopLoading()V

    .line 336
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 326
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 328
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SoundDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->startLoading()V

    .line 329
    return-void
.end method
