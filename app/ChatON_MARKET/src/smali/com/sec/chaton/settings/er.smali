.class public final enum Lcom/sec/chaton/settings/er;
.super Ljava/lang/Enum;
.source "RingtonePreference2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/er;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/er;

.field public static final enum b:Lcom/sec/chaton/settings/er;

.field public static final enum c:Lcom/sec/chaton/settings/er;

.field private static final synthetic d:[Lcom/sec/chaton/settings/er;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 378
    new-instance v0, Lcom/sec/chaton/settings/er;

    const-string v1, "SYSTEM"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/er;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/er;->a:Lcom/sec/chaton/settings/er;

    .line 379
    new-instance v0, Lcom/sec/chaton/settings/er;

    const-string v1, "EMBED"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/settings/er;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/er;->b:Lcom/sec/chaton/settings/er;

    .line 380
    new-instance v0, Lcom/sec/chaton/settings/er;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/settings/er;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/er;->c:Lcom/sec/chaton/settings/er;

    .line 377
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/settings/er;

    sget-object v1, Lcom/sec/chaton/settings/er;->a:Lcom/sec/chaton/settings/er;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/settings/er;->b:Lcom/sec/chaton/settings/er;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/settings/er;->c:Lcom/sec/chaton/settings/er;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/chaton/settings/er;->d:[Lcom/sec/chaton/settings/er;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 377
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/er;
    .locals 1

    .prologue
    .line 377
    const-class v0, Lcom/sec/chaton/settings/er;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/er;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/er;
    .locals 1

    .prologue
    .line 377
    sget-object v0, Lcom/sec/chaton/settings/er;->d:[Lcom/sec/chaton/settings/er;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/er;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/er;

    return-object v0
.end method
