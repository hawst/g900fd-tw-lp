.class Lcom/sec/chaton/settings/tellfriends/bf;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "TwitterManager.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/ba;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;)V
    .locals 1

    .prologue
    .line 365
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bf;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    .line 366
    const-string v0, "GetRequestTokenTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 367
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;Lcom/sec/chaton/settings/tellfriends/bb;)V
    .locals 0

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bf;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bf;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    const-string v1, "GetRequestTokenTask() : Get Request Token"

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->c(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V

    .line 374
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->g()Ltwitter4j/Twitter;

    move-result-object v0

    invoke-interface {v0}, Ltwitter4j/Twitter;->getOAuthRequestToken()Ltwitter4j/auth/RequestToken;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ba;->a(Ltwitter4j/auth/RequestToken;)Ltwitter4j/auth/RequestToken;

    .line 375
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->h()Ltwitter4j/auth/RequestToken;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->h()Ltwitter4j/auth/RequestToken;

    move-result-object v0

    invoke-virtual {v0}, Ltwitter4j/auth/RequestToken;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bf;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    const-string v1, "Incorrect Request Token!"

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->d(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V

    .line 377
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/bf;->e:I

    .line 378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bf;->f:Ljava/lang/Object;
    :try_end_0
    .catch Ltwitter4j/TwitterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    :goto_0
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bf;->e:I

    return v0

    .line 380
    :cond_1
    const/4 v0, -0x1

    :try_start_1
    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/bf;->e:I

    .line 381
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->h()Ltwitter4j/auth/RequestToken;

    move-result-object v0

    invoke-virtual {v0}, Ltwitter4j/auth/RequestToken;->getToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bf;->f:Ljava/lang/Object;
    :try_end_1
    .catch Ltwitter4j/TwitterException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->printStackTrace()V

    .line 385
    const/16 v1, -0x3e9

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bf;->e:I

    .line 386
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->getMessage()Ljava/lang/String;

    goto :goto_0
.end method
