.class Lcom/sec/chaton/settings/cu;
.super Ljava/lang/Object;
.source "CustomTimePicker.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/co;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 180
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v1}, Lcom/sec/chaton/settings/co;->d(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->c(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v1}, Lcom/sec/chaton/settings/co;->b(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/co;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 182
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    iget-object v1, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v1}, Lcom/sec/chaton/settings/co;->a(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/co;->c(Lcom/sec/chaton/settings/co;I)I

    .line 189
    :cond_2
    :goto_0
    return-void

    .line 186
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/co;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    iget-object v1, p0, Lcom/sec/chaton/settings/cu;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v1}, Lcom/sec/chaton/settings/co;->a(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/co;->c(Lcom/sec/chaton/settings/co;I)I

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method
