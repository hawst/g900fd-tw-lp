.class public Lcom/sec/chaton/settings/moreapps/d;
.super Landroid/widget/ArrayAdapter;
.source "PlusMenuAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/ak;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/chaton/settings/moreapps/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/moreapps/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/chaton/PlusFragment;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/chaton/PlusFragment;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/ak;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 32
    iput-object p1, p0, Lcom/sec/chaton/settings/moreapps/d;->b:Landroid/content/Context;

    .line 35
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/d;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/settings/moreapps/d;->c:Landroid/view/LayoutInflater;

    .line 37
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 44
    if-nez p2, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings/moreapps/d;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0300d6

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 46
    new-instance v1, Lcom/sec/chaton/settings/moreapps/e;

    invoke-direct {v1}, Lcom/sec/chaton/settings/moreapps/e;-><init>()V

    .line 47
    const v0, 0x7f07034b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/settings/moreapps/e;->a:Landroid/widget/ImageView;

    .line 48
    const v0, 0x7f07034d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/settings/moreapps/e;->b:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f0703c2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/settings/moreapps/e;->c:Landroid/widget/TextView;

    .line 51
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 56
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/moreapps/d;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/ak;

    .line 57
    iget-object v2, v1, Lcom/sec/chaton/settings/moreapps/e;->b:Landroid/widget/TextView;

    iget v3, v0, Lcom/sec/chaton/ak;->b:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 58
    iget-object v2, v1, Lcom/sec/chaton/settings/moreapps/e;->a:Landroid/widget/ImageView;

    iget v3, v0, Lcom/sec/chaton/ak;->a:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 60
    iget v2, v0, Lcom/sec/chaton/ak;->c:I

    if-lez v2, :cond_2

    .line 61
    iget-object v2, v1, Lcom/sec/chaton/settings/moreapps/e;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    iget v2, v0, Lcom/sec/chaton/ak;->c:I

    const/16 v3, 0x63

    if-le v2, v3, :cond_1

    .line 63
    iget-object v0, v1, Lcom/sec/chaton/settings/moreapps/e;->c:Landroid/widget/TextView;

    const-string v1, "99+"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :goto_1
    return-object p2

    .line 53
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/moreapps/e;

    move-object v1, v0

    goto :goto_0

    .line 65
    :cond_1
    iget-object v1, v1, Lcom/sec/chaton/settings/moreapps/e;->c:Landroid/widget/TextView;

    iget v0, v0, Lcom/sec/chaton/ak;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 68
    :cond_2
    iget-object v0, v1, Lcom/sec/chaton/settings/moreapps/e;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
