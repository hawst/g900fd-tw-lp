.class public Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivitySoundDetail.java"


# instance fields
.field private a:Lcom/sec/chaton/settings/downloads/SoundDetail;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "startPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;->startActivity(Landroid/content/Intent;)V

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/sec/chaton/settings/downloads/SoundDetail;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/SoundDetail;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    .line 35
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;->a:Lcom/sec/chaton/settings/downloads/SoundDetail;

    return-object v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 57
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;->c()V

    .line 59
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 43
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 50
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 46
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/ActivitySoundDetail;->finish()V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
