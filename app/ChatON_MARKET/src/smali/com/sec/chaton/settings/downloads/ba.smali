.class Lcom/sec/chaton/settings/downloads/ba;
.super Landroid/os/Handler;
.source "FontDetail.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/FontDetail;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/FontDetail;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 117
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 119
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/i;

    .line 121
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 148
    :goto_0
    return-void

    .line 124
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 125
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PackageId: "

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    const-string v2, ". Status: "

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/FontDetail;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Lcom/sec/chaton/settings/downloads/FontDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Lcom/sec/chaton/settings/downloads/FontDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 133
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Lcom/sec/chaton/settings/downloads/FontDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Lcom/sec/chaton/settings/downloads/FontDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 138
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-static {v0, v2}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Lcom/sec/chaton/settings/downloads/FontDetail;Lcom/sec/chaton/settings/downloads/a/i;)Lcom/sec/chaton/settings/downloads/a/i;

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/FontDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-virtual {v0, v3, v2, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Lcom/sec/chaton/settings/downloads/FontDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/FontDetail;->a(Lcom/sec/chaton/settings/downloads/FontDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 145
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/ba;->a:Lcom/sec/chaton/settings/downloads/FontDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/FontDetail;->b(Lcom/sec/chaton/settings/downloads/FontDetail;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/i;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
