.class public Lcom/sec/chaton/settings/ActivityDownloads;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "ActivityDownloads.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/sec/chaton/settings/AboutNewNotice;

.field c:Landroid/preference/PreferenceScreen;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/chaton/util/ab;

.field private f:Lcom/sec/chaton/settings/AboutNewNotice;

.field private g:Lcom/sec/chaton/settings/AboutNewNotice;

.field private h:Lcom/sec/chaton/settings/AboutNewNotice;

.field private i:Lcom/sec/chaton/settings/AboutNewNotice;

.field private j:Lcom/sec/chaton/settings/AboutNewNotice;

.field private k:Lcom/sec/chaton/settings/AboutNewNotice;

.field private l:Lcom/sec/common/a/a;

.field private m:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    .line 49
    const-string v0, "Settings"

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->a:Ljava/lang/String;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->e:Lcom/sec/chaton/util/ab;

    .line 80
    new-instance v0, Lcom/sec/chaton/settings/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/z;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->m:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->l:Lcom/sec/common/a/a;

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->l:Lcom/sec/common/a/a;

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->l:Lcom/sec/common/a/a;

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->l:Lcom/sec/common/a/a;

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 228
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityDownloads;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->c()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/ActivityDownloads;Lcom/sec/chaton/settings/ai;)Z
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/ActivityDownloads;->a(Lcom/sec/chaton/settings/ai;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/sec/chaton/settings/ai;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 184
    sget-object v1, Lcom/sec/chaton/settings/ah;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/ai;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 218
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 186
    :pswitch_0
    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 187
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->a()V

    goto :goto_0

    .line 193
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 195
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->a()V

    goto :goto_0

    .line 201
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 203
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->a()V

    goto :goto_0

    .line 206
    :cond_2
    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->a()V

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/chaton/settings/ActivityDownloads;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->d:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 232
    const-string v0, "pref_screen"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->c:Landroid/preference/PreferenceScreen;

    .line 233
    const-string v0, "pref_item_downloads_chaton_v"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 234
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "ChatONVDownloadalbe"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v1, Lcom/sec/chaton/settings/aa;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/aa;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 248
    :goto_0
    const-string v0, "pref_item_downloads_ticcon"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->f:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->f:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v1, Lcom/sec/chaton/settings/ab;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ab;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 269
    const-string v0, "pref_item_downloads_theme"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->g:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->g:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v1, Lcom/sec/chaton/settings/ac;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ac;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 291
    const-string v0, "pref_item_downloads_ams_background"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->h:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->h:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v1, Lcom/sec/chaton/settings/ad;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ad;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 312
    const-string v0, "pref_item_downloads_ams_stamp"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->i:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 315
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->i:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v1, Lcom/sec/chaton/settings/ae;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ae;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 333
    const-string v0, "pref_item_downloads_ams_template"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->j:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->j:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v1, Lcom/sec/chaton/settings/af;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/af;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 354
    const-string v0, "pref_item_downloads_sound"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->k:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->k:Lcom/sec/chaton/settings/AboutNewNotice;

    new-instance v1, Lcom/sec/chaton/settings/ag;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/ag;-><init>(Lcom/sec/chaton/settings/ActivityDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 395
    return-void

    .line 243
    :cond_0
    const-string v0, "settings_title_more_apps"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->c:Landroid/preference/PreferenceScreen;

    const-string v1, "settings_title_more_apps"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/ActivityDownloads;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 398
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "ChatONVDownloadalbe"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->b:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    .line 403
    :cond_0
    invoke-static {}, Lcom/sec/chaton/settings/downloads/u;->a()I

    move-result v0

    .line 405
    if-lez v0, :cond_1

    .line 406
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->f:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 412
    :goto_0
    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->e()I

    move-result v0

    .line 414
    if-lez v0, :cond_2

    .line 415
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->g:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 421
    :goto_1
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    .line 423
    if-lez v0, :cond_3

    .line 424
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->h:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 430
    :goto_2
    sget-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    .line 432
    if-lez v0, :cond_4

    .line 433
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->i:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 439
    :goto_3
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    .line 441
    if-lez v0, :cond_5

    .line 442
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->j:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 448
    :goto_4
    invoke-static {}, Lcom/sec/chaton/settings/downloads/dg;->a()I

    move-result v0

    .line 450
    if-lez v0, :cond_6

    .line 451
    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->k:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 468
    :goto_5
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 469
    return-void

    .line 408
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->f:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    goto :goto_0

    .line 417
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->g:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    goto :goto_1

    .line 426
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->h:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    goto :goto_2

    .line 435
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->i:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    goto :goto_3

    .line 444
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->j:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    goto :goto_4

    .line 453
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->k:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/AboutNewNotice;->a(Z)V

    goto :goto_5
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 155
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 159
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 95
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00110001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 98
    :cond_0
    iput-object p0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->d:Landroid/content/Context;

    .line 100
    const v0, 0x7f050007

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/ActivityDownloads;->addPreferencesFromResource(I)V

    .line 102
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->e:Lcom/sec/chaton/util/ab;

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings/ActivityDownloads;->e:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    .line 114
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 175
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onPause()V

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/ActivityDownloads;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 181
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 131
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onResume()V

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-direct {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->c()V

    .line 139
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 140
    const-string v1, "more_tab_badge_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ActivityDownloads;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 144
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 147
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 122
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/sec/chaton/settings/ActivityDownloads;->finish()V

    .line 124
    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
