.class public Lcom/sec/chaton/settings/tellfriends/ai;
.super Ljava/lang/Object;
.source "SnsHelper.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->e:Ljava/lang/String;

    .line 137
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->a:Ljava/lang/String;

    .line 138
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    .line 139
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/ai;->c:Ljava/lang/String;

    .line 140
    iput-object p4, p0, Lcom/sec/chaton/settings/tellfriends/ai;->d:Ljava/lang/String;

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->f:Z

    .line 142
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->e:Ljava/lang/String;

    .line 130
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->a:Ljava/lang/String;

    .line 131
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    .line 132
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/ai;->d:Ljava/lang/String;

    .line 133
    iput-boolean p4, p0, Lcom/sec/chaton/settings/tellfriends/ai;->f:Z

    .line 134
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/ai;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->e:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->f:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 209
    .line 211
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/sec/chaton/settings/tellfriends/ai;

    if-eqz v1, :cond_0

    .line 212
    check-cast p1, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 213
    iget-object v1, p1, Lcom/sec/chaton/settings/tellfriends/ai;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/ai;->a:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lcom/sec/chaton/settings/tellfriends/ai;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/ai;->d:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 216
    :cond_0
    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 194
    .line 196
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ai;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x431

    .line 197
    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 201
    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SnsFriend id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
