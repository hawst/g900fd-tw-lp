.class Lcom/sec/chaton/settings/downloads/bd;
.super Landroid/os/Handler;
.source "FontDownloads.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/FontDownloads;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/FontDownloads;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 107
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b(Lcom/sec/chaton/settings/downloads/FontDownloads;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    .line 115
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 117
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-static {v1, v4}, Lcom/sec/chaton/settings/downloads/FontDownloads;->a(Lcom/sec/chaton/settings/downloads/FontDownloads;Lcom/sec/chaton/d/a/bz;)Lcom/sec/chaton/d/a/bz;

    .line 119
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/FontDownloads;->c(Lcom/sec/chaton/settings/downloads/FontDownloads;)V

    .line 121
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b(Z)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/settings/downloads/FontDownloads;->a(Z)V

    goto :goto_0

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b(Lcom/sec/chaton/settings/downloads/FontDownloads;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00b3

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/av;->a(I)I

    move-result v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/bd;->a:Lcom/sec/chaton/settings/downloads/FontDownloads;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/FontDownloads;->b(Lcom/sec/chaton/settings/downloads/FontDownloads;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
