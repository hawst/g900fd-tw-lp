.class Lcom/sec/chaton/settings/ef;
.super Ljava/lang/Object;
.source "FragmentSkinChange3.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/FragmentSkinChange3;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V
    .locals 0

    .prologue
    .line 881
    iput-object p1, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const v5, 0x7f0b0414

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 885
    iget-object v0, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0, v3}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;Z)Z

    .line 886
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 887
    add-int/lit8 p2, p2, 0x1

    .line 889
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 925
    :goto_0
    return-void

    .line 891
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 892
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 893
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentSkinChange3;->p(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 894
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 896
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentSkinChange3;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 903
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Lcom/sec/chaton/settings/FragmentSkinChange3;Z)Z

    goto :goto_0

    .line 897
    :catch_0
    move-exception v0

    .line 898
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 899
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 900
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 906
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 907
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 908
    iget-object v1, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 909
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 910
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 911
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 914
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings/FragmentSkinChange3;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 921
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/ef;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0, v4}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Lcom/sec/chaton/settings/FragmentSkinChange3;Z)Z

    goto :goto_0

    .line 915
    :catch_1
    move-exception v0

    .line 916
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 917
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 918
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 889
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
