.class Lcom/sec/chaton/settings/tellfriends/bd;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "TwitterManager.java"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/sec/chaton/settings/tellfriends/ba;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;)V
    .locals 1

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    .line 255
    const-string v0, "GetFriendsListTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    .line 257
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/bd;->c:I

    .line 258
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;Lcom/sec/chaton/settings/tellfriends/bb;)V
    .locals 0

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bd;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;)V

    return-void
.end method

.method private c()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bd;->c:I

    return v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bd;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/bd;->c:I

    .line 313
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 262
    .line 267
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 268
    const-wide/16 v1, -0x1

    .line 270
    :cond_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->g()Ltwitter4j/Twitter;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Ltwitter4j/Twitter;->getFriendsIDs(J)Ltwitter4j/IDs;

    move-result-object v2

    .line 271
    invoke-interface {v2}, Ltwitter4j/IDs;->getIDs()[J

    move-result-object v3

    move v1, v0

    .line 272
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_1

    .line 273
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    aget-wide v5, v3, v1

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 275
    :cond_1
    invoke-interface {v2}, Ltwitter4j/IDs;->getNextCursor()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    .line 278
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/bd;->c()I

    move-result v1

    mul-int/lit8 v2, v1, 0x64

    .line 279
    add-int/lit8 v1, v2, 0x64

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    add-int/lit8 v1, v2, 0x64

    .line 282
    :goto_1
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/bd;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " endIndex = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/chaton/settings/tellfriends/ba;->a(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V

    .line 283
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/bd;->c()I

    move-result v3

    mul-int/lit8 v3, v3, 0x64

    invoke-virtual {v2, v3, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 284
    const/16 v1, 0x64

    new-array v3, v1, [J

    move v1, v0

    .line 286
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 287
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 286
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 279
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_1

    .line 290
    :cond_4
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->g()Ltwitter4j/Twitter;

    move-result-object v0

    invoke-interface {v0, v3}, Ltwitter4j/Twitter;->lookupUsers([J)Ltwitter4j/ResponseList;

    move-result-object v0

    .line 291
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/bd;->d()V

    .line 293
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->e:I
    :try_end_0
    .catch Ltwitter4j/TwitterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 303
    :goto_3
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bd;->f:Ljava/lang/Object;

    .line 304
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bd;->e:I

    return v0

    .line 294
    :catch_0
    move-exception v0

    .line 295
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->printStackTrace()V

    .line 296
    const/16 v1, -0x3e9

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->e:I

    .line 297
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 298
    :catch_1
    move-exception v0

    .line 299
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 300
    const/16 v1, -0x3e8

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->e:I

    .line 301
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 316
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/bd;->c()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 323
    :cond_0
    :goto_0
    return v0

    .line 320
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 323
    const/4 v0, 0x0

    goto :goto_0
.end method
