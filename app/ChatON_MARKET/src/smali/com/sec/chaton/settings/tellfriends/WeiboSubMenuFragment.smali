.class public Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;
.super Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;
.source "WeiboSubMenuFragment.java"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private h:Landroid/view/View;

.field private i:Landroid/app/Activity;

.field private j:Lcom/sec/chaton/settings/tellfriends/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;)Lcom/sec/chaton/settings/tellfriends/ap;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->a(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->c()V

    .line 102
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->h()V

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/cd;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/cd;-><init>(Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ap;->a(Lcom/sec/chaton/settings/tellfriends/af;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ai;)V
    .locals 3

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/tellfriends/WeiboMessageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 121
    const-string v1, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/ai;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    .line 123
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->e()Landroid/widget/TextView;

    move-result-object v0

    .line 107
    const v1, 0x7f0b01fd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 108
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->f:Lcom/sec/chaton/settings/tellfriends/ad;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ap;->a(Lcom/sec/chaton/settings/tellfriends/ad;)V

    .line 116
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 144
    const/16 v0, 0xbb9

    if-ne p1, v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 152
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-direct {v0, p1}, Lcom/sec/chaton/settings/tellfriends/ap;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    .line 35
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onAttach(Landroid/app/Activity;)V

    .line 36
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i:Landroid/app/Activity;

    .line 37
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 127
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 138
    :goto_0
    return-void

    .line 129
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/ActivityManageAccounts;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 133
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/tellfriends/WeiboMessageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    const-string v1, "com.sec.chaton.settings.tellfriends.common.SnsMessageFragment.FriendName"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 127
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f07029d -> :sswitch_0
        0x7f070457 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreate(Landroid/os/Bundle;)V

    .line 30
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->h:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->h:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->j:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->a()V

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->g()V

    .line 61
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDestroy()V

    .line 62
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onDetach()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i:Landroid/app/Activity;

    .line 49
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/SnsSubMenuFragment;->onPause()V

    .line 54
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuFragment;->i()V

    .line 55
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method
