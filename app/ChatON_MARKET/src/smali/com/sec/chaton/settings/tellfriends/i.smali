.class Lcom/sec/chaton/settings/tellfriends/i;
.super Ljava/lang/Object;
.source "RenrenDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONCRETE:",
        "Lcom/sec/chaton/settings/tellfriends/i",
        "<*>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/os/Bundle;

.field private c:Lcom/renren/android/Renren;

.field private d:Ljava/lang/String;

.field private e:Lcom/sec/chaton/settings/tellfriends/common/b;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/renren/android/Renren;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/i;->c:Lcom/renren/android/Renren;

    .line 303
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/chaton/settings/tellfriends/i;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 304
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 307
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/i;->a:Landroid/content/Context;

    .line 308
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/i;->d:Ljava/lang/String;

    .line 309
    if-eqz p3, :cond_0

    .line 310
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/i;->b:Landroid/os/Bundle;

    .line 314
    :goto_0
    return-void

    .line 312
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/i;->b:Landroid/os/Bundle;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/settings/tellfriends/e;
    .locals 5

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/i;->c:Lcom/renren/android/Renren;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/i;->b:Landroid/os/Bundle;

    const-string v1, "app_id"

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/i;->c:Lcom/renren/android/Renren;

    invoke-virtual {v2}, Lcom/renren/android/Renren;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/i;->c:Lcom/renren/android/Renren;

    invoke-virtual {v0}, Lcom/renren/android/Renren;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/i;->b:Landroid/os/Bundle;

    const-string v1, "access_token"

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/i;->c:Lcom/renren/android/Renren;

    invoke-virtual {v2}, Lcom/renren/android/Renren;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/i;->b:Landroid/os/Bundle;

    const-string v1, "redirect_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/i;->b:Landroid/os/Bundle;

    const-string v1, "redirect_uri"

    sget-object v2, Lcom/sec/chaton/c/b;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :cond_1
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/e;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/i;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/i;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/i;->b:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/i;->e:Lcom/sec/chaton/settings/tellfriends/common/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/settings/tellfriends/e;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/chaton/settings/tellfriends/common/b;)V

    return-object v0
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/b;)Lcom/sec/chaton/settings/tellfriends/i;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/settings/tellfriends/common/b;",
            ")TCONCRETE;"
        }
    .end annotation

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/i;->e:Lcom/sec/chaton/settings/tellfriends/common/b;

    .line 327
    return-object p0
.end method
