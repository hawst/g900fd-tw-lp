.class Lcom/sec/chaton/settings/downloads/cp;
.super Ljava/lang/Object;
.source "SkinListView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/SkinListView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/SkinListView;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/cp;->a:Lcom/sec/chaton/settings/downloads/SkinListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cp;->a:Lcom/sec/chaton/settings/downloads/SkinListView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/SkinListView;->d(Lcom/sec/chaton/settings/downloads/SkinListView;)Lcom/sec/chaton/settings/downloads/bw;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/cp;->a:Lcom/sec/chaton/settings/downloads/SkinListView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/SkinListView;->d(Lcom/sec/chaton/settings/downloads/SkinListView;)Lcom/sec/chaton/settings/downloads/bw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/bw;->d()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 209
    :catch_0
    move-exception v0

    .line 210
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 211
    const-string v1, "download_skin, json parsing exception occured."

    sget-object v2, Lcom/sec/chaton/settings/downloads/SkinListView;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/cp;->a:Lcom/sec/chaton/settings/downloads/SkinListView;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/SkinListView;->b(Lcom/sec/chaton/settings/downloads/SkinListView;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/cp;->a:Lcom/sec/chaton/settings/downloads/SkinListView;

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/SkinListView;->b(Lcom/sec/chaton/settings/downloads/SkinListView;)Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b0229

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 214
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
