.class Lcom/sec/chaton/settings/e;
.super Landroid/os/Handler;
.source "AboutServiceFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/AboutServiceFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/AboutServiceFragment;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    const-string v0, "2"

    .line 410
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 411
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 413
    iget-object v2, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->g(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->g(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 414
    iget-object v2, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->g(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 416
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_5

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 419
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateAppsReady"

    const-string v2, "YES"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->h(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->i(Lcom/sec/chaton/settings/AboutServiceFragment;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "YES"

    invoke-static {v0, v1, v4, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 434
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "Samsung apps is ready to upgrade chaton"

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 423
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 424
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->a(Lcom/sec/chaton/settings/AboutServiceFragment;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/UpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 425
    const-string v1, "isCritical"

    iget-object v2, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/AboutServiceFragment;->h(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 426
    const-string v1, "isFromHome"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 428
    iget-object v1, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 430
    :cond_4
    invoke-static {v3}, Lcom/sec/chaton/util/cb;->a(I)V

    goto :goto_1

    .line 439
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->h(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->i(Lcom/sec/chaton/settings/AboutServiceFragment;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/AboutServiceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "NO"

    invoke-static {v0, v1, v4, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 449
    :goto_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 450
    const-string v0, "Samsung apps is NOT ready to upgrade chaton"

    invoke-static {}, Lcom/sec/chaton/settings/AboutServiceFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 442
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/e;->a:Lcom/sec/chaton/settings/AboutServiceFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/AboutServiceFragment;->j(Lcom/sec/chaton/settings/AboutServiceFragment;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 443
    invoke-static {v3}, Lcom/sec/chaton/util/cb;->c(I)V

    goto :goto_2

    .line 445
    :cond_7
    invoke-static {v3}, Lcom/sec/chaton/util/cb;->e(I)V

    goto :goto_2
.end method
