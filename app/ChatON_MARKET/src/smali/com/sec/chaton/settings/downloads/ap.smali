.class Lcom/sec/chaton/settings/downloads/ap;
.super Ljava/lang/Object;
.source "AniconPackageDownloads.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/ap;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 392
    invoke-static {p2}, Lcom/sec/chaton/settings/downloads/au;->a(I)Lcom/sec/chaton/settings/downloads/au;

    move-result-object v0

    .line 393
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "anicon_list, user clicked : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    sget-object v1, Lcom/sec/chaton/settings/downloads/at;->a:[I

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/au;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 404
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 408
    return-void

    .line 401
    :pswitch_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "setting_anicon_sortby"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/ap;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/ap;->a:Lcom/sec/chaton/settings/downloads/AniconPackageDownloads;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 396
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
