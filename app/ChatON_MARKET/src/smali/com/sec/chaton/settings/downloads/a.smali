.class Lcom/sec/chaton/settings/downloads/a;
.super Landroid/support/v4/widget/CursorAdapter;
.source "AmsItemAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/settings/downloads/a/q;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/settings/downloads/aw;

.field private c:Lcom/sec/chaton/d/e;

.field private d:Lcom/sec/chaton/d/d;

.field private e:Lcom/sec/common/f/c;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/settings/downloads/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/sec/chaton/settings/downloads/d;

.field private h:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/a;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ZLcom/sec/chaton/d/e;Lcom/sec/common/f/c;)V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 64
    new-instance v0, Lcom/sec/chaton/settings/downloads/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/b;-><init>(Lcom/sec/chaton/settings/downloads/a;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->h:Landroid/os/Handler;

    .line 197
    iput-object p4, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    .line 198
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/chaton/d/d;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->d:Lcom/sec/chaton/d/d;

    .line 199
    iput-object p5, p0, Lcom/sec/chaton/settings/downloads/a;->e:Lcom/sec/common/f/c;

    .line 202
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->f:Ljava/util/HashMap;

    .line 203
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/a;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->g:Lcom/sec/chaton/settings/downloads/d;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->g:Lcom/sec/chaton/settings/downloads/d;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-interface {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/d;->a(Lcom/sec/chaton/d/e;Ljava/lang/String;)V

    .line 372
    :cond_0
    return-void
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/chaton/settings/downloads/a;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/inner/AmsItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 164
    :goto_0
    return-object v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/c;

    .line 136
    if-nez v0, :cond_1

    move-object v0, v1

    .line 137
    goto :goto_0

    .line 140
    :cond_1
    iget v0, v0, Lcom/sec/chaton/settings/downloads/c;->a:I

    .line 141
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 142
    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 143
    if-nez v0, :cond_2

    move-object v0, v1

    .line 144
    goto :goto_0

    .line 147
    :cond_2
    new-instance v0, Lcom/sec/chaton/io/entry/inner/AmsItem;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/AmsItem;-><init>()V

    .line 148
    const-string v3, "extras"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 150
    :try_start_0
    invoke-static {v0, v3}, Lcom/sec/chaton/e/a/a;->a(Lcom/sec/chaton/io/entry/inner/AmsItem;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    const-string v1, "item_id"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/AmsItem;->id:Ljava/lang/String;

    .line 159
    const-string v1, "expiration_time"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/AmsItem;->expirationdate:Ljava/lang/Long;

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_3

    .line 153
    sget-object v2, Lcom/sec/chaton/settings/downloads/a;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_3
    move-object v0, v1

    .line 155
    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 475
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a;->notifyDataSetChanged()V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->b:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 482
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/aw;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    .line 470
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/downloads/d;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/a;->g:Lcom/sec/chaton/settings/downloads/d;

    .line 169
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 349
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/a;

    .line 351
    if-nez v0, :cond_1

    .line 352
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->d:Lcom/sec/chaton/d/d;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-direct {v0, v1, v2, p1}, Lcom/sec/chaton/settings/downloads/a/a;-><init>(Lcom/sec/chaton/d/d;Lcom/sec/chaton/d/e;Ljava/lang/String;)V

    .line 353
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 355
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->d:Lcom/sec/chaton/d/d;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-direct {v0, v1, v2, p1}, Lcom/sec/chaton/settings/downloads/a/a;-><init>(Lcom/sec/chaton/d/d;Lcom/sec/chaton/d/e;Ljava/lang/String;)V

    .line 356
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_0

    .line 357
    :cond_2
    if-eqz p2, :cond_0

    .line 358
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 362
    :pswitch_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/a;->a(Z)Z

    goto :goto_0

    .line 358
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 112
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 119
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/e/ar;)V

    .line 120
    return-void
.end method

.method public b(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 487
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    .line 495
    :cond_0
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 247
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/c;

    .line 250
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/a;->f:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 252
    const-string v3, "extras"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 253
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 255
    const-string v3, "item_id"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 256
    const-string v5, "thumbnailUrl"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 257
    const-string v5, "install"

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-nez v5, :cond_0

    move v2, v1

    .line 258
    :cond_0
    const-string v1, "new"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 259
    const-string v1, "special"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 261
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 264
    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    invoke-direct {v1, v4}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;)V

    .line 265
    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/a;->e:Lcom/sec/common/f/c;

    iget-object v7, v0, Lcom/sec/chaton/settings/downloads/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v4, v7, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 268
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iput v1, v0, Lcom/sec/chaton/settings/downloads/c;->a:I

    .line 271
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 272
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v4}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/a/a;

    .line 277
    if-eqz v1, :cond_1

    .line 278
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/a;->h:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Lcom/sec/chaton/settings/downloads/a/a;->a(Landroid/os/Handler;)V

    .line 281
    :cond_1
    iget-object v3, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 282
    iget-object v3, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 283
    iget-object v3, v0, Lcom/sec/chaton/settings/downloads/c;->f:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    if-eqz v2, :cond_3

    .line 288
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    const v2, 0x7f0200af

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 289
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 330
    :cond_2
    :goto_0
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 332
    if-eqz v6, :cond_7

    .line 333
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->b:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 335
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/c;->b:Landroid/widget/ImageView;

    const v1, 0x7f0201e0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 346
    :goto_1
    return-void

    .line 291
    :cond_3
    if-nez v1, :cond_4

    .line 292
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    const v2, 0x7f0200ac

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 293
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 343
    :catch_0
    move-exception v0

    .line 344
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 294
    :cond_4
    :try_start_1
    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_5

    .line 295
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    const v2, 0x7f0200ad

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 296
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 299
    :cond_5
    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 301
    :pswitch_0
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    const v2, 0x7f0200ae

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 302
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 305
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v1

    if-nez v1, :cond_2

    .line 306
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 311
    :pswitch_1
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 312
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    const v3, 0x7f0200ae

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 315
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 316
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 318
    :cond_6
    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/downloads/a/a;->g()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_0

    .line 322
    :pswitch_2
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 323
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 324
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->f:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 336
    :cond_7
    if-eqz v5, :cond_8

    .line 337
    iget-object v1, v0, Lcom/sec/chaton/settings/downloads/c;->b:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 339
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/c;->b:Landroid/widget/ImageView;

    const v1, 0x7f0201da

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 341
    :cond_8
    iget-object v0, v0, Lcom/sec/chaton/settings/downloads/c;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 299
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 123
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->c(Lcom/sec/chaton/e/ar;)V

    .line 124
    return-void
.end method

.method public c(Lcom/sec/chaton/settings/downloads/a/l;)V
    .locals 3

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a;->notifyDataSetChanged()V

    .line 501
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_0

    .line 502
    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 505
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 512
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {p1}, Lcom/sec/chaton/settings/downloads/a/l;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->b(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_0

    .line 502
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 375
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 376
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 377
    const/4 v0, -0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 378
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 379
    const-string v0, "item_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 380
    const-string v0, "install"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 381
    :goto_1
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_1

    .line 382
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "download_ams, download all, add into list,  id/install : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/settings/downloads/a;->b:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    if-nez v0, :cond_0

    .line 387
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 380
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 391
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 392
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/downloads/a;->a(Ljava/lang/String;Z)V

    goto :goto_2

    .line 394
    :cond_4
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 207
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    sget-object v2, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    if-ne v0, v2, :cond_0

    .line 210
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 212
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 214
    const v3, 0x7f0901d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 215
    const v3, 0x7f0901d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 217
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 220
    const v0, 0x7f080052

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 223
    :cond_0
    new-instance v2, Lcom/sec/chaton/settings/downloads/c;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/settings/downloads/c;-><init>(Lcom/sec/chaton/settings/downloads/a;Lcom/sec/chaton/settings/downloads/b;)V

    .line 224
    const v0, 0x7f070173

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->b:Landroid/widget/ImageView;

    .line 225
    const v0, 0x7f070172

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->c:Landroid/widget/ImageView;

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    sget-object v3, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    if-ne v0, v3, :cond_1

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 229
    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 230
    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 231
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->c:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 233
    :cond_1
    const v0, 0x7f070175

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    .line 234
    const v0, 0x7f070174

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->e:Landroid/widget/ProgressBar;

    .line 235
    const v0, 0x7f070176

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->f:Landroid/widget/TextView;

    .line 237
    iget-object v0, v2, Lcom/sec/chaton/settings/downloads/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 241
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 398
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/c;

    .line 400
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 401
    iget v0, v0, Lcom/sec/chaton/settings/downloads/c;->a:I

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 403
    const-string v0, "item_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 404
    const-string v0, "install"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 406
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 415
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 404
    goto :goto_0

    .line 410
    :cond_1
    if-eqz v0, :cond_2

    .line 411
    invoke-direct {p0, v3}, Lcom/sec/chaton/settings/downloads/a;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 413
    :cond_2
    invoke-virtual {p0, v3, v1}, Lcom/sec/chaton/settings/downloads/a;->a(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 426
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 427
    const-string v0, "swapCursor(), newCursor : "

    sget-object v1, Lcom/sec/chaton/settings/downloads/a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_0
    if-nez p1, :cond_2

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->a()V

    .line 465
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 435
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    if-eqz v0, :cond_1

    .line 436
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 437
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 438
    const-string v0, "item_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 439
    const-string v0, "install"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 441
    :goto_2
    if-eqz v0, :cond_4

    .line 442
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->c:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 439
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .line 447
    :cond_4
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/a;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v2}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/a;

    .line 449
    if-nez v0, :cond_5

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    sget-object v2, Lcom/sec/chaton/settings/downloads/ay;->a:Lcom/sec/chaton/settings/downloads/ay;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/aw;->a(Ljava/lang/String;Lcom/sec/chaton/settings/downloads/ay;)Z

    goto :goto_1

    .line 455
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/aw;->a(Lcom/sec/chaton/settings/downloads/a/l;)Z

    goto :goto_1

    .line 462
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a;->a:Lcom/sec/chaton/settings/downloads/aw;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/aw;->b()V

    goto :goto_0
.end method
