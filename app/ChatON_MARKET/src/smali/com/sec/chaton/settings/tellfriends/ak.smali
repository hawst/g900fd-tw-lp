.class Lcom/sec/chaton/settings/tellfriends/ak;
.super Ljava/lang/Object;
.source "SnsHelperFacebook.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/c;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/aj;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/aj;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v0, -0x1

    .line 48
    packed-switch p1, :pswitch_data_0

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 50
    :pswitch_0
    if-ne p2, v0, :cond_2

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_OK \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    instance-of v0, p3, Lcom/facebook/model/GraphObjectList;

    if-eqz v0, :cond_0

    .line 54
    check-cast p3, Lcom/facebook/model/GraphObjectList;

    .line 55
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 57
    invoke-interface {p3}, Lcom/facebook/model/GraphObjectList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/model/GraphCustomUser;

    .line 58
    new-instance v3, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-interface {v0}, Lcom/facebook/model/GraphCustomUser;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/facebook/model/GraphCustomUser;->getName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/chaton/c/b;->h:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lcom/facebook/model/GraphCustomUser;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/picture"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lcom/facebook/model/GraphCustomUser;->getInstalled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/chaton/settings/tellfriends/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ad;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 75
    :cond_2
    if-nez p2, :cond_3

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_CANCELED \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ad;->a()V

    goto/16 :goto_0

    .line 81
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_ERROR \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ad;->a(I)V

    goto/16 :goto_0

    .line 87
    :pswitch_1
    if-ne p2, v0, :cond_4

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_OK \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->b(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ah;->a()V

    goto/16 :goto_0

    .line 90
    :cond_4
    if-nez p2, :cond_5

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_CANCELED \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->b(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ah;->b()V

    goto/16 :goto_0

    .line 96
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_ERROR \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->b(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ah;->a(I)V

    goto/16 :goto_0

    .line 103
    :pswitch_2
    if-ne p2, v0, :cond_6

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_OK \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->c(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onComplete()V

    goto/16 :goto_0

    .line 106
    :cond_6
    if-nez p2, :cond_7

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_CANCELED \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->c(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onCancelled()V

    goto/16 :goto_0

    .line 112
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_ERROR \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->c(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onError()V

    goto/16 :goto_0

    .line 120
    :pswitch_3
    if-ne p2, v0, :cond_8

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->d(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onComplete()V

    goto/16 :goto_0

    .line 123
    :cond_8
    if-nez p2, :cond_9

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->d(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onCancelled()V

    goto/16 :goto_0

    .line 129
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_ERROR \t- resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ak;->a:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->d(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/af;->onError(I)V

    goto/16 :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
