.class public Lcom/sec/chaton/settings/FragmentMultiDeviceView;
.super Landroid/support/v4/app/Fragment;
.source "FragmentMultiDeviceView.java"


# instance fields
.field a:Landroid/os/Handler;

.field private b:Ljava/lang/String;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/content/Context;

.field private final e:I

.field private f:Lcom/sec/chaton/settings/dr;

.field private g:Lcom/sec/chaton/d/w;

.field private h:Lcom/sec/chaton/d/at;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/app/ProgressDialog;

.field private k:Ljava/lang/String;

.field private l:Landroid/support/v4/app/FragmentActivity;

.field private m:Landroid/widget/ListView;

.field private n:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 47
    const-class v0, Lcom/sec/chaton/settings/ActivityMultiDeviceView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b:Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->e:I

    .line 51
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->f:Lcom/sec/chaton/settings/dr;

    .line 55
    iput-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->j:Landroid/app/ProgressDialog;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->k:Ljava/lang/String;

    .line 152
    new-instance v0, Lcom/sec/chaton/settings/dq;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/dq;-><init>(Lcom/sec/chaton/settings/FragmentMultiDeviceView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a:Landroid/os/Handler;

    .line 237
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->j:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->j:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/FragmentMultiDeviceView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->i:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Lcom/sec/chaton/settings/dr;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->f:Lcom/sec/chaton/settings/dr;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->m:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Lcom/sec/chaton/d/w;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->g:Lcom/sec/chaton/d/w;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->h:Lcom/sec/chaton/d/at;

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 135
    packed-switch p1, :pswitch_data_0

    .line 150
    :goto_0
    return-void

    .line 138
    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 139
    const-string v0, "mapping was FAILED"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/util/am;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->d:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->j:Landroid/app/ProgressDialog;

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->g:Lcom/sec/chaton/d/w;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 113
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iput-object p1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    .line 114
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 126
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 131
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v4, 0x7f07014c

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 64
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 65
    const v0, 0x7f0300ec

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreateView, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0092

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    const v0, 0x7f07014d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f070423

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 72
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0288

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 75
    const v0, 0x7f070424

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->m:Landroid/widget/ListView;

    .line 76
    const v0, 0x7f030097

    invoke-virtual {p1, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->n:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->d:Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/sec/chaton/settings/dr;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/dr;-><init>(Lcom/sec/chaton/settings/FragmentMultiDeviceView;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->f:Lcom/sec/chaton/settings/dr;

    .line 80
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->g:Lcom/sec/chaton/d/w;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->a:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->h:Lcom/sec/chaton/d/at;

    .line 83
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-ne v0, v6, :cond_1

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/util/am;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    if-nez p3, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b000b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v7, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->j:Landroid/app/ProgressDialog;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->g:Lcom/sec/chaton/d/w;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->f(Ljava/lang/String;)V

    .line 105
    :goto_0
    invoke-static {p0, v6}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 106
    return-object v2

    .line 99
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->d:Landroid/content/Context;

    const-class v3, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 101
    const-string v1, "isSyncContacts"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 233
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    .line 121
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 214
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 207
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 227
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStop, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->l:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/FragmentMultiDeviceView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-void
.end method
