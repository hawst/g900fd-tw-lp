.class public Lcom/sec/chaton/settings/tellfriends/bg;
.super Ljava/lang/Object;
.source "TwitterManager.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/b;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/ba;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;I)V
    .locals 0

    .prologue
    .line 396
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397
    iput p2, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    .line 398
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Canceled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->g(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V

    .line 426
    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/ba;->a(Ltwitter4j/Twitter;)Ltwitter4j/Twitter;

    .line 427
    invoke-static {v3}, Lcom/sec/chaton/settings/tellfriends/ba;->a(Ltwitter4j/auth/RequestToken;)Ltwitter4j/auth/RequestToken;

    .line 428
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/ba;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 430
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/a;)V
    .locals 4

    .prologue
    const/16 v3, -0x3ea

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Webview Error. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->f(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V

    .line 415
    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->a()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/ba;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->a()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 421
    :goto_0
    return-void

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/ba;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    iget v1, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/common/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login Request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Done."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ba;->e(Lcom/sec/chaton/settings/tellfriends/ba;Ljava/lang/String;)V

    .line 404
    instance-of v0, p1, Ltwitter4j/auth/AccessToken;

    if-eqz v0, :cond_0

    .line 405
    check-cast p1, Ltwitter4j/auth/AccessToken;

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/ba;->b:Landroid/app/Activity;

    invoke-static {p1, v0}, Lcom/sec/chaton/settings/tellfriends/bj;->a(Ltwitter4j/auth/AccessToken;Landroid/content/Context;)Z

    .line 407
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/be;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/chaton/settings/tellfriends/be;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;Ltwitter4j/auth/AccessToken;Lcom/sec/chaton/settings/tellfriends/bb;)V

    .line 408
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bg;->a:Lcom/sec/chaton/settings/tellfriends/ba;

    iget v3, p0, Lcom/sec/chaton/settings/tellfriends/bg;->b:I

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 410
    :cond_0
    return-void
.end method
