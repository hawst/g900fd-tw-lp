.class final Lcom/sec/chaton/settings/ez;
.super Ljava/lang/Object;
.source "SendEmailLog.java"

# interfaces
.implements Ljava/io/FileFilter;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 114
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".txt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    const/4 v0, 0x1

    .line 121
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".zip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 118
    const-string v1, "delete previous zip file"

    const-string v2, "SendEmailLog"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
