.class public Lcom/sec/chaton/settings/downloads/a/r;
.super Lcom/sec/chaton/settings/downloads/a/l;
.source "SkinInstallWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/chaton/settings/downloads/a/l",
        "<",
        "Ljava/lang/Void;",
        "[",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final e:Landroid/os/Handler;


# instance fields
.field private b:Lcom/sec/chaton/c/h;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/chaton/settings/downloads/a/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    .line 39
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/s;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/a/s;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/downloads/a/r;->e:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/c/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/settings/downloads/a/l;-><init>(Lcom/sec/chaton/e/ar;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/a/r;->b:Lcom/sec/chaton/c/h;

    .line 72
    iput-object p3, p0, Lcom/sec/chaton/settings/downloads/a/r;->c:Ljava/lang/String;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/a/r;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/r;->e([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 32
    check-cast p1, [Ljava/io/File;

    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/a/r;->a([Ljava/io/File;[Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected varargs a([Ljava/io/File;[Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 171
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/a/l;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Boolean;

    .line 173
    const/4 v0, 0x0

    aget-object v1, p1, v0

    .line 174
    aget-object v2, p1, v2

    .line 178
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a/r;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2, v1}, Lcom/sec/chaton/settings/downloads/cd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)V

    .line 180
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 188
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_0

    .line 189
    const-string v3, "download_skin, onDownloading, install procedure done, delete temp"

    sget-object v4, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 196
    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 197
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 180
    :cond_2
    return-object v0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    :try_start_1
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_3

    .line 183
    sget-object v3, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 186
    :cond_3
    new-instance v3, Lcom/sec/chaton/settings/downloads/a/n;

    invoke-direct {v3, v0}, Lcom/sec/chaton/settings/downloads/a/n;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :catchall_0
    move-exception v0

    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_4

    .line 189
    const-string v3, "download_skin, onDownloading, install procedure done, delete temp"

    sget-object v4, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 193
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 196
    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 197
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 188
    :cond_6
    throw v0
.end method

.method protected varargs a([Ljava/lang/Void;)[Ljava/io/File;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 86
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/downloads/a/l;->b([Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/a/r;->b:Lcom/sec/chaton/c/h;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/a/r;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->a()I

    move-result v3

    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->a()I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/settings/downloads/a/r;->c:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/c/h;->a(Lcom/sec/chaton/d/a/b;Ljava/lang/String;IILjava/lang/String;)Lcom/sec/chaton/d/a/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/av;->b()Lcom/sec/chaton/a/a/f;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_7

    .line 91
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/DownloadSkinEntry;

    .line 93
    iget-object v1, v0, Lcom/sec/chaton/io/entry/DownloadSkinEntry;->skinview:Lcom/sec/chaton/io/entry/inner/SkinView;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/SkinView;->settingview:Lcom/sec/chaton/io/entry/inner/SkinView$SettingView;

    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/SkinView$SettingView;->zipfileurl:Ljava/lang/String;

    .line 94
    iget-object v0, v0, Lcom/sec/chaton/io/entry/DownloadSkinEntry;->skinview:Lcom/sec/chaton/io/entry/inner/SkinView;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/SkinView;->chatview:Lcom/sec/chaton/io/entry/inner/SkinView$ChatView;

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/SkinView$ChatView;->zipfileurl:Ljava/lang/String;

    .line 101
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 102
    new-instance v2, Ljava/io/File;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 103
    :try_start_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    .line 106
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 108
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move v3, v7

    .line 112
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 114
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move v0, v7

    .line 117
    :goto_1
    sget-boolean v6, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v6, :cond_0

    .line 118
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "download_skin, onDownloading, 1.[delete exist? : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "], tmpSettingViewZipFile : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "download_skin, onDownloading, 2.[delete exist? : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "], tmpChatViewZipFile : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/t;

    const/4 v3, 0x1

    invoke-direct {v0, v3, p0}, Lcom/sec/chaton/settings/downloads/a/t;-><init>(ZLcom/sec/chaton/settings/downloads/a/r;)V

    .line 124
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v3

    sget-object v6, Lcom/sec/chaton/settings/downloads/a/r;->e:Landroid/os/Handler;

    invoke-virtual {v3, v6, v4, v2, v0}, Lcom/sec/common/util/a/a;->b(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;

    .line 127
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/t;

    const/4 v3, 0x0

    invoke-direct {v0, v3, p0}, Lcom/sec/chaton/settings/downloads/a/t;-><init>(ZLcom/sec/chaton/settings/downloads/a/r;)V

    .line 128
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/settings/downloads/a/r;->e:Landroid/os/Handler;

    invoke-virtual {v3, v4, v5, v1, v0}, Lcom/sec/common/util/a/a;->b(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;

    .line 130
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/File;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    const/4 v3, 0x1

    aput-object v1, v0, v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3

    return-object v0

    .line 131
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 132
    :goto_2
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_1

    .line 133
    sget-object v2, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 136
    :cond_1
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 140
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 141
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 144
    :cond_3
    new-instance v1, Lcom/sec/chaton/settings/downloads/a/n;

    invoke-direct {v1, v0}, Lcom/sec/chaton/settings/downloads/a/n;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 145
    :catch_1
    move-exception v0

    move-object v1, v6

    move-object v2, v6

    .line 146
    :goto_3
    sget-boolean v3, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v3, :cond_4

    .line 147
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 151
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 154
    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 155
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 158
    :cond_6
    throw v0

    .line 161
    :cond_7
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_8

    .line 162
    new-array v1, v9, [Ljava/lang/Object;

    const-string v2, "Http result code is error. result code: "

    aput-object v2, v1, v8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings/downloads/a/r;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_8
    new-instance v1, Lcom/sec/chaton/settings/downloads/a/n;

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "Http result code is error. result code: "

    aput-object v3, v2, v8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/sec/chaton/settings/downloads/a/n;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :catch_2
    move-exception v0

    move-object v1, v6

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_3

    .line 131
    :catch_4
    move-exception v0

    move-object v1, v6

    move-object v6, v2

    goto/16 :goto_2

    :catch_5
    move-exception v0

    move-object v6, v2

    goto/16 :goto_2

    :cond_9
    move v0, v8

    goto/16 :goto_1

    :cond_a
    move v3, v8

    goto/16 :goto_0
.end method

.method protected synthetic b([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/downloads/a/r;->a([Ljava/lang/Void;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
