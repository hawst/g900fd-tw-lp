.class public Lcom/sec/chaton/settings/tellfriends/common/d;
.super Landroid/widget/ArrayAdapter;
.source "SnsFriendsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/settings/tellfriends/ai;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/sec/chaton/settings/tellfriends/common/g;

.field private d:Landroid/content/Context;

.field private e:Landroid/view/LayoutInflater;

.field private f:Lcom/sec/chaton/settings/tellfriends/common/h;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/c/g;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/c/k;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/chaton/settings/tellfriends/common/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/common/d;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/chaton/settings/tellfriends/common/d;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/chaton/settings/tellfriends/common/h;)V

    .line 54
    return-void
.end method

.method constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/chaton/settings/tellfriends/common/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;",
            "Lcom/sec/chaton/settings/tellfriends/common/h;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->g:Ljava/util/Map;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->h:Ljava/util/Map;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->i:Ljava/util/ArrayList;

    .line 58
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->d:Landroid/content/Context;

    .line 59
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->e:Landroid/view/LayoutInflater;

    .line 60
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    .line 61
    iput-object p4, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->f:Lcom/sec/chaton/settings/tellfriends/common/h;

    .line 62
    return-void
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->f:Lcom/sec/chaton/settings/tellfriends/common/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->f:Lcom/sec/chaton/settings/tellfriends/common/h;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/common/h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "downloadProfilePicture() : set sns type icon"

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->f:Lcom/sec/chaton/settings/tellfriends/common/h;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/common/h;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 147
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/c/k;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    if-nez p3, :cond_2

    .line 186
    invoke-virtual {p1}, Lcom/facebook/c/k;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->i:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 191
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->h:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    :cond_1
    :goto_0
    return-void

    .line 195
    :cond_2
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    invoke-virtual {p1}, Lcom/facebook/c/k;->b()Ljava/lang/Exception;

    move-result-object v0

    .line 197
    invoke-virtual {p1}, Lcom/facebook/c/k;->c()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 198
    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 199
    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 200
    invoke-virtual {p1}, Lcom/facebook/c/k;->a()Lcom/facebook/c/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/c/g;->b()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/common/d;Lcom/facebook/c/k;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Lcom/facebook/c/k;Ljava/lang/String;Landroid/widget/ImageView;)V

    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 346
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/common/d;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/net/URL;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 152
    if-nez p2, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    if-nez p3, :cond_4

    const/4 v0, 0x1

    .line 157
    :goto_1
    if-nez v0, :cond_2

    invoke-virtual {p3}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/net/URL;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 158
    :cond_2
    if-nez v0, :cond_3

    .line 159
    const-string v0, "downloadProfilePicture() : set default image"

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 161
    const v0, 0x7f0201bb

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 164
    :cond_3
    new-instance v0, Lcom/facebook/c/i;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/facebook/c/i;-><init>(Landroid/content/Context;Ljava/net/URL;)V

    invoke-virtual {v0, p0}, Lcom/facebook/c/i;->a(Ljava/lang/Object;)Lcom/facebook/c/i;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/f;

    invoke-direct {v1, p0, p1, p3}, Lcom/sec/chaton/settings/tellfriends/common/f;-><init>(Lcom/sec/chaton/settings/tellfriends/common/d;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/c/i;->a(Lcom/facebook/c/j;)Lcom/facebook/c/i;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Lcom/facebook/c/i;->a()Lcom/facebook/c/g;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->g:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    invoke-static {v0}, Lcom/facebook/c/a;->a(Lcom/facebook/c/g;)V

    goto :goto_0

    .line 156
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected a(Lcom/sec/chaton/settings/tellfriends/ai;)Ljava/net/URL;
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/sec/chaton/settings/tellfriends/ai;->c()Ljava/lang/String;

    move-result-object v1

    .line 134
    if-eqz v1, :cond_0

    .line 136
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    return-object v0

    .line 137
    :catch_0
    move-exception v0

    .line 140
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(III)V
    .locals 5

    .prologue
    .line 206
    if-ge p2, p1, :cond_1

    .line 250
    :cond_0
    return-void

    :cond_1
    move v1, p2

    .line 210
    :goto_0
    if-ltz v1, :cond_3

    .line 211
    if-ltz v1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 213
    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v0

    .line 214
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->g:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/c/g;

    .line 215
    if-eqz v0, :cond_2

    .line 216
    invoke-static {v0}, Lcom/facebook/c/a;->b(Lcom/facebook/c/g;)V

    .line 210
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 221
    :cond_3
    const/4 v0, 0x0

    sub-int v1, p1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 222
    add-int v1, p2, p3

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/common/d;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 223
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 225
    :goto_1
    if-ge v1, p1, :cond_5

    .line 226
    if-ltz v1, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 228
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 231
    :cond_5
    add-int/lit8 v0, p2, 0x1

    move v1, v0

    :goto_2
    if-gt v1, v2, :cond_7

    .line 232
    if-ltz v1, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 234
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 238
    :cond_7
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 239
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Lcom/sec/chaton/settings/tellfriends/ai;)Ljava/net/URL;

    move-result-object v2

    .line 240
    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v0

    .line 242
    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v3

    .line 243
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    if-nez v3, :cond_8

    .line 246
    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Ljava/lang/String;Ljava/net/URL;Landroid/widget/ImageView;)V

    goto :goto_3
.end method

.method protected a(Landroid/view/View;Lcom/sec/chaton/settings/tellfriends/ai;)V
    .locals 3

    .prologue
    .line 90
    invoke-virtual {p2}, Lcom/sec/chaton/settings/tellfriends/ai;->a()Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 93
    const v0, 0x7f0702d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 94
    if-eqz v0, :cond_0

    .line 95
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->f:Lcom/sec/chaton/settings/tellfriends/common/h;

    if-eqz v2, :cond_2

    .line 96
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->f:Lcom/sec/chaton/settings/tellfriends/common/h;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/tellfriends/common/h;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 101
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->b:Lcom/sec/chaton/settings/tellfriends/common/g;

    if-eqz v2, :cond_0

    .line 102
    new-instance v2, Lcom/sec/chaton/settings/tellfriends/common/e;

    invoke-direct {v2, p0, p2}, Lcom/sec/chaton/settings/tellfriends/common/e;-><init>(Lcom/sec/chaton/settings/tellfriends/common/d;Lcom/sec/chaton/settings/tellfriends/ai;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :cond_0
    const v0, 0x7f07014b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 112
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->h:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->h:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/c/k;

    .line 114
    invoke-virtual {v1}, Lcom/facebook/c/k;->c()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 115
    invoke-virtual {v1}, Lcom/facebook/c/k;->a()Lcom/facebook/c/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/c/g;->b()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 122
    :goto_1
    const v0, 0x7f0704c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 123
    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Landroid/widget/ImageView;)V

    .line 125
    const v0, 0x7f07014c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 126
    if-eqz v0, :cond_1

    .line 127
    invoke-virtual {p2}, Lcom/sec/chaton/settings/tellfriends/ai;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :cond_1
    return-void

    .line 98
    :cond_2
    const v2, 0x7f0b01a3

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 117
    :cond_3
    invoke-virtual {p0, p2}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Lcom/sec/chaton/settings/tellfriends/ai;)Ljava/net/URL;

    move-result-object v2

    .line 119
    invoke-direct {p0, v1, v2, v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Ljava/lang/String;Ljava/net/URL;Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/g;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->b:Lcom/sec/chaton/settings/tellfriends/common/g;

    .line 50
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/common/h;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->f:Lcom/sec/chaton/settings/tellfriends/common/h;

    .line 87
    return-void
.end method

.method public addAll(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 68
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 69
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 73
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 76
    if-nez p2, :cond_0

    .line 77
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/d;->e:Landroid/view/LayoutInflater;

    const v2, 0x7f03012d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 80
    :cond_0
    invoke-virtual {p0, p2, v0}, Lcom/sec/chaton/settings/tellfriends/common/d;->a(Landroid/view/View;Lcom/sec/chaton/settings/tellfriends/ai;)V

    .line 82
    return-object p2
.end method
