.class public Lcom/sec/chaton/settings/downloads/SkinDetail;
.super Landroid/support/v4/app/Fragment;
.source "SkinDetail.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/common/f/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/sec/common/f/f;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/text/DateFormat;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Z

.field private h:I

.field private i:I

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/widget/ProgressBar;

.field private n:Lcom/sec/chaton/c/h;

.field private o:Lcom/sec/common/f/c;

.field private p:Lcom/sec/chaton/settings/downloads/a/r;

.field private q:Landroid/database/Cursor;

.field private r:Z

.field private s:Landroid/view/View;

.field private t:Landroid/view/Menu;

.field private u:Landroid/app/Activity;

.field private v:Lcom/sec/chaton/settings/downloads/cc;

.field private w:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/chaton/settings/downloads/ActivitySkinDetail;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/SkinDetail;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->f:J

    .line 113
    new-instance v0, Lcom/sec/chaton/settings/downloads/ca;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/ca;-><init>(Lcom/sec/chaton/settings/downloads/SkinDetail;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->w:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SkinDetail;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->q:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SkinDetail;Lcom/sec/chaton/settings/downloads/a/r;)Lcom/sec/chaton/settings/downloads/a/r;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/16 v8, 0x8

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 436
    const-wide/16 v0, 0x0

    .line 439
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 440
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 444
    const/4 v0, 0x0

    .line 446
    :try_start_0
    const-string v1, "extras"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 447
    invoke-static {v1}, Lcom/sec/chaton/e/a/ab;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ac;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 455
    :cond_0
    :goto_0
    const-string v1, "install"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->g:Z

    .line 458
    const-string v1, "expiration_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 459
    if-eqz v0, :cond_1

    .line 460
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/ac;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->e:Ljava/lang/String;

    .line 463
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/ac;->d()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->f:J

    .line 464
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/ac;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->a:Ljava/lang/String;

    :cond_1
    move-wide v0, v4

    .line 470
    :cond_2
    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 471
    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 473
    iget-boolean v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->g:Z

    if-eqz v4, :cond_6

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    const v1, 0x7f0200ab

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/cd;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    const v1, 0x7f0201cb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 485
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v1, v2

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->f:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "KB) / "

    aput-object v2, v1, v9

    const v2, 0x7f0b022c

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/widget/TextView;)V

    .line 586
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 587
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->e:Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->h:I

    iget v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->i:I

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;IIZ)V

    .line 588
    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a(I)V

    .line 589
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->o:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 592
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->s:Landroid/view/View;

    const v1, 0x7f070440

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    .line 593
    return-void

    .line 448
    :catch_0
    move-exception v1

    .line 449
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_0

    .line 450
    sget-object v4, Lcom/sec/chaton/settings/downloads/SkinDetail;->c:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    move v1, v3

    .line 455
    goto/16 :goto_1

    .line 482
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_2

    .line 492
    :cond_6
    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    if-nez v4, :cond_7

    .line 499
    new-instance v4, Ljava/sql/Date;

    invoke-direct {v4, v0, v1}, Ljava/sql/Date;-><init>(J)V

    .line 500
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 502
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    const v4, 0x7f0200ac

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 503
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 505
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, " ("

    aput-object v5, v4, v2

    iget-wide v5, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->f:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v3

    const-string v2, "KB)"

    aput-object v2, v4, v9

    const-string v2, " / "

    aput-object v2, v4, v10

    const/4 v2, 0x4

    const-string v5, "~"

    aput-object v5, v4, v2

    const/4 v2, 0x5

    aput-object v0, v4, v2

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 508
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 509
    :cond_7
    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    invoke-virtual {v4}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_8

    .line 510
    new-instance v4, Ljava/sql/Date;

    invoke-direct {v4, v0, v1}, Ljava/sql/Date;-><init>(J)V

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 513
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    const v4, 0x7f0200ad

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 514
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 516
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, " ("

    aput-object v5, v4, v2

    iget-wide v5, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->f:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v3

    const-string v2, "KB)"

    aput-object v2, v4, v9

    const-string v2, " / "

    aput-object v2, v4, v10

    const/4 v2, 0x4

    const-string v5, "~"

    aput-object v5, v4, v2

    const/4 v2, 0x5

    aput-object v0, v4, v2

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 521
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_3

    .line 524
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    const v1, 0x7f0200ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-nez v0, :cond_9

    .line 528
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 531
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    new-array v1, v10, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v1, v2

    const v2, 0x7f0b022a

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, ")"

    aput-object v2, v1, v9

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 535
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 539
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    const v1, 0x7f0200ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 542
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 545
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 547
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    new-array v1, v10, [Ljava/lang/Object;

    const-string v4, " ("

    aput-object v4, v1, v2

    iget-wide v4, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->f:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "KB)"

    aput-object v2, v1, v9

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 555
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 556
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    const v1, 0x7f0b022b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->d(Landroid/widget/TextView;)V

    goto/16 :goto_3

    .line 522
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/SkinDetail;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/SkinDetail;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/chaton/settings/downloads/SkinDetail;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/SkinDetail;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/high16 v4, 0x41a00000    # 20.0f

    .line 408
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 409
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 412
    if-nez v2, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 418
    if-eqz v2, :cond_0

    .line 421
    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 422
    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 424
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v4}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 426
    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 428
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/SkinDetail;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->g:Z

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 291
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/cb;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/cb;-><init>(Lcom/sec/chaton/settings/downloads/SkinDetail;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 362
    iput-object p2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->q:Landroid/database/Cursor;

    .line 365
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->r:Z

    if-eqz v0, :cond_0

    .line 366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->r:Z

    .line 369
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/r;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->a(Landroid/os/Handler;)V

    .line 376
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Landroid/database/Cursor;)V

    .line 377
    return-void
.end method

.method public a(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 386
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    const v1, 0x7f02035d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 389
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 394
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 155
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    .line 156
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 318
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->g:Z

    if-eqz v0, :cond_2

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/cd;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 320
    if-eqz v0, :cond_1

    .line 321
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->g(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->v:Lcom/sec/chaton/settings/downloads/cc;

    invoke-interface {v0}, Lcom/sec/chaton/settings/downloads/cc;->c()V

    .line 346
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->q:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 325
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->h(Landroid/content/Context;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 328
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    if-nez v0, :cond_3

    .line 329
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->n:Lcom/sec/chaton/c/h;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/a/r;-><init>(Lcom/sec/chaton/c/h;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->a(Landroid/os/Handler;)V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 332
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 333
    new-instance v0, Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->n:Lcom/sec/chaton/c/h;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/a/r;-><init>(Lcom/sec/chaton/c/h;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->a(Landroid/os/Handler;)V

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->d([Ljava/lang/Object;)Lcom/sec/common/util/a;

    goto :goto_1

    .line 337
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/r;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 341
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->a(Z)Z

    goto :goto_1

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x7f0701d2
        :pswitch_0
    .end packed-switch

    .line 337
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 399
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 401
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->c()V

    .line 402
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 240
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ITEM_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    .line 248
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 354
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "item_id"

    aput-object v1, v0, v2

    const-string v1, "=?"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 355
    new-array v5, v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->d:Ljava/lang/String;

    aput-object v0, v5, v2

    .line 357
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const v1, 0x7f0705ab

    .line 214
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 216
    const v0, 0x7f0f0027

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 218
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->t:Landroid/view/Menu;

    .line 219
    iget-boolean v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->g:Z

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->t:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 225
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->t:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 168
    const v0, 0x7f0300f4

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 169
    iput-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->s:Landroid/view/View;

    .line 171
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->b:Ljava/text/DateFormat;

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/settings/downloads/cc;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->v:Lcom/sec/chaton/settings/downloads/cc;

    .line 175
    new-instance v0, Lcom/sec/chaton/c/h;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    invoke-direct {v0, v2, v5}, Lcom/sec/chaton/c/h;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->n:Lcom/sec/chaton/c/h;

    .line 176
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->o:Lcom/sec/common/f/c;

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->o:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 183
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->h:I

    .line 184
    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->i:I

    .line 192
    const v0, 0x7f070262

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->j:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 196
    const v0, 0x7f0701d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    const v0, 0x7f070440

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->l:Landroid/widget/ImageView;

    .line 201
    const v0, 0x7f07043f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->m:Landroid/widget/ProgressBar;

    .line 204
    iput-boolean v3, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->r:Z

    .line 205
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 207
    invoke-virtual {p0, v3}, Lcom/sec/chaton/settings/downloads/SkinDetail;->setHasOptionsMenu(Z)V

    .line 209
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 267
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->p:Lcom/sec/chaton/settings/downloads/a/r;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/r;->b(Landroid/os/Handler;)V

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->o:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->o:Lcom/sec/common/f/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->o:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 282
    :cond_1
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/SkinDetail;->u:Landroid/app/Activity;

    .line 163
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 382
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 229
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 232
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->a()V

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x7f0705ab
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 260
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 262
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->stopLoading()V

    .line 263
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 253
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 255
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/SkinDetail;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->startLoading()V

    .line 256
    return-void
.end method
