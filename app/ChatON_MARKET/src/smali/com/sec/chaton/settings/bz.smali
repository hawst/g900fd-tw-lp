.class Lcom/sec/chaton/settings/bz;
.super Landroid/os/Handler;
.source "ActivityPrivacy.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V
    .locals 0

    .prologue
    .line 649
    iput-object p1, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const v5, 0x7f0b0205

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 652
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 654
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->d(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 658
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 659
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 662
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 665
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 666
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_3

    .line 667
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->i(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    .line 669
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->d(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0089

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 671
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->j(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "show_phone_number_to_all"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 672
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->k(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "samsung_account_show"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 674
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->d(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 679
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 680
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_5

    .line 681
    iget-object v2, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Lcom/sec/chaton/settings/ActivityPrivacy;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 682
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPrivacyList;

    .line 683
    if-eqz v0, :cond_0

    .line 687
    iget-object v2, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPrivacyList;->privacy:Ljava/util/ArrayList;

    invoke-static {v2, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Lcom/sec/chaton/settings/ActivityPrivacy;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 688
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->l(Lcom/sec/chaton/settings/ActivityPrivacy;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 689
    iget-object v2, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->l(Lcom/sec/chaton/settings/ActivityPrivacy;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->l(Lcom/sec/chaton/settings/ActivityPrivacy;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->_value:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Lcom/sec/chaton/settings/ActivityPrivacy;Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 691
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->i(Lcom/sec/chaton/settings/ActivityPrivacy;)V

    .line 692
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 697
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/settings/ActivityPrivacy;->a(Lcom/sec/chaton/settings/ActivityPrivacy;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 698
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->j(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 699
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->k(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 700
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 701
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->b(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 704
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings/bz;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->d(Lcom/sec/chaton/settings/ActivityPrivacy;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 662
    nop

    :sswitch_data_0
    .sparse-switch
        0x130 -> :sswitch_0
        0x142 -> :sswitch_1
    .end sparse-switch
.end method
