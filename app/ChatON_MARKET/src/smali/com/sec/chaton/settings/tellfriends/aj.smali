.class public Lcom/sec/chaton/settings/tellfriends/aj;
.super Lcom/sec/chaton/settings/tellfriends/aa;
.source "SnsHelperFacebook.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/g/a;

.field private c:Lcom/sec/chaton/settings/tellfriends/common/c;

.field private d:Lcom/sec/chaton/settings/tellfriends/ad;

.field private e:Lcom/sec/chaton/settings/tellfriends/ah;

.field private f:Lcom/sec/chaton/settings/tellfriends/ag;

.field private g:Lcom/sec/chaton/settings/tellfriends/af;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/aj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/aa;-><init>()V

    .line 45
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ak;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/ak;-><init>(Lcom/sec/chaton/settings/tellfriends/aj;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    .line 34
    new-instance v0, Lcom/sec/chaton/g/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/aj;->c:Lcom/sec/chaton/settings/tellfriends/common/c;

    invoke-direct {v0, p1, v1}, Lcom/sec/chaton/g/a;-><init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ad;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->d:Lcom/sec/chaton/settings/tellfriends/ad;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ah;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->e:Lcom/sec/chaton/settings/tellfriends/ah;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/ag;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->f:Lcom/sec/chaton/settings/tellfriends/ag;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/aj;)Lcom/sec/chaton/settings/tellfriends/af;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->g:Lcom/sec/chaton/settings/tellfriends/af;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/aj;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(II)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 219
    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 220
    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 222
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v3}, Lcom/sec/chaton/g/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 224
    const-string v3, "width"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 225
    const-string v0, "height"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 227
    const-string v0, "migration_overrides"

    const-string v1, "{october_2012:true}"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 229
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    const/16 v0, 0x258

    .line 210
    if-eqz p1, :cond_0

    .line 211
    invoke-virtual {p0, v0, v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 213
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0}, Lcom/sec/chaton/g/a;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0}, Lcom/sec/chaton/g/a;->g()V

    .line 41
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/g/a;->a(IILandroid/content/Intent;)V

    .line 237
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ac;)V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/ad;)V
    .locals 1

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/aj;->d:Lcom/sec/chaton/settings/tellfriends/ad;

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0}, Lcom/sec/chaton/g/a;->f()V

    .line 151
    return-void
.end method

.method public a(Lcom/sec/chaton/settings/tellfriends/af;)V
    .locals 2

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/aj;->g:Lcom/sec/chaton/settings/tellfriends/af;

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/chaton/g/a;->a(I)V

    .line 188
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;)V
    .locals 2

    .prologue
    .line 169
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/aj;->e:Lcom/sec/chaton/settings/tellfriends/ah;

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/sec/chaton/g/a;->a(ILjava/lang/String;)V

    .line 171
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/ah;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 162
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/aj;->e:Lcom/sec/chaton/settings/tellfriends/ah;

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p3}, Lcom/sec/chaton/g/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0}, Lcom/sec/chaton/g/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0}, Lcom/sec/chaton/g/a;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0}, Lcom/sec/chaton/g/a;->e()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/aj;->b:Lcom/sec/chaton/g/a;

    invoke-virtual {v0}, Lcom/sec/chaton/g/a;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
