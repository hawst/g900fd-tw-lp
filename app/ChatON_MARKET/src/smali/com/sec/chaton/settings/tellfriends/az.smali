.class Lcom/sec/chaton/settings/tellfriends/az;
.super Landroid/webkit/WebViewClient;
.source "TwDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/ar;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/ar;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/ar;Lcom/sec/chaton/settings/tellfriends/as;)V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/az;-><init>(Lcom/sec/chaton/settings/tellfriends/ar;)V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onLoadResource URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v0, "javascripts"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->g(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 357
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "On Page Finished URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->c(Lcom/sec/chaton/settings/tellfriends/ar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->d(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->e(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->a(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->f(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 300
    sget-object v0, Lcom/sec/chaton/c/b;->n:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->g(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 302
    const-string v0, "mUserPinCode null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string v0, "javascript:window.PinCode.getPinCode(document.getElementsByTagName(\'code\')[0].innerHTML);"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 308
    :cond_1
    :goto_0
    return-void

    .line 305
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mUserPinCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/ar;->g(Lcom/sec/chaton/settings/tellfriends/ar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageStarted URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->c(Lcom/sec/chaton/settings/tellfriends/ar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->d(Lcom/sec/chaton/settings/tellfriends/ar;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 317
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 321
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->b(Lcom/sec/chaton/settings/tellfriends/ar;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/a;

    invoke-direct {v1, p3, p2, p4}, Lcom/sec/chaton/settings/tellfriends/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a(Lcom/sec/chaton/settings/tellfriends/common/a;)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ar;->dismiss()V

    .line 325
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 329
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shouldOverrideUrlLoading URL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v1, "http://www.chaton.com/"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 331
    invoke-static {p2}, Lcom/facebook/a/f;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 333
    const-string v2, "denied"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 334
    if-eqz v1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/ar;->b(Lcom/sec/chaton/settings/tellfriends/ar;)Lcom/sec/chaton/settings/tellfriends/common/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/chaton/settings/tellfriends/common/b;->a()V

    .line 336
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/ar;->dismiss()V

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 340
    :cond_1
    const-string v1, "authorize"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 341
    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/az;->a:Lcom/sec/chaton/settings/tellfriends/ar;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/tellfriends/ar;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
