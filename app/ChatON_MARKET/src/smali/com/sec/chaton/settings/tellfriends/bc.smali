.class Lcom/sec/chaton/settings/tellfriends/bc;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "TwitterManager.java"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/sec/chaton/settings/tellfriends/ba;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;)V
    .locals 1

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bc;->b:Lcom/sec/chaton/settings/tellfriends/ba;

    .line 207
    const-string v0, "GetFriendsIDsTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bc;->a:Ljava/util/ArrayList;

    .line 208
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/ba;Lcom/sec/chaton/settings/tellfriends/bb;)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bc;-><init>(Lcom/sec/chaton/settings/tellfriends/ba;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 212
    .line 216
    const-wide/16 v1, -0x1

    .line 218
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/ba;->g()Ltwitter4j/Twitter;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Ltwitter4j/Twitter;->getFriendsIDs(J)Ltwitter4j/IDs;

    move-result-object v2

    .line 219
    invoke-interface {v2}, Ltwitter4j/IDs;->getIDs()[J

    move-result-object v3

    move v1, v0

    .line 220
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_1

    .line 221
    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/bc;->a:Ljava/util/ArrayList;

    aget-wide v5, v3, v1

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    :cond_1
    invoke-interface {v2}, Ltwitter4j/IDs;->getNextCursor()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    .line 225
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bc;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [J

    move v2, v0

    .line 226
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v1, v2

    .line 226
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 231
    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/bc;->e:I
    :try_end_0
    .catch Ltwitter4j/TwitterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    .line 242
    :goto_2
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bc;->f:Ljava/lang/Object;

    .line 243
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bc;->e:I

    return v0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->printStackTrace()V

    .line 234
    const/16 v1, -0x3e9

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bc;->e:I

    .line 235
    invoke-virtual {v0}, Ltwitter4j/TwitterException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 236
    :catch_1
    move-exception v0

    .line 237
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 238
    const/16 v1, -0x3e8

    iput v1, p0, Lcom/sec/chaton/settings/tellfriends/bc;->e:I

    .line 239
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
