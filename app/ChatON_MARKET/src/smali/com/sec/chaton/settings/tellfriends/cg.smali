.class Lcom/sec/chaton/settings/tellfriends/cg;
.super Ljava/lang/Object;
.source "WeiboTextInputLayout.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/cg;->a:Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/cg;->b:Ljava/lang/String;

    .line 165
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/cg;->b:Ljava/lang/String;

    .line 166
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 170
    .line 171
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    move v1, v0

    .line 176
    :goto_0
    if-ge v1, v4, :cond_1

    .line 177
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 178
    const/16 v6, 0xa

    if-eq v5, v6, :cond_0

    .line 179
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 184
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 185
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, p2

    .line 188
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/cg;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 189
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 190
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    move v1, v0

    .line 191
    :goto_1
    if-ge v1, v4, :cond_3

    .line 192
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 193
    iget-object v6, p0, Lcom/sec/chaton/settings/tellfriends/cg;->b:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    .line 194
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 196
    :cond_2
    const/4 v0, 0x1

    goto :goto_2

    .line 200
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, p2

    .line 203
    if-eqz v0, :cond_4

    move-object v0, v1

    .line 206
    :goto_3
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method
