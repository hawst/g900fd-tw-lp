.class Lcom/sec/chaton/settings/tellfriends/common/k;
.super Ljava/lang/Object;
.source "SnsFriendsPickerFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/ad;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 438
    const-string v0, "onCancelled get friend list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->f(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 441
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->g(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->h(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    .line 447
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/settings/tellfriends/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 416
    const-string v0, "onComplete get friend list"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 419
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/ai;

    .line 420
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->c(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Lcom/sec/chaton/settings/tellfriends/common/n;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/settings/tellfriends/common/n;->b:Lcom/sec/chaton/settings/tellfriends/common/n;

    if-ne v2, v3, :cond_1

    .line 421
    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ai;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 422
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 425
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 428
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->d(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;Ljava/util/ArrayList;)[J

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->a(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;[J)[J

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->e(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)[J

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->b(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;[J)V

    .line 434
    :goto_1
    return-void

    .line 431
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/common/k;->a:Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;->f(Lcom/sec/chaton/settings/tellfriends/common/SnsFriendsPickerFragment;)V

    goto :goto_1
.end method
