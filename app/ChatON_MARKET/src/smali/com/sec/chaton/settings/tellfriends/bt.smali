.class public Lcom/sec/chaton/settings/tellfriends/bt;
.super Lcom/sec/chaton/settings/tellfriends/common/o;
.source "WeiboManager.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static d:Lweibo4android/Weibo;

.field private static e:Lweibo4android/http/RequestToken;


# instance fields
.field private f:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/chaton/settings/tellfriends/bt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/settings/tellfriends/common/o;-><init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;Ljava/lang/String;)V

    .line 404
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/bu;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bt;->f:Landroid/os/Handler;

    .line 42
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lweibo4android/Weibo;

    invoke-direct {v0}, Lweibo4android/Weibo;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    .line 44
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/tellfriends/cc;->a(Lweibo4android/Weibo;Landroid/content/Context;)Z

    .line 46
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/bt;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bt;->f:Landroid/os/Handler;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 118
    invoke-static {p0}, Lcom/sec/chaton/settings/tellfriends/cc;->a(Landroid/content/Context;)V

    .line 119
    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    .line 120
    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->e:Lweibo4android/http/RequestToken;

    .line 121
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bt;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lweibo4android/http/RequestToken;)Lweibo4android/http/RequestToken;
    .locals 0

    .prologue
    .line 32
    sput-object p0, Lcom/sec/chaton/settings/tellfriends/bt;->e:Lweibo4android/http/RequestToken;

    return-object p0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bt;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bt;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bt;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bt;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f()Lweibo4android/Weibo;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bt;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g()Lweibo4android/http/RequestToken;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->e:Lweibo4android/http/RequestToken;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 50
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lweibo4android/Weibo;

    invoke-direct {v0}, Lweibo4android/Weibo;-><init>()V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    .line 52
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/tellfriends/cc;->a(Lweibo4android/Weibo;Landroid/content/Context;)Z

    .line 55
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->e:Lweibo4android/http/RequestToken;

    if-nez v0, :cond_1

    .line 56
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/by;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/tellfriends/by;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;Lcom/sec/chaton/settings/tellfriends/bu;)V

    .line 57
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    const/16 v2, 0x2ee5

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 62
    :goto_0
    return-void

    .line 59
    :cond_1
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/bt;->e:Lweibo4android/http/RequestToken;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/bt;->a(Lweibo4android/http/RequestToken;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 113
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p3, v1}, Lcom/sec/chaton/settings/tellfriends/bz;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/bu;)V

    .line 114
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 115
    return-void
.end method

.method public a(Lweibo4android/http/RequestToken;)V
    .locals 5

    .prologue
    .line 124
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bl;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/settings/tellfriends/bt;->d:Lweibo4android/Weibo;

    new-instance v3, Lcom/sec/chaton/settings/tellfriends/ca;

    const/16 v4, 0x2ee4

    invoke-direct {v3, p0, v4}, Lcom/sec/chaton/settings/tellfriends/ca;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;I)V

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/sec/chaton/settings/tellfriends/bl;-><init>(Landroid/content/Context;Lweibo4android/Weibo;Lweibo4android/http/RequestToken;Lcom/sec/chaton/settings/tellfriends/common/b;)V

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/bl;->show()V

    .line 126
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/cc;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/cc;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 4

    .prologue
    .line 95
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/tellfriends/bw;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;Lcom/sec/chaton/settings/tellfriends/bu;)V

    .line 96
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 97
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/cc;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(I)V
    .locals 4

    .prologue
    .line 101
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/bv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings/tellfriends/bv;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;Lcom/sec/chaton/settings/tellfriends/bu;)V

    .line 102
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 103
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/cc;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
