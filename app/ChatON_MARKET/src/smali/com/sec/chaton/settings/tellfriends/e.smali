.class public Lcom/sec/chaton/settings/tellfriends/e;
.super Landroid/app/Dialog;
.source "RenrenDialog.java"


# static fields
.field static final a:Landroid/widget/FrameLayout$LayoutParams;


# instance fields
.field b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/chaton/settings/tellfriends/common/b;

.field private e:Landroid/app/ProgressDialog;

.field private f:Landroid/webkit/WebView;

.field private g:Landroid/widget/FrameLayout;

.field private h:Landroid/widget/ImageView;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 35
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/e;->a:Landroid/widget/FrameLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/chaton/settings/tellfriends/common/b;)V
    .locals 3

    .prologue
    .line 57
    const v0, 0x1030010

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->i:Z

    .line 58
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/e;->b:Landroid/content/Context;

    .line 60
    if-nez p3, :cond_0

    .line 61
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 63
    :cond_0
    const-string v0, "display"

    const-string v1, "touch"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    sget-object v0, Lcom/sec/chaton/c/b;->v:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dialog/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lcom/renren/android/g;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->c:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/sec/chaton/settings/tellfriends/e;->d:Lcom/sec/chaton/settings/tellfriends/common/b;

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;)V
    .locals 1

    .prologue
    .line 50
    const v0, 0x1030010

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->i:Z

    .line 51
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/e;->b:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/sec/chaton/settings/tellfriends/e;->c:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/e;->d:Lcom/sec/chaton/settings/tellfriends/common/b;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 128
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->h:Landroid/widget/ImageView;

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/h;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/h;-><init>(Lcom/sec/chaton/settings/tellfriends/e;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->h:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 144
    return-void
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 110
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 111
    new-instance v1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    .line 112
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 113
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 114
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 115
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/settings/tellfriends/j;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/settings/tellfriends/j;-><init>(Lcom/sec/chaton/settings/tellfriends/e;Lcom/sec/chaton/settings/tellfriends/f;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 116
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/e;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 119
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 120
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 122
    invoke-virtual {v0, p1, p1, p1, p1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 123
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 124
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 125
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/e;)Lcom/sec/chaton/settings/tellfriends/common/b;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->d:Lcom/sec/chaton/settings/tellfriends/common/b;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/e;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->i:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->g:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/e;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->h:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 163
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->i:Z

    if-nez v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 167
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 169
    :cond_2
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->i:Z

    .line 155
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 156
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 72
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 74
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/f;-><init>(Lcom/sec/chaton/settings/tellfriends/e;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/e;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 83
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b000d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->e:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/tellfriends/g;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/tellfriends/g;-><init>(Lcom/sec/chaton/settings/tellfriends/e;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 95
    invoke-virtual {p0, v5}, Lcom/sec/chaton/settings/tellfriends/e;->requestWindowFeature(I)Z

    .line 96
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/tellfriends/e;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->g:Landroid/widget/FrameLayout;

    .line 98
    invoke-direct {p0}, Lcom/sec/chaton/settings/tellfriends/e;->a()V

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 101
    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings/tellfriends/e;->a(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/e;->h:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->g:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings/tellfriends/e;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/tellfriends/e;->i:Z

    .line 149
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 150
    return-void
.end method
