.class Lcom/sec/chaton/settings/dp;
.super Landroid/widget/BaseAdapter;
.source "FragmentFontChange.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/FragmentFontChange;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/FragmentFontChange;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/chaton/settings/dp;->a:Lcom/sec/chaton/settings/FragmentFontChange;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/chaton/settings/dp;->a:Lcom/sec/chaton/settings/FragmentFontChange;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentFontChange;->a(Lcom/sec/chaton/settings/FragmentFontChange;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/settings/dp;->a:Lcom/sec/chaton/settings/FragmentFontChange;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentFontChange;->a(Lcom/sec/chaton/settings/FragmentFontChange;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 143
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 153
    .line 155
    iget-object v0, p0, Lcom/sec/chaton/settings/dp;->a:Lcom/sec/chaton/settings/FragmentFontChange;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentFontChange;->b(Lcom/sec/chaton/settings/FragmentFontChange;)Lcom/sec/chaton/settings/ActivityFontChange;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03011e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 157
    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 158
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 161
    iget-object v2, p0, Lcom/sec/chaton/settings/dp;->a:Lcom/sec/chaton/settings/FragmentFontChange;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentFontChange;->a(Lcom/sec/chaton/settings/FragmentFontChange;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/io/entry/inner/FontType;

    .line 162
    iget-object v3, p0, Lcom/sec/chaton/settings/dp;->a:Lcom/sec/chaton/settings/FragmentFontChange;

    invoke-static {v3}, Lcom/sec/chaton/settings/FragmentFontChange;->c(Lcom/sec/chaton/settings/FragmentFontChange;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    invoke-virtual {v2}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontType()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 165
    return-object v1
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 148
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 149
    return-void
.end method
