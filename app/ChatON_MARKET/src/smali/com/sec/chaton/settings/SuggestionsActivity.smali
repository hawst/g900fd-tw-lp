.class public Lcom/sec/chaton/settings/SuggestionsActivity;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "SuggestionsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Z

.field private d:Lcom/sec/chaton/util/ab;

.field private e:Lcom/sec/chaton/d/h;

.field private f:Lcom/sec/chaton/d/h;

.field private g:Landroid/preference/CheckBoxPreference;

.field private h:Landroid/preference/CheckBoxPreference;

.field private i:Landroid/app/ProgressDialog;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/PrivacyList;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/os/Handler;

.field private l:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->b:Z

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->c:Z

    .line 62
    iput-object v1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    .line 69
    iput-object v1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    .line 113
    new-instance v0, Lcom/sec/chaton/settings/fg;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/fg;-><init>(Lcom/sec/chaton/settings/SuggestionsActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->k:Landroid/os/Handler;

    .line 400
    new-instance v0, Lcom/sec/chaton/settings/fj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/fj;-><init>(Lcom/sec/chaton/settings/SuggestionsActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->l:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/SuggestionsActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->j:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a()V
    .locals 6

    .prologue
    const v5, 0x7f08003f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 180
    const-string v0, "pref_item_buddy_sugestions"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->g:Landroid/preference/CheckBoxPreference;

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "recomned_receive"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 182
    iput-boolean v1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->b:Z

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "recomned_receive"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->g:Landroid/preference/CheckBoxPreference;

    iget-boolean v3, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->b:Z

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 189
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0218

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {p0, v0, v3, v4}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->g:Landroid/preference/CheckBoxPreference;

    new-instance v3, Lcom/sec/chaton/settings/fh;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings/fh;-><init>(Lcom/sec/chaton/settings/SuggestionsActivity;)V

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "recomned_special"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "recomned_special"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 281
    :cond_0
    const-string v0, "pref_item_exclude_me"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->h:Landroid/preference/CheckBoxPreference;

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "exclude_me"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 285
    iput-boolean v2, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->c:Z

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "exclude_me"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 292
    :goto_1
    iget-object v3, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->h:Landroid/preference/CheckBoxPreference;

    iget-boolean v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->c:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 293
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0217

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->h:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings/fi;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/fi;-><init>(Lcom/sec/chaton/settings/SuggestionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0216

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 335
    return-void

    .line 185
    :cond_1
    iput-boolean v2, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->b:Z

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "recomned_receive"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 288
    :cond_2
    iput-boolean v1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->c:Z

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v3, "exclude_me"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 292
    goto :goto_2
.end method

.method static synthetic a(Lcom/sec/chaton/settings/SuggestionsActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings/SuggestionsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 343
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 344
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 345
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 346
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 153
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 154
    :cond_0
    const-string v0, "have no privacy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_1
    :goto_0
    return-void

    .line 156
    :cond_2
    const-string v0, "recommendationbuddy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 157
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_receive"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 164
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recommendationbuddy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_receive"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    .line 165
    :cond_4
    const-string v0, "ignorerecommendation"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v1, "exclude_me"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 173
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ignorerecommendation : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 170
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v1, "exclude_me"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/sec/chaton/settings/SuggestionsActivity;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/SuggestionsActivity;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->c:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/settings/SuggestionsActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/settings/SuggestionsActivity;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/settings/SuggestionsActivity;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->e:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/SuggestionsActivity;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->b:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/settings/SuggestionsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->g:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 387
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 339
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 340
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iput-object p0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->a:Landroid/content/Context;

    .line 77
    invoke-static {p0, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 84
    const v0, 0x7f050005

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/SuggestionsActivity;->addPreferencesFromResource(I)V

    .line 86
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->l:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->e:Lcom/sec/chaton/d/h;

    .line 87
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->d:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    .line 93
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_0
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->k:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->f:Lcom/sec/chaton/d/h;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->f:Lcom/sec/chaton/d/h;

    const-string v1, "recommendationbuddy|ignorerecommendation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->g(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 102
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 363
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onDestroy()V

    .line 365
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 367
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/SuggestionsActivity;->i:Landroid/app/ProgressDialog;

    .line 370
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 351
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onPause()V

    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 376
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onRestart()V

    .line 377
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 357
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onResume()V

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 106
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/sec/chaton/settings/SuggestionsActivity;->finish()V

    .line 108
    const/4 v0, 0x1

    .line 110
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 382
    return-void
.end method
