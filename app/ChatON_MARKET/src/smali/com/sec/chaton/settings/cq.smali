.class Lcom/sec/chaton/settings/cq;
.super Ljava/lang/Object;
.source "CustomTimePicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/co;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 299
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 325
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->e(Lcom/sec/chaton/settings/co;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v1}, Lcom/sec/chaton/settings/co;->g(Lcom/sec/chaton/settings/co;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->c(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v1}, Lcom/sec/chaton/settings/co;->b(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/co;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 330
    :goto_1
    return-void

    .line 301
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->i(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 304
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->j(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 307
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->k(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 310
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->l(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 313
    :pswitch_5
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->m(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 316
    :pswitch_6
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->n(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 319
    :pswitch_7
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->o(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 322
    :pswitch_8
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->p(Lcom/sec/chaton/settings/co;)V

    goto :goto_0

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/cq;->a:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings/co;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 299
    :pswitch_data_0
    .packed-switch 0x7f070051
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
