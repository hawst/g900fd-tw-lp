.class Lcom/sec/chaton/settings/by;
.super Ljava/lang/Object;
.source "ActivityPrivacy.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityPrivacy;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/sec/chaton/settings/by;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/chaton/settings/by;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->g(Lcom/sec/chaton/settings/ActivityPrivacy;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/by;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-static {v1}, Lcom/sec/chaton/settings/ActivityPrivacy;->h(Lcom/sec/chaton/settings/ActivityPrivacy;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {}, Lcom/sec/chaton/util/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/by;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    const-string v1, "MODE"

    const-string v2, "PRIVACY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    iget-object v1, p0, Lcom/sec/chaton/settings/by;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->startActivity(Landroid/content/Intent;)V

    .line 386
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 381
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings/by;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 384
    iget-object v1, p0, Lcom/sec/chaton/settings/by;->a:Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/ActivityPrivacy;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
