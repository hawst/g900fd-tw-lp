.class Lcom/sec/chaton/settings/tellfriends/bv;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "WeiboManager.java"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/sec/chaton/settings/tellfriends/bt;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;)V
    .locals 1

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bv;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    .line 194
    const-string v0, "GetFriendsIDsTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bv;->a:Ljava/util/ArrayList;

    .line 195
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;Lcom/sec/chaton/settings/tellfriends/bu;)V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bv;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v8, -0x3ed

    .line 199
    .line 204
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/bv;->b:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v2, v2, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/chaton/settings/tellfriends/cc;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 207
    :cond_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/bt;->f()Lweibo4android/Weibo;

    move-result-object v3

    const/16 v4, 0x1388

    invoke-virtual {v3, v2, v1, v4}, Lweibo4android/Weibo;->getFriendsListIDS_test2(Ljava/lang/String;II)Lweibo4android/IDs;

    move-result-object v3

    .line 208
    invoke-virtual {v3}, Lweibo4android/IDs;->getIDs()[J

    move-result-object v4

    move v1, v0

    .line 209
    :goto_0
    array-length v5, v4

    if-ge v1, v5, :cond_1

    .line 210
    iget-object v5, p0, Lcom/sec/chaton/settings/tellfriends/bv;->a:Ljava/util/ArrayList;

    aget-wide v6, v4, v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 212
    :cond_1
    invoke-virtual {v3}, Lweibo4android/IDs;->getNextCursor()J

    move-result-wide v3

    long-to-int v1, v3

    if-nez v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/bv;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [J

    move v2, v0

    .line 215
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v1, v2

    .line 215
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 220
    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/bv;->e:I
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    .line 231
    :goto_2
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bv;->f:Ljava/lang/Object;

    .line 232
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bv;->e:I

    return v0

    .line 221
    :catch_0
    move-exception v0

    .line 222
    invoke-virtual {v0}, Lweibo4android/WeiboException;->printStackTrace()V

    .line 223
    iput v8, p0, Lcom/sec/chaton/settings/tellfriends/bv;->e:I

    .line 224
    invoke-virtual {v0}, Lweibo4android/WeiboException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 225
    :catch_1
    move-exception v0

    .line 226
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 227
    iput v8, p0, Lcom/sec/chaton/settings/tellfriends/bv;->e:I

    .line 228
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
