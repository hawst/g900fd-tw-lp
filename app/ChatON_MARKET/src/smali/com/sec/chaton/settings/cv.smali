.class Lcom/sec/chaton/settings/cv;
.super Ljava/lang/Object;
.source "CustomTimePicker.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/co;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/co;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/chaton/settings/cv;->a:Lcom/sec/chaton/settings/co;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5

    .prologue
    .line 194
    if-nez p2, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings/cv;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->a(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/settings/cv;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->a(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "00"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/cv;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v0}, Lcom/sec/chaton/settings/co;->a(Lcom/sec/chaton/settings/co;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "%02d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/settings/cv;->a:Lcom/sec/chaton/settings/co;

    invoke-static {v4}, Lcom/sec/chaton/settings/co;->g(Lcom/sec/chaton/settings/co;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
