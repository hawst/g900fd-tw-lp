.class Lcom/sec/chaton/settings/ea;
.super Ljava/lang/Object;
.source "FragmentSkinChange3.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/FragmentSkinChange3;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/FragmentSkinChange3;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 369
    .line 371
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/ej;

    .line 372
    if-nez v0, :cond_1

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;)I

    move-result v1

    sget v2, Lcom/sec/chaton/settings/FragmentSkinChange3;->a:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;)I

    move-result v1

    sget v2, Lcom/sec/chaton/settings/FragmentSkinChange3;->b:I

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->c(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;)I

    move-result v1

    sget v2, Lcom/sec/chaton/settings/FragmentSkinChange3;->c:I

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->d(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 380
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->e(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->e(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 383
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0b00f5

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->e(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 389
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;)I

    move-result v1

    sget v2, Lcom/sec/chaton/settings/FragmentSkinChange3;->c:I

    if-ne v1, v2, :cond_e

    .line 390
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    const-string v2, "skin_add"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 391
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->f(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->f(Lcom/sec/chaton/settings/FragmentSkinChange3;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;Ljava/io/File;)V

    goto/16 :goto_0

    .line 397
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->g(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_8

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    const v3, 0x7f0b003d

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Lcom/sec/chaton/settings/FragmentSkinChange3;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 400
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->g(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 402
    :cond_9
    iget-object v1, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    const-string v2, "skin_myskin.png_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 403
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->h(Lcom/sec/chaton/settings/FragmentSkinChange3;)V

    .line 404
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->i(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/GridView;

    move-result-object v1

    if-nez v1, :cond_a

    .line 405
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 407
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 410
    :cond_b
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/downloads/cd;->f(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/ck;

    move-result-object v1

    .line 412
    if-eqz v1, :cond_0

    .line 415
    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    const-string v3, "pa"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 416
    iget-object v2, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/ck;->b:Landroid/graphics/Bitmap;

    invoke-direct {v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {v2, v3}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    .line 417
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->j(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 418
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->k(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 419
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->k(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentSkinChange3;->j(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 423
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->i(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/GridView;

    move-result-object v1

    if-nez v1, :cond_d

    .line 424
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 421
    :cond_c
    iget-object v2, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentSkinChange3;->k(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/ck;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 426
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 429
    :cond_e
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;)I

    move-result v1

    sget v2, Lcom/sec/chaton/settings/FragmentSkinChange3;->b:I

    if-ne v1, v2, :cond_10

    .line 430
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/downloads/cd;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 431
    if-eqz v1, :cond_0

    .line 432
    iget-object v2, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentSkinChange3;->l(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 433
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->i(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/GridView;

    move-result-object v1

    if-nez v1, :cond_f

    .line 434
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 436
    :cond_f
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 441
    :cond_10
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Lcom/sec/chaton/settings/FragmentSkinChange3;)I

    move-result v1

    sget v2, Lcom/sec/chaton/settings/FragmentSkinChange3;->a:I

    if-ne v1, v2, :cond_0

    .line 442
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/settings/downloads/cd;->g(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 443
    if-eqz v1, :cond_0

    .line 444
    iget-object v2, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentSkinChange3;->m(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 445
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    invoke-static {v1}, Lcom/sec/chaton/settings/FragmentSkinChange3;->i(Lcom/sec/chaton/settings/FragmentSkinChange3;)Landroid/widget/GridView;

    move-result-object v1

    if-nez v1, :cond_11

    .line 446
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 448
    :cond_11
    iget-object v1, p0, Lcom/sec/chaton/settings/ea;->a:Lcom/sec/chaton/settings/FragmentSkinChange3;

    iget-object v2, v0, Lcom/sec/chaton/settings/ej;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/settings/ej;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/settings/FragmentSkinChange3;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
