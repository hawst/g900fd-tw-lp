.class Lcom/sec/chaton/settings/tellfriends/ce;
.super Ljava/lang/Object;
.source "WeiboTextInputLayout.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ce;->a:Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 63
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ce;->a:Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->a(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)Lcom/sec/chaton/settings/tellfriends/cf;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/tellfriends/cf;->a(Lcom/sec/chaton/settings/tellfriends/cf;Ljava/lang/String;)I

    move-result v0

    .line 67
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ce;->a:Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->c(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "(%d/%d)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/sec/chaton/settings/tellfriends/ce;->a:Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;

    invoke-static {v4}, Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;->b(Lcom/sec/chaton/settings/tellfriends/WeiboTextInputLayout;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method
