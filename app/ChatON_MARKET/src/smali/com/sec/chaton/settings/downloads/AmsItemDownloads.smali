.class public Lcom/sec/chaton/settings/downloads/AmsItemDownloads;
.super Landroid/support/v4/app/Fragment;
.source "AmsItemDownloads.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/chaton/base/e;
.implements Lcom/sec/chaton/settings/downloads/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/sec/chaton/base/e;",
        "Lcom/sec/chaton/settings/downloads/d;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Lcom/sec/chaton/d/e;

.field private d:Landroid/widget/GridView;

.field private e:Landroid/app/ProgressDialog;

.field private f:Lcom/sec/chaton/widget/c;

.field private g:Lcom/sec/common/a/d;

.field private h:Lcom/sec/chaton/settings/downloads/a;

.field private i:Lcom/sec/chaton/d/d;

.field private j:Lcom/sec/chaton/d/a/l;

.field private k:Lcom/sec/chaton/d/a/j;

.field private l:Lcom/sec/common/f/c;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Landroid/view/Menu;

.field private q:Landroid/os/Handler;

.field private r:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 106
    new-instance v0, Lcom/sec/chaton/settings/downloads/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/g;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->q:Landroid/os/Handler;

    .line 126
    new-instance v0, Lcom/sec/chaton/settings/downloads/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/downloads/i;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->r:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->p:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Lcom/sec/chaton/d/a/j;)Lcom/sec/chaton/d/a/j;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k:Lcom/sec/chaton/d/a/j;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Lcom/sec/chaton/d/a/l;)Lcom/sec/chaton/d/a/l;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j:Lcom/sec/chaton/d/a/l;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->n:Ljava/lang/String;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 695
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f()V

    .line 697
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->m:Ljava/lang/String;

    .line 700
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i:Lcom/sec/chaton/d/d;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/chaton/d/d;->a(Lcom/sec/chaton/d/a/b;Lcom/sec/chaton/d/e;Ljava/lang/String;)Lcom/sec/chaton/d/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k:Lcom/sec/chaton/d/a/j;

    .line 701
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->o:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->f(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 360
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/j;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/j;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 369
    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 587
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->e(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 589
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/k;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/k;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 613
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e()V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->n:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 617
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->a(Landroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings/downloads/l;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/downloads/l;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 624
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 627
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 629
    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->r:Landroid/os/Handler;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 635
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 638
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    if-nez v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->b(Landroid/content/Context;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    .line 641
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b0039

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/settings/downloads/m;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/m;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/widget/c;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 647
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    new-instance v1, Lcom/sec/chaton/settings/downloads/n;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings/downloads/n;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 656
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->show()V

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 659
    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g()V

    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 665
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/widget/c;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->f:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 668
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i()V

    .line 670
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->c(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 673
    const v1, 0x7f0b0024

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/settings/downloads/o;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings/downloads/o;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 680
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g:Lcom/sec/common/a/d;

    .line 683
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 684
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 686
    :cond_1
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/d/e;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    return-object v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 692
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->o:Ljava/lang/String;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 704
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k:Lcom/sec/chaton/d/a/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k:Lcom/sec/chaton/d/a/j;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/j;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 705
    :cond_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->r:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;)Z

    .line 710
    :goto_0
    return-void

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k:Lcom/sec/chaton/d/a/j;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/j;->c()V

    .line 708
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->k:Lcom/sec/chaton/d/a/j;

    goto :goto_0
.end method

.method static synthetic k(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)Lcom/sec/chaton/settings/downloads/a;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 578
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings/downloads/a;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 579
    return-void
.end method

.method public a(Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 730
    .line 733
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 734
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings/downloads/p;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/chaton/settings/downloads/p;-><init>(Lcom/sec/chaton/settings/downloads/AmsItemDownloads;Lcom/sec/chaton/d/e;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 741
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705a8

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->p:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->p:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->p:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 124
    :cond_0
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 555
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 557
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c()V

    .line 559
    const/4 v0, 0x1

    .line 563
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 446
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 448
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 452
    :cond_1
    if-nez p1, :cond_0

    .line 453
    const-string v0, "AMS_SAVE"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->m:Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->o:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/q;->a(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 456
    :catch_0
    move-exception v0

    .line 457
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 458
    sget-object v1, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 460
    :catch_1
    move-exception v0

    .line 461
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 261
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 263
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    .line 264
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 277
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 279
    if-eqz p1, :cond_0

    .line 280
    const-string v0, "mDownloadingAmsItemId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->m:Ljava/lang/String;

    .line 281
    const-string v0, "mDownloadingAmsImageUrl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->n:Ljava/lang/String;

    .line 282
    const-string v0, "mDownloadingAmsItemFilePath"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->o:Ljava/lang/String;

    .line 285
    :cond_0
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->l:Lcom/sec/common/f/c;

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->r:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/sec/chaton/d/d;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i:Lcom/sec/chaton/d/d;

    .line 287
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 568
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "special"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, " DESC,"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "new"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, " DESC,"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, " DESC"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 573
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v2}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 373
    const v0, 0x7f0f0024

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 375
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 377
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 380
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->p:Landroid/view/Menu;

    .line 382
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Z)V

    .line 384
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 385
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const-wide/16 v9, 0xa6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v7, 0xe0

    .line 292
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "amsType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/e;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    if-nez v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 354
    :goto_0
    return-object v2

    .line 301
    :cond_0
    const v0, 0x7f03005c

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 311
    const v0, 0x7f070264

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->d:Landroid/widget/GridView;

    .line 312
    new-instance v0, Lcom/sec/chaton/settings/downloads/a;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    iget-object v4, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    iget-object v5, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->l:Lcom/sec/common/f/c;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings/downloads/a;-><init>(Landroid/content/Context;Landroid/database/Cursor;ZLcom/sec/chaton/d/e;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/downloads/a;->a(Lcom/sec/chaton/settings/downloads/d;)V

    .line 315
    new-instance v0, Lcom/sec/chaton/settings/downloads/aw;

    invoke-direct {v0}, Lcom/sec/chaton/settings/downloads/aw;-><init>()V

    .line 316
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->q:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/aw;->a(Landroid/os/Handler;)V

    .line 317
    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/downloads/a;->a(Lcom/sec/chaton/settings/downloads/aw;)V

    .line 319
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->d:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 322
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->setHasOptionsMenu(Z)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->d:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    sget-object v1, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    if-ne v0, v1, :cond_1

    .line 327
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->d:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setColumnWidth(I)V

    .line 332
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->d()V

    .line 334
    sget-object v0, Lcom/sec/chaton/settings/downloads/h;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v1}, Lcom/sec/chaton/d/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_1
    move-object v2, v6

    .line 354
    goto :goto_0

    .line 337
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b012b

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i:Lcom/sec/chaton/d/d;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    move-wide v2, v7

    move-wide v4, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/d;->a(Lcom/sec/chaton/d/a/b;JJ)Lcom/sec/chaton/d/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j:Lcom/sec/chaton/d/a/l;

    goto :goto_1

    .line 343
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i:Lcom/sec/chaton/d/d;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    move-wide v2, v9

    move-wide v4, v9

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/d;->b(Lcom/sec/chaton/d/a/b;JJ)Lcom/sec/chaton/d/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j:Lcom/sec/chaton/d/a/l;

    goto :goto_1

    .line 349
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0225

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->i:Lcom/sec/chaton/d/d;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    move-wide v2, v7

    move-wide v4, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/d;->c(Lcom/sec/chaton/d/a/b;JJ)Lcom/sec/chaton/d/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j:Lcom/sec/chaton/d/a/l;

    goto :goto_1

    .line 334
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 415
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->l:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->l:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j:Lcom/sec/chaton/d/a/l;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->j:Lcom/sec/chaton/d/a/l;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/l;->c()V

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    if-eqz v0, :cond_3

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a;->b()V

    .line 430
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a;->c()V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a;->a(Lcom/sec/chaton/settings/downloads/d;)V

    .line 434
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 435
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 408
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 410
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a/p;->b(Lcom/sec/chaton/settings/downloads/a/q;)V

    .line 411
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 439
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 441
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    .line 442
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 469
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    if-nez v0, :cond_1

    .line 550
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/settings/downloads/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 474
    const-string v1, "item_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 475
    const-string v2, "install"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 486
    invoke-static {}, Lcom/sec/chaton/settings/downloads/a/p;->a()Lcom/sec/chaton/settings/downloads/a/p;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v3}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lcom/sec/chaton/settings/downloads/a/p;->a(Lcom/sec/chaton/e/ar;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/a/l;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/a;

    .line 491
    if-eqz v2, :cond_4

    .line 492
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    sget-object v2, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    if-ne v0, v2, :cond_3

    .line 493
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {v0, v2, v1}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 494
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    const-class v4, Lcom/sec/vip/amschaton/AMSPlayerActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 495
    const-string v3, "AMS_FILE_PATH"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    const-string v0, "VIEWER_MODE"

    const/16 v3, 0x3ea

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 497
    const-string v0, "AMS_DOWNLOAD_PREVIEW"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 498
    const-string v0, "AMS_FROM_DOWNLOADS"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 499
    const-string v0, "AMS_ITEM_ID"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 500
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a;->a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/inner/AmsItem;

    move-result-object v0

    .line 501
    if-eqz v0, :cond_2

    .line 502
    const-string v1, "AMS_FILE_SIZE"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/AmsItem;->filesize:Ljava/lang/Long;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 503
    const-string v1, "AMS_EXPIRATION_DATE"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/AmsItem;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 504
    const-string v0, "AMS_IS_FAILED_ITEM"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 506
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 509
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    sget-object v2, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    if-ne v0, v2, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {v0, v2, v1}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 511
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    const-class v4, Lcom/sec/chaton/settings/downloads/ActivityAmsItemBackgroundDetail;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 512
    const-string v3, "filePath"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 513
    const-string v0, "amsItemId"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 514
    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 520
    :cond_4
    if-eqz v0, :cond_5

    .line 521
    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 534
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 535
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "download_ams, Clicked item type: "

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    aput-object v2, v0, v6

    const/4 v2, 0x2

    const-string v3, ", item id: "

    aput-object v3, v0, v2

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_6
    sget-object v0, Lcom/sec/chaton/settings/downloads/h;->a:[I

    iget-object v2, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v2}, Lcom/sec/chaton/d/e;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 542
    :pswitch_1
    invoke-direct {p0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 546
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/settings/downloads/a;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 521
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 539
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 72
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/downloads/a;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 584
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 389
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 403
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 392
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->h:Lcom/sec/chaton/settings/downloads/a;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->c()V

    goto :goto_0

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 400
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->b()V

    goto :goto_0

    .line 389
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0705a8 -> :sswitch_1
    .end sparse-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 268
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 270
    const-string v0, "mDownloadingAmsItemId"

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v0, "mDownloadingAmsImageUrl"

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v0, "mDownloadingAmsItemFilePath"

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/AmsItemDownloads;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    return-void
.end method
