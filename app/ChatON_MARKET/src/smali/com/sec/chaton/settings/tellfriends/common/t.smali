.class public Lcom/sec/chaton/settings/tellfriends/common/t;
.super Ljava/lang/Object;
.source "SnsMessageFragment.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field protected a:I

.field final synthetic b:Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/common/t;->b:Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    iput p2, p0, Lcom/sec/chaton/settings/tellfriends/common/t;->a:I

    .line 328
    iput-object p3, p0, Lcom/sec/chaton/settings/tellfriends/common/t;->c:Ljava/lang/String;

    .line 329
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 370
    if-nez p1, :cond_1

    .line 381
    :cond_0
    :goto_0
    return v0

    .line 373
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 375
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/common/t;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v0, v1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 377
    :catch_0
    move-exception v1

    .line 378
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    .line 335
    const/4 v0, 0x0

    invoke-interface {p4, v0, p5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 336
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 337
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v0

    invoke-interface {p4, p6, v0}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 339
    const/4 v1, 0x0

    .line 340
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/t;->a(Ljava/lang/String;)I

    move-result v0

    iget v6, p0, Lcom/sec/chaton/settings/tellfriends/common/t;->a:I

    if-gt v0, v6, :cond_2

    .line 342
    const/4 v0, 0x0

    .line 354
    :cond_0
    if-eqz v1, :cond_1

    .line 363
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings/tellfriends/common/t;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings/tellfriends/common/t;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 364
    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/common/t;->b:Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/settings/tellfriends/common/SnsMessageFragment;->b(I)V

    .line 366
    :cond_1
    return-object v0

    .line 344
    :cond_2
    const/4 v1, 0x1

    .line 345
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v8, v0

    move-object v0, v2

    move v2, v8

    :goto_0
    if-ltz v2, :cond_0

    .line 346
    const/4 v0, 0x0

    invoke-virtual {v4, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 347
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings/tellfriends/common/t;->a(Ljava/lang/String;)I

    move-result v6

    iget v7, p0, Lcom/sec/chaton/settings/tellfriends/common/t;->a:I

    if-le v6, v7, :cond_0

    .line 345
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method
