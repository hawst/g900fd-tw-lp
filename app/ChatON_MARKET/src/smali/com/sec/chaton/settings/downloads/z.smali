.class public final enum Lcom/sec/chaton/settings/downloads/z;
.super Ljava/lang/Enum;
.source "AniconHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/downloads/z;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/downloads/z;

.field public static final enum b:Lcom/sec/chaton/settings/downloads/z;

.field public static final enum c:Lcom/sec/chaton/settings/downloads/z;

.field public static final enum d:Lcom/sec/chaton/settings/downloads/z;

.field public static final enum e:Lcom/sec/chaton/settings/downloads/z;

.field public static final enum f:Lcom/sec/chaton/settings/downloads/z;

.field public static final enum g:Lcom/sec/chaton/settings/downloads/z;

.field public static final enum h:Lcom/sec/chaton/settings/downloads/z;

.field private static final synthetic j:[Lcom/sec/chaton/settings/downloads/z;


# instance fields
.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 874
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    .line 875
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "HAPTIC"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->b:Lcom/sec/chaton/settings/downloads/z;

    .line 876
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "SOUND"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->c:Lcom/sec/chaton/settings/downloads/z;

    .line 877
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "MOTION"

    invoke-direct {v0, v1, v7, v8}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->d:Lcom/sec/chaton/settings/downloads/z;

    .line 878
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "HAPTIC_SOUND"

    invoke-direct {v0, v1, v8, v7}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->e:Lcom/sec/chaton/settings/downloads/z;

    .line 879
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "HAPTIC_MOTION"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->f:Lcom/sec/chaton/settings/downloads/z;

    .line 880
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "SOUND_MOTION"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->g:Lcom/sec/chaton/settings/downloads/z;

    .line 881
    new-instance v0, Lcom/sec/chaton/settings/downloads/z;

    const-string v1, "ALL_HSM"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/settings/downloads/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->h:Lcom/sec/chaton/settings/downloads/z;

    .line 873
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/chaton/settings/downloads/z;

    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->b:Lcom/sec/chaton/settings/downloads/z;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->c:Lcom/sec/chaton/settings/downloads/z;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->d:Lcom/sec/chaton/settings/downloads/z;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->e:Lcom/sec/chaton/settings/downloads/z;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/settings/downloads/z;->f:Lcom/sec/chaton/settings/downloads/z;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/settings/downloads/z;->g:Lcom/sec/chaton/settings/downloads/z;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/settings/downloads/z;->h:Lcom/sec/chaton/settings/downloads/z;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/settings/downloads/z;->j:[Lcom/sec/chaton/settings/downloads/z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 885
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 886
    iput p3, p0, Lcom/sec/chaton/settings/downloads/z;->i:I

    .line 887
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/settings/downloads/z;
    .locals 1

    .prologue
    .line 894
    packed-switch p0, :pswitch_data_0

    .line 912
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    :goto_0
    return-object v0

    .line 896
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 898
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->b:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 900
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->c:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 902
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->e:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 904
    :pswitch_4
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->d:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 906
    :pswitch_5
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->f:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 908
    :pswitch_6
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->g:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 910
    :pswitch_7
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->h:Lcom/sec/chaton/settings/downloads/z;

    goto :goto_0

    .line 894
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;
    .locals 1

    .prologue
    .line 873
    const-class v0, Lcom/sec/chaton/settings/downloads/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/z;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/downloads/z;
    .locals 1

    .prologue
    .line 873
    sget-object v0, Lcom/sec/chaton/settings/downloads/z;->j:[Lcom/sec/chaton/settings/downloads/z;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/downloads/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/downloads/z;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 890
    iget v0, p0, Lcom/sec/chaton/settings/downloads/z;->i:I

    return v0
.end method
