.class Lcom/sec/chaton/settings/tellfriends/bw;
.super Lcom/sec/chaton/settings/tellfriends/common/p;
.source "WeiboManager.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/bt;

.field private b:J


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;)V
    .locals 2

    .prologue
    .line 240
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/bw;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    .line 241
    const-string v0, "GetFriendsListTask"

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings/tellfriends/common/p;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;Ljava/lang/String;)V

    .line 243
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/settings/tellfriends/bw;->b:J

    .line 244
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings/tellfriends/bt;Lcom/sec/chaton/settings/tellfriends/bu;)V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings/tellfriends/bw;-><init>(Lcom/sec/chaton/settings/tellfriends/bt;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 8

    .prologue
    const/16 v7, -0x3ed

    .line 248
    .line 252
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bw;->a:Lcom/sec/chaton/settings/tellfriends/bt;

    iget-object v0, v0, Lcom/sec/chaton/settings/tellfriends/bt;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/cc;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 256
    :cond_0
    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/bt;->f()Lweibo4android/Weibo;

    move-result-object v0

    iget-wide v3, p0, Lcom/sec/chaton/settings/tellfriends/bw;->b:J

    long-to-int v3, v3

    const/16 v4, 0x1388

    invoke-virtual {v0, v2, v3, v4}, Lweibo4android/Weibo;->getFriendsStatuses2(Ljava/lang/String;II)Lweibo4android/UserWapper;

    move-result-object v3

    .line 257
    invoke-virtual {v3}, Lweibo4android/UserWapper;->getUsers()Ljava/util/List;

    move-result-object v0

    .line 258
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lweibo4android/User;

    .line 259
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lweibo4android/WeiboException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 278
    :catch_0
    move-exception v0

    .line 279
    invoke-virtual {v0}, Lweibo4android/WeiboException;->printStackTrace()V

    .line 280
    iput v7, p0, Lcom/sec/chaton/settings/tellfriends/bw;->e:I

    .line 281
    invoke-virtual {v0}, Lweibo4android/WeiboException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 288
    :goto_1
    iput-object v0, p0, Lcom/sec/chaton/settings/tellfriends/bw;->f:Ljava/lang/Object;

    .line 289
    iget v0, p0, Lcom/sec/chaton/settings/tellfriends/bw;->e:I

    return v0

    .line 261
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Lweibo4android/UserWapper;->getNextCursor()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/chaton/settings/tellfriends/bw;->b:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-nez v0, :cond_0

    .line 277
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings/tellfriends/bw;->e:I
    :try_end_1
    .catch Lweibo4android/WeiboException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 286
    goto :goto_1

    .line 282
    :catch_1
    move-exception v0

    .line 283
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 284
    iput v7, p0, Lcom/sec/chaton/settings/tellfriends/bw;->e:I

    .line 285
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
