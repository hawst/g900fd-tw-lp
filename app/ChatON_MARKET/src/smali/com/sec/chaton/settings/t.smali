.class Lcom/sec/chaton/settings/t;
.super Ljava/lang/Object;
.source "ActivityContactSyncFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/chaton/settings/t;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings/t;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/t;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 125
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/settings/t;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "auto_contact_sync"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 131
    :cond_0
    :goto_0
    return v3

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/t;->a:Lcom/sec/chaton/settings/ActivityContactSyncFragment;

    invoke-static {v0}, Lcom/sec/chaton/settings/ActivityContactSyncFragment;->a(Lcom/sec/chaton/settings/ActivityContactSyncFragment;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "auto_contact_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
