.class Lcom/sec/chaton/settings/dw;
.super Ljava/lang/Object;
.source "FragmentPasswordLockHint.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->a(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 251
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->a(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 252
    iget-object v2, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v2}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->a(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_2

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->c(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->c(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->a(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-ge v0, v4, :cond_1

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->c(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/settings/dw;->a:Lcom/sec/chaton/settings/FragmentPasswordLockHint;

    invoke-static {v0}, Lcom/sec/chaton/settings/FragmentPasswordLockHint;->c(Lcom/sec/chaton/settings/FragmentPasswordLockHint;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 281
    :cond_1
    return-void

    .line 251
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
