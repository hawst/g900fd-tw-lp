.class Lcom/sec/chaton/settings/downloads/e;
.super Landroid/os/Handler;
.source "AmsItemBackgroundDetail.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 84
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/settings/downloads/a/a;

    .line 86
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 89
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 90
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "PackageId: "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/downloads/a/a;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    const-string v2, ". Status: "

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 98
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;Landroid/database/Cursor;)V

    goto :goto_0

    .line 103
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;Lcom/sec/chaton/settings/downloads/a/a;)Lcom/sec/chaton/settings/downloads/a/a;

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    iget-object v1, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->a(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;Landroid/database/Cursor;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->b(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->b(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/settings/downloads/e;->a:Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;->b(Lcom/sec/chaton/settings/downloads/AmsItemBackgroundDetail;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
