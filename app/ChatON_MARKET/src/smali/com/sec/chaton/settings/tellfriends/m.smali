.class public Lcom/sec/chaton/settings/tellfriends/m;
.super Lcom/sec/chaton/settings/tellfriends/common/o;
.source "RenrenManager.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static d:Lcom/renren/android/Renren;


# instance fields
.field private e:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/chaton/settings/tellfriends/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V
    .locals 4

    .prologue
    .line 39
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/m;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/settings/tellfriends/common/o;-><init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/m;->e:Landroid/app/Activity;

    .line 44
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/m;->d:Lcom/renren/android/Renren;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/renren/android/Renren;

    const-string v1, "44c448753f244b1891445c240f8fa1e3"

    const-string v2, "5ae8c090db1f4a13bdf1211251f404f0"

    const-string v3, "233775"

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/renren/android/Renren;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/chaton/settings/tellfriends/m;->d:Lcom/renren/android/Renren;

    .line 47
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/m;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/m;->e:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f()Lcom/renren/android/Renren;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/m;->d:Lcom/renren/android/Renren;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings/tellfriends/m;->c(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    .line 51
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/n;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/settings/tellfriends/n;-><init>(Lcom/sec/chaton/settings/tellfriends/m;I)V

    .line 77
    sget-object v1, Lcom/sec/chaton/settings/tellfriends/m;->d:Lcom/renren/android/Renren;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/m;->e:Landroid/app/Activity;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "publish_feed"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "create_album"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "photo_upload"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "read_user_album"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "status_update"

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3, v0}, Lcom/renren/android/Renren;->a(Landroid/app/Activity;[Ljava/lang/String;Lcom/sec/chaton/settings/tellfriends/common/b;)V

    .line 79
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 126
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    :cond_0
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/r;

    invoke-direct {v0, p0, p3}, Lcom/sec/chaton/settings/tellfriends/r;-><init>(Lcom/sec/chaton/settings/tellfriends/m;Ljava/lang/String;)V

    .line 128
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 161
    :goto_0
    return-void

    .line 130
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 131
    const-string v1, "to"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/l;

    iget-object v2, p0, Lcom/sec/chaton/settings/tellfriends/m;->e:Landroid/app/Activity;

    sget-object v3, Lcom/sec/chaton/settings/tellfriends/m;->d:Lcom/renren/android/Renren;

    invoke-direct {v1, v2, v3, v0}, Lcom/sec/chaton/settings/tellfriends/l;-><init>(Landroid/content/Context;Lcom/renren/android/Renren;Landroid/os/Bundle;)V

    new-instance v0, Lcom/sec/chaton/settings/tellfriends/o;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/settings/tellfriends/o;-><init>(Lcom/sec/chaton/settings/tellfriends/m;I)V

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/tellfriends/l;->a(Lcom/sec/chaton/settings/tellfriends/common/b;)Lcom/sec/chaton/settings/tellfriends/i;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/tellfriends/l;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/l;->a()Lcom/sec/chaton/settings/tellfriends/e;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/e;->show()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/sec/chaton/settings/tellfriends/m;->d:Lcom/renren/android/Renren;

    invoke-virtual {v0}, Lcom/renren/android/Renren;->a()Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/m;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/u;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 4

    .prologue
    .line 108
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings/tellfriends/p;-><init>(Lcom/sec/chaton/settings/tellfriends/m;)V

    .line 109
    new-instance v1, Lcom/sec/chaton/settings/tellfriends/common/q;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings/tellfriends/common/q;-><init>(Lcom/sec/chaton/settings/tellfriends/common/o;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/chaton/settings/tellfriends/common/p;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings/tellfriends/common/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/m;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/u;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/m;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/u;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 165
    invoke-super {p0}, Lcom/sec/chaton/settings/tellfriends/common/o;->e()V

    .line 166
    return-void
.end method
