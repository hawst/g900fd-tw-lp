.class public final enum Lcom/sec/chaton/settings/fe;
.super Ljava/lang/Enum;
.source "SnsSignInOutPreference.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/settings/fe;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/settings/fe;

.field public static final enum b:Lcom/sec/chaton/settings/fe;

.field private static final synthetic c:[Lcom/sec/chaton/settings/fe;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/sec/chaton/settings/fe;

    const-string v1, "SIGN_IN"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/fe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/fe;->a:Lcom/sec/chaton/settings/fe;

    .line 26
    new-instance v0, Lcom/sec/chaton/settings/fe;

    const-string v1, "SIGN_OUT"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/settings/fe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/settings/fe;

    sget-object v1, Lcom/sec/chaton/settings/fe;->a:Lcom/sec/chaton/settings/fe;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/settings/fe;->c:[Lcom/sec/chaton/settings/fe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/settings/fe;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/chaton/settings/fe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/fe;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/settings/fe;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/chaton/settings/fe;->c:[Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0}, [Lcom/sec/chaton/settings/fe;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/settings/fe;

    return-object v0
.end method
