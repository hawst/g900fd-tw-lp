.class Lcom/sec/chaton/settings/tellfriends/ao;
.super Ljava/lang/Object;
.source "SnsHelperTwitter.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/common/c;


# instance fields
.field final synthetic a:Lcom/sec/chaton/settings/tellfriends/an;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings/tellfriends/an;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v0, -0x1

    .line 39
    packed-switch p1, :pswitch_data_0

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 41
    :pswitch_0
    if-ne p2, v0, :cond_3

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    instance-of v0, p3, Ltwitter4j/ResponseList;

    if-eqz v0, :cond_0

    .line 45
    check-cast p3, Ltwitter4j/ResponseList;

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 49
    invoke-interface {p3}, Ltwitter4j/ResponseList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltwitter4j/User;

    .line 51
    new-instance v3, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-interface {v0}, Ltwitter4j/User;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ltwitter4j/User;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ltwitter4j/User;->getScreenName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Ltwitter4j/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ltwitter4j/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/chaton/settings/tellfriends/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const-string v0, ""

    goto :goto_2

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->a(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ad;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 57
    :cond_3
    if-nez p2, :cond_4

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->a(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ad;->a()V

    goto/16 :goto_0

    .line 61
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->a(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ad;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ad;->a(I)V

    goto/16 :goto_0

    .line 66
    :pswitch_1
    if-ne p2, v0, :cond_5

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS_ID \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->b(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ac;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/sec/chaton/settings/tellfriends/ac;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 70
    :cond_5
    if-nez p2, :cond_6

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS_ID \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->b(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ac;->a()V

    goto/16 :goto_0

    .line 74
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_FRIENDS_ID \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->b(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ac;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ac;->a(I)V

    goto/16 :goto_0

    .line 79
    :pswitch_2
    if-ne p2, v0, :cond_9

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_USER_INFO \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    instance-of v0, p3, Ltwitter4j/ResponseList;

    if-eqz v0, :cond_0

    .line 82
    check-cast p3, Ltwitter4j/ResponseList;

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 86
    invoke-interface {p3}, Ltwitter4j/ResponseList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltwitter4j/User;

    .line 88
    new-instance v3, Lcom/sec/chaton/settings/tellfriends/ai;

    invoke-interface {v0}, Ltwitter4j/User;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ltwitter4j/User;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ltwitter4j/User;->getScreenName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Ltwitter4j/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-interface {v0}, Ltwitter4j/User;->getProfileImageURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/chaton/settings/tellfriends/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    const-string v0, ""

    goto :goto_4

    .line 92
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->c(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ae;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/chaton/settings/tellfriends/ae;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 95
    :cond_9
    if-nez p2, :cond_a

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_USER_INFO \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->c(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ae;->a()V

    goto/16 :goto_0

    .line 99
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_GET_USER_INFO \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->c(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ae;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ae;->a(I)V

    goto/16 :goto_0

    .line 104
    :pswitch_3
    if-ne p2, v0, :cond_b

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->d(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ah;->a()V

    goto/16 :goto_0

    .line 107
    :cond_b
    if-nez p2, :cond_c

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->d(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ah;->b()V

    goto/16 :goto_0

    .line 111
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_POST_TO_FRIEND \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->d(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ah;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/ah;->a(I)V

    goto/16 :goto_0

    .line 116
    :pswitch_4
    if-ne p2, v0, :cond_d

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->e(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onComplete()V

    goto/16 :goto_0

    .line 119
    :cond_d
    if-nez p2, :cond_e

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->e(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onCancelled()V

    goto/16 :goto_0

    .line 123
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGOUT \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->e(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/ag;->onError()V

    goto/16 :goto_0

    .line 128
    :pswitch_5
    if-ne p2, v0, :cond_f

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->f(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onComplete()V

    goto/16 :goto_0

    .line 131
    :cond_f
    if-nez p2, :cond_10

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->f(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onCancelled()V

    goto/16 :goto_0

    .line 135
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_LOGIN \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->f(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/af;->onError(I)V

    goto/16 :goto_0

    .line 140
    :pswitch_6
    if-ne p2, v0, :cond_11

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_PREPARE_LOGIN \tRESULT_OK \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    iget-object v1, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v1}, Lcom/sec/chaton/settings/tellfriends/an;->f(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/tellfriends/an;->a(Lcom/sec/chaton/settings/tellfriends/af;)V

    goto/16 :goto_0

    .line 143
    :cond_11
    if-nez p2, :cond_12

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_PREPARE_LOGIN \tRESULT_CANCELED \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->f(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/settings/tellfriends/af;->onCancelled()V

    goto/16 :goto_0

    .line 147
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST_PREPARE_LOGIN \tRESULT_ERROR \t- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/settings/tellfriends/an;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/settings/tellfriends/ao;->a:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/an;->f(Lcom/sec/chaton/settings/tellfriends/an;)Lcom/sec/chaton/settings/tellfriends/af;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/chaton/settings/tellfriends/af;->onError(I)V

    goto/16 :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x2ee1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
