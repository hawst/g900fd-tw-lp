.class Lcom/sec/chaton/bp;
.super Ljava/lang/Object;
.source "TabActivity.java"

# interfaces
.implements Lcom/coolots/sso/a/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 3324
    iput-object p1, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveUpdateVerion(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onReceiveUpdateVerion] currentVersionName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",serverVersionName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",versionInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3330
    const-string v0, "chatonVUpdateStatus"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3331
    const-string v0, "chatonv_update_version"

    invoke-static {v0, p2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3333
    if-eqz p3, :cond_0

    .line 3334
    new-instance v0, Landroid/content/Intent;

    const-string v1, "more_tab_badge_update"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3335
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 3337
    packed-switch p3, :pswitch_data_0

    .line 3360
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->d(Landroid/content/Context;)V

    .line 3361
    return-void

    .line 3341
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3342
    iget-object v0, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->e(Lcom/sec/chaton/TabActivity;Z)V

    goto :goto_0

    .line 3344
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v2}, Lcom/sec/chaton/TabActivity;->i(Lcom/sec/chaton/TabActivity;Z)Z

    goto :goto_0

    .line 3350
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3351
    iget-object v0, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v2}, Lcom/sec/chaton/TabActivity;->e(Lcom/sec/chaton/TabActivity;Z)V

    .line 3356
    :goto_1
    const-string v0, "chatonv_critical_update"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 3353
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/bp;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v2}, Lcom/sec/chaton/TabActivity;->i(Lcom/sec/chaton/TabActivity;Z)Z

    goto :goto_1

    .line 3337
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
