.class Lcom/sec/chaton/aq;
.super Landroid/content/BroadcastReceiver;
.source "TabActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 1188
    iput-object p1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1191
    iget-object v0, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->t(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1192
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chatonv_critical_update"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1193
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1194
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receiver : updateChatONV, Critical is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1, v0}, Lcom/sec/chaton/TabActivity;->e(Lcom/sec/chaton/TabActivity;Z)V

    .line 1198
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->u(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1199
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1200
    const-string v0, "Receiver : updateSPP"

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    const-class v2, Lcom/sec/chaton/NewSPPUpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1203
    const-string v1, "isCritical"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1204
    const-string v1, "isFromHome"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1206
    iget-object v1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 1208
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->v(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1209
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Receiver : updateDisclaimer\n VersionInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->disclaimerUID:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1213
    iget-object v0, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    iget-object v1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->disclaimerUID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Ljava/lang/String;)V

    .line 1216
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->x(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1217
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 1218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Receiver : updateChatON\n VersionInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    :cond_6
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateAppsReady"

    const-string v2, "NO"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1221
    iget-object v1, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1, v2, v4, v0}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 1224
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/aq;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->y(Lcom/sec/chaton/TabActivity;)V

    .line 1225
    return-void
.end method
