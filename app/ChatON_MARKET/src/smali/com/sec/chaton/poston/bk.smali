.class Lcom/sec/chaton/poston/bk;
.super Ljava/lang/Object;
.source "PostONWriteFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 1346
    iput-object p1, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1349
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1350
    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1351
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->h(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1349
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1354
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iput v2, v0, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    .line 1355
    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1356
    const/4 v1, -0x3

    if-eq v1, v0, :cond_3

    const/4 v1, -0x2

    if-ne v1, v0, :cond_4

    .line 1357
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->u(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1368
    :goto_1
    return-void

    .line 1360
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->v(Lcom/sec/chaton/poston/PostONWriteFragment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1361
    new-instance v0, Lcom/sec/chaton/poston/by;

    iget-object v1, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v3, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->h(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/chaton/poston/by;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/util/ArrayList;Lcom/sec/chaton/e/w;)V

    .line 1362
    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/poston/by;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 1364
    :cond_5
    new-instance v0, Lcom/sec/chaton/poston/by;

    iget-object v1, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v3, p0, Lcom/sec/chaton/poston/bk;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->h(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/chaton/poston/by;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/util/ArrayList;Lcom/sec/chaton/e/w;)V

    .line 1365
    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/poston/by;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method
