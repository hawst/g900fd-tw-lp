.class Lcom/sec/chaton/poston/aa;
.super Landroid/os/Handler;
.source "PostONDetailFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const v7, 0x7f0b0182

    const v5, 0x7f0b0037

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 950
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1213
    :cond_0
    :goto_0
    return-void

    .line 953
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 976
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 977
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 979
    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 980
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_7

    .line 981
    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->v(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0188

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 987
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONList;

    .line 988
    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    if-eqz v2, :cond_1a

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1a

    .line 989
    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1a

    .line 990
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/PostONList;->hasmore:Ljava/lang/String;

    .line 991
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/PostONList;->endtime:Ljava/lang/String;

    .line 992
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->isblind:Ljava/lang/String;

    .line 997
    :goto_1
    iget-object v3, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONDetailFragment;->D(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONDetailFragment;->D(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/PostONDetailFragment;->p:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 998
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1004
    :goto_2
    const-string v4, "HAS_MORE"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1005
    const-string v2, "END_TIME"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1006
    const-string v1, "IS_BLIND"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1007
    const-string v0, "IS_DELETED"

    invoke-virtual {v3, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1008
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1009
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 957
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->A(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->A(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 958
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->A(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 960
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->B(Lcom/sec/chaton/poston/PostONDetailFragment;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 961
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->v(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->B(Lcom/sec/chaton/poston/PostONDetailFragment;)I

    move-result v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 965
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->C(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 966
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->C(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 969
    :cond_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 971
    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0

    .line 999
    :cond_5
    iget-object v3, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONDetailFragment;->E(Lcom/sec/chaton/poston/PostONDetailFragment;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1000
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_2

    .line 1002
    :cond_6
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_2

    .line 1012
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x15fa0

    if-eq v1, v2, :cond_8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x16d3f

    if-ne v0, v1, :cond_9

    .line 1014
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0160

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/ab;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/ab;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1034
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->v(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00f2

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1035
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 1041
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1042
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1044
    :cond_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1045
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_b

    .line 1046
    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1, v6}, Lcom/sec/chaton/poston/PostONDetailFragment;->c(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1049
    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->v(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0189

    invoke-static {v1, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1052
    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/a/a/f;)V

    .line 1053
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v6}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    goto/16 :goto_0

    .line 1056
    :cond_b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x15fa1

    if-ne v1, v2, :cond_c

    .line 1057
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0160

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/ac;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/ac;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1070
    :cond_c
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x15fa0

    if-ne v0, v1, :cond_d

    .line 1071
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0160

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/ad;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/ad;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1085
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->v(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00f2

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1091
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1092
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1095
    :cond_e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1096
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_10

    .line 1097
    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1098
    const-string v2, "HTTP Success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 1100
    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1104
    :cond_f
    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0b0170

    invoke-static {v1, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1106
    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/a/a/f;)V

    .line 1107
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v6}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    goto/16 :goto_0

    .line 1109
    :cond_10
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 1110
    const/4 v3, -0x3

    if-eq v3, v2, :cond_11

    const/4 v3, -0x2

    if-ne v3, v2, :cond_12

    .line 1111
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1112
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v6}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    goto/16 :goto_0

    .line 1115
    :cond_12
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_0

    .line 1117
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    const v3, 0x15f9c

    if-ne v2, v3, :cond_15

    .line 1118
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->F(Lcom/sec/chaton/poston/PostONDetailFragment;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1119
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1120
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1121
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1123
    :cond_13
    const-string v0, "WRITE_POSTON_COMMENT_DUPLICATED_PREVIOUS_ONE: mResponseErr true"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->j(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/d/v;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->i(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/v;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1137
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1138
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0164

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0169

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/ae;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/ae;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1147
    :cond_15
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc355

    if-ne v1, v2, :cond_16

    .line 1148
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1149
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0289

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/af;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/af;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1159
    :cond_16
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x15fa0

    if-eq v1, v2, :cond_17

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x16d3f

    if-ne v1, v2, :cond_18

    .line 1161
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1162
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0160

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/ag;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/ag;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1180
    :cond_18
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x1637c

    if-ne v0, v1, :cond_19

    .line 1181
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1182
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->v(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0409

    invoke-static {v0, v1, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1184
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 1186
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v6}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z

    .line 1187
    iget-object v0, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/aa;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0097

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/poston/ai;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/ai;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/poston/ah;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/ah;-><init>(Lcom/sec/chaton/poston/aa;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    :cond_1a
    move-object v0, v1

    move-object v2, v1

    goto/16 :goto_1

    .line 953
    nop

    :pswitch_data_0
    .packed-switch 0x387
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
