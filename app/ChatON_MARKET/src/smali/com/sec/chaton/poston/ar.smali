.class Lcom/sec/chaton/poston/ar;
.super Ljava/lang/Object;
.source "PostONDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 392
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostONCommentItem: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    new-instance v0, Lcom/sec/chaton/poston/p;

    iget-object v1, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->g(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->c(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->h(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "-1"

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/poston/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v1, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->j(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/d/v;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONDetailFragment;->i(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/chaton/d/v;->a(Ljava/lang/String;Lcom/sec/chaton/poston/p;Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->k(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/poston/ar;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
