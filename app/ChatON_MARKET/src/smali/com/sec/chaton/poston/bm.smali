.class Lcom/sec/chaton/poston/bm;
.super Landroid/os/Handler;
.source "PostONWriteFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 1408
    iput-object p1, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1410
    if-nez p1, :cond_1

    .line 1494
    :cond_0
    :goto_0
    return-void

    .line 1413
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1416
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1418
    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iput v1, v2, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    .line 1419
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1421
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/j/c/k;

    .line 1422
    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->w(Lcom/sec/chaton/poston/PostONWriteFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1423
    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 1425
    :cond_2
    if-eqz v0, :cond_0

    .line 1426
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 1427
    iget v3, p1, Landroid/os/Message;->arg2:I

    .line 1429
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mFileUploadHandler/ nResultCode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " /nFaultCode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " /result.getResult(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/j/c/k;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1431
    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    .line 1433
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/k;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1434
    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1437
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFileUploadHandler / result.getResultUrl() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/j/c/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget v3, v2, Lcom/sec/chaton/poston/PostONWriteFragment;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/sec/chaton/poston/PostONWriteFragment;->b:I

    .line 1440
    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 1441
    iget-object v2, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/j/c/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1445
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->x(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1446
    aget-object v0, v2, v6

    if-eqz v0, :cond_6

    aget-object v0, v2, v6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1447
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1448
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1449
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->x(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1450
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    array-length v4, v3

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->c(Ljava/lang/String;)V

    .line 1451
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_4

    .line 1452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "yeseul / mPreviewData.get(index).getFileName(): "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->g()Ljava/lang/String;

    move-result-object v0

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1456
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_5

    .line 1457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is Equal >> mPreviewData.get(index): "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " /strArray[1]:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v2, v2, v6

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    iget-object v1, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->x(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->b(Ljava/lang/String;)V

    .line 1465
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget v0, v0, Lcom/sec/chaton/poston/PostONWriteFragment;->b:I

    iget-object v1, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget v1, v1, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1466
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->y(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    goto/16 :goto_0

    .line 1447
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 1469
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget v1, v0, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    .line 1470
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_9

    .line 1471
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "numUploadFail : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget v1, v1, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget v0, v0, Lcom/sec/chaton/poston/PostONWriteFragment;->b:I

    iget-object v1, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget v1, v1, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->y(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    goto/16 :goto_0

    .line 1478
    :cond_a
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_b

    .line 1479
    const-string v0, "Failed or Canceled"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1484
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1485
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 1487
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/poston/bm;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0b013e

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1419
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
