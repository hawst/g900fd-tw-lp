.class public Lcom/sec/chaton/poston/r;
.super Landroid/widget/BaseAdapter;
.source "PostONContentsAdapter.java"


# instance fields
.field a:Lcom/sec/chaton/poston/u;

.field private b:Landroid/content/Context;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/LayoutInflater;

.field private e:Lcom/sec/common/f/c;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/poston/r;->g:Z

    .line 47
    iput-object p1, p0, Lcom/sec/chaton/poston/r;->b:Landroid/content/Context;

    .line 48
    iput-object p3, p0, Lcom/sec/chaton/poston/r;->c:Ljava/util/ArrayList;

    .line 49
    iput-object p4, p0, Lcom/sec/chaton/poston/r;->e:Lcom/sec/common/f/c;

    .line 50
    iput p2, p0, Lcom/sec/chaton/poston/r;->f:I

    .line 51
    iget-object v0, p0, Lcom/sec/chaton/poston/r;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/poston/r;->d:Landroid/view/LayoutInflater;

    .line 52
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/multimedia/multisend/PreviewData;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/poston/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    return-object v0
.end method

.method public a(Lcom/sec/chaton/poston/u;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/chaton/poston/r;->a:Lcom/sec/chaton/poston/u;

    .line 40
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/chaton/poston/r;->g:Z

    .line 44
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/chaton/poston/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/chaton/poston/r;->a(I)Lcom/sec/chaton/multimedia/multisend/PreviewData;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 142
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 61
    .line 64
    invoke-virtual {p0, p1}, Lcom/sec/chaton/poston/r;->a(I)Lcom/sec/chaton/multimedia/multisend/PreviewData;

    move-result-object v5

    .line 66
    if-nez p2, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/poston/r;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/poston/r;->f:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 68
    new-instance v2, Lcom/sec/chaton/poston/v;

    move-object v0, v1

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-direct {v2, v0}, Lcom/sec/chaton/poston/v;-><init>(Landroid/widget/RelativeLayout;)V

    move-object v8, v2

    move-object p2, v1

    .line 74
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/poston/r;->g:Z

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/chaton/poston/v;->d:Ljava/lang/String;

    .line 76
    new-instance v0, Lcom/sec/chaton/multimedia/image/c;

    iget-object v1, v8, Lcom/sec/chaton/poston/v;->d:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/poston/v;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/chaton/multimedia/image/c;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 77
    iget-object v1, p0, Lcom/sec/chaton/poston/r;->e:Lcom/sec/common/f/c;

    iget-object v2, v8, Lcom/sec/chaton/poston/v;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 104
    :goto_1
    iget-object v0, v8, Lcom/sec/chaton/poston/v;->c:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/poston/t;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/poston/t;-><init>(Lcom/sec/chaton/poston/r;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    invoke-virtual {p2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 114
    return-object p2

    .line 70
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/v;

    move-object v8, v0

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/chaton/poston/v;->d:Ljava/lang/String;

    .line 81
    invoke-virtual {v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/chaton/poston/v;->e:Ljava/lang/String;

    .line 82
    invoke-virtual {v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/chaton/poston/v;->f:Ljava/lang/String;

    .line 83
    invoke-virtual {v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/chaton/poston/v;->g:Ljava/lang/String;

    .line 84
    iget-object v0, v8, Lcom/sec/chaton/poston/v;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 85
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/i;

    iget-object v1, v8, Lcom/sec/chaton/poston/v;->d:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/poston/v;->e:Ljava/lang/String;

    iget-object v5, v8, Lcom/sec/chaton/poston/v;->g:Ljava/lang/String;

    move v4, v3

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/multimedia/multisend/i;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V

    .line 91
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/poston/r;->e:Lcom/sec/common/f/c;

    iget-object v2, v8, Lcom/sec/chaton/poston/v;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 93
    iget-object v0, v8, Lcom/sec/chaton/poston/v;->b:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/poston/s;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/poston/s;-><init>(Lcom/sec/chaton/poston/r;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 86
    :cond_2
    iget-object v0, v8, Lcom/sec/chaton/poston/v;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 87
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/i;

    iget-object v1, v8, Lcom/sec/chaton/poston/v;->f:Ljava/lang/String;

    iget-object v5, v8, Lcom/sec/chaton/poston/v;->g:Ljava/lang/String;

    move-object v2, v6

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/multimedia/multisend/i;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V

    goto :goto_2

    .line 89
    :cond_3
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/i;

    iget-object v1, v8, Lcom/sec/chaton/poston/v;->f:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/poston/v;->e:Ljava/lang/String;

    iget-object v5, v8, Lcom/sec/chaton/poston/v;->g:Ljava/lang/String;

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/multimedia/multisend/i;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V

    goto :goto_2
.end method
