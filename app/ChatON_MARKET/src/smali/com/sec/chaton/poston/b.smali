.class Lcom/sec/chaton/poston/b;
.super Landroid/widget/ArrayAdapter;
.source "HideListPostONAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/block/aa;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 44
    iput-object p1, p0, Lcom/sec/chaton/poston/b;->b:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/sec/chaton/poston/b;->a:Ljava/util/ArrayList;

    .line 47
    iget-object v0, p0, Lcom/sec/chaton/poston/b;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/poston/b;->c:Landroid/view/LayoutInflater;

    .line 48
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 53
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/poston/b;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030125

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    .line 56
    new-instance v2, Lcom/sec/chaton/poston/c;

    invoke-direct {v2}, Lcom/sec/chaton/poston/c;-><init>()V

    .line 58
    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/sec/chaton/poston/c;->a:Landroid/widget/TextView;

    .line 59
    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/sec/chaton/poston/c;->b:Landroid/widget/TextView;

    .line 60
    iget-object v1, v2, Lcom/sec/chaton/poston/c;->b:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 61
    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/sec/chaton/poston/c;->c:Landroid/widget/ImageView;

    .line 62
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 63
    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 65
    invoke-virtual {p0, p1}, Lcom/sec/chaton/poston/b;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/block/aa;

    .line 66
    iget-object v3, v2, Lcom/sec/chaton/poston/c;->a:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/sec/chaton/block/aa;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v3, p0, Lcom/sec/chaton/poston/b;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v3

    iget-object v2, v2, Lcom/sec/chaton/poston/c;->c:Landroid/widget/ImageView;

    iget-object v1, v1, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v1}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 69
    return-object v0
.end method
