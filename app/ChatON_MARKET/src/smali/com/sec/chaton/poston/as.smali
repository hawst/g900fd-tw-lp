.class Lcom/sec/chaton/poston/as;
.super Ljava/lang/Object;
.source "PostONDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/chaton/poston/as;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 551
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    :goto_0
    return-void

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/as;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->m(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/poston/as;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->m(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 555
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Video tmpFile exists : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/as;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->m(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/as;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/ax;

    iget-object v1, p0, Lcom/sec/chaton/poston/as;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->m(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/poston/ax;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 560
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 561
    const-string v0, "Video tmpFile not exist"

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/as;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->n(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    goto :goto_0
.end method
