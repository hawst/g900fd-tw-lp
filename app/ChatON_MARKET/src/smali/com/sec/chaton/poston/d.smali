.class public Lcom/sec/chaton/poston/d;
.super Landroid/widget/BaseAdapter;
.source "PostONAdapter.java"


# static fields
.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field private static m:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/poston/j;

.field private g:Landroid/content/Context;

.field private h:Lcom/sec/common/f/c;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/poston/k;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/view/LayoutInflater;

.field private k:I

.field private l:Ljava/lang/String;

.field private n:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    const-string v0, "1"

    sput-object v0, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    .line 62
    const-string v0, "2"

    sput-object v0, Lcom/sec/chaton/poston/d;->c:Ljava/lang/String;

    .line 63
    const-string v0, "3"

    sput-object v0, Lcom/sec/chaton/poston/d;->d:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/poston/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/poston/d;->m:Ljava/lang/String;

    .line 65
    const-string v0, "mypage"

    sput-object v0, Lcom/sec/chaton/poston/d;->e:Ljava/lang/String;

    .line 66
    const-string v0, "buddy"

    sput-object v0, Lcom/sec/chaton/poston/d;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;ILcom/sec/common/f/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/poston/k;",
            ">;I",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 58
    iput-object v2, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    .line 341
    new-instance v0, Lcom/sec/chaton/poston/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/i;-><init>(Lcom/sec/chaton/poston/d;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/d;->n:Landroid/view/View$OnClickListener;

    .line 85
    iput-object p2, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    .line 86
    iput-object p3, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    .line 87
    iput p4, p0, Lcom/sec/chaton/poston/d;->k:I

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/poston/d;->j:Landroid/view/LayoutInflater;

    .line 90
    iput-object v2, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    .line 91
    iput-object p5, p0, Lcom/sec/chaton/poston/d;->h:Lcom/sec/common/f/c;

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/common/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/poston/k;",
            ">;I",
            "Ljava/lang/String;",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    .line 341
    new-instance v0, Lcom/sec/chaton/poston/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/i;-><init>(Lcom/sec/chaton/poston/d;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/d;->n:Landroid/view/View$OnClickListener;

    .line 96
    iput-object p2, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    .line 97
    iput-object p3, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    .line 98
    iput p4, p0, Lcom/sec/chaton/poston/d;->k:I

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/poston/d;->j:Landroid/view/LayoutInflater;

    .line 101
    iput-object p5, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    .line 102
    iput-object p6, p0, Lcom/sec/chaton/poston/d;->h:Lcom/sec/common/f/c;

    .line 103
    return-void
.end method

.method public static a(J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 568
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 569
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 570
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 571
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-MM-dd"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 572
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy"

    invoke-direct {v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 574
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 579
    :goto_0
    return-object v0

    .line 576
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 577
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 579
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/poston/d;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/poston/d;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method a(Lcom/sec/chaton/e/a/aa;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 525
    invoke-virtual {p1}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 526
    invoke-virtual {p1}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 527
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v1, ","

    invoke-direct {v3, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const-string v1, ""

    .line 529
    const/4 v0, 0x0

    .line 530
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 531
    add-int/lit8 v2, v0, 0x1

    .line 532
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 533
    const/4 v4, 0x3

    if-lt v2, v4, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 534
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    :goto_1
    move-object v1, v0

    move v0, v2

    .line 540
    goto :goto_0

    .line 537
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 544
    :cond_1
    const/4 v1, 0x0

    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/poston/j;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/chaton/poston/d;->a:Lcom/sec/chaton/poston/j;

    .line 82
    return-void
.end method

.method b(Lcom/sec/chaton/e/a/aa;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 550
    invoke-virtual {p1}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 551
    invoke-virtual {p1}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 552
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, ","

    invoke-direct {v1, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 556
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 557
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 562
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 119
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 124
    .line 126
    if-nez p2, :cond_3

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->j:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/poston/d;->k:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 129
    const v0, 0x7f0702e2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 130
    iget-object v1, p0, Lcom/sec/chaton/poston/d;->j:Landroid/view/LayoutInflater;

    const v3, 0x7f030136

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 133
    const v0, 0x7f0702ea

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 134
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 135
    const/16 v3, 0x10

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 136
    new-instance v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 137
    iget-object v3, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020138

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 138
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 139
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 141
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x1

    invoke-direct {v1, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 143
    const/16 v3, 0x11

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    :cond_0
    new-instance v1, Lcom/sec/chaton/poston/l;

    move-object v0, v2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v1, v0}, Lcom/sec/chaton/poston/l;-><init>(Landroid/view/ViewGroup;)V

    .line 149
    iget-object v0, v1, Lcom/sec/chaton/poston/l;->k:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/poston/d;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v6, v1

    move-object p2, v2

    .line 155
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_4

    .line 156
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->m:Landroid/widget/ImageView;

    const v1, 0x7f02043e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 163
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 164
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    iget-object v1, v6, Lcom/sec/chaton/poston/l;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :goto_2
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->h:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/chaton/poston/e;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/poston/e;-><init>(Lcom/sec/chaton/poston/d;I)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->g:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 184
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/poston/f;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/poston/f;-><init>(Lcom/sec/chaton/poston/d;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/poston/g;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/g;-><init>(Lcom/sec/chaton/poston/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 204
    iget-object v1, v6, Lcom/sec/chaton/poston/l;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/chaton/poston/d;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, v6, Lcom/sec/chaton/poston/l;->a:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 210
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 227
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 228
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->b:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0094

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    :goto_4
    iget-object v1, v6, Lcom/sec/chaton/poston/l;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->j()Ljava/util/ArrayList;

    move-result-object v4

    .line 243
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->l:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 244
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_14

    .line 245
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "for bGeo Position = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_1
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 252
    const/4 v3, 0x0

    .line 253
    const/4 v2, 0x0

    .line 254
    const/4 v0, 0x0

    .line 255
    const/4 v1, 0x0

    .line 256
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v7, v3

    move v3, v0

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/aa;

    .line 257
    if-nez v3, :cond_17

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v4

    const-string v8, "1"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v4

    const-string v8, "2"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 260
    :cond_2
    const/4 v3, 0x1

    move-object v4, v0

    .line 263
    :goto_6
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v7

    const-string v8, "3"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 264
    const/4 v1, 0x1

    move v9, v1

    move-object v1, v0

    move v0, v9

    :goto_7
    move-object v2, v1

    move-object v7, v4

    move v1, v0

    .line 265
    goto :goto_5

    .line 151
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/l;

    move-object v6, v0

    goto/16 :goto_0

    .line 158
    :cond_4
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->m:Landroid/widget/ImageView;

    const v1, 0x7f02043d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 167
    :cond_5
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 211
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 212
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_3

    .line 214
    :cond_7
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->a:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/poston/h;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/poston/h;-><init>(Lcom/sec/chaton/poston/d;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 236
    :cond_8
    iget-object v1, v6, Lcom/sec/chaton/poston/l;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/poston/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 269
    :cond_9
    if-eqz v1, :cond_d

    .line 270
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_a

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bGeo = true / Position = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_a
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->j:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 274
    invoke-virtual {p0, v2}, Lcom/sec/chaton/poston/d;->a(Lcom/sec/chaton/e/a/aa;)Ljava/lang/String;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_b

    .line 278
    iget-object v1, v6, Lcom/sec/chaton/poston/l;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->k:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/common/util/j;->a(Landroid/widget/TextView;)V

    .line 280
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 288
    :cond_b
    :goto_8
    if-eqz v3, :cond_13

    if-eqz v7, :cond_13

    .line 289
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 292
    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 293
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 296
    iget-object v1, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    if-nez v1, :cond_10

    .line 297
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/chaton/poston/d;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 299
    new-instance v0, Lcom/sec/chaton/poston/bb;

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Lcom/sec/chaton/poston/d;->e:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/poston/bb;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/bb;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 311
    :goto_9
    if-eqz v0, :cond_c

    .line 312
    iget-object v1, p0, Lcom/sec/chaton/poston/d;->h:Lcom/sec/common/f/c;

    iget-object v2, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 315
    :cond_c
    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/poston/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 316
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->l:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 336
    :goto_a
    invoke-virtual {p2, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 337
    return-object p2

    .line 283
    :cond_d
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_e

    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bGeo = false / Position = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_e
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->j:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_8

    .line 301
    :cond_f
    new-instance v0, Lcom/sec/chaton/poston/bb;

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Lcom/sec/chaton/poston/d;->e:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/poston/bb;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/bb;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 304
    :cond_10
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/chaton/poston/d;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 306
    new-instance v0, Lcom/sec/chaton/poston/bb;

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Lcom/sec/chaton/poston/d;->e:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/poston/bb;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/bb;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 308
    :cond_11
    new-instance v0, Lcom/sec/chaton/poston/bb;

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/d;->l:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Lcom/sec/chaton/poston/d;->e:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/poston/bb;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/bb;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 318
    :cond_12
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->l:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_a

    .line 321
    :cond_13
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->h:Lcom/sec/common/f/c;

    iget-object v1, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 323
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    .line 327
    :cond_14
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_15

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "for bGeo Position multimediaArray not exist = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :cond_15
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/poston/d;->h:Lcom/sec/common/f/c;

    iget-object v1, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 332
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 333
    iget-object v0, v6, Lcom/sec/chaton/poston/l;->j:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_a

    :cond_16
    move v0, v1

    move-object v1, v2

    goto/16 :goto_7

    :cond_17
    move-object v4, v7

    goto/16 :goto_6
.end method
