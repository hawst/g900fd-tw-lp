.class Lcom/sec/chaton/poston/i;
.super Ljava/lang/Object;
.source "PostONAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/d;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/d;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/sec/chaton/poston/i;->a:Lcom/sec/chaton/poston/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 345
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/poston/i;->a:Lcom/sec/chaton/poston/d;

    invoke-static {v0}, Lcom/sec/chaton/poston/d;->b(Lcom/sec/chaton/poston/d;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00de

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/aa;

    .line 352
    if-eqz v0, :cond_0

    .line 353
    iget-object v1, p0, Lcom/sec/chaton/poston/i;->a:Lcom/sec/chaton/poston/d;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/poston/d;->b(Lcom/sec/chaton/e/a/aa;)Ljava/lang/String;

    move-result-object v0

    .line 354
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 356
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 357
    const-string v3, "http://maps.google.com/maps?q=loc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 358
    if-eqz v0, :cond_2

    .line 359
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 362
    :cond_2
    if-eqz v1, :cond_3

    .line 363
    const-string v0, "[\\(\\)]"

    const-string v3, " "

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 364
    const-string v1, "("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 367
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 368
    iget-object v1, p0, Lcom/sec/chaton/poston/i;->a:Lcom/sec/chaton/poston/d;

    invoke-static {v1}, Lcom/sec/chaton/poston/d;->b(Lcom/sec/chaton/poston/d;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
