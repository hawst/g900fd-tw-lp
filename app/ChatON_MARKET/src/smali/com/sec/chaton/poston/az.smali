.class Lcom/sec/chaton/poston/az;
.super Ljava/lang/Object;
.source "PostONHideListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONHideListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONHideListFragment;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 151
    check-cast p1, Landroid/widget/CheckedTextView;

    .line 152
    invoke-virtual {p1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    .line 153
    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->b(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->c(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move v0, v1

    .line 157
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONHideListFragment;->d(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 158
    iget-object v2, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONHideListFragment;->d(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->b(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->c(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 165
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->d(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->d(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 165
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/az;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->d(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 171
    return-void
.end method
