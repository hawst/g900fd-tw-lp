.class Lcom/sec/chaton/poston/bt;
.super Ljava/lang/Object;
.source "PostONWriteFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 686
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 691
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/16 v5, 0x8c

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 695
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 697
    iget-object v1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->j(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 698
    iget-object v1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 699
    iget-object v1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 700
    iget-object v1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->j(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 701
    iget-object v1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->k(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 726
    :goto_0
    return-void

    .line 704
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->k(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 706
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->l(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->l(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 707
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 708
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->j(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 710
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->j(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 711
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    goto :goto_0

    .line 717
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_5

    .line 718
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    goto/16 :goto_0

    .line 720
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 721
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    goto/16 :goto_0

    .line 723
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/poston/bt;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    goto/16 :goto_0
.end method
