.class Lcom/sec/chaton/poston/ap;
.super Ljava/lang/Object;
.source "PostONDetailFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/chaton/poston/ap;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 367
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 349
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/chaton/poston/ap;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/poston/ap;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 362
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/ap;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->e(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method
