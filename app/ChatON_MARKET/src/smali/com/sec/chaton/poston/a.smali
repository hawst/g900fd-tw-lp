.class public Lcom/sec/chaton/poston/a;
.super Lcom/sec/common/f/a;
.source "CoverStoryDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field private static r:Ljava/lang/String;

.field private static s:Ljava/lang/String;


# instance fields
.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Landroid/content/Context;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private final t:I

.field private final u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-string v0, "mypage"

    sput-object v0, Lcom/sec/chaton/poston/a;->r:Ljava/lang/String;

    .line 77
    const-string v0, "buddy"

    sput-object v0, Lcom/sec/chaton/poston/a;->s:Ljava/lang/String;

    .line 78
    const-string v0, "sample"

    sput-object v0, Lcom/sec/chaton/poston/a;->a:Ljava/lang/String;

    .line 79
    const-string v0, "content"

    sput-object v0, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 69
    const-class v0, Lcom/sec/chaton/poston/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->m:Ljava/lang/String;

    .line 80
    const/high16 v0, 0x438f0000    # 286.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/poston/a;->t:I

    .line 81
    const/high16 v0, 0x43380000    # 184.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/poston/a;->u:I

    .line 85
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "CoverStoryDispatcherTask Start..."

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/poston/a;->c:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Lcom/sec/chaton/poston/a;->i:Ljava/lang/String;

    .line 91
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/poston/a;->d:I

    .line 92
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/poston/a;->e:I

    .line 93
    iput-object p3, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    .line 94
    iput-object p4, p0, Lcom/sec/chaton/poston/a;->o:Landroid/content/Context;

    .line 95
    iput-object p5, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    .line 96
    sget-object v0, Lcom/sec/chaton/poston/a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    iput-object p6, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    .line 98
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IMAGE_FROM_SAMPLE [mFileName] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ## [mFileDir] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    :goto_0
    sget-object v0, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 115
    iput-object p7, p0, Lcom/sec/chaton/poston/a;->p:Ljava/lang/String;

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/random/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->q:Ljava/lang/String;

    .line 118
    :cond_2
    return-void

    .line 101
    :cond_3
    sget-object v0, Lcom/sec/chaton/poston/a;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 102
    const-string v0, "mycoverstory.jpg"

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    .line 103
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IMAGE_FROM_MYPAGE [mFileName] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ## [mFileDir] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_4
    iput-object p6, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    .line 109
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOT IMAGE_FROM_SAMPLE [mFileName] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ## [mFileDir] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 69
    const-class v0, Lcom/sec/chaton/poston/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->m:Ljava/lang/String;

    .line 80
    const/high16 v0, 0x438f0000    # 286.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/poston/a;->t:I

    .line 81
    const/high16 v0, 0x43380000    # 184.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/poston/a;->u:I

    .line 138
    iput-object p1, p0, Lcom/sec/chaton/poston/a;->c:Ljava/lang/String;

    .line 139
    iput-object p2, p0, Lcom/sec/chaton/poston/a;->i:Ljava/lang/String;

    .line 140
    iput-object p3, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    .line 141
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/poston/a;->d:I

    .line 142
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/poston/a;->e:I

    .line 143
    iput-object p4, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    .line 144
    iput-object p5, p0, Lcom/sec/chaton/poston/a;->o:Landroid/content/Context;

    .line 146
    const-string v0, "coverstory.jpg"

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 69
    const-class v0, Lcom/sec/chaton/poston/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/a;->m:Ljava/lang/String;

    .line 80
    const/high16 v0, 0x438f0000    # 286.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/poston/a;->t:I

    .line 81
    const/high16 v0, 0x43380000    # 184.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/poston/a;->u:I

    .line 152
    iput-object p1, p0, Lcom/sec/chaton/poston/a;->c:Ljava/lang/String;

    .line 153
    iput-object p2, p0, Lcom/sec/chaton/poston/a;->i:Ljava/lang/String;

    .line 154
    iput-object p3, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    .line 155
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/poston/a;->d:I

    .line 156
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/poston/a;->e:I

    .line 157
    iput-object p5, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    .line 158
    iput-object p6, p0, Lcom/sec/chaton/poston/a;->o:Landroid/content/Context;

    .line 159
    iput-object p4, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    .line 160
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 333
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->a()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 314
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 317
    const-string v0, " coverstory onPostDispatch getView() is NULL"

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 181
    sget-object v0, Lcom/sec/chaton/poston/a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->a()Landroid/widget/ImageView;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_1

    .line 184
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 185
    const-string v1, "onPreDispatch() WHITE SCREEN CHECK"

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 194
    :cond_1
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 199
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDispatch() [mFileDir] : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ## [mFileName] : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 206
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 210
    :cond_1
    :try_start_0
    sget-object v0, Lcom/sec/chaton/poston/a;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 211
    :cond_2
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 212
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IMAGE_FROM_SAMPLE mUrl || IMAGE_FROM_CONTENT  : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_3
    :goto_0
    new-instance v0, Lcom/sec/common/e/b;

    invoke-direct {v0}, Lcom/sec/common/e/b;-><init>()V

    invoke-virtual {v0, v2}, Lcom/sec/common/e/b;->a(Ljava/io/File;)Lcom/sec/common/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/e/b;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_4

    .line 224
    const/16 v3, 0xa0

    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->setDensity(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 280
    :cond_4
    :goto_1
    sget-object v1, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/poston/a;->p:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 284
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    .line 285
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "copyCoverStoryforRandomImages\t[mOriginPath] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ## [mDestDir] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/poston/a;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 288
    iget-object v2, p0, Lcom/sec/chaton/poston/a;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/poston/a;->q:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_6
    if-nez v0, :cond_7

    .line 293
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 295
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/poston/a;->i:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_4

    .line 307
    :cond_7
    :goto_2
    return-object v0

    .line 218
    :cond_8
    :try_start_2
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/poston/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/poston/a;->m:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 227
    :catch_0
    move-exception v0

    .line 228
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_9

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FileNotFoundException onDispatch() [mFileDir] : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ## [mFileName] : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->a()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 232
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_a

    .line 233
    const-string v0, "FileNotFoundException onDispatch() Pattern Setting ~!! loadDefaultCoverStory()"

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_a
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    .line 240
    :cond_b
    const-string v0, "coverstory_metaid"

    const-string v2, "1"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_c

    .line 242
    const-string v0, "[CoverStory] #1# PrefConst.PREF_MYPAGE_COVERSTORY_METAID set 1"

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move-object v0, v1

    .line 256
    goto/16 :goto_1

    .line 245
    :catch_1
    move-exception v0

    .line 246
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 247
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_d

    .line 248
    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 251
    :cond_d
    const-string v0, "coverstory_metaid"

    const-string v2, "1"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_e

    .line 253
    const-string v0, "[CoverStory] #2# PrefConst.PREF_MYPAGE_COVERSTORY_METAID set 1"

    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    move-object v0, v1

    .line 256
    goto/16 :goto_1

    .line 258
    :cond_f
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 262
    :try_start_3
    new-instance v0, Lcom/sec/common/e/b;

    invoke-direct {v0}, Lcom/sec/common/e/b;-><init>()V

    invoke-virtual {v0, v2}, Lcom/sec/common/e/b;->a(Ljava/io/File;)Lcom/sec/common/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/e/b;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 263
    if-eqz v0, :cond_4

    .line 264
    const/16 v3, 0xa0

    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->setDensity(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 266
    :catch_2
    move-exception v0

    .line 267
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 268
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_10

    .line 269
    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_10
    move-object v0, v1

    .line 273
    goto/16 :goto_1

    :cond_11
    move-object v0, v1

    .line 275
    goto/16 :goto_1

    .line 296
    :catch_3
    move-exception v1

    .line 297
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_7

    .line 298
    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 300
    :catch_4
    move-exception v1

    .line 301
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_7

    .line 302
    iget-object v2, p0, Lcom/sec/chaton/poston/a;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public d()V
    .locals 2

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 325
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/poston/a;->a(Landroid/view/View;)V

    .line 326
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 327
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 329
    :cond_0
    return-void
.end method

.method public e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 338
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/chaton/poston/a;->a()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
