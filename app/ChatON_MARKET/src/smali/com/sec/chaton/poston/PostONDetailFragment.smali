.class public Lcom/sec/chaton/poston/PostONDetailFragment;
.super Landroid/support/v4/app/Fragment;
.source "PostONDetailFragment.java"

# interfaces
.implements Lcom/sec/chaton/poston/o;
.implements Lcom/sec/chaton/poston/w;


# static fields
.field public static a:Ljava/lang/String;

.field private static av:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Ljava/lang/String;

.field public static m:Ljava/lang/String;

.field public static n:Ljava/lang/String;

.field public static o:Ljava/lang/String;

.field public static p:Ljava/lang/String;

.field public static final q:Ljava/lang/String;


# instance fields
.field private A:Landroid/content/Context;

.field private B:Landroid/widget/ImageView;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Landroid/widget/ListView;

.field private S:Lcom/sec/chaton/poston/m;

.field private T:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/poston/p;",
            ">;"
        }
    .end annotation
.end field

.field private U:Lcom/sec/chaton/d/v;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Ljava/lang/String;

.field private Y:Landroid/app/ProgressDialog;

.field private Z:Landroid/widget/TextView;

.field private aA:Landroid/os/Handler;

.field private aa:Landroid/widget/TextView;

.field private ab:Landroid/widget/FrameLayout;

.field private ac:Landroid/widget/FrameLayout;

.field private ad:Landroid/widget/ImageButton;

.field private ae:Landroid/widget/EditText;

.field private af:Landroid/view/inputmethod/InputMethodManager;

.field private ag:Landroid/view/Menu;

.field private ah:Landroid/app/ProgressDialog;

.field private ai:I

.field private aj:Lcom/sec/common/a/d;

.field private ak:Landroid/widget/FrameLayout;

.field private al:Z

.field private am:Z

.field private an:Lcom/sec/common/f/c;

.field private ao:Landroid/view/LayoutInflater;

.field private ap:Z

.field private aq:Z

.field private ar:Z

.field private as:Z

.field private at:Ljava/lang/String;

.field private au:Ljava/io/File;

.field private aw:Lcom/sec/chaton/widget/c;

.field private ax:Ljava/lang/String;

.field private ay:Landroid/os/Handler;

.field private az:Landroid/view/View$OnClickListener;

.field r:Landroid/widget/AbsListView$OnScrollListener;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/ImageView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 98
    const-string v0, "POSTON_BUDDYID"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->a:Ljava/lang/String;

    .line 99
    const-string v0, "POSTON_SENDER"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->b:Ljava/lang/String;

    .line 100
    const-string v0, "POSTON_SENDER_NAME"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->c:Ljava/lang/String;

    .line 101
    const-string v0, "POSTON_REG_TIME"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->d:Ljava/lang/String;

    .line 102
    const-string v0, "POSTON_FORMATTED_TIME"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->e:Ljava/lang/String;

    .line 103
    const-string v0, "POSTON"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->f:Ljava/lang/String;

    .line 104
    const-string v0, "POSTON_COMMENT_COUNT"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    .line 105
    const-string v0, "POSTON_COMMENT_UPDATED"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->h:Ljava/lang/String;

    .line 106
    const-string v0, "POSTON_ID"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->i:Ljava/lang/String;

    .line 107
    const-string v0, "POSTON_MULTIMEDIA_URL"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->j:Ljava/lang/String;

    .line 108
    const-string v0, "POSTON_META_TYPE"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->k:Ljava/lang/String;

    .line 109
    const-string v0, "POSTON_SENDER_NO"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->l:Ljava/lang/String;

    .line 110
    const-string v0, "POSTON_GEO_TAG"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->m:Ljava/lang/String;

    .line 111
    const-string v0, "POSTON_ISFROM"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->n:Ljava/lang/String;

    .line 115
    const-string v0, "POSTON_CALLED_FROM"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->o:Ljava/lang/String;

    .line 121
    const-string v0, "FROM_PAGER"

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->p:Ljava/lang/String;

    .line 123
    const-class v0, Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/poston/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->av:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 96
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->T:Ljava/util/ArrayList;

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ax:Ljava/lang/String;

    .line 621
    new-instance v0, Lcom/sec/chaton/poston/au;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/au;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ay:Landroid/os/Handler;

    .line 673
    new-instance v0, Lcom/sec/chaton/poston/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/av;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->az:Landroid/view/View$OnClickListener;

    .line 902
    new-instance v0, Lcom/sec/chaton/poston/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/z;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->r:Landroid/widget/AbsListView$OnScrollListener;

    .line 947
    new-instance v0, Lcom/sec/chaton/poston/aa;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/aa;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aA:Landroid/os/Handler;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ah:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/chaton/poston/PostONDetailFragment;)I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ai:I

    return v0
.end method

.method static synthetic C(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic D(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lcom/sec/chaton/poston/PostONDetailFragment;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->am:Z

    return v0
.end method

.method static synthetic F(Lcom/sec/chaton/poston/PostONDetailFragment;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ar:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONDetailFragment;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ad:Landroid/widget/ImageButton;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONDetailFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aj:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->O:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONDetailFragment;Z)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 857
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ap:Z

    .line 859
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 860
    if-eqz p1, :cond_1

    .line 861
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 866
    :cond_0
    :goto_0
    return-void

    .line 863
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 492
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_1

    .line 493
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 494
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v1, ","

    invoke-direct {v3, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v1, ""

    .line 496
    const/4 v0, 0x0

    .line 497
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 498
    add-int/lit8 v2, v0, 0x1

    .line 499
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 500
    const/4 v4, 0x3

    if-lt v2, v4, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 501
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    :goto_1
    move-object v1, v0

    move v0, v2

    .line 507
    goto :goto_0

    .line 504
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 511
    :cond_1
    const/4 v1, 0x0

    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 226
    new-instance v0, Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aA:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/v;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->U:Lcom/sec/chaton/d/v;

    .line 227
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    .line 228
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 230
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 231
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    .line 232
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->E:Ljava/lang/String;

    .line 233
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->F:Ljava/lang/String;

    .line 234
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->G:Ljava/lang/String;

    .line 235
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->H:Ljava/lang/String;

    .line 236
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->I:Ljava/lang/String;

    .line 237
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->J:Ljava/lang/String;

    .line 238
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->K:Ljava/lang/String;

    .line 239
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    .line 240
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->M:Ljava/lang/String;

    .line 241
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->N:Ljava/lang/String;

    .line 242
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->O:Ljava/lang/String;

    .line 243
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->P:Ljava/lang/String;

    .line 244
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Q:Ljava/lang/String;

    .line 247
    const-string v0, "MY_PAGE"

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->am:Z

    .line 253
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->af:Landroid/view/inputmethod/InputMethodManager;

    .line 254
    return-void

    .line 250
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->am:Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/poston/PostONDetailFragment;Z)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1327
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1328
    const-string v1, "read_comment_count"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    const-string v1, "unread_comment_count"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1331
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ae;->a:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "poston_id=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->K:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1332
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 869
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aq:Z

    .line 871
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 872
    if-eqz p1, :cond_1

    .line 873
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 878
    :cond_0
    :goto_0
    return-void

    .line 875
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->F:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->P:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->P:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->v:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->P:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    if-eqz v0, :cond_1

    .line 474
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLocationView addr = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aa:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aa:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/common/util/j;->a(Landroid/widget/TextView;)V

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aa:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 489
    :cond_1
    :goto_0
    return-void

    .line 484
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 485
    const-string v0, "setLocationView no location"

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->v:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->al:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ae:Landroid/widget/EditText;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 518
    invoke-direct {p0, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Z)V

    .line 519
    invoke-direct {p0, v4}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Z)V

    .line 521
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->M:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/poston/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 522
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 523
    const-string v0, "setImageView has Multimedia"

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 534
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 535
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 536
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->at:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->au:Ljava/io/File;

    .line 538
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->au:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->au:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 539
    invoke-direct {p0, v5}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Z)V

    .line 540
    invoke-direct {p0, v5}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Z)V

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->M:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/poston/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ab:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ac:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ac:Landroid/widget/FrameLayout;

    const v1, 0x7f020020

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ac:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/sec/chaton/poston/as;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/as;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 576
    :goto_0
    new-instance v0, Lcom/sec/chaton/poston/bb;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->M:Ljava/lang/String;

    sget-object v4, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Lcom/sec/chaton/poston/bb;->c:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/poston/bb;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ay:Landroid/os/Handler;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/poston/bb;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 578
    if-eqz v0, :cond_2

    .line 579
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->an:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->B:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 593
    :cond_2
    :goto_1
    return-void

    .line 572
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ab:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 573
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->B:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 582
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 583
    const-string v0, "setImageView has No Multimedia"

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 589
    invoke-direct {p0, v5}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Z)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->as:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ad:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 596
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    if-nez v0, :cond_0

    .line 597
    new-instance v0, Lcom/sec/chaton/widget/c;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/widget/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b0226

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setTitle(I)V

    .line 599
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b01ce

    invoke-virtual {p0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setMessage(Ljava/lang/CharSequence;)V

    .line 600
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/c;->setCancelable(Z)V

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/at;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/at;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/widget/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 608
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->show()V

    .line 609
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 612
    :try_start_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ay:Landroid/os/Handler;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ax:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->au:Ljava/io/File;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 619
    :cond_1
    :goto_0
    return-void

    .line 614
    :catch_0
    move-exception v0

    .line 615
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 616
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/poston/PostONDetailFragment;Z)Z
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ar:Z

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->s:Landroid/view/View;

    return-object v0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1336
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1353
    :goto_0
    return-void

    .line 1340
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1341
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1344
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1345
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 1347
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 1348
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveMedia File : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->at:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    :cond_2
    new-instance v1, Lcom/sec/chaton/multimedia/a/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->at:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ChatON"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2, v4}, Lcom/sec/chaton/multimedia/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1352
    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->E:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .locals 7

    .prologue
    const v2, 0x7f0b0166

    const/4 v3, 0x0

    .line 1359
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1385
    :cond_0
    :goto_0
    return-void

    .line 1364
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1365
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1366
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v6, v0, v1

    .line 1367
    sget-object v0, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1368
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->at:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    .line 1373
    :cond_2
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 1374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "shareMedia File : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1381
    :cond_3
    :goto_2
    if-eqz v3, :cond_0

    .line 1382
    invoke-virtual {p0, v3}, Lcom/sec/chaton/poston/PostONDetailFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1370
    :cond_4
    sget-object v0, Lcom/sec/chaton/poston/d;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1371
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->at:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v3, v3}, Lcom/sec/chaton/util/ch;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    goto :goto_1

    .line 1376
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1377
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->I:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)Landroid/content/Intent;

    move-result-object v3

    goto :goto_2
.end method

.method static synthetic h(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->G:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/d/v;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->U:Lcom/sec/chaton/d/v;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->af:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Y:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->au:Ljava/io/File;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/poston/PostONDetailFragment;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->e()V

    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ay:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ax:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->M:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->B:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->an:Lcom/sec/common/f/c;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/chaton/poston/PostONDetailFragment;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->al:Z

    return v0
.end method

.method static synthetic y(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->V:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->W:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 709
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 710
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 711
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, ","

    invoke-direct {v1, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 715
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 716
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 721
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 1500
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/ax;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Z:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->as:Z

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/poston/ax;->a(Ljava/lang/String;Z)V

    .line 1502
    return-void
.end method

.method public a(ILandroid/app/ProgressDialog;)V
    .locals 3

    .prologue
    .line 885
    if-eqz p2, :cond_0

    .line 886
    iput-object p2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ah:Landroid/app/ProgressDialog;

    .line 887
    iput p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ai:I

    .line 892
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->al:Z

    .line 893
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->U:Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->K:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/v;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    return-void

    .line 889
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 10

    .prologue
    const/16 v2, 0x8

    const/4 v9, 0x0

    .line 1217
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_b

    .line 1218
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONCommentList;

    .line 1219
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONCommentList;->commentlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONCommentList;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->hasmore:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->V:Ljava/lang/String;

    .line 1220
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONCommentList;->commentlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONCommentList;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->endtime:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->W:Ljava/lang/String;

    .line 1221
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONCommentList;->commentlist:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONCommentList;->totalcount:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->X:Ljava/lang/String;

    .line 1223
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1224
    const/4 v0, 0x0

    .line 1225
    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1228
    :try_start_0
    invoke-static {v1}, Lcom/sec/chaton/e/a/z;->c(Landroid/content/ContentResolver;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    .line 1229
    if-nez v7, :cond_1

    .line 1231
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Z:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1296
    if-eqz v7, :cond_0

    .line 1297
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1324
    :cond_0
    :goto_0
    return-void

    .line 1234
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostON Comment list count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->J:Ljava/lang/String;

    .line 1238
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->am:Z

    if-eqz v0, :cond_2

    .line 1239
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->X:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->W:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    .line 1243
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 1244
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1246
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    if-eqz v0, :cond_4

    .line 1247
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/m;->notifyDataSetChanged()V

    .line 1249
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1253
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Z:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1296
    :goto_1
    if-eqz v7, :cond_5

    .line 1297
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1323
    :cond_5
    :goto_2
    iput-boolean v9, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->al:Z

    goto :goto_0

    .line 1256
    :cond_6
    :goto_3
    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1257
    const-string v0, "buddy_no"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1259
    const-string v0, "joined_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1260
    const-string v0, "comment"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1261
    const-string v0, "timestamp"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1262
    const-string v0, "isread"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1263
    const-string v0, "commentid"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "name: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "comment: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "time: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "isRead: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "sender: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    iget-object v8, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->T:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/poston/p;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/poston/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 1293
    :catch_0
    move-exception v0

    move-object v0, v7

    .line 1294
    :goto_4
    :try_start_4
    const-string v1, "Exception is occurred in showPostONCommentList"

    sget-object v2, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1296
    if-eqz v0, :cond_5

    .line 1297
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 1273
    :cond_7
    :try_start_5
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_8

    .line 1274
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1278
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    if-nez v0, :cond_a

    .line 1279
    new-instance v0, Lcom/sec/chaton/poston/m;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->T:Ljava/util/ArrayList;

    const v4, 0x7f03011f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/poston/m;-><init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    .line 1280
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1290
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->X:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 1296
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v7, :cond_9

    .line 1297
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1296
    :cond_9
    throw v0

    .line 1282
    :cond_a
    :try_start_6
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/m;->notifyDataSetChanged()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 1301
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Y:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_c

    .line 1302
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Y:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1305
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_d

    .line 1306
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1310
    :cond_d
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1311
    const/4 v1, -0x3

    if-eq v1, v0, :cond_e

    const/4 v1, -0x2

    if-ne v1, v0, :cond_f

    .line 1312
    :cond_e
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v9}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1318
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1319
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    if-eqz v0, :cond_5

    .line 1320
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/m;->notifyDataSetChanged()V

    goto/16 :goto_2

    .line 1315
    :cond_f
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v9}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_7

    .line 1296
    :catchall_1
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_6

    :catchall_2
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_6

    .line 1293
    :catch_1
    move-exception v1

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1390
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1396
    :goto_0
    return-void

    .line 1394
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/ax;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/poston/ax;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 216
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 217
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->V:Ljava/lang/String;

    .line 218
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->W:Ljava/lang/String;

    .line 219
    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ah:Landroid/app/ProgressDialog;

    .line 220
    iput-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aj:Lcom/sec/common/a/d;

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->al:Z

    .line 222
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->b()V

    .line 223
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 11

    .prologue
    const v10, 0x7f0b0138

    const v9, 0x7f0b0094

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1402
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1404
    new-instance v1, Lcom/sec/chaton/b/a;

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    invoke-direct {v1, v0, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 1406
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 1407
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget v2, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/p;

    .line 1409
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1495
    :cond_0
    :goto_0
    return-void

    .line 1412
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/poston/p;->d()Ljava/lang/String;

    move-result-object v2

    .line 1413
    invoke-virtual {v0}, Lcom/sec/chaton/poston/p;->a()Ljava/lang/String;

    move-result-object v3

    .line 1414
    invoke-virtual {v0}, Lcom/sec/chaton/poston/p;->e()Ljava/lang/String;

    move-result-object v4

    .line 1416
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 1421
    iget-object v5, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1422
    iget-object v5, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1423
    invoke-virtual {v1, v9}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    .line 1428
    :goto_1
    invoke-virtual {v1, v7, v7, v8, v10}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/poston/aj;

    invoke-direct {v1, p0, v3, v2, v4}, Lcom/sec/chaton/poston/aj;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 1425
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/poston/p;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    goto :goto_1

    .line 1464
    :cond_3
    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1465
    invoke-virtual {v1, v9}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    .line 1466
    invoke-virtual {v1, v7, v7, v8, v10}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/poston/am;

    invoke-direct {v1, p0, v2, v4}, Lcom/sec/chaton/poston/am;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 792
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    .line 793
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-super {p0, v0, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 794
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const v4, 0x7f07014c

    const/16 v3, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 258
    invoke-static {p0, v8}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 259
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    .line 260
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->an:Lcom/sec/common/f/c;

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ao:Landroid/view/LayoutInflater;

    .line 265
    const v0, 0x7f0300de

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->s:Landroid/view/View;

    .line 266
    const v0, 0x7f03007e

    invoke-virtual {p1, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->w:Landroid/widget/ImageView;

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->x:Landroid/widget/TextView;

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->y:Landroid/widget/TextView;

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    const v1, 0x7f0702e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->z:Landroid/widget/TextView;

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    const v1, 0x7f0702e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->B:Landroid/widget/ImageView;

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    const v1, 0x7f0702e2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ab:Landroid/widget/FrameLayout;

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    const v1, 0x7f0702e0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ac:Landroid/widget/FrameLayout;

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->z:Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 278
    new-instance v1, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 279
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020138

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ab:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 282
    const/16 v2, 0x10

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ab:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ab:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->av:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->at:Ljava/lang/String;

    .line 289
    const v0, 0x7f0300dd

    invoke-virtual {p1, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->u:Landroid/view/View;

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->u:Landroid/view/View;

    const v1, 0x7f0703df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->v:Landroid/view/View;

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->v:Landroid/view/View;

    const v1, 0x7f0704ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aa:Landroid/widget/TextView;

    .line 293
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aa:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->az:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->u:Landroid/view/View;

    const v1, 0x7f0703e0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 296
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Z:Landroid/widget/TextView;

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Z:Landroid/widget/TextView;

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->u:Landroid/view/View;

    const v1, 0x7f0703e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ak:Landroid/widget/FrameLayout;

    .line 304
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 310
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->c()V

    .line 311
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->d()V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->A:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->w:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->w:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/poston/x;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/x;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->y:Landroid/widget/TextView;

    const v1, 0x7f0b0094

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 338
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->x:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->s:Landroid/view/View;

    const v1, 0x7f0703e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ae:Landroid/widget/EditText;

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ae:Landroid/widget/EditText;

    new-array v1, v8, [Landroid/text/InputFilter;

    new-instance v2, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/16 v4, 0x8c

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ae:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/poston/ap;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/ap;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ae:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/poston/aq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/aq;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->s:Landroid/view/View;

    const v1, 0x7f0703e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ad:Landroid/widget/ImageButton;

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ad:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ad:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/poston/ar;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/ar;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->s:Landroid/view/View;

    const v1, 0x7f0703e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 425
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Y:Landroid/app/ProgressDialog;

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v1, v7, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 428
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v1, v7, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 432
    new-instance v0, Lcom/sec/chaton/poston/m;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->T:Ljava/util/ArrayList;

    const v4, 0x7f03011f

    iget-object v5, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/m;-><init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/poston/m;->a(Lcom/sec/chaton/poston/o;)V

    .line 434
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->S:Lcom/sec/chaton/poston/m;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->R:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->r:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 444
    invoke-virtual {p0, v6, v7}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(ILandroid/app/ProgressDialog;)V

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->s:Landroid/view/View;

    return-object v0

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->y:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 924
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 925
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Y:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 926
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Y:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ah:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 929
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ah:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 931
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aj:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    .line 932
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aj:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 935
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->U:Lcom/sec/chaton/d/v;

    invoke-virtual {v0}, Lcom/sec/chaton/d/v;->d()V

    .line 936
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 940
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 941
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 942
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ay:Landroid/os/Handler;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->L:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ax:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;)Z

    .line 943
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aw:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 945
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 798
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 852
    :goto_0
    return v3

    .line 802
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/ax;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->Z:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->as:Z

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/poston/ax;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 807
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->f()V

    goto :goto_0

    .line 811
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->g()V

    goto :goto_0

    .line 815
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0138

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b03f6

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/poston/y;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/y;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/poston/aw;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/aw;-><init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aj:Lcom/sec/common/a/d;

    goto :goto_0

    .line 798
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 7

    .prologue
    const v6, 0x7f02030e

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 765
    const-string v0, "onPrepareOptionMenu..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 767
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->D:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->E:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 768
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    const v1, 0x7f0b0165

    invoke-virtual {p0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v4, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020312

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 769
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    const v1, 0x7f0b003c

    invoke-virtual {p0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v2, v4, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 776
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ap:Z

    if-eqz v0, :cond_1

    .line 777
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 781
    :goto_1
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->aq:Z

    if-eqz v0, :cond_2

    .line 782
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 787
    :goto_2
    return-void

    .line 771
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    const v1, 0x7f0b0165

    invoke-virtual {p0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v4, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020312

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 772
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    const v1, 0x7f0b003c

    invoke-virtual {p0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v2, v4, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 773
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    const v1, 0x7f0b005b

    invoke-interface {v0, v3, v5, v5, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020305

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 774
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 779
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 784
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailFragment;->ag:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method
