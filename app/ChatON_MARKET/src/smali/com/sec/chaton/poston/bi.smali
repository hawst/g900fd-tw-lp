.class Lcom/sec/chaton/poston/bi;
.super Ljava/lang/Object;
.source "PostONWriteFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 960
    iput-object p1, p0, Lcom/sec/chaton/poston/bi;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 965
    packed-switch p2, :pswitch_data_0

    .line 995
    :cond_0
    :goto_0
    return-void

    .line 968
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 969
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 972
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/poston/bi;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 973
    :catch_0
    move-exception v0

    .line 974
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 975
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 976
    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 982
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 983
    iget-object v0, p0, Lcom/sec/chaton/poston/bi;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 985
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 987
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/poston/bi;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 988
    const-string v2, "preview_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 989
    const-string v0, "caller"

    const-string v2, "POSTON"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 990
    const-string v0, "attachedimagecount"

    iget-object v2, p0, Lcom/sec/chaton/poston/bi;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 991
    iget-object v0, p0, Lcom/sec/chaton/poston/bi;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 965
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
