.class Lcom/sec/chaton/poston/by;
.super Landroid/os/AsyncTask;
.source "PostONWriteFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/sec/chaton/e/w;

.field private d:Lcom/sec/chaton/j/c/i;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/util/ArrayList;Lcom/sec/chaton/e/w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;",
            "Lcom/sec/chaton/e/w;",
            ")V"
        }
    .end annotation

    .prologue
    .line 561
    iput-object p1, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 562
    iput-object p2, p0, Lcom/sec/chaton/poston/by;->b:Ljava/util/ArrayList;

    .line 563
    iput-object p3, p0, Lcom/sec/chaton/poston/by;->c:Lcom/sec/chaton/e/w;

    .line 564
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 576
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 577
    iget-object v3, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v0, p0, Lcom/sec/chaton/poston/by;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-static {v3, v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/chaton/multimedia/multisend/PreviewData;)V

    .line 576
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 579
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 582
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 583
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/poston/by;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 587
    :goto_2
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->f(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/poston/by;->c:Lcom/sec/chaton/e/w;

    iget-object v5, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v5}, Lcom/sec/chaton/poston/PostONWriteFragment;->g(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/sec/chaton/j/c/g;->a(Landroid/os/Handler;Ljava/io/File;Lcom/sec/chaton/e/w;Landroid/os/Handler;)Lcom/sec/chaton/j/c/i;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/by;->d:Lcom/sec/chaton/j/c/i;

    .line 579
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 585
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/poston/by;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_2

    .line 588
    :catch_0
    move-exception v0

    .line 589
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 593
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->d:Lcom/sec/chaton/j/c/i;

    if-eqz v0, :cond_0

    .line 606
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/by;->d:Lcom/sec/chaton/j/c/i;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/g;->a(Lcom/sec/chaton/j/c/i;)Z

    .line 608
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 1

    .prologue
    .line 598
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 599
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->h(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->h(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 602
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 555
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/poston/by;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 555
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/poston/by;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 568
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->d(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/chaton/widget/c;)Lcom/sec/chaton/widget/c;

    .line 570
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/poston/by;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->show()V

    .line 572
    return-void
.end method
