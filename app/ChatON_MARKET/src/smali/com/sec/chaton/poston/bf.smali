.class Lcom/sec/chaton/poston/bf;
.super Ljava/lang/Object;
.source "PostONWriteFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 869
    iput-object p1, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 872
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->l(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->l(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 873
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_1

    .line 874
    const-string v0, "mLocationData null"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Landroid/widget/EditText;Z)V

    .line 883
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 884
    const-string v1, "sendbutton"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 885
    iget-object v1, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 910
    :goto_0
    return-void

    .line 889
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_3

    .line 890
    const-string v0, "mLocationButton Click"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 893
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->o(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 894
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->n(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    .line 895
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 907
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Landroid/widget/EditText;Z)V

    goto :goto_0

    .line 897
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->o(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 898
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 900
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->o(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 901
    iget-object v0, p0, Lcom/sec/chaton/poston/bf;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->n(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_1
.end method
