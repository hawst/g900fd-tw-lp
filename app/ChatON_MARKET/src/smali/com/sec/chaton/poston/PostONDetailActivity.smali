.class public Lcom/sec/chaton/poston/PostONDetailActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "PostONDetailActivity.java"

# interfaces
.implements Lcom/sec/chaton/poston/ax;


# instance fields
.field private a:Lcom/sec/chaton/poston/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-direct {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/chaton/poston/PostONDetailActivity;->a:Lcom/sec/chaton/poston/w;

    .line 36
    return-object v0
.end method

.method public a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    const-class v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 59
    const-string v1, "video/*"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v1, "android.intent.extra.finishOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 62
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/poston/PostONDetailActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 65
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 75
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    invoke-virtual {p0, v0}, Lcom/sec/chaton/poston/PostONDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 96
    :goto_0
    return-void

    .line 83
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 89
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 94
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/chaton/poston/PostONDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 91
    :cond_1
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 130
    if-eqz p1, :cond_0

    .line 131
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 132
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 134
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/poston/PostONDetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONDetailActivity;->finish()V

    .line 137
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailActivity;->a:Lcom/sec/chaton/poston/w;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONDetailActivity;->a:Lcom/sec/chaton/poston/w;

    invoke-interface {v0}, Lcom/sec/chaton/poston/w;->a()V

    .line 125
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 113
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 116
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 104
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 107
    :cond_0
    return-void
.end method
