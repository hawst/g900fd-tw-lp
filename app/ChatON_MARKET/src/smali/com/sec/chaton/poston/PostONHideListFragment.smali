.class public Lcom/sec/chaton/poston/PostONHideListFragment;
.super Landroid/support/v4/app/ListFragment;
.source "PostONHideListFragment.java"


# static fields
.field static a:Landroid/app/ProgressDialog;


# instance fields
.field public b:Landroid/os/Handler;

.field private c:Landroid/widget/ListView;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Lcom/sec/chaton/poston/b;

.field private g:Landroid/view/View;

.field private h:Landroid/view/ViewStub;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/CheckedTextView;

.field private k:Ljava/lang/String;

.field private l:Landroid/view/MenuItem;

.field private m:Lcom/sec/chaton/d/v;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 289
    new-instance v0, Lcom/sec/chaton/poston/ba;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/ba;-><init>(Lcom/sec/chaton/poston/PostONHideListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->i:Landroid/view/View;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 194
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 195
    iget-object v2, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    const v3, 0x7f07014d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 196
    const v3, 0x7f02034b

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 197
    const v0, 0x7f0b0155

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 198
    const v0, 0x7f0b0184

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 200
    :cond_0
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTotalData.get(i).buddyid = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    iget-object v0, v0, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    iget-object v0, v0, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 250
    :cond_2
    return-void
.end method

.method private b()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 326
    move v1, v0

    .line 328
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 329
    iget-object v2, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    add-int/lit8 v1, v1, 0x1

    .line 328
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 334
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCheckedItemCount() Result : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :cond_2
    return v1
.end method

.method static synthetic b(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/poston/PostONHideListFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 84
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 85
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 204
    const v0, 0x7f0f0004

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 205
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 206
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 97
    const v0, 0x7f0300eb

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 100
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000d

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    sput-object v0, Lcom/sec/chaton/poston/PostONHideListFragment;->a:Landroid/app/ProgressDialog;

    .line 102
    new-instance v0, Lcom/sec/chaton/d/v;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->b:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/v;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->m:Lcom/sec/chaton/d/v;

    .line 105
    const v0, 0x7f070422

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->h:Landroid/view/ViewStub;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    .line 109
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "block_buddy_list"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->e:I

    .line 115
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "block_buddy_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->k:Ljava/lang/String;

    .line 118
    new-instance v0, Lcom/sec/chaton/poston/b;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/poston/b;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->f:Lcom/sec/chaton/poston/b;

    .line 121
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->f:Lcom/sec/chaton/poston/b;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->c:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/poston/ay;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/ay;-><init>(Lcom/sec/chaton/poston/PostONHideListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 148
    const v0, 0x7f070324

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    new-instance v2, Lcom/sec/chaton/poston/az;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/az;-><init>(Lcom/sec/chaton/poston/PostONHideListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->e:I

    if-nez v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 177
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->a()V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 187
    :cond_0
    :goto_0
    return-object v1

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 255
    invoke-super/range {p0 .. p5}, Landroid/support/v4/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 258
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 264
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->b()I

    move-result v0

    if-lez v0, :cond_3

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 270
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "list size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "list getCheckedItemCount() = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_1
    return-void

    .line 260
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->b()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->e:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->j:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 267
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 221
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070574

    if-ne v0, v1, :cond_0

    .line 222
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 224
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070575

    if-ne v0, v1, :cond_1

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 226
    invoke-direct {p0, v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->a(Ljava/util/ArrayList;)V

    .line 227
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->k:Ljava/lang/String;

    const-string v2, "post_on"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 228
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->m:Lcom/sec/chaton/d/v;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/d/v;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 229
    sget-object v0, Lcom/sec/chaton/poston/PostONHideListFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 232
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_2

    .line 233
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 234
    const/4 v0, 0x1

    .line 236
    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 211
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    .line 213
    const v0, 0x7f070575

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 217
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 279
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 281
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONHideListFragment;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONHideListFragment;->l:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
