.class Lcom/sec/chaton/poston/bg;
.super Ljava/lang/Object;
.source "PostONWriteFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 913
    iput-object p1, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 916
    const-string v0, "mLocationDelete Click"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 918
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 919
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->q(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 920
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 921
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 924
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v0, v3, :cond_2

    .line 925
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 926
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->j(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 932
    :goto_0
    return-void

    .line 928
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 929
    iget-object v0, p0, Lcom/sec/chaton/poston/bg;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->j(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
