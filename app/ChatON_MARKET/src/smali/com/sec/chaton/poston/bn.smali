.class Lcom/sec/chaton/poston/bn;
.super Landroid/os/Handler;
.source "PostONWriteFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONWriteFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 1497
    iput-object p1, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v7, 0x7f0b0037

    const v6, 0x7f0b0182

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1500
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1637
    :cond_0
    :goto_0
    return-void

    .line 1504
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1506
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1507
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->t(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->t(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1508
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->t(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1510
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1511
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 1513
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_6

    .line 1514
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2, v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 1515
    const-string v2, "HTTP Success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1517
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1519
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1520
    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0b0170

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1528
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONList;

    .line 1529
    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_d

    .line 1530
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/PostONList;->hasmore:Ljava/lang/String;

    .line 1531
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/PostONList;->endtime:Ljava/lang/String;

    .line 1532
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->isblind:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1535
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/bx;

    iget-object v4, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->z(Lcom/sec/chaton/poston/PostONWriteFragment;)Z

    move-result v4

    invoke-interface {v0, v4, v3, v2, v1}, Lcom/sec/chaton/poston/bx;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1539
    :cond_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 1540
    const/4 v2, -0x3

    if-eq v2, v1, :cond_7

    const/4 v2, -0x2

    if-ne v2, v1, :cond_8

    .line 1541
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1542
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    goto/16 :goto_0

    .line 1546
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x15f99

    if-ne v1, v2, :cond_a

    .line 1547
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->A(Lcom/sec/chaton/poston/PostONWriteFragment;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1548
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 1549
    const-string v0, "WRITE_POSTON_DUPLICATED_PREVIOUS_ONE: mResponseErr true"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1570
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/bx;

    invoke-interface {v0}, Lcom/sec/chaton/poston/bx;->c()V

    goto/16 :goto_0

    .line 1573
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 1577
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b018a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/bo;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/bo;-><init>(Lcom/sec/chaton/poston/bn;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1587
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc355

    if-ne v1, v2, :cond_b

    .line 1588
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 1589
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0289

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/bp;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/bp;-><init>(Lcom/sec/chaton/poston/bn;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1598
    :cond_b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x1637b

    if-ne v0, v1, :cond_c

    .line 1599
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 1600
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0408

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1601
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/bx;

    invoke-interface {v0}, Lcom/sec/chaton/poston/bx;->c()V

    goto/16 :goto_0

    .line 1605
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/poston/PostONWriteFragment;->c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z

    .line 1606
    iget-object v0, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    iget-object v1, p0, Lcom/sec/chaton/poston/bn;->a:Lcom/sec/chaton/poston/PostONWriteFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0097

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/poston/br;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/br;-><init>(Lcom/sec/chaton/poston/bn;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/poston/bq;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/bq;-><init>(Lcom/sec/chaton/poston/bn;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    :cond_d
    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_1

    .line 1504
    nop

    :pswitch_data_0
    .packed-switch 0x386
        :pswitch_0
    .end packed-switch
.end method
