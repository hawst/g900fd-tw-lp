.class public Lcom/sec/chaton/poston/l;
.super Ljava/lang/Object;
.source "PostONAdapter.java"


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/RelativeLayout;

.field public h:Landroid/widget/RelativeLayout;

.field public i:Landroid/widget/ImageView;

.field public j:Landroid/widget/FrameLayout;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/FrameLayout;

.field public m:Landroid/widget/ImageView;

.field public n:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    iput-object p1, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->a:Landroid/widget/ImageView;

    .line 397
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->b:Landroid/widget/TextView;

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0702e3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->d:Landroid/widget/TextView;

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0704bb

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->c:Landroid/widget/TextView;

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0704bd

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->f:Landroid/widget/TextView;

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0702e1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0702ea

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->l:Landroid/widget/FrameLayout;

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->i:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07002d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->g:Landroid/widget/RelativeLayout;

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0704bc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->h:Landroid/widget/RelativeLayout;

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0702e2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->j:Landroid/widget/FrameLayout;

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0704ee

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->k:Landroid/widget/TextView;

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->m:Landroid/widget/ImageView;

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->c:Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/poston/l;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07048d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/poston/l;->n:Landroid/widget/ImageView;

    .line 414
    return-void
.end method
