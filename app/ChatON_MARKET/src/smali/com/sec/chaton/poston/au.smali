.class Lcom/sec/chaton/poston/au;
.super Landroid/os/Handler;
.source "PostONDetailFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONDetailFragment;)V
    .locals 0

    .prologue
    .line 621
    iput-object p1, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 626
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 669
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 665
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Z)V

    .line 666
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Lcom/sec/chaton/poston/PostONDetailFragment;Z)V

    goto :goto_0

    .line 632
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 633
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 635
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->a(Lcom/sec/chaton/poston/PostONDetailFragment;Z)V

    .line 636
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Lcom/sec/chaton/poston/PostONDetailFragment;Z)V

    .line 637
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 638
    const-string v0, "FileDownloadTask.FILE_DOWNLOAD_DONE"

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :cond_3
    new-instance v0, Lcom/sec/chaton/poston/bb;

    iget-object v1, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->p(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->b(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v3}, Lcom/sec/chaton/poston/PostONDetailFragment;->s(Lcom/sec/chaton/poston/PostONDetailFragment;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/d;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Lcom/sec/chaton/poston/bb;->c:Ljava/lang/String;

    sget-object v5, Lcom/sec/chaton/poston/bb;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/bb;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 644
    if-eqz v0, :cond_4

    .line 645
    iget-object v1, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->u(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/poston/PostONDetailFragment;->t(Lcom/sec/chaton/poston/PostONDetailFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 647
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 652
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/poston/PostONDetailFragment;->r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    goto/16 :goto_0

    .line 660
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 661
    iget-object v1, p0, Lcom/sec/chaton/poston/au;->a:Lcom/sec/chaton/poston/PostONDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/poston/PostONDetailFragment;->r(Lcom/sec/chaton/poston/PostONDetailFragment;)Lcom/sec/chaton/widget/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/c;->a(I)V

    goto/16 :goto_0

    .line 630
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
