.class Lcom/sec/chaton/poston/ba;
.super Landroid/os/Handler;
.source "PostONHideListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/poston/PostONHideListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/poston/PostONHideListFragment;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/sec/chaton/poston/ba;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 292
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 293
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 322
    :goto_0
    return-void

    .line 295
    :pswitch_0
    sget-object v1, Lcom/sec/chaton/poston/PostONHideListFragment;->a:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 296
    sget-object v1, Lcom/sec/chaton/poston/PostONHideListFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 298
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_2

    .line 300
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONBlindList;

    .line 301
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONBlindList;->blind:Ljava/util/ArrayList;

    .line 302
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v4, v1, [Ljava/lang/String;

    .line 304
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;

    .line 305
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/sec/chaton/e/a/d;->h(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 306
    add-int/lit8 v3, v1, 0x1

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    aput-object v0, v4, v1

    move v0, v3

    :goto_2
    move v1, v0

    goto :goto_1

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/ba;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b041a

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 312
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/poston/ba;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/PostONHideFragment;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 313
    const-string v1, "block_buddy_result"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 314
    iget-object v1, p0, Lcom/sec/chaton/poston/ba;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 315
    iget-object v0, p0, Lcom/sec/chaton/poston/ba;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 317
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/ba;->a:Lcom/sec/chaton/poston/PostONHideListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/PostONHideListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00b3

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    .line 293
    nop

    :pswitch_data_0
    .packed-switch 0x388
        :pswitch_0
    .end packed-switch
.end method
