.class public Lcom/sec/chaton/poston/PostONWriteFragment;
.super Landroid/support/v4/app/Fragment;
.source "PostONWriteFragment.java"

# interfaces
.implements Lcom/sec/chaton/poston/bc;
.implements Lcom/sec/chaton/poston/u;


# static fields
.field private static X:Ljava/lang/String;

.field private static Y:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Lcom/sec/chaton/widget/c;

.field private D:Landroid/app/ProgressDialog;

.field private E:Lcom/sec/common/a/d;

.field private F:Lcom/sec/chaton/d/v;

.field private G:Landroid/widget/Toast;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Landroid/view/MenuItem;

.field private K:Z

.field private L:Landroid/net/Uri;

.field private M:Landroid/net/Uri;

.field private N:I

.field private O:I

.field private P:Z

.field private Q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private S:Lcom/sec/chaton/poston/r;

.field private T:Lcom/sec/common/f/c;

.field private U:Landroid/widget/GridView;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private Z:Z

.field a:I

.field private aa:Z

.field private ab:Lcom/sec/chaton/poston/by;

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Landroid/text/TextWatcher;

.field private ag:Landroid/view/View$OnClickListener;

.field private ah:Landroid/content/DialogInterface$OnClickListener;

.field private ai:Landroid/os/Handler;

.field private aj:Landroid/os/Handler;

.field private ak:Landroid/os/Handler;

.field b:I

.field c:I

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private q:Landroid/widget/ImageButton;

.field private r:Landroid/widget/ImageButton;

.field private s:Landroid/widget/ImageButton;

.field private t:Landroid/widget/ImageButton;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/EditText;

.field private w:Landroid/widget/LinearLayout;

.field private x:Landroid/widget/LinearLayout;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/poston/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/poston/PostONWriteFragment;->X:Ljava/lang/String;

    .line 168
    const-string v0, ".jpg"

    sput-object v0, Lcom/sec/chaton/poston/PostONWriteFragment;->Y:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 87
    const-string v0, "PostONWriteFragment"

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->d:Ljava/lang/String;

    .line 92
    iput v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->e:I

    .line 93
    iput v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->f:I

    .line 96
    iput v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->g:I

    .line 97
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->h:I

    .line 98
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->i:I

    .line 99
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->j:I

    .line 100
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->k:I

    .line 101
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->l:I

    .line 102
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->m:I

    .line 105
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->n:I

    .line 106
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->o:I

    .line 107
    const/16 v0, 0x99

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->p:I

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->R:Ljava/util/ArrayList;

    .line 169
    iput v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->a:I

    .line 170
    iput v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->b:I

    .line 172
    iput-boolean v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    .line 173
    iput-boolean v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->aa:Z

    .line 682
    new-instance v0, Lcom/sec/chaton/poston/bt;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/bt;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->af:Landroid/text/TextWatcher;

    .line 937
    new-instance v0, Lcom/sec/chaton/poston/bh;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/bh;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ag:Landroid/view/View$OnClickListener;

    .line 960
    new-instance v0, Lcom/sec/chaton/poston/bi;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/bi;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ah:Landroid/content/DialogInterface$OnClickListener;

    .line 1388
    new-instance v0, Lcom/sec/chaton/poston/bl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/bl;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ai:Landroid/os/Handler;

    .line 1408
    new-instance v0, Lcom/sec/chaton/poston/bm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/bm;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->aj:Landroid/os/Handler;

    .line 1497
    new-instance v0, Lcom/sec/chaton/poston/bn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/poston/bn;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ak:Landroid/os/Handler;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/poston/PostONWriteFragment;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->aa:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/poston/bz;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 532
    new-instance v2, Lcom/sec/chaton/poston/bz;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/bz;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    .line 533
    const-string v0, "3"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 535
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 536
    if-eqz v3, :cond_1

    move v0, v1

    .line 537
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 538
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " strArray["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    :cond_0
    array-length v0, v3

    const/4 v4, 0x3

    if-ne v0, v4, :cond_1

    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/poston/bz;->a:Ljava/lang/String;

    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/poston/bz;->b:Ljava/lang/String;

    .line 543
    iput-object p2, v2, Lcom/sec/chaton/poston/bz;->c:Ljava/lang/String;

    .line 552
    :cond_1
    :goto_1
    return-object v2

    .line 547
    :cond_2
    iput-object p1, v2, Lcom/sec/chaton/poston/bz;->a:Ljava/lang/String;

    .line 548
    iput-object p1, v2, Lcom/sec/chaton/poston/bz;->b:Ljava/lang/String;

    .line 549
    iput-object p2, v2, Lcom/sec/chaton/poston/bz;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/chaton/widget/c;)Lcom/sec/chaton/widget/c;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->E:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1203
    if-eqz p1, :cond_2

    const-string v0, "GEOPOINT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1204
    iput-boolean v4, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ae:Z

    .line 1205
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GEOPOINT: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GEOPOINT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/ GEOADDRESS: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GEOADDRESS"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    const-string v0, "GEOADDRESS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1208
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1209
    const v0, 0x7f0b00a1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1211
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GEOPOINT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    .line 1212
    const-string v1, "GEOPOINT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->B:Ljava/lang/String;

    .line 1214
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->y:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1216
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1218
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v5, :cond_1

    .line 1219
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1222
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    invoke-virtual {v0, v5}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1223
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1226
    :cond_2
    return-void
.end method

.method private a(Landroid/widget/EditText;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 432
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 433
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 434
    if-eqz p2, :cond_1

    .line 435
    new-instance v1, Lcom/sec/chaton/poston/PostONWriteFragment$2;

    invoke-direct {v1, p0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment$2;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, p1, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/poston/PostONWriteFragment$3;

    invoke-direct {v2, p0, v4}, Lcom/sec/chaton/poston/PostONWriteFragment$3;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/multimedia/multisend/PreviewData;)V
    .locals 5

    .prologue
    .line 613
    .line 616
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 618
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 619
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 620
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    .line 627
    :goto_0
    invoke-static {v1}, Lcom/sec/chaton/util/bc;->a(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    if-nez v3, :cond_2

    .line 629
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/poston/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->I:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/chaton/util/bc;->b(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Landroid/net/Uri;Z)Ljava/io/File;

    move-result-object v0

    .line 630
    if-eqz v0, :cond_0

    .line 631
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a(Ljava/lang/String;)V

    .line 632
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->c(Ljava/lang/String;)V

    .line 633
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "yeseul / getAbsolutePath : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ getName()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 659
    :cond_0
    :goto_1
    return-void

    .line 622
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 623
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 635
    :catch_0
    move-exception v0

    .line 636
    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 637
    :catch_1
    move-exception v0

    .line 638
    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 639
    :catch_2
    move-exception v0

    .line 640
    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 641
    :catch_3
    move-exception v0

    .line 642
    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 646
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v4, "."

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 647
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 648
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "yeseul / PostONFilePath :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->W:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " /mImageId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->W:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 652
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->W:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a(Ljava/lang/String;)V

    .line 653
    invoke-virtual {p1, v1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    goto/16 :goto_1

    .line 655
    :catch_4
    move-exception v0

    .line 656
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;Landroid/widget/EditText;Z)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Landroid/widget/EditText;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;Lcom/sec/chaton/multimedia/multisend/PreviewData;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Lcom/sec/chaton/multimedia/multisend/PreviewData;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1695
    const-string v0, "[SendingMedia] Start - Picture"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1696
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1697
    if-eqz p2, :cond_0

    .line 1698
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1699
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1704
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1705
    const-string v0, "return-data"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1706
    const-string v0, "randomFName"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1708
    const/4 v0, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1709
    return-void

    .line 1701
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 460
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostONCommentItem: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 466
    if-eqz p1, :cond_5

    .line 467
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    if-eqz v0, :cond_3

    move v1, v2

    .line 468
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 469
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 470
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "writePostONImage / url = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    const-string v3, "2"

    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/poston/bz;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 475
    :goto_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 476
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "writePostONVideo / url = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->f()Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/poston/bz;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 483
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "writePostON / LocationData = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    const-string v1, "3"

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/poston/bz;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    invoke-direct {p0, v0, v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Landroid/widget/EditText;Z)V

    .line 490
    new-instance v0, Lcom/sec/chaton/poston/k;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->I:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->I:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->H:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    const-string v6, "0"

    const-string v7, "0"

    const-string v8, "-1"

    move-object v11, v10

    move-object v12, v10

    invoke-direct/range {v0 .. v12}, Lcom/sec/chaton/poston/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->F:Lcom/sec/chaton/d/v;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->I:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/d/v;->a(Lcom/sec/chaton/poston/k;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto/16 :goto_0
.end method

.method private a(ILandroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1145
    .line 1147
    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 1162
    :goto_0
    return v0

    .line 1149
    :pswitch_0
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1150
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->e()V

    goto :goto_0

    .line 1155
    :pswitch_1
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1156
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->f()V

    goto :goto_0

    .line 1147
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ac:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->B:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    return p1
.end method

.method private c()Lcom/sec/chaton/widget/c;
    .locals 4

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    if-nez v0, :cond_0

    .line 664
    new-instance v0, Lcom/sec/chaton/widget/c;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/widget/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    .line 665
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b018b

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setTitle(I)V

    .line 666
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b0230

    invoke-virtual {p0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setMessage(Ljava/lang/CharSequence;)V

    .line 667
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setCancelable(Z)V

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    const/4 v1, -0x2

    const v2, 0x7f0b0039

    invoke-virtual {p0, v2}, Lcom/sec/chaton/poston/PostONWriteFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/poston/bs;

    invoke-direct {v3, p0}, Lcom/sec/chaton/poston/bs;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/widget/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 679
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->V:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/poston/PostONWriteFragment;Z)Z
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->aa:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->c()Lcom/sec/chaton/widget/c;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 732
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/poston/bu;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/bu;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 754
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/poston/bv;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/bv;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 777
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/poston/bw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/bw;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 826
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->s:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/poston/be;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/be;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 869
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->t:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/poston/bf;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/bf;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 913
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->z:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/poston/bg;

    invoke-direct {v1, p0}, Lcom/sec/chaton/poston/bg;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 935
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/widget/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1166
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1167
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1170
    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1177
    :cond_0
    :goto_0
    return-void

    .line 1171
    :catch_0
    move-exception v0

    .line 1172
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1173
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 1174
    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->aj:Landroid/os/Handler;

    return-object v0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMdd_HHmmss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/poston/PostONWriteFragment;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1181
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "//Camera//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1182
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1183
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 1186
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "//Camera//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    .line 1187
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1188
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1189
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1190
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1192
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1200
    :cond_1
    :goto_0
    return-void

    .line 1193
    :catch_0
    move-exception v0

    .line 1194
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1195
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 1196
    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ai:Landroid/os/Handler;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1336
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 1338
    :cond_0
    iget v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->c:I

    if-nez v0, :cond_1

    .line 1339
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/util/ArrayList;)V

    .line 1340
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->b:I

    .line 1386
    :goto_0
    return-void

    .line 1343
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0097

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/poston/bk;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/bk;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/poston/bj;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/bj;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->E:Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->R:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/poston/PostONWriteFragment;)Lcom/sec/chaton/poston/by;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ab:Lcom/sec/chaton/poston/by;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->u:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->f()V

    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->e()V

    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->t:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->y:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->D:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/chaton/poston/PostONWriteFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->G:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/chaton/poston/PostONWriteFragment;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    return v0
.end method

.method static synthetic w(Lcom/sec/chaton/poston/PostONWriteFragment;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ac:Z

    return v0
.end method

.method static synthetic x(Lcom/sec/chaton/poston/PostONWriteFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->V:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/chaton/poston/PostONWriteFragment;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->g()V

    return-void
.end method

.method static synthetic z(Lcom/sec/chaton/poston/PostONWriteFragment;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->K:Z

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 1680
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    if-eqz v0, :cond_0

    .line 1691
    :goto_0
    return-void

    .line 1683
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    .line 1684
    iput p1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->O:I

    .line 1685
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v1

    .line 1686
    if-nez v1, :cond_1

    .line 1687
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1689
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 1713
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1714
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1715
    const/4 v0, 0x0

    .line 1717
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method b()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1272
    iput-boolean v5, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ae:Z

    .line 1273
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v7, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1274
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1277
    :cond_0
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->T:Lcom/sec/common/f/c;

    .line 1278
    new-instance v0, Lcom/sec/chaton/poston/r;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f03004d

    iget-object v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->T:Lcom/sec/common/f/c;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/poston/r;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->S:Lcom/sec/chaton/poston/r;

    .line 1279
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->S:Lcom/sec/chaton/poston/r;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/poston/r;->a(Lcom/sec/chaton/poston/u;)V

    .line 1280
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->S:Lcom/sec/chaton/poston/r;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1281
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setDescendantFocusability(I)V

    .line 1283
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v0, v5, :cond_4

    .line 1284
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1285
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1287
    iput-boolean v5, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    .line 1288
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 1289
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1292
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    if-eqz v0, :cond_3

    .line 1293
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->S:Lcom/sec/chaton/poston/r;

    invoke-virtual {v0, v5}, Lcom/sec/chaton/poston/r;->a(Z)V

    .line 1294
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1295
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1298
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1332
    :cond_2
    :goto_0
    return-void

    .line 1301
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1304
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->N:I

    .line 1305
    iget v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->N:I

    if-lt v0, v5, :cond_2

    .line 1308
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1309
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1310
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 1316
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1317
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1320
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1323
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1324
    :cond_5
    iput-boolean v5, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    .line 1325
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 1327
    :cond_6
    iput-boolean v6, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    .line 1328
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1643
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1645
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1648
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 1650
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->S:Lcom/sec/chaton/poston/r;

    if-eqz v0, :cond_1

    .line 1651
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->S:Lcom/sec/chaton/poston/r;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/r;->notifyDataSetChanged()V

    .line 1655
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1656
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1658
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1659
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1661
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 1662
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1663
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1665
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1666
    :cond_2
    iput-boolean v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    .line 1667
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1675
    :cond_3
    :goto_0
    return-void

    .line 1669
    :cond_4
    iput-boolean v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    .line 1670
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 295
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 313
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1000
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1002
    const/4 v0, -0x1

    if-ne p2, v0, :cond_b

    .line 1003
    packed-switch p1, :pswitch_data_0

    .line 1141
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1005
    :pswitch_1
    iput-boolean v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->P:Z

    .line 1006
    iput-boolean v7, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    .line 1007
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1008
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1009
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1010
    const-string v1, "randomFName"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1011
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1016
    :pswitch_2
    iput-boolean v7, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    .line 1017
    if-nez p3, :cond_1

    .line 1018
    const-string v0, "To pick image From Gallery is NULL"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1027
    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->L:Landroid/net/Uri;

    .line 1028
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1029
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->L:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1030
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1031
    const-string v1, "randomFName"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1032
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1038
    :pswitch_3
    iput-boolean v7, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    .line 1040
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 1041
    const-string v0, "preview_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    .line 1049
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b()V

    goto :goto_0

    .line 1043
    :cond_3
    const-string v0, "preview_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1044
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    .line 1045
    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1057
    :pswitch_4
    iput-boolean v7, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    .line 1059
    if-nez p3, :cond_4

    .line 1060
    const-string v0, "To pick image From Gallery is NULL"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1063
    :cond_4
    iget v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->N:I

    if-eqz v0, :cond_5

    .line 1064
    iget v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->N:I

    .line 1070
    :goto_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "temp_file_path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1071
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->P:Z

    if-eqz v0, :cond_6

    .line 1072
    iget-object v6, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1073
    iput-boolean v7, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->P:Z

    .line 1078
    :goto_3
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b()V

    goto/16 :goto_0

    .line 1066
    :cond_5
    iput v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->N:I

    goto :goto_2

    .line 1075
    :cond_6
    iget-object v6, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->L:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1085
    :pswitch_5
    iput-boolean v7, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    .line 1086
    const-string v0, "Get the item modified by effect function"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    if-eqz p3, :cond_0

    .line 1089
    const-string v0, "temp_file_path"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1090
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->O:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a(Ljava/lang/String;)V

    .line 1091
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    goto/16 :goto_0

    .line 1096
    :pswitch_6
    iput-boolean v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    .line 1097
    if-nez p3, :cond_7

    .line 1098
    const-string v0, "To pick image From Gallery is NULL"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1108
    :cond_7
    const-string v0, "file"

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1109
    new-instance v0, Ljava/io/File;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1117
    :goto_4
    if-nez v0, :cond_9

    .line 1118
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 1119
    const-string v0, "sendVideo file is null"

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1111
    :cond_8
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/r;->c(Ljava/lang/String;)Lcom/sec/chaton/util/u;

    move-result-object v0

    .line 1112
    if-eqz v0, :cond_c

    .line 1113
    iget-object v0, v0, Lcom/sec/chaton/util/u;->a:Ljava/io/File;

    goto :goto_4

    .line 1124
    :cond_9
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/32 v5, 0xa00000

    cmp-long v1, v3, v5

    if-lez v1, :cond_a

    .line 1125
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00b0

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1129
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/multimedia/multisend/PreviewData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fi.getAbsolutePath(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " /fi.getName(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PostONWriteFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->b()V

    goto/16 :goto_0

    .line 1135
    :pswitch_7
    invoke-direct {p0, p3}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1138
    :cond_b
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    :cond_c
    move-object v0, v2

    goto/16 :goto_4

    .line 1003
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 191
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 193
    const-string v0, "MY_PAGE"

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "WRITE_FROM"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    iput-boolean v5, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->K:Z

    .line 198
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CHATON_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 199
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CHATON_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->I:Ljava/lang/String;

    .line 203
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Push Name"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->H:Ljava/lang/String;

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/chaton/poston/PostONWriteFragment;->X:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->W:Ljava/lang/String;

    .line 205
    iput-object v4, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->L:Landroid/net/Uri;

    .line 207
    if-nez p1, :cond_3

    .line 208
    iput-object v4, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    .line 218
    :cond_0
    :goto_2
    iput v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->O:I

    .line 219
    iput v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->N:I

    .line 220
    iput-object v4, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->E:Lcom/sec/common/a/d;

    .line 221
    iput-boolean v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->P:Z

    .line 222
    iput-boolean v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    .line 224
    new-instance v0, Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ak:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/v;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->F:Lcom/sec/chaton/d/v;

    .line 225
    invoke-static {p0, v5}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 227
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->G:Landroid/widget/Toast;

    .line 231
    return-void

    .line 196
    :cond_1
    iput-boolean v3, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->K:Z

    goto :goto_0

    .line 201
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->I:Ljava/lang/String;

    goto :goto_1

    .line 211
    :cond_3
    const-string v0, "CAPTURE_IMAGE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 214
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 326
    const v0, 0x7f0f000b

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 327
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 328
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 235
    const v0, 0x7f0300df

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 238
    const v0, 0x7f0703e6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->af:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/16 v5, 0x8c

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v3, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/chaton/poston/bd;

    invoke-direct {v2, p0}, Lcom/sec/chaton/poston/bd;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 259
    const v0, 0x7f0703f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    const v2, 0x7f0703f1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->y:Landroid/widget/TextView;

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    const v2, 0x7f0703f2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->z:Landroid/widget/ImageButton;

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->w:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ag:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    const v0, 0x7f0703e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    .line 266
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 270
    :cond_0
    const v0, 0x7f0703ea

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->r:Landroid/widget/ImageButton;

    .line 271
    const v0, 0x7f0703eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->s:Landroid/widget/ImageButton;

    .line 272
    const v0, 0x7f0703ec

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->t:Landroid/widget/ImageButton;

    .line 273
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 278
    :goto_0
    const v0, 0x7f0703ed

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->u:Landroid/widget/TextView;

    .line 281
    const v0, 0x7f0703ee

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    .line 282
    const v0, 0x7f0703ef

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    .line 285
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b0041

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->D:Landroid/app/ProgressDialog;

    .line 287
    invoke-direct {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->d()V

    .line 289
    return-object v1

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->t:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 399
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ab:Lcom/sec/chaton/poston/by;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ab:Lcom/sec/chaton/poston/by;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/by;->a()V

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->E:Lcom/sec/common/a/d;

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->E:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->D:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->D:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 413
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 417
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 419
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->S:Lcom/sec/chaton/poston/r;

    .line 422
    :cond_4
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 380
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 382
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->af:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->U:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->T:Lcom/sec/common/f/c;

    if-eqz v0, :cond_2

    .line 392
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->T:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 394
    :cond_2
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 344
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070583

    if-ne v1, v2, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 347
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070584

    if-ne v1, v2, :cond_6

    .line 348
    iget-boolean v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ac:Z

    if-eqz v1, :cond_1

    .line 349
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0b022a

    invoke-static {v1, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 374
    :goto_0
    return v0

    .line 352
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 353
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 354
    const/4 v2, -0x3

    if-eq v2, v1, :cond_2

    const/4 v2, -0x2

    if-ne v2, v1, :cond_3

    .line 355
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->G:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 358
    :cond_3
    iget-boolean v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ad:Z

    if-eqz v1, :cond_4

    .line 359
    new-instance v1, Lcom/sec/chaton/poston/by;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    sget-object v3, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/chaton/poston/by;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/util/ArrayList;Lcom/sec/chaton/e/w;)V

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ab:Lcom/sec/chaton/poston/by;

    .line 360
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ab:Lcom/sec/chaton/poston/by;

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/poston/by;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 362
    :cond_4
    new-instance v1, Lcom/sec/chaton/poston/by;

    iget-object v2, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Q:Ljava/util/ArrayList;

    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/chaton/poston/by;-><init>(Lcom/sec/chaton/poston/PostONWriteFragment;Ljava/util/ArrayList;Lcom/sec/chaton/e/w;)V

    iput-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ab:Lcom/sec/chaton/poston/by;

    .line 363
    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ab:Lcom/sec/chaton/poston/by;

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/poston/by;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 366
    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/chaton/poston/PostONWriteFragment;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 370
    :cond_6
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_7

    .line 371
    invoke-virtual {p0}, Lcom/sec/chaton/poston/PostONWriteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 374
    :cond_7
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 332
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 333
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    .line 334
    const v0, 0x7f070584

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    .line 335
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->Z:Z

    if-ne v0, v1, :cond_0

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 340
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->J:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1724
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1726
    iget-boolean v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ae:Z

    if-nez v0, :cond_0

    .line 1727
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1732
    :goto_0
    return-void

    .line 1729
    :cond_0
    iput-boolean v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->ae:Z

    .line 1730
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 317
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 320
    const-string v0, "CAPTURE_IMAGE_URI"

    iget-object v1, p0, Lcom/sec/chaton/poston/PostONWriteFragment;->M:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_0
    return-void
.end method
