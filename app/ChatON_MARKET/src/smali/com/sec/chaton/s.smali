.class Lcom/sec/chaton/s;
.super Landroid/os/AsyncTask;
.source "ExitAppDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic b:Lcom/sec/chaton/ExitAppDialogActivity;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/ExitAppDialogActivity;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/ExitAppDialogActivity;Lcom/sec/chaton/r;)V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0, p1}, Lcom/sec/chaton/s;-><init>(Lcom/sec/chaton/ExitAppDialogActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/d/a/ak;->a(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/s;->a:I

    .line 207
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 227
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->c(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->c(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->c(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 232
    :cond_0
    iget v0, p0, Lcom/sec/chaton/s;->a:I

    if-nez v0, :cond_2

    .line 234
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "exit_app_done"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "exit_app_done"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 246
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/global/GlobalApplication;->a(Landroid/content/Context;)V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->finish()V

    .line 248
    return-void

    .line 240
    :cond_2
    iget v0, p0, Lcom/sec/chaton/s;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 241
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 242
    :cond_3
    iget v0, p0, Lcom/sec/chaton/s;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 243
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002d

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 200
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/s;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->c(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->c(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->c(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 223
    :cond_0
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 200
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/s;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 212
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    iget-object v1, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v1}, Lcom/sec/chaton/ExitAppDialogActivity;->b(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/s;->b:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-virtual {v3}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0041

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/ExitAppDialogActivity;->a(Lcom/sec/chaton/ExitAppDialogActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 214
    return-void
.end method
