.class Lcom/sec/chaton/block/f;
.super Ljava/lang/Object;
.source "BlockFragment.java"

# interfaces
.implements Lcom/sec/chaton/block/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/BlockFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/BlockFragment;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/j/o;)V
    .locals 6

    .prologue
    const v5, 0x7f0b02cb

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 185
    sget-object v0, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne p1, v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/block/BlockFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v2}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    iget-object v1, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->d(Lcom/sec/chaton/block/BlockFragment;)Lcom/sec/chaton/block/a;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/block/a;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/block/BlockFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v2}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->e(Lcom/sec/chaton/block/BlockFragment;)V

    .line 215
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;)I

    move-result v0

    .line 217
    iget-object v1, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v1

    .line 218
    sub-int v0, v1, v0

    if-gtz v0, :cond_5

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 223
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 226
    :cond_3
    return-void

    .line 209
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 221
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/block/f;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method
