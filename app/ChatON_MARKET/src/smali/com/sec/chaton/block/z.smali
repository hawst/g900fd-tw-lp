.class Lcom/sec/chaton/block/z;
.super Landroid/os/Handler;
.source "BlockListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/BlockListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/BlockListFragment;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/chaton/block/z;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 381
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 382
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 384
    :pswitch_0
    sget-object v1, Lcom/sec/chaton/block/BlockListFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 385
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 386
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/UnBlock;

    move v2, v3

    .line 387
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/block/z;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->d(Lcom/sec/chaton/block/BlockListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 388
    iget-object v4, v0, Lcom/sec/chaton/io/entry/UnBlock;->buddyid:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/block/z;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->d(Lcom/sec/chaton/block/BlockListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/block/aa;

    iget-object v1, v1, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 392
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0294

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 394
    iget-object v1, p0, Lcom/sec/chaton/block/z;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->d(Lcom/sec/chaton/block/BlockListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 387
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 399
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b3

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/block/z;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/block/BlockListFragment;->a()V

    goto :goto_0

    .line 382
    :pswitch_data_0
    .packed-switch 0x25b
        :pswitch_0
    .end packed-switch
.end method
