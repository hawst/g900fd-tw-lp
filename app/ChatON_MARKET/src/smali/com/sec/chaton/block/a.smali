.class Lcom/sec/chaton/block/a;
.super Landroid/widget/ArrayAdapter;
.source "BlockBuddyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/block/aa;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/os/Handler;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/chaton/d/f;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/view/LayoutInflater;

.field private h:Landroid/app/ProgressDialog;

.field private i:Landroid/view/ViewGroup;

.field private j:Lcom/sec/chaton/block/d;

.field private k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 70
    new-instance v0, Lcom/sec/chaton/block/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/b;-><init>(Lcom/sec/chaton/block/a;)V

    iput-object v0, p0, Lcom/sec/chaton/block/a;->k:Landroid/view/View$OnClickListener;

    .line 129
    new-instance v0, Lcom/sec/chaton/block/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/c;-><init>(Lcom/sec/chaton/block/a;)V

    iput-object v0, p0, Lcom/sec/chaton/block/a;->b:Landroid/os/Handler;

    .line 60
    iput-object p1, p0, Lcom/sec/chaton/block/a;->c:Landroid/content/Context;

    .line 61
    iput-object p3, p0, Lcom/sec/chaton/block/a;->a:Ljava/util/ArrayList;

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/block/a;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/block/a;->g:Landroid/view/LayoutInflater;

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/block/a;->g:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/block/a;->i:Landroid/view/ViewGroup;

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/block/a;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/block/a;->h:Landroid/app/ProgressDialog;

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/block/a;Lcom/sec/chaton/d/f;)Lcom/sec/chaton/d/f;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/chaton/block/a;->d:Lcom/sec/chaton/d/f;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/block/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/block/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/block/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/chaton/block/a;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/block/a;)Lcom/sec/chaton/d/f;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/block/a;->d:Lcom/sec/chaton/d/f;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/block/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/chaton/block/a;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/block/a;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/block/a;->h:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/block/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/block/a;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/block/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/block/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/block/a;)Lcom/sec/chaton/block/d;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/block/a;->j:Lcom/sec/chaton/block/d;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/block/d;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/chaton/block/a;->j:Lcom/sec/chaton/block/d;

    .line 55
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 88
    if-nez p2, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/block/a;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f030124

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 90
    new-instance v1, Lcom/sec/chaton/block/e;

    invoke-direct {v1}, Lcom/sec/chaton/block/e;-><init>()V

    .line 91
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/block/e;->a:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f07014b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/block/e;->b:Landroid/widget/ImageView;

    .line 95
    const v0, 0x7f0702d7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    .line 96
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 101
    :goto_0
    iget-object v0, v1, Lcom/sec/chaton/block/e;->b:Landroid/widget/ImageView;

    const v2, 0x7f02028d

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 102
    iget-object v0, v1, Lcom/sec/chaton/block/e;->a:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 103
    iget-object v0, v1, Lcom/sec/chaton/block/e;->a:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 105
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/block/a;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 107
    iget-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    const/high16 v3, 0x42c80000    # 100.0f

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 108
    iget-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 109
    iget-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setMaxLines(I)V

    .line 110
    iget-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 111
    iget-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    const v2, 0x7f0b007b

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 112
    iget-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 114
    iget-object v0, v1, Lcom/sec/chaton/block/e;->c:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/block/a;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    invoke-virtual {p0, p1}, Lcom/sec/chaton/block/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    .line 117
    iget-object v2, v1, Lcom/sec/chaton/block/e;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/block/aa;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v2, p0, Lcom/sec/chaton/block/a;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/block/e;->b:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 119
    return-object p2

    .line 98
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/e;

    move-object v1, v0

    goto/16 :goto_0
.end method
