.class Lcom/sec/chaton/block/i;
.super Landroid/os/Handler;
.source "BlockFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/BlockFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/BlockFragment;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 308
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 310
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 358
    :sswitch_0
    sget-object v1, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 360
    :try_start_0
    sget-object v1, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :cond_2
    :goto_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_3

    .line 367
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 368
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->d(Lcom/sec/chaton/block/BlockFragment;)Lcom/sec/chaton/block/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    .line 369
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 370
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 371
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 374
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_c

    .line 375
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_b

    .line 376
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 378
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBlockBuddyList;

    .line 379
    iget-object v4, v0, Lcom/sec/chaton/io/entry/GetBlockBuddyList;->buddy:Ljava/util/ArrayList;

    .line 381
    if-eqz v4, :cond_0

    .line 382
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move v1, v2

    .line 389
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NO : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", Name : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->name:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", ORGNUM : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->orgnum:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->h(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    :cond_4
    new-instance v5, Lcom/sec/chaton/block/aa;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->name:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->orgnum:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v0}, Lcom/sec/chaton/block/aa;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 347
    :sswitch_1
    sget-object v1, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 348
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/block/BlockFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_5

    .line 349
    sget-object v1, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 352
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->g(Lcom/sec/chaton/block/BlockFragment;)Lcom/sec/chaton/d/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/f;->a()V

    goto/16 :goto_0

    .line 362
    :catch_0
    move-exception v1

    .line 363
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 399
    :cond_6
    new-instance v0, Lcom/sec/chaton/block/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/j;-><init>(Lcom/sec/chaton/block/i;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->d(Lcom/sec/chaton/block/BlockFragment;)Lcom/sec/chaton/block/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    .line 436
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 440
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/block/BlockFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v3}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 444
    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 449
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->e(Lcom/sec/chaton/block/BlockFragment;)V

    .line 458
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/block/BlockFragment;->a()V

    .line 460
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;)I

    move-result v0

    .line 462
    iget-object v1, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v1

    .line 463
    sub-int v0, v1, v0

    if-gtz v0, :cond_e

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 468
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_0

    .line 422
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->d(Lcom/sec/chaton/block/BlockFragment;)Lcom/sec/chaton/block/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_3

    .line 430
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_3

    .line 451
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_5

    .line 466
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/block/i;->a:Lcom/sec/chaton/block/BlockFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockFragment;->f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_6

    .line 441
    :catch_1
    move-exception v0

    goto/16 :goto_4

    .line 313
    nop

    :sswitch_data_0
    .sparse-switch
        0x259 -> :sswitch_0
        0x25e -> :sswitch_1
    .end sparse-switch
.end method
