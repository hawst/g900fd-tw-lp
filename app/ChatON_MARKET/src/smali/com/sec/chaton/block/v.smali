.class Lcom/sec/chaton/block/v;
.super Ljava/lang/Object;
.source "BlockListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/BlockListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/BlockListFragment;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 280
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 286
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->e(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/block/s;->onClick()V

    .line 287
    return-void

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->h(Lcom/sec/chaton/block/BlockListFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 283
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/block/v;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
