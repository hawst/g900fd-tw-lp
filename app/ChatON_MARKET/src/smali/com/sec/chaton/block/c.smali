.class Lcom/sec/chaton/block/c;
.super Landroid/os/Handler;
.source "BlockBuddyAdapter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/a;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/a;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 132
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 133
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 135
    :sswitch_0
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->c(Lcom/sec/chaton/block/a;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->c(Lcom/sec/chaton/block/a;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->d(Lcom/sec/chaton/block/a;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 137
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->c(Lcom/sec/chaton/block/a;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 140
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 141
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/UnBlock;

    move v3, v4

    .line 142
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    iget-object v2, v2, Lcom/sec/chaton/block/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 143
    iget-object v5, v1, Lcom/sec/chaton/io/entry/UnBlock;->buddyid:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    iget-object v2, v2, Lcom/sec/chaton/block/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/block/aa;

    iget-object v2, v2, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 151
    iget-object v2, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v2}, Lcom/sec/chaton/block/a;->d(Lcom/sec/chaton/block/a;)Landroid/content/Context;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v5}, Lcom/sec/chaton/block/a;->d(Lcom/sec/chaton/block/a;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0142

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v8}, Lcom/sec/chaton/block/a;->e(Lcom/sec/chaton/block/a;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 155
    iget-object v2, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    iget-object v2, v2, Lcom/sec/chaton/block/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 142
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 159
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-virtual {v1}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    .line 160
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->f(Lcom/sec/chaton/block/a;)Lcom/sec/chaton/block/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/sec/chaton/block/d;->a(Lcom/sec/chaton/j/o;)V

    goto/16 :goto_0

    .line 164
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v0}, Lcom/sec/chaton/block/a;->d(Lcom/sec/chaton/block/a;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b3

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 167
    :sswitch_1
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->c(Lcom/sec/chaton/block/a;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->c(Lcom/sec/chaton/block/a;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 168
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->d(Lcom/sec/chaton/block/a;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_5

    .line 169
    iget-object v1, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-static {v1}, Lcom/sec/chaton/block/a;->c(Lcom/sec/chaton/block/a;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 172
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/block/c;->a:Lcom/sec/chaton/block/a;

    invoke-virtual {v0}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 133
    nop

    :sswitch_data_0
    .sparse-switch
        0x13d -> :sswitch_1
        0x25b -> :sswitch_0
    .end sparse-switch
.end method
