.class Lcom/sec/chaton/block/y;
.super Ljava/lang/Object;
.source "BlockListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/BlockListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/BlockListFragment;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    .line 338
    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/block/q;->a(Z)V

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->g(Lcom/sec/chaton/block/BlockListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move v0, v1

    .line 342
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v2}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 343
    iget-object v2, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v2}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 347
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/block/q;->a(Z)V

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->g(Lcom/sec/chaton/block/BlockListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 349
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 349
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 353
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/q;->notifyDataSetChanged()V

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 355
    iget-object v0, p0, Lcom/sec/chaton/block/y;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/q;->b()V

    .line 356
    return-void
.end method
