.class Lcom/sec/chaton/block/m;
.super Ljava/lang/Object;
.source "BlockImpl.java"

# interfaces
.implements Lcom/sec/chaton/block/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/k;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/k;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/j/o;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const v4, 0x7f0b02cb

    const/4 v3, 0x0

    .line 166
    sget-object v0, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne p1, v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 172
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 181
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 182
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_4

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 193
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    iget-object v1, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->e(Lcom/sec/chaton/block/k;)Lcom/sec/chaton/block/a;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/block/a;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 198
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_5

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 207
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 209
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_6

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 217
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->f(Lcom/sec/chaton/block/k;)V

    .line 230
    :cond_2
    :goto_4
    return-void

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->c(Lcom/sec/chaton/block/k;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_1

    .line 203
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->c(Lcom/sec/chaton/block/k;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v2}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 213
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_3

    .line 219
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 220
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_8

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_4

    .line 224
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/block/m;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_4
.end method
