.class public Lcom/sec/chaton/block/BlockListFragment;
.super Landroid/support/v4/app/Fragment;
.source "BlockListFragment.java"


# static fields
.field static a:Z

.field static b:Landroid/app/ProgressDialog;


# instance fields
.field c:Landroid/app/Dialog;

.field d:Landroid/view/View;

.field public e:Landroid/os/Handler;

.field private f:Landroid/widget/ListView;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/sec/chaton/d/f;

.field private i:Lcom/sec/chaton/block/q;

.field private j:Landroid/widget/CheckedTextView;

.field private k:Lcom/sec/chaton/block/s;

.field private l:Landroid/app/Activity;

.field private m:Landroid/view/MenuItem;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/block/BlockListFragment;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->c:Landroid/app/Dialog;

    .line 71
    const-string v0, "block_list"

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->n:Ljava/lang/String;

    .line 72
    const-string v0, "m_data"

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->o:Ljava/lang/String;

    .line 73
    const-string v0, "block_data"

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->p:Ljava/lang/String;

    .line 74
    const-string v0, "is_check"

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->q:Ljava/lang/String;

    .line 378
    new-instance v0, Lcom/sec/chaton/block/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/z;-><init>(Lcom/sec/chaton/block/BlockListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->e:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->j:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/block/BlockListFragment;Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/q;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/block/BlockListFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    return-object p1
.end method

.method private b()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 363
    move v1, v0

    .line 364
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 365
    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 366
    add-int/lit8 v1, v1, 0x1

    .line 364
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369
    :cond_1
    return v1
.end method

.method static synthetic b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/block/BlockListFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/block/BlockListFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/s;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->k:Lcom/sec/chaton/block/s;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/block/BlockListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->m:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/block/BlockListFragment;)I
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/chaton/block/BlockListFragment;->b()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/sec/chaton/block/BlockListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->m:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->m:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 80
    iput-object p1, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    .line 81
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 165
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 112
    const v0, 0x7f0f002d

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 114
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 115
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 177
    const v0, 0x7f030069

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 179
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "block_buddy_list"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000d

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    sput-object v0, Lcom/sec/chaton/block/BlockListFragment;->b:Landroid/app/ProgressDialog;

    .line 186
    new-instance v0, Lcom/sec/chaton/block/u;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/u;-><init>(Lcom/sec/chaton/block/BlockListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->k:Lcom/sec/chaton/block/s;

    .line 242
    if-eqz p3, :cond_0

    .line 243
    const-string v0, "block_list"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 244
    if-eqz v0, :cond_0

    .line 245
    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 246
    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    const-string v3, "m_data"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 250
    :cond_0
    new-instance v0, Lcom/sec/chaton/block/q;

    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    const v3, 0x7f030125

    iget-object v4, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/block/q;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->k:Lcom/sec/chaton/block/s;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/block/q;->a(Lcom/sec/chaton/block/s;)V

    .line 253
    if-eqz p3, :cond_1

    .line 254
    const-string v0, "block_list"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 255
    if-eqz v2, :cond_1

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v3, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    const-string v0, "block_data"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v3, v0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    const-string v0, "m_data"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    const-string v3, "is_check"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/block/q;->c:[Z

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->h:Lcom/sec/chaton/d/f;

    .line 266
    const v0, 0x7f070298

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/block/v;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/v;-><init>(Lcom/sec/chaton/block/BlockListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/block/w;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/w;-><init>(Lcom/sec/chaton/block/BlockListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->f:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/block/x;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/x;-><init>(Lcom/sec/chaton/block/BlockListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 333
    const v0, 0x7f070299

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->j:Landroid/widget/CheckedTextView;

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->j:Landroid/widget/CheckedTextView;

    new-instance v2, Lcom/sec/chaton/block/y;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/y;-><init>(Lcom/sec/chaton/block/BlockListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 359
    return-object v1
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    .line 88
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 133
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070575

    if-ne v0, v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    invoke-virtual {v0}, Lcom/sec/chaton/block/q;->a()V

    .line 155
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070574

    if-ne v0, v1, :cond_1

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->l:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 159
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 94
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 121
    const v0, 0x7f070575

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->m:Landroid/view/MenuItem;

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->m:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->k:Lcom/sec/chaton/block/s;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/block/BlockListFragment;->k:Lcom/sec/chaton/block/s;

    invoke-interface {v0}, Lcom/sec/chaton/block/s;->onClick()V

    .line 128
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 129
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 375
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 376
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 419
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 420
    const-string v1, "m_data"

    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 421
    const-string v1, "block_data"

    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v2, v2, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 422
    const-string v1, "is_check"

    iget-object v2, p0, Lcom/sec/chaton/block/BlockListFragment;->i:Lcom/sec/chaton/block/q;

    iget-object v2, v2, Lcom/sec/chaton/block/q;->c:[Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 423
    const-string v1, "block_list"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 424
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 100
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 106
    return-void
.end method
