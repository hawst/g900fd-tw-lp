.class Lcom/sec/chaton/block/r;
.super Landroid/os/Handler;
.source "BlockListBuddyAdapter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/q;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/q;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const v4, 0x7f0b0294

    const/4 v3, 0x0

    .line 161
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 162
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 164
    :pswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_6

    .line 165
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_1

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->a(Lcom/sec/chaton/block/q;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v1}, Lcom/sec/chaton/block/q;->a(Lcom/sec/chaton/block/q;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->b(Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/block/s;->c()V

    goto :goto_0

    .line 169
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_0

    .line 171
    :try_start_0
    const-string v1, ""

    move v2, v3

    .line 172
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    move v4, v3

    .line 173
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    iget-object v5, v0, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    iget-object v0, v0, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    iget-object v0, v0, Lcom/sec/chaton/block/aa;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v4, v1, :cond_2

    .line 177
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v1, v1, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 173
    :goto_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v1, v0

    goto :goto_2

    .line 172
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->a(Lcom/sec/chaton/block/q;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v1}, Lcom/sec/chaton/block/q;->a(Lcom/sec/chaton/block/q;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0294

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-virtual {v0}, Lcom/sec/chaton/block/q;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->c(Lcom/sec/chaton/block/q;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->c(Lcom/sec/chaton/block/q;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->c(Lcom/sec/chaton/block/q;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 197
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v1, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v1, v1, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Z

    iput-object v1, v0, Lcom/sec/chaton/block/q;->c:[Z

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->b(Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/block/s;->a()V

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->b(Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/block/s;->b()V

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    iget-object v0, v0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 190
    :catch_0
    move-exception v0

    .line 191
    const-string v0, "index out of bounds exception occured"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 206
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->a(Lcom/sec/chaton/block/q;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b3

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->c(Lcom/sec/chaton/block/q;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->c(Lcom/sec/chaton/block/q;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->c(Lcom/sec/chaton/block/q;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 210
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->b(Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/block/s;->a()V

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/block/r;->a:Lcom/sec/chaton/block/q;

    invoke-static {v0}, Lcom/sec/chaton/block/q;->b(Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/block/s;->b()V

    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    goto/16 :goto_3

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x25c
        :pswitch_0
    .end packed-switch
.end method
