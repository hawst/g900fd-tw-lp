.class Lcom/sec/chaton/block/p;
.super Landroid/os/Handler;
.source "BlockImpl.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/k;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/k;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 328
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 329
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 363
    :sswitch_0
    sget-object v1, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 364
    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 365
    sget-object v1, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 368
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->g(Lcom/sec/chaton/block/k;)Lcom/sec/chaton/d/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/f;->a()V

    goto :goto_0

    .line 374
    :sswitch_1
    sget-object v1, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 376
    :try_start_0
    sget-object v1, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    :cond_2
    :goto_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_3

    .line 383
    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 384
    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->e(Lcom/sec/chaton/block/k;)Lcom/sec/chaton/block/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    .line 385
    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 386
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 387
    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 395
    :cond_3
    :goto_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_d

    .line 396
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_b

    .line 397
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 399
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBlockBuddyList;

    .line 400
    iget-object v4, v0, Lcom/sec/chaton/io/entry/GetBlockBuddyList;->buddy:Ljava/util/ArrayList;

    .line 402
    if-eqz v4, :cond_0

    .line 403
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move v1, v2

    .line 410
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NO : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", Name : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->name:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", ORGNUM : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->orgnum:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->h(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    :cond_4
    new-instance v5, Lcom/sec/chaton/block/aa;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->value:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->name:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BlockBuddy;->orgnum:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v0}, Lcom/sec/chaton/block/aa;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    .line 378
    :catch_0
    move-exception v1

    .line 379
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 390
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 391
    iget-object v1, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v1}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_2

    .line 424
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->e(Lcom/sec/chaton/block/k;)Lcom/sec/chaton/block/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    .line 453
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 458
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_e

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v3}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v3}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 470
    :cond_8
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 472
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_f

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 480
    :cond_9
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->f(Lcom/sec/chaton/block/k;)V

    .line 494
    :cond_a
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-virtual {v0}, Lcom/sec/chaton/block/k;->d()V

    goto/16 :goto_0

    .line 434
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->e(Lcom/sec/chaton/block/k;)Lcom/sec/chaton/block/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/a;->notifyDataSetChanged()V

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 437
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_c

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_4

    .line 441
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 442
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_4

    .line 447
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_4

    .line 463
    :cond_e
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->c(Lcom/sec/chaton/block/k;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v3}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v3}, Lcom/sec/chaton/block/k;->b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_5

    .line 467
    :catch_1
    move-exception v0

    goto/16 :goto_5

    .line 476
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_6

    .line 482
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 483
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 484
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_11

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_7

    .line 488
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/block/p;->a:Lcom/sec/chaton/block/k;

    invoke-static {v0}, Lcom/sec/chaton/block/k;->d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto/16 :goto_7

    .line 329
    nop

    :sswitch_data_0
    .sparse-switch
        0x259 -> :sswitch_1
        0x25e -> :sswitch_0
    .end sparse-switch
.end method
