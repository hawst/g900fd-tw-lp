.class Lcom/sec/chaton/block/u;
.super Ljava/lang/Object;
.source "BlockListFragment.java"

# interfaces
.implements Lcom/sec/chaton/block/s;


# instance fields
.field final synthetic a:Lcom/sec/chaton/block/BlockListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/block/BlockListFragment;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/q;->b()V

    .line 203
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    iget-object v1, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    new-instance v1, Lcom/sec/chaton/block/q;

    iget-object v2, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v2}, Lcom/sec/chaton/block/BlockListFragment;->c(Lcom/sec/chaton/block/BlockListFragment;)Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f030125

    iget-object v4, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v4}, Lcom/sec/chaton/block/BlockListFragment;->d(Lcom/sec/chaton/block/BlockListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/chaton/block/q;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/q;

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->e(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/block/q;->a(Lcom/sec/chaton/block/s;)V

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->f(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v1}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->d(Lcom/sec/chaton/block/BlockListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->d(Lcom/sec/chaton/block/BlockListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/block/BlockListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->a(Lcom/sec/chaton/block/BlockListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/chaton/block/q;->a(Z)V

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->g(Lcom/sec/chaton/block/BlockListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 221
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->c(Lcom/sec/chaton/block/BlockListFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 239
    return-void
.end method

.method public onClick()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->g(Lcom/sec/chaton/block/BlockListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->g(Lcom/sec/chaton/block/BlockListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 233
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->b(Lcom/sec/chaton/block/BlockListFragment;)Lcom/sec/chaton/block/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/block/q;->b()V

    .line 234
    return-void

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/u;->a:Lcom/sec/chaton/block/BlockListFragment;

    invoke-static {v0}, Lcom/sec/chaton/block/BlockListFragment;->g(Lcom/sec/chaton/block/BlockListFragment;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
