.class public Lcom/sec/chaton/block/BlockFragment;
.super Landroid/support/v4/app/Fragment;
.source "BlockFragment.java"


# static fields
.field static a:Z

.field static b:Landroid/app/ProgressDialog;


# instance fields
.field c:Landroid/app/Dialog;

.field d:Landroid/view/View;

.field e:[Ljava/lang/String;

.field public f:Landroid/os/Handler;

.field private g:Landroid/widget/ListView;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/sec/chaton/d/f;

.field private j:Lcom/sec/chaton/block/a;

.field private k:Landroid/view/ViewStub;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Z

.field private q:Landroid/app/Activity;

.field private r:Landroid/view/MenuItem;

.field private s:Landroid/view/MenuItem;

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/block/BlockFragment;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->c:Landroid/app/Dialog;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/block/BlockFragment;->p:Z

    .line 68
    iput-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->r:Landroid/view/MenuItem;

    .line 69
    iput-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    .line 70
    iput-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->e:[Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    .line 305
    new-instance v0, Lcom/sec/chaton/block/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/i;-><init>(Lcom/sec/chaton/block/BlockFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->f:Landroid/os/Handler;

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/block/BlockFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/block/BlockFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/block/BlockFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 549
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->l:Landroid/view/View;

    if-nez v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->k:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->l:Landroid/view/View;

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->l:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->m:Landroid/widget/ImageView;

    .line 552
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->m:Landroid/widget/ImageView;

    const v1, 0x7f02034b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->l:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->n:Landroid/widget/TextView;

    .line 554
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->l:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->o:Landroid/widget/TextView;

    .line 556
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 557
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->k:Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 559
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/block/BlockFragment;)Lcom/sec/chaton/block/a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->j:Lcom/sec/chaton/block/a;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/block/BlockFragment;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/chaton/block/BlockFragment;->b()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/block/BlockFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->r:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/block/BlockFragment;)Lcom/sec/chaton/d/f;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->i:Lcom/sec/chaton/d/f;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/block/BlockFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/block/BlockFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02cb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public a([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->i:Lcom/sec/chaton/d/f;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/d/f;->a([Ljava/lang/String;)V

    .line 161
    sget-object v0, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 162
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 142
    sparse-switch p1, :sswitch_data_0

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 144
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->i:Lcom/sec/chaton/d/f;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->i:Lcom/sec/chaton/d/f;

    invoke-virtual {v0}, Lcom/sec/chaton/d/f;->a()V

    .line 146
    sget-object v0, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 150
    :sswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 151
    const-string v0, "Remove multiple block buddy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v0, "blindlist"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->e:[Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->e:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/block/BlockFragment;->a([Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 78
    iput-object p1, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    .line 79
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 91
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 96
    const v0, 0x7f0f0013

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 97
    const v0, 0x7f070595

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->r:Landroid/view/MenuItem;

    .line 98
    const v0, 0x7f070596

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;)I

    move-result v0

    .line 102
    iget-object v1, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v1

    .line 103
    sub-int v0, v1, v0

    if-gtz v0, :cond_2

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 108
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 115
    :cond_1
    return-void

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->r:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 173
    const v0, 0x7f030068

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    .line 176
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000d

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    sput-object v0, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    .line 179
    const v0, 0x7f0700a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->k:Landroid/view/ViewStub;

    .line 181
    new-instance v0, Lcom/sec/chaton/block/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/f;-><init>(Lcom/sec/chaton/block/BlockFragment;)V

    .line 230
    new-instance v2, Lcom/sec/chaton/block/a;

    iget-object v3, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    const v4, 0x7f030124

    iget-object v5, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/block/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v2, p0, Lcom/sec/chaton/block/BlockFragment;->j:Lcom/sec/chaton/block/a;

    .line 231
    iget-object v2, p0, Lcom/sec/chaton/block/BlockFragment;->j:Lcom/sec/chaton/block/a;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/block/a;->a(Lcom/sec/chaton/block/d;)V

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->f:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->i:Lcom/sec/chaton/d/f;

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->i:Lcom/sec/chaton/d/f;

    invoke-virtual {v0}, Lcom/sec/chaton/d/f;->a()V

    .line 235
    sget-object v0, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 237
    const v0, 0x7f070298

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->g:Landroid/widget/ListView;

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->g:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/block/BlockFragment;->j:Lcom/sec/chaton/block/a;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->g:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/block/g;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/g;-><init>(Lcom/sec/chaton/block/BlockFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->g:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/block/h;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/h;-><init>(Lcom/sec/chaton/block/BlockFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 280
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 299
    sget-object v0, Lcom/sec/chaton/block/BlockFragment;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 302
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 303
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    .line 86
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 120
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 135
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 122
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/block/BlockListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    const-string v1, "block_buddy_list"

    iget-object v2, p0, Lcom/sec/chaton/block/BlockFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 124
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/block/BlockFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 127
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/block/BlockFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 128
    const-string v1, "BUDDY_SORT_STYLE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 129
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 130
    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 131
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/block/BlockFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x7f070595
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 499
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 504
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 506
    iget-object v2, p0, Lcom/sec/chaton/block/BlockFragment;->q:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 504
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 510
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/block/BlockFragment;->t:Ljava/util/ArrayList;

    .line 531
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 286
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 292
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 494
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 495
    return-void
.end method
