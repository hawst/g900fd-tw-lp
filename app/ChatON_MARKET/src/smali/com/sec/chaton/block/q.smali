.class Lcom/sec/chaton/block/q;
.super Landroid/widget/ArrayAdapter;
.source "BlockListBuddyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/block/aa;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field public c:[Z

.field public d:Landroid/os/Handler;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/chaton/d/f;

.field private g:Landroid/view/LayoutInflater;

.field private h:Landroid/app/ProgressDialog;

.field private i:Landroid/view/ViewGroup;

.field private j:Lcom/sec/chaton/block/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 158
    new-instance v0, Lcom/sec/chaton/block/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/r;-><init>(Lcom/sec/chaton/block/q;)V

    iput-object v0, p0, Lcom/sec/chaton/block/q;->d:Landroid/os/Handler;

    .line 74
    iput-object p1, p0, Lcom/sec/chaton/block/q;->e:Landroid/content/Context;

    .line 75
    iput-object p3, p0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/chaton/block/q;->c:[Z

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/block/q;->e:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/block/q;->g:Landroid/view/LayoutInflater;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/block/q;->g:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/block/q;->i:Landroid/view/ViewGroup;

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/block/q;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/block/q;->h:Landroid/app/ProgressDialog;

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/block/q;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/block/q;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/block/q;)Lcom/sec/chaton/block/s;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/block/q;->j:Lcom/sec/chaton/block/s;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/block/q;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/block/q;->h:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/block/q;->d:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/q;->f:Lcom/sec/chaton/d/f;

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/block/q;->f:Lcom/sec/chaton/d/f;

    iget-object v1, p0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/f;->a(Ljava/util/ArrayList;)V

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/block/q;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 90
    return-void
.end method

.method public a(Lcom/sec/chaton/block/s;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/chaton/block/q;->j:Lcom/sec/chaton/block/s;

    .line 69
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/block/q;->c:[Z

    array-length v1, v0

    .line 94
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 95
    iget-object v2, p0, Lcom/sec/chaton/block/q;->c:[Z

    aput-boolean p1, v2, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    if-eqz p1, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/block/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 103
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/block/q;->notifyDataSetChanged()V

    .line 104
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1
.end method

.method public b()V
    .locals 4

    .prologue
    .line 111
    const-string v1, ""

    .line 112
    const/4 v0, 0x0

    move v3, v0

    move-object v0, v1

    move v1, v3

    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 113
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/block/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    iget-object v0, v0, Lcom/sec/chaton/block/aa;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 112
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0

    .line 115
    :cond_0
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 121
    if-nez p2, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/block/q;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f030125

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 124
    new-instance v2, Lcom/sec/chaton/block/t;

    move-object v0, v1

    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    invoke-direct {v2, v0}, Lcom/sec/chaton/block/t;-><init>(Lcom/sec/chaton/widget/CheckableRelativeLayout;)V

    .line 127
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object p2, v1

    move-object v1, v2

    .line 132
    :goto_0
    iget-object v0, v1, Lcom/sec/chaton/block/t;->d:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/chaton/block/q;->c:[Z

    aget-boolean v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 134
    invoke-virtual {p0, p1}, Lcom/sec/chaton/block/q;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/aa;

    .line 135
    iget-object v2, v1, Lcom/sec/chaton/block/t;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/block/aa;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v2, p0, Lcom/sec/chaton/block/q;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/block/t;->c:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/sec/chaton/block/aa;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 137
    return-object p2

    .line 129
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/block/t;

    move-object v1, v0

    goto :goto_0
.end method
