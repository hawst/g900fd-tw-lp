.class public Lcom/sec/chaton/block/k;
.super Ljava/lang/Object;
.source "BlockImpl.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# static fields
.field static a:Z

.field static b:Landroid/app/ProgressDialog;


# instance fields
.field c:Landroid/app/Dialog;

.field d:Landroid/view/View;

.field public e:Landroid/os/Handler;

.field private f:Lcom/sec/chaton/settings2/PrefFragmentBlock;

.field private g:Landroid/widget/ListView;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/sec/chaton/d/f;

.field private j:Lcom/sec/chaton/block/a;

.field private k:Landroid/view/ViewStub;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Z

.field private q:Landroid/app/Activity;

.field private r:Landroid/view/MenuItem;

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/block/k;->a:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentBlock;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/sec/chaton/block/k;->c:Landroid/app/Dialog;

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/block/k;->p:Z

    .line 72
    iput-object v1, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    .line 74
    iput-object v1, p0, Lcom/sec/chaton/block/k;->s:Ljava/util/ArrayList;

    .line 325
    new-instance v0, Lcom/sec/chaton/block/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/p;-><init>(Lcom/sec/chaton/block/k;)V

    iput-object v0, p0, Lcom/sec/chaton/block/k;->e:Landroid/os/Handler;

    .line 80
    iput-object p1, p0, Lcom/sec/chaton/block/k;->f:Lcom/sec/chaton/settings2/PrefFragmentBlock;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/block/k;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/block/k;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/block/k;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/chaton/block/k;->s:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/block/k;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/block/k;->t:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/block/k;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/block/k;)Lcom/sec/chaton/block/a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/block/k;->j:Lcom/sec/chaton/block/a;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/chaton/block/k;->l:Landroid/view/View;

    if-nez v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/block/k;->k:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/k;->l:Landroid/view/View;

    .line 599
    iget-object v0, p0, Lcom/sec/chaton/block/k;->l:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/block/k;->m:Landroid/widget/ImageView;

    .line 600
    iget-object v0, p0, Lcom/sec/chaton/block/k;->m:Landroid/widget/ImageView;

    const v1, 0x7f02034b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 601
    iget-object v0, p0, Lcom/sec/chaton/block/k;->l:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/block/k;->n:Landroid/widget/TextView;

    .line 602
    iget-object v0, p0, Lcom/sec/chaton/block/k;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/block/k;->l:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/block/k;->o:Landroid/widget/TextView;

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/block/k;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 605
    iget-object v0, p0, Lcom/sec/chaton/block/k;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/block/k;->k:Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 607
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/block/k;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/block/k;->e()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/block/k;)Lcom/sec/chaton/d/f;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/block/k;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/block/k;->s:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 135
    const v0, 0x7f030068

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000d

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    sput-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    .line 141
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const v0, 0x7f070416

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 143
    const v0, 0x7f07050b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 144
    const v3, 0x7f0b01e5

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 145
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 146
    new-instance v3, Lcom/sec/chaton/block/l;

    invoke-direct {v3, p0}, Lcom/sec/chaton/block/l;-><init>(Lcom/sec/chaton/block/k;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    const v0, 0x7f07050d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/block/k;->t:Landroid/widget/TextView;

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/block/k;->t:Landroid/widget/TextView;

    const v3, 0x7f0b02cb

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/block/k;->t:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 155
    const v0, 0x7f07050e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 156
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    :cond_0
    const v0, 0x7f0700a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/block/k;->k:Landroid/view/ViewStub;

    .line 162
    new-instance v0, Lcom/sec/chaton/block/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/m;-><init>(Lcom/sec/chaton/block/k;)V

    .line 234
    new-instance v2, Lcom/sec/chaton/block/a;

    iget-object v3, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    const v4, 0x7f030124

    iget-object v5, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/chaton/block/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v2, p0, Lcom/sec/chaton/block/k;->j:Lcom/sec/chaton/block/a;

    .line 235
    iget-object v2, p0, Lcom/sec/chaton/block/k;->j:Lcom/sec/chaton/block/a;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/block/a;->a(Lcom/sec/chaton/block/d;)V

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/block/k;->e:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    invoke-virtual {v0}, Lcom/sec/chaton/d/f;->a()V

    .line 239
    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 241
    const v0, 0x7f070298

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/block/k;->g:Landroid/widget/ListView;

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/block/k;->g:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/block/k;->j:Lcom/sec/chaton/block/a;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/block/k;->g:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/block/n;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/n;-><init>(Lcom/sec/chaton/block/k;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/block/k;->g:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/block/o;

    invoke-direct {v2, p0}, Lcom/sec/chaton/block/o;-><init>(Lcom/sec/chaton/block/k;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 284
    return-object v1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    .line 91
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 308
    sparse-switch p1, :sswitch_data_0

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 310
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    invoke-virtual {v0}, Lcom/sec/chaton/d/f;->a()V

    .line 312
    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 316
    :sswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 317
    const-string v0, "Remove multiple block buddy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v1, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    const-string v0, "blindlist"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/f;->a([Ljava/lang/String;)V

    .line 319
    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 308
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    .line 87
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 589
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 99
    const-string v0, "BIMA"

    const-string v1, "onPrepareOptionsMenu"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const v0, 0x7f070596

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 102
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const v0, 0x7f070595

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 105
    :cond_0
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 94
    const-string v0, "BIMA"

    const-string v1, "onCreateOptionsMenu"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const v0, 0x7f0f0013

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 96
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 109
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 129
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 112
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/block/BlockListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    const-string v1, "block_buddy_list"

    iget-object v2, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 114
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/sec/chaton/block/k;->f:Lcom/sec/chaton/settings2/PrefFragmentBlock;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentBlock;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 121
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 122
    const-string v1, "BUDDY_SORT_STYLE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 125
    iget-object v1, p0, Lcom/sec/chaton/block/k;->f:Lcom/sec/chaton/settings2/PrefFragmentBlock;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentBlock;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x7f070595
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 288
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/block/k;->i:Lcom/sec/chaton/d/f;

    invoke-virtual {v0}, Lcom/sec/chaton/d/f;->a()V

    .line 291
    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 294
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 299
    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 300
    sget-object v0, Lcom/sec/chaton/block/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 303
    :cond_0
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    const v5, 0x7f0b02cb

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 505
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 506
    iget-object v0, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 514
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 515
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 516
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 517
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 536
    :cond_0
    :goto_1
    return-void

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/block/k;->t:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/block/k;->q:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/block/k;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 520
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 521
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_1

    .line 525
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 526
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_4

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 530
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/block/k;->r:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_1
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0, p1}, Lcom/sec/chaton/block/k;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
