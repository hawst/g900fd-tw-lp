.class Lcom/sec/chaton/ac;
.super Ljava/lang/Object;
.source "PlusFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/PlusFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/PlusFragment;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/aj;

    iget v1, v0, Lcom/sec/chaton/aj;->b:I

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/aj;

    iget-object v2, v0, Lcom/sec/chaton/aj;->e:Ljava/lang/String;

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/aj;

    iget-object v0, v0, Lcom/sec/chaton/aj;->g:Ljava/lang/String;

    .line 305
    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 309
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/aj;

    iget-object v0, v0, Lcom/sec/chaton/aj;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 311
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/PlusFragment;->startActivity(Landroid/content/Intent;)V

    .line 328
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/sec/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 319
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    invoke-static {v2}, Lcom/sec/chaton/util/am;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 322
    :cond_1
    invoke-static {v2}, Lcom/sec/chaton/util/am;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 325
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/ac;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/chaton/util/am;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
