.class Lcom/sec/chaton/bk;
.super Landroid/os/Handler;
.source "TabActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 2184
    iput-object p1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2187
    iget-object v0, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->F(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2299
    :cond_0
    :goto_0
    return-void

    .line 2191
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 2192
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x450

    if-ne v1, v2, :cond_b

    .line 2193
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 2194
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 2195
    if-eqz v0, :cond_0

    .line 2196
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2199
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "3.3.51"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2200
    :cond_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 2201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2205
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->G(Lcom/sec/chaton/TabActivity;)V

    .line 2228
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->H(Lcom/sec/chaton/TabActivity;)Ljava/lang/String;

    move-result-object v1

    .line 2230
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Disclaimer statue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->critical:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v4, :cond_0

    .line 2241
    :cond_4
    const-string v2, "RUN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 2242
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->needPopup:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->disclaimerUID:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2244
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2245
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->disclaimerUID:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Ljava/lang/String;)V

    .line 2257
    :goto_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "is_first_accept"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Accept time : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "accept_time"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Server time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "server_time"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2259
    const-string v0, "accept_time"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "server_time"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2260
    const-string v0, "is_first_accept"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 2210
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2211
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1, v0}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 2212
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    iget-object v1, v1, Lcom/sec/chaton/TabActivity;->d:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/bj;->c()V

    goto/16 :goto_1

    .line 2215
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2216
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    const-string v2, "NO"

    invoke-static {v0, v1, v4, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 2218
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1, v4}, Lcom/sec/chaton/TabActivity;->g(Lcom/sec/chaton/TabActivity;Z)Z

    .line 2219
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1, v0}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    goto/16 :goto_1

    .line 2247
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1, v4}, Lcom/sec/chaton/TabActivity;->h(Lcom/sec/chaton/TabActivity;Z)Z

    .line 2248
    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1, v0}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    goto/16 :goto_2

    .line 2252
    :cond_9
    const-string v0, "needPopup or disclaimerUID is null"

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2255
    :cond_a
    const-string v0, "Disclaimer State is running"

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2269
    :cond_b
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x452

    if-ne v1, v2, :cond_0

    .line 2270
    const-string v1, "2"

    .line 2271
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 2273
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_d

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2276
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateAppsReady"

    const-string v2, "YES"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2277
    iget-object v0, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2278
    iget-object v0, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    const-string v2, "YES"

    invoke-static {v0, v1, v4, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 2283
    :goto_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2284
    const-string v0, "Samsung apps is ready to upgrade chaton"

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2280
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->g(Lcom/sec/chaton/TabActivity;Z)Z

    goto :goto_3

    .line 2288
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 2289
    iget-object v0, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    const-string v2, "NO"

    invoke-static {v0, v1, v4, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 2294
    :goto_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2295
    const-string v0, "Samsung apps is NOT ready to upgrade chaton"

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2291
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/bk;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->g(Lcom/sec/chaton/TabActivity;Z)Z

    goto :goto_4
.end method
