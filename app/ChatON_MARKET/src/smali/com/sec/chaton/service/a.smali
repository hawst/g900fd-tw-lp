.class Lcom/sec/chaton/service/a;
.super Lcom/sec/chaton/util/ar;
.source "BackGroundRegiService.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/service/BackGroundRegiService;


# direct methods
.method constructor <init>(Lcom/sec/chaton/service/BackGroundRegiService;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/chaton/service/a;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-direct {p0}, Lcom/sec/chaton/util/ar;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 194
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_3

    .line 195
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 196
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Push registration failed. Network is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v0, :cond_1

    const-string v0, "unavailable"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/service/a;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0}, Lcom/sec/chaton/service/BackGroundRegiService;->a(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/at;->b()V

    .line 218
    :cond_0
    :goto_2
    return-void

    .line 197
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 199
    :cond_2
    const-string v0, "Push registration success."

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 203
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3ea

    if-ne v0, v1, :cond_0

    .line 206
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 207
    const-string v0, "Push deregistration is success. execute chaton deregistration."

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/service/a;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    iget-object v1, p0, Lcom/sec/chaton/service/a;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v1}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/ap;->a()Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/service/BackGroundRegiService;->a(Lcom/sec/chaton/service/BackGroundRegiService;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;

    goto :goto_2

    .line 210
    :cond_4
    const-string v0, "Push deregistration is fail"

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
