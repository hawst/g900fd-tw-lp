.class public Lcom/sec/chaton/service/ChatONVService;
.super Landroid/app/Service;
.source "ChatONVService.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/coolots/sso/a/a;

.field private d:Lcom/sec/chaton/d/h;

.field private e:Z

.field private f:I

.field private g:Z

.field private h:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/chaton/service/ChatONVService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 229
    new-instance v0, Lcom/sec/chaton/service/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/service/e;-><init>(Lcom/sec/chaton/service/ChatONVService;)V

    iput-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->h:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/service/ChatONVService;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/chaton/service/ChatONVService;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/sec/chaton/service/ChatONVService;->g:Z

    if-nez v0, :cond_1

    .line 259
    invoke-virtual {p0}, Lcom/sec/chaton/service/ChatONVService;->stopSelf()V

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/service/ChatONVService;->e:Z

    .line 261
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 262
    const-string v0, "ChatONV result : no pendding task"

    sget-object v1, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    iget v0, p0, Lcom/sec/chaton/service/ChatONVService;->f:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/service/ChatONVService;->a(I)V

    .line 267
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 268
    const-string v0, "checkTaskFinish : task is pendding"

    sget-object v1, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 207
    if-nez p1, :cond_1

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->d:Lcom/sec/chaton/d/h;

    const-string v1, "voip"

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;IZ)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    if-ne p1, v3, :cond_3

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->c:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/service/ChatONVService;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/service/ChatONVService;->c:Lcom/coolots/sso/a/a;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/sec/chaton/service/ChatONVService;->b()V

    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->d:Lcom/sec/chaton/d/h;

    const-string v1, "voip"

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    .line 221
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "type is not valid"

    sget-object v1, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual {p0}, Lcom/sec/chaton/service/ChatONVService;->stopSelf()V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 169
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 170
    const-string v0, "onCreate "

    sget-object v1, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 192
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 193
    const-string v0, "onDestroy "

    sget-object v1, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->c:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->c:Lcom/coolots/sso/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 203
    :cond_1
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 292
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveCreateAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/service/ChatONVService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_0
    if-ne p1, v3, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/service/ChatONVService;->d:Lcom/sec/chaton/d/h;

    const-string v1, "voip"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;IZ)V

    .line 302
    :goto_0
    return-void

    .line 299
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/service/ChatONVService;->b()V

    goto :goto_0
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 0

    .prologue
    .line 308
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/sec/chaton/service/ChatONVService;->stopSelf()V

    .line 48
    const/4 v0, 0x2

    return v0
.end method
