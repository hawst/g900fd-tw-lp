.class public Lcom/sec/chaton/service/BackGroundRegiService;
.super Landroid/app/Service;
.source "BackGroundRegiService.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:Z

.field private d:Lcom/coolots/sso/a/a;

.field private e:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private f:Lcom/sec/chaton/d/ap;

.field private g:Lcom/sec/chaton/d/l;

.field private h:Lcom/sec/chaton/d/at;

.field private i:Lcom/sec/chaton/d/a/ak;

.field private j:I

.field private k:Lcom/sec/chaton/util/ar;

.field private l:Landroid/os/Handler;

.field private m:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->d:Lcom/coolots/sso/a/a;

    .line 189
    new-instance v0, Lcom/sec/chaton/service/a;

    invoke-direct {v0, p0}, Lcom/sec/chaton/service/a;-><init>(Lcom/sec/chaton/service/BackGroundRegiService;)V

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    .line 221
    new-instance v0, Lcom/sec/chaton/service/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/service/b;-><init>(Lcom/sec/chaton/service/BackGroundRegiService;)V

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->l:Landroid/os/Handler;

    .line 368
    new-instance v0, Lcom/sec/chaton/service/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/service/c;-><init>(Lcom/sec/chaton/service/BackGroundRegiService;)V

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->m:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/service/BackGroundRegiService;I)I
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->j:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/service/BackGroundRegiService;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->i:Lcom/sec/chaton/d/a/ak;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->h:Lcom/sec/chaton/d/at;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/service/BackGroundRegiService;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/chaton/service/BackGroundRegiService;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 319
    invoke-direct {p0}, Lcom/sec/chaton/service/BackGroundRegiService;->b()Z

    move-result v0

    if-ne v0, v4, :cond_2

    .line 320
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 322
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "packageName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :cond_0
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "user_id"

    aput-object v2, v1, v3

    const-string v2, "email_id"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "mcc"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "api_server_url"

    aput-object v3, v1, v2

    .line 328
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 329
    const-string v3, "client_id"

    const-string v4, "fs24s8z0hh"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const-string v3, "client_secret"

    const-string v4, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 331
    const-string v3, "mypackage"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    const-string v0, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    const-string v0, "MODE"

    const-string v3, "BACKGROUND"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    const-string v0, "additional"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    if-eqz p1, :cond_1

    .line 338
    const-string v0, "expired_access_token"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "samsung_account_token"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/chaton/service/BackGroundRegiService;->sendBroadcast(Landroid/content/Intent;)V

    .line 349
    :goto_0
    return-void

    .line 343
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 344
    const-string v0, "it was NOT supported auto SSO Registration"

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_3
    invoke-direct {p0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/ap;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->f:Lcom/sec/chaton/d/ap;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/service/BackGroundRegiService;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 404
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " finishRegiService : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_0
    if-eqz p1, :cond_1

    .line 409
    invoke-static {}, Lcom/sec/chaton/c/a;->d()V

    .line 410
    sget-object v0, Lcom/sec/chaton/util/cc;->b:Lcom/sec/chaton/util/cc;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Lcom/sec/chaton/util/cc;)V

    .line 411
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "first_time_after_regi"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 412
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->e()V

    .line 419
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "back_auto_regi"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 420
    const-string v1, "mode"

    const-string v2, "end"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 423
    invoke-virtual {p0}, Lcom/sec/chaton/service/BackGroundRegiService;->stopSelf()V

    .line 425
    return-void

    .line 414
    :cond_1
    sget-object v0, Lcom/sec/chaton/util/cc;->a:Lcom/sec/chaton/util/cc;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Lcom/sec/chaton/util/cc;)V

    goto :goto_0
.end method

.method private b()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 352
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 353
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Landroid/content/Context;)I

    move-result v4

    .line 355
    const/16 v0, 0x32c9

    if-lt v4, v0, :cond_1

    move v0, v1

    .line 357
    :goto_0
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_0

    .line 358
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "accountEmail : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ssoVer : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ssoSuppoted : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    if-ne v0, v1, :cond_2

    .line 364
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 355
    goto :goto_0

    :cond_2
    move v1, v2

    .line 364
    goto :goto_1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 428
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 429
    const-string v1, "com.sec.chaton"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 431
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 433
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 435
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/service/BackGroundRegiService;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/chaton/service/BackGroundRegiService;->c()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/service/BackGroundRegiService;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->j:I

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/service/BackGroundRegiService;)Z
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/chaton/service/BackGroundRegiService;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/service/BackGroundRegiService;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->j:I

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/a/ak;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->i:Lcom/sec/chaton/d/a/ak;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/service/BackGroundRegiService;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->c:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/util/ar;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->e:Lcom/sec/chaton/d/a;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/16 v3, 0x7530

    const/16 v2, 0x3ea

    .line 148
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 149
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "type is not valid"

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    if-nez p1, :cond_4

    .line 155
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->d:Lcom/coolots/sso/a/a;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->d:Lcom/coolots/sso/a/a;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->e:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ar;->a(II)V

    goto :goto_0

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->e:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ar;->a(II)V

    goto :goto_0

    .line 172
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->e:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ar;->a(II)V

    goto :goto_0

    .line 176
    :cond_4
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 92
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 94
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "onCreate "

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->d:Lcom/coolots/sso/a/a;

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->d:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->l:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->f:Lcom/sec/chaton/d/ap;

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->l:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->h:Lcom/sec/chaton/d/at;

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->l:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->g:Lcom/sec/chaton/d/l;

    .line 110
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->e:Lcom/sec/chaton/d/a;

    .line 112
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 113
    const-string v1, "action_sso_receive_code"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/service/BackGroundRegiService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->j:I

    .line 117
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 121
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 123
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "onDestroy "

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/service/BackGroundRegiService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 130
    iget v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 131
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/global/GlobalApplication;->g:Z

    .line 133
    invoke-static {}, Lcom/sec/chaton/util/cb;->a()Lcom/sec/chaton/util/cc;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cc;->d:Lcom/sec/chaton/util/cc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/cc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    new-instance v0, Landroid/content/Intent;

    const-string v1, "back_auto_regi"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    const-string v1, "mode"

    const-string v2, "end"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 140
    :cond_1
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->d:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->d:Lcom/coolots/sso/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 144
    :cond_2
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 440
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 441
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveCreateAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_0
    return-void
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 3

    .prologue
    .line 448
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveRemoveAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->e:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->k:Lcom/sec/chaton/util/ar;

    const/16 v1, 0x3ea

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ar;->a(II)V

    .line 460
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 74
    if-nez p1, :cond_0

    .line 75
    const/4 v0, 0x2

    .line 87
    :goto_0
    return v0

    .line 78
    :cond_0
    const-string v0, "request_type"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->b:I

    .line 79
    const-string v0, "request_on_chaton"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->c:Z

    .line 81
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onStartCommand, startId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mOnChatON : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/service/BackGroundRegiService;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/service/BackGroundRegiService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_1
    iget v0, p0, Lcom/sec/chaton/service/BackGroundRegiService;->b:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/service/BackGroundRegiService;->a(I)V

    .line 87
    const/4 v0, 0x1

    goto :goto_0
.end method
