.class public Lcom/sec/chaton/service/LocalService;
.super Lcom/sec/chaton/service/BaseService;
.source "LocalService.java"


# instance fields
.field private final c:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/chaton/service/BaseService;-><init>()V

    .line 74
    new-instance v0, Lcom/sec/chaton/service/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/service/f;-><init>(Lcom/sec/chaton/service/LocalService;)V

    iput-object v0, p0, Lcom/sec/chaton/service/LocalService;->c:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 37
    if-nez p1, :cond_0

    .line 47
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/service/LocalService;->c()Lcom/sec/chaton/service/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/service/d;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 44
    iput p2, v0, Landroid/os/Message;->what:I

    .line 45
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/service/LocalService;->c()Lcom/sec/chaton/service/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/service/d;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected a(Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/service/LocalService;->c:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Lcom/sec/chaton/service/BaseService;->onCreate()V

    .line 29
    invoke-virtual {p0}, Lcom/sec/chaton/service/LocalService;->a()V

    .line 30
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Lcom/sec/chaton/service/BaseService;->onDestroy()V

    .line 53
    invoke-virtual {p0}, Lcom/sec/chaton/service/LocalService;->b()V

    .line 54
    return-void
.end method
