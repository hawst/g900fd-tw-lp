.class Lcom/sec/chaton/service/b;
.super Landroid/os/Handler;
.source "BackGroundRegiService.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/service/BackGroundRegiService;


# direct methods
.method constructor <init>(Lcom/sec/chaton/service/BackGroundRegiService;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 224
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 225
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 315
    :goto_0
    return-void

    .line 228
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->a(Lcom/sec/chaton/service/BackGroundRegiService;Z)V

    goto :goto_0

    .line 231
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 232
    const-string v0, "provision was failed"

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Lcom/sec/chaton/service/BackGroundRegiService;Z)V

    goto :goto_0

    .line 241
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 242
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 243
    const-string v0, "Fianlly register was success with SA automatically"

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_2
    const-string v0, "did_samsung_account_mapping"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->a(Lcom/sec/chaton/service/BackGroundRegiService;I)I

    .line 250
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "temp_account_country_code"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 253
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Final account ISO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v1}, Lcom/sec/chaton/service/BackGroundRegiService;->c(Lcom/sec/chaton/service/BackGroundRegiService;)V

    .line 257
    const-string v1, "account_country_code"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v4}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Lcom/sec/chaton/service/BackGroundRegiService;Z)V

    goto/16 :goto_0

    .line 261
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xc356

    if-ne v0, v1, :cond_9

    .line 262
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SSO token validaion was finished, retryCount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v1}, Lcom/sec/chaton/service/BackGroundRegiService;->d(Lcom/sec/chaton/service/BackGroundRegiService;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0}, Lcom/sec/chaton/service/BackGroundRegiService;->e(Lcom/sec/chaton/service/BackGroundRegiService;)Z

    move-result v0

    if-ne v0, v4, :cond_7

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0}, Lcom/sec/chaton/service/BackGroundRegiService;->f(Lcom/sec/chaton/service/BackGroundRegiService;)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_6

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v4}, Lcom/sec/chaton/service/BackGroundRegiService;->a(Lcom/sec/chaton/service/BackGroundRegiService;Z)V

    goto/16 :goto_0

    .line 271
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->a(Lcom/sec/chaton/service/BackGroundRegiService;I)I

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Lcom/sec/chaton/service/BackGroundRegiService;Z)V

    goto/16 :goto_0

    .line 275
    :cond_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 276
    const-string v0, "it was NOT supported auto SSO Registration"

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Lcom/sec/chaton/service/BackGroundRegiService;Z)V

    goto/16 :goto_0

    .line 282
    :cond_9
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_a

    .line 283
    const-string v0, "SSO token validaion was failed"

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0, v3}, Lcom/sec/chaton/service/BackGroundRegiService;->b(Lcom/sec/chaton/service/BackGroundRegiService;Z)V

    goto/16 :goto_0

    .line 291
    :sswitch_2
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0}, Lcom/sec/chaton/service/BackGroundRegiService;->g(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/a/ak;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0}, Lcom/sec/chaton/service/BackGroundRegiService;->g(Lcom/sec/chaton/service/BackGroundRegiService;)Lcom/sec/chaton/d/a/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/ak;->i()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 295
    const-string v0, "ChatON deregistration is success."

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-virtual {v1}, Lcom/sec/chaton/service/BackGroundRegiService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b029c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 298
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/global/GlobalApplication;->a(Landroid/content/Context;)V

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-static {v0}, Lcom/sec/chaton/service/BackGroundRegiService;->h(Lcom/sec/chaton/service/BackGroundRegiService;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 300
    const-string v0, "delete_account"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 311
    :cond_b
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/service/b;->a:Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-virtual {v0}, Lcom/sec/chaton/service/BackGroundRegiService;->stopSelf()V

    goto/16 :goto_0

    .line 308
    :cond_c
    const-string v0, "ChatON deregistration is fail."

    invoke-static {}, Lcom/sec/chaton/service/BackGroundRegiService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const-string v0, "back_deregi_fail"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    .line 225
    nop

    :sswitch_data_0
    .sparse-switch
        0x68 -> :sswitch_0
        0xca -> :sswitch_2
        0x7d6 -> :sswitch_1
    .end sparse-switch
.end method
