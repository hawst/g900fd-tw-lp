.class public Lcom/sec/chaton/service/PushRegistrationService;
.super Landroid/app/Service;
.source "PushRegistrationService.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private c:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/service/PushRegistrationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/service/PushRegistrationService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/service/PushRegistrationService;->b:Z

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/chaton/service/PushRegistrationService;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/service/PushRegistrationService;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/chaton/service/PushRegistrationService;->b()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/service/PushRegistrationService;Z)Z
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sec/chaton/service/PushRegistrationService;->b:Z

    return p1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 113
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/service/h;

    iget-object v2, p0, Lcom/sec/chaton/service/PushRegistrationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/service/h;-><init>(Lcom/sec/chaton/service/PushRegistrationService;Landroid/os/Looper;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->a(Landroid/os/Handler;)V

    .line 137
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/service/PushRegistrationService;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/chaton/service/PushRegistrationService;->c()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 140
    new-instance v0, Lcom/sec/chaton/service/i;

    iget-object v1, p0, Lcom/sec/chaton/service/PushRegistrationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/service/i;-><init>(Lcom/sec/chaton/service/PushRegistrationService;Landroid/os/Looper;)V

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->a()V

    .line 161
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/service/PushRegistrationService;->b:Z

    .line 44
    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lcom/sec/chaton/service/PushRegistrationService;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/service/PushRegistrationService;->c:Landroid/os/HandlerThread;

    .line 45
    iget-object v0, p0, Lcom/sec/chaton/service/PushRegistrationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 46
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 54
    iget-boolean v0, p0, Lcom/sec/chaton/service/PushRegistrationService;->b:Z

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "Push registration is processing."

    sget-object v1, Lcom/sec/chaton/service/PushRegistrationService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/service/PushRegistrationService;->stopSelf()V

    .line 105
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    const-string v0, "Couldn\'t find ChatON user."

    sget-object v1, Lcom/sec/chaton/service/PushRegistrationService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/service/PushRegistrationService;->stopSelf()V

    goto :goto_0

    .line 69
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/service/PushRegistrationService;->b:Z

    .line 71
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/service/g;

    iget-object v2, p0, Lcom/sec/chaton/service/PushRegistrationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/service/g;-><init>(Lcom/sec/chaton/service/PushRegistrationService;Landroid/os/Looper;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->d(Landroid/os/Handler;)V

    goto :goto_0
.end method
