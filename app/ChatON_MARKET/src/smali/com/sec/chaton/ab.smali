.class Lcom/sec/chaton/ab;
.super Ljava/lang/Object;
.source "PlusFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/PlusFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/PlusFragment;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/chaton/ab;->a:Lcom/sec/chaton/PlusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/chaton/ab;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->b(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/ak;

    iget v1, v0, Lcom/sec/chaton/ak;->b:I

    .line 270
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 271
    sparse-switch v1, :sswitch_data_0

    .line 286
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/ab;->a:Lcom/sec/chaton/PlusFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/PlusFragment;->startActivity(Landroid/content/Intent;)V

    .line 287
    return-void

    .line 274
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/ab;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityDownloads;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 278
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/ab;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 281
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/ab;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/specialbuddy/ChatONLiveEventActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 271
    :sswitch_data_0
    .sparse-switch
        0x7f0b027a -> :sswitch_2
        0x7f0b030d -> :sswitch_0
        0x7f0b03c2 -> :sswitch_1
    .end sparse-switch
.end method
