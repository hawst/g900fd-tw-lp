.class Lcom/sec/chaton/r;
.super Ljava/lang/Object;
.source "ExitAppDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/ExitAppDialogActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/ExitAppDialogActivity;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->a(Lcom/sec/chaton/ExitAppDialogActivity;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v1}, Lcom/sec/chaton/ExitAppDialogActivity;->a(Lcom/sec/chaton/ExitAppDialogActivity;)I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v1}, Lcom/sec/chaton/ExitAppDialogActivity;->a(Lcom/sec/chaton/ExitAppDialogActivity;)I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v1}, Lcom/sec/chaton/ExitAppDialogActivity;->a(Lcom/sec/chaton/ExitAppDialogActivity;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    const-string v1, "PASSWORD_LOCK"

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/ExitAppDialogActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 100
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 101
    const-string v1, "LOCK_STATE"

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 102
    const-string v1, "PASSWORD"

    const-string v2, "0000"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 103
    const-string v1, "PASSWORD_HINT"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 104
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 108
    new-instance v0, Lcom/sec/chaton/s;

    iget-object v1, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/s;-><init>(Lcom/sec/chaton/ExitAppDialogActivity;Lcom/sec/chaton/r;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/s;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 117
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "exit_app_done"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->finish()V

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/r;->a:Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-static {v0}, Lcom/sec/chaton/global/GlobalApplication;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
