.class Lcom/sec/chaton/localbackup/a/b;
.super Landroid/os/Handler;
.source "BackupFileDownloadHelperTask.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/a/a;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/a/a;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 156
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 158
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 160
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/a;->a(Lcom/sec/chaton/localbackup/a/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file progress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/localbackup/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/a/a;->b(Lcom/sec/chaton/localbackup/a/a;)Lcom/sec/chaton/widget/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/c;->a(I)V

    goto :goto_0

    .line 171
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/a;->b(Lcom/sec/chaton/localbackup/a/a;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/a;->b(Lcom/sec/chaton/localbackup/a/a;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 175
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/common/util/a/g;

    .line 177
    invoke-virtual {v0}, Lcom/sec/common/util/a/g;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/a/d;

    .line 181
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 182
    const-string v2, "message_download_uri"

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/d;->a(Lcom/sec/chaton/localbackup/a/d;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v2, "message_type"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 185
    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/d;->b(Lcom/sec/chaton/localbackup/a/d;)Lcom/sec/chaton/e/w;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    if-ne v2, v3, :cond_2

    .line 186
    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/d;->a(Lcom/sec/chaton/localbackup/a/d;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/r;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 187
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 188
    const-string v3, "message_formatted"

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v2}, Lcom/sec/chaton/localbackup/a/a;->c(Lcom/sec/chaton/localbackup/a/a;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/localbackup/database/a;->b()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "message_sever_id=? AND message_inbox_no=? AND message_sender=?"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/d;->c(Lcom/sec/chaton/localbackup/a/d;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/d;->d(Lcom/sec/chaton/localbackup/a/d;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/d;->e(Lcom/sec/chaton/localbackup/a/d;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/a;->d(Lcom/sec/chaton/localbackup/a/a;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 198
    iget v1, p1, Landroid/os/Message;->what:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 199
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 200
    iget-object v1, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/a/a;->d(Lcom/sec/chaton/localbackup/a/a;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 208
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/a;->d(Lcom/sec/chaton/localbackup/a/a;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 209
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 210
    iget v1, p1, Landroid/os/Message;->what:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 211
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 212
    iget-object v1, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/a/a;->d(Lcom/sec/chaton/localbackup/a/a;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 215
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/a;->b(Lcom/sec/chaton/localbackup/a/a;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/b;->a:Lcom/sec/chaton/localbackup/a/a;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/a/a;->b(Lcom/sec/chaton/localbackup/a/a;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 218
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b5

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
