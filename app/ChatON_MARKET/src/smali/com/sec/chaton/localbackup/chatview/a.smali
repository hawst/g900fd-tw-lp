.class public Lcom/sec/chaton/localbackup/chatview/a;
.super Ljava/lang/Object;
.source "BackupMediaPlayerManager.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/sec/chaton/localbackup/chatview/a;


# instance fields
.field private c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/sec/chaton/localbackup/chatview/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/chaton/localbackup/chatview/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/chatview/a;->a:Ljava/lang/String;

    .line 12
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/a;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/chatview/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/localbackup/chatview/a;->b:Lcom/sec/chaton/localbackup/chatview/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 16
    return-void
.end method

.method public static a()Lcom/sec/chaton/localbackup/chatview/a;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/chaton/localbackup/chatview/a;->b:Lcom/sec/chaton/localbackup/chatview/a;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 50
    sget-object v1, Lcom/sec/chaton/localbackup/chatview/a;->a:Ljava/lang/String;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/b;

    .line 53
    if-eqz v0, :cond_0

    .line 54
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/chaton/localbackup/chatview/b;->cancel(Z)Z

    goto :goto_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 57
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 58
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;JLandroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    invoke-virtual {p0, p2, p3}, Lcom/sec/chaton/localbackup/chatview/a;->a(J)Z

    move-result v0

    .line 26
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/chatview/a;->d()V

    .line 28
    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/b;

    invoke-direct {v0, p4, p1, p2, p3}, Lcom/sec/chaton/localbackup/chatview/b;-><init>(Landroid/view/View;Ljava/lang/String;J)V

    .line 30
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_1

    .line 33
    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/chatview/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/chatview/b;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public a(J)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/b;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/b;->c()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/b;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v2, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public b(J)Lcom/sec/chaton/localbackup/chatview/b;
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/b;

    .line 74
    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/b;->c()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 80
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 43
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/chatview/a;->d()V

    .line 47
    return-void
.end method
