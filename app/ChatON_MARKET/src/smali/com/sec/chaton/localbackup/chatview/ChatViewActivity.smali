.class public Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ChatViewActivity.java"


# instance fields
.field private a:Lcom/sec/chaton/localbackup/chatview/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 13
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->a:Lcom/sec/chaton/localbackup/chatview/c;

    .line 21
    return-object v0
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->finish()V

    .line 26
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->a:Lcom/sec/chaton/localbackup/chatview/c;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->a:Lcom/sec/chaton/localbackup/chatview/c;

    invoke-interface {v0}, Lcom/sec/chaton/localbackup/chatview/c;->a()V

    .line 33
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onBackPressed()V

    .line 34
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->a(Landroid/app/Activity;)V

    .line 51
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 40
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-static {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->a(Landroid/app/Activity;)V

    .line 43
    :cond_0
    return-void
.end method
