.class public Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;
.super Lcom/sec/chaton/multimedia/image/ImagePagerFragment;
.source "BackupImagePagerFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 14
    return-void
.end method

.method public b()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 17
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 18
    new-instance v0, Lcom/sec/chaton/localbackup/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->b:J

    iget-object v6, p0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->d:Ljava/lang/String;

    sget-object v7, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->g()Landroid/os/Handler;

    move-result-object v9

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/localbackup/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;Landroid/os/Handler;Z)V

    .line 20
    new-array v1, v10, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 21
    return-void
.end method

.method protected c()V
    .locals 11

    .prologue
    .line 25
    new-instance v0, Lcom/sec/chaton/localbackup/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->b:J

    iget-object v6, p0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->d:Ljava/lang/String;

    sget-object v7, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->g()Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/localbackup/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;Landroid/os/Handler;Z)V

    .line 27
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 28
    return-void
.end method
