.class public Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;
.super Landroid/support/v4/app/Fragment;
.source "ChatListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/e/a/v;

.field private c:Lcom/sec/chaton/e/a/u;

.field private d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

.field private e:Lcom/sec/chaton/localbackup/chatlist/a;

.field private f:Landroid/widget/ListView;

.field private g:Ljava/lang/String;

.field private h:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 61
    new-instance v0, Lcom/sec/chaton/localbackup/chatlist/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/localbackup/chatlist/d;-><init>(Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->a:Lcom/sec/chaton/e/a/v;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->c()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;)Lcom/sec/chaton/localbackup/chatlist/a;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->e:Lcom/sec/chaton/localbackup/chatlist/a;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->h:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->h:Landroid/app/ProgressDialog;

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 169
    :cond_1
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 175
    :cond_0
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 117
    move-object v0, p1

    check-cast v0, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    .line 118
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 119
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->a:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->c:Lcom/sec/chaton/e/a/u;

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->setHasOptionsMenu(Z)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/sec/chaton/localbackup/BackupListView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->g:Ljava/lang/String;

    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 51
    const v0, 0x7f0300b9

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 52
    new-instance v0, Lcom/sec/chaton/localbackup/chatlist/a;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f030125

    invoke-direct {v0, v3, v4, v2}, Lcom/sec/chaton/localbackup/chatlist/a;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->e:Lcom/sec/chaton/localbackup/chatlist/a;

    .line 53
    const v0, 0x7f0700fd

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->f:Landroid/widget/ListView;

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->f:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->e:Lcom/sec/chaton/localbackup/chatlist/a;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->c:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/localbackup/database/a;->a()Landroid/net/Uri;

    move-result-object v3

    new-array v6, v1, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v6, v5

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->b()V

    .line 58
    return-object v8
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->c()V

    .line 111
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 112
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    .line 124
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 125
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 129
    const/4 v0, 0x0

    .line 130
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->e:Lcom/sec/chaton/localbackup/chatlist/a;

    if-eqz v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->e:Lcom/sec/chaton/localbackup/chatlist/a;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/localbackup/chatlist/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 134
    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 136
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    const-class v3, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    const-string v2, "inbox_no"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 138
    const-string v3, "inbox_chat_type"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 140
    const-string v4, "inboxNO"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v2, "chatType"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 142
    const-string v2, "inbox_title"

    const-string v3, "inbox_title"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string v2, "inbox_participants"

    const-string v3, "inbox_participants"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 145
    invoke-virtual {p0, v1}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->startActivity(Landroid/content/Intent;)V

    .line 148
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 152
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->d:Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;->c()V

    .line 156
    :cond_0
    const/4 v0, 0x1

    .line 158
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 105
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 106
    return-void
.end method
