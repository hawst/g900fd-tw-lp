.class public Lcom/sec/chaton/localbackup/BackupListView;
.super Landroid/support/v4/app/Fragment;
.source "BackupListView.java"

# interfaces
.implements Lcom/sec/chaton/localbackup/b;


# static fields
.field public static a:Ljava/lang/String;

.field private static final f:Ljava/lang/String;


# instance fields
.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/localbackup/m;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/sec/chaton/localbackup/o;

.field d:Landroid/widget/ListView;

.field e:Landroid/widget/TextView;

.field private g:Lcom/sec/chaton/localbackup/n;

.field private h:Landroid/widget/CheckedTextView;

.field private i:Landroid/view/Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/chaton/localbackup/BackupListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/BackupListView;->f:Ljava/lang/String;

    .line 42
    const-string v0, "backup_file_name"

    sput-object v0, Lcom/sec/chaton/localbackup/BackupListView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 190
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/BackupListView;)Lcom/sec/chaton/localbackup/n;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->g:Lcom/sec/chaton/localbackup/n;

    return-object v0
.end method

.method private a(Ljava/lang/Long;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 229
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 230
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 231
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd hh:mm:ss a"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/BackupListView;Lcom/sec/chaton/localbackup/n;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/n;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/BackupListView;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/BackupListView;->a(Z)V

    return-void
.end method

.method private a(Lcom/sec/chaton/localbackup/n;)V
    .locals 7

    .prologue
    const v6, 0x7f070590

    const v5, 0x7f07058e

    const v2, 0x7f07058f

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 416
    iput-object p1, p0, Lcom/sec/chaton/localbackup/BackupListView;->g:Lcom/sec/chaton/localbackup/n;

    .line 418
    sget-object v0, Lcom/sec/chaton/localbackup/l;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/localbackup/n;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 470
    :cond_0
    :goto_0
    return-void

    .line 420
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v5, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v6, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 431
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 432
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/ActivityBackupList;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0332

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 435
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    if-eqz v0, :cond_3

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    .line 439
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_4

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 442
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    if-eqz v0, :cond_5

    .line 443
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 446
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->c:Lcom/sec/chaton/localbackup/o;

    iput-boolean v3, v0, Lcom/sec/chaton/localbackup/o;->a:Z

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_0

    .line 429
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 455
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    if-eqz v0, :cond_7

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v5, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 458
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v6, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    const v1, 0x7f070592

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 460
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    const v1, 0x7f070592

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 463
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/ActivityBackupList;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 466
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->c:Lcom/sec/chaton/localbackup/o;

    iput-boolean v4, v0, Lcom/sec/chaton/localbackup/o;->a:Z

    .line 467
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto/16 :goto_0

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v1, v0, p1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private b()V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 206
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/chaton/localbackup/q;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    if-eqz v1, :cond_2

    .line 209
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 210
    if-eqz v1, :cond_1

    .line 212
    iget-object v2, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 213
    iget-object v2, p0, Lcom/sec/chaton/localbackup/BackupListView;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 214
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 215
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".crypt"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 216
    iget-object v4, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/chaton/localbackup/m;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/chaton/localbackup/BackupListView;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v6, v7, v8, v3}, Lcom/sec/chaton/localbackup/m;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    .line 342
    if-lez v0, :cond_0

    .line 343
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    const v2, 0x7f070592

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 353
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const v7, 0x7f0b0167

    const/4 v2, 0x0

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    .line 361
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 363
    :goto_0
    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 364
    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/m;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 369
    :cond_1
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v1, v2

    .line 371
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/m;

    .line 373
    iget-object v5, v0, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 374
    iget-object v3, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 377
    :try_start_0
    new-instance v3, Ljava/io/File;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 379
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v7, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 371
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 381
    :catch_0
    move-exception v0

    .line 383
    :try_start_1
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_4

    .line 384
    sget-object v3, Lcom/sec/chaton/localbackup/BackupListView;->f:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v7, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v7, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    throw v0

    .line 400
    :cond_5
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    const v5, 0x7f070590

    const v4, 0x7f07058e

    const v3, 0x7f07058f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 238
    const v0, 0x7f0f0011

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 240
    iput-object p1, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    .line 243
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v4, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v5, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    const v1, 0x7f070592

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    const v1, 0x7f070592

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 263
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 264
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v4, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v5, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 260
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 69
    const v0, 0x7f030067

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 72
    const v0, 0x7f070296

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    .line 74
    const v0, 0x7f070294

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    .line 76
    const v0, 0x7f070297

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->e:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f070295

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 79
    const v0, 0x7f07014c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 80
    sget-object v3, Lcom/sec/chaton/localbackup/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 86
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/BackupListView;->b()V

    .line 88
    new-instance v0, Lcom/sec/chaton/localbackup/o;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v3, v4}, Lcom/sec/chaton/localbackup/o;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->c:Lcom/sec/chaton/localbackup/o;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/chaton/localbackup/BackupListView;->c:Lcom/sec/chaton/localbackup/o;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/chaton/localbackup/i;

    invoke-direct {v3, p0}, Lcom/sec/chaton/localbackup/i;-><init>(Lcom/sec/chaton/localbackup/BackupListView;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    new-instance v3, Lcom/sec/chaton/localbackup/j;

    invoke-direct {v3, p0}, Lcom/sec/chaton/localbackup/j;-><init>(Lcom/sec/chaton/localbackup/BackupListView;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/BackupListView;->setHasOptionsMenu(Z)V

    .line 170
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const v0, 0x7f070416

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 172
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 173
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 174
    sget-object v0, Lcom/sec/chaton/localbackup/n;->b:Lcom/sec/chaton/localbackup/n;

    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/n;)V

    .line 175
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/BackupListView;->c()V

    .line 180
    :goto_0
    return-object v1

    .line 177
    :cond_0
    sget-object v0, Lcom/sec/chaton/localbackup/n;->a:Lcom/sec/chaton/localbackup/n;

    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/n;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 480
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 481
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 275
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 331
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 278
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/localbackup/n;->b:Lcom/sec/chaton/localbackup/n;

    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/n;)V

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    .line 282
    if-lez v0, :cond_1

    .line 283
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    if-eqz v1, :cond_1

    .line 284
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->i:Landroid/view/Menu;

    const v2, 0x7f070592

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 286
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/localbackup/BackupListView;->h:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 296
    :pswitch_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 297
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 299
    :cond_2
    sget-object v0, Lcom/sec/chaton/localbackup/n;->a:Lcom/sec/chaton/localbackup/n;

    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/n;)V

    goto :goto_0

    .line 305
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0335

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/localbackup/k;

    invoke-direct {v2, p0}, Lcom/sec/chaton/localbackup/k;-><init>(Lcom/sec/chaton/localbackup/BackupListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 275
    :pswitch_data_0
    .packed-switch 0x7f07058f
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 268
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 270
    return-void
.end method

.method public switchViewMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 495
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 505
    :goto_0
    return v0

    .line 500
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/localbackup/BackupListView;->g:Lcom/sec/chaton/localbackup/n;

    sget-object v2, Lcom/sec/chaton/localbackup/n;->b:Lcom/sec/chaton/localbackup/n;

    if-ne v1, v2, :cond_1

    .line 501
    sget-object v1, Lcom/sec/chaton/localbackup/n;->a:Lcom/sec/chaton/localbackup/n;

    invoke-direct {p0, v1}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/n;)V

    goto :goto_0

    .line 505
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
