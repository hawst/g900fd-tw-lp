.class Lcom/sec/chaton/localbackup/chatlist/d;
.super Ljava/lang/Object;
.source "ChatListFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatlist/d;->a:Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/d;->a:Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->a(Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;)V

    .line 67
    if-eqz p3, :cond_1

    .line 68
    const/4 v1, 0x0

    .line 70
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/d;->a:Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;->b(Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;)Lcom/sec/chaton/localbackup/chatlist/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/chaton/localbackup/chatlist/a;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 76
    if-eqz v0, :cond_1

    .line 77
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 83
    :cond_1
    return-void

    .line 76
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 77
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 76
    :cond_2
    throw v0
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
