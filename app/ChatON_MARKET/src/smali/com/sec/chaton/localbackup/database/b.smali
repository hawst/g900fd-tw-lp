.class public Lcom/sec/chaton/localbackup/database/b;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "BackupDBHelper.java"


# static fields
.field private static a:Lcom/sec/chaton/localbackup/database/b;


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    .line 29
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "chaton_local_backup.db"

    const/4 v2, 0x0

    const/16 v3, 0x55

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 30
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/localbackup/database/b;
    .locals 2

    .prologue
    .line 21
    const-class v1, Lcom/sec/chaton/localbackup/database/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/localbackup/database/b;->a:Lcom/sec/chaton/localbackup/database/b;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/sec/chaton/localbackup/database/b;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/database/b;-><init>()V

    sput-object v0, Lcom/sec/chaton/localbackup/database/b;->a:Lcom/sec/chaton/localbackup/database/b;

    .line 25
    :cond_0
    sget-object v0, Lcom/sec/chaton/localbackup/database/b;->a:Lcom/sec/chaton/localbackup/database/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 47
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 48
    const-string v1, "message_download_uri"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/sec/chaton/localbackup/database/b;->a()Lcom/sec/chaton/localbackup/database/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/database/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 50
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/database/sqlite/SQLiteOpenHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 44
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method
