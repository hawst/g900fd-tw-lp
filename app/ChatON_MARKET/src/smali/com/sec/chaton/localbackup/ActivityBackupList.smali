.class public Lcom/sec/chaton/localbackup/ActivityBackupList;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityBackupList.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/BroadcastReceiver;

.field private c:Lcom/sec/chaton/localbackup/BackupListView;

.field private d:Lcom/sec/chaton/localbackup/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/chaton/localbackup/ActivityBackupList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/ActivityBackupList;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/ActivityBackupList;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->f()V

    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/chaton/localbackup/ActivityBackupList;->a:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lcom/sec/chaton/localbackup/a;

    invoke-direct {v0, p0}, Lcom/sec/chaton/localbackup/a;-><init>(Lcom/sec/chaton/localbackup/ActivityBackupList;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->b:Landroid/content/BroadcastReceiver;

    .line 89
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 90
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 92
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 93
    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 94
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->f()V

    .line 95
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 100
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->finish()V

    .line 103
    :cond_0
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 114
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/sec/chaton/localbackup/BackupListView;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/BackupListView;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->c:Lcom/sec/chaton/localbackup/BackupListView;

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->c:Lcom/sec/chaton/localbackup/BackupListView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->d:Lcom/sec/chaton/localbackup/b;

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->c:Lcom/sec/chaton/localbackup/BackupListView;

    return-object v0
.end method

.method public c()Lcom/sec/common/actionbar/a;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->d:Lcom/sec/chaton/localbackup/b;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->d:Lcom/sec/chaton/localbackup/b;

    invoke-interface {v0}, Lcom/sec/chaton/localbackup/b;->switchViewMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onBackPressed()V

    .line 51
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 76
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->a(Landroid/app/Activity;)V

    .line 79
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    if-eqz p1, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/b;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityBackupList;->d:Lcom/sec/chaton/localbackup/b;

    .line 37
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 108
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPause()V

    .line 109
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->g()V

    .line 110
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->e()V

    .line 66
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-static {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->a(Landroid/app/Activity;)V

    .line 69
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 70
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 124
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 131
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 127
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->finish()V

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
