.class public Lcom/sec/chaton/localbackup/a/a;
.super Landroid/os/AsyncTask;
.source "BackupFileDownloadHelperTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:Ljava/lang/String;

.field private g:Lcom/sec/chaton/e/w;

.field private h:Lcom/sec/chaton/widget/c;

.field private i:Landroid/os/Handler;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Landroid/view/View;

.field private m:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/chaton/localbackup/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;Landroid/os/Handler;Z)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 152
    new-instance v0, Lcom/sec/chaton/localbackup/a/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/localbackup/a/b;-><init>(Lcom/sec/chaton/localbackup/a/a;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->m:Landroid/os/Handler;

    .line 52
    iput-object p1, p0, Lcom/sec/chaton/localbackup/a/a;->b:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lcom/sec/chaton/localbackup/a/a;->c:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lcom/sec/chaton/localbackup/a/a;->d:Ljava/lang/String;

    .line 55
    iput-wide p4, p0, Lcom/sec/chaton/localbackup/a/a;->e:J

    .line 56
    iput-object p6, p0, Lcom/sec/chaton/localbackup/a/a;->f:Ljava/lang/String;

    .line 57
    iput-object p7, p0, Lcom/sec/chaton/localbackup/a/a;->g:Lcom/sec/chaton/e/w;

    .line 58
    iput-object p9, p0, Lcom/sec/chaton/localbackup/a/a;->i:Landroid/os/Handler;

    .line 59
    iput-boolean p10, p0, Lcom/sec/chaton/localbackup/a/a;->j:Z

    .line 60
    iput-object p8, p0, Lcom/sec/chaton/localbackup/a/a;->l:Landroid/view/View;

    .line 61
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/chaton/localbackup/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/a/a;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/a/a;->j:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/a/a;)Lcom/sec/chaton/widget/c;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lcom/sec/chaton/widget/c;
    .locals 4

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Lcom/sec/chaton/widget/c;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/a/a;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/chaton/widget/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b0226

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setTitle(I)V

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/a/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setMessage(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setCancelable(Z)V

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    const/4 v1, -0x2

    iget-object v2, p0, Lcom/sec/chaton/localbackup/a/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/localbackup/a/c;

    invoke-direct {v3, p0, p1}, Lcom/sec/chaton/localbackup/a/c;-><init>(Lcom/sec/chaton/localbackup/a/a;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/widget/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/localbackup/a/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/localbackup/a/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->i:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/localbackup/a/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->m:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 66
    .line 68
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mContnet :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/localbackup/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/localbackup/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 112
    :goto_0
    return-object v0

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->c:Ljava/lang/String;

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 78
    const-string v2, ""

    .line 79
    const-string v2, ""

    .line 81
    if-eqz v0, :cond_2

    array-length v2, v0

    if-gt v2, v4, :cond_3

    :cond_2
    move-object v0, v1

    .line 82
    goto :goto_0

    .line 85
    :cond_3
    const/4 v2, 0x2

    aget-object v2, v0, v2

    .line 86
    const/4 v3, 0x3

    aget-object v3, v0, v3

    iput-object v3, p0, Lcom/sec/chaton/localbackup/a/a;->k:Ljava/lang/String;

    .line 87
    aget-object v0, v0, v4

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "imei="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&buddyid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&filename="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/localbackup/a/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 92
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "filedownLoadUrl:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/localbackup/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_4
    const-string v0, ""

    .line 98
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v4

    .line 100
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 101
    new-instance v1, Lcom/sec/chaton/util/a;

    invoke-static {v4}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v5

    invoke-static {v4}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v1, v5, v4}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 106
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 112
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&param="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    .line 103
    goto/16 :goto_0

    .line 108
    :catch_0
    move-exception v1

    .line 109
    sget-object v3, Lcom/sec/chaton/localbackup/a/a;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 119
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b5

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 148
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/a/a;->j:Z

    if-eqz v0, :cond_1

    .line 126
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/a/a;->b(Ljava/lang/String;)Lcom/sec/chaton/widget/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->h:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->show()V

    .line 131
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/a;->b:Landroid/content/Context;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 136
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "localBackup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    new-instance v8, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/a/a;->k:Ljava/lang/String;

    invoke-direct {v8, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v0, Lcom/sec/chaton/localbackup/a/d;

    iget-wide v1, p0, Lcom/sec/chaton/localbackup/a/a;->e:J

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/localbackup/a/a;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/localbackup/a/a;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/localbackup/a/a;->g:Lcom/sec/chaton/e/w;

    iget-object v7, p0, Lcom/sec/chaton/localbackup/a/a;->l:Landroid/view/View;

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/localbackup/a/d;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;)V

    .line 145
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/localbackup/a/a;->m:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v8, v0}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    .line 147
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/a/a;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/a/a;->a(Ljava/lang/String;)V

    return-void
.end method
