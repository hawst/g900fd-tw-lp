.class public Lcom/sec/chaton/localbackup/o;
.super Landroid/widget/ArrayAdapter;
.source "BackuplistAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/localbackup/m;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Z

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/localbackup/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 26
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/o;->b:Landroid/view/LayoutInflater;

    .line 28
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 36
    if-nez p2, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/o;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030123

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 39
    new-instance v1, Lcom/sec/chaton/localbackup/p;

    invoke-direct {v1}, Lcom/sec/chaton/localbackup/p;-><init>()V

    .line 40
    const v0, 0x7f07014c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/localbackup/p;->a:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f07014d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/localbackup/p;->b:Landroid/widget/TextView;

    .line 42
    const v0, 0x1020001

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/sec/chaton/localbackup/p;->c:Landroid/widget/CheckBox;

    .line 43
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v1

    .line 48
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/o;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/m;

    .line 50
    iget-object v1, v0, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    iput-object v1, v3, Lcom/sec/chaton/localbackup/p;->d:Ljava/lang/String;

    .line 51
    iget-object v1, v0, Lcom/sec/chaton/localbackup/m;->c:Ljava/lang/String;

    iput-object v1, v3, Lcom/sec/chaton/localbackup/p;->e:Ljava/lang/String;

    .line 52
    iget-object v1, v0, Lcom/sec/chaton/localbackup/m;->d:Ljava/lang/String;

    iput-object v1, v3, Lcom/sec/chaton/localbackup/p;->f:Ljava/lang/String;

    .line 53
    iget-object v1, v0, Lcom/sec/chaton/localbackup/m;->b:Ljava/lang/String;

    iput-object v1, v3, Lcom/sec/chaton/localbackup/p;->g:Ljava/lang/String;

    .line 55
    iget-boolean v1, p0, Lcom/sec/chaton/localbackup/o;->a:Z

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 56
    check-cast v1, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 62
    :goto_1
    iget-object v1, v3, Lcom/sec/chaton/localbackup/p;->a:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/sec/chaton/localbackup/m;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v1, v3, Lcom/sec/chaton/localbackup/p;->b:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/m;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    return-object v2

    .line 45
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/p;

    move-object v3, v0

    move-object v2, p2

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 58
    check-cast v1, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    goto :goto_1
.end method
