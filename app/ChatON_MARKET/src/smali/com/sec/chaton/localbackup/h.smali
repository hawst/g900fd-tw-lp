.class Lcom/sec/chaton/localbackup/h;
.super Landroid/os/AsyncTask;
.source "ActivityLocalBackup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/sec/chaton/localbackup/q;

.field final synthetic b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V
    .locals 1

    .prologue
    .line 331
    iput-object p1, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 333
    new-instance v0, Lcom/sec/chaton/localbackup/q;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/q;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/h;->a:Lcom/sec/chaton/localbackup/q;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Lcom/sec/chaton/localbackup/c;)V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/h;-><init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->a:Lcom/sec/chaton/localbackup/q;

    sget-object v1, Lcom/sec/chaton/localbackup/s;->b:Lcom/sec/chaton/localbackup/s;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/s;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/sec/chaton/localbackup/h;->a:Lcom/sec/chaton/localbackup/q;

    const-string v2, "local-backup.db.crypt"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/localbackup/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 350
    iget-object v2, p0, Lcom/sec/chaton/localbackup/h;->a:Lcom/sec/chaton/localbackup/q;

    sget-object v3, Lcom/sec/chaton/localbackup/t;->a:Lcom/sec/chaton/localbackup/t;

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 359
    :goto_0
    return-object v0

    .line 353
    :catch_0
    move-exception v0

    .line 355
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 356
    invoke-static {}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 359
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 365
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->f(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 375
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[File encryption] complete "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a()V

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->h(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b033e

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Z)Z

    .line 387
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Lcom/sec/chaton/localbackup/h;)Lcom/sec/chaton/localbackup/h;

    goto :goto_0

    .line 382
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0338

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 385
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0339

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 331
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/h;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/h;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 338
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/localbackup/h;->b:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    .line 341
    return-void
.end method
