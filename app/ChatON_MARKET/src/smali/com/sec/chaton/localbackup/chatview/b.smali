.class public Lcom/sec/chaton/localbackup/chatview/b;
.super Lcom/sec/chaton/multimedia/audio/h;
.source "BackupPlayVoiceTask.java"


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 13
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/audio/h;-><init>(Landroid/view/View;Ljava/lang/String;JZ)V

    .line 14
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 5

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/f;

    .line 22
    iget-wide v1, v0, Lcom/sec/chaton/localbackup/chatview/f;->i:J

    iput-wide v1, p0, Lcom/sec/chaton/localbackup/chatview/b;->i:J

    .line 23
    iget-wide v1, p0, Lcom/sec/chaton/localbackup/chatview/b;->i:J

    iget-wide v3, p0, Lcom/sec/chaton/localbackup/chatview/b;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 25
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatview/f;->a:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/chaton/localbackup/chatview/b;->c:Landroid/widget/ImageView;

    .line 26
    iget-object v0, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->d:Landroid/widget/TextView;

    .line 28
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->c:Landroid/widget/ImageView;

    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 29
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->h:Landroid/graphics/drawable/AnimationDrawable;

    .line 30
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->h:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 35
    :cond_0
    return-void
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 40
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/f;

    .line 43
    iget-wide v0, v0, Lcom/sec/chaton/localbackup/chatview/f;->i:J

    iput-wide v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->i:J

    .line 44
    iget-wide v0, p0, Lcom/sec/chaton/localbackup/chatview/b;->i:J

    iget-wide v2, p0, Lcom/sec/chaton/localbackup/chatview/b;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 45
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, 0xea60

    div-int/2addr v0, v1

    .line 46
    aget-object v1, p1, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    rem-int/lit8 v1, v1, 0x3c

    .line 48
    const-string v2, "%d:%02d/%d:%02d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    iget v1, p0, Lcom/sec/chaton/localbackup/chatview/b;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x3

    iget v1, p0, Lcom/sec/chaton/localbackup/chatview/b;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/b;->d:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/b;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/a;->a()Lcom/sec/chaton/localbackup/chatview/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/a;->b()V

    .line 59
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/chatview/b;->a([Ljava/lang/Integer;)V

    return-void
.end method
