.class public Lcom/sec/chaton/localbackup/ActivityLocalBackup;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "ActivityLocalBackup.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/util/ab;

.field private c:Landroid/content/Context;

.field private d:Landroid/preference/CheckBoxPreference;

.field private e:Landroid/preference/Preference;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/app/Dialog;

.field private h:Z

.field private i:Z

.field private j:Landroid/app/Dialog;

.field private k:Lcom/sec/chaton/localbackup/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    .line 331
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Lcom/sec/chaton/localbackup/h;)Lcom/sec/chaton/localbackup/h;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->k:Lcom/sec/chaton/localbackup/h;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d()V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 103
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 105
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 107
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const v5, 0x7f08001f

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 114
    const-string v0, "pref_auto_backup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/localbackup/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/localbackup/c;-><init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 146
    const-string v0, "pref_backup_now"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/localbackup/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/localbackup/d;-><init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 165
    const-string v0, "pref_setting_backup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->f:Landroid/preference/Preference;

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->f:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/localbackup/e;

    invoke-direct {v1, p0}, Lcom/sec/chaton/localbackup/e;-><init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 180
    const-string v0, "pref_view_backedup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 181
    new-instance v1, Lcom/sec/chaton/localbackup/f;

    invoke-direct {v1, p0}, Lcom/sec/chaton/localbackup/f;-><init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    const-string v1, "auto_backup_on"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 204
    const v0, 0x7f0b0333

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 205
    const v0, 0x7f0b0336

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    const-string v1, "setting_backup_enable"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 210
    invoke-virtual {p0, v4, v3}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(ZZ)V

    .line 216
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    const v2, 0x7f0b0344

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 219
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a()V

    .line 220
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->f:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0341

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->f()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->j:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e8

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b033a

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/localbackup/g;

    invoke-direct {v2, p0}, Lcom/sec/chaton/localbackup/g;-><init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->j:Landroid/app/Dialog;

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->j:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 274
    :cond_1
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g:Landroid/app/Dialog;

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 315
    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e()V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 394
    new-instance v0, Lcom/sec/chaton/localbackup/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/localbackup/h;-><init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Lcom/sec/chaton/localbackup/c;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->k:Lcom/sec/chaton/localbackup/h;

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[File encryption] start "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->k:Lcom/sec/chaton/localbackup/h;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 403
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->k:Lcom/sec/chaton/localbackup/h;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/h;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->h:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->g:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->i:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0334

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "backup_last_modified"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 226
    :cond_0
    return-void
.end method

.method public a(ZZ)V
    .locals 5

    .prologue
    const v4, 0x7f08003f

    const/4 v3, 0x0

    .line 230
    if-eqz p1, :cond_1

    .line 232
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/localbackup/ActivitySecretKey;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    const-string v1, "password_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 234
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->startActivityForResult(Landroid/content/Intent;I)V

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    const-string v1, "setting_backup_enable"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 240
    const v0, 0x7f0b0333

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    const-string v1, "auto_backup_on"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 243
    const v0, 0x7f0b0336

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 244
    if-nez p2, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const v4, 0x7f08001f

    const/4 v3, 0x1

    .line 279
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 281
    packed-switch p1, :pswitch_data_0

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 283
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    const-string v1, "setting_backup_enable"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 291
    const v0, 0x7f0b0333

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 292
    const v0, 0x7f0b0336

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->e:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->f:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0341

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    const v1, 0x7f0b033f

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 298
    :cond_1
    if-nez p2, :cond_0

    goto :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const v0, 0x7f050013

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->addPreferencesFromResource(I)V

    .line 65
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 71
    iput-object p0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->h:Z

    .line 76
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 84
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 80
    :catch_1
    move-exception v0

    .line 82
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->k:Lcom/sec/chaton/localbackup/h;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->k:Lcom/sec/chaton/localbackup/h;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/localbackup/h;->cancel(Z)Z

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c:Landroid/content/Context;

    const v1, 0x7f0b0339

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 95
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->h:Z

    .line 96
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onDestroy()V

    .line 97
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 321
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 328
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 324
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->finish()V

    goto :goto_0

    .line 321
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
