.class public Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;
.super Landroid/support/v4/app/Fragment;
.source "ChatViewFragment.java"

# interfaces
.implements Lcom/sec/chaton/localbackup/chatview/c;
.implements Lcom/sec/chaton/localbackup/chatview/g;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Landroid/database/ContentObserver;

.field b:Lcom/sec/chaton/e/a/v;

.field private final d:I

.field private e:I

.field private f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

.field private g:Lcom/sec/common/f/c;

.field private h:Lcom/sec/chaton/localbackup/chatview/d;

.field private i:Lcom/sec/chaton/e/a/u;

.field private j:Landroid/widget/ListView;

.field private k:Ljava/lang/String;

.field private l:Lcom/sec/chaton/e/r;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x1e

    const/4 v0, 0x1

    .line 80
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 86
    iput v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->d:I

    .line 87
    iput v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e:I

    .line 102
    iput-boolean v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->p:Z

    .line 105
    iput-boolean v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->r:Z

    .line 221
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/j;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/localbackup/chatview/j;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a:Landroid/database/ContentObserver;

    .line 260
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/k;

    invoke-direct {v0, p0}, Lcom/sec/chaton/localbackup/chatview/k;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b:Lcom/sec/chaton/e/a/v;

    .line 757
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/o;

    invoke-direct {v0, p0}, Lcom/sec/chaton/localbackup/chatview/o;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->s:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;I)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j:Landroid/widget/ListView;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x6

    const/4 v5, 0x0

    .line 903
    .line 904
    new-array v2, v5, [Ljava/lang/String;

    .line 906
    sget-object v2, Lcom/sec/chaton/localbackup/chatview/i;->a:[I

    invoke-virtual {p3}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    .line 930
    :goto_0
    return-object v0

    .line 910
    :pswitch_1
    if-eqz p1, :cond_1

    .line 911
    const-string v2, "\n"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 912
    array-length v3, v2

    if-le v3, v0, :cond_1

    const-string v3, "mixed"

    aget-object v4, v2, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 913
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 914
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 915
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 914
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 917
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    move-object v0, p2

    .line 925
    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 906
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 798
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/b/e;->c()Landroid/os/Message;

    move-result-object v0

    .line 799
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 800
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/b/e;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 801
    return-void
.end method

.method private a(Lcom/sec/chaton/localbackup/a/d;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 776
    sget-object v0, Lcom/sec/chaton/localbackup/chatview/i;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/localbackup/a/d;->b()Lcom/sec/chaton/e/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 794
    :goto_0
    :pswitch_0
    return-void

    .line 778
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/a;->a()Lcom/sec/chaton/localbackup/chatview/a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/localbackup/a/d;->a()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/chaton/localbackup/a/d;->c()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, p2, v1, v2, v3}, Lcom/sec/chaton/localbackup/chatview/a;->a(Ljava/lang/String;JLandroid/view/View;)V

    goto :goto_0

    .line 782
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 785
    :pswitch_3
    invoke-direct {p0, p2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 788
    :pswitch_4
    invoke-direct {p0, p2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 776
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Lcom/sec/chaton/localbackup/a/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/a/d;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const v6, 0x7f0b01ac

    const v5, 0x7f0b0037

    .line 646
    if-eqz p1, :cond_0

    .line 647
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 648
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 651
    const-string v0, "file://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 652
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 655
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 658
    invoke-virtual {p0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->startActivity(Landroid/content/Intent;)V

    .line 698
    :cond_0
    :goto_1
    return-void

    .line 661
    :cond_1
    invoke-static {p1}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v1

    .line 662
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->j:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v0, :cond_2

    .line 663
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 664
    invoke-virtual {v0, v6}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0207

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_1

    .line 666
    :cond_2
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v0, :cond_0

    .line 667
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 668
    const-string v0, "Office viewer"

    .line 669
    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->e:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v3, :cond_5

    .line 670
    const-string v0, "\ud55c\uae00\ubdf0\uc5b4"

    .line 676
    :cond_3
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "market://search?q="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&c=apps"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 677
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 679
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    .line 681
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_4

    .line 682
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isMarketAvailable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", strUri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 686
    invoke-virtual {v0, v6}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v3

    const v4, 0x7f0b0266

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/localbackup/chatview/n;

    invoke-direct {v4, p0, v1, v2}, Lcom/sec/chaton/localbackup/chatview/n;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;ZLandroid/content/Intent;)V

    invoke-virtual {v3, v5, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 694
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_1

    .line 671
    :cond_5
    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->d:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v3, :cond_6

    .line 672
    const-string v0, "\ud6c8\ubbfc\uc815\uc74c"

    goto :goto_2

    .line 673
    :cond_6
    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v3, :cond_7

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->c:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v3, :cond_7

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v1, v3, :cond_7

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v1, v3, :cond_3

    .line 674
    :cond_7
    const-string v0, "Office viewer"

    goto/16 :goto_2

    :cond_8
    move-object v0, p1

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252
    const-string v0, ""

    .line 253
    if-lez p2, :cond_1

    .line 254
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, ")"

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 256
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object v1, v2, v4

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->b(Ljava/lang/CharSequence;)V

    .line 258
    :cond_0
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;I)I
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->o:I

    return p1
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 701
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    const-class v2, Lcom/sec/chaton/multimedia/vcard/ReadVCardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 702
    const-string v1, "URI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 703
    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->startActivity(Landroid/content/Intent;)V

    .line 704
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->r:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->p:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->i:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 247
    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x0

    .line 707
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 708
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    const v1, 0x7f0b00de

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 746
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 713
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 718
    const/4 v1, 0x0

    .line 720
    array-length v3, v2

    if-le v3, v6, :cond_0

    .line 721
    array-length v3, v2

    if-le v3, v0, :cond_4

    .line 722
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 723
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 724
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 723
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 726
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 729
    :goto_2
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 730
    const-string v3, "http://maps.google.com/maps?q=loc:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 731
    aget-object v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 733
    if-eqz v0, :cond_3

    .line 734
    const-string v2, "[\\(\\)]"

    const-string v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 735
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 739
    :cond_3
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 740
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 741
    :catch_0
    move-exception v0

    .line 742
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    const v1, 0x7f0b0405

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->r:Z

    return p1
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 750
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    const-class v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 751
    const-string v1, "ACTIVITY_PURPOSE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 752
    const-string v1, "URI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 754
    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->startActivity(Landroid/content/Intent;)V

    .line 755
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->q:Z

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e:I

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/d;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->h:Lcom/sec/chaton/localbackup/chatview/d;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->p:Z

    return v0
.end method

.method static synthetic j(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->o:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 806
    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/a;->a()Lcom/sec/chaton/localbackup/chatview/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/a;->c()V

    .line 807
    return-void
.end method

.method public a(Landroid/view/View;ILandroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 399
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v11}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 642
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1, v9}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 416
    :cond_2
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p3, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    const-string v0, "message_content_type"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v7

    .line 420
    const-string v0, "message_download_uri"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 421
    const-string v1, "message_content"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 422
    const-string v1, "message_inbox_no"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 423
    const-string v1, "message_sever_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 424
    const-string v1, "message_sender"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 426
    sget-object v1, Lcom/sec/chaton/localbackup/chatview/i;->a:[I

    invoke-virtual {v7}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 430
    :pswitch_0
    const-string v0, "message_inbox_no"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 431
    const-string v0, "message_sever_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 432
    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 434
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    const-class v7, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 435
    const-string v6, "messageId"

    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-nez v7, :cond_3

    :goto_1
    invoke-virtual {v5, v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 436
    const-string v0, "inboxNo"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    const-string v0, "isValid"

    invoke-virtual {v5, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 438
    const-string v0, "chatType"

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 439
    const-string v0, "backup"

    invoke-virtual {v5, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v0, v5}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_3
    move-wide v0, v2

    .line 435
    goto :goto_1

    .line 444
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v1

    if-nez v1, :cond_5

    .line 445
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    const v1, 0x7f0b00dd

    invoke-static {v0, v1, v11}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 450
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 451
    :cond_6
    new-instance v0, Lcom/sec/chaton/localbackup/a/a;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    iget-object v9, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->s:Landroid/os/Handler;

    move-object v8, p1

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/localbackup/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;Landroid/os/Handler;Z)V

    .line 452
    new-array v1, v11, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 454
    :cond_7
    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/a;->a()Lcom/sec/chaton/localbackup/chatview/a;

    move-result-object v1

    invoke-virtual {v1, v0, v4, v5, p1}, Lcom/sec/chaton/localbackup/chatview/a;->a(Ljava/lang/String;JLandroid/view/View;)V

    goto/16 :goto_0

    .line 459
    :pswitch_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 460
    :cond_8
    new-instance v0, Lcom/sec/chaton/localbackup/a/a;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    iget-object v9, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->s:Landroid/os/Handler;

    move-object v8, p1

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/localbackup/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;Landroid/os/Handler;Z)V

    .line 461
    new-array v1, v11, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 463
    :cond_9
    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 467
    :pswitch_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 468
    :cond_a
    new-instance v0, Lcom/sec/chaton/localbackup/a/a;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    iget-object v9, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->s:Landroid/os/Handler;

    move-object v8, p1

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/localbackup/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;Landroid/os/Handler;Z)V

    .line 469
    new-array v1, v11, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 471
    :cond_b
    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 475
    :pswitch_4
    invoke-direct {p0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 479
    :pswitch_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 480
    :cond_c
    new-instance v0, Lcom/sec/chaton/localbackup/a/a;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    iget-object v9, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->s:Landroid/os/Handler;

    move-object v8, p1

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/localbackup/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;Landroid/os/Handler;Z)V

    .line 481
    new-array v1, v11, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 483
    :cond_d
    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 489
    :pswitch_6
    :try_start_0
    invoke-static {v2}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->parse(Ljava/lang/String;)Lcom/sec/chaton/io/entry/MessageType4Entry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 497
    instance-of v1, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    if-eqz v1, :cond_0

    .line 500
    check-cast v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    .line 501
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_e

    .line 502
    const-string v1, "onMediaClicked()"

    sget-object v2, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_e
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getAppName()Ljava/lang/String;

    move-result-object v1

    .line 548
    const-string v2, "android"

    const-string v3, "phone"

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getParam(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;

    move-result-object v2

    .line 549
    if-nez v2, :cond_f

    .line 550
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 551
    const-string v0, "-- not matched anything-- "

    sget-object v1, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 490
    :catch_0
    move-exception v0

    .line 491
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 492
    sget-object v1, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 556
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 560
    :try_start_1
    iget-object v0, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    const/16 v4, 0x40

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 561
    if-eqz v0, :cond_12

    .line 562
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_10

    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " is installed"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_10
    move v0, v10

    .line 571
    :goto_2
    if-eqz v0, :cond_14

    .line 574
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    iget-object v4, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->executeUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 583
    const/high16 v0, 0x10000

    invoke-virtual {v3, v9, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 585
    if-eqz v0, :cond_13

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_13

    .line 586
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_11

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " supports this action"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_11
    invoke-virtual {p0, v9}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 567
    :catch_1
    move-exception v0

    .line 568
    sget-object v4, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_12
    move v0, v11

    goto :goto_2

    .line 575
    :catch_2
    move-exception v0

    .line 576
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 577
    sget-object v1, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 592
    :cond_13
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 593
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " does not support this action"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 598
    :cond_14
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_15

    .line 599
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " is not installed"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :cond_15
    iget-object v0, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->installUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 603
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " does not have installUrl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 609
    :cond_16
    iget-object v0, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->installUrl:Ljava/lang/String;

    .line 610
    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 611
    const v3, 0x7f0b01ac

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 612
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b024e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v1, v4, v11

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v3, 0x7f0b0042

    new-instance v4, Lcom/sec/chaton/localbackup/chatview/m;

    invoke-direct {v4, p0, v0}, Lcom/sec/chaton/localbackup/chatview/m;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/localbackup/chatview/l;

    invoke-direct {v3, p0}, Lcom/sec/chaton/localbackup/chatview/l;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 631
    invoke-virtual {v2}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 426
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 366
    move-object v0, p1

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    .line 367
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 368
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 109
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->i:Lcom/sec/chaton/e/a/u;

    .line 110
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g:Lcom/sec/common/f/c;

    .line 112
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 114
    const-string v1, "inboxNO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->k:Ljava/lang/String;

    .line 115
    const-string v1, "chatType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->l:Lcom/sec/chaton/e/r;

    .line 116
    const-string v1, "inbox_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->m:Ljava/lang/String;

    .line 117
    const-string v1, "inbox_participants"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->n:I

    .line 119
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->setHasOptionsMenu(Z)V

    .line 120
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 121
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8

    .prologue
    .line 812
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 814
    new-instance v6, Lcom/sec/chaton/b/a;

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-direct {v6, v0, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 816
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 818
    iget-object v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/f;

    .line 820
    if-eqz v0, :cond_0

    .line 822
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget v1, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 823
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 824
    const-string v1, "message_content_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v4

    .line 825
    const-string v1, "message_content"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 826
    const-string v2, "message_download_uri"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 827
    const-string v2, "message_formatted"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 829
    sget-object v2, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    if-eq v4, v2, :cond_0

    sget-object v2, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_1

    .line 899
    :cond_0
    :goto_0
    return-void

    .line 833
    :cond_1
    invoke-static {v4}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    .line 834
    invoke-virtual {v6}, Lcom/sec/chaton/b/a;->clear()V

    .line 836
    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_4

    move-object v2, v1

    .line 837
    :goto_1
    invoke-direct {p0, v1, v0, v4}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)Ljava/lang/String;

    move-result-object v3

    .line 839
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v4, v0, :cond_2

    .line 841
    const v0, 0x7f0b005c

    invoke-virtual {v6, v0}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v7, Lcom/sec/chaton/localbackup/chatview/p;

    invoke-direct {v7, p0, v1}, Lcom/sec/chaton/localbackup/chatview/p;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Ljava/lang/String;)V

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 863
    :cond_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v4, v0, :cond_0

    .line 864
    :cond_3
    const v0, 0x7f0b0165

    invoke-virtual {v6, v0}, Lcom/sec/chaton/b/a;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    new-instance v0, Lcom/sec/chaton/localbackup/chatview/q;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/localbackup/chatview/q;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    move-object v2, v5

    .line 836
    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 126
    const v0, 0x7f0300ba

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 127
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/d;

    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0300b8

    iget-object v4, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g:Lcom/sec/common/f/c;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/localbackup/chatview/d;-><init>(Landroid/content/Context;ILandroid/database/Cursor;Lcom/sec/common/f/c;Lcom/sec/chaton/localbackup/chatview/g;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->h:Lcom/sec/chaton/localbackup/chatview/d;

    .line 128
    const v0, 0x7f070349

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j:Landroid/widget/ListView;

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->h:Lcom/sec/chaton/localbackup/chatview/d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setTranscriptMode(I)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/chaton/localbackup/chatview/h;

    invoke-direct {v1, p0}, Lcom/sec/chaton/localbackup/chatview/h;-><init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 175
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->i:Lcom/sec/chaton/e/a/u;

    iget v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e:I

    invoke-static {v0}, Lcom/sec/chaton/localbackup/database/a;->a(I)Landroid/net/Uri;

    move-result-object v4

    const-string v6, "message_inbox_no=?"

    new-array v7, v10, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->k:Ljava/lang/String;

    aput-object v0, v7, v8

    move v2, v10

    move-object v5, v3

    move-object v8, v3

    invoke-virtual/range {v1 .. v8}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->l:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_0

    .line 187
    const v0, 0x7f0b0063

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->n:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Ljava/lang/String;I)V

    .line 194
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/localbackup/database/a;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v10, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 196
    return-object v9

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->l:Lcom/sec/chaton/e/r;

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    const v0, 0x7f0b0064

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->n:I

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 191
    :cond_1
    const v0, 0x7f0b01a0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 381
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 382
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 211
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 212
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    .line 373
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 374
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 387
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f:Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->c()V

    .line 391
    :cond_0
    const/4 v0, 0x1

    .line 393
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 217
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 218
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c()V

    .line 204
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 205
    return-void
.end method
