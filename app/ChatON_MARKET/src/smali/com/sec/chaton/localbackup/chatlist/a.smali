.class public Lcom/sec/chaton/localbackup/chatlist/a;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "BackupChatListAdapter.java"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "inbox_participants"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "inbox_title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "inbox_unread_count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "inbox_last_time"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "inbox_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inbox_last_message"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "inbox_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "inbox_title_fixed"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/localbackup/chatlist/a;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 48
    sget-object v4, Lcom/sec/chaton/localbackup/chatlist/a;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 49
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    .line 50
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/a;->c:Landroid/view/LayoutInflater;

    .line 51
    iput p2, p0, Lcom/sec/chaton/localbackup/chatlist/a;->d:I

    .line 52
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 280
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 281
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 282
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 284
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 286
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 290
    :goto_0
    return-object v0

    .line 289
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 290
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatlist/c;

    .line 58
    const-string v1, "inbox_chat_type"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 59
    const-string v1, "inbox_last_message"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 60
    const-string v1, "inbox_title"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 62
    const-string v1, "inbox_title_fixed"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 63
    const-string v1, "inbox_participants"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 64
    const-string v1, "inbox_last_time"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 66
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_3

    .line 68
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->d:Landroid/widget/TextView;

    invoke-direct {p0, v8, v9}, Lcom/sec/chaton/localbackup/chatlist/a;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    :goto_0
    if-eqz v2, :cond_2

    .line 74
    const-string v1, ";"

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 76
    const-string v1, ""

    .line 77
    array-length v8, v3

    const/4 v9, 0x2

    if-le v8, v9, :cond_0

    .line 78
    const/4 v1, 0x2

    aget-object v1, v3, v1

    .line 81
    :cond_0
    array-length v8, v3

    const/4 v9, 0x2

    if-lt v8, v9, :cond_2

    .line 82
    const/4 v8, 0x0

    aget-object v8, v3, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 83
    const/4 v9, 0x1

    aget-object v9, v3, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v9

    .line 85
    const-string v10, ""

    .line 86
    const-string v10, ""

    .line 88
    const/4 v10, 0x1

    if-eq v8, v10, :cond_1

    const/4 v10, 0x2

    if-ne v8, v10, :cond_2

    :cond_1
    sget-object v10, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    if-eq v9, v10, :cond_2

    .line 89
    const/4 v2, 0x1

    if-ne v8, v2, :cond_4

    .line 90
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 91
    const-string v2, "mixed"

    const/4 v8, 0x0

    aget-object v1, v1, v8

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 92
    invoke-static {v9, v3, v1}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;[Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 227
    :cond_2
    :goto_1
    if-nez v2, :cond_17

    .line 228
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->c:Landroid/widget/TextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 240
    :goto_2
    sget-object v1, Lcom/sec/chaton/localbackup/chatlist/b;->a:[I

    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 271
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->a:Landroid/widget/ImageView;

    const v2, 0x7f0201bb

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 276
    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 277
    return-void

    .line 70
    :cond_3
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->d:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 94
    :cond_4
    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    sget-object v8, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v2, v8, :cond_e

    .line 96
    array-length v2, v3

    const/4 v8, 0x4

    if-ge v2, v8, :cond_6

    .line 97
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f0b00b4

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 102
    :goto_4
    sget-object v8, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v9, v8, :cond_8

    .line 103
    array-length v1, v3

    const/4 v8, 0x3

    if-ge v1, v8, :cond_7

    .line 104
    const-string v1, ""

    .line 113
    :cond_5
    :goto_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 99
    :cond_6
    const/4 v2, 0x3

    aget-object v2, v3, v2

    invoke-static {v2}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 106
    :cond_7
    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 110
    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    .line 115
    :cond_8
    sget-object v8, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    if-ne v9, v8, :cond_a

    .line 116
    array-length v1, v3

    const/4 v8, 0x3

    if-ge v1, v8, :cond_9

    .line 117
    const-string v1, ""

    .line 123
    :goto_6
    invoke-static {v1}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 124
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 136
    goto/16 :goto_1

    .line 119
    :cond_9
    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    .line 136
    :cond_a
    sget-object v8, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    if-ne v9, v8, :cond_d

    .line 137
    const-string v1, ""

    .line 139
    array-length v8, v3

    const/4 v9, 0x3

    if-ge v8, v9, :cond_b

    .line 140
    const-string v3, ""

    .line 145
    :goto_7
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 146
    const-string v3, "push_message"

    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 147
    const-string v8, "content_type"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 148
    const-string v8, "result"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 149
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const v8, 0x7f0b022f

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 156
    :goto_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 158
    goto/16 :goto_1

    .line 142
    :cond_b
    const/4 v8, 0x2

    aget-object v3, v3, v8

    invoke-static {v3}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 151
    :cond_c
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const v8, 0x7f0b022e

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_8

    .line 153
    :catch_0
    move-exception v3

    .line 154
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_8

    .line 159
    :cond_d
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 160
    const-string v8, "mixed"

    const/4 v10, 0x0

    aget-object v3, v3, v10

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 161
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, ": "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v9, v1, v3}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 162
    goto/16 :goto_1

    .line 165
    :cond_e
    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v9, v2, :cond_11

    .line 166
    array-length v1, v3

    const/4 v2, 0x3

    if-ge v1, v2, :cond_10

    .line 167
    const-string v1, ""

    :cond_f
    :goto_9
    move-object v2, v1

    .line 175
    goto/16 :goto_1

    .line 169
    :cond_10
    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 171
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 172
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_9

    .line 176
    :cond_11
    sget-object v2, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    if-ne v9, v2, :cond_13

    .line 177
    array-length v1, v3

    const/4 v2, 0x3

    if-ge v1, v2, :cond_12

    .line 178
    const-string v1, ""

    .line 193
    :goto_a
    invoke-static {v1}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 195
    goto/16 :goto_1

    .line 180
    :cond_12
    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-static {v1}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    .line 195
    :cond_13
    sget-object v2, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    if-ne v9, v2, :cond_16

    .line 196
    const-string v1, ""

    .line 198
    array-length v2, v3

    const/4 v8, 0x3

    if-ge v2, v8, :cond_14

    .line 199
    const-string v2, ""

    .line 204
    :goto_b
    :try_start_2
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 205
    const-string v2, "push_message"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 206
    const-string v3, "content_type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 207
    const-string v3, "result"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 208
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b022f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    :goto_c
    move-object v2, v1

    .line 216
    goto/16 :goto_1

    .line 201
    :cond_14
    const/4 v2, 0x2

    aget-object v2, v3, v2

    invoke-static {v2}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_b

    .line 210
    :cond_15
    :try_start_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b022e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v1

    goto :goto_c

    .line 212
    :catch_1
    move-exception v2

    .line 213
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_c

    .line 217
    :cond_16
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 218
    const-string v3, "mixed"

    const/4 v8, 0x0

    aget-object v2, v2, v8

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 219
    invoke-static {v9, v1, v2}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_1

    .line 230
    :cond_17
    const v1, 0x3f99999a    # 1.2f

    .line 231
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 232
    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    const/high16 v8, 0x40000000    # 2.0f

    cmpl-float v3, v3, v8

    if-ltz v3, :cond_18

    .line 233
    const/high16 v1, 0x3f800000    # 1.0f

    .line 235
    :cond_18
    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    iget-object v8, v0, Lcom/sec/chaton/localbackup/chatlist/c;->c:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getLineHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v1, v8

    float-to-int v1, v1

    invoke-static {v3, v2, v1}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 237
    iget-object v2, v0, Lcom/sec/chaton/localbackup/chatlist/c;->c:Landroid/widget/TextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto/16 :goto_2

    .line 242
    :pswitch_0
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->a:Landroid/widget/ImageView;

    const v2, 0x7f0201bb

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 243
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->e:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 246
    :pswitch_1
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->a:Landroid/widget/ImageView;

    const v2, 0x7f0201bc

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 248
    const-string v1, "Y"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    if-gtz v7, :cond_19

    .line 249
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->b:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 251
    :cond_19
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->e:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->e:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v7, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 259
    :pswitch_2
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->a:Landroid/widget/ImageView;

    const v2, 0x7f020151

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 261
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->b:Landroid/widget/TextView;

    invoke-static {v6, v5, v7}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->e:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    if-lez v7, :cond_1a

    .line 265
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->e:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 267
    :cond_1a
    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatlist/c;->e:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v7, -0x2

    const/4 v8, 0x0

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatlist/a;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/localbackup/chatlist/a;->d:I

    invoke-virtual {v0, v1, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 296
    new-instance v2, Lcom/sec/chaton/localbackup/chatlist/c;

    invoke-direct {v2}, Lcom/sec/chaton/localbackup/chatlist/c;-><init>()V

    .line 298
    const v0, 0x7f07014b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatlist/c;->a:Landroid/widget/ImageView;

    .line 299
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatlist/c;->b:Landroid/widget/TextView;

    .line 300
    const v0, 0x7f07014d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatlist/c;->c:Landroid/widget/TextView;

    .line 302
    const v0, 0x7f0704b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 303
    invoke-virtual {v0, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 305
    new-instance v3, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 307
    new-instance v4, Lcom/sec/chaton/widget/AdaptableTextView;

    iget-object v5, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;)V

    .line 308
    iget-object v5, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    const v6, 0x7f0c0056

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 309
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v5, v7, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 310
    iput-object v4, v2, Lcom/sec/chaton/localbackup/chatlist/c;->e:Landroid/widget/TextView;

    .line 312
    new-instance v5, Lcom/sec/chaton/widget/AdaptableTextView;

    iget-object v6, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;)V

    .line 313
    iget-object v6, p0, Lcom/sec/chaton/localbackup/chatlist/a;->b:Landroid/content/Context;

    const v7, 0x7f0c0060

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 314
    iput-object v5, v2, Lcom/sec/chaton/localbackup/chatlist/c;->d:Landroid/widget/TextView;

    .line 316
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 317
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 318
    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 320
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 322
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 323
    return-object v1
.end method
