.class Lcom/sec/chaton/localbackup/chatview/j;
.super Landroid/database/ContentObserver;
.source "ChatViewFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatview/j;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/j;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0, v7}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/j;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatview/j;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v3}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/localbackup/database/a;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v1, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/localbackup/chatview/j;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v4}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onChange - QUERY_MESSAGE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/j;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-void
.end method
