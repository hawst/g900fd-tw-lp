.class public Lcom/sec/chaton/localbackup/chatview/s;
.super Lcom/sec/chaton/multimedia/image/c;
.source "IconDispatcherTask.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/e/w;

.field private c:Lcom/sec/chaton/multimedia/doc/b;

.field private d:J

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/chaton/e/w;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/chaton/localbackup/chatview/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/chatview/s;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/sec/chaton/e/w;Lcom/sec/chaton/multimedia/doc/b;J)V
    .locals 3

    .prologue
    .line 29
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/multimedia/image/c;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/sec/chaton/e/w;)V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    .line 30
    iput-object p5, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    .line 31
    iput-object p6, p0, Lcom/sec/chaton/localbackup/chatview/s;->c:Lcom/sec/chaton/multimedia/doc/b;

    .line 32
    iput-wide p7, p0, Lcom/sec/chaton/localbackup/chatview/s;->d:J

    .line 34
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    const v2, 0x7f0200a9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    const v2, 0x7f02018f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    const v2, 0x7f020191

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    const v2, 0x7f020195

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    const v2, 0x7f020156

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    return-void
.end method

.method private a(Lcom/sec/chaton/e/w;)I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 162
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020443

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/multimedia/doc/b;)I
    .locals 2

    .prologue
    .line 150
    sget-object v0, Lcom/sec/chaton/chat/a/q;->F:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    sget-object v0, Lcom/sec/chaton/chat/a/q;->F:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 154
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/a/q;->F:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 137
    :try_start_0
    instance-of v1, p1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v1

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    instance-of v1, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v1

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 142
    :catch_0
    move-exception v1

    .line 143
    sget-object v2, Lcom/sec/chaton/localbackup/chatview/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :catch_1
    move-exception v1

    .line 145
    sget-object v2, Lcom/sec/chaton/localbackup/chatview/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/sec/chaton/localbackup/chatview/t;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 132
    :goto_0
    return-void

    .line 109
    :pswitch_0
    if-nez p1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020068

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 112
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/chatview/s;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :pswitch_1
    if-nez p1, :cond_1

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0202ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 119
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/chatview/s;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 123
    :pswitch_2
    if-nez p1, :cond_2

    .line 124
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020199

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 126
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/chatview/s;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x0

    .line 44
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v3, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 50
    :goto_0
    sget-object v0, Lcom/sec/chaton/localbackup/chatview/t;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 85
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    invoke-direct {p0, v1}, Lcom/sec/chaton/localbackup/chatview/s;->a(Lcom/sec/chaton/e/w;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v3, :cond_4

    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    :cond_0
    :goto_1
    return-void

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 54
    :pswitch_0
    invoke-super {p0}, Lcom/sec/chaton/multimedia/image/c;->b()V

    goto :goto_1

    .line 58
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/s;->c:Lcom/sec/chaton/multimedia/doc/b;

    invoke-direct {p0, v1}, Lcom/sec/chaton/localbackup/chatview/s;->a(Lcom/sec/chaton/multimedia/doc/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 61
    :pswitch_2
    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/a;->a()Lcom/sec/chaton/localbackup/chatview/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/localbackup/chatview/s;->d:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/chatview/a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 66
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02011a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 69
    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/a;->a()Lcom/sec/chaton/localbackup/chatview/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/localbackup/chatview/s;->d:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/chatview/a;->b(J)Lcom/sec/chaton/localbackup/chatview/b;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 72
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    goto :goto_1

    .line 64
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02011e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 78
    :pswitch_3
    invoke-static {}, Lcom/sec/chaton/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 79
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    invoke-direct {p0, v1}, Lcom/sec/chaton/localbackup/chatview/s;->a(Lcom/sec/chaton/e/w;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 81
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02018d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 89
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatview/s;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public c()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/s;->b:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_1

    .line 100
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/multimedia/image/c;->c()Ljava/lang/Object;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
