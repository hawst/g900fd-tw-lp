.class Lcom/sec/chaton/localbackup/chatview/h;
.super Ljava/lang/Object;
.source "ChatViewFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 10

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setSelected(Z)V

    .line 147
    :cond_0
    if-nez p2, :cond_1

    add-int/lit8 v0, p4, -0x1

    if-lez v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->d(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const/16 v3, 0x1e

    invoke-static {v0, v3}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;I)I

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onScroll - QUERY_MESSAGE_LOAD_MORE, "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v3}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v3}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->e(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/localbackup/database/a;->a(I)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "message_inbox_no=?"

    new-array v6, v9, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v4}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->f(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v8

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0, v9}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z

    .line 159
    :cond_1
    add-int/lit8 v0, p2, 0x1

    add-int/2addr v0, p3

    if-le v0, p4, :cond_3

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTranscriptMode(I)V

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0, v9}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z

    .line 172
    :cond_2
    :goto_0
    return-void

    .line 166
    :cond_3
    if-eqz p3, :cond_2

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0, v8}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/h;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setTranscriptMode(I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method
