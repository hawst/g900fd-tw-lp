.class public Lcom/sec/chaton/localbackup/noti/AutoBackupService;
.super Landroid/app/IntentService;
.source "AutoBackupService.java"


# instance fields
.field private a:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "AutoBackupService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 29
    new-instance v1, Lcom/sec/chaton/localbackup/q;

    invoke-direct {v1}, Lcom/sec/chaton/localbackup/q;-><init>()V

    .line 32
    :try_start_0
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "ChatON"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    .line 33
    iget-object v0, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 36
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wake lock is acquired "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_1
    sget-object v0, Lcom/sec/chaton/localbackup/s;->b:Lcom/sec/chaton/localbackup/s;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/s;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    const-string v2, "local-backup.db.crypt"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/localbackup/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    sget-object v3, Lcom/sec/chaton/localbackup/t;->a:Lcom/sec/chaton/localbackup/t;

    invoke-virtual {v1, v3, v0, v2}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_2

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "autobackup complete : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "auto_backup_start"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 56
    invoke-static {p0, v5}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;Z)V

    .line 58
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_3

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "wake lock relesed "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_4

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    :goto_0
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 69
    :cond_4
    return-void

    .line 48
    :catch_0
    move-exception v0

    .line 50
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_5

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "autobackup error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " time : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "auto_backup_start"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 56
    invoke-static {p0, v5}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;Z)V

    .line 58
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_6

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "wake lock relesed "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_4

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "auto_backup_start"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 56
    invoke-static {p0, v5}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;Z)V

    .line 58
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_7

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wake lock relesed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_8

    .line 63
    iget-object v1, p0, Lcom/sec/chaton/localbackup/noti/AutoBackupService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 55
    :cond_8
    throw v0
.end method
