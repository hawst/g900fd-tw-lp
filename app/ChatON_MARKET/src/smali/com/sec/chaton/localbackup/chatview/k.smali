.class Lcom/sec/chaton/localbackup/chatview/k;
.super Ljava/lang/Object;
.source "ChatViewFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x1e

    const/4 v2, 0x1

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    if-ne p1, v2, :cond_6

    .line 271
    if-eqz p3, :cond_0

    .line 274
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->h(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/chaton/localbackup/chatview/d;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 281
    if-eqz p3, :cond_4

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;I)I

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->i(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 287
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :goto_1
    if-eqz v1, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/database/Cursor;)V

    goto :goto_0

    .line 290
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 295
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 296
    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v2, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/database/Cursor;)V

    .line 295
    :cond_5
    throw v0

    .line 301
    :cond_6
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 304
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->h(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/chaton/localbackup/chatview/d;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 305
    if-eqz v1, :cond_7

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/database/Cursor;)V

    .line 309
    :cond_7
    if-eqz p3, :cond_e

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 310
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryCompleted - QUERY_MESSAGE_LOAD_MORE, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_8
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 314
    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->j(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)I

    move-result v2

    sub-int v2, v0, v2

    .line 315
    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v3, v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->b(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;I)I

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->i(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSelection(I)V

    .line 329
    :cond_9
    :goto_2
    if-lez v2, :cond_d

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z

    .line 340
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 343
    if-eqz v1, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 321
    :cond_a
    if-lez v2, :cond_9

    .line 322
    if-ge v2, v4, :cond_c

    .line 323
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 343
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_b

    .line 344
    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v2, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Landroid/database/Cursor;)V

    .line 343
    :cond_b
    throw v0

    .line 325
    :cond_c
    :try_start_4
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/16 v3, 0x1e

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_2

    .line 332
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z

    goto :goto_3

    .line 336
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->a(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/k;->a:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->c(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Z)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 356
    return-void
.end method
