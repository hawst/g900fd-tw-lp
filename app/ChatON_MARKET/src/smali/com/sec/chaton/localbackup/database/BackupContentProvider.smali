.class public Lcom/sec/chaton/localbackup/database/BackupContentProvider;
.super Landroid/content/ContentProvider;
.source "BackupContentProvider.java"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;


# instance fields
.field private a:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM(SELECT ifnull(c.buddy_name,\'\') AS buddy_name,ifnull(c.buddy_no,a.message_sender) AS buddy_no,a.message_content,a.message_content_type,a.message_download_uri,a.message_formatted,a._id,a.message_inbox_no,a.message_sender,a.message_sever_id,a.message_session_id,a.message_time,a.message_is_failed,a.message_is_file_upload,a.message_type FROM ( SELECT * FROM message WHERE message_inbox_no = ? AND (message_is_truncated = \'N\' OR message_content_type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ifnull("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participant"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_sender"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "c."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->b:Ljava/lang/String;

    .line 174
    const/16 v0, 0x60

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "a.*, "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "r."

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "relation_buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\tFROM ( "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "j."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "j."

    aput-object v2, v0, v1

    const-string v1, "buddy_name"

    aput-object v1, v0, v3

    const/16 v1, 0xd

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "i.*"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "inbox"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " i"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " p"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " b "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, " NOT null"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ) j "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "j."

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "inbox_last_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " != "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "inbox_valid"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, " != ?"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "inbox_web_url"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, " IS null"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " ) a "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "inbox_buddy_relation"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, " r "

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "r."

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, " ORDER BY "

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "inbox_last_time"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " DESC, a._id"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 38
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a:Landroid/content/UriMatcher;

    .line 39
    iget-object v0, p0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.localbackup"

    const-string v2, "inbox/path_inbox"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 40
    iget-object v0, p0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.localbackup"

    const-string v2, "message/path_message/*"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.chaton.localbackup"

    const-string v2, "message/path_update_media"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 93
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 83
    :pswitch_1
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.inbox"

    goto :goto_0

    .line 86
    :pswitch_2
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 89
    :pswitch_3
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.message"

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a()V

    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 47
    invoke-static {}, Lcom/sec/chaton/localbackup/database/b;->a()Lcom/sec/chaton/localbackup/database/b;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/database/b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 74
    :pswitch_0
    const-string v1, "message"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 76
    :cond_0
    :goto_0
    return-object v5

    .line 54
    :pswitch_1
    sget-object v1, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 58
    :pswitch_2
    invoke-static {p1}, Lcom/sec/chaton/e/v;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 60
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 62
    array-length v5, p4

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_1

    aget-object v6, p4, v1

    .line 63
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 65
    :cond_1
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 68
    sget-object v2, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 110
    const/4 v0, -0x1

    .line 111
    invoke-static {}, Lcom/sec/chaton/localbackup/database/b;->a()Lcom/sec/chaton/localbackup/database/b;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/database/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 114
    iget-object v2, p0, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 123
    :goto_0
    return v0

    .line 116
    :pswitch_0
    const-string v0, "message"

    invoke-virtual {v1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/database/BackupContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
