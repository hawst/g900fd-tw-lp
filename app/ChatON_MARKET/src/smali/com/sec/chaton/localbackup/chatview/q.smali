.class Lcom/sec/chaton/localbackup/chatview/q;
.super Ljava/lang/Object;
.source "ChatViewFragment.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/sec/chaton/e/w;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatview/q;->e:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    iput-object p2, p0, Lcom/sec/chaton/localbackup/chatview/q;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/chaton/localbackup/chatview/q;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/chaton/localbackup/chatview/q;->c:Lcom/sec/chaton/e/w;

    iput-object p5, p0, Lcom/sec/chaton/localbackup/chatview/q;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 868
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/q;->e:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/q;->e:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    const v2, 0x7f0b0166

    invoke-virtual {v1, v2}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/q;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatview/q;->b:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/localbackup/chatview/q;->c:Lcom/sec/chaton/e/w;

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)Landroid/content/Intent;

    move-result-object v1

    .line 869
    if-eqz v1, :cond_1

    .line 873
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "TrunkShareCheckPopup"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 874
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/q;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/q;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v2, :cond_3

    .line 875
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/q;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    .line 879
    :goto_0
    if-eqz v0, :cond_2

    .line 880
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/q;->e:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/localbackup/chatview/r;

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/localbackup/chatview/r;-><init>(Lcom/sec/chaton/localbackup/chatview/q;Landroid/content/Intent;)V

    invoke-static {v0, v2}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 887
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 892
    :cond_1
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 889
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/q;->e:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_0
.end method
