.class Lcom/sec/chaton/localbackup/j;
.super Ljava/lang/Object;
.source "BackupListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/BackupListView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/BackupListView;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f070592

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->b(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->b(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->c(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/BackupListView;Z)V

    .line 164
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/BackupListView;->c:Lcom/sec/chaton/localbackup/o;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/o;->notifyDataSetChanged()V

    .line 165
    return-void

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->b(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/BackupListView;Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/localbackup/j;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->c(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
