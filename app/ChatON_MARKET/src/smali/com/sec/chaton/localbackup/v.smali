.class Lcom/sec/chaton/localbackup/v;
.super Ljava/lang/Object;
.source "SecretKeyView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/SecretKeyView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/chaton/localbackup/v;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/chaton/localbackup/v;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/v;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Ljava/lang/String;)Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/localbackup/v;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->c(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/v;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->f(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/localbackup/v;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Z)V

    .line 208
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/v;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Z)V

    goto :goto_0
.end method
