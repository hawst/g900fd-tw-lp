.class public Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ChatListActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/chatlist/ChatListFragment;-><init>()V

    .line 14
    return-object v0
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;->finish()V

    .line 19
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 33
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;->a(Landroid/app/Activity;)V

    .line 36
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 24
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 25
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-static {p0}, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;->a(Landroid/app/Activity;)V

    .line 28
    :cond_0
    return-void
.end method
