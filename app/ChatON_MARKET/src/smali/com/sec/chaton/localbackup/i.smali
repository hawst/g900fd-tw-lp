.class Lcom/sec/chaton/localbackup/i;
.super Ljava/lang/Object;
.source "BackupListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/BackupListView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/BackupListView;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const v5, 0x7f070592

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 98
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/p;

    .line 99
    iget-object v2, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v2}, Lcom/sec/chaton/localbackup/BackupListView;->a(Lcom/sec/chaton/localbackup/BackupListView;)Lcom/sec/chaton/localbackup/n;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/localbackup/n;->b:Lcom/sec/chaton/localbackup/n;

    if-ne v2, v3, :cond_5

    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v2

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/BackupListView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v2, v0, :cond_3

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->b(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 121
    :goto_0
    if-lez v2, :cond_4

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->c(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 142
    :goto_1
    return-void

    :cond_1
    move v0, v1

    move v2, v1

    .line 108
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    iget-object v3, v3, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 109
    iget-object v3, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    iget-object v3, v3, Lcom/sec/chaton/localbackup/BackupListView;->d:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 110
    add-int/lit8 v2, v2, 0x1

    .line 108
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 118
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->b(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 124
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/BackupListView;->c(Lcom/sec/chaton/localbackup/BackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 130
    :cond_5
    iget-object v2, v0, Lcom/sec/chaton/localbackup/p;->f:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "backup_checkkey"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 131
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-virtual {v3}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/chaton/localbackup/ActivitySecretKey;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    const-string v3, "password_mode"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 133
    const-string v1, "target_filepath"

    iget-object v3, v0, Lcom/sec/chaton/localbackup/p;->d:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    sget-object v1, Lcom/sec/chaton/localbackup/BackupListView;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/p;->g:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/localbackup/BackupListView;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 137
    :cond_6
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-virtual {v2}, Lcom/sec/chaton/localbackup/BackupListView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    sget-object v2, Lcom/sec/chaton/localbackup/BackupListView;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/p;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/localbackup/i;->a:Lcom/sec/chaton/localbackup/BackupListView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/BackupListView;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method
