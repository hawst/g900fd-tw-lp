.class public Lcom/sec/chaton/localbackup/ActivitySecretKey;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivitySecretKey.java"

# interfaces
.implements Lcom/sec/chaton/localbackup/z;


# instance fields
.field private a:Lcom/sec/chaton/localbackup/SecretKeyView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/ActivitySecretKey;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    .line 29
    iget-object v0, p0, Lcom/sec/chaton/localbackup/ActivitySecretKey;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 53
    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivitySecretKey;->c()V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivitySecretKey;->finish()V

    .line 59
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/localbackup/ActivitySecretKey;->setResult(I)V

    .line 63
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    if-eqz p1, :cond_0

    .line 23
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 40
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 47
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 43
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/ActivitySecretKey;->finish()V

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
