.class Lcom/sec/chaton/localbackup/c;
.super Ljava/lang/Object;
.source "ActivityLocalBackup.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 124
    if-eqz p2, :cond_1

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 126
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 127
    iget-object v1, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    .line 141
    :goto_0
    return v0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "auto_backup_on"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Z)Z

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->c(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)V

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;Z)V

    :goto_1
    move v0, v1

    .line 141
    goto :goto_0

    .line 137
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->b(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "auto_backup_on"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 138
    iget-object v2, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v2}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->d(Lcom/sec/chaton/localbackup/ActivityLocalBackup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;)V

    .line 139
    iget-object v2, p0, Lcom/sec/chaton/localbackup/c;->a:Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-static {v2, v0}, Lcom/sec/chaton/localbackup/ActivityLocalBackup;->a(Lcom/sec/chaton/localbackup/ActivityLocalBackup;Z)Z

    goto :goto_1
.end method
