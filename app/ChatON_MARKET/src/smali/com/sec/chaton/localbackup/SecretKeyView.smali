.class public Lcom/sec/chaton/localbackup/SecretKeyView;
.super Landroid/support/v4/app/Fragment;
.source "SecretKeyView.java"


# instance fields
.field private final a:I

.field private final b:I

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/LinearLayout;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Landroid/app/Dialog;

.field private m:Landroid/app/Dialog;

.field private n:Landroid/app/Dialog;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Landroid/view/Menu;

.field private r:Landroid/app/Activity;

.field private s:Lcom/sec/chaton/localbackup/y;

.field private t:Landroid/widget/LinearLayout;

.field private u:Lcom/sec/chaton/localbackup/z;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 42
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->a:I

    .line 43
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->b:I

    .line 403
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/SecretKeyView;Lcom/sec/chaton/localbackup/y;)Lcom/sec/chaton/localbackup/y;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->s:Lcom/sec/chaton/localbackup/y;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/SecretKeyView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->i:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->m:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e8

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b033a

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/localbackup/w;

    invoke-direct {v2, p0}, Lcom/sec/chaton/localbackup/w;-><init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->m:Landroid/app/Dialog;

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->m:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->m:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->m:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 255
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/SecretKeyView;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->q:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->q:Landroid/view/Menu;

    const v1, 0x7f070593

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 229
    :cond_0
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->n:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0347

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0346

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/localbackup/x;

    invoke-direct {v2, p0}, Lcom/sec/chaton/localbackup/x;-><init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->n:Landroid/app/Dialog;

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->n:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->n:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->n:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 278
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/SecretKeyView;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->k:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->i:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->l:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->l:Landroid/app/Dialog;

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->l:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->l:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->l:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 330
    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 462
    new-instance v0, Lcom/sec/chaton/localbackup/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/localbackup/y;-><init>(Lcom/sec/chaton/localbackup/SecretKeyView;Lcom/sec/chaton/localbackup/u;)V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->s:Lcom/sec/chaton/localbackup/y;

    .line 464
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->s:Lcom/sec/chaton/localbackup/y;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/y;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 469
    :goto_0
    return-void

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->s:Lcom/sec/chaton/localbackup/y;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/y;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->f:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/localbackup/SecretKeyView;)Lcom/sec/chaton/localbackup/z;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->u:Lcom/sec/chaton/localbackup/z;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/localbackup/SecretKeyView;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->c()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->l:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/localbackup/SecretKeyView;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->b()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 95
    iput-object p1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/localbackup/z;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->u:Lcom/sec/chaton/localbackup/z;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "%s must implement ITrunkViewListener."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    const-string v1, "password_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->k:Z

    .line 83
    const-string v1, "target_filepath"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->o:Ljava/lang/String;

    .line 84
    sget-object v1, Lcom/sec/chaton/localbackup/BackupListView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->p:Ljava/lang/String;

    .line 86
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->a()V

    .line 89
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 336
    const v0, 0x7f0f0012

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 338
    iput-object p1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->q:Landroid/view/Menu;

    .line 340
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Z)V

    .line 342
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 343
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v4, 0x8

    const/4 v5, 0x0

    .line 119
    const v0, 0x7f0300e6

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 121
    const v0, 0x7f070405

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->t:Landroid/widget/LinearLayout;

    .line 122
    const v0, 0x7f070407

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->c:Landroid/widget/EditText;

    .line 123
    const v0, 0x7f070408

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->d:Landroid/widget/TextView;

    .line 124
    const v0, 0x7f07040a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->f:Landroid/widget/EditText;

    .line 125
    const v0, 0x7f07040b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->g:Landroid/widget/TextView;

    .line 127
    iget-boolean v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->k:Z

    if-nez v0, :cond_0

    .line 128
    const v0, 0x7f070409

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->h:Landroid/widget/LinearLayout;

    .line 129
    const v0, 0x7f070406

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->e:Landroid/widget/TextView;

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->e:Landroid/widget/TextView;

    const v2, 0x7f0b0305

    invoke-virtual {p0, v2}, Lcom/sec/chaton/localbackup/SecretKeyView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 135
    :cond_0
    new-instance v0, Landroid/text/method/PasswordTransformationMethod;

    invoke-direct {v0}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    .line 136
    iget-object v2, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->c:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 138
    iget-boolean v2, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->k:Z

    if-eqz v2, :cond_1

    .line 139
    iget-object v2, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->f:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    const v3, 0x7f0b0345

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->c:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/chaton/localbackup/u;

    invoke-direct {v2, p0}, Lcom/sec/chaton/localbackup/u;-><init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->f:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/chaton/localbackup/v;

    invoke-direct {v2, p0}, Lcom/sec/chaton/localbackup/v;-><init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 216
    invoke-virtual {p0, v6}, Lcom/sec/chaton/localbackup/SecretKeyView;->setHasOptionsMenu(Z)V

    .line 222
    return-object v1
.end method

.method public onDetach()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 106
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->s:Lcom/sec/chaton/localbackup/y;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->s:Lcom/sec/chaton/localbackup/y;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/localbackup/y;->cancel(Z)Z

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    const v1, 0x7f0b0339

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 111
    :cond_0
    iput-object v3, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->r:Landroid/app/Activity;

    .line 112
    iput-object v3, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->u:Lcom/sec/chaton/localbackup/z;

    .line 113
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 349
    new-instance v0, Lcom/sec/chaton/localbackup/q;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/q;-><init>()V

    .line 350
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 400
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 353
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->i:Ljava/lang/String;

    .line 354
    iget-boolean v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->k:Z

    if-eqz v1, :cond_2

    .line 355
    iget-object v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->j:Ljava/lang/String;

    .line 357
    iget-object v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 358
    iget-object v1, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361
    :try_start_0
    sget-object v1, Lcom/sec/chaton/localbackup/s;->a:Lcom/sec/chaton/localbackup/s;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/s;Ljava/lang/String;)Ljava/lang/String;

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->u:Lcom/sec/chaton/localbackup/z;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/chaton/localbackup/z;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 363
    :catch_0
    move-exception v0

    .line 365
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 369
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->b()V

    goto :goto_0

    .line 375
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->i:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 377
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->d()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 384
    :catch_1
    move-exception v0

    .line 386
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 387
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->b()V

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 380
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->b()V

    .line 382
    iget-object v0, p0, Lcom/sec/chaton/localbackup/SecretKeyView;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 396
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/localbackup/SecretKeyView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 350
    nop

    :pswitch_data_0
    .packed-switch 0x7f070593
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
