.class Lcom/sec/chaton/localbackup/y;
.super Landroid/os/AsyncTask;
.source "SecretKeyView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/sec/chaton/localbackup/q;

.field final synthetic b:Lcom/sec/chaton/localbackup/SecretKeyView;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V
    .locals 1

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 405
    new-instance v0, Lcom/sec/chaton/localbackup/q;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/q;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/y;->a:Lcom/sec/chaton/localbackup/q;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/localbackup/SecretKeyView;Lcom/sec/chaton/localbackup/u;)V
    .locals 0

    .prologue
    .line 403
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/y;-><init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 420
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->a:Lcom/sec/chaton/localbackup/q;

    sget-object v1, Lcom/sec/chaton/localbackup/t;->b:Lcom/sec/chaton/localbackup/t;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v2}, Lcom/sec/chaton/localbackup/SecretKeyView;->c(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v3}, Lcom/sec/chaton/localbackup/SecretKeyView;->i(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-static {}, Lcom/sec/chaton/localbackup/database/b;->b()V

    .line 422
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 428
    :goto_0
    return-object v0

    .line 424
    :catch_0
    move-exception v0

    .line 426
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 428
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 434
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->e(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 456
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->j(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->j(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 444
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 445
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 446
    sget-object v1, Lcom/sec/chaton/localbackup/BackupListView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v2}, Lcom/sec/chaton/localbackup/SecretKeyView;->k(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    iget-object v1, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->startActivity(Landroid/content/Intent;)V

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->g(Lcom/sec/chaton/localbackup/SecretKeyView;)Lcom/sec/chaton/localbackup/z;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/chaton/localbackup/z;->a(Z)V

    .line 455
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Lcom/sec/chaton/localbackup/y;)Lcom/sec/chaton/localbackup/y;

    goto :goto_0

    .line 450
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "backup_checkkey"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->l(Lcom/sec/chaton/localbackup/SecretKeyView;)V

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 403
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/y;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 403
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/localbackup/y;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 410
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/localbackup/y;->b:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->h(Lcom/sec/chaton/localbackup/SecretKeyView;)V

    .line 413
    return-void
.end method
