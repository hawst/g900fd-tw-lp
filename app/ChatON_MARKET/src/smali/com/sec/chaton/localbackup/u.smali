.class Lcom/sec/chaton/localbackup/u;
.super Ljava/lang/Object;
.source "SecretKeyView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/localbackup/SecretKeyView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/SecretKeyView;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7

    .prologue
    const/16 v3, 0x10

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Ljava/lang/String;)Ljava/lang/String;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->b(Lcom/sec/chaton/localbackup/SecretKeyView;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->c(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->c(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v6, :cond_1

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->d(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lt v0, v6, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->f(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0, v5}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Z)V

    .line 183
    :goto_1
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->c(Lcom/sec/chaton/localbackup/SecretKeyView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v3, :cond_2

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->e(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v1}, Lcom/sec/chaton/localbackup/SecretKeyView;->e(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b033d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 165
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->d(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0, v4}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Z)V

    goto :goto_1

    .line 178
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0, v5}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Z)V

    goto :goto_1

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/localbackup/u;->a:Lcom/sec/chaton/localbackup/SecretKeyView;

    invoke-static {v0, v4}, Lcom/sec/chaton/localbackup/SecretKeyView;->a(Lcom/sec/chaton/localbackup/SecretKeyView;Z)V

    goto :goto_1
.end method
