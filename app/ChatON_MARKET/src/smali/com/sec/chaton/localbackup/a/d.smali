.class public Lcom/sec/chaton/localbackup/a/d;
.super Ljava/lang/Object;
.source "BackupFileDownloadHelperTask.java"


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/sec/chaton/e/w;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-wide p1, p0, Lcom/sec/chaton/localbackup/a/d;->a:J

    .line 257
    iput-object p3, p0, Lcom/sec/chaton/localbackup/a/d;->b:Ljava/lang/String;

    .line 258
    iput-object p4, p0, Lcom/sec/chaton/localbackup/a/d;->c:Ljava/lang/String;

    .line 259
    iput-object p5, p0, Lcom/sec/chaton/localbackup/a/d;->d:Ljava/lang/String;

    .line 260
    iput-object p6, p0, Lcom/sec/chaton/localbackup/a/d;->e:Lcom/sec/chaton/e/w;

    .line 261
    iput-object p7, p0, Lcom/sec/chaton/localbackup/a/d;->f:Landroid/view/View;

    .line 262
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/localbackup/a/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/localbackup/a/d;)Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/d;->e:Lcom/sec/chaton/e/w;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/localbackup/a/d;)J
    .locals 2

    .prologue
    .line 247
    iget-wide v0, p0, Lcom/sec/chaton/localbackup/a/d;->a:J

    return-wide v0
.end method

.method static synthetic d(Lcom/sec/chaton/localbackup/a/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/localbackup/a/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/d;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 265
    iget-wide v0, p0, Lcom/sec/chaton/localbackup/a/d;->a:J

    return-wide v0
.end method

.method public b()Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/d;->e:Lcom/sec/chaton/e/w;

    return-object v0
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/chaton/localbackup/a/d;->f:Landroid/view/View;

    return-object v0
.end method
