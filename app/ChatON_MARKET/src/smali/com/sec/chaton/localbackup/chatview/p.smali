.class Lcom/sec/chaton/localbackup/chatview/p;
.super Ljava/lang/Object;
.source "ChatViewFragment.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 841
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatview/p;->b:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    iput-object p2, p0, Lcom/sec/chaton/localbackup/chatview/p;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 847
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 848
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/p;->b:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 849
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 857
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/p;->b:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    move-result-object v0

    const v1, 0x7f0b0246

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 858
    const/4 v0, 0x1

    return v0

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/p;->b:Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;->g(Lcom/sec/chaton/localbackup/chatview/ChatViewFragment;)Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/chatview/ChatViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 853
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/p;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    goto :goto_0
.end method
