.class public Lcom/sec/chaton/localbackup/chatview/d;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "ChatViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:I

.field private e:Lcom/sec/common/f/c;

.field private f:Ljava/util/Calendar;

.field private g:Ljava/util/Calendar;

.field private h:Ljava/util/Date;

.field private i:Ljava/util/Date;

.field private j:Lcom/sec/chaton/localbackup/chatview/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "message_session_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "message_content"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "message_is_failed"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/localbackup/chatview/d;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;Lcom/sec/common/f/c;Lcom/sec/chaton/localbackup/chatview/g;)V
    .locals 7

    .prologue
    .line 68
    sget-object v4, Lcom/sec/chaton/localbackup/chatview/d;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 69
    iput-object p1, p0, Lcom/sec/chaton/localbackup/chatview/d;->b:Landroid/content/Context;

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->c:Landroid/view/LayoutInflater;

    .line 71
    iput p2, p0, Lcom/sec/chaton/localbackup/chatview/d;->d:I

    .line 72
    iput-object p4, p0, Lcom/sec/chaton/localbackup/chatview/d;->e:Lcom/sec/common/f/c;

    .line 74
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->f:Ljava/util/Calendar;

    .line 75
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->g:Ljava/util/Calendar;

    .line 77
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->h:Ljava/util/Date;

    .line 78
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->i:Ljava/util/Date;

    .line 79
    iput-object p5, p0, Lcom/sec/chaton/localbackup/chatview/d;->j:Lcom/sec/chaton/localbackup/chatview/g;

    .line 80
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 307
    .line 310
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 311
    const-string v3, "content"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 312
    const-string v3, "message"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 313
    const-string v4, "text"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 314
    const-string v3, "appInfo"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 315
    const-string v3, "name"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 316
    const-string v3, "param"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 319
    const-string v0, ""

    .line 320
    const-string v0, ""

    .line 321
    const-string v0, ""

    move v3, v2

    .line 323
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 324
    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 326
    const-string v7, "OS"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 327
    const-string v8, "deviceType"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 328
    const-string v9, "executeUri"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 330
    const-string v9, "android"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "phone"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 336
    :goto_1
    if-eqz v1, :cond_1

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b024d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 344
    :goto_2
    return-object v0

    .line 323
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 340
    :cond_1
    const-string v1, "%s\n\n%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_2

    .line 341
    :catch_0
    move-exception v0

    .line 342
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 344
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private a(IIILcom/sec/chaton/localbackup/chatview/f;JLandroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->i:Ljava/util/Date;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 146
    if-eqz p2, :cond_0

    .line 147
    iget-object v0, p4, Lcom/sec/chaton/localbackup/chatview/f;->e:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 178
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 155
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    if-eqz v1, :cond_3

    .line 156
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 161
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->i:Ljava/util/Date;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    const-string v4, "message_time"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Date;->setTime(J)V

    .line 163
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->g:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->i:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->f:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->g:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->f:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->g:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->f:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->g:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 172
    :cond_2
    iget-object v0, p4, Lcom/sec/chaton/localbackup/chatview/f;->e:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 174
    iget-object v0, p4, Lcom/sec/chaton/localbackup/chatview/f;->f:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/d;->h:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 158
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    .line 176
    :cond_4
    iget-object v0, p4, Lcom/sec/chaton/localbackup/chatview/f;->e:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/chatview/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 350
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    :goto_0
    return-void

    .line 353
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->b:Landroid/content/Context;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v1, v0, v2}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;JLcom/sec/chaton/localbackup/chatview/f;Landroid/view/View;)V
    .locals 13

    .prologue
    .line 182
    .line 183
    move-object/from16 v0, p7

    iget-object v2, v0, Lcom/sec/chaton/localbackup/chatview/f;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    sget-object v8, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    .line 185
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 186
    const/4 v5, 0x0

    .line 187
    move-object/from16 v0, p7

    iget-object v2, v0, Lcom/sec/chaton/localbackup/chatview/f;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 189
    sget-object v4, Lcom/sec/chaton/localbackup/chatview/e;->a:[I

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 301
    :cond_0
    :goto_0
    :pswitch_0
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->a:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 302
    new-instance v2, Lcom/sec/chaton/localbackup/chatview/s;

    const/4 v6, 0x1

    move-object/from16 v4, p4

    move-object/from16 v7, p3

    move-wide/from16 v9, p5

    invoke-direct/range {v2 .. v10}, Lcom/sec/chaton/localbackup/chatview/s;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/sec/chaton/e/w;Lcom/sec/chaton/multimedia/doc/b;J)V

    .line 303
    iget-object v3, p0, Lcom/sec/chaton/localbackup/chatview/d;->e:Lcom/sec/common/f/c;

    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, v4, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 304
    return-void

    .line 192
    :pswitch_1
    const/4 v5, 0x1

    .line 193
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 194
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-direct {p0, p1, v4}, Lcom/sec/chaton/localbackup/chatview/d;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 196
    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 197
    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0

    .line 201
    :pswitch_2
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-direct {p0, p1, v4}, Lcom/sec/chaton/localbackup/chatview/d;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 202
    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 203
    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0

    .line 206
    :pswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 207
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-direct {p0, p1, v4}, Lcom/sec/chaton/localbackup/chatview/d;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 208
    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 209
    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 213
    :pswitch_4
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 215
    const v6, 0xea60

    div-int v6, v4, v6

    .line 216
    div-int/lit16 v4, v4, 0x3e8

    rem-int/lit8 v4, v4, 0x3c

    .line 218
    move-object/from16 v0, p7

    iget-object v7, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const-string v9, "%d:%02d/%d:%02d"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v11

    const/4 v6, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v10, v6

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_1
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 226
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 219
    :catch_0
    move-exception v4

    .line 220
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 229
    :pswitch_5
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    const/high16 v4, 0x420c0000    # 35.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 233
    const/high16 v4, 0x42020000    # 32.5f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 236
    :pswitch_6
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    const/high16 v4, 0x420c0000    # 35.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 240
    const/high16 v4, 0x42020000    # 32.5f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 243
    :pswitch_7
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-direct {p0, p1, v4}, Lcom/sec/chaton/localbackup/chatview/d;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 245
    const/high16 v4, 0x420c0000    # 35.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 246
    const/high16 v4, 0x42020000    # 32.5f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 250
    :pswitch_8
    invoke-static {p2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v8

    .line 251
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 255
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 258
    :pswitch_9
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/chaton/localbackup/chatview/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 262
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 265
    :pswitch_a
    invoke-static {p1}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v4

    .line 266
    if-eqz v4, :cond_0

    .line 267
    const-string v6, "%s\n\n%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v4, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    aput-object v10, v7, v9

    const/4 v9, 0x1

    iget-object v4, v4, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->name:Ljava/lang/String;

    aput-object v4, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 268
    move-object/from16 v0, p7

    iget-object v6, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 272
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 277
    :pswitch_b
    invoke-static {p1}, Lcom/sec/chaton/specialbuddy/g;->c(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v4

    .line 278
    if-eqz v4, :cond_0

    .line 279
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b03c5

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 280
    const-string v7, "%s\n\n%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v6, 0x1

    iget-object v4, v4, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    aput-object v4, v9, v6

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 281
    move-object/from16 v0, p7

    iget-object v6, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 285
    const/high16 v4, 0x42180000    # 38.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
    .end packed-switch
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x6

    const/4 v5, 0x0

    .line 359
    const/4 v1, 0x0

    .line 360
    new-array v2, v5, [Ljava/lang/String;

    .line 362
    if-eqz p1, :cond_1

    .line 363
    const-string v2, "\n"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 364
    array-length v3, v2

    if-le v3, v0, :cond_1

    const-string v3, "mixed"

    aget-object v4, v2, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 365
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 366
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 367
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 372
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 17

    .prologue
    .line 84
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/chaton/localbackup/chatview/f;

    .line 86
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iput v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->h:I

    .line 88
    const-string v1, "message_sever_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 89
    const-string v1, "message_content_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v9

    .line 90
    const-string v1, "message_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 91
    const-string v1, "message_content"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 92
    const-string v1, "message_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 93
    const-string v1, "message_sender"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 94
    const-string v2, "message_download_uri"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 95
    const-string v2, "message_formatted"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 96
    const-string v2, "message_is_failed"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 97
    const-string v2, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 99
    iput-wide v11, v5, Lcom/sec/chaton/localbackup/chatview/f;->i:J

    .line 101
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v6, v7}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/chaton/localbackup/chatview/d;->h:Ljava/util/Date;

    .line 102
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/chaton/localbackup/chatview/d;->f:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/localbackup/chatview/d;->h:Ljava/util/Date;

    invoke-virtual {v8, v15}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 104
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v8

    const-string v15, "chaton_id"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->b:Landroid/widget/TextView;

    const v2, 0x7f0b0094

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 112
    :goto_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    move-object/from16 v1, p0

    move-object/from16 v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/sec/chaton/localbackup/chatview/d;->a(IIILcom/sec/chaton/localbackup/chatview/f;JLandroid/database/Cursor;)V

    .line 114
    const-wide/16 v1, 0x0

    cmp-long v1, v6, v1

    if-nez v1, :cond_2

    .line 115
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->d:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    :goto_1
    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v9, v1, :cond_3

    .line 123
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->g:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 124
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/localbackup/chatview/d;->b:Landroid/content/Context;

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v2, v13, v3}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    :goto_2
    const/4 v1, -0x1

    if-ne v4, v1, :cond_4

    .line 133
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->j:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 139
    return-void

    .line 106
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 109
    :cond_1
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->b:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v8, 0x7f0b00b4

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 117
    :cond_2
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->d:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->d:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/localbackup/chatview/d;->b:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/localbackup/chatview/d;->f:Ljava/util/Calendar;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto :goto_1

    .line 127
    :cond_3
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->g:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 128
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    move-object/from16 v6, p0

    move-object v7, v13

    move-object v8, v14

    move-object v13, v5

    move-object/from16 v14, p1

    .line 129
    invoke-direct/range {v6 .. v14}, Lcom/sec/chaton/localbackup/chatview/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;JLcom/sec/chaton/localbackup/chatview/f;Landroid/view/View;)V

    goto :goto_2

    .line 135
    :cond_4
    iget-object v1, v5, Lcom/sec/chaton/localbackup/chatview/f;->j:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/chaton/localbackup/chatview/d;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 378
    new-instance v2, Lcom/sec/chaton/localbackup/chatview/f;

    invoke-direct {v2}, Lcom/sec/chaton/localbackup/chatview/f;-><init>()V

    .line 380
    const v0, 0x7f070348

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->a:Landroid/widget/ImageView;

    .line 381
    const v0, 0x7f070344

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->b:Landroid/widget/TextView;

    .line 382
    const v0, 0x7f0702ff

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->c:Landroid/widget/TextView;

    .line 383
    const v0, 0x7f070345

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->d:Landroid/widget/TextView;

    .line 384
    const v0, 0x7f07019e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->e:Landroid/widget/RelativeLayout;

    .line 385
    const v0, 0x7f0701a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->f:Landroid/widget/TextView;

    .line 386
    const v0, 0x7f070347

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->g:Landroid/widget/LinearLayout;

    .line 387
    const v0, 0x7f070346

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/localbackup/chatview/f;->j:Landroid/widget/ImageView;

    .line 389
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 390
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 411
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/chatview/f;

    .line 413
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    iget v2, v0, Lcom/sec/chaton/localbackup/chatview/f;->h:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 414
    iget-object v1, p0, Lcom/sec/chaton/localbackup/chatview/d;->j:Lcom/sec/chaton/localbackup/chatview/g;

    iget v0, v0, Lcom/sec/chaton/localbackup/chatview/f;->h:I

    iget-object v2, p0, Lcom/sec/chaton/localbackup/chatview/d;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1, v0, v2}, Lcom/sec/chaton/localbackup/chatview/g;->a(Landroid/view/View;ILandroid/database/Cursor;)V

    .line 417
    :cond_0
    return-void
.end method
