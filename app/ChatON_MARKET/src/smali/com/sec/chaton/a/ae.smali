.class public final Lcom/sec/chaton/a/ae;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ad;",
        "Lcom/sec/chaton/a/ae;",
        ">;",
        "Lcom/sec/chaton/a/af;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/ag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 24161
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 24329
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->c:Ljava/lang/Object;

    .line 24365
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->d:Ljava/lang/Object;

    .line 24401
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    .line 24162
    invoke-direct {p0}, Lcom/sec/chaton/a/ae;->g()V

    .line 24163
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ae;)Lcom/sec/chaton/a/ad;
    .locals 1

    .prologue
    .line 24156
    invoke-direct {p0}, Lcom/sec/chaton/a/ae;->i()Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24156
    invoke-static {}, Lcom/sec/chaton/a/ae;->h()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 24166
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24168
    new-instance v0, Lcom/sec/chaton/a/ae;

    invoke-direct {v0}, Lcom/sec/chaton/a/ae;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/ad;
    .locals 2

    .prologue
    .line 24202
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->e()Lcom/sec/chaton/a/ad;

    move-result-object v0

    .line 24203
    invoke-virtual {v0}, Lcom/sec/chaton/a/ad;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 24204
    invoke-static {v0}, Lcom/sec/chaton/a/ae;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 24207
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 24404
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 24405
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    .line 24406
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24408
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ae;
    .locals 2

    .prologue
    .line 24172
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 24173
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ae;->b:J

    .line 24174
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24175
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->c:Ljava/lang/Object;

    .line 24176
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24177
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->d:Ljava/lang/Object;

    .line 24178
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24179
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    .line 24180
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24181
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24316
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24317
    iput-wide p1, p0, Lcom/sec/chaton/a/ae;->b:J

    .line 24319
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ae;
    .locals 2

    .prologue
    .line 24268
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 24269
    sparse-switch v0, :sswitch_data_0

    .line 24274
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ae;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24276
    :sswitch_0
    return-object p0

    .line 24281
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24282
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ae;->b:J

    goto :goto_0

    .line 24286
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24287
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->c:Ljava/lang/Object;

    goto :goto_0

    .line 24291
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24292
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->d:Ljava/lang/Object;

    goto :goto_0

    .line 24296
    :sswitch_4
    invoke-static {}, Lcom/sec/chaton/a/ag;->newBuilder()Lcom/sec/chaton/a/ah;

    move-result-object v0

    .line 24297
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 24298
    invoke-virtual {v0}, Lcom/sec/chaton/a/ah;->e()Lcom/sec/chaton/a/ag;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ae;->a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ae;

    goto :goto_0

    .line 24269
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ad;)Lcom/sec/chaton/a/ae;
    .locals 2

    .prologue
    .line 24236
    invoke-static {}, Lcom/sec/chaton/a/ad;->a()Lcom/sec/chaton/a/ad;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 24256
    :cond_0
    :goto_0
    return-object p0

    .line 24237
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24238
    invoke-virtual {p1}, Lcom/sec/chaton/a/ad;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ae;->a(J)Lcom/sec/chaton/a/ae;

    .line 24240
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ad;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 24241
    invoke-virtual {p1}, Lcom/sec/chaton/a/ad;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ae;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ae;

    .line 24243
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/ad;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 24244
    invoke-virtual {p1}, Lcom/sec/chaton/a/ad;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ae;->b(Ljava/lang/String;)Lcom/sec/chaton/a/ae;

    .line 24246
    :cond_4
    invoke-static {p1}, Lcom/sec/chaton/a/ad;->b(Lcom/sec/chaton/a/ad;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24247
    iget-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 24248
    invoke-static {p1}, Lcom/sec/chaton/a/ad;->b(Lcom/sec/chaton/a/ad;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    .line 24249
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    goto :goto_0

    .line 24251
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/a/ae;->j()V

    .line 24252
    iget-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/ad;->b(Lcom/sec/chaton/a/ad;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24437
    if-nez p1, :cond_0

    .line 24438
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24440
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/ae;->j()V

    .line 24441
    iget-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24443
    return-object p0
.end method

.method public a(Ljava/lang/Iterable;)Lcom/sec/chaton/a/ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sec/chaton/a/ag;",
            ">;)",
            "Lcom/sec/chaton/a/ae;"
        }
    .end annotation

    .prologue
    .line 24471
    invoke-direct {p0}, Lcom/sec/chaton/a/ae;->j()V

    .line 24472
    iget-object v0, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 24474
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24344
    if-nez p1, :cond_0

    .line 24345
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24347
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24348
    iput-object p1, p0, Lcom/sec/chaton/a/ae;->c:Ljava/lang/Object;

    .line 24350
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ae;
    .locals 2

    .prologue
    .line 24185
    invoke-static {}, Lcom/sec/chaton/a/ae;->h()Lcom/sec/chaton/a/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->e()Lcom/sec/chaton/a/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ae;->a(Lcom/sec/chaton/a/ad;)Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24380
    if-nez p1, :cond_0

    .line 24381
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24383
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ae;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24384
    iput-object p1, p0, Lcom/sec/chaton/a/ae;->d:Ljava/lang/Object;

    .line 24386
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->d()Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->e()Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ad;
    .locals 1

    .prologue
    .line 24189
    invoke-static {}, Lcom/sec/chaton/a/ad;->a()Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->a()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->a()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->b()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->b()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->b()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->b()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ad;
    .locals 2

    .prologue
    .line 24193
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->e()Lcom/sec/chaton/a/ad;

    move-result-object v0

    .line 24194
    invoke-virtual {v0}, Lcom/sec/chaton/a/ad;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 24195
    invoke-static {v0}, Lcom/sec/chaton/a/ae;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 24197
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ad;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 24211
    new-instance v2, Lcom/sec/chaton/a/ad;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ad;-><init>(Lcom/sec/chaton/a/ae;Lcom/sec/chaton/a/b;)V

    .line 24212
    iget v3, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24213
    const/4 v1, 0x0

    .line 24214
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 24217
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/ae;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/ad;->a(Lcom/sec/chaton/a/ad;J)J

    .line 24218
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 24219
    or-int/lit8 v0, v0, 0x2

    .line 24221
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ae;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ad;->a(Lcom/sec/chaton/a/ad;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24222
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 24223
    or-int/lit8 v0, v0, 0x4

    .line 24225
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/ae;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ad;->b(Lcom/sec/chaton/a/ad;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24226
    iget v1, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 24227
    iget-object v1, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    .line 24228
    iget v1, p0, Lcom/sec/chaton/a/ae;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/sec/chaton/a/ae;->a:I

    .line 24230
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/ae;->e:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ad;->a(Lcom/sec/chaton/a/ad;Ljava/util/List;)Ljava/util/List;

    .line 24231
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ad;->a(Lcom/sec/chaton/a/ad;I)I

    .line 24232
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->c()Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0}, Lcom/sec/chaton/a/ae;->c()Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 24260
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ae;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    check-cast p1, Lcom/sec/chaton/a/ad;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ae;->a(Lcom/sec/chaton/a/ad;)Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24156
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ae;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method
