.class public final Lcom/sec/chaton/a/ah;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ai;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ag;",
        "Lcom/sec/chaton/a/ah;",
        ">;",
        "Lcom/sec/chaton/a/ai;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/sec/chaton/a/bc;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/protobuf/LazyStringList;

.field private e:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 6828
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 6979
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->b:Lcom/sec/chaton/a/bc;

    .line 7003
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->c:Ljava/lang/Object;

    .line 7039
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    .line 6829
    invoke-direct {p0}, Lcom/sec/chaton/a/ah;->g()V

    .line 6830
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 6823
    invoke-static {}, Lcom/sec/chaton/a/ah;->h()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 6833
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 6835
    new-instance v0, Lcom/sec/chaton/a/ah;

    invoke-direct {v0}, Lcom/sec/chaton/a/ah;-><init>()V

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 7041
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 7042
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    .line 7043
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 7045
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ah;
    .locals 2

    .prologue
    .line 6839
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 6840
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->b:Lcom/sec/chaton/a/bc;

    .line 6841
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6842
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->c:Ljava/lang/Object;

    .line 6843
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6844
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    .line 6845
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6846
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ah;->e:J

    .line 6847
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6848
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 7103
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 7104
    iput-wide p1, p0, Lcom/sec/chaton/a/ah;->e:J

    .line 7106
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ah;
    .locals 2

    .prologue
    .line 6936
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 6937
    sparse-switch v0, :sswitch_data_0

    .line 6942
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ah;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6944
    :sswitch_0
    return-object p0

    .line 6949
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 6950
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 6951
    if-eqz v0, :cond_0

    .line 6952
    iget v1, p0, Lcom/sec/chaton/a/ah;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6953
    iput-object v0, p0, Lcom/sec/chaton/a/ah;->b:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 6958
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6959
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->c:Ljava/lang/Object;

    goto :goto_0

    .line 6963
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/chaton/a/ah;->i()V

    .line 6964
    iget-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 6968
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6969
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ah;->e:J

    goto :goto_0

    .line 6937
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ah;
    .locals 2

    .prologue
    .line 6904
    invoke-static {}, Lcom/sec/chaton/a/ag;->a()Lcom/sec/chaton/a/ag;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 6924
    :cond_0
    :goto_0
    return-object p0

    .line 6905
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ag;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6906
    invoke-virtual {p1}, Lcom/sec/chaton/a/ag;->d()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ah;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/ah;

    .line 6908
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ag;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6909
    invoke-virtual {p1}, Lcom/sec/chaton/a/ag;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ah;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ah;

    .line 6911
    :cond_3
    invoke-static {p1}, Lcom/sec/chaton/a/ag;->b(Lcom/sec/chaton/a/ag;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 6912
    iget-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6913
    invoke-static {p1}, Lcom/sec/chaton/a/ag;->b(Lcom/sec/chaton/a/ag;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    .line 6914
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6921
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ag;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6922
    invoke-virtual {p1}, Lcom/sec/chaton/a/ag;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ah;->a(J)Lcom/sec/chaton/a/ah;

    goto :goto_0

    .line 6916
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/a/ah;->i()V

    .line 6917
    iget-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/ag;->b(Lcom/sec/chaton/a/ag;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 6987
    if-nez p1, :cond_0

    .line 6988
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6990
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6991
    iput-object p1, p0, Lcom/sec/chaton/a/ah;->b:Lcom/sec/chaton/a/bc;

    .line 6993
    return-object p0
.end method

.method public a(Ljava/lang/Iterable;)Lcom/sec/chaton/a/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/chaton/a/ah;"
        }
    .end annotation

    .prologue
    .line 7077
    invoke-direct {p0}, Lcom/sec/chaton/a/ah;->i()V

    .line 7078
    iget-object v0, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 7080
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 7018
    if-nez p1, :cond_0

    .line 7019
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7021
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ah;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 7022
    iput-object p1, p0, Lcom/sec/chaton/a/ah;->c:Ljava/lang/Object;

    .line 7024
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ah;
    .locals 2

    .prologue
    .line 6852
    invoke-static {}, Lcom/sec/chaton/a/ah;->h()Lcom/sec/chaton/a/ah;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->e()Lcom/sec/chaton/a/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ah;->a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->d()Lcom/sec/chaton/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->e()Lcom/sec/chaton/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ag;
    .locals 1

    .prologue
    .line 6856
    invoke-static {}, Lcom/sec/chaton/a/ag;->a()Lcom/sec/chaton/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->a()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->a()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->b()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->b()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->b()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->b()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ag;
    .locals 2

    .prologue
    .line 6860
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->e()Lcom/sec/chaton/a/ag;

    move-result-object v0

    .line 6861
    invoke-virtual {v0}, Lcom/sec/chaton/a/ag;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6862
    invoke-static {v0}, Lcom/sec/chaton/a/ah;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 6864
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ag;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 6878
    new-instance v2, Lcom/sec/chaton/a/ag;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ag;-><init>(Lcom/sec/chaton/a/ah;Lcom/sec/chaton/a/b;)V

    .line 6879
    iget v3, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6880
    const/4 v1, 0x0

    .line 6881
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 6884
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/ah;->b:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ag;->a(Lcom/sec/chaton/a/ag;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 6885
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 6886
    or-int/lit8 v0, v0, 0x2

    .line 6888
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ah;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ag;->a(Lcom/sec/chaton/a/ag;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6889
    iget v1, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 6890
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    .line 6892
    iget v1, p0, Lcom/sec/chaton/a/ah;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/sec/chaton/a/ah;->a:I

    .line 6894
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/ah;->d:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ag;->a(Lcom/sec/chaton/a/ag;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 6895
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 6896
    or-int/lit8 v0, v0, 0x4

    .line 6898
    :cond_2
    iget-wide v3, p0, Lcom/sec/chaton/a/ah;->e:J

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/a/ag;->a(Lcom/sec/chaton/a/ag;J)J

    .line 6899
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ag;->a(Lcom/sec/chaton/a/ag;I)I

    .line 6900
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->c()Lcom/sec/chaton/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0}, Lcom/sec/chaton/a/ah;->c()Lcom/sec/chaton/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 6928
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ah;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    check-cast p1, Lcom/sec/chaton/a/ag;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ah;->a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6823
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ah;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method
