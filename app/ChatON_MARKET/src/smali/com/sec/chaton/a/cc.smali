.class public final Lcom/sec/chaton/a/cc;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ce;


# static fields
.field private static final a:Lcom/sec/chaton/a/cc;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/bc;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Lcom/google/protobuf/LazyStringList;

.field private i:J

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15739
    new-instance v0, Lcom/sec/chaton/a/cc;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/cc;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/cc;->a:Lcom/sec/chaton/a/cc;

    .line 15740
    sget-object v0, Lcom/sec/chaton/a/cc;->a:Lcom/sec/chaton/a/cc;

    invoke-direct {v0}, Lcom/sec/chaton/a/cc;->u()V

    .line 15741
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cd;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14986
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 15149
    iput-byte v0, p0, Lcom/sec/chaton/a/cc;->j:B

    .line 15184
    iput v0, p0, Lcom/sec/chaton/a/cc;->k:I

    .line 14987
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cd;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 14981
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/cc;-><init>(Lcom/sec/chaton/a/cd;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14988
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 15149
    iput-byte v0, p0, Lcom/sec/chaton/a/cc;->j:B

    .line 15184
    iput v0, p0, Lcom/sec/chaton/a/cc;->k:I

    .line 14988
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cc;I)I
    .locals 0

    .prologue
    .line 14981
    iput p1, p0, Lcom/sec/chaton/a/cc;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cc;J)J
    .locals 0

    .prologue
    .line 14981
    iput-wide p1, p0, Lcom/sec/chaton/a/cc;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cc;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 14981
    iput-object p1, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cc;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 14981
    iput-object p1, p0, Lcom/sec/chaton/a/cc;->d:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/cc;
    .locals 1

    .prologue
    .line 14992
    sget-object v0, Lcom/sec/chaton/a/cc;->a:Lcom/sec/chaton/a/cc;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/cc;
    .locals 1

    .prologue
    .line 15248
    invoke-static {}, Lcom/sec/chaton/a/cc;->newBuilder()Lcom/sec/chaton/a/cd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cd;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cd;

    invoke-static {v0}, Lcom/sec/chaton/a/cd;->a(Lcom/sec/chaton/a/cd;)Lcom/sec/chaton/a/cc;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/cc;)Lcom/sec/chaton/a/cd;
    .locals 1

    .prologue
    .line 15304
    invoke-static {}, Lcom/sec/chaton/a/cc;->newBuilder()Lcom/sec/chaton/a/cd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cd;->a(Lcom/sec/chaton/a/cc;)Lcom/sec/chaton/a/cd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/cc;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 14981
    iput-object p1, p0, Lcom/sec/chaton/a/cc;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cc;J)J
    .locals 0

    .prologue
    .line 14981
    iput-wide p1, p0, Lcom/sec/chaton/a/cc;->i:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cc;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 14981
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/cc;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 14981
    iput-object p1, p0, Lcom/sec/chaton/a/cc;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/cc;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 14981
    iput-object p1, p0, Lcom/sec/chaton/a/cc;->g:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cd;
    .locals 1

    .prologue
    .line 15301
    invoke-static {}, Lcom/sec/chaton/a/cd;->l()Lcom/sec/chaton/a/cd;

    move-result-object v0

    return-object v0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 15041
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->e:Ljava/lang/Object;

    .line 15042
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15043
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 15045
    iput-object v0, p0, Lcom/sec/chaton/a/cc;->e:Ljava/lang/Object;

    .line 15048
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 15073
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->f:Ljava/lang/Object;

    .line 15074
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15075
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 15077
    iput-object v0, p0, Lcom/sec/chaton/a/cc;->f:Ljava/lang/Object;

    .line 15080
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 15105
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->g:Ljava/lang/Object;

    .line 15106
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15107
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 15109
    iput-object v0, p0, Lcom/sec/chaton/a/cc;->g:Ljava/lang/Object;

    .line 15112
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private u()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 15141
    iput-wide v1, p0, Lcom/sec/chaton/a/cc;->c:J

    .line 15142
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/cc;->d:Lcom/sec/chaton/a/bc;

    .line 15143
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cc;->e:Ljava/lang/Object;

    .line 15144
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cc;->f:Ljava/lang/Object;

    .line 15145
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cc;->g:Ljava/lang/Object;

    .line 15146
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    .line 15147
    iput-wide v1, p0, Lcom/sec/chaton/a/cc;->i:J

    .line 15148
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/cc;
    .locals 1

    .prologue
    .line 14996
    sget-object v0, Lcom/sec/chaton/a/cc;->a:Lcom/sec/chaton/a/cc;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 15004
    iget v1, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 15007
    iget-wide v0, p0, Lcom/sec/chaton/a/cc;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 15014
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 15017
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->d:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 15024
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14981
    invoke-virtual {p0}, Lcom/sec/chaton/a/cc;->b()Lcom/sec/chaton/a/cc;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 15186
    iget v0, p0, Lcom/sec/chaton/a/cc;->k:I

    .line 15187
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 15224
    :goto_0
    return v0

    .line 15190
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 15191
    iget-wide v2, p0, Lcom/sec/chaton/a/cc;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 15194
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 15195
    iget-object v2, p0, Lcom/sec/chaton/a/cc;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 15198
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 15199
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/cc;->r()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 15202
    :cond_2
    iget v2, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 15203
    invoke-direct {p0}, Lcom/sec/chaton/a/cc;->s()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 15206
    :cond_3
    iget v2, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 15207
    const/4 v2, 0x5

    invoke-direct {p0}, Lcom/sec/chaton/a/cc;->t()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    move v2, v1

    .line 15212
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 15213
    iget-object v3, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 15212
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 15216
    :cond_5
    add-int/2addr v0, v2

    .line 15217
    invoke-virtual {p0}, Lcom/sec/chaton/a/cc;->m()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 15219
    iget v1, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 15220
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/sec/chaton/a/cc;->i:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 15223
    :cond_6
    iput v0, p0, Lcom/sec/chaton/a/cc;->k:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15027
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->e:Ljava/lang/Object;

    .line 15028
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15029
    check-cast v0, Ljava/lang/String;

    .line 15037
    :goto_0
    return-object v0

    .line 15031
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 15033
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 15034
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15035
    iput-object v1, p0, Lcom/sec/chaton/a/cc;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 15037
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 15056
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 15151
    iget-byte v1, p0, Lcom/sec/chaton/a/cc;->j:B

    .line 15152
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 15155
    :goto_0
    return v0

    .line 15152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 15154
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/cc;->j:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15059
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->f:Ljava/lang/Object;

    .line 15060
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15061
    check-cast v0, Ljava/lang/String;

    .line 15069
    :goto_0
    return-object v0

    .line 15063
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 15065
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 15066
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15067
    iput-object v1, p0, Lcom/sec/chaton/a/cc;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 15069
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 15088
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15091
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->g:Ljava/lang/Object;

    .line 15092
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15093
    check-cast v0, Ljava/lang/String;

    .line 15101
    :goto_0
    return-object v0

    .line 15095
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 15097
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 15098
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15099
    iput-object v1, p0, Lcom/sec/chaton/a/cc;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 15101
    goto :goto_0
.end method

.method public m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15121
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 15134
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14981
    invoke-virtual {p0}, Lcom/sec/chaton/a/cc;->p()Lcom/sec/chaton/a/cd;

    move-result-object v0

    return-object v0
.end method

.method public o()J
    .locals 2

    .prologue
    .line 15137
    iget-wide v0, p0, Lcom/sec/chaton/a/cc;->i:J

    return-wide v0
.end method

.method public p()Lcom/sec/chaton/a/cd;
    .locals 1

    .prologue
    .line 15302
    invoke-static {}, Lcom/sec/chaton/a/cc;->newBuilder()Lcom/sec/chaton/a/cd;

    move-result-object v0

    return-object v0
.end method

.method public q()Lcom/sec/chaton/a/cd;
    .locals 1

    .prologue
    .line 15306
    invoke-static {p0}, Lcom/sec/chaton/a/cc;->a(Lcom/sec/chaton/a/cc;)Lcom/sec/chaton/a/cd;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14981
    invoke-virtual {p0}, Lcom/sec/chaton/a/cc;->q()Lcom/sec/chaton/a/cd;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15231
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 15160
    invoke-virtual {p0}, Lcom/sec/chaton/a/cc;->getSerializedSize()I

    .line 15161
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 15162
    iget-wide v0, p0, Lcom/sec/chaton/a/cc;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 15164
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 15165
    iget-object v0, p0, Lcom/sec/chaton/a/cc;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 15167
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 15168
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/cc;->r()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15170
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 15171
    invoke-direct {p0}, Lcom/sec/chaton/a/cc;->s()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15173
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 15174
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sec/chaton/a/cc;->t()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15176
    :cond_4
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 15177
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/chaton/a/cc;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15176
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15179
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/cc;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 15180
    const/4 v0, 0x7

    iget-wide v1, p0, Lcom/sec/chaton/a/cc;->i:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 15182
    :cond_6
    return-void
.end method
