.class public final Lcom/sec/chaton/a/bk;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/bj;",
        "Lcom/sec/chaton/a/bk;",
        ">;",
        "Lcom/sec/chaton/a/bo;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/bl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/sec/chaton/a/ej;

.field private e:I

.field private f:J

.field private g:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14514
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 14693
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->b:Ljava/lang/Object;

    .line 14729
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    .line 14818
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    .line 14903
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->g:Ljava/lang/Object;

    .line 14515
    invoke-direct {p0}, Lcom/sec/chaton/a/bk;->l()V

    .line 14516
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bk;)Lcom/sec/chaton/a/bj;
    .locals 1

    .prologue
    .line 14509
    invoke-direct {p0}, Lcom/sec/chaton/a/bk;->n()Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k()Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14509
    invoke-static {}, Lcom/sec/chaton/a/bk;->m()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 0

    .prologue
    .line 14519
    return-void
.end method

.method private static m()Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14521
    new-instance v0, Lcom/sec/chaton/a/bk;

    invoke-direct {v0}, Lcom/sec/chaton/a/bk;-><init>()V

    return-object v0
.end method

.method private n()Lcom/sec/chaton/a/bj;
    .locals 2

    .prologue
    .line 14559
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->e()Lcom/sec/chaton/a/bj;

    move-result-object v0

    .line 14560
    invoke-virtual {v0}, Lcom/sec/chaton/a/bj;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 14561
    invoke-static {v0}, Lcom/sec/chaton/a/bk;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 14564
    :cond_0
    return-object v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 14732
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 14733
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    .line 14734
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14736
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/bk;
    .locals 2

    .prologue
    .line 14525
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 14526
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->b:Ljava/lang/Object;

    .line 14527
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14528
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    .line 14529
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14530
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    .line 14531
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14532
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/bk;->e:I

    .line 14533
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14534
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bk;->f:J

    .line 14535
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14536
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->g:Ljava/lang/Object;

    .line 14537
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14538
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14890
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14891
    iput-wide p1, p0, Lcom/sec/chaton/a/bk;->f:J

    .line 14893
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bk;
    .locals 2

    .prologue
    .line 14639
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 14640
    sparse-switch v0, :sswitch_data_0

    .line 14645
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/bk;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14647
    :sswitch_0
    return-object p0

    .line 14652
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14653
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->b:Ljava/lang/Object;

    goto :goto_0

    .line 14657
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/bl;->newBuilder()Lcom/sec/chaton/a/bm;

    move-result-object v0

    .line 14658
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 14659
    invoke-virtual {v0}, Lcom/sec/chaton/a/bm;->e()Lcom/sec/chaton/a/bl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bk;

    goto :goto_0

    .line 14663
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 14664
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14665
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->i()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 14667
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 14668
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bk;

    goto :goto_0

    .line 14672
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14673
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/a/bk;->e:I

    goto :goto_0

    .line 14677
    :sswitch_5
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14678
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/bk;->f:J

    goto :goto_0

    .line 14682
    :sswitch_6
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14683
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->g:Ljava/lang/Object;

    goto :goto_0

    .line 14640
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bj;)Lcom/sec/chaton/a/bk;
    .locals 2

    .prologue
    .line 14601
    invoke-static {}, Lcom/sec/chaton/a/bj;->a()Lcom/sec/chaton/a/bj;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 14627
    :cond_0
    :goto_0
    return-object p0

    .line 14602
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 14603
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bk;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bk;

    .line 14605
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/a/bj;->b(Lcom/sec/chaton/a/bj;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 14606
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 14607
    invoke-static {p1}, Lcom/sec/chaton/a/bj;->b(Lcom/sec/chaton/a/bj;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    .line 14608
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14615
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 14616
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->h()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bk;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bk;

    .line 14618
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 14619
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->j()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bk;->b(I)Lcom/sec/chaton/a/bk;

    .line 14621
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 14622
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->l()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/bk;->a(J)Lcom/sec/chaton/a/bk;

    .line 14624
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14625
    invoke-virtual {p1}, Lcom/sec/chaton/a/bj;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bk;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bk;

    goto :goto_0

    .line 14610
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/a/bk;->o()V

    .line 14611
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/bj;->b(Lcom/sec/chaton/a/bj;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14765
    if-nez p1, :cond_0

    .line 14766
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14768
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/bk;->o()V

    .line 14769
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 14771
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14826
    if-nez p1, :cond_0

    .line 14827
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14829
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    .line 14831
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14832
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14708
    if-nez p1, :cond_0

    .line 14709
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14711
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14712
    iput-object p1, p0, Lcom/sec/chaton/a/bk;->b:Ljava/lang/Object;

    .line 14714
    return-object p0
.end method

.method public a(I)Lcom/sec/chaton/a/bl;
    .locals 1

    .prologue
    .line 14745
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bl;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/bk;
    .locals 2

    .prologue
    .line 14542
    invoke-static {}, Lcom/sec/chaton/a/bk;->m()Lcom/sec/chaton/a/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->e()Lcom/sec/chaton/a/bj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/bj;)Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14869
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14870
    iput p1, p0, Lcom/sec/chaton/a/bk;->e:I

    .line 14872
    return-object p0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bk;
    .locals 2

    .prologue
    .line 14842
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 14844
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    .line 14850
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14851
    return-object p0

    .line 14847
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14918
    if-nez p1, :cond_0

    .line 14919
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14921
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14922
    iput-object p1, p0, Lcom/sec/chaton/a/bk;->g:Ljava/lang/Object;

    .line 14924
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->d()Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->e()Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/bj;
    .locals 1

    .prologue
    .line 14546
    invoke-static {}, Lcom/sec/chaton/a/bj;->a()Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->a()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->a()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->b()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->b()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->b()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->b()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/bj;
    .locals 2

    .prologue
    .line 14550
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->e()Lcom/sec/chaton/a/bj;

    move-result-object v0

    .line 14551
    invoke-virtual {v0}, Lcom/sec/chaton/a/bj;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 14552
    invoke-static {v0}, Lcom/sec/chaton/a/bk;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 14554
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/bj;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 14568
    new-instance v2, Lcom/sec/chaton/a/bj;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/bj;-><init>(Lcom/sec/chaton/a/bk;Lcom/sec/chaton/a/b;)V

    .line 14569
    iget v3, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14570
    const/4 v1, 0x0

    .line 14571
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 14574
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/bk;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bj;->a(Lcom/sec/chaton/a/bj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14575
    iget v1, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 14576
    iget-object v1, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    .line 14577
    iget v1, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/chaton/a/bk;->a:I

    .line 14579
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bj;->a(Lcom/sec/chaton/a/bj;Ljava/util/List;)Ljava/util/List;

    .line 14580
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 14581
    or-int/lit8 v0, v0, 0x2

    .line 14583
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bj;->a(Lcom/sec/chaton/a/bj;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 14584
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 14585
    or-int/lit8 v0, v0, 0x4

    .line 14587
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/bk;->e:I

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bj;->a(Lcom/sec/chaton/a/bj;I)I

    .line 14588
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 14589
    or-int/lit8 v0, v0, 0x8

    .line 14591
    :cond_3
    iget-wide v4, p0, Lcom/sec/chaton/a/bk;->f:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/bj;->a(Lcom/sec/chaton/a/bj;J)J

    .line 14592
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 14593
    or-int/lit8 v0, v0, 0x10

    .line 14595
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/bk;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bj;->b(Lcom/sec/chaton/a/bj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14596
    invoke-static {v2, v0}, Lcom/sec/chaton/a/bj;->b(Lcom/sec/chaton/a/bj;I)I

    .line 14597
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/bl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14739
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 14742
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->c()Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0}, Lcom/sec/chaton/a/bk;->c()Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 14820
    iget v0, p0, Lcom/sec/chaton/a/bk;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 14823
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 14631
    const/4 v0, 0x1

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14908
    iget-object v0, p0, Lcom/sec/chaton/a/bk;->g:Ljava/lang/Object;

    .line 14909
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 14910
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 14911
    iput-object v0, p0, Lcom/sec/chaton/a/bk;->g:Ljava/lang/Object;

    .line 14914
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bk;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    check-cast p1, Lcom/sec/chaton/a/bj;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/bj;)Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14509
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bk;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method
