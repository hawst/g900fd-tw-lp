.class public final Lcom/sec/chaton/a/dh;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/di;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/dg;",
        "Lcom/sec/chaton/a/dh;",
        ">;",
        "Lcom/sec/chaton/a/di;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/dp;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/protobuf/LazyStringList;

.field private g:Lcom/google/protobuf/LazyStringList;

.field private h:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 17384
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 17607
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->c:Lcom/sec/chaton/a/dp;

    .line 17631
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->d:Ljava/lang/Object;

    .line 17667
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->e:Ljava/lang/Object;

    .line 17703
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    .line 17759
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    .line 17385
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->n()V

    .line 17386
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dh;)Lcom/sec/chaton/a/dg;
    .locals 1

    .prologue
    .line 17379
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->p()Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m()Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17379
    invoke-static {}, Lcom/sec/chaton/a/dh;->o()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 17389
    return-void
.end method

.method private static o()Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17391
    new-instance v0, Lcom/sec/chaton/a/dh;

    invoke-direct {v0}, Lcom/sec/chaton/a/dh;-><init>()V

    return-object v0
.end method

.method private p()Lcom/sec/chaton/a/dg;
    .locals 2

    .prologue
    .line 17431
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->e()Lcom/sec/chaton/a/dg;

    move-result-object v0

    .line 17432
    invoke-virtual {v0}, Lcom/sec/chaton/a/dg;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17433
    invoke-static {v0}, Lcom/sec/chaton/a/dh;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 17436
    :cond_0
    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 17705
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 17706
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    .line 17707
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17709
    :cond_0
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 17761
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 17762
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    .line 17763
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17765
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/dh;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 17395
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 17396
    iput-wide v1, p0, Lcom/sec/chaton/a/dh;->b:J

    .line 17397
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17398
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->c:Lcom/sec/chaton/a/dp;

    .line 17399
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17400
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->d:Ljava/lang/Object;

    .line 17401
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17402
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->e:Ljava/lang/Object;

    .line 17403
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17404
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    .line 17405
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17406
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    .line 17407
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17408
    iput-wide v1, p0, Lcom/sec/chaton/a/dh;->h:J

    .line 17409
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17410
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17594
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17595
    iput-wide p1, p0, Lcom/sec/chaton/a/dh;->b:J

    .line 17597
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dh;
    .locals 2

    .prologue
    .line 17528
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 17529
    sparse-switch v0, :sswitch_data_0

    .line 17534
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/dh;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17536
    :sswitch_0
    return-object p0

    .line 17541
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17542
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/dh;->b:J

    goto :goto_0

    .line 17546
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 17547
    invoke-static {v0}, Lcom/sec/chaton/a/dp;->a(I)Lcom/sec/chaton/a/dp;

    move-result-object v0

    .line 17548
    if-eqz v0, :cond_0

    .line 17549
    iget v1, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17550
    iput-object v0, p0, Lcom/sec/chaton/a/dh;->c:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 17555
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17556
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->d:Ljava/lang/Object;

    goto :goto_0

    .line 17560
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17561
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->e:Ljava/lang/Object;

    goto :goto_0

    .line 17565
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->q()V

    .line 17566
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 17570
    :sswitch_6
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->r()V

    .line 17571
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 17575
    :sswitch_7
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17576
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/dh;->h:J

    goto :goto_0

    .line 17529
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dg;)Lcom/sec/chaton/a/dh;
    .locals 2

    .prologue
    .line 17480
    invoke-static {}, Lcom/sec/chaton/a/dg;->a()Lcom/sec/chaton/a/dg;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 17516
    :cond_0
    :goto_0
    return-object p0

    .line 17481
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 17482
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/dh;->a(J)Lcom/sec/chaton/a/dh;

    .line 17484
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 17485
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->f()Lcom/sec/chaton/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dh;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dh;

    .line 17487
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 17488
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dh;->a(Ljava/lang/String;)Lcom/sec/chaton/a/dh;

    .line 17490
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 17491
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dh;->b(Ljava/lang/String;)Lcom/sec/chaton/a/dh;

    .line 17493
    :cond_5
    invoke-static {p1}, Lcom/sec/chaton/a/dg;->b(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 17494
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 17495
    invoke-static {p1}, Lcom/sec/chaton/a/dg;->b(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    .line 17496
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17503
    :cond_6
    :goto_1
    invoke-static {p1}, Lcom/sec/chaton/a/dg;->c(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 17504
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 17505
    invoke-static {p1}, Lcom/sec/chaton/a/dg;->c(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    .line 17506
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17513
    :cond_7
    :goto_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17514
    invoke-virtual {p1}, Lcom/sec/chaton/a/dg;->n()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/dh;->b(J)Lcom/sec/chaton/a/dh;

    goto :goto_0

    .line 17498
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->q()V

    .line 17499
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/dg;->b(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 17508
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->r()V

    .line 17509
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/dg;->c(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17615
    if-nez p1, :cond_0

    .line 17616
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17618
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17619
    iput-object p1, p0, Lcom/sec/chaton/a/dh;->c:Lcom/sec/chaton/a/dp;

    .line 17621
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17646
    if-nez p1, :cond_0

    .line 17647
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17649
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17650
    iput-object p1, p0, Lcom/sec/chaton/a/dh;->d:Ljava/lang/Object;

    .line 17652
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/dh;
    .locals 2

    .prologue
    .line 17414
    invoke-static {}, Lcom/sec/chaton/a/dh;->o()Lcom/sec/chaton/a/dh;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->e()Lcom/sec/chaton/a/dg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/dh;->a(Lcom/sec/chaton/a/dg;)Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17823
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17824
    iput-wide p1, p0, Lcom/sec/chaton/a/dh;->h:J

    .line 17826
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17682
    if-nez p1, :cond_0

    .line 17683
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17685
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17686
    iput-object p1, p0, Lcom/sec/chaton/a/dh;->e:Ljava/lang/Object;

    .line 17688
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->d()Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->e()Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/dg;
    .locals 1

    .prologue
    .line 17418
    invoke-static {}, Lcom/sec/chaton/a/dg;->a()Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17731
    if-nez p1, :cond_0

    .line 17732
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17734
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->q()V

    .line 17735
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 17737
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->a()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->a()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->b()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->b()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->b()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->b()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/dg;
    .locals 2

    .prologue
    .line 17422
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->e()Lcom/sec/chaton/a/dg;

    move-result-object v0

    .line 17423
    invoke-virtual {v0}, Lcom/sec/chaton/a/dg;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17424
    invoke-static {v0}, Lcom/sec/chaton/a/dh;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 17426
    :cond_0
    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17787
    if-nez p1, :cond_0

    .line 17788
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17790
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/dh;->r()V

    .line 17791
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 17793
    return-object p0
.end method

.method public e()Lcom/sec/chaton/a/dg;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 17440
    new-instance v2, Lcom/sec/chaton/a/dg;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/dg;-><init>(Lcom/sec/chaton/a/dh;Lcom/sec/chaton/a/b;)V

    .line 17441
    iget v3, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17442
    const/4 v1, 0x0

    .line 17443
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    .line 17446
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/dh;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/dg;->a(Lcom/sec/chaton/a/dg;J)J

    .line 17447
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 17448
    or-int/lit8 v0, v0, 0x2

    .line 17450
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/dh;->c:Lcom/sec/chaton/a/dp;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dg;->a(Lcom/sec/chaton/a/dg;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;

    .line 17451
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 17452
    or-int/lit8 v0, v0, 0x4

    .line 17454
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/dh;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dg;->a(Lcom/sec/chaton/a/dg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17455
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 17456
    or-int/lit8 v0, v0, 0x8

    .line 17458
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/dh;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dg;->b(Lcom/sec/chaton/a/dg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17459
    iget v1, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 17460
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    .line 17462
    iget v1, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17464
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dg;->a(Lcom/sec/chaton/a/dg;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 17465
    iget v1, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 17466
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    .line 17468
    iget v1, p0, Lcom/sec/chaton/a/dh;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/sec/chaton/a/dh;->a:I

    .line 17470
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dg;->b(Lcom/sec/chaton/a/dg;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 17471
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 17472
    or-int/lit8 v0, v0, 0x10

    .line 17474
    :cond_5
    iget-wide v3, p0, Lcom/sec/chaton/a/dh;->h:J

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/a/dg;->b(Lcom/sec/chaton/a/dg;J)J

    .line 17475
    invoke-static {v2, v0}, Lcom/sec/chaton/a/dg;->a(Lcom/sec/chaton/a/dg;I)I

    .line 17476
    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 17591
    iget-wide v0, p0, Lcom/sec/chaton/a/dh;->b:J

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17672
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->e:Ljava/lang/Object;

    .line 17673
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 17674
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 17675
    iput-object v0, p0, Lcom/sec/chaton/a/dh;->e:Ljava/lang/Object;

    .line 17678
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->c()Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0}, Lcom/sec/chaton/a/dh;->c()Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17712
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 17715
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 17520
    const/4 v0, 0x1

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17768
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 17771
    iget-object v0, p0, Lcom/sec/chaton/a/dh;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 17820
    iget-wide v0, p0, Lcom/sec/chaton/a/dh;->h:J

    return-wide v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dh;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    check-cast p1, Lcom/sec/chaton/a/dg;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/dh;->a(Lcom/sec/chaton/a/dg;)Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17379
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dh;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method
