.class public final enum Lcom/sec/chaton/a/bc;
.super Ljava/lang/Enum;
.source "SSMGPB.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/bc;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/bc;

.field public static final enum b:Lcom/sec/chaton/a/bc;

.field public static final enum c:Lcom/sec/chaton/a/bc;

.field public static final enum d:Lcom/sec/chaton/a/bc;

.field public static final enum e:Lcom/sec/chaton/a/bc;

.field private static f:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sec/chaton/a/bc;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic h:[Lcom/sec/chaton/a/bc;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/sec/chaton/a/bc;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sec/chaton/a/bc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    .line 14
    new-instance v0, Lcom/sec/chaton/a/bc;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sec/chaton/a/bc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bc;->b:Lcom/sec/chaton/a/bc;

    .line 15
    new-instance v0, Lcom/sec/chaton/a/bc;

    const-string v1, "BROADCAST"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sec/chaton/a/bc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bc;->c:Lcom/sec/chaton/a/bc;

    .line 16
    new-instance v0, Lcom/sec/chaton/a/bc;

    const-string v1, "BR2"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sec/chaton/a/bc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bc;->d:Lcom/sec/chaton/a/bc;

    .line 17
    new-instance v0, Lcom/sec/chaton/a/bc;

    const-string v1, "MONOLOGUE"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sec/chaton/a/bc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bc;->e:Lcom/sec/chaton/a/bc;

    .line 11
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/a/bc;

    sget-object v1, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/bc;->b:Lcom/sec/chaton/a/bc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/a/bc;->c:Lcom/sec/chaton/a/bc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/a/bc;->d:Lcom/sec/chaton/a/bc;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/a/bc;->e:Lcom/sec/chaton/a/bc;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/a/bc;->h:[Lcom/sec/chaton/a/bc;

    .line 45
    new-instance v0, Lcom/sec/chaton/a/bd;

    invoke-direct {v0}, Lcom/sec/chaton/a/bd;-><init>()V

    sput-object v0, Lcom/sec/chaton/a/bc;->f:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput p4, p0, Lcom/sec/chaton/a/bc;->g:I

    .line 56
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 30
    packed-switch p0, :pswitch_data_0

    .line 36
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 31
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 32
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/a/bc;->b:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 33
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/a/bc;->c:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 34
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/a/bc;->d:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 35
    :pswitch_4
    sget-object v0, Lcom/sec/chaton/a/bc;->e:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/chaton/a/bc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/chaton/a/bc;->h:[Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/bc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/bc;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/chaton/a/bc;->g:I

    return v0
.end method
