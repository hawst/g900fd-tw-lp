.class public final Lcom/sec/chaton/a/fc;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/fd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/fb;",
        "Lcom/sec/chaton/a/fc;",
        ">;",
        "Lcom/sec/chaton/a/fd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/google/protobuf/LazyStringList;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dm;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1006
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 1183
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    .line 1239
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    .line 1328
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->e:Ljava/lang/Object;

    .line 1007
    invoke-direct {p0}, Lcom/sec/chaton/a/fc;->g()V

    .line 1008
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 1001
    invoke-static {}, Lcom/sec/chaton/a/fc;->h()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 1011
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 1013
    new-instance v0, Lcom/sec/chaton/a/fc;

    invoke-direct {v0}, Lcom/sec/chaton/a/fc;-><init>()V

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1185
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1186
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    .line 1187
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1189
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1242
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1243
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    .line 1244
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1246
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/fc;
    .locals 2

    .prologue
    .line 1017
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1018
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/fc;->b:J

    .line 1019
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1020
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    .line 1021
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1022
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    .line 1023
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1024
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->e:Ljava/lang/Object;

    .line 1025
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1026
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 1170
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1171
    iput-wide p1, p0, Lcom/sec/chaton/a/fc;->b:J

    .line 1173
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/fc;
    .locals 2

    .prologue
    .line 1122
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1123
    sparse-switch v0, :sswitch_data_0

    .line 1128
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/fc;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1130
    :sswitch_0
    return-object p0

    .line 1135
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1136
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/fc;->b:J

    goto :goto_0

    .line 1140
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/a/fc;->i()V

    .line 1141
    iget-object v0, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 1145
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/dm;->newBuilder()Lcom/sec/chaton/a/dn;

    move-result-object v0

    .line 1146
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 1147
    invoke-virtual {v0}, Lcom/sec/chaton/a/dn;->e()Lcom/sec/chaton/a/dm;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/fc;->a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/fc;

    goto :goto_0

    .line 1151
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1152
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->e:Ljava/lang/Object;

    goto :goto_0

    .line 1123
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 1275
    if-nez p1, :cond_0

    .line 1276
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1278
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/fc;->j()V

    .line 1279
    iget-object v0, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1281
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;
    .locals 2

    .prologue
    .line 1083
    invoke-static {}, Lcom/sec/chaton/a/fb;->a()Lcom/sec/chaton/a/fb;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1110
    :cond_0
    :goto_0
    return-object p0

    .line 1084
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/fb;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1085
    invoke-virtual {p1}, Lcom/sec/chaton/a/fb;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/fc;->a(J)Lcom/sec/chaton/a/fc;

    .line 1087
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/a/fb;->b(Lcom/sec/chaton/a/fb;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1088
    iget-object v0, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1089
    invoke-static {p1}, Lcom/sec/chaton/a/fb;->b(Lcom/sec/chaton/a/fb;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    .line 1090
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1097
    :cond_3
    :goto_1
    invoke-static {p1}, Lcom/sec/chaton/a/fb;->c(Lcom/sec/chaton/a/fb;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1098
    iget-object v0, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1099
    invoke-static {p1}, Lcom/sec/chaton/a/fb;->c(Lcom/sec/chaton/a/fb;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    .line 1100
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1107
    :cond_4
    :goto_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/fb;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1108
    invoke-virtual {p1}, Lcom/sec/chaton/a/fb;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/fc;->a(Ljava/lang/String;)Lcom/sec/chaton/a/fc;

    goto :goto_0

    .line 1092
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/a/fc;->i()V

    .line 1093
    iget-object v0, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/fb;->b(Lcom/sec/chaton/a/fb;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1102
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/a/fc;->j()V

    .line 1103
    iget-object v0, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/fb;->c(Lcom/sec/chaton/a/fb;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 1343
    if-nez p1, :cond_0

    .line 1344
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1346
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/fc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1347
    iput-object p1, p0, Lcom/sec/chaton/a/fc;->e:Ljava/lang/Object;

    .line 1349
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/fc;
    .locals 2

    .prologue
    .line 1030
    invoke-static {}, Lcom/sec/chaton/a/fc;->h()Lcom/sec/chaton/a/fc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->e()Lcom/sec/chaton/a/fb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/fc;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->d()Lcom/sec/chaton/a/fb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->e()Lcom/sec/chaton/a/fb;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/fb;
    .locals 1

    .prologue
    .line 1034
    invoke-static {}, Lcom/sec/chaton/a/fb;->a()Lcom/sec/chaton/a/fb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->a()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->a()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->b()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->b()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->b()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->b()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/fb;
    .locals 2

    .prologue
    .line 1038
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->e()Lcom/sec/chaton/a/fb;

    move-result-object v0

    .line 1039
    invoke-virtual {v0}, Lcom/sec/chaton/a/fb;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1040
    invoke-static {v0}, Lcom/sec/chaton/a/fc;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1042
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/fb;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1056
    new-instance v2, Lcom/sec/chaton/a/fb;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/fb;-><init>(Lcom/sec/chaton/a/fc;Lcom/sec/chaton/a/b;)V

    .line 1057
    iget v3, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1058
    const/4 v1, 0x0

    .line 1059
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 1062
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/fc;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/fb;->a(Lcom/sec/chaton/a/fb;J)J

    .line 1063
    iget v1, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1064
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    .line 1066
    iget v1, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1068
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/fc;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/fb;->a(Lcom/sec/chaton/a/fb;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 1069
    iget v1, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1070
    iget-object v1, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    .line 1071
    iget v1, p0, Lcom/sec/chaton/a/fc;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/sec/chaton/a/fc;->a:I

    .line 1073
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/fc;->d:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/fb;->a(Lcom/sec/chaton/a/fb;Ljava/util/List;)Ljava/util/List;

    .line 1074
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 1075
    or-int/lit8 v0, v0, 0x2

    .line 1077
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/fc;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/fb;->a(Lcom/sec/chaton/a/fb;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1078
    invoke-static {v2, v0}, Lcom/sec/chaton/a/fb;->a(Lcom/sec/chaton/a/fb;I)I

    .line 1079
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->c()Lcom/sec/chaton/a/fb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/a/fc;->c()Lcom/sec/chaton/a/fb;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1114
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/fc;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    check-cast p1, Lcom/sec/chaton/a/fb;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/fc;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/fc;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method
