.class public final Lcom/sec/chaton/a/dd;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/df;


# static fields
.field private static final a:Lcom/sec/chaton/a/dd;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;

.field private e:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18262
    new-instance v0, Lcom/sec/chaton/a/dd;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/dd;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/dd;->a:Lcom/sec/chaton/a/dd;

    .line 18263
    sget-object v0, Lcom/sec/chaton/a/dd;->a:Lcom/sec/chaton/a/dd;

    invoke-direct {v0}, Lcom/sec/chaton/a/dd;->k()V

    .line 18264
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/de;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17866
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 17915
    iput-byte v0, p0, Lcom/sec/chaton/a/dd;->f:B

    .line 17938
    iput v0, p0, Lcom/sec/chaton/a/dd;->g:I

    .line 17867
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/de;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 17861
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/dd;-><init>(Lcom/sec/chaton/a/de;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17868
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 17915
    iput-byte v0, p0, Lcom/sec/chaton/a/dd;->f:B

    .line 17938
    iput v0, p0, Lcom/sec/chaton/a/dd;->g:I

    .line 17868
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dd;I)I
    .locals 0

    .prologue
    .line 17861
    iput p1, p0, Lcom/sec/chaton/a/dd;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dd;J)J
    .locals 0

    .prologue
    .line 17861
    iput-wide p1, p0, Lcom/sec/chaton/a/dd;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/dd;
    .locals 1

    .prologue
    .line 17872
    sget-object v0, Lcom/sec/chaton/a/dd;->a:Lcom/sec/chaton/a/dd;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/dd;
    .locals 1

    .prologue
    .line 17981
    invoke-static {}, Lcom/sec/chaton/a/dd;->newBuilder()Lcom/sec/chaton/a/de;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/de;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/de;

    invoke-static {v0}, Lcom/sec/chaton/a/de;->a(Lcom/sec/chaton/a/de;)Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/dd;)Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18037
    invoke-static {}, Lcom/sec/chaton/a/dd;->newBuilder()Lcom/sec/chaton/a/de;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/de;->a(Lcom/sec/chaton/a/dd;)Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/dd;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 17861
    iput-object p1, p0, Lcom/sec/chaton/a/dd;->d:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dd;J)J
    .locals 0

    .prologue
    .line 17861
    iput-wide p1, p0, Lcom/sec/chaton/a/dd;->e:J

    return-wide p1
.end method

.method private k()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 17911
    iput-wide v1, p0, Lcom/sec/chaton/a/dd;->c:J

    .line 17912
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dd;->d:Lcom/sec/chaton/a/ej;

    .line 17913
    iput-wide v1, p0, Lcom/sec/chaton/a/dd;->e:J

    .line 17914
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18034
    invoke-static {}, Lcom/sec/chaton/a/de;->h()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/dd;
    .locals 1

    .prologue
    .line 17876
    sget-object v0, Lcom/sec/chaton/a/dd;->a:Lcom/sec/chaton/a/dd;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 17884
    iget v1, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 17887
    iget-wide v0, p0, Lcom/sec/chaton/a/dd;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 17894
    iget v0, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 17897
    iget-object v0, p0, Lcom/sec/chaton/a/dd;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 17904
    iget v0, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17861
    invoke-virtual {p0}, Lcom/sec/chaton/a/dd;->b()Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 17940
    iget v0, p0, Lcom/sec/chaton/a/dd;->g:I

    .line 17941
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 17957
    :goto_0
    return v0

    .line 17943
    :cond_0
    const/4 v0, 0x0

    .line 17944
    iget v1, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 17945
    iget-wide v1, p0, Lcom/sec/chaton/a/dd;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17948
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 17949
    iget-object v1, p0, Lcom/sec/chaton/a/dd;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17952
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 17953
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sec/chaton/a/dd;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17956
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/dd;->g:I

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 17907
    iget-wide v0, p0, Lcom/sec/chaton/a/dd;->e:J

    return-wide v0
.end method

.method public i()Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18035
    invoke-static {}, Lcom/sec/chaton/a/dd;->newBuilder()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 17917
    iget-byte v1, p0, Lcom/sec/chaton/a/dd;->f:B

    .line 17918
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 17921
    :goto_0
    return v0

    .line 17918
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 17920
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/dd;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18039
    invoke-static {p0}, Lcom/sec/chaton/a/dd;->a(Lcom/sec/chaton/a/dd;)Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17861
    invoke-virtual {p0}, Lcom/sec/chaton/a/dd;->i()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17861
    invoke-virtual {p0}, Lcom/sec/chaton/a/dd;->j()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17964
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 17926
    invoke-virtual {p0}, Lcom/sec/chaton/a/dd;->getSerializedSize()I

    .line 17927
    iget v0, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 17928
    iget-wide v0, p0, Lcom/sec/chaton/a/dd;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 17930
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 17931
    iget-object v0, p0, Lcom/sec/chaton/a/dd;->d:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 17933
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/dd;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 17934
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sec/chaton/a/dd;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 17936
    :cond_2
    return-void
.end method
