.class public final Lcom/sec/chaton/a/cf;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ch;


# static fields
.field private static final a:Lcom/sec/chaton/a/cf;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/sec/chaton/a/cr;

.field private e:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29600
    new-instance v0, Lcom/sec/chaton/a/cf;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/cf;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/cf;->a:Lcom/sec/chaton/a/cf;

    .line 29601
    sget-object v0, Lcom/sec/chaton/a/cf;->a:Lcom/sec/chaton/a/cf;

    invoke-direct {v0}, Lcom/sec/chaton/a/cf;->l()V

    .line 29602
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cg;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 29167
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 29238
    iput-byte v0, p0, Lcom/sec/chaton/a/cf;->f:B

    .line 29261
    iput v0, p0, Lcom/sec/chaton/a/cf;->g:I

    .line 29168
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cg;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 29162
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/cf;-><init>(Lcom/sec/chaton/a/cg;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 29169
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 29238
    iput-byte v0, p0, Lcom/sec/chaton/a/cf;->f:B

    .line 29261
    iput v0, p0, Lcom/sec/chaton/a/cf;->g:I

    .line 29169
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cf;I)I
    .locals 0

    .prologue
    .line 29162
    iput p1, p0, Lcom/sec/chaton/a/cf;->b:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/cf;
    .locals 1

    .prologue
    .line 29173
    sget-object v0, Lcom/sec/chaton/a/cf;->a:Lcom/sec/chaton/a/cf;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/cf;
    .locals 1

    .prologue
    .line 29304
    invoke-static {}, Lcom/sec/chaton/a/cf;->newBuilder()Lcom/sec/chaton/a/cg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cg;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cg;

    invoke-static {v0}, Lcom/sec/chaton/a/cg;->a(Lcom/sec/chaton/a/cg;)Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/cf;)Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29360
    invoke-static {}, Lcom/sec/chaton/a/cf;->newBuilder()Lcom/sec/chaton/a/cg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cg;->a(Lcom/sec/chaton/a/cf;)Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/cf;Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cr;
    .locals 0

    .prologue
    .line 29162
    iput-object p1, p0, Lcom/sec/chaton/a/cf;->d:Lcom/sec/chaton/a/cr;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cf;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 29162
    iput-object p1, p0, Lcom/sec/chaton/a/cf;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cf;Z)Z
    .locals 0

    .prologue
    .line 29162
    iput-boolean p1, p0, Lcom/sec/chaton/a/cf;->e:Z

    return p1
.end method

.method private k()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 29202
    iget-object v0, p0, Lcom/sec/chaton/a/cf;->c:Ljava/lang/Object;

    .line 29203
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29204
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 29206
    iput-object v0, p0, Lcom/sec/chaton/a/cf;->c:Ljava/lang/Object;

    .line 29209
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 29234
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cf;->c:Ljava/lang/Object;

    .line 29235
    invoke-static {}, Lcom/sec/chaton/a/cr;->a()Lcom/sec/chaton/a/cr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cf;->d:Lcom/sec/chaton/a/cr;

    .line 29236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/cf;->e:Z

    .line 29237
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29357
    invoke-static {}, Lcom/sec/chaton/a/cg;->h()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/cf;
    .locals 1

    .prologue
    .line 29177
    sget-object v0, Lcom/sec/chaton/a/cf;->a:Lcom/sec/chaton/a/cf;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 29185
    iget v1, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29188
    iget-object v0, p0, Lcom/sec/chaton/a/cf;->c:Ljava/lang/Object;

    .line 29189
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29190
    check-cast v0, Ljava/lang/String;

    .line 29198
    :goto_0
    return-object v0

    .line 29192
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 29194
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 29195
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29196
    iput-object v1, p0, Lcom/sec/chaton/a/cf;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 29198
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 29217
    iget v0, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/cr;
    .locals 1

    .prologue
    .line 29220
    iget-object v0, p0, Lcom/sec/chaton/a/cf;->d:Lcom/sec/chaton/a/cr;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 29227
    iget v0, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29162
    invoke-virtual {p0}, Lcom/sec/chaton/a/cf;->b()Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 29263
    iget v0, p0, Lcom/sec/chaton/a/cf;->g:I

    .line 29264
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 29280
    :goto_0
    return v0

    .line 29266
    :cond_0
    const/4 v0, 0x0

    .line 29267
    iget v1, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 29268
    invoke-direct {p0}, Lcom/sec/chaton/a/cf;->k()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29271
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 29272
    iget-object v1, p0, Lcom/sec/chaton/a/cf;->d:Lcom/sec/chaton/a/cr;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29275
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 29276
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sec/chaton/a/cf;->e:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 29279
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/cf;->g:I

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 29230
    iget-boolean v0, p0, Lcom/sec/chaton/a/cf;->e:Z

    return v0
.end method

.method public i()Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29358
    invoke-static {}, Lcom/sec/chaton/a/cf;->newBuilder()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 29240
    iget-byte v1, p0, Lcom/sec/chaton/a/cf;->f:B

    .line 29241
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 29244
    :goto_0
    return v0

    .line 29241
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 29243
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/cf;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29362
    invoke-static {p0}, Lcom/sec/chaton/a/cf;->a(Lcom/sec/chaton/a/cf;)Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29162
    invoke-virtual {p0}, Lcom/sec/chaton/a/cf;->i()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29162
    invoke-virtual {p0}, Lcom/sec/chaton/a/cf;->j()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29287
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 29249
    invoke-virtual {p0}, Lcom/sec/chaton/a/cf;->getSerializedSize()I

    .line 29250
    iget v0, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 29251
    invoke-direct {p0}, Lcom/sec/chaton/a/cf;->k()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 29253
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 29254
    iget-object v0, p0, Lcom/sec/chaton/a/cf;->d:Lcom/sec/chaton/a/cr;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 29256
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/cf;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 29257
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sec/chaton/a/cf;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 29259
    :cond_2
    return-void
.end method
