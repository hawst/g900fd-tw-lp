.class public final Lcom/sec/chaton/a/bz;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cb;


# static fields
.field private static final a:Lcom/sec/chaton/a/bz;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16105
    new-instance v0, Lcom/sec/chaton/a/bz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/bz;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/bz;->a:Lcom/sec/chaton/a/bz;

    .line 16106
    sget-object v0, Lcom/sec/chaton/a/bz;->a:Lcom/sec/chaton/a/bz;

    invoke-direct {v0}, Lcom/sec/chaton/a/bz;->i()V

    .line 16107
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ca;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15762
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 15800
    iput-byte v0, p0, Lcom/sec/chaton/a/bz;->e:B

    .line 15820
    iput v0, p0, Lcom/sec/chaton/a/bz;->f:I

    .line 15763
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ca;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 15757
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/bz;-><init>(Lcom/sec/chaton/a/ca;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15764
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 15800
    iput-byte v0, p0, Lcom/sec/chaton/a/bz;->e:B

    .line 15820
    iput v0, p0, Lcom/sec/chaton/a/bz;->f:I

    .line 15764
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bz;I)I
    .locals 0

    .prologue
    .line 15757
    iput p1, p0, Lcom/sec/chaton/a/bz;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bz;J)J
    .locals 0

    .prologue
    .line 15757
    iput-wide p1, p0, Lcom/sec/chaton/a/bz;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/bz;
    .locals 1

    .prologue
    .line 15768
    sget-object v0, Lcom/sec/chaton/a/bz;->a:Lcom/sec/chaton/a/bz;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/bz;
    .locals 1

    .prologue
    .line 15859
    invoke-static {}, Lcom/sec/chaton/a/bz;->newBuilder()Lcom/sec/chaton/a/ca;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ca;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/ca;

    invoke-static {v0}, Lcom/sec/chaton/a/ca;->a(Lcom/sec/chaton/a/ca;)Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/bz;)Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 15915
    invoke-static {}, Lcom/sec/chaton/a/bz;->newBuilder()Lcom/sec/chaton/a/ca;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ca;->a(Lcom/sec/chaton/a/bz;)Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/bz;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 15757
    iput-object p1, p0, Lcom/sec/chaton/a/bz;->d:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method private i()V
    .locals 2

    .prologue
    .line 15797
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bz;->c:J

    .line 15798
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bz;->d:Lcom/sec/chaton/a/ej;

    .line 15799
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 15912
    invoke-static {}, Lcom/sec/chaton/a/ca;->h()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/bz;
    .locals 1

    .prologue
    .line 15772
    sget-object v0, Lcom/sec/chaton/a/bz;->a:Lcom/sec/chaton/a/bz;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 15780
    iget v1, p0, Lcom/sec/chaton/a/bz;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 15783
    iget-wide v0, p0, Lcom/sec/chaton/a/bz;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 15790
    iget v0, p0, Lcom/sec/chaton/a/bz;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 15793
    iget-object v0, p0, Lcom/sec/chaton/a/bz;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 15913
    invoke-static {}, Lcom/sec/chaton/a/bz;->newBuilder()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15757
    invoke-virtual {p0}, Lcom/sec/chaton/a/bz;->b()Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 15822
    iget v0, p0, Lcom/sec/chaton/a/bz;->f:I

    .line 15823
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 15835
    :goto_0
    return v0

    .line 15825
    :cond_0
    const/4 v0, 0x0

    .line 15826
    iget v1, p0, Lcom/sec/chaton/a/bz;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 15827
    iget-wide v1, p0, Lcom/sec/chaton/a/bz;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 15830
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/bz;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 15831
    iget-object v1, p0, Lcom/sec/chaton/a/bz;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15834
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/bz;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 15917
    invoke-static {p0}, Lcom/sec/chaton/a/bz;->a(Lcom/sec/chaton/a/bz;)Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 15802
    iget-byte v1, p0, Lcom/sec/chaton/a/bz;->e:B

    .line 15803
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 15806
    :goto_0
    return v0

    .line 15803
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 15805
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/bz;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15757
    invoke-virtual {p0}, Lcom/sec/chaton/a/bz;->g()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15757
    invoke-virtual {p0}, Lcom/sec/chaton/a/bz;->h()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15842
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 15811
    invoke-virtual {p0}, Lcom/sec/chaton/a/bz;->getSerializedSize()I

    .line 15812
    iget v0, p0, Lcom/sec/chaton/a/bz;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 15813
    iget-wide v0, p0, Lcom/sec/chaton/a/bz;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 15815
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bz;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 15816
    iget-object v0, p0, Lcom/sec/chaton/a/bz;->d:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 15818
    :cond_1
    return-void
.end method
