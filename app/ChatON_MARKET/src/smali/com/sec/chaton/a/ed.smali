.class public final Lcom/sec/chaton/a/ed;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ef;


# static fields
.field private static final a:Lcom/sec/chaton/a/ed;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32833
    new-instance v0, Lcom/sec/chaton/a/ed;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ed;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ed;->a:Lcom/sec/chaton/a/ed;

    .line 32834
    sget-object v0, Lcom/sec/chaton/a/ed;->a:Lcom/sec/chaton/a/ed;

    invoke-direct {v0}, Lcom/sec/chaton/a/ed;->i()V

    .line 32835
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ee;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 32490
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 32528
    iput-byte v0, p0, Lcom/sec/chaton/a/ed;->e:B

    .line 32548
    iput v0, p0, Lcom/sec/chaton/a/ed;->f:I

    .line 32491
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ee;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 32485
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ed;-><init>(Lcom/sec/chaton/a/ee;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 32492
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32528
    iput-byte v0, p0, Lcom/sec/chaton/a/ed;->e:B

    .line 32548
    iput v0, p0, Lcom/sec/chaton/a/ed;->f:I

    .line 32492
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ed;I)I
    .locals 0

    .prologue
    .line 32485
    iput p1, p0, Lcom/sec/chaton/a/ed;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ed;J)J
    .locals 0

    .prologue
    .line 32485
    iput-wide p1, p0, Lcom/sec/chaton/a/ed;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/ed;
    .locals 1

    .prologue
    .line 32496
    sget-object v0, Lcom/sec/chaton/a/ed;->a:Lcom/sec/chaton/a/ed;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/ed;
    .locals 1

    .prologue
    .line 32587
    invoke-static {}, Lcom/sec/chaton/a/ed;->newBuilder()Lcom/sec/chaton/a/ee;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ee;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/ee;

    invoke-static {v0}, Lcom/sec/chaton/a/ee;->a(Lcom/sec/chaton/a/ee;)Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ed;)Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32643
    invoke-static {}, Lcom/sec/chaton/a/ed;->newBuilder()Lcom/sec/chaton/a/ee;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ee;->a(Lcom/sec/chaton/a/ed;)Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ed;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 32485
    iput-object p1, p0, Lcom/sec/chaton/a/ed;->d:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method private i()V
    .locals 2

    .prologue
    .line 32525
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ed;->c:J

    .line 32526
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ed;->d:Lcom/sec/chaton/a/ej;

    .line 32527
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32640
    invoke-static {}, Lcom/sec/chaton/a/ee;->h()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ed;
    .locals 1

    .prologue
    .line 32500
    sget-object v0, Lcom/sec/chaton/a/ed;->a:Lcom/sec/chaton/a/ed;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 32508
    iget v1, p0, Lcom/sec/chaton/a/ed;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 32511
    iget-wide v0, p0, Lcom/sec/chaton/a/ed;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 32518
    iget v0, p0, Lcom/sec/chaton/a/ed;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 32521
    iget-object v0, p0, Lcom/sec/chaton/a/ed;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32641
    invoke-static {}, Lcom/sec/chaton/a/ed;->newBuilder()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32485
    invoke-virtual {p0}, Lcom/sec/chaton/a/ed;->b()Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 32550
    iget v0, p0, Lcom/sec/chaton/a/ed;->f:I

    .line 32551
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 32563
    :goto_0
    return v0

    .line 32553
    :cond_0
    const/4 v0, 0x0

    .line 32554
    iget v1, p0, Lcom/sec/chaton/a/ed;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 32555
    iget-wide v1, p0, Lcom/sec/chaton/a/ed;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 32558
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/ed;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 32559
    iget-object v1, p0, Lcom/sec/chaton/a/ed;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32562
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/ed;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32645
    invoke-static {p0}, Lcom/sec/chaton/a/ed;->a(Lcom/sec/chaton/a/ed;)Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 32530
    iget-byte v1, p0, Lcom/sec/chaton/a/ed;->e:B

    .line 32531
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 32534
    :goto_0
    return v0

    .line 32531
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 32533
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ed;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32485
    invoke-virtual {p0}, Lcom/sec/chaton/a/ed;->g()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32485
    invoke-virtual {p0}, Lcom/sec/chaton/a/ed;->h()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32570
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 32539
    invoke-virtual {p0}, Lcom/sec/chaton/a/ed;->getSerializedSize()I

    .line 32540
    iget v0, p0, Lcom/sec/chaton/a/ed;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 32541
    iget-wide v0, p0, Lcom/sec/chaton/a/ed;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 32543
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ed;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 32544
    iget-object v0, p0, Lcom/sec/chaton/a/ed;->d:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 32546
    :cond_1
    return-void
.end method
