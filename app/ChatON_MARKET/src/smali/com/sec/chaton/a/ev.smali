.class public final Lcom/sec/chaton/a/ev;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ex;


# static fields
.field private static final a:Lcom/sec/chaton/a/ev;


# instance fields
.field private b:I

.field private c:J

.field private d:J

.field private e:Lcom/sec/chaton/a/ej;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8474
    new-instance v0, Lcom/sec/chaton/a/ev;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ev;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ev;->a:Lcom/sec/chaton/a/ev;

    .line 8475
    sget-object v0, Lcom/sec/chaton/a/ev;->a:Lcom/sec/chaton/a/ev;

    invoke-direct {v0}, Lcom/sec/chaton/a/ev;->k()V

    .line 8476
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ew;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 8078
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 8127
    iput-byte v0, p0, Lcom/sec/chaton/a/ev;->f:B

    .line 8150
    iput v0, p0, Lcom/sec/chaton/a/ev;->g:I

    .line 8079
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ew;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 8073
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ev;-><init>(Lcom/sec/chaton/a/ew;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 8080
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 8127
    iput-byte v0, p0, Lcom/sec/chaton/a/ev;->f:B

    .line 8150
    iput v0, p0, Lcom/sec/chaton/a/ev;->g:I

    .line 8080
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ev;I)I
    .locals 0

    .prologue
    .line 8073
    iput p1, p0, Lcom/sec/chaton/a/ev;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ev;J)J
    .locals 0

    .prologue
    .line 8073
    iput-wide p1, p0, Lcom/sec/chaton/a/ev;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ev;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 8073
    iput-object p1, p0, Lcom/sec/chaton/a/ev;->e:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/ev;
    .locals 1

    .prologue
    .line 8084
    sget-object v0, Lcom/sec/chaton/a/ev;->a:Lcom/sec/chaton/a/ev;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8249
    invoke-static {}, Lcom/sec/chaton/a/ev;->newBuilder()Lcom/sec/chaton/a/ew;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ew;->a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/ev;J)J
    .locals 0

    .prologue
    .line 8073
    iput-wide p1, p0, Lcom/sec/chaton/a/ev;->d:J

    return-wide p1
.end method

.method private k()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 8123
    iput-wide v0, p0, Lcom/sec/chaton/a/ev;->c:J

    .line 8124
    iput-wide v0, p0, Lcom/sec/chaton/a/ev;->d:J

    .line 8125
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ev;->e:Lcom/sec/chaton/a/ej;

    .line 8126
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8246
    invoke-static {}, Lcom/sec/chaton/a/ew;->h()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ev;
    .locals 1

    .prologue
    .line 8088
    sget-object v0, Lcom/sec/chaton/a/ev;->a:Lcom/sec/chaton/a/ev;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 8096
    iget v1, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 8099
    iget-wide v0, p0, Lcom/sec/chaton/a/ev;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 8106
    iget v0, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 8109
    iget-wide v0, p0, Lcom/sec/chaton/a/ev;->d:J

    return-wide v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 8116
    iget v0, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8073
    invoke-virtual {p0}, Lcom/sec/chaton/a/ev;->b()Lcom/sec/chaton/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 8152
    iget v0, p0, Lcom/sec/chaton/a/ev;->g:I

    .line 8153
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 8169
    :goto_0
    return v0

    .line 8155
    :cond_0
    const/4 v0, 0x0

    .line 8156
    iget v1, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 8157
    iget-wide v1, p0, Lcom/sec/chaton/a/ev;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8160
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 8161
    iget-wide v1, p0, Lcom/sec/chaton/a/ev;->d:J

    invoke-static {v4, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8164
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 8165
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/chaton/a/ev;->e:Lcom/sec/chaton/a/ej;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8168
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/ev;->g:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 8119
    iget-object v0, p0, Lcom/sec/chaton/a/ev;->e:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public i()Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8247
    invoke-static {}, Lcom/sec/chaton/a/ev;->newBuilder()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 8129
    iget-byte v1, p0, Lcom/sec/chaton/a/ev;->f:B

    .line 8130
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 8133
    :goto_0
    return v0

    .line 8130
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 8132
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ev;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8251
    invoke-static {p0}, Lcom/sec/chaton/a/ev;->a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8073
    invoke-virtual {p0}, Lcom/sec/chaton/a/ev;->i()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8073
    invoke-virtual {p0}, Lcom/sec/chaton/a/ev;->j()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8176
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 8138
    invoke-virtual {p0}, Lcom/sec/chaton/a/ev;->getSerializedSize()I

    .line 8139
    iget v0, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 8140
    iget-wide v0, p0, Lcom/sec/chaton/a/ev;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 8142
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 8143
    iget-wide v0, p0, Lcom/sec/chaton/a/ev;->d:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 8145
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/ev;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 8146
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/ev;->e:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 8148
    :cond_2
    return-void
.end method
