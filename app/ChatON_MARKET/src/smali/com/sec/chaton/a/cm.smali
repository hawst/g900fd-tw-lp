.class public final Lcom/sec/chaton/a/cm;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/cl;",
        "Lcom/sec/chaton/a/cm;",
        ">;",
        "Lcom/sec/chaton/a/cn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30477
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 30624
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->b:Ljava/lang/Object;

    .line 30660
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    .line 30770
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->e:Ljava/lang/Object;

    .line 30478
    invoke-direct {p0}, Lcom/sec/chaton/a/cm;->g()V

    .line 30479
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cm;)Lcom/sec/chaton/a/cl;
    .locals 1

    .prologue
    .line 30472
    invoke-direct {p0}, Lcom/sec/chaton/a/cm;->i()Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30472
    invoke-static {}, Lcom/sec/chaton/a/cm;->h()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 30482
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30484
    new-instance v0, Lcom/sec/chaton/a/cm;

    invoke-direct {v0}, Lcom/sec/chaton/a/cm;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/cl;
    .locals 2

    .prologue
    .line 30518
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->e()Lcom/sec/chaton/a/cl;

    move-result-object v0

    .line 30519
    invoke-virtual {v0}, Lcom/sec/chaton/a/cl;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 30520
    invoke-static {v0}, Lcom/sec/chaton/a/cm;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 30523
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 30663
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 30664
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    .line 30665
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30667
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30488
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 30489
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->b:Ljava/lang/Object;

    .line 30490
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30491
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    .line 30492
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30493
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/cm;->d:Z

    .line 30494
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30495
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->e:Ljava/lang/Object;

    .line 30496
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30497
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30584
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 30585
    sparse-switch v0, :sswitch_data_0

    .line 30590
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/cm;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30592
    :sswitch_0
    return-object p0

    .line 30597
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30598
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->b:Ljava/lang/Object;

    goto :goto_0

    .line 30602
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/cr;->newBuilder()Lcom/sec/chaton/a/cs;

    move-result-object v0

    .line 30603
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 30604
    invoke-virtual {v0}, Lcom/sec/chaton/a/cs;->e()Lcom/sec/chaton/a/cr;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cm;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cm;

    goto :goto_0

    .line 30608
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30609
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/a/cm;->d:Z

    goto :goto_0

    .line 30613
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30614
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->e:Ljava/lang/Object;

    goto :goto_0

    .line 30585
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/cl;)Lcom/sec/chaton/a/cm;
    .locals 2

    .prologue
    .line 30552
    invoke-static {}, Lcom/sec/chaton/a/cl;->a()Lcom/sec/chaton/a/cl;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 30572
    :cond_0
    :goto_0
    return-object p0

    .line 30553
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/cl;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 30554
    invoke-virtual {p1}, Lcom/sec/chaton/a/cl;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cm;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cm;

    .line 30556
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/a/cl;->b(Lcom/sec/chaton/a/cl;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 30557
    iget-object v0, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 30558
    invoke-static {p1}, Lcom/sec/chaton/a/cl;->b(Lcom/sec/chaton/a/cl;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    .line 30559
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30566
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/cl;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 30567
    invoke-virtual {p1}, Lcom/sec/chaton/a/cl;->h()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cm;->a(Z)Lcom/sec/chaton/a/cm;

    .line 30569
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/cl;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30570
    invoke-virtual {p1}, Lcom/sec/chaton/a/cl;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cm;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cm;

    goto :goto_0

    .line 30561
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/a/cm;->j()V

    .line 30562
    iget-object v0, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/cl;->b(Lcom/sec/chaton/a/cl;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30696
    if-nez p1, :cond_0

    .line 30697
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30699
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/cm;->j()V

    .line 30700
    iget-object v0, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30702
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30639
    if-nez p1, :cond_0

    .line 30640
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30642
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30643
    iput-object p1, p0, Lcom/sec/chaton/a/cm;->b:Ljava/lang/Object;

    .line 30645
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30757
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30758
    iput-boolean p1, p0, Lcom/sec/chaton/a/cm;->d:Z

    .line 30760
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/cm;
    .locals 2

    .prologue
    .line 30501
    invoke-static {}, Lcom/sec/chaton/a/cm;->h()Lcom/sec/chaton/a/cm;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->e()Lcom/sec/chaton/a/cl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cm;->a(Lcom/sec/chaton/a/cl;)Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30785
    if-nez p1, :cond_0

    .line 30786
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30788
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30789
    iput-object p1, p0, Lcom/sec/chaton/a/cm;->e:Ljava/lang/Object;

    .line 30791
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->d()Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->e()Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/cl;
    .locals 1

    .prologue
    .line 30505
    invoke-static {}, Lcom/sec/chaton/a/cl;->a()Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->a()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->a()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->b()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->b()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->b()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->b()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/cl;
    .locals 2

    .prologue
    .line 30509
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->e()Lcom/sec/chaton/a/cl;

    move-result-object v0

    .line 30510
    invoke-virtual {v0}, Lcom/sec/chaton/a/cl;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 30511
    invoke-static {v0}, Lcom/sec/chaton/a/cm;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 30513
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/cl;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 30527
    new-instance v2, Lcom/sec/chaton/a/cl;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/cl;-><init>(Lcom/sec/chaton/a/cm;Lcom/sec/chaton/a/b;)V

    .line 30528
    iget v3, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30529
    const/4 v1, 0x0

    .line 30530
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 30533
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/cm;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cl;->a(Lcom/sec/chaton/a/cl;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30534
    iget v1, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 30535
    iget-object v1, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    .line 30536
    iget v1, p0, Lcom/sec/chaton/a/cm;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/chaton/a/cm;->a:I

    .line 30538
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/cm;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cl;->a(Lcom/sec/chaton/a/cl;Ljava/util/List;)Ljava/util/List;

    .line 30539
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 30540
    or-int/lit8 v0, v0, 0x2

    .line 30542
    :cond_1
    iget-boolean v1, p0, Lcom/sec/chaton/a/cm;->d:Z

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cl;->a(Lcom/sec/chaton/a/cl;Z)Z

    .line 30543
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 30544
    or-int/lit8 v0, v0, 0x4

    .line 30546
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/cm;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cl;->b(Lcom/sec/chaton/a/cl;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30547
    invoke-static {v2, v0}, Lcom/sec/chaton/a/cl;->a(Lcom/sec/chaton/a/cl;I)I

    .line 30548
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->c()Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0}, Lcom/sec/chaton/a/cm;->c()Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 30576
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cm;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    check-cast p1, Lcom/sec/chaton/a/cl;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/cm;->a(Lcom/sec/chaton/a/cl;)Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30472
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cm;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method
