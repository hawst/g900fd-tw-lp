.class public final Lcom/sec/chaton/a/dj;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dl;


# static fields
.field private static final a:Lcom/sec/chaton/a/dj;


# instance fields
.field private b:I

.field private c:Lcom/sec/chaton/a/bc;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/sec/chaton/a/be;

.field private h:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23875
    new-instance v0, Lcom/sec/chaton/a/dj;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/dj;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/dj;->a:Lcom/sec/chaton/a/dj;

    .line 23876
    sget-object v0, Lcom/sec/chaton/a/dj;->a:Lcom/sec/chaton/a/dj;

    invoke-direct {v0}, Lcom/sec/chaton/a/dj;->t()V

    .line 23877
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/dk;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 23221
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 23369
    iput-byte v0, p0, Lcom/sec/chaton/a/dj;->i:B

    .line 23401
    iput v0, p0, Lcom/sec/chaton/a/dj;->j:I

    .line 23222
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/dk;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 23216
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/dj;-><init>(Lcom/sec/chaton/a/dk;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 23223
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 23369
    iput-byte v0, p0, Lcom/sec/chaton/a/dj;->i:B

    .line 23401
    iput v0, p0, Lcom/sec/chaton/a/dj;->j:I

    .line 23223
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dj;I)I
    .locals 0

    .prologue
    .line 23216
    iput p1, p0, Lcom/sec/chaton/a/dj;->h:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dj;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 23216
    iput-object p1, p0, Lcom/sec/chaton/a/dj;->c:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dj;Lcom/sec/chaton/a/be;)Lcom/sec/chaton/a/be;
    .locals 0

    .prologue
    .line 23216
    iput-object p1, p0, Lcom/sec/chaton/a/dj;->g:Lcom/sec/chaton/a/be;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/dj;
    .locals 1

    .prologue
    .line 23227
    sget-object v0, Lcom/sec/chaton/a/dj;->a:Lcom/sec/chaton/a/dj;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/dj;
    .locals 1

    .prologue
    .line 23456
    invoke-static {}, Lcom/sec/chaton/a/dj;->newBuilder()Lcom/sec/chaton/a/dk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dk;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dk;

    invoke-static {v0}, Lcom/sec/chaton/a/dk;->a(Lcom/sec/chaton/a/dk;)Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/dj;)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23512
    invoke-static {}, Lcom/sec/chaton/a/dj;->newBuilder()Lcom/sec/chaton/a/dk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dk;->a(Lcom/sec/chaton/a/dj;)Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/dj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23216
    iput-object p1, p0, Lcom/sec/chaton/a/dj;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dj;I)I
    .locals 0

    .prologue
    .line 23216
    iput p1, p0, Lcom/sec/chaton/a/dj;->b:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23216
    iput-object p1, p0, Lcom/sec/chaton/a/dj;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/dj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23216
    iput-object p1, p0, Lcom/sec/chaton/a/dj;->f:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23509
    invoke-static {}, Lcom/sec/chaton/a/dk;->f()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 23266
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->d:Ljava/lang/Object;

    .line 23267
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23268
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 23270
    iput-object v0, p0, Lcom/sec/chaton/a/dj;->d:Ljava/lang/Object;

    .line 23273
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 23298
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->e:Ljava/lang/Object;

    .line 23299
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23300
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 23302
    iput-object v0, p0, Lcom/sec/chaton/a/dj;->e:Ljava/lang/Object;

    .line 23305
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 23330
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->f:Ljava/lang/Object;

    .line 23331
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23332
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 23334
    iput-object v0, p0, Lcom/sec/chaton/a/dj;->f:Ljava/lang/Object;

    .line 23337
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()V
    .locals 1

    .prologue
    .line 23362
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/dj;->c:Lcom/sec/chaton/a/bc;

    .line 23363
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dj;->d:Ljava/lang/Object;

    .line 23364
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dj;->e:Ljava/lang/Object;

    .line 23365
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dj;->f:Ljava/lang/Object;

    .line 23366
    sget-object v0, Lcom/sec/chaton/a/be;->a:Lcom/sec/chaton/a/be;

    iput-object v0, p0, Lcom/sec/chaton/a/dj;->g:Lcom/sec/chaton/a/be;

    .line 23367
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/dj;->h:I

    .line 23368
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/dj;
    .locals 1

    .prologue
    .line 23231
    sget-object v0, Lcom/sec/chaton/a/dj;->a:Lcom/sec/chaton/a/dj;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 23239
    iget v1, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 23242
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->c:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 23249
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23252
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->d:Ljava/lang/Object;

    .line 23253
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23254
    check-cast v0, Ljava/lang/String;

    .line 23262
    :goto_0
    return-object v0

    .line 23256
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 23258
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 23259
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23260
    iput-object v1, p0, Lcom/sec/chaton/a/dj;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 23262
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 23281
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23216
    invoke-virtual {p0}, Lcom/sec/chaton/a/dj;->b()Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 23403
    iget v0, p0, Lcom/sec/chaton/a/dj;->j:I

    .line 23404
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 23432
    :goto_0
    return v0

    .line 23406
    :cond_0
    const/4 v0, 0x0

    .line 23407
    iget v1, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 23408
    iget-object v1, p0, Lcom/sec/chaton/a/dj;->c:Lcom/sec/chaton/a/bc;

    invoke-virtual {v1}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23411
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 23412
    invoke-direct {p0}, Lcom/sec/chaton/a/dj;->q()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23415
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 23416
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/dj;->r()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23419
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 23420
    invoke-direct {p0}, Lcom/sec/chaton/a/dj;->s()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23423
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 23424
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/a/dj;->g:Lcom/sec/chaton/a/be;

    invoke-virtual {v2}, Lcom/sec/chaton/a/be;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23427
    :cond_5
    iget v1, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 23428
    const/4 v1, 0x6

    iget v2, p0, Lcom/sec/chaton/a/dj;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23431
    :cond_6
    iput v0, p0, Lcom/sec/chaton/a/dj;->j:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23284
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->e:Ljava/lang/Object;

    .line 23285
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23286
    check-cast v0, Ljava/lang/String;

    .line 23294
    :goto_0
    return-object v0

    .line 23288
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 23290
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 23291
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23292
    iput-object v1, p0, Lcom/sec/chaton/a/dj;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 23294
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 23313
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 23371
    iget-byte v1, p0, Lcom/sec/chaton/a/dj;->i:B

    .line 23372
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 23375
    :goto_0
    return v0

    .line 23372
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 23374
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/dj;->i:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23316
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->f:Ljava/lang/Object;

    .line 23317
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23318
    check-cast v0, Ljava/lang/String;

    .line 23326
    :goto_0
    return-object v0

    .line 23320
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 23322
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 23323
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23324
    iput-object v1, p0, Lcom/sec/chaton/a/dj;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 23326
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 23345
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lcom/sec/chaton/a/be;
    .locals 1

    .prologue
    .line 23348
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->g:Lcom/sec/chaton/a/be;

    return-object v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 23355
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 23358
    iget v0, p0, Lcom/sec/chaton/a/dj;->h:I

    return v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23216
    invoke-virtual {p0}, Lcom/sec/chaton/a/dj;->o()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23510
    invoke-static {}, Lcom/sec/chaton/a/dj;->newBuilder()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23514
    invoke-static {p0}, Lcom/sec/chaton/a/dj;->a(Lcom/sec/chaton/a/dj;)Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23216
    invoke-virtual {p0}, Lcom/sec/chaton/a/dj;->p()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23439
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 23380
    invoke-virtual {p0}, Lcom/sec/chaton/a/dj;->getSerializedSize()I

    .line 23381
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 23382
    iget-object v0, p0, Lcom/sec/chaton/a/dj;->c:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 23384
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 23385
    invoke-direct {p0}, Lcom/sec/chaton/a/dj;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 23387
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 23388
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/dj;->r()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 23390
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 23391
    invoke-direct {p0}, Lcom/sec/chaton/a/dj;->s()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 23393
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 23394
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sec/chaton/a/dj;->g:Lcom/sec/chaton/a/be;

    invoke-virtual {v1}, Lcom/sec/chaton/a/be;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 23396
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/dj;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 23397
    const/4 v0, 0x6

    iget v1, p0, Lcom/sec/chaton/a/dj;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 23399
    :cond_5
    return-void
.end method
