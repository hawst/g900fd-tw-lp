.class public final enum Lcom/sec/chaton/a/a/i;
.super Ljava/lang/Enum;
.source "HttpEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/a/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/a/i;

.field public static final enum b:Lcom/sec/chaton/a/a/i;

.field public static final enum c:Lcom/sec/chaton/a/a/i;

.field public static final enum d:Lcom/sec/chaton/a/a/i;

.field private static final synthetic e:[Lcom/sec/chaton/a/a/i;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 165
    new-instance v0, Lcom/sec/chaton/a/a/i;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/i;->a:Lcom/sec/chaton/a/a/i;

    .line 166
    new-instance v0, Lcom/sec/chaton/a/a/i;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/a/a/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/i;->b:Lcom/sec/chaton/a/a/i;

    .line 167
    new-instance v0, Lcom/sec/chaton/a/a/i;

    const-string v1, "NO_REQUEST"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/a/a/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/i;->c:Lcom/sec/chaton/a/a/i;

    .line 168
    new-instance v0, Lcom/sec/chaton/a/a/i;

    const-string v1, "NO_CONTENT"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/a/a/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/i;->d:Lcom/sec/chaton/a/a/i;

    .line 164
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/a/a/i;

    sget-object v1, Lcom/sec/chaton/a/a/i;->a:Lcom/sec/chaton/a/a/i;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/a/i;->b:Lcom/sec/chaton/a/a/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/a/a/i;->c:Lcom/sec/chaton/a/a/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/a/a/i;->d:Lcom/sec/chaton/a/a/i;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/a/a/i;->e:[Lcom/sec/chaton/a/a/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/a/i;
    .locals 1

    .prologue
    .line 164
    const-class v0, Lcom/sec/chaton/a/a/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/i;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/a/i;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/sec/chaton/a/a/i;->e:[Lcom/sec/chaton/a/a/i;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/a/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/a/i;

    return-object v0
.end method
