.class public final Lcom/sec/chaton/a/ez;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/fa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ey;",
        "Lcom/sec/chaton/a/ez;",
        ">;",
        "Lcom/sec/chaton/a/fa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5919
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 6029
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ez;->b:Ljava/lang/Object;

    .line 5920
    invoke-direct {p0}, Lcom/sec/chaton/a/ez;->g()V

    .line 5921
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5914
    invoke-static {}, Lcom/sec/chaton/a/ez;->h()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 5924
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5926
    new-instance v0, Lcom/sec/chaton/a/ez;

    invoke-direct {v0}, Lcom/sec/chaton/a/ez;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5930
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 5931
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ez;->b:Ljava/lang/Object;

    .line 5932
    iget v0, p0, Lcom/sec/chaton/a/ez;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ez;->a:I

    .line 5933
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/ez;->c:I

    .line 5934
    iget v0, p0, Lcom/sec/chaton/a/ez;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ez;->a:I

    .line 5935
    return-object p0
.end method

.method public a(I)Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 6073
    iget v0, p0, Lcom/sec/chaton/a/ez;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ez;->a:I

    .line 6074
    iput p1, p0, Lcom/sec/chaton/a/ez;->c:I

    .line 6076
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 6000
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 6001
    sparse-switch v0, :sswitch_data_0

    .line 6006
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ez;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6008
    :sswitch_0
    return-object p0

    .line 6013
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ez;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ez;->a:I

    .line 6014
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ez;->b:Ljava/lang/Object;

    goto :goto_0

    .line 6018
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ez;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ez;->a:I

    .line 6019
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/a/ez;->c:I

    goto :goto_0

    .line 6001
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5981
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 5988
    :cond_0
    :goto_0
    return-object p0

    .line 5982
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ey;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5983
    invoke-virtual {p1}, Lcom/sec/chaton/a/ey;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ez;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ez;

    .line 5985
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ey;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5986
    invoke-virtual {p1}, Lcom/sec/chaton/a/ey;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ez;->a(I)Lcom/sec/chaton/a/ez;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 6044
    if-nez p1, :cond_0

    .line 6045
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6047
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ez;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ez;->a:I

    .line 6048
    iput-object p1, p0, Lcom/sec/chaton/a/ez;->b:Ljava/lang/Object;

    .line 6050
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ez;
    .locals 2

    .prologue
    .line 5939
    invoke-static {}, Lcom/sec/chaton/a/ez;->h()Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->d()Lcom/sec/chaton/a/ey;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 5943
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->a()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->a()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->b()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->b()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->b()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->b()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ey;
    .locals 2

    .prologue
    .line 5947
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    .line 5948
    invoke-virtual {v0}, Lcom/sec/chaton/a/ey;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5949
    invoke-static {v0}, Lcom/sec/chaton/a/ez;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5951
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ey;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5965
    new-instance v2, Lcom/sec/chaton/a/ey;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ey;-><init>(Lcom/sec/chaton/a/ez;Lcom/sec/chaton/a/b;)V

    .line 5966
    iget v3, p0, Lcom/sec/chaton/a/ez;->a:I

    .line 5967
    const/4 v1, 0x0

    .line 5968
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 5971
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/ez;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ey;->a(Lcom/sec/chaton/a/ey;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5972
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 5973
    or-int/lit8 v0, v0, 0x2

    .line 5975
    :cond_0
    iget v1, p0, Lcom/sec/chaton/a/ez;->c:I

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ey;->a(Lcom/sec/chaton/a/ey;I)I

    .line 5976
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ey;->b(Lcom/sec/chaton/a/ey;I)I

    .line 5977
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->c()Lcom/sec/chaton/a/ey;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0}, Lcom/sec/chaton/a/ez;->c()Lcom/sec/chaton/a/ey;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 5992
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ez;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    check-cast p1, Lcom/sec/chaton/a/ey;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5914
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ez;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method
