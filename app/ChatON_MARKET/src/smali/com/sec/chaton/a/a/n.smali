.class public final enum Lcom/sec/chaton/a/a/n;
.super Ljava/lang/Enum;
.source "ResultEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/a/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/a/n;

.field public static final enum b:Lcom/sec/chaton/a/a/n;

.field public static final enum c:Lcom/sec/chaton/a/a/n;

.field public static final enum d:Lcom/sec/chaton/a/a/n;

.field private static final synthetic e:[Lcom/sec/chaton/a/a/n;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/sec/chaton/a/a/n;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    .line 18
    new-instance v0, Lcom/sec/chaton/a/a/n;

    const-string v1, "SUCCESS_UPLOAD_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/a/a/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/n;->b:Lcom/sec/chaton/a/a/n;

    .line 19
    new-instance v0, Lcom/sec/chaton/a/a/n;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/a/a/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/n;->c:Lcom/sec/chaton/a/a/n;

    .line 20
    new-instance v0, Lcom/sec/chaton/a/a/n;

    const-string v1, "NETWORKFAIL"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/a/a/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/n;->d:Lcom/sec/chaton/a/a/n;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/a/a/n;

    sget-object v1, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/a/n;->b:Lcom/sec/chaton/a/a/n;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/a/a/n;->c:Lcom/sec/chaton/a/a/n;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/a/a/n;->d:Lcom/sec/chaton/a/a/n;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/a/a/n;->e:[Lcom/sec/chaton/a/a/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/a/n;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/chaton/a/a/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/n;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/a/n;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/chaton/a/a/n;->e:[Lcom/sec/chaton/a/a/n;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/a/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/a/n;

    return-object v0
.end method
