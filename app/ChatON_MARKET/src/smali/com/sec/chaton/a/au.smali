.class public final enum Lcom/sec/chaton/a/au;
.super Ljava/lang/Enum;
.source "SSMGPB.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/au;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/au;

.field public static final enum b:Lcom/sec/chaton/a/au;

.field private static c:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sec/chaton/a/au;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic e:[Lcom/sec/chaton/a/au;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 245
    new-instance v0, Lcom/sec/chaton/a/au;

    const-string v1, "ENTER"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sec/chaton/a/au;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/au;->a:Lcom/sec/chaton/a/au;

    .line 246
    new-instance v0, Lcom/sec/chaton/a/au;

    const-string v1, "LEAVE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sec/chaton/a/au;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/au;->b:Lcom/sec/chaton/a/au;

    .line 243
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/a/au;

    sget-object v1, Lcom/sec/chaton/a/au;->a:Lcom/sec/chaton/a/au;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/au;->b:Lcom/sec/chaton/a/au;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/a/au;->e:[Lcom/sec/chaton/a/au;

    .line 268
    new-instance v0, Lcom/sec/chaton/a/av;

    invoke-direct {v0}, Lcom/sec/chaton/a/av;-><init>()V

    sput-object v0, Lcom/sec/chaton/a/au;->c:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 277
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 278
    iput p4, p0, Lcom/sec/chaton/a/au;->d:I

    .line 279
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/a/au;
    .locals 1

    .prologue
    .line 256
    packed-switch p0, :pswitch_data_0

    .line 259
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 257
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/a/au;->a:Lcom/sec/chaton/a/au;

    goto :goto_0

    .line 258
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/a/au;->b:Lcom/sec/chaton/a/au;

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/au;
    .locals 1

    .prologue
    .line 243
    const-class v0, Lcom/sec/chaton/a/au;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/au;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/au;
    .locals 1

    .prologue
    .line 243
    sget-object v0, Lcom/sec/chaton/a/au;->e:[Lcom/sec/chaton/a/au;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/au;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/au;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/sec/chaton/a/au;->d:I

    return v0
.end method
