.class public final Lcom/sec/chaton/a/o;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/q;


# static fields
.field private static final a:Lcom/sec/chaton/a/o;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20285
    new-instance v0, Lcom/sec/chaton/a/o;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/o;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/o;->a:Lcom/sec/chaton/a/o;

    .line 20286
    sget-object v0, Lcom/sec/chaton/a/o;->a:Lcom/sec/chaton/a/o;

    invoke-direct {v0}, Lcom/sec/chaton/a/o;->i()V

    .line 20287
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/p;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19942
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 19980
    iput-byte v0, p0, Lcom/sec/chaton/a/o;->e:B

    .line 20000
    iput v0, p0, Lcom/sec/chaton/a/o;->f:I

    .line 19943
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/p;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 19937
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/o;-><init>(Lcom/sec/chaton/a/p;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19944
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 19980
    iput-byte v0, p0, Lcom/sec/chaton/a/o;->e:B

    .line 20000
    iput v0, p0, Lcom/sec/chaton/a/o;->f:I

    .line 19944
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/o;I)I
    .locals 0

    .prologue
    .line 19937
    iput p1, p0, Lcom/sec/chaton/a/o;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/o;J)J
    .locals 0

    .prologue
    .line 19937
    iput-wide p1, p0, Lcom/sec/chaton/a/o;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/o;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 19937
    iput-object p1, p0, Lcom/sec/chaton/a/o;->d:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/o;
    .locals 1

    .prologue
    .line 19948
    sget-object v0, Lcom/sec/chaton/a/o;->a:Lcom/sec/chaton/a/o;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/o;
    .locals 1

    .prologue
    .line 20039
    invoke-static {}, Lcom/sec/chaton/a/o;->newBuilder()Lcom/sec/chaton/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/p;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/p;

    invoke-static {v0}, Lcom/sec/chaton/a/p;->a(Lcom/sec/chaton/a/p;)Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/o;)Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20095
    invoke-static {}, Lcom/sec/chaton/a/o;->newBuilder()Lcom/sec/chaton/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/p;->a(Lcom/sec/chaton/a/o;)Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 19977
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/o;->c:J

    .line 19978
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/o;->d:Lcom/sec/chaton/a/ej;

    .line 19979
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20092
    invoke-static {}, Lcom/sec/chaton/a/p;->i()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/o;
    .locals 1

    .prologue
    .line 19952
    sget-object v0, Lcom/sec/chaton/a/o;->a:Lcom/sec/chaton/a/o;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 19960
    iget v1, p0, Lcom/sec/chaton/a/o;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 19963
    iget-wide v0, p0, Lcom/sec/chaton/a/o;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 19970
    iget v0, p0, Lcom/sec/chaton/a/o;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 19973
    iget-object v0, p0, Lcom/sec/chaton/a/o;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20093
    invoke-static {}, Lcom/sec/chaton/a/o;->newBuilder()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19937
    invoke-virtual {p0}, Lcom/sec/chaton/a/o;->b()Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 20002
    iget v0, p0, Lcom/sec/chaton/a/o;->f:I

    .line 20003
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 20015
    :goto_0
    return v0

    .line 20005
    :cond_0
    const/4 v0, 0x0

    .line 20006
    iget v1, p0, Lcom/sec/chaton/a/o;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 20007
    iget-wide v1, p0, Lcom/sec/chaton/a/o;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 20010
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/o;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 20011
    iget-object v1, p0, Lcom/sec/chaton/a/o;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20014
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/o;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20097
    invoke-static {p0}, Lcom/sec/chaton/a/o;->a(Lcom/sec/chaton/a/o;)Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 19982
    iget-byte v1, p0, Lcom/sec/chaton/a/o;->e:B

    .line 19983
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 19986
    :goto_0
    return v0

    .line 19983
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 19985
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/o;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19937
    invoke-virtual {p0}, Lcom/sec/chaton/a/o;->g()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19937
    invoke-virtual {p0}, Lcom/sec/chaton/a/o;->h()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20022
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 19991
    invoke-virtual {p0}, Lcom/sec/chaton/a/o;->getSerializedSize()I

    .line 19992
    iget v0, p0, Lcom/sec/chaton/a/o;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 19993
    iget-wide v0, p0, Lcom/sec/chaton/a/o;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 19995
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/o;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 19996
    iget-object v0, p0, Lcom/sec/chaton/a/o;->d:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 19998
    :cond_1
    return-void
.end method
