.class public final Lcom/sec/chaton/a/ax;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ay;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/aw;",
        "Lcom/sec/chaton/a/ax;",
        ">;",
        "Lcom/sec/chaton/a/ay;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/Object;

.field private d:Lcom/sec/chaton/a/ey;

.field private e:J

.field private f:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 13081
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 13262
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->c:Ljava/lang/Object;

    .line 13298
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    .line 13362
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    .line 13082
    invoke-direct {p0}, Lcom/sec/chaton/a/ax;->k()V

    .line 13083
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ax;)Lcom/sec/chaton/a/aw;
    .locals 1

    .prologue
    .line 13076
    invoke-direct {p0}, Lcom/sec/chaton/a/ax;->m()Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j()Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13076
    invoke-static {}, Lcom/sec/chaton/a/ax;->l()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 13086
    return-void
.end method

.method private static l()Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13088
    new-instance v0, Lcom/sec/chaton/a/ax;

    invoke-direct {v0}, Lcom/sec/chaton/a/ax;-><init>()V

    return-object v0
.end method

.method private m()Lcom/sec/chaton/a/aw;
    .locals 2

    .prologue
    .line 13124
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->e()Lcom/sec/chaton/a/aw;

    move-result-object v0

    .line 13125
    invoke-virtual {v0}, Lcom/sec/chaton/a/aw;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13126
    invoke-static {v0}, Lcom/sec/chaton/a/ax;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 13129
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ax;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 13092
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 13093
    iput-wide v1, p0, Lcom/sec/chaton/a/ax;->b:J

    .line 13094
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13095
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->c:Ljava/lang/Object;

    .line 13096
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13097
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    .line 13098
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13099
    iput-wide v1, p0, Lcom/sec/chaton/a/ax;->e:J

    .line 13100
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13101
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    .line 13102
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13103
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13249
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13250
    iput-wide p1, p0, Lcom/sec/chaton/a/ax;->b:J

    .line 13252
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ax;
    .locals 2

    .prologue
    .line 13189
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 13190
    sparse-switch v0, :sswitch_data_0

    .line 13195
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ax;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13197
    :sswitch_0
    return-object p0

    .line 13202
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13203
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ax;->b:J

    goto :goto_0

    .line 13207
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13208
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->c:Ljava/lang/Object;

    goto :goto_0

    .line 13212
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/ey;->newBuilder()Lcom/sec/chaton/a/ez;

    move-result-object v0

    .line 13213
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13214
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->g()Lcom/sec/chaton/a/ey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    .line 13216
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 13217
    invoke-virtual {v0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ax;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ax;

    goto :goto_0

    .line 13221
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13222
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ax;->e:J

    goto :goto_0

    .line 13226
    :sswitch_5
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 13227
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13228
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->i()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 13230
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 13231
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ax;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ax;

    goto :goto_0

    .line 13190
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/aw;)Lcom/sec/chaton/a/ax;
    .locals 2

    .prologue
    .line 13161
    invoke-static {}, Lcom/sec/chaton/a/aw;->a()Lcom/sec/chaton/a/aw;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 13177
    :cond_0
    :goto_0
    return-object p0

    .line 13162
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 13163
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ax;->a(J)Lcom/sec/chaton/a/ax;

    .line 13165
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 13166
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ax;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ax;

    .line 13168
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 13169
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->h()Lcom/sec/chaton/a/ey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ax;->b(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ax;

    .line 13171
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 13172
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ax;->b(J)Lcom/sec/chaton/a/ax;

    .line 13174
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13175
    invoke-virtual {p1}, Lcom/sec/chaton/a/aw;->l()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ax;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ax;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13370
    if-nez p1, :cond_0

    .line 13371
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13373
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    .line 13375
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13376
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ek;)Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13380
    invoke-virtual {p1}, Lcom/sec/chaton/a/ek;->d()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    .line 13382
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13383
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13306
    if-nez p1, :cond_0

    .line 13307
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13309
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    .line 13311
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13312
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13277
    if-nez p1, :cond_0

    .line 13278
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13280
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13281
    iput-object p1, p0, Lcom/sec/chaton/a/ax;->c:Ljava/lang/Object;

    .line 13283
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ax;
    .locals 2

    .prologue
    .line 13107
    invoke-static {}, Lcom/sec/chaton/a/ax;->l()Lcom/sec/chaton/a/ax;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->e()Lcom/sec/chaton/a/aw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ax;->a(Lcom/sec/chaton/a/aw;)Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13349
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13350
    iput-wide p1, p0, Lcom/sec/chaton/a/ax;->e:J

    .line 13352
    return-object p0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ax;
    .locals 2

    .prologue
    .line 13386
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 13388
    iget-object v0, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    .line 13394
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13395
    return-object p0

    .line 13391
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public b(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ax;
    .locals 2

    .prologue
    .line 13322
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 13324
    iget-object v0, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    invoke-static {v0}, Lcom/sec/chaton/a/ey;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    .line 13330
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13331
    return-object p0

    .line 13327
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->d()Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->e()Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/aw;
    .locals 1

    .prologue
    .line 13111
    invoke-static {}, Lcom/sec/chaton/a/aw;->a()Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->a()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->a()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->b()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->b()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->b()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->b()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/aw;
    .locals 2

    .prologue
    .line 13115
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->e()Lcom/sec/chaton/a/aw;

    move-result-object v0

    .line 13116
    invoke-virtual {v0}, Lcom/sec/chaton/a/aw;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13117
    invoke-static {v0}, Lcom/sec/chaton/a/ax;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 13119
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/aw;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 13133
    new-instance v2, Lcom/sec/chaton/a/aw;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/aw;-><init>(Lcom/sec/chaton/a/ax;Lcom/sec/chaton/a/b;)V

    .line 13134
    iget v3, p0, Lcom/sec/chaton/a/ax;->a:I

    .line 13135
    const/4 v1, 0x0

    .line 13136
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 13139
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/ax;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/aw;->a(Lcom/sec/chaton/a/aw;J)J

    .line 13140
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 13141
    or-int/lit8 v0, v0, 0x2

    .line 13143
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ax;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/aw;->a(Lcom/sec/chaton/a/aw;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13144
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 13145
    or-int/lit8 v0, v0, 0x4

    .line 13147
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/aw;->a(Lcom/sec/chaton/a/aw;Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ey;

    .line 13148
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 13149
    or-int/lit8 v0, v0, 0x8

    .line 13151
    :cond_2
    iget-wide v4, p0, Lcom/sec/chaton/a/ax;->e:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/aw;->b(Lcom/sec/chaton/a/aw;J)J

    .line 13152
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 13153
    or-int/lit8 v0, v0, 0x10

    .line 13155
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/aw;->a(Lcom/sec/chaton/a/aw;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 13156
    invoke-static {v2, v0}, Lcom/sec/chaton/a/aw;->a(Lcom/sec/chaton/a/aw;I)I

    .line 13157
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 13300
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 13303
    iget-object v0, p0, Lcom/sec/chaton/a/ax;->d:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->c()Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0}, Lcom/sec/chaton/a/ax;->c()Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 13364
    iget v0, p0, Lcom/sec/chaton/a/ax;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 13367
    iget-object v0, p0, Lcom/sec/chaton/a/ax;->f:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 13181
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ax;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    check-cast p1, Lcom/sec/chaton/a/aw;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ax;->a(Lcom/sec/chaton/a/aw;)Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13076
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ax;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method
