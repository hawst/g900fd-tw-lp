.class public final enum Lcom/sec/chaton/a/be;
.super Ljava/lang/Enum;
.source "SSMGPB.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/be;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/be;

.field public static final enum b:Lcom/sec/chaton/a/be;

.field public static final enum c:Lcom/sec/chaton/a/be;

.field public static final enum d:Lcom/sec/chaton/a/be;

.field private static e:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sec/chaton/a/be;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic g:[Lcom/sec/chaton/a/be;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 116
    new-instance v0, Lcom/sec/chaton/a/be;

    const-string v1, "CONTENT_TEXT"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sec/chaton/a/be;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/be;->a:Lcom/sec/chaton/a/be;

    .line 117
    new-instance v0, Lcom/sec/chaton/a/be;

    const-string v1, "CONTENT_IMAGE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sec/chaton/a/be;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/be;->b:Lcom/sec/chaton/a/be;

    .line 118
    new-instance v0, Lcom/sec/chaton/a/be;

    const-string v1, "CONTENT_VIDEO"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sec/chaton/a/be;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/be;->c:Lcom/sec/chaton/a/be;

    .line 119
    new-instance v0, Lcom/sec/chaton/a/be;

    const-string v1, "CONTENT_AUDIO"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sec/chaton/a/be;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/be;->d:Lcom/sec/chaton/a/be;

    .line 114
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/a/be;

    sget-object v1, Lcom/sec/chaton/a/be;->a:Lcom/sec/chaton/a/be;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/be;->b:Lcom/sec/chaton/a/be;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/a/be;->c:Lcom/sec/chaton/a/be;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/a/be;->d:Lcom/sec/chaton/a/be;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/a/be;->g:[Lcom/sec/chaton/a/be;

    .line 145
    new-instance v0, Lcom/sec/chaton/a/bf;

    invoke-direct {v0}, Lcom/sec/chaton/a/bf;-><init>()V

    sput-object v0, Lcom/sec/chaton/a/be;->e:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 155
    iput p4, p0, Lcom/sec/chaton/a/be;->f:I

    .line 156
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/a/be;
    .locals 1

    .prologue
    .line 131
    packed-switch p0, :pswitch_data_0

    .line 136
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 132
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/a/be;->a:Lcom/sec/chaton/a/be;

    goto :goto_0

    .line 133
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/a/be;->b:Lcom/sec/chaton/a/be;

    goto :goto_0

    .line 134
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/a/be;->c:Lcom/sec/chaton/a/be;

    goto :goto_0

    .line 135
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/a/be;->d:Lcom/sec/chaton/a/be;

    goto :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/be;
    .locals 1

    .prologue
    .line 114
    const-class v0, Lcom/sec/chaton/a/be;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/be;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/be;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/sec/chaton/a/be;->g:[Lcom/sec/chaton/a/be;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/be;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/be;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/chaton/a/be;->f:I

    return v0
.end method
