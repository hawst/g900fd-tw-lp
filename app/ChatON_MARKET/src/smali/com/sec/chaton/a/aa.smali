.class public final Lcom/sec/chaton/a/aa;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ac;


# static fields
.field private static final a:Lcom/sec/chaton/a/aa;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24859
    new-instance v0, Lcom/sec/chaton/a/aa;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/aa;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/aa;->a:Lcom/sec/chaton/a/aa;

    .line 24860
    sget-object v0, Lcom/sec/chaton/a/aa;->a:Lcom/sec/chaton/a/aa;

    invoke-direct {v0}, Lcom/sec/chaton/a/aa;->i()V

    .line 24861
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ab;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24516
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 24554
    iput-byte v0, p0, Lcom/sec/chaton/a/aa;->e:B

    .line 24574
    iput v0, p0, Lcom/sec/chaton/a/aa;->f:I

    .line 24517
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ab;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 24511
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/aa;-><init>(Lcom/sec/chaton/a/ab;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24518
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24554
    iput-byte v0, p0, Lcom/sec/chaton/a/aa;->e:B

    .line 24574
    iput v0, p0, Lcom/sec/chaton/a/aa;->f:I

    .line 24518
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/aa;I)I
    .locals 0

    .prologue
    .line 24511
    iput p1, p0, Lcom/sec/chaton/a/aa;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/aa;J)J
    .locals 0

    .prologue
    .line 24511
    iput-wide p1, p0, Lcom/sec/chaton/a/aa;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/aa;
    .locals 1

    .prologue
    .line 24522
    sget-object v0, Lcom/sec/chaton/a/aa;->a:Lcom/sec/chaton/a/aa;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/aa;
    .locals 1

    .prologue
    .line 24613
    invoke-static {}, Lcom/sec/chaton/a/aa;->newBuilder()Lcom/sec/chaton/a/ab;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ab;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/ab;

    invoke-static {v0}, Lcom/sec/chaton/a/ab;->a(Lcom/sec/chaton/a/ab;)Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/aa;)Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24669
    invoke-static {}, Lcom/sec/chaton/a/aa;->newBuilder()Lcom/sec/chaton/a/ab;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ab;->a(Lcom/sec/chaton/a/aa;)Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/aa;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 24511
    iput-object p1, p0, Lcom/sec/chaton/a/aa;->d:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method private i()V
    .locals 2

    .prologue
    .line 24551
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/aa;->c:J

    .line 24552
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/aa;->d:Lcom/sec/chaton/a/ej;

    .line 24553
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24666
    invoke-static {}, Lcom/sec/chaton/a/ab;->h()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/aa;
    .locals 1

    .prologue
    .line 24526
    sget-object v0, Lcom/sec/chaton/a/aa;->a:Lcom/sec/chaton/a/aa;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 24534
    iget v1, p0, Lcom/sec/chaton/a/aa;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 24537
    iget-wide v0, p0, Lcom/sec/chaton/a/aa;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 24544
    iget v0, p0, Lcom/sec/chaton/a/aa;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 24547
    iget-object v0, p0, Lcom/sec/chaton/a/aa;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24667
    invoke-static {}, Lcom/sec/chaton/a/aa;->newBuilder()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24511
    invoke-virtual {p0}, Lcom/sec/chaton/a/aa;->b()Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 24576
    iget v0, p0, Lcom/sec/chaton/a/aa;->f:I

    .line 24577
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 24589
    :goto_0
    return v0

    .line 24579
    :cond_0
    const/4 v0, 0x0

    .line 24580
    iget v1, p0, Lcom/sec/chaton/a/aa;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 24581
    iget-wide v1, p0, Lcom/sec/chaton/a/aa;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 24584
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/aa;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 24585
    iget-object v1, p0, Lcom/sec/chaton/a/aa;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24588
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/aa;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24671
    invoke-static {p0}, Lcom/sec/chaton/a/aa;->a(Lcom/sec/chaton/a/aa;)Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 24556
    iget-byte v1, p0, Lcom/sec/chaton/a/aa;->e:B

    .line 24557
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 24560
    :goto_0
    return v0

    .line 24557
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 24559
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/aa;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24511
    invoke-virtual {p0}, Lcom/sec/chaton/a/aa;->g()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24511
    invoke-virtual {p0}, Lcom/sec/chaton/a/aa;->h()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24596
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 24565
    invoke-virtual {p0}, Lcom/sec/chaton/a/aa;->getSerializedSize()I

    .line 24566
    iget v0, p0, Lcom/sec/chaton/a/aa;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 24567
    iget-wide v0, p0, Lcom/sec/chaton/a/aa;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 24569
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/aa;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 24570
    iget-object v0, p0, Lcom/sec/chaton/a/aa;->d:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 24572
    :cond_1
    return-void
.end method
