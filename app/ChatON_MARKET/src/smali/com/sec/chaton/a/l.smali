.class public final Lcom/sec/chaton/a/l;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/n;


# static fields
.field private static final a:Lcom/sec/chaton/a/l;


# instance fields
.field private b:I

.field private c:J

.field private d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/aj;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/protobuf/LazyStringList;

.field private g:J

.field private h:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19919
    new-instance v0, Lcom/sec/chaton/a/l;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/l;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/l;->a:Lcom/sec/chaton/a/l;

    .line 19920
    sget-object v0, Lcom/sec/chaton/a/l;->a:Lcom/sec/chaton/a/l;

    invoke-direct {v0}, Lcom/sec/chaton/a/l;->s()V

    .line 19921
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/m;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19175
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 19316
    iput-byte v0, p0, Lcom/sec/chaton/a/l;->i:B

    .line 19348
    iput v0, p0, Lcom/sec/chaton/a/l;->j:I

    .line 19176
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/m;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 19170
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/l;-><init>(Lcom/sec/chaton/a/m;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19177
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 19316
    iput-byte v0, p0, Lcom/sec/chaton/a/l;->i:B

    .line 19348
    iput v0, p0, Lcom/sec/chaton/a/l;->j:I

    .line 19177
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/l;I)I
    .locals 0

    .prologue
    .line 19170
    iput p1, p0, Lcom/sec/chaton/a/l;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/l;J)J
    .locals 0

    .prologue
    .line 19170
    iput-wide p1, p0, Lcom/sec/chaton/a/l;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/l;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 19170
    iput-object p1, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/l;
    .locals 1

    .prologue
    .line 19181
    sget-object v0, Lcom/sec/chaton/a/l;->a:Lcom/sec/chaton/a/l;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/l;
    .locals 1

    .prologue
    .line 19408
    invoke-static {}, Lcom/sec/chaton/a/l;->newBuilder()Lcom/sec/chaton/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/m;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/m;

    invoke-static {v0}, Lcom/sec/chaton/a/m;->a(Lcom/sec/chaton/a/m;)Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/l;)Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19464
    invoke-static {}, Lcom/sec/chaton/a/l;->newBuilder()Lcom/sec/chaton/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/m;->a(Lcom/sec/chaton/a/l;)Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/l;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 19170
    iput-object p1, p0, Lcom/sec/chaton/a/l;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/l;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 19170
    iput-object p1, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/l;J)J
    .locals 0

    .prologue
    .line 19170
    iput-wide p1, p0, Lcom/sec/chaton/a/l;->g:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/l;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 19170
    iput-object p1, p0, Lcom/sec/chaton/a/l;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/l;)Ljava/util/List;
    .locals 1

    .prologue
    .line 19170
    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/a/l;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 19170
    iget-object v0, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19461
    invoke-static {}, Lcom/sec/chaton/a/m;->f()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 19220
    iget-object v0, p0, Lcom/sec/chaton/a/l;->d:Ljava/lang/Object;

    .line 19221
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19222
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 19224
    iput-object v0, p0, Lcom/sec/chaton/a/l;->d:Ljava/lang/Object;

    .line 19227
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 19297
    iget-object v0, p0, Lcom/sec/chaton/a/l;->h:Ljava/lang/Object;

    .line 19298
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19299
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 19301
    iput-object v0, p0, Lcom/sec/chaton/a/l;->h:Ljava/lang/Object;

    .line 19304
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 19309
    iput-wide v1, p0, Lcom/sec/chaton/a/l;->c:J

    .line 19310
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/l;->d:Ljava/lang/Object;

    .line 19311
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    .line 19312
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    .line 19313
    iput-wide v1, p0, Lcom/sec/chaton/a/l;->g:J

    .line 19314
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/l;->h:Ljava/lang/Object;

    .line 19315
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/a/aj;
    .locals 1

    .prologue
    .line 19245
    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/aj;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/l;
    .locals 1

    .prologue
    .line 19185
    sget-object v0, Lcom/sec/chaton/a/l;->a:Lcom/sec/chaton/a/l;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 19193
    iget v1, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 19196
    iget-wide v0, p0, Lcom/sec/chaton/a/l;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 19203
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19206
    iget-object v0, p0, Lcom/sec/chaton/a/l;->d:Ljava/lang/Object;

    .line 19207
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19208
    check-cast v0, Ljava/lang/String;

    .line 19216
    :goto_0
    return-object v0

    .line 19210
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 19212
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 19213
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19214
    iput-object v1, p0, Lcom/sec/chaton/a/l;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 19216
    goto :goto_0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19235
    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19170
    invoke-virtual {p0}, Lcom/sec/chaton/a/l;->b()Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 19350
    iget v0, p0, Lcom/sec/chaton/a/l;->j:I

    .line 19351
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 19384
    :goto_0
    return v0

    .line 19354
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 19355
    iget-wide v2, p0, Lcom/sec/chaton/a/l;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 19358
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 19359
    invoke-direct {p0}, Lcom/sec/chaton/a/l;->q()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v0

    .line 19362
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 19363
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    .line 19362
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 19368
    :goto_3
    iget-object v2, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 19369
    iget-object v2, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 19368
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 19372
    :cond_3
    add-int/2addr v0, v3

    .line 19373
    invoke-virtual {p0}, Lcom/sec/chaton/a/l;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 19375
    iget v1, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 19376
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/sec/chaton/a/l;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 19379
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 19380
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/l;->r()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19383
    :cond_5
    iput v0, p0, Lcom/sec/chaton/a/l;->j:I

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public h()I
    .locals 1

    .prologue
    .line 19242
    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19257
    iget-object v0, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 19318
    iget-byte v1, p0, Lcom/sec/chaton/a/l;->i:B

    .line 19319
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 19322
    :goto_0
    return v0

    .line 19319
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 19321
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/l;->i:B

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 19260
    iget-object v0, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 19270
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 19273
    iget-wide v0, p0, Lcom/sec/chaton/a/l;->g:J

    return-wide v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 19280
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19283
    iget-object v0, p0, Lcom/sec/chaton/a/l;->h:Ljava/lang/Object;

    .line 19284
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19285
    check-cast v0, Ljava/lang/String;

    .line 19293
    :goto_0
    return-object v0

    .line 19287
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 19289
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 19290
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19291
    iput-object v1, p0, Lcom/sec/chaton/a/l;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 19293
    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19170
    invoke-virtual {p0}, Lcom/sec/chaton/a/l;->o()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19462
    invoke-static {}, Lcom/sec/chaton/a/l;->newBuilder()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19466
    invoke-static {p0}, Lcom/sec/chaton/a/l;->a(Lcom/sec/chaton/a/l;)Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19170
    invoke-virtual {p0}, Lcom/sec/chaton/a/l;->p()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19391
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19327
    invoke-virtual {p0}, Lcom/sec/chaton/a/l;->getSerializedSize()I

    .line 19328
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 19329
    iget-wide v0, p0, Lcom/sec/chaton/a/l;->c:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 19331
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 19332
    invoke-direct {p0}, Lcom/sec/chaton/a/l;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    move v1, v2

    .line 19334
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 19335
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/l;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 19334
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 19337
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 19338
    iget-object v0, p0, Lcom/sec/chaton/a/l;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, v2}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 19337
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 19340
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 19341
    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sec/chaton/a/l;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 19343
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/l;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 19344
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/l;->r()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 19346
    :cond_5
    return-void
.end method
