.class public final Lcom/sec/chaton/a/ey;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/fa;


# static fields
.field private static final a:Lcom/sec/chaton/a/ey;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6089
    new-instance v0, Lcom/sec/chaton/a/ey;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ey;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ey;->a:Lcom/sec/chaton/a/ey;

    .line 6090
    sget-object v0, Lcom/sec/chaton/a/ey;->a:Lcom/sec/chaton/a/ey;

    invoke-direct {v0}, Lcom/sec/chaton/a/ey;->j()V

    .line 6091
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ez;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5735
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 5795
    iput-byte v0, p0, Lcom/sec/chaton/a/ey;->e:B

    .line 5815
    iput v0, p0, Lcom/sec/chaton/a/ey;->f:I

    .line 5736
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ez;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 5730
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ey;-><init>(Lcom/sec/chaton/a/ez;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5737
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5795
    iput-byte v0, p0, Lcom/sec/chaton/a/ey;->e:B

    .line 5815
    iput v0, p0, Lcom/sec/chaton/a/ey;->f:I

    .line 5737
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ey;I)I
    .locals 0

    .prologue
    .line 5730
    iput p1, p0, Lcom/sec/chaton/a/ey;->d:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 5741
    sget-object v0, Lcom/sec/chaton/a/ey;->a:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5910
    invoke-static {}, Lcom/sec/chaton/a/ey;->newBuilder()Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ey;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5730
    iput-object p1, p0, Lcom/sec/chaton/a/ey;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ey;I)I
    .locals 0

    .prologue
    .line 5730
    iput p1, p0, Lcom/sec/chaton/a/ey;->b:I

    return p1
.end method

.method private i()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5770
    iget-object v0, p0, Lcom/sec/chaton/a/ey;->c:Ljava/lang/Object;

    .line 5771
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5772
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5774
    iput-object v0, p0, Lcom/sec/chaton/a/ey;->c:Ljava/lang/Object;

    .line 5777
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 5792
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ey;->c:Ljava/lang/Object;

    .line 5793
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/ey;->d:I

    .line 5794
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5907
    invoke-static {}, Lcom/sec/chaton/a/ez;->f()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 5745
    sget-object v0, Lcom/sec/chaton/a/ey;->a:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5753
    iget v1, p0, Lcom/sec/chaton/a/ey;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5756
    iget-object v0, p0, Lcom/sec/chaton/a/ey;->c:Ljava/lang/Object;

    .line 5757
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5758
    check-cast v0, Ljava/lang/String;

    .line 5766
    :goto_0
    return-object v0

    .line 5760
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5762
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5763
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5764
    iput-object v1, p0, Lcom/sec/chaton/a/ey;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 5766
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 5785
    iget v0, p0, Lcom/sec/chaton/a/ey;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 5788
    iget v0, p0, Lcom/sec/chaton/a/ey;->d:I

    return v0
.end method

.method public g()Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5908
    invoke-static {}, Lcom/sec/chaton/a/ey;->newBuilder()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5730
    invoke-virtual {p0}, Lcom/sec/chaton/a/ey;->b()Lcom/sec/chaton/a/ey;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5817
    iget v0, p0, Lcom/sec/chaton/a/ey;->f:I

    .line 5818
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5830
    :goto_0
    return v0

    .line 5820
    :cond_0
    const/4 v0, 0x0

    .line 5821
    iget v1, p0, Lcom/sec/chaton/a/ey;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 5822
    invoke-direct {p0}, Lcom/sec/chaton/a/ey;->i()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5825
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/ey;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 5826
    iget v1, p0, Lcom/sec/chaton/a/ey;->d:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5829
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/ey;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ez;
    .locals 1

    .prologue
    .line 5912
    invoke-static {p0}, Lcom/sec/chaton/a/ey;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 5797
    iget-byte v1, p0, Lcom/sec/chaton/a/ey;->e:B

    .line 5798
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 5801
    :goto_0
    return v0

    .line 5798
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5800
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ey;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5730
    invoke-virtual {p0}, Lcom/sec/chaton/a/ey;->g()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5730
    invoke-virtual {p0}, Lcom/sec/chaton/a/ey;->h()Lcom/sec/chaton/a/ez;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5837
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5806
    invoke-virtual {p0}, Lcom/sec/chaton/a/ey;->getSerializedSize()I

    .line 5807
    iget v0, p0, Lcom/sec/chaton/a/ey;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5808
    invoke-direct {p0}, Lcom/sec/chaton/a/ey;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5810
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ey;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 5811
    iget v0, p0, Lcom/sec/chaton/a/ey;->d:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 5813
    :cond_1
    return-void
.end method
