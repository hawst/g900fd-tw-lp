.class public final Lcom/sec/chaton/a/m;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/l;",
        "Lcom/sec/chaton/a/m;",
        ">;",
        "Lcom/sec/chaton/a/n;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/aj;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/protobuf/LazyStringList;

.field private f:J

.field private g:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 19473
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 19678
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/m;->c:Ljava/lang/Object;

    .line 19714
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    .line 19803
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    .line 19880
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/m;->g:Ljava/lang/Object;

    .line 19474
    invoke-direct {p0}, Lcom/sec/chaton/a/m;->g()V

    .line 19475
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/m;)Lcom/sec/chaton/a/l;
    .locals 1

    .prologue
    .line 19468
    invoke-direct {p0}, Lcom/sec/chaton/a/m;->i()Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19468
    invoke-static {}, Lcom/sec/chaton/a/m;->h()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 19478
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19480
    new-instance v0, Lcom/sec/chaton/a/m;

    invoke-direct {v0}, Lcom/sec/chaton/a/m;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/l;
    .locals 2

    .prologue
    .line 19518
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->e()Lcom/sec/chaton/a/l;

    move-result-object v0

    .line 19519
    invoke-virtual {v0}, Lcom/sec/chaton/a/l;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 19520
    invoke-static {v0}, Lcom/sec/chaton/a/m;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 19523
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 19717
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 19718
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    .line 19719
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19721
    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 19805
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 19806
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    .line 19807
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19809
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/m;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 19484
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 19485
    iput-wide v1, p0, Lcom/sec/chaton/a/m;->b:J

    .line 19486
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19487
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/m;->c:Ljava/lang/Object;

    .line 19488
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19489
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    .line 19490
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19491
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    .line 19492
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19493
    iput-wide v1, p0, Lcom/sec/chaton/a/m;->f:J

    .line 19494
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19495
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/m;->g:Ljava/lang/Object;

    .line 19496
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19497
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19665
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19666
    iput-wide p1, p0, Lcom/sec/chaton/a/m;->b:J

    .line 19668
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/m;
    .locals 2

    .prologue
    .line 19607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 19608
    sparse-switch v0, :sswitch_data_0

    .line 19613
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/m;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19615
    :sswitch_0
    return-object p0

    .line 19620
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19621
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/m;->b:J

    goto :goto_0

    .line 19625
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19626
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/m;->c:Ljava/lang/Object;

    goto :goto_0

    .line 19630
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/aj;->newBuilder()Lcom/sec/chaton/a/ak;

    move-result-object v0

    .line 19631
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 19632
    invoke-virtual {v0}, Lcom/sec/chaton/a/ak;->e()Lcom/sec/chaton/a/aj;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/m;->a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/m;

    goto :goto_0

    .line 19636
    :sswitch_4
    invoke-direct {p0}, Lcom/sec/chaton/a/m;->k()V

    .line 19637
    iget-object v0, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 19641
    :sswitch_5
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19642
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/m;->f:J

    goto :goto_0

    .line 19646
    :sswitch_6
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19647
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/m;->g:Ljava/lang/Object;

    goto :goto_0

    .line 19608
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19750
    if-nez p1, :cond_0

    .line 19751
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19753
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/m;->j()V

    .line 19754
    iget-object v0, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19756
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/l;)Lcom/sec/chaton/a/m;
    .locals 2

    .prologue
    .line 19562
    invoke-static {}, Lcom/sec/chaton/a/l;->a()Lcom/sec/chaton/a/l;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 19595
    :cond_0
    :goto_0
    return-object p0

    .line 19563
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 19564
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/m;->a(J)Lcom/sec/chaton/a/m;

    .line 19566
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 19567
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/m;->a(Ljava/lang/String;)Lcom/sec/chaton/a/m;

    .line 19569
    :cond_3
    invoke-static {p1}, Lcom/sec/chaton/a/l;->b(Lcom/sec/chaton/a/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 19570
    iget-object v0, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 19571
    invoke-static {p1}, Lcom/sec/chaton/a/l;->b(Lcom/sec/chaton/a/l;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    .line 19572
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19579
    :cond_4
    :goto_1
    invoke-static {p1}, Lcom/sec/chaton/a/l;->c(Lcom/sec/chaton/a/l;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 19580
    iget-object v0, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 19581
    invoke-static {p1}, Lcom/sec/chaton/a/l;->c(Lcom/sec/chaton/a/l;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    .line 19582
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19589
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 19590
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->l()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/m;->b(J)Lcom/sec/chaton/a/m;

    .line 19592
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19593
    invoke-virtual {p1}, Lcom/sec/chaton/a/l;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/m;->b(Ljava/lang/String;)Lcom/sec/chaton/a/m;

    goto :goto_0

    .line 19574
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/a/m;->j()V

    .line 19575
    iget-object v0, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/l;->b(Lcom/sec/chaton/a/l;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 19584
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/a/m;->k()V

    .line 19585
    iget-object v0, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/l;->c(Lcom/sec/chaton/a/l;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19693
    if-nez p1, :cond_0

    .line 19694
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19696
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19697
    iput-object p1, p0, Lcom/sec/chaton/a/m;->c:Ljava/lang/Object;

    .line 19699
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/m;
    .locals 2

    .prologue
    .line 19501
    invoke-static {}, Lcom/sec/chaton/a/m;->h()Lcom/sec/chaton/a/m;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->e()Lcom/sec/chaton/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/m;->a(Lcom/sec/chaton/a/l;)Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19867
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19868
    iput-wide p1, p0, Lcom/sec/chaton/a/m;->f:J

    .line 19870
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/m;
    .locals 1

    .prologue
    .line 19895
    if-nez p1, :cond_0

    .line 19896
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19898
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/m;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19899
    iput-object p1, p0, Lcom/sec/chaton/a/m;->g:Ljava/lang/Object;

    .line 19901
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->d()Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->e()Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/l;
    .locals 1

    .prologue
    .line 19505
    invoke-static {}, Lcom/sec/chaton/a/l;->a()Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->a()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->a()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->b()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->b()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->b()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->b()Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/l;
    .locals 2

    .prologue
    .line 19509
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->e()Lcom/sec/chaton/a/l;

    move-result-object v0

    .line 19510
    invoke-virtual {v0}, Lcom/sec/chaton/a/l;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 19511
    invoke-static {v0}, Lcom/sec/chaton/a/m;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 19513
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/l;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 19527
    new-instance v2, Lcom/sec/chaton/a/l;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/l;-><init>(Lcom/sec/chaton/a/m;Lcom/sec/chaton/a/b;)V

    .line 19528
    iget v3, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19529
    const/4 v1, 0x0

    .line 19530
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 19533
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/m;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/l;->a(Lcom/sec/chaton/a/l;J)J

    .line 19534
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 19535
    or-int/lit8 v0, v0, 0x2

    .line 19537
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/m;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/l;->a(Lcom/sec/chaton/a/l;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19538
    iget v1, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 19539
    iget-object v1, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    .line 19540
    iget v1, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19542
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/m;->d:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/l;->a(Lcom/sec/chaton/a/l;Ljava/util/List;)Ljava/util/List;

    .line 19543
    iget v1, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 19544
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    .line 19546
    iget v1, p0, Lcom/sec/chaton/a/m;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/sec/chaton/a/m;->a:I

    .line 19548
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/m;->e:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/l;->a(Lcom/sec/chaton/a/l;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 19549
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 19550
    or-int/lit8 v0, v0, 0x4

    .line 19552
    :cond_3
    iget-wide v4, p0, Lcom/sec/chaton/a/m;->f:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/l;->b(Lcom/sec/chaton/a/l;J)J

    .line 19553
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 19554
    or-int/lit8 v0, v0, 0x8

    .line 19556
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/m;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/l;->b(Lcom/sec/chaton/a/l;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19557
    invoke-static {v2, v0}, Lcom/sec/chaton/a/l;->a(Lcom/sec/chaton/a/l;I)I

    .line 19558
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->c()Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0}, Lcom/sec/chaton/a/m;->c()Lcom/sec/chaton/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 19599
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/m;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    check-cast p1, Lcom/sec/chaton/a/l;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/m;->a(Lcom/sec/chaton/a/l;)Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19468
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/m;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/m;

    move-result-object v0

    return-object v0
.end method
