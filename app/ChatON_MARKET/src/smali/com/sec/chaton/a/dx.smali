.class public final Lcom/sec/chaton/a/dx;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dz;


# static fields
.field private static final a:Lcom/sec/chaton/a/dx;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/google/protobuf/LazyStringList;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20679
    new-instance v0, Lcom/sec/chaton/a/dx;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/dx;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/dx;->a:Lcom/sec/chaton/a/dx;

    .line 20680
    sget-object v0, Lcom/sec/chaton/a/dx;->a:Lcom/sec/chaton/a/dx;

    invoke-direct {v0}, Lcom/sec/chaton/a/dx;->h()V

    .line 20681
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/dy;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 20309
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 20351
    iput-byte v0, p0, Lcom/sec/chaton/a/dx;->e:B

    .line 20371
    iput v0, p0, Lcom/sec/chaton/a/dx;->f:I

    .line 20310
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/dy;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 20304
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/dx;-><init>(Lcom/sec/chaton/a/dy;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 20311
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 20351
    iput-byte v0, p0, Lcom/sec/chaton/a/dx;->e:B

    .line 20371
    iput v0, p0, Lcom/sec/chaton/a/dx;->f:I

    .line 20311
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dx;I)I
    .locals 0

    .prologue
    .line 20304
    iput p1, p0, Lcom/sec/chaton/a/dx;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dx;J)J
    .locals 0

    .prologue
    .line 20304
    iput-wide p1, p0, Lcom/sec/chaton/a/dx;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dx;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 20304
    iput-object p1, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/dx;
    .locals 1

    .prologue
    .line 20315
    sget-object v0, Lcom/sec/chaton/a/dx;->a:Lcom/sec/chaton/a/dx;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/dx;
    .locals 1

    .prologue
    .line 20415
    invoke-static {}, Lcom/sec/chaton/a/dx;->newBuilder()Lcom/sec/chaton/a/dy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dy;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dy;

    invoke-static {v0}, Lcom/sec/chaton/a/dy;->a(Lcom/sec/chaton/a/dy;)Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/dx;)Lcom/sec/chaton/a/dy;
    .locals 1

    .prologue
    .line 20471
    invoke-static {}, Lcom/sec/chaton/a/dx;->newBuilder()Lcom/sec/chaton/a/dy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dy;->a(Lcom/sec/chaton/a/dx;)Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/dx;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 20304
    iget-object v0, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 20348
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/dx;->c:J

    .line 20349
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    .line 20350
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/dy;
    .locals 1

    .prologue
    .line 20468
    invoke-static {}, Lcom/sec/chaton/a/dy;->f()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/dx;
    .locals 1

    .prologue
    .line 20319
    sget-object v0, Lcom/sec/chaton/a/dx;->a:Lcom/sec/chaton/a/dx;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 20327
    iget v1, p0, Lcom/sec/chaton/a/dx;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 20330
    iget-wide v0, p0, Lcom/sec/chaton/a/dx;->c:J

    return-wide v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20338
    iget-object v0, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public f()Lcom/sec/chaton/a/dy;
    .locals 1

    .prologue
    .line 20469
    invoke-static {}, Lcom/sec/chaton/a/dx;->newBuilder()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/dy;
    .locals 1

    .prologue
    .line 20473
    invoke-static {p0}, Lcom/sec/chaton/a/dx;->a(Lcom/sec/chaton/a/dx;)Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20304
    invoke-virtual {p0}, Lcom/sec/chaton/a/dx;->b()Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 20373
    iget v0, p0, Lcom/sec/chaton/a/dx;->f:I

    .line 20374
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 20391
    :goto_0
    return v0

    .line 20377
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dx;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_2

    .line 20378
    iget-wide v2, p0, Lcom/sec/chaton/a/dx;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v1

    .line 20383
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 20384
    iget-object v3, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 20383
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 20387
    :cond_1
    add-int/2addr v0, v2

    .line 20388
    invoke-virtual {p0}, Lcom/sec/chaton/a/dx;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20390
    iput v0, p0, Lcom/sec/chaton/a/dx;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 20353
    iget-byte v1, p0, Lcom/sec/chaton/a/dx;->e:B

    .line 20354
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 20357
    :goto_0
    return v0

    .line 20354
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 20356
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/dx;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20304
    invoke-virtual {p0}, Lcom/sec/chaton/a/dx;->f()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20304
    invoke-virtual {p0}, Lcom/sec/chaton/a/dx;->g()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20398
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 20362
    invoke-virtual {p0}, Lcom/sec/chaton/a/dx;->getSerializedSize()I

    .line 20363
    iget v0, p0, Lcom/sec/chaton/a/dx;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 20364
    iget-wide v0, p0, Lcom/sec/chaton/a/dx;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 20366
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 20367
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/chaton/a/dx;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 20366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20369
    :cond_1
    return-void
.end method
