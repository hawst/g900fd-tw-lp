.class public final enum Lcom/sec/chaton/a/a/l;
.super Ljava/lang/Enum;
.source "MessageResultEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/a/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/a/l;

.field public static final enum b:Lcom/sec/chaton/a/a/l;

.field public static final enum c:Lcom/sec/chaton/a/a/l;

.field public static final enum d:Lcom/sec/chaton/a/a/l;

.field public static final enum e:Lcom/sec/chaton/a/a/l;

.field public static final enum f:Lcom/sec/chaton/a/a/l;

.field public static final enum g:Lcom/sec/chaton/a/a/l;

.field public static final enum h:Lcom/sec/chaton/a/a/l;

.field public static final enum i:Lcom/sec/chaton/a/a/l;

.field public static final enum j:Lcom/sec/chaton/a/a/l;

.field public static final enum k:Lcom/sec/chaton/a/a/l;

.field public static final enum l:Lcom/sec/chaton/a/a/l;

.field public static final enum m:Lcom/sec/chaton/a/a/l;

.field public static final enum n:Lcom/sec/chaton/a/a/l;

.field private static final synthetic o:[Lcom/sec/chaton/a/a/l;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "SUCCESS_RECEIVER_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->a:Lcom/sec/chaton/a/a/l;

    .line 59
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->b:Lcom/sec/chaton/a/a/l;

    .line 60
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "CHANGE_SERVER"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->c:Lcom/sec/chaton/a/a/l;

    .line 61
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->d:Lcom/sec/chaton/a/a/l;

    .line 62
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "REGARD_SUCCEESS"

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->e:Lcom/sec/chaton/a/a/l;

    .line 63
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "NEED_HANDLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->f:Lcom/sec/chaton/a/a/l;

    .line 66
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "BLOCK_RECEIVER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->g:Lcom/sec/chaton/a/a/l;

    .line 67
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "INVALID_USER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->h:Lcom/sec/chaton/a/a/l;

    .line 69
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "END_APP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    .line 70
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "RESTART_APP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    .line 71
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "SESSION_USER_NOT_EXIST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->k:Lcom/sec/chaton/a/a/l;

    .line 72
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "RESET_BY_WEB"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->l:Lcom/sec/chaton/a/a/l;

    .line 73
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "NON_WEB_USER_DETECTED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->m:Lcom/sec/chaton/a/a/l;

    .line 74
    new-instance v0, Lcom/sec/chaton/a/a/l;

    const-string v1, "SESSION_DEACTIVATED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/a/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/a/a/l;->n:Lcom/sec/chaton/a/a/l;

    .line 55
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/sec/chaton/a/a/l;

    sget-object v1, Lcom/sec/chaton/a/a/l;->a:Lcom/sec/chaton/a/a/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/a/a/l;->b:Lcom/sec/chaton/a/a/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/a/a/l;->c:Lcom/sec/chaton/a/a/l;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/a/a/l;->d:Lcom/sec/chaton/a/a/l;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/a/a/l;->e:Lcom/sec/chaton/a/a/l;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/a/a/l;->f:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/a/a/l;->g:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/a/a/l;->h:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/a/a/l;->k:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/chaton/a/a/l;->l:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/chaton/a/a/l;->m:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/chaton/a/a/l;->n:Lcom/sec/chaton/a/a/l;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/a/a/l;->o:[Lcom/sec/chaton/a/a/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/a/l;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/chaton/a/a/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/l;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/a/l;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/chaton/a/a/l;->o:[Lcom/sec/chaton/a/a/l;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/a/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/a/l;

    return-object v0
.end method
