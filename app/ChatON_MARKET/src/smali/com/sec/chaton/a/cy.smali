.class public final Lcom/sec/chaton/a/cy;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/cx;",
        "Lcom/sec/chaton/a/cy;",
        ">;",
        "Lcom/sec/chaton/a/cz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/Object;

.field private d:J

.field private e:Lcom/sec/chaton/a/ey;

.field private f:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9759
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 9940
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->c:Ljava/lang/Object;

    .line 9997
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    .line 10040
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    .line 9760
    invoke-direct {p0}, Lcom/sec/chaton/a/cy;->k()V

    .line 9761
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cy;)Lcom/sec/chaton/a/cx;
    .locals 1

    .prologue
    .line 9754
    invoke-direct {p0}, Lcom/sec/chaton/a/cy;->m()Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j()Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9754
    invoke-static {}, Lcom/sec/chaton/a/cy;->l()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 9764
    return-void
.end method

.method private static l()Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9766
    new-instance v0, Lcom/sec/chaton/a/cy;

    invoke-direct {v0}, Lcom/sec/chaton/a/cy;-><init>()V

    return-object v0
.end method

.method private m()Lcom/sec/chaton/a/cx;
    .locals 2

    .prologue
    .line 9802
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->e()Lcom/sec/chaton/a/cx;

    move-result-object v0

    .line 9803
    invoke-virtual {v0}, Lcom/sec/chaton/a/cx;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 9804
    invoke-static {v0}, Lcom/sec/chaton/a/cy;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 9807
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/cy;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 9770
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 9771
    iput-wide v1, p0, Lcom/sec/chaton/a/cy;->b:J

    .line 9772
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9773
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->c:Ljava/lang/Object;

    .line 9774
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9775
    iput-wide v1, p0, Lcom/sec/chaton/a/cy;->d:J

    .line 9776
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9777
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    .line 9778
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9779
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    .line 9780
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9781
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9927
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9928
    iput-wide p1, p0, Lcom/sec/chaton/a/cy;->b:J

    .line 9930
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cy;
    .locals 2

    .prologue
    .line 9867
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 9868
    sparse-switch v0, :sswitch_data_0

    .line 9873
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/cy;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9875
    :sswitch_0
    return-object p0

    .line 9880
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9881
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/cy;->b:J

    goto :goto_0

    .line 9885
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9886
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->c:Ljava/lang/Object;

    goto :goto_0

    .line 9890
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9891
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/cy;->d:J

    goto :goto_0

    .line 9895
    :sswitch_4
    invoke-static {}, Lcom/sec/chaton/a/ey;->newBuilder()Lcom/sec/chaton/a/ez;

    move-result-object v0

    .line 9896
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 9897
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->g()Lcom/sec/chaton/a/ey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    .line 9899
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 9900
    invoke-virtual {v0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cy;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/cy;

    goto :goto_0

    .line 9904
    :sswitch_5
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 9905
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 9906
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->i()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 9908
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 9909
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cy;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/cy;

    goto :goto_0

    .line 9868
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/cx;)Lcom/sec/chaton/a/cy;
    .locals 2

    .prologue
    .line 9839
    invoke-static {}, Lcom/sec/chaton/a/cx;->a()Lcom/sec/chaton/a/cx;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 9855
    :cond_0
    :goto_0
    return-object p0

    .line 9840
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9841
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/cy;->a(J)Lcom/sec/chaton/a/cy;

    .line 9843
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 9844
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cy;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cy;

    .line 9846
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 9847
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/cy;->b(J)Lcom/sec/chaton/a/cy;

    .line 9849
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 9850
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->j()Lcom/sec/chaton/a/ey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cy;->b(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/cy;

    .line 9852
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9853
    invoke-virtual {p1}, Lcom/sec/chaton/a/cx;->l()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cy;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/cy;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 10048
    if-nez p1, :cond_0

    .line 10049
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10051
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    .line 10053
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 10054
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ek;)Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 10058
    invoke-virtual {p1}, Lcom/sec/chaton/a/ek;->d()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    .line 10060
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 10061
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 10005
    if-nez p1, :cond_0

    .line 10006
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10008
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    .line 10010
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 10011
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9955
    if-nez p1, :cond_0

    .line 9956
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9958
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9959
    iput-object p1, p0, Lcom/sec/chaton/a/cy;->c:Ljava/lang/Object;

    .line 9961
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/cy;
    .locals 2

    .prologue
    .line 9785
    invoke-static {}, Lcom/sec/chaton/a/cy;->l()Lcom/sec/chaton/a/cy;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->e()Lcom/sec/chaton/a/cx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cy;->a(Lcom/sec/chaton/a/cx;)Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9984
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9985
    iput-wide p1, p0, Lcom/sec/chaton/a/cy;->d:J

    .line 9987
    return-object p0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/cy;
    .locals 2

    .prologue
    .line 10064
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 10066
    iget-object v0, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    .line 10072
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 10073
    return-object p0

    .line 10069
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public b(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/cy;
    .locals 2

    .prologue
    .line 10021
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 10023
    iget-object v0, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    invoke-static {v0}, Lcom/sec/chaton/a/ey;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    .line 10029
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 10030
    return-object p0

    .line 10026
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->d()Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->e()Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/cx;
    .locals 1

    .prologue
    .line 9789
    invoke-static {}, Lcom/sec/chaton/a/cx;->a()Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->a()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->a()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->b()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->b()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->b()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->b()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/cx;
    .locals 2

    .prologue
    .line 9793
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->e()Lcom/sec/chaton/a/cx;

    move-result-object v0

    .line 9794
    invoke-virtual {v0}, Lcom/sec/chaton/a/cx;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 9795
    invoke-static {v0}, Lcom/sec/chaton/a/cy;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 9797
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/cx;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 9811
    new-instance v2, Lcom/sec/chaton/a/cx;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/cx;-><init>(Lcom/sec/chaton/a/cy;Lcom/sec/chaton/a/b;)V

    .line 9812
    iget v3, p0, Lcom/sec/chaton/a/cy;->a:I

    .line 9813
    const/4 v1, 0x0

    .line 9814
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 9817
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/cy;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/cx;->a(Lcom/sec/chaton/a/cx;J)J

    .line 9818
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 9819
    or-int/lit8 v0, v0, 0x2

    .line 9821
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/cy;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cx;->a(Lcom/sec/chaton/a/cx;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9822
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 9823
    or-int/lit8 v0, v0, 0x4

    .line 9825
    :cond_1
    iget-wide v4, p0, Lcom/sec/chaton/a/cy;->d:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/cx;->b(Lcom/sec/chaton/a/cx;J)J

    .line 9826
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 9827
    or-int/lit8 v0, v0, 0x8

    .line 9829
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cx;->a(Lcom/sec/chaton/a/cx;Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ey;

    .line 9830
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 9831
    or-int/lit8 v0, v0, 0x10

    .line 9833
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cx;->a(Lcom/sec/chaton/a/cx;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 9834
    invoke-static {v2, v0}, Lcom/sec/chaton/a/cx;->a(Lcom/sec/chaton/a/cx;I)I

    .line 9835
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 9999
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 10002
    iget-object v0, p0, Lcom/sec/chaton/a/cy;->e:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->c()Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0}, Lcom/sec/chaton/a/cy;->c()Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 10042
    iget v0, p0, Lcom/sec/chaton/a/cy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 10045
    iget-object v0, p0, Lcom/sec/chaton/a/cy;->f:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 9859
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cy;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    check-cast p1, Lcom/sec/chaton/a/cx;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/cy;->a(Lcom/sec/chaton/a/cx;)Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9754
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cy;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method
