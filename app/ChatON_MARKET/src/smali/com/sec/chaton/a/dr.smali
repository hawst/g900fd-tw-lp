.class public final Lcom/sec/chaton/a/dr;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dt;


# static fields
.field private static final a:Lcom/sec/chaton/a/dr;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2805
    new-instance v0, Lcom/sec/chaton/a/dr;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/dr;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/dr;->a:Lcom/sec/chaton/a/dr;

    .line 2806
    sget-object v0, Lcom/sec/chaton/a/dr;->a:Lcom/sec/chaton/a/dr;

    invoke-direct {v0}, Lcom/sec/chaton/a/dr;->k()V

    .line 2807
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ds;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2414
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 2496
    iput-byte v0, p0, Lcom/sec/chaton/a/dr;->e:B

    .line 2516
    iput v0, p0, Lcom/sec/chaton/a/dr;->f:I

    .line 2415
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ds;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 2409
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/dr;-><init>(Lcom/sec/chaton/a/ds;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2416
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2496
    iput-byte v0, p0, Lcom/sec/chaton/a/dr;->e:B

    .line 2516
    iput v0, p0, Lcom/sec/chaton/a/dr;->f:I

    .line 2416
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dr;I)I
    .locals 0

    .prologue
    .line 2409
    iput p1, p0, Lcom/sec/chaton/a/dr;->b:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/dr;
    .locals 1

    .prologue
    .line 2420
    sget-object v0, Lcom/sec/chaton/a/dr;->a:Lcom/sec/chaton/a/dr;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2611
    invoke-static {}, Lcom/sec/chaton/a/dr;->newBuilder()Lcom/sec/chaton/a/ds;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ds;->a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/dr;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2409
    iput-object p1, p0, Lcom/sec/chaton/a/dr;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dr;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2409
    iput-object p1, p0, Lcom/sec/chaton/a/dr;->d:Ljava/lang/Object;

    return-object p1
.end method

.method private i()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2449
    iget-object v0, p0, Lcom/sec/chaton/a/dr;->c:Ljava/lang/Object;

    .line 2450
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2451
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2453
    iput-object v0, p0, Lcom/sec/chaton/a/dr;->c:Ljava/lang/Object;

    .line 2456
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private j()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2481
    iget-object v0, p0, Lcom/sec/chaton/a/dr;->d:Ljava/lang/Object;

    .line 2482
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2483
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2485
    iput-object v0, p0, Lcom/sec/chaton/a/dr;->d:Ljava/lang/Object;

    .line 2488
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 2493
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dr;->c:Ljava/lang/Object;

    .line 2494
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dr;->d:Ljava/lang/Object;

    .line 2495
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2608
    invoke-static {}, Lcom/sec/chaton/a/ds;->f()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/dr;
    .locals 1

    .prologue
    .line 2424
    sget-object v0, Lcom/sec/chaton/a/dr;->a:Lcom/sec/chaton/a/dr;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2432
    iget v1, p0, Lcom/sec/chaton/a/dr;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2435
    iget-object v0, p0, Lcom/sec/chaton/a/dr;->c:Ljava/lang/Object;

    .line 2436
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2437
    check-cast v0, Ljava/lang/String;

    .line 2445
    :goto_0
    return-object v0

    .line 2439
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2441
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2442
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2443
    iput-object v1, p0, Lcom/sec/chaton/a/dr;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2445
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 2464
    iget v0, p0, Lcom/sec/chaton/a/dr;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2467
    iget-object v0, p0, Lcom/sec/chaton/a/dr;->d:Ljava/lang/Object;

    .line 2468
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2469
    check-cast v0, Ljava/lang/String;

    .line 2477
    :goto_0
    return-object v0

    .line 2471
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2473
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2474
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2475
    iput-object v1, p0, Lcom/sec/chaton/a/dr;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2477
    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2609
    invoke-static {}, Lcom/sec/chaton/a/dr;->newBuilder()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2409
    invoke-virtual {p0}, Lcom/sec/chaton/a/dr;->b()Lcom/sec/chaton/a/dr;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2518
    iget v0, p0, Lcom/sec/chaton/a/dr;->f:I

    .line 2519
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2531
    :goto_0
    return v0

    .line 2521
    :cond_0
    const/4 v0, 0x0

    .line 2522
    iget v1, p0, Lcom/sec/chaton/a/dr;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2523
    invoke-direct {p0}, Lcom/sec/chaton/a/dr;->i()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2526
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/dr;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2527
    invoke-direct {p0}, Lcom/sec/chaton/a/dr;->j()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2530
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/dr;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2613
    invoke-static {p0}, Lcom/sec/chaton/a/dr;->a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2498
    iget-byte v1, p0, Lcom/sec/chaton/a/dr;->e:B

    .line 2499
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2502
    :goto_0
    return v0

    .line 2499
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2501
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/dr;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2409
    invoke-virtual {p0}, Lcom/sec/chaton/a/dr;->g()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2409
    invoke-virtual {p0}, Lcom/sec/chaton/a/dr;->h()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2538
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2507
    invoke-virtual {p0}, Lcom/sec/chaton/a/dr;->getSerializedSize()I

    .line 2508
    iget v0, p0, Lcom/sec/chaton/a/dr;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2509
    invoke-direct {p0}, Lcom/sec/chaton/a/dr;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2511
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dr;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2512
    invoke-direct {p0}, Lcom/sec/chaton/a/dr;->j()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2514
    :cond_1
    return-void
.end method
