.class public final Lcom/sec/chaton/a/dg;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/di;


# static fields
.field private static final a:Lcom/sec/chaton/a/dg;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/dp;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/protobuf/LazyStringList;

.field private h:Lcom/google/protobuf/LazyStringList;

.field private i:J

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17839
    new-instance v0, Lcom/sec/chaton/a/dg;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/dg;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/dg;->a:Lcom/sec/chaton/a/dg;

    .line 17840
    sget-object v0, Lcom/sec/chaton/a/dg;->a:Lcom/sec/chaton/a/dg;

    invoke-direct {v0}, Lcom/sec/chaton/a/dg;->s()V

    .line 17841
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/dh;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17070
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 17215
    iput-byte v0, p0, Lcom/sec/chaton/a/dg;->j:B

    .line 17250
    iput v0, p0, Lcom/sec/chaton/a/dg;->k:I

    .line 17071
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/dh;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 17065
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/dg;-><init>(Lcom/sec/chaton/a/dh;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17072
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 17215
    iput-byte v0, p0, Lcom/sec/chaton/a/dg;->j:B

    .line 17250
    iput v0, p0, Lcom/sec/chaton/a/dg;->k:I

    .line 17072
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dg;I)I
    .locals 0

    .prologue
    .line 17065
    iput p1, p0, Lcom/sec/chaton/a/dg;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dg;J)J
    .locals 0

    .prologue
    .line 17065
    iput-wide p1, p0, Lcom/sec/chaton/a/dg;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dg;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 17065
    iput-object p1, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/dg;
    .locals 1

    .prologue
    .line 17076
    sget-object v0, Lcom/sec/chaton/a/dg;->a:Lcom/sec/chaton/a/dg;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/dg;
    .locals 1

    .prologue
    .line 17319
    invoke-static {}, Lcom/sec/chaton/a/dg;->newBuilder()Lcom/sec/chaton/a/dh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dh;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dh;

    invoke-static {v0}, Lcom/sec/chaton/a/dh;->a(Lcom/sec/chaton/a/dh;)Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/dg;)Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17375
    invoke-static {}, Lcom/sec/chaton/a/dg;->newBuilder()Lcom/sec/chaton/a/dh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dh;->a(Lcom/sec/chaton/a/dg;)Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/dg;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;
    .locals 0

    .prologue
    .line 17065
    iput-object p1, p0, Lcom/sec/chaton/a/dg;->d:Lcom/sec/chaton/a/dp;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/dg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 17065
    iput-object p1, p0, Lcom/sec/chaton/a/dg;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dg;J)J
    .locals 0

    .prologue
    .line 17065
    iput-wide p1, p0, Lcom/sec/chaton/a/dg;->i:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 17065
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/dg;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 17065
    iput-object p1, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 17065
    iput-object p1, p0, Lcom/sec/chaton/a/dg;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/dg;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 17065
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17372
    invoke-static {}, Lcom/sec/chaton/a/dh;->m()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 17125
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->e:Ljava/lang/Object;

    .line 17126
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17127
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 17129
    iput-object v0, p0, Lcom/sec/chaton/a/dg;->e:Ljava/lang/Object;

    .line 17132
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 17157
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->f:Ljava/lang/Object;

    .line 17158
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17159
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 17161
    iput-object v0, p0, Lcom/sec/chaton/a/dg;->f:Ljava/lang/Object;

    .line 17164
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 17207
    iput-wide v1, p0, Lcom/sec/chaton/a/dg;->c:J

    .line 17208
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/dg;->d:Lcom/sec/chaton/a/dp;

    .line 17209
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dg;->e:Ljava/lang/Object;

    .line 17210
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dg;->f:Ljava/lang/Object;

    .line 17211
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    .line 17212
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    .line 17213
    iput-wide v1, p0, Lcom/sec/chaton/a/dg;->i:J

    .line 17214
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/dg;
    .locals 1

    .prologue
    .line 17080
    sget-object v0, Lcom/sec/chaton/a/dg;->a:Lcom/sec/chaton/a/dg;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 17088
    iget v1, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 17091
    iget-wide v0, p0, Lcom/sec/chaton/a/dg;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 17098
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 17101
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->d:Lcom/sec/chaton/a/dp;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 17108
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17065
    invoke-virtual {p0}, Lcom/sec/chaton/a/dg;->b()Lcom/sec/chaton/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 17252
    iget v0, p0, Lcom/sec/chaton/a/dg;->k:I

    .line 17253
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 17295
    :goto_0
    return v0

    .line 17256
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 17257
    iget-wide v2, p0, Lcom/sec/chaton/a/dg;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 17260
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 17261
    iget-object v2, p0, Lcom/sec/chaton/a/dg;->d:Lcom/sec/chaton/a/dp;

    invoke-virtual {v2}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 17264
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 17265
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/dg;->q()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17268
    :cond_2
    iget v2, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 17269
    invoke-direct {p0}, Lcom/sec/chaton/a/dg;->r()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v1

    .line 17274
    :goto_2
    iget-object v4, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 17275
    iget-object v4, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v2}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v3, v4

    .line 17274
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 17278
    :cond_4
    add-int/2addr v0, v3

    .line 17279
    invoke-virtual {p0}, Lcom/sec/chaton/a/dg;->k()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    move v0, v1

    .line 17283
    :goto_3
    iget-object v3, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 17284
    iget-object v3, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v0, v3

    .line 17283
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 17287
    :cond_5
    add-int/2addr v0, v2

    .line 17288
    invoke-virtual {p0}, Lcom/sec/chaton/a/dg;->l()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17290
    iget v1, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 17291
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/sec/chaton/a/dg;->i:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 17294
    :cond_6
    iput v0, p0, Lcom/sec/chaton/a/dg;->k:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17111
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->e:Ljava/lang/Object;

    .line 17112
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17113
    check-cast v0, Ljava/lang/String;

    .line 17121
    :goto_0
    return-object v0

    .line 17115
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 17117
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 17118
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17119
    iput-object v1, p0, Lcom/sec/chaton/a/dg;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 17121
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 17140
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 17217
    iget-byte v1, p0, Lcom/sec/chaton/a/dg;->j:B

    .line 17218
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 17221
    :goto_0
    return v0

    .line 17218
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 17220
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/dg;->j:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17143
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->f:Ljava/lang/Object;

    .line 17144
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17145
    check-cast v0, Ljava/lang/String;

    .line 17153
    :goto_0
    return-object v0

    .line 17147
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 17149
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 17150
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17151
    iput-object v1, p0, Lcom/sec/chaton/a/dg;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 17153
    goto :goto_0
.end method

.method public k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17173
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public l()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17187
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 17200
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 17203
    iget-wide v0, p0, Lcom/sec/chaton/a/dg;->i:J

    return-wide v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17065
    invoke-virtual {p0}, Lcom/sec/chaton/a/dg;->o()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17373
    invoke-static {}, Lcom/sec/chaton/a/dg;->newBuilder()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/sec/chaton/a/dh;
    .locals 1

    .prologue
    .line 17377
    invoke-static {p0}, Lcom/sec/chaton/a/dg;->a(Lcom/sec/chaton/a/dg;)Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17065
    invoke-virtual {p0}, Lcom/sec/chaton/a/dg;->p()Lcom/sec/chaton/a/dh;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17302
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 17226
    invoke-virtual {p0}, Lcom/sec/chaton/a/dg;->getSerializedSize()I

    .line 17227
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 17228
    iget-wide v2, p0, Lcom/sec/chaton/a/dg;->c:J

    invoke-virtual {p1, v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 17230
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 17231
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->d:Lcom/sec/chaton/a/dp;

    invoke-virtual {v0}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 17233
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 17234
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/dg;->q()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 17236
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 17237
    invoke-direct {p0}, Lcom/sec/chaton/a/dg;->r()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    move v0, v1

    .line 17239
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 17240
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/sec/chaton/a/dg;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 17239
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17242
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 17243
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/sec/chaton/a/dg;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 17242
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 17245
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/dg;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 17246
    const/4 v0, 0x7

    iget-wide v1, p0, Lcom/sec/chaton/a/dg;->i:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 17248
    :cond_6
    return-void
.end method
