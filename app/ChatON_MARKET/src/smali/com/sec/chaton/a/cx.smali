.class public final Lcom/sec/chaton/a/cx;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cz;


# static fields
.field private static final a:Lcom/sec/chaton/a/cx;


# instance fields
.field private b:I

.field private c:J

.field private d:Ljava/lang/Object;

.field private e:J

.field private f:Lcom/sec/chaton/a/ey;

.field private g:Lcom/sec/chaton/a/ej;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10086
    new-instance v0, Lcom/sec/chaton/a/cx;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/cx;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/cx;->a:Lcom/sec/chaton/a/cx;

    .line 10087
    sget-object v0, Lcom/sec/chaton/a/cx;->a:Lcom/sec/chaton/a/cx;

    invoke-direct {v0}, Lcom/sec/chaton/a/cx;->p()V

    .line 10088
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cy;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 9521
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 9614
    iput-byte v0, p0, Lcom/sec/chaton/a/cx;->h:B

    .line 9643
    iput v0, p0, Lcom/sec/chaton/a/cx;->i:I

    .line 9522
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cy;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 9516
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/cx;-><init>(Lcom/sec/chaton/a/cy;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 9523
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 9614
    iput-byte v0, p0, Lcom/sec/chaton/a/cx;->h:B

    .line 9643
    iput v0, p0, Lcom/sec/chaton/a/cx;->i:I

    .line 9523
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cx;I)I
    .locals 0

    .prologue
    .line 9516
    iput p1, p0, Lcom/sec/chaton/a/cx;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cx;J)J
    .locals 0

    .prologue
    .line 9516
    iput-wide p1, p0, Lcom/sec/chaton/a/cx;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/cx;
    .locals 1

    .prologue
    .line 9527
    sget-object v0, Lcom/sec/chaton/a/cx;->a:Lcom/sec/chaton/a/cx;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/cx;
    .locals 1

    .prologue
    .line 9694
    invoke-static {}, Lcom/sec/chaton/a/cx;->newBuilder()Lcom/sec/chaton/a/cy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cy;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cy;

    invoke-static {v0}, Lcom/sec/chaton/a/cy;->a(Lcom/sec/chaton/a/cy;)Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/cx;)Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9750
    invoke-static {}, Lcom/sec/chaton/a/cx;->newBuilder()Lcom/sec/chaton/a/cy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cy;->a(Lcom/sec/chaton/a/cx;)Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/cx;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 9516
    iput-object p1, p0, Lcom/sec/chaton/a/cx;->g:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cx;Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ey;
    .locals 0

    .prologue
    .line 9516
    iput-object p1, p0, Lcom/sec/chaton/a/cx;->f:Lcom/sec/chaton/a/ey;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cx;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 9516
    iput-object p1, p0, Lcom/sec/chaton/a/cx;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cx;J)J
    .locals 0

    .prologue
    .line 9516
    iput-wide p1, p0, Lcom/sec/chaton/a/cx;->e:J

    return-wide p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9747
    invoke-static {}, Lcom/sec/chaton/a/cy;->j()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9566
    iget-object v0, p0, Lcom/sec/chaton/a/cx;->d:Ljava/lang/Object;

    .line 9567
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9568
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9570
    iput-object v0, p0, Lcom/sec/chaton/a/cx;->d:Ljava/lang/Object;

    .line 9573
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 9608
    iput-wide v1, p0, Lcom/sec/chaton/a/cx;->c:J

    .line 9609
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cx;->d:Ljava/lang/Object;

    .line 9610
    iput-wide v1, p0, Lcom/sec/chaton/a/cx;->e:J

    .line 9611
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cx;->f:Lcom/sec/chaton/a/ey;

    .line 9612
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cx;->g:Lcom/sec/chaton/a/ej;

    .line 9613
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/cx;
    .locals 1

    .prologue
    .line 9531
    sget-object v0, Lcom/sec/chaton/a/cx;->a:Lcom/sec/chaton/a/cx;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 9539
    iget v1, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 9542
    iget-wide v0, p0, Lcom/sec/chaton/a/cx;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 9549
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9552
    iget-object v0, p0, Lcom/sec/chaton/a/cx;->d:Ljava/lang/Object;

    .line 9553
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9554
    check-cast v0, Ljava/lang/String;

    .line 9562
    :goto_0
    return-object v0

    .line 9556
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9558
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9559
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9560
    iput-object v1, p0, Lcom/sec/chaton/a/cx;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9562
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 9581
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9516
    invoke-virtual {p0}, Lcom/sec/chaton/a/cx;->b()Lcom/sec/chaton/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 9645
    iget v0, p0, Lcom/sec/chaton/a/cx;->i:I

    .line 9646
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 9670
    :goto_0
    return v0

    .line 9648
    :cond_0
    const/4 v0, 0x0

    .line 9649
    iget v1, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 9650
    iget-wide v1, p0, Lcom/sec/chaton/a/cx;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9653
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 9654
    invoke-direct {p0}, Lcom/sec/chaton/a/cx;->o()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9657
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    .line 9658
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sec/chaton/a/cx;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9661
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 9662
    iget-object v1, p0, Lcom/sec/chaton/a/cx;->f:Lcom/sec/chaton/a/ey;

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9665
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 9666
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/a/cx;->g:Lcom/sec/chaton/a/ej;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9669
    :cond_5
    iput v0, p0, Lcom/sec/chaton/a/cx;->i:I

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 9584
    iget-wide v0, p0, Lcom/sec/chaton/a/cx;->e:J

    return-wide v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 9591
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 9616
    iget-byte v1, p0, Lcom/sec/chaton/a/cx;->h:B

    .line 9617
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 9620
    :goto_0
    return v0

    .line 9617
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 9619
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/cx;->h:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 9594
    iget-object v0, p0, Lcom/sec/chaton/a/cx;->f:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 9601
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 9604
    iget-object v0, p0, Lcom/sec/chaton/a/cx;->g:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public m()Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9748
    invoke-static {}, Lcom/sec/chaton/a/cx;->newBuilder()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/sec/chaton/a/cy;
    .locals 1

    .prologue
    .line 9752
    invoke-static {p0}, Lcom/sec/chaton/a/cx;->a(Lcom/sec/chaton/a/cx;)Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9516
    invoke-virtual {p0}, Lcom/sec/chaton/a/cx;->m()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9516
    invoke-virtual {p0}, Lcom/sec/chaton/a/cx;->n()Lcom/sec/chaton/a/cy;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9677
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 9625
    invoke-virtual {p0}, Lcom/sec/chaton/a/cx;->getSerializedSize()I

    .line 9626
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 9627
    iget-wide v0, p0, Lcom/sec/chaton/a/cx;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 9629
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 9630
    invoke-direct {p0}, Lcom/sec/chaton/a/cx;->o()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9632
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 9633
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sec/chaton/a/cx;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 9635
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 9636
    iget-object v0, p0, Lcom/sec/chaton/a/cx;->f:Lcom/sec/chaton/a/ey;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 9638
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/cx;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 9639
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sec/chaton/a/cx;->g:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 9641
    :cond_4
    return-void
.end method
