.class public final Lcom/sec/chaton/a/en;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/eo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/em;",
        "Lcom/sec/chaton/a/en;",
        ">;",
        "Lcom/sec/chaton/a/eo;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/ev;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/a/ey;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 26161
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 26333
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/en;->c:Ljava/lang/Object;

    .line 26369
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    .line 26458
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    .line 26162
    invoke-direct {p0}, Lcom/sec/chaton/a/en;->i()V

    .line 26163
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/en;)Lcom/sec/chaton/a/em;
    .locals 1

    .prologue
    .line 26156
    invoke-direct {p0}, Lcom/sec/chaton/a/en;->k()Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26156
    invoke-static {}, Lcom/sec/chaton/a/en;->j()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 26166
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26168
    new-instance v0, Lcom/sec/chaton/a/en;

    invoke-direct {v0}, Lcom/sec/chaton/a/en;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/em;
    .locals 2

    .prologue
    .line 26202
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->e()Lcom/sec/chaton/a/em;

    move-result-object v0

    .line 26203
    invoke-virtual {v0}, Lcom/sec/chaton/a/em;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 26204
    invoke-static {v0}, Lcom/sec/chaton/a/en;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 26207
    :cond_0
    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 26372
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 26373
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    .line 26374
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26376
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/en;
    .locals 2

    .prologue
    .line 26172
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 26173
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/en;->b:J

    .line 26174
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26175
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/en;->c:Ljava/lang/Object;

    .line 26176
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26177
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    .line 26178
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26179
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    .line 26180
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26181
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26320
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26321
    iput-wide p1, p0, Lcom/sec/chaton/a/en;->b:J

    .line 26323
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/en;
    .locals 2

    .prologue
    .line 26268
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 26269
    sparse-switch v0, :sswitch_data_0

    .line 26274
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/en;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26276
    :sswitch_0
    return-object p0

    .line 26281
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26282
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/en;->b:J

    goto :goto_0

    .line 26286
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26287
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/en;->c:Ljava/lang/Object;

    goto :goto_0

    .line 26291
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/ev;->newBuilder()Lcom/sec/chaton/a/ew;

    move-result-object v0

    .line 26292
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 26293
    invoke-virtual {v0}, Lcom/sec/chaton/a/ew;->e()Lcom/sec/chaton/a/ev;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/en;->a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/en;

    goto :goto_0

    .line 26297
    :sswitch_4
    invoke-static {}, Lcom/sec/chaton/a/ey;->newBuilder()Lcom/sec/chaton/a/ez;

    move-result-object v0

    .line 26298
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 26299
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->g()Lcom/sec/chaton/a/ey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    .line 26301
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 26302
    invoke-virtual {v0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/en;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/en;

    goto :goto_0

    .line 26269
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/em;)Lcom/sec/chaton/a/en;
    .locals 2

    .prologue
    .line 26236
    invoke-static {}, Lcom/sec/chaton/a/em;->a()Lcom/sec/chaton/a/em;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 26256
    :cond_0
    :goto_0
    return-object p0

    .line 26237
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/em;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26238
    invoke-virtual {p1}, Lcom/sec/chaton/a/em;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/en;->a(J)Lcom/sec/chaton/a/en;

    .line 26240
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/em;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 26241
    invoke-virtual {p1}, Lcom/sec/chaton/a/em;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/en;->a(Ljava/lang/String;)Lcom/sec/chaton/a/en;

    .line 26243
    :cond_3
    invoke-static {p1}, Lcom/sec/chaton/a/em;->b(Lcom/sec/chaton/a/em;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 26244
    iget-object v0, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 26245
    invoke-static {p1}, Lcom/sec/chaton/a/em;->b(Lcom/sec/chaton/a/em;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    .line 26246
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26253
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/em;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26254
    invoke-virtual {p1}, Lcom/sec/chaton/a/em;->i()Lcom/sec/chaton/a/ey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/en;->b(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/en;

    goto :goto_0

    .line 26248
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/a/en;->l()V

    .line 26249
    iget-object v0, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/em;->b(Lcom/sec/chaton/a/em;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26405
    if-nez p1, :cond_0

    .line 26406
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26408
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/en;->l()V

    .line 26409
    iget-object v0, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26411
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26466
    if-nez p1, :cond_0

    .line 26467
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26469
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    .line 26471
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26472
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26348
    if-nez p1, :cond_0

    .line 26349
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26351
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26352
    iput-object p1, p0, Lcom/sec/chaton/a/en;->c:Ljava/lang/Object;

    .line 26354
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/en;
    .locals 2

    .prologue
    .line 26185
    invoke-static {}, Lcom/sec/chaton/a/en;->j()Lcom/sec/chaton/a/en;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->e()Lcom/sec/chaton/a/em;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/en;->a(Lcom/sec/chaton/a/em;)Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/en;
    .locals 2

    .prologue
    .line 26482
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 26484
    iget-object v0, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    invoke-static {v0}, Lcom/sec/chaton/a/ey;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ez;->a(Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ez;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ez;->e()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    .line 26490
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26491
    return-object p0

    .line 26487
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->d()Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->e()Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/em;
    .locals 1

    .prologue
    .line 26189
    invoke-static {}, Lcom/sec/chaton/a/em;->a()Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->a()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->a()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->b()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->b()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->b()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->b()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/em;
    .locals 2

    .prologue
    .line 26193
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->e()Lcom/sec/chaton/a/em;

    move-result-object v0

    .line 26194
    invoke-virtual {v0}, Lcom/sec/chaton/a/em;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 26195
    invoke-static {v0}, Lcom/sec/chaton/a/en;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 26197
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/em;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 26211
    new-instance v2, Lcom/sec/chaton/a/em;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/em;-><init>(Lcom/sec/chaton/a/en;Lcom/sec/chaton/a/b;)V

    .line 26212
    iget v3, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26213
    const/4 v1, 0x0

    .line 26214
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 26217
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/en;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/em;->a(Lcom/sec/chaton/a/em;J)J

    .line 26218
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 26219
    or-int/lit8 v0, v0, 0x2

    .line 26221
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/en;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/em;->a(Lcom/sec/chaton/a/em;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26222
    iget v1, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 26223
    iget-object v1, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    .line 26224
    iget v1, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/sec/chaton/a/en;->a:I

    .line 26226
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/en;->d:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/em;->a(Lcom/sec/chaton/a/em;Ljava/util/List;)Ljava/util/List;

    .line 26227
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 26228
    or-int/lit8 v0, v0, 0x4

    .line 26230
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/em;->a(Lcom/sec/chaton/a/em;Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ey;

    .line 26231
    invoke-static {v2, v0}, Lcom/sec/chaton/a/em;->a(Lcom/sec/chaton/a/em;I)I

    .line 26232
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 26460
    iget v0, p0, Lcom/sec/chaton/a/en;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 26463
    iget-object v0, p0, Lcom/sec/chaton/a/en;->e:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->c()Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0}, Lcom/sec/chaton/a/en;->c()Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 26260
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/en;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    check-cast p1, Lcom/sec/chaton/a/em;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/en;->a(Lcom/sec/chaton/a/em;)Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26156
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/en;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method
