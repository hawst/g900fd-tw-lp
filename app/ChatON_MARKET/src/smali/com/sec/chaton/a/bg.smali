.class public final Lcom/sec/chaton/a/bg;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bi;


# static fields
.field private static final a:Lcom/sec/chaton/a/bg;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/sec/chaton/a/bc;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;

.field private g:Z

.field private h:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14184
    new-instance v0, Lcom/sec/chaton/a/bg;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/bg;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/bg;->a:Lcom/sec/chaton/a/bg;

    .line 14185
    sget-object v0, Lcom/sec/chaton/a/bg;->a:Lcom/sec/chaton/a/bg;

    invoke-direct {v0}, Lcom/sec/chaton/a/bg;->t()V

    .line 14186
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/bh;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13449
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 13608
    iput-byte v0, p0, Lcom/sec/chaton/a/bg;->i:B

    .line 13640
    iput v0, p0, Lcom/sec/chaton/a/bg;->j:I

    .line 13450
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/bh;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 13444
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/bg;-><init>(Lcom/sec/chaton/a/bh;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13451
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 13608
    iput-byte v0, p0, Lcom/sec/chaton/a/bg;->i:B

    .line 13640
    iput v0, p0, Lcom/sec/chaton/a/bg;->j:I

    .line 13451
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bg;I)I
    .locals 0

    .prologue
    .line 13444
    iput p1, p0, Lcom/sec/chaton/a/bg;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bg;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 13444
    iput-object p1, p0, Lcom/sec/chaton/a/bg;->d:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/bg;
    .locals 1

    .prologue
    .line 13455
    sget-object v0, Lcom/sec/chaton/a/bg;->a:Lcom/sec/chaton/a/bg;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/bg;
    .locals 1

    .prologue
    .line 13695
    invoke-static {}, Lcom/sec/chaton/a/bg;->newBuilder()Lcom/sec/chaton/a/bh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bh;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bh;

    invoke-static {v0}, Lcom/sec/chaton/a/bh;->a(Lcom/sec/chaton/a/bh;)Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/bg;)Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13751
    invoke-static {}, Lcom/sec/chaton/a/bg;->newBuilder()Lcom/sec/chaton/a/bh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bh;->a(Lcom/sec/chaton/a/bg;)Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/bg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 13444
    iput-object p1, p0, Lcom/sec/chaton/a/bg;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bg;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 13444
    iput-object p1, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bg;Z)Z
    .locals 0

    .prologue
    .line 13444
    iput-boolean p1, p0, Lcom/sec/chaton/a/bg;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/bg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 13444
    iput-object p1, p0, Lcom/sec/chaton/a/bg;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/bg;)Ljava/util/List;
    .locals 1

    .prologue
    .line 13444
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/a/bg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 13444
    iput-object p1, p0, Lcom/sec/chaton/a/bg;->h:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13748
    invoke-static {}, Lcom/sec/chaton/a/bh;->f()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 13484
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->c:Ljava/lang/Object;

    .line 13485
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13486
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 13488
    iput-object v0, p0, Lcom/sec/chaton/a/bg;->c:Ljava/lang/Object;

    .line 13491
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 13547
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->f:Ljava/lang/Object;

    .line 13548
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13549
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 13551
    iput-object v0, p0, Lcom/sec/chaton/a/bg;->f:Ljava/lang/Object;

    .line 13554
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 13589
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->h:Ljava/lang/Object;

    .line 13590
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13591
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 13593
    iput-object v0, p0, Lcom/sec/chaton/a/bg;->h:Ljava/lang/Object;

    .line 13596
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()V
    .locals 1

    .prologue
    .line 13601
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bg;->c:Ljava/lang/Object;

    .line 13602
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/bg;->d:Lcom/sec/chaton/a/bc;

    .line 13603
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    .line 13604
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bg;->f:Ljava/lang/Object;

    .line 13605
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/bg;->g:Z

    .line 13606
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bg;->h:Ljava/lang/Object;

    .line 13607
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/a/cr;
    .locals 1

    .prologue
    .line 13519
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cr;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/bg;
    .locals 1

    .prologue
    .line 13459
    sget-object v0, Lcom/sec/chaton/a/bg;->a:Lcom/sec/chaton/a/bg;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13467
    iget v1, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13470
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->c:Ljava/lang/Object;

    .line 13471
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13472
    check-cast v0, Ljava/lang/String;

    .line 13480
    :goto_0
    return-object v0

    .line 13474
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 13476
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 13477
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13478
    iput-object v1, p0, Lcom/sec/chaton/a/bg;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 13480
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 13499
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 13502
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->d:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13509
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13444
    invoke-virtual {p0}, Lcom/sec/chaton/a/bg;->b()Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 13642
    iget v2, p0, Lcom/sec/chaton/a/bg;->j:I

    .line 13643
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 13671
    :goto_0
    return v2

    .line 13646
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 13647
    invoke-direct {p0}, Lcom/sec/chaton/a/bg;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v1

    .line 13650
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 13651
    iget-object v2, p0, Lcom/sec/chaton/a/bg;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v0

    .line 13654
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 13655
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 13654
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 13658
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 13659
    invoke-direct {p0}, Lcom/sec/chaton/a/bg;->r()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 13662
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 13663
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sec/chaton/a/bg;->g:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 13666
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 13667
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/bg;->s()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 13670
    :cond_5
    iput v2, p0, Lcom/sec/chaton/a/bg;->j:I

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public h()I
    .locals 1

    .prologue
    .line 13516
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 13530
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 13610
    iget-byte v1, p0, Lcom/sec/chaton/a/bg;->i:B

    .line 13611
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 13614
    :goto_0
    return v0

    .line 13611
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 13613
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/bg;->i:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13533
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->f:Ljava/lang/Object;

    .line 13534
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13535
    check-cast v0, Ljava/lang/String;

    .line 13543
    :goto_0
    return-object v0

    .line 13537
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 13539
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 13540
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13541
    iput-object v1, p0, Lcom/sec/chaton/a/bg;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 13543
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 13562
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 13565
    iget-boolean v0, p0, Lcom/sec/chaton/a/bg;->g:Z

    return v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 13572
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13575
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->h:Ljava/lang/Object;

    .line 13576
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13577
    check-cast v0, Ljava/lang/String;

    .line 13585
    :goto_0
    return-object v0

    .line 13579
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 13581
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 13582
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13583
    iput-object v1, p0, Lcom/sec/chaton/a/bg;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 13585
    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13444
    invoke-virtual {p0}, Lcom/sec/chaton/a/bg;->o()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13749
    invoke-static {}, Lcom/sec/chaton/a/bg;->newBuilder()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13753
    invoke-static {p0}, Lcom/sec/chaton/a/bg;->a(Lcom/sec/chaton/a/bg;)Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13444
    invoke-virtual {p0}, Lcom/sec/chaton/a/bg;->p()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13678
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 13619
    invoke-virtual {p0}, Lcom/sec/chaton/a/bg;->getSerializedSize()I

    .line 13620
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13621
    invoke-direct {p0}, Lcom/sec/chaton/a/bg;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 13623
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 13624
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 13626
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 13627
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/bg;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 13626
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 13629
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 13630
    invoke-direct {p0}, Lcom/sec/chaton/a/bg;->r()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 13632
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 13633
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sec/chaton/a/bg;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 13635
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/bg;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 13636
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/bg;->s()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 13638
    :cond_5
    return-void
.end method
