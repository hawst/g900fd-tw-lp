.class public final Lcom/sec/chaton/a/i;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/k;


# static fields
.field private static final a:Lcom/sec/chaton/a/i;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/bc;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:I

.field private k:J

.field private l:J

.field private m:Z

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11131
    new-instance v0, Lcom/sec/chaton/a/i;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/i;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/i;->a:Lcom/sec/chaton/a/i;

    .line 11132
    sget-object v0, Lcom/sec/chaton/a/i;->a:Lcom/sec/chaton/a/i;

    invoke-direct {v0}, Lcom/sec/chaton/a/i;->F()V

    .line 11133
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/j;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 10145
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 10392
    iput-byte v0, p0, Lcom/sec/chaton/a/i;->n:B

    .line 10439
    iput v0, p0, Lcom/sec/chaton/a/i;->o:I

    .line 10146
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/j;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 10140
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/i;-><init>(Lcom/sec/chaton/a/j;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 10147
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 10392
    iput-byte v0, p0, Lcom/sec/chaton/a/i;->n:B

    .line 10439
    iput v0, p0, Lcom/sec/chaton/a/i;->o:I

    .line 10147
    return-void
.end method

.method private A()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 10200
    iget-object v0, p0, Lcom/sec/chaton/a/i;->e:Ljava/lang/Object;

    .line 10201
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10202
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 10204
    iput-object v0, p0, Lcom/sec/chaton/a/i;->e:Ljava/lang/Object;

    .line 10207
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private B()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 10232
    iget-object v0, p0, Lcom/sec/chaton/a/i;->f:Ljava/lang/Object;

    .line 10233
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10234
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 10236
    iput-object v0, p0, Lcom/sec/chaton/a/i;->f:Ljava/lang/Object;

    .line 10239
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private C()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 10264
    iget-object v0, p0, Lcom/sec/chaton/a/i;->g:Ljava/lang/Object;

    .line 10265
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10266
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 10268
    iput-object v0, p0, Lcom/sec/chaton/a/i;->g:Ljava/lang/Object;

    .line 10271
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private D()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 10296
    iget-object v0, p0, Lcom/sec/chaton/a/i;->h:Ljava/lang/Object;

    .line 10297
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10298
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 10300
    iput-object v0, p0, Lcom/sec/chaton/a/i;->h:Ljava/lang/Object;

    .line 10303
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private E()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 10328
    iget-object v0, p0, Lcom/sec/chaton/a/i;->i:Ljava/lang/Object;

    .line 10329
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10330
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 10332
    iput-object v0, p0, Lcom/sec/chaton/a/i;->i:Ljava/lang/Object;

    .line 10335
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private F()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    .line 10380
    iput-wide v1, p0, Lcom/sec/chaton/a/i;->c:J

    .line 10381
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/i;->d:Lcom/sec/chaton/a/bc;

    .line 10382
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/i;->e:Ljava/lang/Object;

    .line 10383
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/i;->f:Ljava/lang/Object;

    .line 10384
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/i;->g:Ljava/lang/Object;

    .line 10385
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/i;->h:Ljava/lang/Object;

    .line 10386
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/i;->i:Ljava/lang/Object;

    .line 10387
    iput v3, p0, Lcom/sec/chaton/a/i;->j:I

    .line 10388
    iput-wide v1, p0, Lcom/sec/chaton/a/i;->k:J

    .line 10389
    iput-wide v1, p0, Lcom/sec/chaton/a/i;->l:J

    .line 10390
    iput-boolean v3, p0, Lcom/sec/chaton/a/i;->m:Z

    .line 10391
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/i;I)I
    .locals 0

    .prologue
    .line 10140
    iput p1, p0, Lcom/sec/chaton/a/i;->j:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/i;J)J
    .locals 0

    .prologue
    .line 10140
    iput-wide p1, p0, Lcom/sec/chaton/a/i;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/i;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 10140
    iput-object p1, p0, Lcom/sec/chaton/a/i;->d:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/i;
    .locals 1

    .prologue
    .line 10151
    sget-object v0, Lcom/sec/chaton/a/i;->a:Lcom/sec/chaton/a/i;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/i;
    .locals 1

    .prologue
    .line 10514
    invoke-static {}, Lcom/sec/chaton/a/i;->newBuilder()Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/j;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/j;

    invoke-static {v0}, Lcom/sec/chaton/a/j;->a(Lcom/sec/chaton/a/j;)Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/i;)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10570
    invoke-static {}, Lcom/sec/chaton/a/i;->newBuilder()Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/j;->a(Lcom/sec/chaton/a/i;)Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 10140
    iput-object p1, p0, Lcom/sec/chaton/a/i;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/i;Z)Z
    .locals 0

    .prologue
    .line 10140
    iput-boolean p1, p0, Lcom/sec/chaton/a/i;->m:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/i;I)I
    .locals 0

    .prologue
    .line 10140
    iput p1, p0, Lcom/sec/chaton/a/i;->b:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/i;J)J
    .locals 0

    .prologue
    .line 10140
    iput-wide p1, p0, Lcom/sec/chaton/a/i;->k:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 10140
    iput-object p1, p0, Lcom/sec/chaton/a/i;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/i;J)J
    .locals 0

    .prologue
    .line 10140
    iput-wide p1, p0, Lcom/sec/chaton/a/i;->l:J

    return-wide p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 10140
    iput-object p1, p0, Lcom/sec/chaton/a/i;->g:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 10140
    iput-object p1, p0, Lcom/sec/chaton/a/i;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic e(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 10140
    iput-object p1, p0, Lcom/sec/chaton/a/i;->i:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10567
    invoke-static {}, Lcom/sec/chaton/a/j;->f()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/i;
    .locals 1

    .prologue
    .line 10155
    sget-object v0, Lcom/sec/chaton/a/i;->a:Lcom/sec/chaton/a/i;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 10163
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 10166
    iget-wide v0, p0, Lcom/sec/chaton/a/i;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 10173
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 10176
    iget-object v0, p0, Lcom/sec/chaton/a/i;->d:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 10183
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 10140
    invoke-virtual {p0}, Lcom/sec/chaton/a/i;->b()Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 10441
    iget v0, p0, Lcom/sec/chaton/a/i;->o:I

    .line 10442
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 10490
    :goto_0
    return v0

    .line 10444
    :cond_0
    const/4 v0, 0x0

    .line 10445
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 10446
    iget-wide v1, p0, Lcom/sec/chaton/a/i;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10449
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 10450
    iget-object v1, p0, Lcom/sec/chaton/a/i;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v1}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10453
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    .line 10454
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->A()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10457
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_4

    .line 10458
    invoke-direct {p0}, Lcom/sec/chaton/a/i;->B()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10461
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 10462
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->C()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10465
    :cond_5
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 10466
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->D()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10469
    :cond_6
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 10470
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->E()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10473
    :cond_7
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 10474
    iget v1, p0, Lcom/sec/chaton/a/i;->j:I

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10477
    :cond_8
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 10478
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/sec/chaton/a/i;->k:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10481
    :cond_9
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 10482
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/sec/chaton/a/i;->l:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10485
    :cond_a
    iget v1, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 10486
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/sec/chaton/a/i;->m:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10489
    :cond_b
    iput v0, p0, Lcom/sec/chaton/a/i;->o:I

    goto/16 :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10186
    iget-object v0, p0, Lcom/sec/chaton/a/i;->e:Ljava/lang/Object;

    .line 10187
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10188
    check-cast v0, Ljava/lang/String;

    .line 10196
    :goto_0
    return-object v0

    .line 10190
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 10192
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 10193
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10194
    iput-object v1, p0, Lcom/sec/chaton/a/i;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 10196
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 10215
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 10394
    iget-byte v1, p0, Lcom/sec/chaton/a/i;->n:B

    .line 10395
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 10398
    :goto_0
    return v0

    .line 10395
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 10397
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/i;->n:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10218
    iget-object v0, p0, Lcom/sec/chaton/a/i;->f:Ljava/lang/Object;

    .line 10219
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10220
    check-cast v0, Ljava/lang/String;

    .line 10228
    :goto_0
    return-object v0

    .line 10222
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 10224
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 10225
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10226
    iput-object v1, p0, Lcom/sec/chaton/a/i;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 10228
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 10247
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10250
    iget-object v0, p0, Lcom/sec/chaton/a/i;->g:Ljava/lang/Object;

    .line 10251
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10252
    check-cast v0, Ljava/lang/String;

    .line 10260
    :goto_0
    return-object v0

    .line 10254
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 10256
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 10257
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10258
    iput-object v1, p0, Lcom/sec/chaton/a/i;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 10260
    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 10279
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10282
    iget-object v0, p0, Lcom/sec/chaton/a/i;->h:Ljava/lang/Object;

    .line 10283
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10284
    check-cast v0, Ljava/lang/String;

    .line 10292
    :goto_0
    return-object v0

    .line 10286
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 10288
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 10289
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10290
    iput-object v1, p0, Lcom/sec/chaton/a/i;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 10292
    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 10140
    invoke-virtual {p0}, Lcom/sec/chaton/a/i;->y()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 10311
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10314
    iget-object v0, p0, Lcom/sec/chaton/a/i;->i:Ljava/lang/Object;

    .line 10315
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10316
    check-cast v0, Ljava/lang/String;

    .line 10324
    :goto_0
    return-object v0

    .line 10318
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 10320
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 10321
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10322
    iput-object v1, p0, Lcom/sec/chaton/a/i;->i:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 10324
    goto :goto_0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 10343
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 10346
    iget v0, p0, Lcom/sec/chaton/a/i;->j:I

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 10353
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 10356
    iget-wide v0, p0, Lcom/sec/chaton/a/i;->k:J

    return-wide v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 10140
    invoke-virtual {p0}, Lcom/sec/chaton/a/i;->z()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 10363
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 10366
    iget-wide v0, p0, Lcom/sec/chaton/a/i;->l:J

    return-wide v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 10373
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10497
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 10403
    invoke-virtual {p0}, Lcom/sec/chaton/a/i;->getSerializedSize()I

    .line 10404
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 10405
    iget-wide v0, p0, Lcom/sec/chaton/a/i;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 10407
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 10408
    iget-object v0, p0, Lcom/sec/chaton/a/i;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 10410
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 10411
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->A()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 10413
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 10414
    invoke-direct {p0}, Lcom/sec/chaton/a/i;->B()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 10416
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 10417
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->C()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 10419
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 10420
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->D()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 10422
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 10423
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sec/chaton/a/i;->E()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 10425
    :cond_6
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 10426
    iget v0, p0, Lcom/sec/chaton/a/i;->j:I

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 10428
    :cond_7
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 10429
    const/16 v0, 0x9

    iget-wide v1, p0, Lcom/sec/chaton/a/i;->k:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 10431
    :cond_8
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 10432
    const/16 v0, 0xa

    iget-wide v1, p0, Lcom/sec/chaton/a/i;->l:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 10434
    :cond_9
    iget v0, p0, Lcom/sec/chaton/a/i;->b:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 10435
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/sec/chaton/a/i;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 10437
    :cond_a
    return-void
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 10376
    iget-boolean v0, p0, Lcom/sec/chaton/a/i;->m:Z

    return v0
.end method

.method public y()Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10568
    invoke-static {}, Lcom/sec/chaton/a/i;->newBuilder()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public z()Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10572
    invoke-static {p0}, Lcom/sec/chaton/a/i;->a(Lcom/sec/chaton/a/i;)Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method
