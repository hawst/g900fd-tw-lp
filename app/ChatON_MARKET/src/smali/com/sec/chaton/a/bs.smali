.class public final Lcom/sec/chaton/a/bs;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/br;",
        "Lcom/sec/chaton/a/bs;",
        ">;",
        "Lcom/sec/chaton/a/bt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/bp;

.field private d:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 18910
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 19063
    sget-object v0, Lcom/sec/chaton/a/bp;->a:Lcom/sec/chaton/a/bp;

    iput-object v0, p0, Lcom/sec/chaton/a/bs;->c:Lcom/sec/chaton/a/bp;

    .line 19087
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    .line 18911
    invoke-direct {p0}, Lcom/sec/chaton/a/bs;->k()V

    .line 18912
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bs;)Lcom/sec/chaton/a/br;
    .locals 1

    .prologue
    .line 18905
    invoke-direct {p0}, Lcom/sec/chaton/a/bs;->m()Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j()Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 18905
    invoke-static {}, Lcom/sec/chaton/a/bs;->l()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 18915
    return-void
.end method

.method private static l()Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 18917
    new-instance v0, Lcom/sec/chaton/a/bs;

    invoke-direct {v0}, Lcom/sec/chaton/a/bs;-><init>()V

    return-object v0
.end method

.method private m()Lcom/sec/chaton/a/br;
    .locals 2

    .prologue
    .line 18949
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->e()Lcom/sec/chaton/a/br;

    move-result-object v0

    .line 18950
    invoke-virtual {v0}, Lcom/sec/chaton/a/br;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18951
    invoke-static {v0}, Lcom/sec/chaton/a/bs;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 18954
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/bs;
    .locals 2

    .prologue
    .line 18921
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 18922
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bs;->b:J

    .line 18923
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 18924
    sget-object v0, Lcom/sec/chaton/a/bp;->a:Lcom/sec/chaton/a/bp;

    iput-object v0, p0, Lcom/sec/chaton/a/bs;->c:Lcom/sec/chaton/a/bp;

    .line 18925
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 18926
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    .line 18927
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 18928
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 19050
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 19051
    iput-wide p1, p0, Lcom/sec/chaton/a/bs;->b:J

    .line 19053
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bs;
    .locals 2

    .prologue
    .line 19000
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 19001
    sparse-switch v0, :sswitch_data_0

    .line 19006
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/bs;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19008
    :sswitch_0
    return-object p0

    .line 19013
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 19014
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/bs;->b:J

    goto :goto_0

    .line 19018
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 19019
    invoke-static {v0}, Lcom/sec/chaton/a/bp;->a(I)Lcom/sec/chaton/a/bp;

    move-result-object v0

    .line 19020
    if-eqz v0, :cond_0

    .line 19021
    iget v1, p0, Lcom/sec/chaton/a/bs;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 19022
    iput-object v0, p0, Lcom/sec/chaton/a/bs;->c:Lcom/sec/chaton/a/bp;

    goto :goto_0

    .line 19027
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 19028
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19029
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->i()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 19031
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 19032
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bs;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bs;

    goto :goto_0

    .line 19001
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bp;)Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 19071
    if-nez p1, :cond_0

    .line 19072
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19074
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 19075
    iput-object p1, p0, Lcom/sec/chaton/a/bs;->c:Lcom/sec/chaton/a/bp;

    .line 19077
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/br;)Lcom/sec/chaton/a/bs;
    .locals 2

    .prologue
    .line 18978
    invoke-static {}, Lcom/sec/chaton/a/br;->a()Lcom/sec/chaton/a/br;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 18988
    :cond_0
    :goto_0
    return-object p0

    .line 18979
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/br;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 18980
    invoke-virtual {p1}, Lcom/sec/chaton/a/br;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/bs;->a(J)Lcom/sec/chaton/a/bs;

    .line 18982
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/br;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18983
    invoke-virtual {p1}, Lcom/sec/chaton/a/br;->f()Lcom/sec/chaton/a/bp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bs;->a(Lcom/sec/chaton/a/bp;)Lcom/sec/chaton/a/bs;

    .line 18985
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/br;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18986
    invoke-virtual {p1}, Lcom/sec/chaton/a/br;->h()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bs;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bs;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 19095
    if-nez p1, :cond_0

    .line 19096
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19098
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    .line 19100
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 19101
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/bs;
    .locals 2

    .prologue
    .line 18932
    invoke-static {}, Lcom/sec/chaton/a/bs;->l()Lcom/sec/chaton/a/bs;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->e()Lcom/sec/chaton/a/br;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bs;->a(Lcom/sec/chaton/a/br;)Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/bs;
    .locals 2

    .prologue
    .line 19111
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 19113
    iget-object v0, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    .line 19119
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 19120
    return-object p0

    .line 19116
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->d()Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->e()Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/br;
    .locals 1

    .prologue
    .line 18936
    invoke-static {}, Lcom/sec/chaton/a/br;->a()Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->a()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->a()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->b()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->b()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->b()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->b()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/br;
    .locals 2

    .prologue
    .line 18940
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->e()Lcom/sec/chaton/a/br;

    move-result-object v0

    .line 18941
    invoke-virtual {v0}, Lcom/sec/chaton/a/br;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18942
    invoke-static {v0}, Lcom/sec/chaton/a/bs;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 18944
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/br;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 18958
    new-instance v2, Lcom/sec/chaton/a/br;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/br;-><init>(Lcom/sec/chaton/a/bs;Lcom/sec/chaton/a/b;)V

    .line 18959
    iget v3, p0, Lcom/sec/chaton/a/bs;->a:I

    .line 18960
    const/4 v1, 0x0

    .line 18961
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 18964
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/bs;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/br;->a(Lcom/sec/chaton/a/br;J)J

    .line 18965
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 18966
    or-int/lit8 v0, v0, 0x2

    .line 18968
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/bs;->c:Lcom/sec/chaton/a/bp;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/br;->a(Lcom/sec/chaton/a/br;Lcom/sec/chaton/a/bp;)Lcom/sec/chaton/a/bp;

    .line 18969
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 18970
    or-int/lit8 v0, v0, 0x4

    .line 18972
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/br;->a(Lcom/sec/chaton/a/br;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 18973
    invoke-static {v2, v0}, Lcom/sec/chaton/a/br;->a(Lcom/sec/chaton/a/br;I)I

    .line 18974
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 19047
    iget-wide v0, p0, Lcom/sec/chaton/a/bs;->b:J

    return-wide v0
.end method

.method public g()Lcom/sec/chaton/a/bp;
    .locals 1

    .prologue
    .line 19068
    iget-object v0, p0, Lcom/sec/chaton/a/bs;->c:Lcom/sec/chaton/a/bp;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->c()Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0}, Lcom/sec/chaton/a/bs;->c()Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 19089
    iget v0, p0, Lcom/sec/chaton/a/bs;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 19092
    iget-object v0, p0, Lcom/sec/chaton/a/bs;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 18992
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bs;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    check-cast p1, Lcom/sec/chaton/a/br;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/bs;->a(Lcom/sec/chaton/a/br;)Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18905
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bs;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method
