.class public Lcom/sec/chaton/a/a/f;
.super Lcom/sec/common/d/a/a/c;
.source "HttpEntry.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/Throwable;

.field private c:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/chaton/a/a/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/a/a/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/common/d/a/c;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/common/d/a/a/c;-><init>(Lcom/sec/common/d/a/c;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/a/a/f;->c:Ljava/lang/Object;

    .line 27
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/chaton/a/a/f;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/chaton/a/a/f;->c:Ljava/lang/Object;

    .line 33
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/chaton/a/a/f;->b:Ljava/lang/Throwable;

    .line 141
    return-void
.end method

.method public b()Lcom/sec/chaton/j/o;
    .locals 2

    .prologue
    .line 40
    sget-object v0, Lcom/sec/chaton/a/a/g;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->c()Lcom/sec/chaton/a/a/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown http result code"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    .line 51
    :goto_0
    return-object v0

    .line 45
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    goto :goto_0

    .line 48
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/j/o;->d:Lcom/sec/chaton/j/o;

    goto :goto_0

    .line 51
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public c()Lcom/sec/chaton/a/a/i;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->m()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->m()I

    move-result v0

    if-nez v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/a/a/f;->b:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Lcom/sec/chaton/a/a/i;->b:Lcom/sec/chaton/a/a/i;

    .line 76
    :goto_0
    return-object v0

    .line 64
    :cond_0
    sget-object v0, Lcom/sec/chaton/a/a/i;->c:Lcom/sec/chaton/a/a/i;

    goto :goto_0

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->m()I

    move-result v0

    const/16 v1, 0xcc

    if-ne v0, v1, :cond_2

    .line 67
    sget-object v0, Lcom/sec/chaton/a/a/i;->d:Lcom/sec/chaton/a/a/i;

    goto :goto_0

    .line 70
    :cond_2
    sget-object v0, Lcom/sec/chaton/a/a/i;->b:Lcom/sec/chaton/a/a/i;

    goto :goto_0

    .line 73
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/a/a/f;->b:Ljava/lang/Throwable;

    if-eqz v0, :cond_4

    .line 74
    sget-object v0, Lcom/sec/chaton/a/a/i;->b:Lcom/sec/chaton/a/a/i;

    goto :goto_0

    .line 76
    :cond_4
    sget-object v0, Lcom/sec/chaton/a/a/i;->a:Lcom/sec/chaton/a/a/i;

    goto :goto_0
.end method

.method public d()Lcom/sec/chaton/j/h;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->k()Lcom/sec/common/d/a/c;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/j/h;

    return-object v0
.end method

.method public e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->p()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 89
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/h;

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/h;->a()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 100
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 94
    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_1

    .line 97
    sget-object v2, Lcom/sec/chaton/a/a/f;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_1
    move v0, v1

    .line 100
    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 106
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/h;

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/h;->b()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 111
    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_1

    .line 114
    sget-object v2, Lcom/sec/chaton/a/a/f;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    .line 116
    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->j()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->j()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 124
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/sec/chaton/a/a/f;->m()I

    move-result v0

    return v0
.end method

.method public j()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/chaton/a/a/f;->b:Ljava/lang/Throwable;

    return-object v0
.end method
