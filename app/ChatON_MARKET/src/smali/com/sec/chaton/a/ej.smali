.class public final Lcom/sec/chaton/a/ej;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/el;


# static fields
.field private static final a:Lcom/sec/chaton/a/ej;


# instance fields
.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Lcom/sec/chaton/a/fb;

.field private f:Lcom/sec/chaton/a/ea;

.field private g:Lcom/google/protobuf/LazyStringList;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2391
    new-instance v0, Lcom/sec/chaton/a/ej;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ej;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ej;->a:Lcom/sec/chaton/a/ej;

    .line 2392
    sget-object v0, Lcom/sec/chaton/a/ej;->a:Lcom/sec/chaton/a/ej;

    invoke-direct {v0}, Lcom/sec/chaton/a/ej;->o()V

    .line 2393
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ek;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1773
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1870
    iput-byte v0, p0, Lcom/sec/chaton/a/ej;->h:B

    .line 1899
    iput v0, p0, Lcom/sec/chaton/a/ej;->i:I

    .line 1774
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ek;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 1768
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ej;-><init>(Lcom/sec/chaton/a/ek;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1775
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1870
    iput-byte v0, p0, Lcom/sec/chaton/a/ej;->h:B

    .line 1899
    iput v0, p0, Lcom/sec/chaton/a/ej;->i:I

    .line 1775
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ej;I)I
    .locals 0

    .prologue
    .line 1768
    iput p1, p0, Lcom/sec/chaton/a/ej;->c:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ej;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 1768
    iput-object p1, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ej;Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/ea;
    .locals 0

    .prologue
    .line 1768
    iput-object p1, p0, Lcom/sec/chaton/a/ej;->f:Lcom/sec/chaton/a/ea;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 1779
    sget-object v0, Lcom/sec/chaton/a/ej;->a:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2011
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ej;Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fb;
    .locals 0

    .prologue
    .line 1768
    iput-object p1, p0, Lcom/sec/chaton/a/ej;->e:Lcom/sec/chaton/a/fb;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ej;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1768
    iput-object p1, p0, Lcom/sec/chaton/a/ej;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ej;I)I
    .locals 0

    .prologue
    .line 1768
    iput p1, p0, Lcom/sec/chaton/a/ej;->b:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ej;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 1768
    iget-object v0, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method private n()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1818
    iget-object v0, p0, Lcom/sec/chaton/a/ej;->d:Ljava/lang/Object;

    .line 1819
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1820
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1822
    iput-object v0, p0, Lcom/sec/chaton/a/ej;->d:Ljava/lang/Object;

    .line 1825
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2008
    invoke-static {}, Lcom/sec/chaton/a/ek;->j()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 1864
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/ej;->c:I

    .line 1865
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ej;->d:Ljava/lang/Object;

    .line 1866
    invoke-static {}, Lcom/sec/chaton/a/fb;->a()Lcom/sec/chaton/a/fb;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ej;->e:Lcom/sec/chaton/a/fb;

    .line 1867
    invoke-static {}, Lcom/sec/chaton/a/ea;->a()Lcom/sec/chaton/a/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ej;->f:Lcom/sec/chaton/a/ea;

    .line 1868
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    .line 1869
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 1783
    sget-object v0, Lcom/sec/chaton/a/ej;->a:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1791
    iget v1, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1794
    iget v0, p0, Lcom/sec/chaton/a/ej;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 1801
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1804
    iget-object v0, p0, Lcom/sec/chaton/a/ej;->d:Ljava/lang/Object;

    .line 1805
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1806
    check-cast v0, Ljava/lang/String;

    .line 1814
    :goto_0
    return-object v0

    .line 1808
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1810
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1811
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1812
    iput-object v1, p0, Lcom/sec/chaton/a/ej;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1814
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 1833
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1768
    invoke-virtual {p0}, Lcom/sec/chaton/a/ej;->b()Lcom/sec/chaton/a/ej;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1901
    iget v0, p0, Lcom/sec/chaton/a/ej;->i:I

    .line 1902
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1931
    :goto_0
    return v0

    .line 1905
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 1906
    iget v0, p0, Lcom/sec/chaton/a/ej;->c:I

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 1909
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1910
    invoke-direct {p0}, Lcom/sec/chaton/a/ej;->n()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1913
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 1914
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/chaton/a/ej;->e:Lcom/sec/chaton/a/fb;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1917
    :cond_2
    iget v2, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 1918
    iget-object v2, p0, Lcom/sec/chaton/a/ej;->f:Lcom/sec/chaton/a/ea;

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    .line 1923
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 1924
    iget-object v3, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1923
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1927
    :cond_4
    add-int/2addr v0, v2

    .line 1928
    invoke-virtual {p0}, Lcom/sec/chaton/a/ej;->k()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1930
    iput v0, p0, Lcom/sec/chaton/a/ej;->i:I

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public h()Lcom/sec/chaton/a/fb;
    .locals 1

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/sec/chaton/a/ej;->e:Lcom/sec/chaton/a/fb;

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 1843
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1872
    iget-byte v1, p0, Lcom/sec/chaton/a/ej;->h:B

    .line 1873
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 1876
    :goto_0
    return v0

    .line 1873
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1875
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ej;->h:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/ea;
    .locals 1

    .prologue
    .line 1846
    iget-object v0, p0, Lcom/sec/chaton/a/ej;->f:Lcom/sec/chaton/a/ea;

    return-object v0
.end method

.method public k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1854
    iget-object v0, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public l()Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2009
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2013
    invoke-static {p0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1768
    invoke-virtual {p0}, Lcom/sec/chaton/a/ej;->l()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1768
    invoke-virtual {p0}, Lcom/sec/chaton/a/ej;->m()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1938
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1881
    invoke-virtual {p0}, Lcom/sec/chaton/a/ej;->getSerializedSize()I

    .line 1882
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1883
    iget v0, p0, Lcom/sec/chaton/a/ej;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1885
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1886
    invoke-direct {p0}, Lcom/sec/chaton/a/ej;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1888
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1889
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/ej;->e:Lcom/sec/chaton/a/fb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 1891
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/ej;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1892
    iget-object v0, p0, Lcom/sec/chaton/a/ej;->f:Lcom/sec/chaton/a/ea;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 1894
    :cond_3
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1895
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/a/ej;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1894
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1897
    :cond_4
    return-void
.end method
