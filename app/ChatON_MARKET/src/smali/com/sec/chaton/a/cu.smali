.class public final Lcom/sec/chaton/a/cu;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cw;


# static fields
.field private static final a:Lcom/sec/chaton/a/cu;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:J

.field private f:J

.field private g:Lcom/sec/chaton/a/dp;

.field private h:Lcom/sec/chaton/a/bc;

.field private i:Ljava/lang/Object;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4416
    new-instance v0, Lcom/sec/chaton/a/cu;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/cu;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/cu;->a:Lcom/sec/chaton/a/cu;

    .line 4417
    sget-object v0, Lcom/sec/chaton/a/cu;->a:Lcom/sec/chaton/a/cu;

    invoke-direct {v0}, Lcom/sec/chaton/a/cu;->v()V

    .line 4418
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cv;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3709
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3868
    iput-byte v0, p0, Lcom/sec/chaton/a/cu;->j:B

    .line 3903
    iput v0, p0, Lcom/sec/chaton/a/cu;->k:I

    .line 3710
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cv;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 3704
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/cu;-><init>(Lcom/sec/chaton/a/cv;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3711
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3868
    iput-byte v0, p0, Lcom/sec/chaton/a/cu;->j:B

    .line 3903
    iput v0, p0, Lcom/sec/chaton/a/cu;->k:I

    .line 3711
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cu;I)I
    .locals 0

    .prologue
    .line 3704
    iput p1, p0, Lcom/sec/chaton/a/cu;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cu;J)J
    .locals 0

    .prologue
    .line 3704
    iput-wide p1, p0, Lcom/sec/chaton/a/cu;->e:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cu;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 3704
    iput-object p1, p0, Lcom/sec/chaton/a/cu;->h:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/cu;
    .locals 1

    .prologue
    .line 3715
    sget-object v0, Lcom/sec/chaton/a/cu;->a:Lcom/sec/chaton/a/cu;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4018
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/cu;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;
    .locals 0

    .prologue
    .line 3704
    iput-object p1, p0, Lcom/sec/chaton/a/cu;->g:Lcom/sec/chaton/a/dp;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cu;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 3704
    iput-object p1, p0, Lcom/sec/chaton/a/cu;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cu;J)J
    .locals 0

    .prologue
    .line 3704
    iput-wide p1, p0, Lcom/sec/chaton/a/cu;->f:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cu;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 3704
    iput-object p1, p0, Lcom/sec/chaton/a/cu;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/cu;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 3704
    iput-object p1, p0, Lcom/sec/chaton/a/cu;->i:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4015
    invoke-static {}, Lcom/sec/chaton/a/cv;->f()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3744
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->c:Ljava/lang/Object;

    .line 3745
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3746
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3748
    iput-object v0, p0, Lcom/sec/chaton/a/cu;->c:Ljava/lang/Object;

    .line 3751
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3776
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->d:Ljava/lang/Object;

    .line 3777
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3778
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3780
    iput-object v0, p0, Lcom/sec/chaton/a/cu;->d:Ljava/lang/Object;

    .line 3783
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private u()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3848
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->i:Ljava/lang/Object;

    .line 3849
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3850
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3852
    iput-object v0, p0, Lcom/sec/chaton/a/cu;->i:Ljava/lang/Object;

    .line 3855
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private v()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 3860
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cu;->c:Ljava/lang/Object;

    .line 3861
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cu;->d:Ljava/lang/Object;

    .line 3862
    iput-wide v1, p0, Lcom/sec/chaton/a/cu;->e:J

    .line 3863
    iput-wide v1, p0, Lcom/sec/chaton/a/cu;->f:J

    .line 3864
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/cu;->g:Lcom/sec/chaton/a/dp;

    .line 3865
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/cu;->h:Lcom/sec/chaton/a/bc;

    .line 3866
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cu;->i:Ljava/lang/Object;

    .line 3867
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/cu;
    .locals 1

    .prologue
    .line 3719
    sget-object v0, Lcom/sec/chaton/a/cu;->a:Lcom/sec/chaton/a/cu;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3727
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3730
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->c:Ljava/lang/Object;

    .line 3731
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3732
    check-cast v0, Ljava/lang/String;

    .line 3740
    :goto_0
    return-object v0

    .line 3734
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3736
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3737
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3738
    iput-object v1, p0, Lcom/sec/chaton/a/cu;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3740
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 3759
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3762
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->d:Ljava/lang/Object;

    .line 3763
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3764
    check-cast v0, Ljava/lang/String;

    .line 3772
    :goto_0
    return-object v0

    .line 3766
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3768
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3769
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3770
    iput-object v1, p0, Lcom/sec/chaton/a/cu;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3772
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 3791
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3704
    invoke-virtual {p0}, Lcom/sec/chaton/a/cu;->b()Lcom/sec/chaton/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3905
    iget v0, p0, Lcom/sec/chaton/a/cu;->k:I

    .line 3906
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3938
    :goto_0
    return v0

    .line 3908
    :cond_0
    const/4 v0, 0x0

    .line 3909
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 3910
    invoke-direct {p0}, Lcom/sec/chaton/a/cu;->s()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3913
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 3914
    invoke-direct {p0}, Lcom/sec/chaton/a/cu;->t()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3917
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 3918
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sec/chaton/a/cu;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3921
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 3922
    iget-wide v1, p0, Lcom/sec/chaton/a/cu;->f:J

    invoke-static {v4, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3925
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 3926
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/a/cu;->g:Lcom/sec/chaton/a/dp;

    invoke-virtual {v2}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3929
    :cond_5
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 3930
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/chaton/a/cu;->h:Lcom/sec/chaton/a/bc;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3933
    :cond_6
    iget v1, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 3934
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sec/chaton/a/cu;->u()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3937
    :cond_7
    iput v0, p0, Lcom/sec/chaton/a/cu;->k:I

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 3794
    iget-wide v0, p0, Lcom/sec/chaton/a/cu;->e:J

    return-wide v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 3801
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3870
    iget-byte v1, p0, Lcom/sec/chaton/a/cu;->j:B

    .line 3871
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 3874
    :goto_0
    return v0

    .line 3871
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3873
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/cu;->j:B

    goto :goto_0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 3804
    iget-wide v0, p0, Lcom/sec/chaton/a/cu;->f:J

    return-wide v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 3811
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 3814
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->g:Lcom/sec/chaton/a/dp;

    return-object v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 3821
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 3824
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->h:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3704
    invoke-virtual {p0}, Lcom/sec/chaton/a/cu;->q()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 3831
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3834
    iget-object v0, p0, Lcom/sec/chaton/a/cu;->i:Ljava/lang/Object;

    .line 3835
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3836
    check-cast v0, Ljava/lang/String;

    .line 3844
    :goto_0
    return-object v0

    .line 3838
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3840
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3841
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3842
    iput-object v1, p0, Lcom/sec/chaton/a/cu;->i:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3844
    goto :goto_0
.end method

.method public q()Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4016
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public r()Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4020
    invoke-static {p0}, Lcom/sec/chaton/a/cu;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3704
    invoke-virtual {p0}, Lcom/sec/chaton/a/cu;->r()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3945
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3879
    invoke-virtual {p0}, Lcom/sec/chaton/a/cu;->getSerializedSize()I

    .line 3880
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3881
    invoke-direct {p0}, Lcom/sec/chaton/a/cu;->s()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3883
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3884
    invoke-direct {p0}, Lcom/sec/chaton/a/cu;->t()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3886
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 3887
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sec/chaton/a/cu;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 3889
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 3890
    iget-wide v0, p0, Lcom/sec/chaton/a/cu;->f:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 3892
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 3893
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sec/chaton/a/cu;->g:Lcom/sec/chaton/a/dp;

    invoke-virtual {v1}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 3895
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 3896
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/sec/chaton/a/cu;->h:Lcom/sec/chaton/a/bc;

    invoke-virtual {v1}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 3898
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/cu;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 3899
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sec/chaton/a/cu;->u()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3901
    :cond_6
    return-void
.end method
