.class public final Lcom/sec/chaton/a/ab;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ac;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/aa;",
        "Lcom/sec/chaton/a/ab;",
        ">;",
        "Lcom/sec/chaton/a/ac;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 24678
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 24813
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    .line 24679
    invoke-direct {p0}, Lcom/sec/chaton/a/ab;->i()V

    .line 24680
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ab;)Lcom/sec/chaton/a/aa;
    .locals 1

    .prologue
    .line 24673
    invoke-direct {p0}, Lcom/sec/chaton/a/ab;->k()Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24673
    invoke-static {}, Lcom/sec/chaton/a/ab;->j()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 24683
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24685
    new-instance v0, Lcom/sec/chaton/a/ab;

    invoke-direct {v0}, Lcom/sec/chaton/a/ab;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/aa;
    .locals 2

    .prologue
    .line 24715
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->e()Lcom/sec/chaton/a/aa;

    move-result-object v0

    .line 24716
    invoke-virtual {v0}, Lcom/sec/chaton/a/aa;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 24717
    invoke-static {v0}, Lcom/sec/chaton/a/ab;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 24720
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ab;
    .locals 2

    .prologue
    .line 24689
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 24690
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ab;->b:J

    .line 24691
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ab;->a:I

    .line 24692
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    .line 24693
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ab;->a:I

    .line 24694
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24800
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ab;->a:I

    .line 24801
    iput-wide p1, p0, Lcom/sec/chaton/a/ab;->b:J

    .line 24803
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ab;
    .locals 2

    .prologue
    .line 24759
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 24760
    sparse-switch v0, :sswitch_data_0

    .line 24765
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ab;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24767
    :sswitch_0
    return-object p0

    .line 24772
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ab;->a:I

    .line 24773
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ab;->b:J

    goto :goto_0

    .line 24777
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 24778
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 24779
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 24781
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 24782
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ab;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ab;

    goto :goto_0

    .line 24760
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/aa;)Lcom/sec/chaton/a/ab;
    .locals 2

    .prologue
    .line 24740
    invoke-static {}, Lcom/sec/chaton/a/aa;->a()Lcom/sec/chaton/a/aa;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 24747
    :cond_0
    :goto_0
    return-object p0

    .line 24741
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/aa;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24742
    invoke-virtual {p1}, Lcom/sec/chaton/a/aa;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ab;->a(J)Lcom/sec/chaton/a/ab;

    .line 24744
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/aa;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24745
    invoke-virtual {p1}, Lcom/sec/chaton/a/aa;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ab;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ab;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ab;
    .locals 1

    .prologue
    .line 24821
    if-nez p1, :cond_0

    .line 24822
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24824
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    .line 24826
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ab;->a:I

    .line 24827
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ab;
    .locals 2

    .prologue
    .line 24698
    invoke-static {}, Lcom/sec/chaton/a/ab;->j()Lcom/sec/chaton/a/ab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->e()Lcom/sec/chaton/a/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ab;->a(Lcom/sec/chaton/a/aa;)Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ab;
    .locals 2

    .prologue
    .line 24837
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 24839
    iget-object v0, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    .line 24845
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ab;->a:I

    .line 24846
    return-object p0

    .line 24842
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->d()Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->e()Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/aa;
    .locals 1

    .prologue
    .line 24702
    invoke-static {}, Lcom/sec/chaton/a/aa;->a()Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->a()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->a()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->b()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->b()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->b()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->b()Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/aa;
    .locals 2

    .prologue
    .line 24706
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->e()Lcom/sec/chaton/a/aa;

    move-result-object v0

    .line 24707
    invoke-virtual {v0}, Lcom/sec/chaton/a/aa;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 24708
    invoke-static {v0}, Lcom/sec/chaton/a/ab;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 24710
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/aa;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 24724
    new-instance v2, Lcom/sec/chaton/a/aa;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/aa;-><init>(Lcom/sec/chaton/a/ab;Lcom/sec/chaton/a/b;)V

    .line 24725
    iget v3, p0, Lcom/sec/chaton/a/ab;->a:I

    .line 24726
    const/4 v1, 0x0

    .line 24727
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 24730
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/ab;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/aa;->a(Lcom/sec/chaton/a/aa;J)J

    .line 24731
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 24732
    or-int/lit8 v0, v0, 0x2

    .line 24734
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/aa;->a(Lcom/sec/chaton/a/aa;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 24735
    invoke-static {v2, v0}, Lcom/sec/chaton/a/aa;->a(Lcom/sec/chaton/a/aa;I)I

    .line 24736
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 24815
    iget v0, p0, Lcom/sec/chaton/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 24818
    iget-object v0, p0, Lcom/sec/chaton/a/ab;->c:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->c()Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0}, Lcom/sec/chaton/a/ab;->c()Lcom/sec/chaton/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 24751
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ab;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    check-cast p1, Lcom/sec/chaton/a/aa;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ab;->a(Lcom/sec/chaton/a/aa;)Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24673
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ab;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ab;

    move-result-object v0

    return-object v0
.end method
