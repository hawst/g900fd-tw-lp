.class public final Lcom/sec/chaton/a/bl;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bn;


# static fields
.field private static final a:Lcom/sec/chaton/a/bl;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:J

.field private f:Lcom/sec/chaton/a/dp;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5335
    new-instance v0, Lcom/sec/chaton/a/bl;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/bl;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/bl;->a:Lcom/sec/chaton/a/bl;

    .line 5336
    sget-object v0, Lcom/sec/chaton/a/bl;->a:Lcom/sec/chaton/a/bl;

    invoke-direct {v0}, Lcom/sec/chaton/a/bl;->o()V

    .line 5337
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/bm;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4831
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 4935
    iput-byte v0, p0, Lcom/sec/chaton/a/bl;->g:B

    .line 4961
    iput v0, p0, Lcom/sec/chaton/a/bl;->h:I

    .line 4832
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/bm;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 4826
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/bl;-><init>(Lcom/sec/chaton/a/bm;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4833
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4935
    iput-byte v0, p0, Lcom/sec/chaton/a/bl;->g:B

    .line 4961
    iput v0, p0, Lcom/sec/chaton/a/bl;->h:I

    .line 4833
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bl;I)I
    .locals 0

    .prologue
    .line 4826
    iput p1, p0, Lcom/sec/chaton/a/bl;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bl;J)J
    .locals 0

    .prologue
    .line 4826
    iput-wide p1, p0, Lcom/sec/chaton/a/bl;->e:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/bl;
    .locals 1

    .prologue
    .line 4837
    sget-object v0, Lcom/sec/chaton/a/bl;->a:Lcom/sec/chaton/a/bl;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5064
    invoke-static {}, Lcom/sec/chaton/a/bl;->newBuilder()Lcom/sec/chaton/a/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/bl;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;
    .locals 0

    .prologue
    .line 4826
    iput-object p1, p0, Lcom/sec/chaton/a/bl;->f:Lcom/sec/chaton/a/dp;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bl;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 4826
    iput-object p1, p0, Lcom/sec/chaton/a/bl;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/bl;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 4826
    iput-object p1, p0, Lcom/sec/chaton/a/bl;->d:Ljava/lang/Object;

    return-object p1
.end method

.method private m()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 4866
    iget-object v0, p0, Lcom/sec/chaton/a/bl;->c:Ljava/lang/Object;

    .line 4867
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4868
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 4870
    iput-object v0, p0, Lcom/sec/chaton/a/bl;->c:Ljava/lang/Object;

    .line 4873
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private n()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 4898
    iget-object v0, p0, Lcom/sec/chaton/a/bl;->d:Ljava/lang/Object;

    .line 4899
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4900
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 4902
    iput-object v0, p0, Lcom/sec/chaton/a/bl;->d:Ljava/lang/Object;

    .line 4905
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5061
    invoke-static {}, Lcom/sec/chaton/a/bm;->f()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 4930
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bl;->c:Ljava/lang/Object;

    .line 4931
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bl;->d:Ljava/lang/Object;

    .line 4932
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bl;->e:J

    .line 4933
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/bl;->f:Lcom/sec/chaton/a/dp;

    .line 4934
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/bl;
    .locals 1

    .prologue
    .line 4841
    sget-object v0, Lcom/sec/chaton/a/bl;->a:Lcom/sec/chaton/a/bl;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4849
    iget v1, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4852
    iget-object v0, p0, Lcom/sec/chaton/a/bl;->c:Ljava/lang/Object;

    .line 4853
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4854
    check-cast v0, Ljava/lang/String;

    .line 4862
    :goto_0
    return-object v0

    .line 4856
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 4858
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 4859
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4860
    iput-object v1, p0, Lcom/sec/chaton/a/bl;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 4862
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 4881
    iget v0, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4884
    iget-object v0, p0, Lcom/sec/chaton/a/bl;->d:Ljava/lang/Object;

    .line 4885
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4886
    check-cast v0, Ljava/lang/String;

    .line 4894
    :goto_0
    return-object v0

    .line 4888
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 4890
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 4891
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4892
    iput-object v1, p0, Lcom/sec/chaton/a/bl;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 4894
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 4913
    iget v0, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4826
    invoke-virtual {p0}, Lcom/sec/chaton/a/bl;->b()Lcom/sec/chaton/a/bl;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4963
    iget v0, p0, Lcom/sec/chaton/a/bl;->h:I

    .line 4964
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4984
    :goto_0
    return v0

    .line 4966
    :cond_0
    const/4 v0, 0x0

    .line 4967
    iget v1, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 4968
    invoke-direct {p0}, Lcom/sec/chaton/a/bl;->m()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4971
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 4972
    invoke-direct {p0}, Lcom/sec/chaton/a/bl;->n()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4975
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 4976
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sec/chaton/a/bl;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4979
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 4980
    iget-object v1, p0, Lcom/sec/chaton/a/bl;->f:Lcom/sec/chaton/a/dp;

    invoke-virtual {v1}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4983
    :cond_4
    iput v0, p0, Lcom/sec/chaton/a/bl;->h:I

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 4916
    iget-wide v0, p0, Lcom/sec/chaton/a/bl;->e:J

    return-wide v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 4923
    iget v0, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 4937
    iget-byte v1, p0, Lcom/sec/chaton/a/bl;->g:B

    .line 4938
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 4941
    :goto_0
    return v0

    .line 4938
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4940
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/bl;->g:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 4926
    iget-object v0, p0, Lcom/sec/chaton/a/bl;->f:Lcom/sec/chaton/a/dp;

    return-object v0
.end method

.method public k()Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5062
    invoke-static {}, Lcom/sec/chaton/a/bl;->newBuilder()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5066
    invoke-static {p0}, Lcom/sec/chaton/a/bl;->a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4826
    invoke-virtual {p0}, Lcom/sec/chaton/a/bl;->k()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4826
    invoke-virtual {p0}, Lcom/sec/chaton/a/bl;->l()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4991
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4946
    invoke-virtual {p0}, Lcom/sec/chaton/a/bl;->getSerializedSize()I

    .line 4947
    iget v0, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4948
    invoke-direct {p0}, Lcom/sec/chaton/a/bl;->m()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 4950
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4951
    invoke-direct {p0}, Lcom/sec/chaton/a/bl;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 4953
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 4954
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sec/chaton/a/bl;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 4956
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/bl;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 4957
    iget-object v0, p0, Lcom/sec/chaton/a/bl;->f:Lcom/sec/chaton/a/dp;

    invoke-virtual {v0}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 4959
    :cond_3
    return-void
.end method
