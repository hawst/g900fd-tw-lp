.class public final Lcom/sec/chaton/a/dm;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/do;


# static fields
.field private static final a:Lcom/sec/chaton/a/dm;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 732
    new-instance v0, Lcom/sec/chaton/a/dm;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/dm;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/dm;->a:Lcom/sec/chaton/a/dm;

    .line 733
    sget-object v0, Lcom/sec/chaton/a/dm;->a:Lcom/sec/chaton/a/dm;

    invoke-direct {v0}, Lcom/sec/chaton/a/dm;->k()V

    .line 734
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/dn;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 341
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 423
    iput-byte v0, p0, Lcom/sec/chaton/a/dm;->e:B

    .line 443
    iput v0, p0, Lcom/sec/chaton/a/dm;->f:I

    .line 342
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/dn;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/dm;-><init>(Lcom/sec/chaton/a/dn;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 343
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 423
    iput-byte v0, p0, Lcom/sec/chaton/a/dm;->e:B

    .line 443
    iput v0, p0, Lcom/sec/chaton/a/dm;->f:I

    .line 343
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dm;I)I
    .locals 0

    .prologue
    .line 336
    iput p1, p0, Lcom/sec/chaton/a/dm;->b:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/dm;
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/sec/chaton/a/dm;->a:Lcom/sec/chaton/a/dm;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 538
    invoke-static {}, Lcom/sec/chaton/a/dm;->newBuilder()Lcom/sec/chaton/a/dn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dn;->a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/dm;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/sec/chaton/a/dm;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/dm;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/sec/chaton/a/dm;->d:Ljava/lang/Object;

    return-object p1
.end method

.method private i()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/chaton/a/dm;->c:Ljava/lang/Object;

    .line 377
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 378
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 380
    iput-object v0, p0, Lcom/sec/chaton/a/dm;->c:Ljava/lang/Object;

    .line 383
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private j()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 408
    iget-object v0, p0, Lcom/sec/chaton/a/dm;->d:Ljava/lang/Object;

    .line 409
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 410
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 412
    iput-object v0, p0, Lcom/sec/chaton/a/dm;->d:Ljava/lang/Object;

    .line 415
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 420
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dm;->c:Ljava/lang/Object;

    .line 421
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dm;->d:Ljava/lang/Object;

    .line 422
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 535
    invoke-static {}, Lcom/sec/chaton/a/dn;->f()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/dm;
    .locals 1

    .prologue
    .line 351
    sget-object v0, Lcom/sec/chaton/a/dm;->a:Lcom/sec/chaton/a/dm;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 359
    iget v1, p0, Lcom/sec/chaton/a/dm;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/chaton/a/dm;->c:Ljava/lang/Object;

    .line 363
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 364
    check-cast v0, Ljava/lang/String;

    .line 372
    :goto_0
    return-object v0

    .line 366
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 368
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 369
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    iput-object v1, p0, Lcom/sec/chaton/a/dm;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 372
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 391
    iget v0, p0, Lcom/sec/chaton/a/dm;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/chaton/a/dm;->d:Ljava/lang/Object;

    .line 395
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 396
    check-cast v0, Ljava/lang/String;

    .line 404
    :goto_0
    return-object v0

    .line 398
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 400
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 401
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    iput-object v1, p0, Lcom/sec/chaton/a/dm;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 404
    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 536
    invoke-static {}, Lcom/sec/chaton/a/dm;->newBuilder()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/a/dm;->b()Lcom/sec/chaton/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 445
    iget v0, p0, Lcom/sec/chaton/a/dm;->f:I

    .line 446
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 458
    :goto_0
    return v0

    .line 448
    :cond_0
    const/4 v0, 0x0

    .line 449
    iget v1, p0, Lcom/sec/chaton/a/dm;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 450
    invoke-direct {p0}, Lcom/sec/chaton/a/dm;->i()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/dm;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 454
    invoke-direct {p0}, Lcom/sec/chaton/a/dm;->j()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/dm;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 540
    invoke-static {p0}, Lcom/sec/chaton/a/dm;->a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 425
    iget-byte v1, p0, Lcom/sec/chaton/a/dm;->e:B

    .line 426
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 429
    :goto_0
    return v0

    .line 426
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 428
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/dm;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/a/dm;->g()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/a/dm;->h()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 465
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 434
    invoke-virtual {p0}, Lcom/sec/chaton/a/dm;->getSerializedSize()I

    .line 435
    iget v0, p0, Lcom/sec/chaton/a/dm;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 436
    invoke-direct {p0}, Lcom/sec/chaton/a/dm;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 438
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dm;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 439
    invoke-direct {p0}, Lcom/sec/chaton/a/dm;->j()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 441
    :cond_1
    return-void
.end method
