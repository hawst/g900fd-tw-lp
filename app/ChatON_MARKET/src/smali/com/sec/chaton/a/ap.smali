.class public final Lcom/sec/chaton/a/ap;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ar;


# static fields
.field private static final a:Lcom/sec/chaton/a/ap;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/bc;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/protobuf/LazyStringList;

.field private h:Lcom/sec/chaton/a/as;

.field private i:Lcom/google/protobuf/LazyStringList;

.field private j:J

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28250
    new-instance v0, Lcom/sec/chaton/a/ap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ap;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ap;->a:Lcom/sec/chaton/a/ap;

    .line 28251
    sget-object v0, Lcom/sec/chaton/a/ap;->a:Lcom/sec/chaton/a/ap;

    invoke-direct {v0}, Lcom/sec/chaton/a/ap;->u()V

    .line 28252
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/aq;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 27421
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 27577
    iput-byte v0, p0, Lcom/sec/chaton/a/ap;->k:B

    .line 27615
    iput v0, p0, Lcom/sec/chaton/a/ap;->l:I

    .line 27422
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/aq;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 27416
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ap;-><init>(Lcom/sec/chaton/a/aq;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 27423
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 27577
    iput-byte v0, p0, Lcom/sec/chaton/a/ap;->k:B

    .line 27615
    iput v0, p0, Lcom/sec/chaton/a/ap;->l:I

    .line 27423
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ap;I)I
    .locals 0

    .prologue
    .line 27416
    iput p1, p0, Lcom/sec/chaton/a/ap;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ap;J)J
    .locals 0

    .prologue
    .line 27416
    iput-wide p1, p0, Lcom/sec/chaton/a/ap;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ap;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 27416
    iput-object p1, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/ap;
    .locals 1

    .prologue
    .line 27427
    sget-object v0, Lcom/sec/chaton/a/ap;->a:Lcom/sec/chaton/a/ap;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/ap;
    .locals 1

    .prologue
    .line 27688
    invoke-static {}, Lcom/sec/chaton/a/ap;->newBuilder()Lcom/sec/chaton/a/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/aq;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/aq;

    invoke-static {v0}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/aq;)Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ap;)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 27744
    invoke-static {}, Lcom/sec/chaton/a/ap;->newBuilder()Lcom/sec/chaton/a/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/ap;)Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ap;Lcom/sec/chaton/a/as;)Lcom/sec/chaton/a/as;
    .locals 0

    .prologue
    .line 27416
    iput-object p1, p0, Lcom/sec/chaton/a/ap;->h:Lcom/sec/chaton/a/as;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ap;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 27416
    iput-object p1, p0, Lcom/sec/chaton/a/ap;->d:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ap;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 27416
    iput-object p1, p0, Lcom/sec/chaton/a/ap;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ap;J)J
    .locals 0

    .prologue
    .line 27416
    iput-wide p1, p0, Lcom/sec/chaton/a/ap;->j:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 27416
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/ap;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 27416
    iput-object p1, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ap;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 27416
    iput-object p1, p0, Lcom/sec/chaton/a/ap;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 27416
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 27741
    invoke-static {}, Lcom/sec/chaton/a/aq;->p()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 27476
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->e:Ljava/lang/Object;

    .line 27477
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 27478
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 27480
    iput-object v0, p0, Lcom/sec/chaton/a/ap;->e:Ljava/lang/Object;

    .line 27483
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 27508
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->f:Ljava/lang/Object;

    .line 27509
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 27510
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 27512
    iput-object v0, p0, Lcom/sec/chaton/a/ap;->f:Ljava/lang/Object;

    .line 27515
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private u()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 27568
    iput-wide v1, p0, Lcom/sec/chaton/a/ap;->c:J

    .line 27569
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/ap;->d:Lcom/sec/chaton/a/bc;

    .line 27570
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ap;->e:Ljava/lang/Object;

    .line 27571
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ap;->f:Ljava/lang/Object;

    .line 27572
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    .line 27573
    sget-object v0, Lcom/sec/chaton/a/as;->a:Lcom/sec/chaton/a/as;

    iput-object v0, p0, Lcom/sec/chaton/a/ap;->h:Lcom/sec/chaton/a/as;

    .line 27574
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    .line 27575
    iput-wide v1, p0, Lcom/sec/chaton/a/ap;->j:J

    .line 27576
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ap;
    .locals 1

    .prologue
    .line 27431
    sget-object v0, Lcom/sec/chaton/a/ap;->a:Lcom/sec/chaton/a/ap;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 27439
    iget v1, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 27442
    iget-wide v0, p0, Lcom/sec/chaton/a/ap;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 27449
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 27452
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->d:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 27459
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27416
    invoke-virtual {p0}, Lcom/sec/chaton/a/ap;->b()Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 27617
    iget v0, p0, Lcom/sec/chaton/a/ap;->l:I

    .line 27618
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 27664
    :goto_0
    return v0

    .line 27621
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 27622
    iget-wide v2, p0, Lcom/sec/chaton/a/ap;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 27625
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 27626
    iget-object v2, p0, Lcom/sec/chaton/a/ap;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 27629
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 27630
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ap;->s()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 27633
    :cond_2
    iget v2, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_3

    .line 27634
    invoke-direct {p0}, Lcom/sec/chaton/a/ap;->t()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v1

    .line 27639
    :goto_2
    iget-object v4, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 27640
    iget-object v4, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v2}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v3, v4

    .line 27639
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 27643
    :cond_4
    add-int/2addr v0, v3

    .line 27644
    invoke-virtual {p0}, Lcom/sec/chaton/a/ap;->k()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 27646
    iget v2, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    .line 27647
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/sec/chaton/a/ap;->h:Lcom/sec/chaton/a/as;

    invoke-virtual {v3}, Lcom/sec/chaton/a/as;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    move v2, v1

    .line 27652
    :goto_3
    iget-object v3, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_6

    .line 27653
    iget-object v3, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 27652
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 27656
    :cond_6
    add-int/2addr v0, v2

    .line 27657
    invoke-virtual {p0}, Lcom/sec/chaton/a/ap;->n()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 27659
    iget v1, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 27660
    iget-wide v1, p0, Lcom/sec/chaton/a/ap;->j:J

    invoke-static {v7, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27663
    :cond_7
    iput v0, p0, Lcom/sec/chaton/a/ap;->l:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27462
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->e:Ljava/lang/Object;

    .line 27463
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 27464
    check-cast v0, Ljava/lang/String;

    .line 27472
    :goto_0
    return-object v0

    .line 27466
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 27468
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 27469
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27470
    iput-object v1, p0, Lcom/sec/chaton/a/ap;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 27472
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 27491
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 27579
    iget-byte v1, p0, Lcom/sec/chaton/a/ap;->k:B

    .line 27580
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 27583
    :goto_0
    return v0

    .line 27580
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 27582
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ap;->k:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27494
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->f:Ljava/lang/Object;

    .line 27495
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 27496
    check-cast v0, Ljava/lang/String;

    .line 27504
    :goto_0
    return-object v0

    .line 27498
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 27500
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 27501
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27502
    iput-object v1, p0, Lcom/sec/chaton/a/ap;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 27504
    goto :goto_0
.end method

.method public k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27524
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 27537
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Lcom/sec/chaton/a/as;
    .locals 1

    .prologue
    .line 27540
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->h:Lcom/sec/chaton/a/as;

    return-object v0
.end method

.method public n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27548
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27416
    invoke-virtual {p0}, Lcom/sec/chaton/a/ap;->q()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 27561
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()J
    .locals 2

    .prologue
    .line 27564
    iget-wide v0, p0, Lcom/sec/chaton/a/ap;->j:J

    return-wide v0
.end method

.method public q()Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 27742
    invoke-static {}, Lcom/sec/chaton/a/ap;->newBuilder()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public r()Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 27746
    invoke-static {p0}, Lcom/sec/chaton/a/ap;->a(Lcom/sec/chaton/a/ap;)Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27416
    invoke-virtual {p0}, Lcom/sec/chaton/a/ap;->r()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27671
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 27588
    invoke-virtual {p0}, Lcom/sec/chaton/a/ap;->getSerializedSize()I

    .line 27589
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 27590
    iget-wide v2, p0, Lcom/sec/chaton/a/ap;->c:J

    invoke-virtual {p1, v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 27592
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 27593
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 27595
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 27596
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ap;->s()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 27598
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 27599
    invoke-direct {p0}, Lcom/sec/chaton/a/ap;->t()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    move v0, v1

    .line 27601
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 27602
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/sec/chaton/a/ap;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 27601
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27604
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 27605
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/sec/chaton/a/ap;->h:Lcom/sec/chaton/a/as;

    invoke-virtual {v2}, Lcom/sec/chaton/a/as;->getNumber()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 27607
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 27608
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/sec/chaton/a/ap;->i:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 27607
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 27610
    :cond_6
    iget v0, p0, Lcom/sec/chaton/a/ap;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 27611
    iget-wide v0, p0, Lcom/sec/chaton/a/ap;->j:J

    invoke-virtual {p1, v7, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 27613
    :cond_7
    return-void
.end method
