.class public final Lcom/sec/chaton/a/ds;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/dr;",
        "Lcom/sec/chaton/a/ds;",
        ">;",
        "Lcom/sec/chaton/a/dt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2620
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2730
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ds;->b:Ljava/lang/Object;

    .line 2766
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ds;->c:Ljava/lang/Object;

    .line 2621
    invoke-direct {p0}, Lcom/sec/chaton/a/ds;->g()V

    .line 2622
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2615
    invoke-static {}, Lcom/sec/chaton/a/ds;->h()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 2625
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2627
    new-instance v0, Lcom/sec/chaton/a/ds;

    invoke-direct {v0}, Lcom/sec/chaton/a/ds;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2631
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2632
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ds;->b:Ljava/lang/Object;

    .line 2633
    iget v0, p0, Lcom/sec/chaton/a/ds;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ds;->a:I

    .line 2634
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ds;->c:Ljava/lang/Object;

    .line 2635
    iget v0, p0, Lcom/sec/chaton/a/ds;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ds;->a:I

    .line 2636
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2701
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2702
    sparse-switch v0, :sswitch_data_0

    .line 2707
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ds;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2709
    :sswitch_0
    return-object p0

    .line 2714
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ds;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ds;->a:I

    .line 2715
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ds;->b:Ljava/lang/Object;

    goto :goto_0

    .line 2719
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ds;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ds;->a:I

    .line 2720
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ds;->c:Ljava/lang/Object;

    goto :goto_0

    .line 2702
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2682
    invoke-static {}, Lcom/sec/chaton/a/dr;->a()Lcom/sec/chaton/a/dr;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2689
    :cond_0
    :goto_0
    return-object p0

    .line 2683
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/dr;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2684
    invoke-virtual {p1}, Lcom/sec/chaton/a/dr;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ds;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ds;

    .line 2686
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/dr;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687
    invoke-virtual {p1}, Lcom/sec/chaton/a/dr;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ds;->b(Ljava/lang/String;)Lcom/sec/chaton/a/ds;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2745
    if-nez p1, :cond_0

    .line 2746
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2748
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ds;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ds;->a:I

    .line 2749
    iput-object p1, p0, Lcom/sec/chaton/a/ds;->b:Ljava/lang/Object;

    .line 2751
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ds;
    .locals 2

    .prologue
    .line 2640
    invoke-static {}, Lcom/sec/chaton/a/ds;->h()Lcom/sec/chaton/a/ds;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->e()Lcom/sec/chaton/a/dr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ds;->a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/ds;
    .locals 1

    .prologue
    .line 2781
    if-nez p1, :cond_0

    .line 2782
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2784
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ds;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ds;->a:I

    .line 2785
    iput-object p1, p0, Lcom/sec/chaton/a/ds;->c:Ljava/lang/Object;

    .line 2787
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->d()Lcom/sec/chaton/a/dr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->e()Lcom/sec/chaton/a/dr;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/dr;
    .locals 1

    .prologue
    .line 2644
    invoke-static {}, Lcom/sec/chaton/a/dr;->a()Lcom/sec/chaton/a/dr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->a()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->a()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->b()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->b()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->b()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->b()Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/dr;
    .locals 2

    .prologue
    .line 2648
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->e()Lcom/sec/chaton/a/dr;

    move-result-object v0

    .line 2649
    invoke-virtual {v0}, Lcom/sec/chaton/a/dr;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2650
    invoke-static {v0}, Lcom/sec/chaton/a/ds;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2652
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/dr;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2666
    new-instance v2, Lcom/sec/chaton/a/dr;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/dr;-><init>(Lcom/sec/chaton/a/ds;Lcom/sec/chaton/a/b;)V

    .line 2667
    iget v3, p0, Lcom/sec/chaton/a/ds;->a:I

    .line 2668
    const/4 v1, 0x0

    .line 2669
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2672
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/ds;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dr;->a(Lcom/sec/chaton/a/dr;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2674
    or-int/lit8 v0, v0, 0x2

    .line 2676
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ds;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dr;->b(Lcom/sec/chaton/a/dr;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2677
    invoke-static {v2, v0}, Lcom/sec/chaton/a/dr;->a(Lcom/sec/chaton/a/dr;I)I

    .line 2678
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->c()Lcom/sec/chaton/a/dr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0}, Lcom/sec/chaton/a/ds;->c()Lcom/sec/chaton/a/dr;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 2693
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ds;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    check-cast p1, Lcom/sec/chaton/a/dr;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ds;->a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2615
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ds;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ds;

    move-result-object v0

    return-object v0
.end method
