.class public final Lcom/sec/chaton/a/cl;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cn;


# static fields
.field private static final a:Lcom/sec/chaton/a/cl;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30809
    new-instance v0, Lcom/sec/chaton/a/cl;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/cl;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/cl;->a:Lcom/sec/chaton/a/cl;

    .line 30810
    sget-object v0, Lcom/sec/chaton/a/cl;->a:Lcom/sec/chaton/a/cl;

    invoke-direct {v0}, Lcom/sec/chaton/a/cl;->o()V

    .line 30811
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cm;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 30224
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 30339
    iput-byte v0, p0, Lcom/sec/chaton/a/cl;->g:B

    .line 30365
    iput v0, p0, Lcom/sec/chaton/a/cl;->h:I

    .line 30225
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cm;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 30219
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/cl;-><init>(Lcom/sec/chaton/a/cm;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 30226
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 30339
    iput-byte v0, p0, Lcom/sec/chaton/a/cl;->g:B

    .line 30365
    iput v0, p0, Lcom/sec/chaton/a/cl;->h:I

    .line 30226
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cl;I)I
    .locals 0

    .prologue
    .line 30219
    iput p1, p0, Lcom/sec/chaton/a/cl;->b:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/cl;
    .locals 1

    .prologue
    .line 30230
    sget-object v0, Lcom/sec/chaton/a/cl;->a:Lcom/sec/chaton/a/cl;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/cl;
    .locals 1

    .prologue
    .line 30412
    invoke-static {}, Lcom/sec/chaton/a/cl;->newBuilder()Lcom/sec/chaton/a/cm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cm;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cm;

    invoke-static {v0}, Lcom/sec/chaton/a/cm;->a(Lcom/sec/chaton/a/cm;)Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/cl;)Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30468
    invoke-static {}, Lcom/sec/chaton/a/cl;->newBuilder()Lcom/sec/chaton/a/cm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cm;->a(Lcom/sec/chaton/a/cl;)Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/cl;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 30219
    iput-object p1, p0, Lcom/sec/chaton/a/cl;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cl;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 30219
    iput-object p1, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cl;Z)Z
    .locals 0

    .prologue
    .line 30219
    iput-boolean p1, p0, Lcom/sec/chaton/a/cl;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cl;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 30219
    iput-object p1, p0, Lcom/sec/chaton/a/cl;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cl;)Ljava/util/List;
    .locals 1

    .prologue
    .line 30219
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    return-object v0
.end method

.method private m()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30259
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->c:Ljava/lang/Object;

    .line 30260
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30261
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30263
    iput-object v0, p0, Lcom/sec/chaton/a/cl;->c:Ljava/lang/Object;

    .line 30266
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private n()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30322
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->f:Ljava/lang/Object;

    .line 30323
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30324
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30326
    iput-object v0, p0, Lcom/sec/chaton/a/cl;->f:Ljava/lang/Object;

    .line 30329
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30465
    invoke-static {}, Lcom/sec/chaton/a/cm;->f()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 30334
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cl;->c:Ljava/lang/Object;

    .line 30335
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    .line 30336
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/cl;->e:Z

    .line 30337
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cl;->f:Ljava/lang/Object;

    .line 30338
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/a/cr;
    .locals 1

    .prologue
    .line 30284
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cr;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/cl;
    .locals 1

    .prologue
    .line 30234
    sget-object v0, Lcom/sec/chaton/a/cl;->a:Lcom/sec/chaton/a/cl;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 30242
    iget v1, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30245
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->c:Ljava/lang/Object;

    .line 30246
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30247
    check-cast v0, Ljava/lang/String;

    .line 30255
    :goto_0
    return-object v0

    .line 30249
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30251
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30252
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30253
    iput-object v1, p0, Lcom/sec/chaton/a/cl;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30255
    goto :goto_0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30274
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 30281
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 30295
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30219
    invoke-virtual {p0}, Lcom/sec/chaton/a/cl;->b()Lcom/sec/chaton/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 30367
    iget v2, p0, Lcom/sec/chaton/a/cl;->h:I

    .line 30368
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 30388
    :goto_0
    return v2

    .line 30371
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 30372
    invoke-direct {p0}, Lcom/sec/chaton/a/cl;->m()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v0

    .line 30375
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 30376
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 30375
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 30379
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 30380
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sec/chaton/a/cl;->e:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 30383
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 30384
    invoke-direct {p0}, Lcom/sec/chaton/a/cl;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 30387
    :cond_3
    iput v2, p0, Lcom/sec/chaton/a/cl;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 30298
    iget-boolean v0, p0, Lcom/sec/chaton/a/cl;->e:Z

    return v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 30305
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 30341
    iget-byte v1, p0, Lcom/sec/chaton/a/cl;->g:B

    .line 30342
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 30345
    :goto_0
    return v0

    .line 30342
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 30344
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/cl;->g:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30308
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->f:Ljava/lang/Object;

    .line 30309
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30310
    check-cast v0, Ljava/lang/String;

    .line 30318
    :goto_0
    return-object v0

    .line 30312
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30314
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30315
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30316
    iput-object v1, p0, Lcom/sec/chaton/a/cl;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30318
    goto :goto_0
.end method

.method public k()Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30466
    invoke-static {}, Lcom/sec/chaton/a/cl;->newBuilder()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/sec/chaton/a/cm;
    .locals 1

    .prologue
    .line 30470
    invoke-static {p0}, Lcom/sec/chaton/a/cl;->a(Lcom/sec/chaton/a/cl;)Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30219
    invoke-virtual {p0}, Lcom/sec/chaton/a/cl;->k()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30219
    invoke-virtual {p0}, Lcom/sec/chaton/a/cl;->l()Lcom/sec/chaton/a/cm;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30395
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 30350
    invoke-virtual {p0}, Lcom/sec/chaton/a/cl;->getSerializedSize()I

    .line 30351
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 30352
    invoke-direct {p0}, Lcom/sec/chaton/a/cl;->m()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 30354
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 30355
    iget-object v0, p0, Lcom/sec/chaton/a/cl;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 30354
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 30357
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 30358
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sec/chaton/a/cl;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 30360
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/cl;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 30361
    invoke-direct {p0}, Lcom/sec/chaton/a/cl;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 30363
    :cond_3
    return-void
.end method
