.class public final Lcom/sec/chaton/a/ea;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ec;


# static fields
.field private static final a:Lcom/sec/chaton/a/ea;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dm;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1737
    new-instance v0, Lcom/sec/chaton/a/ea;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ea;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ea;->a:Lcom/sec/chaton/a/ea;

    .line 1738
    sget-object v0, Lcom/sec/chaton/a/ea;->a:Lcom/sec/chaton/a/ea;

    invoke-direct {v0}, Lcom/sec/chaton/a/ea;->f()V

    .line 1739
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/eb;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1388
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1425
    iput-byte v0, p0, Lcom/sec/chaton/a/ea;->c:B

    .line 1442
    iput v0, p0, Lcom/sec/chaton/a/ea;->d:I

    .line 1389
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/eb;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 1383
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ea;-><init>(Lcom/sec/chaton/a/eb;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1390
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1425
    iput-byte v0, p0, Lcom/sec/chaton/a/ea;->c:B

    .line 1442
    iput v0, p0, Lcom/sec/chaton/a/ea;->d:I

    .line 1390
    return-void
.end method

.method public static a()Lcom/sec/chaton/a/ea;
    .locals 1

    .prologue
    .line 1394
    sget-object v0, Lcom/sec/chaton/a/ea;->a:Lcom/sec/chaton/a/ea;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1533
    invoke-static {}, Lcom/sec/chaton/a/ea;->newBuilder()Lcom/sec/chaton/a/eb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/eb;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ea;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 1383
    iput-object p1, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ea;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1383
    iget-object v0, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1423
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    .line 1424
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1530
    invoke-static {}, Lcom/sec/chaton/a/eb;->f()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ea;
    .locals 1

    .prologue
    .line 1398
    sget-object v0, Lcom/sec/chaton/a/ea;->a:Lcom/sec/chaton/a/ea;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1531
    invoke-static {}, Lcom/sec/chaton/a/ea;->newBuilder()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1535
    invoke-static {p0}, Lcom/sec/chaton/a/ea;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1383
    invoke-virtual {p0}, Lcom/sec/chaton/a/ea;->b()Lcom/sec/chaton/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1444
    iget v2, p0, Lcom/sec/chaton/a/ea;->d:I

    .line 1445
    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 1453
    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    .line 1448
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1449
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 1448
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1452
    :cond_1
    iput v2, p0, Lcom/sec/chaton/a/ea;->d:I

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1427
    iget-byte v1, p0, Lcom/sec/chaton/a/ea;->c:B

    .line 1428
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 1431
    :goto_0
    return v0

    .line 1428
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1430
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ea;->c:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1383
    invoke-virtual {p0}, Lcom/sec/chaton/a/ea;->d()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1383
    invoke-virtual {p0}, Lcom/sec/chaton/a/ea;->e()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1460
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    .line 1436
    invoke-virtual {p0}, Lcom/sec/chaton/a/ea;->getSerializedSize()I

    .line 1437
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1438
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/chaton/a/ea;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 1437
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1440
    :cond_0
    return-void
.end method
