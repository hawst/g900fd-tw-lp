.class public final Lcom/sec/chaton/a/r;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/t;


# static fields
.field private static final a:Lcom/sec/chaton/a/r;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/x;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16475
    new-instance v0, Lcom/sec/chaton/a/r;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/r;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/r;->a:Lcom/sec/chaton/a/r;

    .line 16476
    sget-object v0, Lcom/sec/chaton/a/r;->a:Lcom/sec/chaton/a/r;

    invoke-direct {v0}, Lcom/sec/chaton/a/r;->g()V

    .line 16477
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/s;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 16126
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 16163
    iput-byte v0, p0, Lcom/sec/chaton/a/r;->c:B

    .line 16180
    iput v0, p0, Lcom/sec/chaton/a/r;->d:I

    .line 16127
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/s;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 16121
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/r;-><init>(Lcom/sec/chaton/a/s;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 16128
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 16163
    iput-byte v0, p0, Lcom/sec/chaton/a/r;->c:B

    .line 16180
    iput v0, p0, Lcom/sec/chaton/a/r;->d:I

    .line 16128
    return-void
.end method

.method public static a()Lcom/sec/chaton/a/r;
    .locals 1

    .prologue
    .line 16132
    sget-object v0, Lcom/sec/chaton/a/r;->a:Lcom/sec/chaton/a/r;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/r;
    .locals 1

    .prologue
    .line 16215
    invoke-static {}, Lcom/sec/chaton/a/r;->newBuilder()Lcom/sec/chaton/a/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/s;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/s;

    invoke-static {v0}, Lcom/sec/chaton/a/s;->a(Lcom/sec/chaton/a/s;)Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/r;)Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16271
    invoke-static {}, Lcom/sec/chaton/a/r;->newBuilder()Lcom/sec/chaton/a/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/s;->a(Lcom/sec/chaton/a/r;)Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/r;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 16121
    iput-object p1, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/r;)Ljava/util/List;
    .locals 1

    .prologue
    .line 16121
    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 16161
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    .line 16162
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16268
    invoke-static {}, Lcom/sec/chaton/a/s;->f()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/a/x;
    .locals 1

    .prologue
    .line 16153
    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/x;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/r;
    .locals 1

    .prologue
    .line 16136
    sget-object v0, Lcom/sec/chaton/a/r;->a:Lcom/sec/chaton/a/r;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16143
    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 16150
    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public e()Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16269
    invoke-static {}, Lcom/sec/chaton/a/r;->newBuilder()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16273
    invoke-static {p0}, Lcom/sec/chaton/a/r;->a(Lcom/sec/chaton/a/r;)Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16121
    invoke-virtual {p0}, Lcom/sec/chaton/a/r;->b()Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 16182
    iget v2, p0, Lcom/sec/chaton/a/r;->d:I

    .line 16183
    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 16191
    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    .line 16186
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 16187
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 16186
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 16190
    :cond_1
    iput v2, p0, Lcom/sec/chaton/a/r;->d:I

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 16165
    iget-byte v1, p0, Lcom/sec/chaton/a/r;->c:B

    .line 16166
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 16169
    :goto_0
    return v0

    .line 16166
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 16168
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/r;->c:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16121
    invoke-virtual {p0}, Lcom/sec/chaton/a/r;->e()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16121
    invoke-virtual {p0}, Lcom/sec/chaton/a/r;->f()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16198
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    .line 16174
    invoke-virtual {p0}, Lcom/sec/chaton/a/r;->getSerializedSize()I

    .line 16175
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 16176
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/chaton/a/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 16175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 16178
    :cond_0
    return-void
.end method
