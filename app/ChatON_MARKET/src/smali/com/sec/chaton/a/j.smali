.class public final Lcom/sec/chaton/a/j;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/i;",
        "Lcom/sec/chaton/a/j;",
        ">;",
        "Lcom/sec/chaton/a/k;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/bc;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:I

.field private j:J

.field private k:J

.field private l:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 10579
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 10840
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/j;->c:Lcom/sec/chaton/a/bc;

    .line 10864
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->d:Ljava/lang/Object;

    .line 10900
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->e:Ljava/lang/Object;

    .line 10936
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->f:Ljava/lang/Object;

    .line 10972
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->g:Ljava/lang/Object;

    .line 11008
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->h:Ljava/lang/Object;

    .line 10580
    invoke-direct {p0}, Lcom/sec/chaton/a/j;->g()V

    .line 10581
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/j;)Lcom/sec/chaton/a/i;
    .locals 1

    .prologue
    .line 10574
    invoke-direct {p0}, Lcom/sec/chaton/a/j;->i()Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10574
    invoke-static {}, Lcom/sec/chaton/a/j;->h()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 10584
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10586
    new-instance v0, Lcom/sec/chaton/a/j;

    invoke-direct {v0}, Lcom/sec/chaton/a/j;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/i;
    .locals 2

    .prologue
    .line 10634
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->e()Lcom/sec/chaton/a/i;

    move-result-object v0

    .line 10635
    invoke-virtual {v0}, Lcom/sec/chaton/a/i;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 10636
    invoke-static {v0}, Lcom/sec/chaton/a/j;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 10639
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/j;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    .line 10590
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 10591
    iput-wide v1, p0, Lcom/sec/chaton/a/j;->b:J

    .line 10592
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10593
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/j;->c:Lcom/sec/chaton/a/bc;

    .line 10594
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10595
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->d:Ljava/lang/Object;

    .line 10596
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10597
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->e:Ljava/lang/Object;

    .line 10598
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10599
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->f:Ljava/lang/Object;

    .line 10600
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10601
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->g:Ljava/lang/Object;

    .line 10602
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10603
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/j;->h:Ljava/lang/Object;

    .line 10604
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10605
    iput v3, p0, Lcom/sec/chaton/a/j;->i:I

    .line 10606
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10607
    iput-wide v1, p0, Lcom/sec/chaton/a/j;->j:J

    .line 10608
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10609
    iput-wide v1, p0, Lcom/sec/chaton/a/j;->k:J

    .line 10610
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10611
    iput-boolean v3, p0, Lcom/sec/chaton/a/j;->l:Z

    .line 10612
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10613
    return-object p0
.end method

.method public a(I)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 11052
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 11053
    iput p1, p0, Lcom/sec/chaton/a/j;->i:I

    .line 11055
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10827
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10828
    iput-wide p1, p0, Lcom/sec/chaton/a/j;->b:J

    .line 10830
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/j;
    .locals 2

    .prologue
    .line 10741
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 10742
    sparse-switch v0, :sswitch_data_0

    .line 10747
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/j;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10749
    :sswitch_0
    return-object p0

    .line 10754
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10755
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/j;->b:J

    goto :goto_0

    .line 10759
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 10760
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 10761
    if-eqz v0, :cond_0

    .line 10762
    iget v1, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10763
    iput-object v0, p0, Lcom/sec/chaton/a/j;->c:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 10768
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10769
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/j;->d:Ljava/lang/Object;

    goto :goto_0

    .line 10773
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10774
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/j;->e:Ljava/lang/Object;

    goto :goto_0

    .line 10778
    :sswitch_5
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10779
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/j;->f:Ljava/lang/Object;

    goto :goto_0

    .line 10783
    :sswitch_6
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10784
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/j;->g:Ljava/lang/Object;

    goto :goto_0

    .line 10788
    :sswitch_7
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10789
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/j;->h:Ljava/lang/Object;

    goto :goto_0

    .line 10793
    :sswitch_8
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10794
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/a/j;->i:I

    goto :goto_0

    .line 10798
    :sswitch_9
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10799
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/j;->j:J

    goto/16 :goto_0

    .line 10803
    :sswitch_a
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10804
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/j;->k:J

    goto/16 :goto_0

    .line 10808
    :sswitch_b
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10809
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/a/j;->l:Z

    goto/16 :goto_0

    .line 10742
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10848
    if-nez p1, :cond_0

    .line 10849
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10851
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10852
    iput-object p1, p0, Lcom/sec/chaton/a/j;->c:Lcom/sec/chaton/a/bc;

    .line 10854
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/i;)Lcom/sec/chaton/a/j;
    .locals 2

    .prologue
    .line 10695
    invoke-static {}, Lcom/sec/chaton/a/i;->a()Lcom/sec/chaton/a/i;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 10729
    :cond_0
    :goto_0
    return-object p0

    .line 10696
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10697
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/j;->a(J)Lcom/sec/chaton/a/j;

    .line 10699
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10700
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->f()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/j;

    .line 10702
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10703
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->a(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    .line 10705
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 10706
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->b(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    .line 10708
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 10709
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->c(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    .line 10711
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 10712
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->d(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    .line 10714
    :cond_7
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 10715
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->e(Ljava/lang/String;)Lcom/sec/chaton/a/j;

    .line 10717
    :cond_8
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->q()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 10718
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->a(I)Lcom/sec/chaton/a/j;

    .line 10720
    :cond_9
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->s()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 10721
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->t()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/j;->b(J)Lcom/sec/chaton/a/j;

    .line 10723
    :cond_a
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->u()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 10724
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->v()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/j;->c(J)Lcom/sec/chaton/a/j;

    .line 10726
    :cond_b
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10727
    invoke-virtual {p1}, Lcom/sec/chaton/a/i;->x()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/j;->a(Z)Lcom/sec/chaton/a/j;

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10879
    if-nez p1, :cond_0

    .line 10880
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10882
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10883
    iput-object p1, p0, Lcom/sec/chaton/a/j;->d:Ljava/lang/Object;

    .line 10885
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 11115
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 11116
    iput-boolean p1, p0, Lcom/sec/chaton/a/j;->l:Z

    .line 11118
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/j;
    .locals 2

    .prologue
    .line 10617
    invoke-static {}, Lcom/sec/chaton/a/j;->h()Lcom/sec/chaton/a/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->e()Lcom/sec/chaton/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/j;->a(Lcom/sec/chaton/a/i;)Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 11073
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 11074
    iput-wide p1, p0, Lcom/sec/chaton/a/j;->j:J

    .line 11076
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10915
    if-nez p1, :cond_0

    .line 10916
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10918
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10919
    iput-object p1, p0, Lcom/sec/chaton/a/j;->e:Ljava/lang/Object;

    .line 10921
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->d()Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->e()Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/i;
    .locals 1

    .prologue
    .line 10621
    invoke-static {}, Lcom/sec/chaton/a/i;->a()Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 11094
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 11095
    iput-wide p1, p0, Lcom/sec/chaton/a/j;->k:J

    .line 11097
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10951
    if-nez p1, :cond_0

    .line 10952
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10954
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10955
    iput-object p1, p0, Lcom/sec/chaton/a/j;->f:Ljava/lang/Object;

    .line 10957
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->a()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->a()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->b()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->b()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->b()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->b()Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/i;
    .locals 2

    .prologue
    .line 10625
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->e()Lcom/sec/chaton/a/i;

    move-result-object v0

    .line 10626
    invoke-virtual {v0}, Lcom/sec/chaton/a/i;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 10627
    invoke-static {v0}, Lcom/sec/chaton/a/j;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 10629
    :cond_0
    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 10987
    if-nez p1, :cond_0

    .line 10988
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10990
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10991
    iput-object p1, p0, Lcom/sec/chaton/a/j;->g:Ljava/lang/Object;

    .line 10993
    return-object p0
.end method

.method public e()Lcom/sec/chaton/a/i;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 10643
    new-instance v2, Lcom/sec/chaton/a/i;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/i;-><init>(Lcom/sec/chaton/a/j;Lcom/sec/chaton/a/b;)V

    .line 10644
    iget v3, p0, Lcom/sec/chaton/a/j;->a:I

    .line 10645
    const/4 v1, 0x0

    .line 10646
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    .line 10649
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/j;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/i;->a(Lcom/sec/chaton/a/i;J)J

    .line 10650
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 10651
    or-int/lit8 v0, v0, 0x2

    .line 10653
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/j;->c:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->a(Lcom/sec/chaton/a/i;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 10654
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 10655
    or-int/lit8 v0, v0, 0x4

    .line 10657
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/j;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->a(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10658
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 10659
    or-int/lit8 v0, v0, 0x8

    .line 10661
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/j;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->b(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10662
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 10663
    or-int/lit8 v0, v0, 0x10

    .line 10665
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/j;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->c(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10666
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 10667
    or-int/lit8 v0, v0, 0x20

    .line 10669
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/j;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->d(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10670
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 10671
    or-int/lit8 v0, v0, 0x40

    .line 10673
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/a/j;->h:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->e(Lcom/sec/chaton/a/i;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10674
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 10675
    or-int/lit16 v0, v0, 0x80

    .line 10677
    :cond_6
    iget v1, p0, Lcom/sec/chaton/a/j;->i:I

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->a(Lcom/sec/chaton/a/i;I)I

    .line 10678
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 10679
    or-int/lit16 v0, v0, 0x100

    .line 10681
    :cond_7
    iget-wide v4, p0, Lcom/sec/chaton/a/j;->j:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/i;->b(Lcom/sec/chaton/a/i;J)J

    .line 10682
    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 10683
    or-int/lit16 v0, v0, 0x200

    .line 10685
    :cond_8
    iget-wide v4, p0, Lcom/sec/chaton/a/j;->k:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/i;->c(Lcom/sec/chaton/a/i;J)J

    .line 10686
    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    .line 10687
    or-int/lit16 v0, v0, 0x400

    .line 10689
    :cond_9
    iget-boolean v1, p0, Lcom/sec/chaton/a/j;->l:Z

    invoke-static {v2, v1}, Lcom/sec/chaton/a/i;->a(Lcom/sec/chaton/a/i;Z)Z

    .line 10690
    invoke-static {v2, v0}, Lcom/sec/chaton/a/i;->b(Lcom/sec/chaton/a/i;I)I

    .line 10691
    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public e(Ljava/lang/String;)Lcom/sec/chaton/a/j;
    .locals 1

    .prologue
    .line 11023
    if-nez p1, :cond_0

    .line 11024
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11026
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/j;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/j;->a:I

    .line 11027
    iput-object p1, p0, Lcom/sec/chaton/a/j;->h:Ljava/lang/Object;

    .line 11029
    return-object p0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->c()Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0}, Lcom/sec/chaton/a/j;->c()Lcom/sec/chaton/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 10733
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/j;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    check-cast p1, Lcom/sec/chaton/a/i;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/j;->a(Lcom/sec/chaton/a/i;)Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 10574
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/j;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/j;

    move-result-object v0

    return-object v0
.end method
