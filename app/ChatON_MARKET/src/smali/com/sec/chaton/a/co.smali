.class public final Lcom/sec/chaton/a/co;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cq;


# static fields
.field private static final a:Lcom/sec/chaton/a/co;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31615
    new-instance v0, Lcom/sec/chaton/a/co;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/co;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/co;->a:Lcom/sec/chaton/a/co;

    .line 31616
    sget-object v0, Lcom/sec/chaton/a/co;->a:Lcom/sec/chaton/a/co;

    invoke-direct {v0}, Lcom/sec/chaton/a/co;->u()V

    .line 31617
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cp;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 30850
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 31031
    iput-byte v0, p0, Lcom/sec/chaton/a/co;->i:B

    .line 31063
    iput v0, p0, Lcom/sec/chaton/a/co;->j:I

    .line 30851
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cp;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 30845
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/co;-><init>(Lcom/sec/chaton/a/cp;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 30852
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31031
    iput-byte v0, p0, Lcom/sec/chaton/a/co;->i:B

    .line 31063
    iput v0, p0, Lcom/sec/chaton/a/co;->j:I

    .line 30852
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/co;I)I
    .locals 0

    .prologue
    .line 30845
    iput p1, p0, Lcom/sec/chaton/a/co;->g:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/co;
    .locals 1

    .prologue
    .line 30856
    sget-object v0, Lcom/sec/chaton/a/co;->a:Lcom/sec/chaton/a/co;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/co;
    .locals 1

    .prologue
    .line 31118
    invoke-static {}, Lcom/sec/chaton/a/co;->newBuilder()Lcom/sec/chaton/a/cp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cp;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cp;

    invoke-static {v0}, Lcom/sec/chaton/a/cp;->a(Lcom/sec/chaton/a/cp;)Lcom/sec/chaton/a/co;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/co;)Lcom/sec/chaton/a/cp;
    .locals 1

    .prologue
    .line 31174
    invoke-static {}, Lcom/sec/chaton/a/co;->newBuilder()Lcom/sec/chaton/a/cp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cp;->a(Lcom/sec/chaton/a/co;)Lcom/sec/chaton/a/cp;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/co;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 30845
    iput-object p1, p0, Lcom/sec/chaton/a/co;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/co;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 30845
    iput-object p1, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/co;I)I
    .locals 0

    .prologue
    .line 30845
    iput p1, p0, Lcom/sec/chaton/a/co;->b:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/co;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 30845
    iput-object p1, p0, Lcom/sec/chaton/a/co;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/co;)Ljava/util/List;
    .locals 1

    .prologue
    .line 30845
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/a/co;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 30845
    iput-object p1, p0, Lcom/sec/chaton/a/co;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/chaton/a/co;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 30845
    iput-object p1, p0, Lcom/sec/chaton/a/co;->h:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cp;
    .locals 1

    .prologue
    .line 31171
    invoke-static {}, Lcom/sec/chaton/a/cp;->f()Lcom/sec/chaton/a/cp;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30885
    iget-object v0, p0, Lcom/sec/chaton/a/co;->c:Ljava/lang/Object;

    .line 30886
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30887
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30889
    iput-object v0, p0, Lcom/sec/chaton/a/co;->c:Ljava/lang/Object;

    .line 30892
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30917
    iget-object v0, p0, Lcom/sec/chaton/a/co;->d:Ljava/lang/Object;

    .line 30918
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30919
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30921
    iput-object v0, p0, Lcom/sec/chaton/a/co;->d:Ljava/lang/Object;

    .line 30924
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30949
    iget-object v0, p0, Lcom/sec/chaton/a/co;->e:Ljava/lang/Object;

    .line 30950
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30951
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30953
    iput-object v0, p0, Lcom/sec/chaton/a/co;->e:Ljava/lang/Object;

    .line 30956
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31012
    iget-object v0, p0, Lcom/sec/chaton/a/co;->h:Ljava/lang/Object;

    .line 31013
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31014
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31016
    iput-object v0, p0, Lcom/sec/chaton/a/co;->h:Ljava/lang/Object;

    .line 31019
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 31024
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/co;->c:Ljava/lang/Object;

    .line 31025
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/co;->d:Ljava/lang/Object;

    .line 31026
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/co;->e:Ljava/lang/Object;

    .line 31027
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    .line 31028
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/co;->g:I

    .line 31029
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/co;->h:Ljava/lang/Object;

    .line 31030
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/a/cu;
    .locals 1

    .prologue
    .line 30974
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cu;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/co;
    .locals 1

    .prologue
    .line 30860
    sget-object v0, Lcom/sec/chaton/a/co;->a:Lcom/sec/chaton/a/co;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 30868
    iget v1, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30871
    iget-object v0, p0, Lcom/sec/chaton/a/co;->c:Ljava/lang/Object;

    .line 30872
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30873
    check-cast v0, Ljava/lang/String;

    .line 30881
    :goto_0
    return-object v0

    .line 30875
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30877
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30878
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30879
    iput-object v1, p0, Lcom/sec/chaton/a/co;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30881
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 30900
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30903
    iget-object v0, p0, Lcom/sec/chaton/a/co;->d:Ljava/lang/Object;

    .line 30904
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30905
    check-cast v0, Ljava/lang/String;

    .line 30913
    :goto_0
    return-object v0

    .line 30907
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30909
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30910
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30911
    iput-object v1, p0, Lcom/sec/chaton/a/co;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30913
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 30932
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30845
    invoke-virtual {p0}, Lcom/sec/chaton/a/co;->b()Lcom/sec/chaton/a/co;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 31065
    iget v2, p0, Lcom/sec/chaton/a/co;->j:I

    .line 31066
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 31094
    :goto_0
    return v2

    .line 31069
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 31070
    invoke-direct {p0}, Lcom/sec/chaton/a/co;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v1

    .line 31073
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 31074
    invoke-direct {p0}, Lcom/sec/chaton/a/co;->r()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 31077
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 31078
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/co;->s()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    move v2, v0

    .line 31081
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 31082
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 31081
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 31085
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 31086
    const/4 v0, 0x5

    iget v1, p0, Lcom/sec/chaton/a/co;->g:I

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 31089
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 31090
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/co;->t()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 31093
    :cond_5
    iput v2, p0, Lcom/sec/chaton/a/co;->j:I

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30935
    iget-object v0, p0, Lcom/sec/chaton/a/co;->e:Ljava/lang/Object;

    .line 30936
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30937
    check-cast v0, Ljava/lang/String;

    .line 30945
    :goto_0
    return-object v0

    .line 30939
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30941
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30942
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30943
    iput-object v1, p0, Lcom/sec/chaton/a/co;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30945
    goto :goto_0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30964
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 31033
    iget-byte v1, p0, Lcom/sec/chaton/a/co;->i:B

    .line 31034
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 31037
    :goto_0
    return v0

    .line 31034
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 31036
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/co;->i:B

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 30971
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 30985
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 30988
    iget v0, p0, Lcom/sec/chaton/a/co;->g:I

    return v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 30995
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30998
    iget-object v0, p0, Lcom/sec/chaton/a/co;->h:Ljava/lang/Object;

    .line 30999
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31000
    check-cast v0, Ljava/lang/String;

    .line 31008
    :goto_0
    return-object v0

    .line 31002
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31004
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31005
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31006
    iput-object v1, p0, Lcom/sec/chaton/a/co;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31008
    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30845
    invoke-virtual {p0}, Lcom/sec/chaton/a/co;->o()Lcom/sec/chaton/a/cp;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/sec/chaton/a/cp;
    .locals 1

    .prologue
    .line 31172
    invoke-static {}, Lcom/sec/chaton/a/co;->newBuilder()Lcom/sec/chaton/a/cp;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/sec/chaton/a/cp;
    .locals 1

    .prologue
    .line 31176
    invoke-static {p0}, Lcom/sec/chaton/a/co;->a(Lcom/sec/chaton/a/co;)Lcom/sec/chaton/a/cp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30845
    invoke-virtual {p0}, Lcom/sec/chaton/a/co;->p()Lcom/sec/chaton/a/cp;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31101
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 31042
    invoke-virtual {p0}, Lcom/sec/chaton/a/co;->getSerializedSize()I

    .line 31043
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 31044
    invoke-direct {p0}, Lcom/sec/chaton/a/co;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31046
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 31047
    invoke-direct {p0}, Lcom/sec/chaton/a/co;->r()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31049
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 31050
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/co;->s()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31052
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 31053
    iget-object v0, p0, Lcom/sec/chaton/a/co;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 31052
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 31055
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 31056
    const/4 v0, 0x5

    iget v1, p0, Lcom/sec/chaton/a/co;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 31058
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/co;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 31059
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/co;->t()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31061
    :cond_5
    return-void
.end method
