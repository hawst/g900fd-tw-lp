.class public final Lcom/sec/chaton/a/g;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/f;",
        "Lcom/sec/chaton/a/g;",
        ">;",
        "Lcom/sec/chaton/a/h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/ej;

.field private d:Lcom/google/protobuf/LazyStringList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 11348
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 11506
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    .line 11549
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    .line 11349
    invoke-direct {p0}, Lcom/sec/chaton/a/g;->i()V

    .line 11350
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/g;)Lcom/sec/chaton/a/f;
    .locals 1

    .prologue
    .line 11343
    invoke-direct {p0}, Lcom/sec/chaton/a/g;->k()Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11343
    invoke-static {}, Lcom/sec/chaton/a/g;->j()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 11353
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11355
    new-instance v0, Lcom/sec/chaton/a/g;

    invoke-direct {v0}, Lcom/sec/chaton/a/g;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/f;
    .locals 2

    .prologue
    .line 11387
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->e()Lcom/sec/chaton/a/f;

    move-result-object v0

    .line 11388
    invoke-virtual {v0}, Lcom/sec/chaton/a/f;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 11389
    invoke-static {v0}, Lcom/sec/chaton/a/g;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 11392
    :cond_0
    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 11551
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 11552
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    .line 11553
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11555
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/g;
    .locals 2

    .prologue
    .line 11359
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 11360
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/g;->b:J

    .line 11361
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11362
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    .line 11363
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11364
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    .line 11365
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11366
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11493
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11494
    iput-wide p1, p0, Lcom/sec/chaton/a/g;->b:J

    .line 11496
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/g;
    .locals 2

    .prologue
    .line 11447
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 11448
    sparse-switch v0, :sswitch_data_0

    .line 11453
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/g;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11455
    :sswitch_0
    return-object p0

    .line 11460
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11461
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/g;->b:J

    goto :goto_0

    .line 11465
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 11466
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11467
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 11469
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 11470
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/g;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/g;

    goto :goto_0

    .line 11474
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/chaton/a/g;->l()V

    .line 11475
    iget-object v0, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 11448
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11514
    if-nez p1, :cond_0

    .line 11515
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11517
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    .line 11519
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11520
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ek;)Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11524
    invoke-virtual {p1}, Lcom/sec/chaton/a/ek;->d()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    .line 11526
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11527
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/f;)Lcom/sec/chaton/a/g;
    .locals 2

    .prologue
    .line 11418
    invoke-static {}, Lcom/sec/chaton/a/f;->a()Lcom/sec/chaton/a/f;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 11435
    :cond_0
    :goto_0
    return-object p0

    .line 11419
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/f;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11420
    invoke-virtual {p1}, Lcom/sec/chaton/a/f;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/g;->a(J)Lcom/sec/chaton/a/g;

    .line 11422
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 11423
    invoke-virtual {p1}, Lcom/sec/chaton/a/f;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/g;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/g;

    .line 11425
    :cond_3
    invoke-static {p1}, Lcom/sec/chaton/a/f;->b(Lcom/sec/chaton/a/f;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 11426
    iget-object v0, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 11427
    invoke-static {p1}, Lcom/sec/chaton/a/f;->b(Lcom/sec/chaton/a/f;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    .line 11428
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    goto :goto_0

    .line 11430
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/a/g;->l()V

    .line 11431
    iget-object v0, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/f;->b(Lcom/sec/chaton/a/f;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public b()Lcom/sec/chaton/a/g;
    .locals 2

    .prologue
    .line 11370
    invoke-static {}, Lcom/sec/chaton/a/g;->j()Lcom/sec/chaton/a/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->e()Lcom/sec/chaton/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/g;->a(Lcom/sec/chaton/a/f;)Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/g;
    .locals 2

    .prologue
    .line 11530
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 11532
    iget-object v0, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    .line 11538
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11539
    return-object p0

    .line 11535
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->d()Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->e()Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/f;
    .locals 1

    .prologue
    .line 11374
    invoke-static {}, Lcom/sec/chaton/a/f;->a()Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->a()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->a()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->b()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->b()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->b()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->b()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/f;
    .locals 2

    .prologue
    .line 11378
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->e()Lcom/sec/chaton/a/f;

    move-result-object v0

    .line 11379
    invoke-virtual {v0}, Lcom/sec/chaton/a/f;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 11380
    invoke-static {v0}, Lcom/sec/chaton/a/g;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 11382
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/f;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 11396
    new-instance v2, Lcom/sec/chaton/a/f;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/f;-><init>(Lcom/sec/chaton/a/g;Lcom/sec/chaton/a/b;)V

    .line 11397
    iget v3, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11398
    const/4 v1, 0x0

    .line 11399
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 11402
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/g;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/f;->a(Lcom/sec/chaton/a/f;J)J

    .line 11403
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 11404
    or-int/lit8 v0, v0, 0x2

    .line 11406
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/f;->a(Lcom/sec/chaton/a/f;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 11407
    iget v1, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 11408
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v3, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v3}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    .line 11410
    iget v1, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/sec/chaton/a/g;->a:I

    .line 11412
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/g;->d:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/f;->a(Lcom/sec/chaton/a/f;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 11413
    invoke-static {v2, v0}, Lcom/sec/chaton/a/f;->a(Lcom/sec/chaton/a/f;I)I

    .line 11414
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 11508
    iget v0, p0, Lcom/sec/chaton/a/g;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 11511
    iget-object v0, p0, Lcom/sec/chaton/a/g;->c:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->c()Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0}, Lcom/sec/chaton/a/g;->c()Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 11439
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/g;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    check-cast p1, Lcom/sec/chaton/a/f;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/g;->a(Lcom/sec/chaton/a/f;)Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11343
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/g;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method
