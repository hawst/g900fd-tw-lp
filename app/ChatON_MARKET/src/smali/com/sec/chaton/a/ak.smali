.class public final Lcom/sec/chaton/a/ak;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/al;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/aj;",
        "Lcom/sec/chaton/a/ak;",
        ">;",
        "Lcom/sec/chaton/a/al;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/sec/chaton/a/au;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 6340
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 6468
    sget-object v0, Lcom/sec/chaton/a/au;->a:Lcom/sec/chaton/a/au;

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->b:Lcom/sec/chaton/a/au;

    .line 6492
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->c:Ljava/lang/Object;

    .line 6528
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->d:Ljava/lang/Object;

    .line 6341
    invoke-direct {p0}, Lcom/sec/chaton/a/ak;->g()V

    .line 6342
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6335
    invoke-static {}, Lcom/sec/chaton/a/ak;->h()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 6345
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6347
    new-instance v0, Lcom/sec/chaton/a/ak;

    invoke-direct {v0}, Lcom/sec/chaton/a/ak;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6351
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 6352
    sget-object v0, Lcom/sec/chaton/a/au;->a:Lcom/sec/chaton/a/au;

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->b:Lcom/sec/chaton/a/au;

    .line 6353
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6354
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->c:Ljava/lang/Object;

    .line 6355
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6356
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->d:Ljava/lang/Object;

    .line 6357
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6358
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ak;
    .locals 2

    .prologue
    .line 6430
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 6431
    sparse-switch v0, :sswitch_data_0

    .line 6436
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ak;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6438
    :sswitch_0
    return-object p0

    .line 6443
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 6444
    invoke-static {v0}, Lcom/sec/chaton/a/au;->a(I)Lcom/sec/chaton/a/au;

    move-result-object v0

    .line 6445
    if-eqz v0, :cond_0

    .line 6446
    iget v1, p0, Lcom/sec/chaton/a/ak;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6447
    iput-object v0, p0, Lcom/sec/chaton/a/ak;->b:Lcom/sec/chaton/a/au;

    goto :goto_0

    .line 6452
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6453
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->c:Ljava/lang/Object;

    goto :goto_0

    .line 6457
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6458
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ak;->d:Ljava/lang/Object;

    goto :goto_0

    .line 6431
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6408
    invoke-static {}, Lcom/sec/chaton/a/aj;->a()Lcom/sec/chaton/a/aj;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 6418
    :cond_0
    :goto_0
    return-object p0

    .line 6409
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/aj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6410
    invoke-virtual {p1}, Lcom/sec/chaton/a/aj;->d()Lcom/sec/chaton/a/au;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ak;->a(Lcom/sec/chaton/a/au;)Lcom/sec/chaton/a/ak;

    .line 6412
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6413
    invoke-virtual {p1}, Lcom/sec/chaton/a/aj;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ak;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ak;

    .line 6415
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/aj;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6416
    invoke-virtual {p1}, Lcom/sec/chaton/a/aj;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ak;->b(Ljava/lang/String;)Lcom/sec/chaton/a/ak;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/au;)Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6476
    if-nez p1, :cond_0

    .line 6477
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6479
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6480
    iput-object p1, p0, Lcom/sec/chaton/a/ak;->b:Lcom/sec/chaton/a/au;

    .line 6482
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6507
    if-nez p1, :cond_0

    .line 6508
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6510
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6511
    iput-object p1, p0, Lcom/sec/chaton/a/ak;->c:Ljava/lang/Object;

    .line 6513
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ak;
    .locals 2

    .prologue
    .line 6362
    invoke-static {}, Lcom/sec/chaton/a/ak;->h()Lcom/sec/chaton/a/ak;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->e()Lcom/sec/chaton/a/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ak;->a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6543
    if-nez p1, :cond_0

    .line 6544
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6546
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ak;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6547
    iput-object p1, p0, Lcom/sec/chaton/a/ak;->d:Ljava/lang/Object;

    .line 6549
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->d()Lcom/sec/chaton/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->e()Lcom/sec/chaton/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/aj;
    .locals 1

    .prologue
    .line 6366
    invoke-static {}, Lcom/sec/chaton/a/aj;->a()Lcom/sec/chaton/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->a()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->a()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->b()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->b()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->b()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->b()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/aj;
    .locals 2

    .prologue
    .line 6370
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->e()Lcom/sec/chaton/a/aj;

    move-result-object v0

    .line 6371
    invoke-virtual {v0}, Lcom/sec/chaton/a/aj;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6372
    invoke-static {v0}, Lcom/sec/chaton/a/ak;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 6374
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/aj;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 6388
    new-instance v2, Lcom/sec/chaton/a/aj;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/aj;-><init>(Lcom/sec/chaton/a/ak;Lcom/sec/chaton/a/b;)V

    .line 6389
    iget v3, p0, Lcom/sec/chaton/a/ak;->a:I

    .line 6390
    const/4 v1, 0x0

    .line 6391
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 6394
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/ak;->b:Lcom/sec/chaton/a/au;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/aj;->a(Lcom/sec/chaton/a/aj;Lcom/sec/chaton/a/au;)Lcom/sec/chaton/a/au;

    .line 6395
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 6396
    or-int/lit8 v0, v0, 0x2

    .line 6398
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ak;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/aj;->a(Lcom/sec/chaton/a/aj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6399
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 6400
    or-int/lit8 v0, v0, 0x4

    .line 6402
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/ak;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/aj;->b(Lcom/sec/chaton/a/aj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6403
    invoke-static {v2, v0}, Lcom/sec/chaton/a/aj;->a(Lcom/sec/chaton/a/aj;I)I

    .line 6404
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->c()Lcom/sec/chaton/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0}, Lcom/sec/chaton/a/ak;->c()Lcom/sec/chaton/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 6422
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ak;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    check-cast p1, Lcom/sec/chaton/a/aj;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ak;->a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6335
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ak;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method
