.class public Lcom/sec/chaton/a/a/c;
.super Ljava/lang/Object;
.source "DeliveryChatResultEntry.java"


# instance fields
.field protected a:Z

.field protected b:Lcom/sec/chaton/a/a/n;

.field protected c:I

.field protected d:Ljava/lang/String;

.field protected e:Z

.field protected f:I

.field protected g:Ljava/lang/String;

.field protected h:Ljava/lang/String;

.field protected i:Lcom/sec/chaton/e/w;

.field protected j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v1, p0, Lcom/sec/chaton/a/a/c;->a:Z

    .line 29
    sget-object v0, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    iput-object v0, p0, Lcom/sec/chaton/a/a/c;->b:Lcom/sec/chaton/a/a/n;

    .line 30
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/chaton/a/a/c;->c:I

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/a/c;->d:Ljava/lang/String;

    .line 32
    iput-boolean v1, p0, Lcom/sec/chaton/a/a/c;->e:Z

    .line 33
    iput v1, p0, Lcom/sec/chaton/a/a/c;->f:I

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/a/a/c;->j:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/a/b;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/sec/chaton/a/a/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/a/a/b;-><init>(Lcom/sec/chaton/a/a/c;)V

    return-object v0
.end method

.method public a(I)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/chaton/a/a/c;->f:I

    .line 66
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/a/n;)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/chaton/a/a/c;->b:Lcom/sec/chaton/a/a/n;

    .line 51
    return-object p0
.end method

.method public a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/chaton/a/a/c;->i:Lcom/sec/chaton/e/w;

    .line 81
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/chaton/a/a/c;->d:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/sec/chaton/a/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/chaton/a/a/c;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/a/c;->j:Ljava/util/ArrayList;

    .line 86
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/chaton/a/a/c;->a:Z

    .line 41
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/chaton/a/a/c;->h:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method public b(Z)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/chaton/a/a/c;->e:Z

    .line 61
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/a/c;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/chaton/a/a/c;->g:Ljava/lang/String;

    .line 76
    return-object p0
.end method
