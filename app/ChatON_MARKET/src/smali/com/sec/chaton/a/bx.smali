.class public final enum Lcom/sec/chaton/a/bx;
.super Ljava/lang/Enum;
.source "SSMGPB.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/bx;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/bx;

.field public static final enum b:Lcom/sec/chaton/a/bx;

.field private static c:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sec/chaton/a/bx;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic e:[Lcom/sec/chaton/a/bx;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163
    new-instance v0, Lcom/sec/chaton/a/bx;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sec/chaton/a/bx;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bx;->a:Lcom/sec/chaton/a/bx;

    .line 164
    new-instance v0, Lcom/sec/chaton/a/bx;

    const-string v1, "GROUPCHAT_END"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sec/chaton/a/bx;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bx;->b:Lcom/sec/chaton/a/bx;

    .line 161
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/a/bx;

    sget-object v1, Lcom/sec/chaton/a/bx;->a:Lcom/sec/chaton/a/bx;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/bx;->b:Lcom/sec/chaton/a/bx;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/a/bx;->e:[Lcom/sec/chaton/a/bx;

    .line 186
    new-instance v0, Lcom/sec/chaton/a/by;

    invoke-direct {v0}, Lcom/sec/chaton/a/by;-><init>()V

    sput-object v0, Lcom/sec/chaton/a/bx;->c:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 196
    iput p4, p0, Lcom/sec/chaton/a/bx;->d:I

    .line 197
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/a/bx;
    .locals 1

    .prologue
    .line 174
    packed-switch p0, :pswitch_data_0

    .line 177
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 175
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/a/bx;->a:Lcom/sec/chaton/a/bx;

    goto :goto_0

    .line 176
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/a/bx;->b:Lcom/sec/chaton/a/bx;

    goto :goto_0

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/bx;
    .locals 1

    .prologue
    .line 161
    const-class v0, Lcom/sec/chaton/a/bx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bx;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/bx;
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lcom/sec/chaton/a/bx;->e:[Lcom/sec/chaton/a/bx;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/bx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/bx;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/sec/chaton/a/bx;->d:I

    return v0
.end method
