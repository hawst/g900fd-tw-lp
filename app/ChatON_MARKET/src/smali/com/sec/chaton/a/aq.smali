.class public final Lcom/sec/chaton/a/aq;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ap;",
        "Lcom/sec/chaton/a/aq;",
        ">;",
        "Lcom/sec/chaton/a/ar;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/bc;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/protobuf/LazyStringList;

.field private g:Lcom/sec/chaton/a/as;

.field private h:Lcom/google/protobuf/LazyStringList;

.field private i:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27753
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 27994
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->c:Lcom/sec/chaton/a/bc;

    .line 28018
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->d:Ljava/lang/Object;

    .line 28054
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->e:Ljava/lang/Object;

    .line 28090
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    .line 28146
    sget-object v0, Lcom/sec/chaton/a/as;->a:Lcom/sec/chaton/a/as;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->g:Lcom/sec/chaton/a/as;

    .line 28170
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    .line 27754
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->q()V

    .line 27755
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/aq;)Lcom/sec/chaton/a/ap;
    .locals 1

    .prologue
    .line 27748
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->s()Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p()Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 27748
    invoke-static {}, Lcom/sec/chaton/a/aq;->r()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 0

    .prologue
    .line 27758
    return-void
.end method

.method private static r()Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 27760
    new-instance v0, Lcom/sec/chaton/a/aq;

    invoke-direct {v0}, Lcom/sec/chaton/a/aq;-><init>()V

    return-object v0
.end method

.method private s()Lcom/sec/chaton/a/ap;
    .locals 2

    .prologue
    .line 27802
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->e()Lcom/sec/chaton/a/ap;

    move-result-object v0

    .line 27803
    invoke-virtual {v0}, Lcom/sec/chaton/a/ap;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 27804
    invoke-static {v0}, Lcom/sec/chaton/a/aq;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 27807
    :cond_0
    return-object v0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 28092
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 28093
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    .line 28094
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 28096
    :cond_0
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 28172
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    .line 28173
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    .line 28174
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 28176
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/aq;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 27764
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 27765
    iput-wide v1, p0, Lcom/sec/chaton/a/aq;->b:J

    .line 27766
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27767
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->c:Lcom/sec/chaton/a/bc;

    .line 27768
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27769
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->d:Ljava/lang/Object;

    .line 27770
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27771
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->e:Ljava/lang/Object;

    .line 27772
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27773
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    .line 27774
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27775
    sget-object v0, Lcom/sec/chaton/a/as;->a:Lcom/sec/chaton/a/as;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->g:Lcom/sec/chaton/a/as;

    .line 27776
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27777
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    .line 27778
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27779
    iput-wide v1, p0, Lcom/sec/chaton/a/aq;->i:J

    .line 27780
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27781
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 27981
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27982
    iput-wide p1, p0, Lcom/sec/chaton/a/aq;->b:J

    .line 27984
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/aq;
    .locals 2

    .prologue
    .line 27906
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 27907
    sparse-switch v0, :sswitch_data_0

    .line 27912
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/aq;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27914
    :sswitch_0
    return-object p0

    .line 27919
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27920
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/aq;->b:J

    goto :goto_0

    .line 27924
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 27925
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 27926
    if-eqz v0, :cond_0

    .line 27927
    iget v1, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27928
    iput-object v0, p0, Lcom/sec/chaton/a/aq;->c:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 27933
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27934
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->d:Ljava/lang/Object;

    goto :goto_0

    .line 27938
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27939
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->e:Ljava/lang/Object;

    goto :goto_0

    .line 27943
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->t()V

    .line 27944
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 27948
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 27949
    invoke-static {v0}, Lcom/sec/chaton/a/as;->a(I)Lcom/sec/chaton/a/as;

    move-result-object v0

    .line 27950
    if-eqz v0, :cond_0

    .line 27951
    iget v1, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27952
    iput-object v0, p0, Lcom/sec/chaton/a/aq;->g:Lcom/sec/chaton/a/as;

    goto :goto_0

    .line 27957
    :sswitch_7
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->u()V

    .line 27958
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 27962
    :sswitch_8
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27963
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/aq;->i:J

    goto/16 :goto_0

    .line 27907
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ap;)Lcom/sec/chaton/a/aq;
    .locals 2

    .prologue
    .line 27855
    invoke-static {}, Lcom/sec/chaton/a/ap;->a()Lcom/sec/chaton/a/ap;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 27894
    :cond_0
    :goto_0
    return-object p0

    .line 27856
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 27857
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/aq;->a(J)Lcom/sec/chaton/a/aq;

    .line 27859
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 27860
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->f()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/aq;

    .line 27862
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 27863
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/aq;->a(Ljava/lang/String;)Lcom/sec/chaton/a/aq;

    .line 27865
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 27866
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/aq;->b(Ljava/lang/String;)Lcom/sec/chaton/a/aq;

    .line 27868
    :cond_5
    invoke-static {p1}, Lcom/sec/chaton/a/ap;->b(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 27869
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 27870
    invoke-static {p1}, Lcom/sec/chaton/a/ap;->b(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    .line 27871
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27878
    :cond_6
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->l()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 27879
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->m()Lcom/sec/chaton/a/as;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/as;)Lcom/sec/chaton/a/aq;

    .line 27881
    :cond_7
    invoke-static {p1}, Lcom/sec/chaton/a/ap;->c(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 27882
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 27883
    invoke-static {p1}, Lcom/sec/chaton/a/ap;->c(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    .line 27884
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27891
    :cond_8
    :goto_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27892
    invoke-virtual {p1}, Lcom/sec/chaton/a/ap;->p()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/aq;->b(J)Lcom/sec/chaton/a/aq;

    goto/16 :goto_0

    .line 27873
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->t()V

    .line 27874
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/ap;->b(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 27886
    :cond_a
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->u()V

    .line 27887
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/ap;->c(Lcom/sec/chaton/a/ap;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public a(Lcom/sec/chaton/a/as;)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 28154
    if-nez p1, :cond_0

    .line 28155
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28157
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 28158
    iput-object p1, p0, Lcom/sec/chaton/a/aq;->g:Lcom/sec/chaton/a/as;

    .line 28160
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 28002
    if-nez p1, :cond_0

    .line 28003
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28005
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 28006
    iput-object p1, p0, Lcom/sec/chaton/a/aq;->c:Lcom/sec/chaton/a/bc;

    .line 28008
    return-object p0
.end method

.method public a(Ljava/lang/Iterable;)Lcom/sec/chaton/a/aq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/chaton/a/aq;"
        }
    .end annotation

    .prologue
    .line 28208
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->u()V

    .line 28209
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 28211
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 28033
    if-nez p1, :cond_0

    .line 28034
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28036
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 28037
    iput-object p1, p0, Lcom/sec/chaton/a/aq;->d:Ljava/lang/Object;

    .line 28039
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/aq;
    .locals 2

    .prologue
    .line 27785
    invoke-static {}, Lcom/sec/chaton/a/aq;->r()Lcom/sec/chaton/a/aq;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->e()Lcom/sec/chaton/a/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/ap;)Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 28234
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 28235
    iput-wide p1, p0, Lcom/sec/chaton/a/aq;->i:J

    .line 28237
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 28069
    if-nez p1, :cond_0

    .line 28070
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28072
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/aq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 28073
    iput-object p1, p0, Lcom/sec/chaton/a/aq;->e:Ljava/lang/Object;

    .line 28075
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->d()Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->e()Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ap;
    .locals 1

    .prologue
    .line 27789
    invoke-static {}, Lcom/sec/chaton/a/ap;->a()Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/aq;
    .locals 1

    .prologue
    .line 28118
    if-nez p1, :cond_0

    .line 28119
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28121
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/aq;->t()V

    .line 28122
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 28124
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->a()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->a()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->b()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->b()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->b()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->b()Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ap;
    .locals 2

    .prologue
    .line 27793
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->e()Lcom/sec/chaton/a/ap;

    move-result-object v0

    .line 27794
    invoke-virtual {v0}, Lcom/sec/chaton/a/ap;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 27795
    invoke-static {v0}, Lcom/sec/chaton/a/aq;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 27797
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ap;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 27811
    new-instance v2, Lcom/sec/chaton/a/ap;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ap;-><init>(Lcom/sec/chaton/a/aq;Lcom/sec/chaton/a/b;)V

    .line 27812
    iget v3, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27813
    const/4 v1, 0x0

    .line 27814
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 27817
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/aq;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/ap;->a(Lcom/sec/chaton/a/ap;J)J

    .line 27818
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 27819
    or-int/lit8 v0, v0, 0x2

    .line 27821
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/aq;->c:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ap;->a(Lcom/sec/chaton/a/ap;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 27822
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 27823
    or-int/lit8 v0, v0, 0x4

    .line 27825
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/aq;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ap;->a(Lcom/sec/chaton/a/ap;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27826
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 27827
    or-int/lit8 v0, v0, 0x8

    .line 27829
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/aq;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ap;->b(Lcom/sec/chaton/a/ap;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27830
    iget v1, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 27831
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    .line 27833
    iget v1, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27835
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ap;->a(Lcom/sec/chaton/a/ap;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 27836
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 27837
    or-int/lit8 v0, v0, 0x10

    .line 27839
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/aq;->g:Lcom/sec/chaton/a/as;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ap;->a(Lcom/sec/chaton/a/ap;Lcom/sec/chaton/a/as;)Lcom/sec/chaton/a/as;

    .line 27840
    iget v1, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 27841
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    .line 27843
    iget v1, p0, Lcom/sec/chaton/a/aq;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/sec/chaton/a/aq;->a:I

    .line 27845
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ap;->b(Lcom/sec/chaton/a/ap;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 27846
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 27847
    or-int/lit8 v0, v0, 0x20

    .line 27849
    :cond_6
    iget-wide v3, p0, Lcom/sec/chaton/a/aq;->i:J

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/a/ap;->b(Lcom/sec/chaton/a/ap;J)J

    .line 27850
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ap;->a(Lcom/sec/chaton/a/ap;I)I

    .line 27851
    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 27978
    iget-wide v0, p0, Lcom/sec/chaton/a/aq;->b:J

    return-wide v0
.end method

.method public g()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 27999
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->c:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->c()Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0}, Lcom/sec/chaton/a/aq;->c()Lcom/sec/chaton/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28023
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->d:Ljava/lang/Object;

    .line 28024
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 28025
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 28026
    iput-object v0, p0, Lcom/sec/chaton/a/aq;->d:Ljava/lang/Object;

    .line 28029
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28059
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->e:Ljava/lang/Object;

    .line 28060
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 28061
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 28062
    iput-object v0, p0, Lcom/sec/chaton/a/aq;->e:Ljava/lang/Object;

    .line 28065
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 27898
    const/4 v0, 0x1

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28099
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 28102
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public l()Lcom/sec/chaton/a/as;
    .locals 1

    .prologue
    .line 28151
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->g:Lcom/sec/chaton/a/as;

    return-object v0
.end method

.method public m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28179
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/aq;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    check-cast p1, Lcom/sec/chaton/a/ap;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/aq;->a(Lcom/sec/chaton/a/ap;)Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27748
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/aq;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 28182
    iget-object v0, p0, Lcom/sec/chaton/a/aq;->h:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public o()J
    .locals 2

    .prologue
    .line 28231
    iget-wide v0, p0, Lcom/sec/chaton/a/aq;->i:J

    return-wide v0
.end method
