.class public final Lcom/sec/chaton/a/y;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/x;",
        "Lcom/sec/chaton/a/y;",
        ">;",
        "Lcom/sec/chaton/a/z;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5542
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 5652
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/y;->b:Ljava/lang/Object;

    .line 5543
    invoke-direct {p0}, Lcom/sec/chaton/a/y;->g()V

    .line 5544
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5537
    invoke-static {}, Lcom/sec/chaton/a/y;->h()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 5547
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5549
    new-instance v0, Lcom/sec/chaton/a/y;

    invoke-direct {v0}, Lcom/sec/chaton/a/y;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/y;
    .locals 2

    .prologue
    .line 5553
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 5554
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/y;->b:Ljava/lang/Object;

    .line 5555
    iget v0, p0, Lcom/sec/chaton/a/y;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/y;->a:I

    .line 5556
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/y;->c:J

    .line 5557
    iget v0, p0, Lcom/sec/chaton/a/y;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/y;->a:I

    .line 5558
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5696
    iget v0, p0, Lcom/sec/chaton/a/y;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/y;->a:I

    .line 5697
    iput-wide p1, p0, Lcom/sec/chaton/a/y;->c:J

    .line 5699
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/y;
    .locals 2

    .prologue
    .line 5623
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 5624
    sparse-switch v0, :sswitch_data_0

    .line 5629
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/y;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5631
    :sswitch_0
    return-object p0

    .line 5636
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/y;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/y;->a:I

    .line 5637
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/y;->b:Ljava/lang/Object;

    goto :goto_0

    .line 5641
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/y;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/y;->a:I

    .line 5642
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/y;->c:J

    goto :goto_0

    .line 5624
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/y;
    .locals 2

    .prologue
    .line 5604
    invoke-static {}, Lcom/sec/chaton/a/x;->a()Lcom/sec/chaton/a/x;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 5611
    :cond_0
    :goto_0
    return-object p0

    .line 5605
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/x;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5606
    invoke-virtual {p1}, Lcom/sec/chaton/a/x;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/y;->a(Ljava/lang/String;)Lcom/sec/chaton/a/y;

    .line 5608
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/x;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5609
    invoke-virtual {p1}, Lcom/sec/chaton/a/x;->f()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/y;->a(J)Lcom/sec/chaton/a/y;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5667
    if-nez p1, :cond_0

    .line 5668
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5670
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/y;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/y;->a:I

    .line 5671
    iput-object p1, p0, Lcom/sec/chaton/a/y;->b:Ljava/lang/Object;

    .line 5673
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/y;
    .locals 2

    .prologue
    .line 5562
    invoke-static {}, Lcom/sec/chaton/a/y;->h()Lcom/sec/chaton/a/y;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->e()Lcom/sec/chaton/a/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/y;->a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->d()Lcom/sec/chaton/a/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->e()Lcom/sec/chaton/a/x;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/x;
    .locals 1

    .prologue
    .line 5566
    invoke-static {}, Lcom/sec/chaton/a/x;->a()Lcom/sec/chaton/a/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->a()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->a()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->b()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->b()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->b()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->b()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/x;
    .locals 2

    .prologue
    .line 5570
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->e()Lcom/sec/chaton/a/x;

    move-result-object v0

    .line 5571
    invoke-virtual {v0}, Lcom/sec/chaton/a/x;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5572
    invoke-static {v0}, Lcom/sec/chaton/a/y;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5574
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/x;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5588
    new-instance v2, Lcom/sec/chaton/a/x;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/x;-><init>(Lcom/sec/chaton/a/y;Lcom/sec/chaton/a/b;)V

    .line 5589
    iget v3, p0, Lcom/sec/chaton/a/y;->a:I

    .line 5590
    const/4 v1, 0x0

    .line 5591
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 5594
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/y;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/x;->a(Lcom/sec/chaton/a/x;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5595
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 5596
    or-int/lit8 v0, v0, 0x2

    .line 5598
    :cond_0
    iget-wide v3, p0, Lcom/sec/chaton/a/y;->c:J

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/a/x;->a(Lcom/sec/chaton/a/x;J)J

    .line 5599
    invoke-static {v2, v0}, Lcom/sec/chaton/a/x;->a(Lcom/sec/chaton/a/x;I)I

    .line 5600
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->c()Lcom/sec/chaton/a/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0}, Lcom/sec/chaton/a/y;->c()Lcom/sec/chaton/a/x;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 5615
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/y;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    check-cast p1, Lcom/sec/chaton/a/x;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/y;->a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5537
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/y;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method
