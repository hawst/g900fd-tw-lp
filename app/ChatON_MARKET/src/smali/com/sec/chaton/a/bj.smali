.class public final Lcom/sec/chaton/a/bj;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bo;


# static fields
.field private static final a:Lcom/sec/chaton/a/bj;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/bl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/a/ej;

.field private f:I

.field private g:J

.field private h:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14942
    new-instance v0, Lcom/sec/chaton/a/bj;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/bj;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/bj;->a:Lcom/sec/chaton/a/bj;

    .line 14943
    sget-object v0, Lcom/sec/chaton/a/bj;->a:Lcom/sec/chaton/a/bj;

    invoke-direct {v0}, Lcom/sec/chaton/a/bj;->s()V

    .line 14944
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/bk;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14225
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 14362
    iput-byte v0, p0, Lcom/sec/chaton/a/bj;->i:B

    .line 14394
    iput v0, p0, Lcom/sec/chaton/a/bj;->j:I

    .line 14226
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/bk;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 14220
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/bj;-><init>(Lcom/sec/chaton/a/bk;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14227
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 14362
    iput-byte v0, p0, Lcom/sec/chaton/a/bj;->i:B

    .line 14394
    iput v0, p0, Lcom/sec/chaton/a/bj;->j:I

    .line 14227
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bj;I)I
    .locals 0

    .prologue
    .line 14220
    iput p1, p0, Lcom/sec/chaton/a/bj;->f:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bj;J)J
    .locals 0

    .prologue
    .line 14220
    iput-wide p1, p0, Lcom/sec/chaton/a/bj;->g:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/bj;
    .locals 1

    .prologue
    .line 14231
    sget-object v0, Lcom/sec/chaton/a/bj;->a:Lcom/sec/chaton/a/bj;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/bj;
    .locals 1

    .prologue
    .line 14449
    invoke-static {}, Lcom/sec/chaton/a/bj;->newBuilder()Lcom/sec/chaton/a/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bk;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bk;

    invoke-static {v0}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/bk;)Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/bj;)Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14505
    invoke-static {}, Lcom/sec/chaton/a/bj;->newBuilder()Lcom/sec/chaton/a/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bk;->a(Lcom/sec/chaton/a/bj;)Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/bj;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 14220
    iput-object p1, p0, Lcom/sec/chaton/a/bj;->e:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 14220
    iput-object p1, p0, Lcom/sec/chaton/a/bj;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bj;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 14220
    iput-object p1, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/bj;I)I
    .locals 0

    .prologue
    .line 14220
    iput p1, p0, Lcom/sec/chaton/a/bj;->b:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/bj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 14220
    iput-object p1, p0, Lcom/sec/chaton/a/bj;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/bj;)Ljava/util/List;
    .locals 1

    .prologue
    .line 14220
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    return-object v0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14502
    invoke-static {}, Lcom/sec/chaton/a/bk;->k()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14260
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->c:Ljava/lang/Object;

    .line 14261
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14262
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14264
    iput-object v0, p0, Lcom/sec/chaton/a/bj;->c:Ljava/lang/Object;

    .line 14267
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14343
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->h:Ljava/lang/Object;

    .line 14344
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14345
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14347
    iput-object v0, p0, Lcom/sec/chaton/a/bj;->h:Ljava/lang/Object;

    .line 14350
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 14355
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bj;->c:Ljava/lang/Object;

    .line 14356
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    .line 14357
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bj;->e:Lcom/sec/chaton/a/ej;

    .line 14358
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/bj;->f:I

    .line 14359
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bj;->g:J

    .line 14360
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bj;->h:Ljava/lang/Object;

    .line 14361
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/a/bl;
    .locals 1

    .prologue
    .line 14285
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bl;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/bj;
    .locals 1

    .prologue
    .line 14235
    sget-object v0, Lcom/sec/chaton/a/bj;->a:Lcom/sec/chaton/a/bj;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 14243
    iget v1, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14246
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->c:Ljava/lang/Object;

    .line 14247
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14248
    check-cast v0, Ljava/lang/String;

    .line 14256
    :goto_0
    return-object v0

    .line 14250
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14252
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14253
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14254
    iput-object v1, p0, Lcom/sec/chaton/a/bj;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14256
    goto :goto_0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/bl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14275
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 14282
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 14296
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14220
    invoke-virtual {p0}, Lcom/sec/chaton/a/bj;->b()Lcom/sec/chaton/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 14396
    iget v2, p0, Lcom/sec/chaton/a/bj;->j:I

    .line 14397
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 14425
    :goto_0
    return v2

    .line 14400
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 14401
    invoke-direct {p0}, Lcom/sec/chaton/a/bj;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v0

    .line 14404
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 14405
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 14404
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 14408
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 14409
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/bj;->e:Lcom/sec/chaton/a/ej;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 14412
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 14413
    iget v0, p0, Lcom/sec/chaton/a/bj;->f:I

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 14416
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 14417
    const/4 v0, 0x5

    iget-wide v3, p0, Lcom/sec/chaton/a/bj;->g:J

    invoke-static {v0, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v2, v0

    .line 14420
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 14421
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/bj;->r()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 14424
    :cond_5
    iput v2, p0, Lcom/sec/chaton/a/bj;->j:I

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public h()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 14299
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->e:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 14306
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 14364
    iget-byte v1, p0, Lcom/sec/chaton/a/bj;->i:B

    .line 14365
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 14368
    :goto_0
    return v0

    .line 14365
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 14367
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/bj;->i:B

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 14309
    iget v0, p0, Lcom/sec/chaton/a/bj;->f:I

    return v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 14316
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 14319
    iget-wide v0, p0, Lcom/sec/chaton/a/bj;->g:J

    return-wide v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 14326
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14329
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->h:Ljava/lang/Object;

    .line 14330
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14331
    check-cast v0, Ljava/lang/String;

    .line 14339
    :goto_0
    return-object v0

    .line 14333
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14335
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14336
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14337
    iput-object v1, p0, Lcom/sec/chaton/a/bj;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14339
    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14220
    invoke-virtual {p0}, Lcom/sec/chaton/a/bj;->o()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14503
    invoke-static {}, Lcom/sec/chaton/a/bj;->newBuilder()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/sec/chaton/a/bk;
    .locals 1

    .prologue
    .line 14507
    invoke-static {p0}, Lcom/sec/chaton/a/bj;->a(Lcom/sec/chaton/a/bj;)Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14220
    invoke-virtual {p0}, Lcom/sec/chaton/a/bj;->p()Lcom/sec/chaton/a/bk;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14432
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 14373
    invoke-virtual {p0}, Lcom/sec/chaton/a/bj;->getSerializedSize()I

    .line 14374
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 14375
    invoke-direct {p0}, Lcom/sec/chaton/a/bj;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 14377
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 14378
    iget-object v0, p0, Lcom/sec/chaton/a/bj;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 14377
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 14380
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 14381
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/bj;->e:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 14383
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 14384
    iget v0, p0, Lcom/sec/chaton/a/bj;->f:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 14386
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 14387
    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sec/chaton/a/bj;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 14389
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/bj;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 14390
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/bj;->r()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 14392
    :cond_5
    return-void
.end method
