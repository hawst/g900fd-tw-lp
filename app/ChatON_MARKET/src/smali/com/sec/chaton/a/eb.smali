.class public final Lcom/sec/chaton/a/eb;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ec;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ea;",
        "Lcom/sec/chaton/a/eb;",
        ">;",
        "Lcom/sec/chaton/a/ec;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1542
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 1645
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    .line 1543
    invoke-direct {p0}, Lcom/sec/chaton/a/eb;->g()V

    .line 1544
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1537
    invoke-static {}, Lcom/sec/chaton/a/eb;->h()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 1547
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1549
    new-instance v0, Lcom/sec/chaton/a/eb;

    invoke-direct {v0}, Lcom/sec/chaton/a/eb;-><init>()V

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1648
    iget v0, p0, Lcom/sec/chaton/a/eb;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1649
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    .line 1650
    iget v0, p0, Lcom/sec/chaton/a/eb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/eb;->a:I

    .line 1652
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1553
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1554
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    .line 1555
    iget v0, p0, Lcom/sec/chaton/a/eb;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/eb;->a:I

    .line 1556
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1620
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1621
    sparse-switch v0, :sswitch_data_0

    .line 1626
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/eb;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1628
    :sswitch_0
    return-object p0

    .line 1633
    :sswitch_1
    invoke-static {}, Lcom/sec/chaton/a/dm;->newBuilder()Lcom/sec/chaton/a/dn;

    move-result-object v0

    .line 1634
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 1635
    invoke-virtual {v0}, Lcom/sec/chaton/a/dn;->e()Lcom/sec/chaton/a/dm;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/eb;->a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/eb;

    goto :goto_0

    .line 1621
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/eb;
    .locals 1

    .prologue
    .line 1681
    if-nez p1, :cond_0

    .line 1682
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1684
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/eb;->i()V

    .line 1685
    iget-object v0, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;
    .locals 2

    .prologue
    .line 1597
    invoke-static {}, Lcom/sec/chaton/a/ea;->a()Lcom/sec/chaton/a/ea;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1608
    :cond_0
    :goto_0
    return-object p0

    .line 1598
    :cond_1
    invoke-static {p1}, Lcom/sec/chaton/a/ea;->b(Lcom/sec/chaton/a/ea;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1599
    iget-object v0, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1600
    invoke-static {p1}, Lcom/sec/chaton/a/ea;->b(Lcom/sec/chaton/a/ea;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    .line 1601
    iget v0, p0, Lcom/sec/chaton/a/eb;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/eb;->a:I

    goto :goto_0

    .line 1603
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/a/eb;->i()V

    .line 1604
    iget-object v0, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/ea;->b(Lcom/sec/chaton/a/ea;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public b()Lcom/sec/chaton/a/eb;
    .locals 2

    .prologue
    .line 1560
    invoke-static {}, Lcom/sec/chaton/a/eb;->h()Lcom/sec/chaton/a/eb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->e()Lcom/sec/chaton/a/ea;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eb;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->d()Lcom/sec/chaton/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->e()Lcom/sec/chaton/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ea;
    .locals 1

    .prologue
    .line 1564
    invoke-static {}, Lcom/sec/chaton/a/ea;->a()Lcom/sec/chaton/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->a()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->a()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->b()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->b()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->b()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->b()Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ea;
    .locals 2

    .prologue
    .line 1568
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->e()Lcom/sec/chaton/a/ea;

    move-result-object v0

    .line 1569
    invoke-virtual {v0}, Lcom/sec/chaton/a/ea;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1570
    invoke-static {v0}, Lcom/sec/chaton/a/eb;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1572
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ea;
    .locals 3

    .prologue
    .line 1586
    new-instance v0, Lcom/sec/chaton/a/ea;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/a/ea;-><init>(Lcom/sec/chaton/a/eb;Lcom/sec/chaton/a/b;)V

    .line 1587
    iget v1, p0, Lcom/sec/chaton/a/eb;->a:I

    .line 1588
    iget v1, p0, Lcom/sec/chaton/a/eb;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1589
    iget-object v1, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    .line 1590
    iget v1, p0, Lcom/sec/chaton/a/eb;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sec/chaton/a/eb;->a:I

    .line 1592
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/eb;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/sec/chaton/a/ea;->a(Lcom/sec/chaton/a/ea;Ljava/util/List;)Ljava/util/List;

    .line 1593
    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->c()Lcom/sec/chaton/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/sec/chaton/a/eb;->c()Lcom/sec/chaton/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1612
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/eb;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    check-cast p1, Lcom/sec/chaton/a/ea;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/eb;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/eb;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    return-object v0
.end method
