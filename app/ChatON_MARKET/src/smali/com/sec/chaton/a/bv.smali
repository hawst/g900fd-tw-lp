.class public final Lcom/sec/chaton/a/bv;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/bu;",
        "Lcom/sec/chaton/a/bv;",
        ">;",
        "Lcom/sec/chaton/a/bw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/bx;

.field private d:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 18491
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 18640
    sget-object v0, Lcom/sec/chaton/a/bx;->a:Lcom/sec/chaton/a/bx;

    iput-object v0, p0, Lcom/sec/chaton/a/bv;->c:Lcom/sec/chaton/a/bx;

    .line 18664
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bv;->d:Ljava/lang/Object;

    .line 18492
    invoke-direct {p0}, Lcom/sec/chaton/a/bv;->g()V

    .line 18493
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bv;)Lcom/sec/chaton/a/bu;
    .locals 1

    .prologue
    .line 18486
    invoke-direct {p0}, Lcom/sec/chaton/a/bv;->i()Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18486
    invoke-static {}, Lcom/sec/chaton/a/bv;->h()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 18496
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18498
    new-instance v0, Lcom/sec/chaton/a/bv;

    invoke-direct {v0}, Lcom/sec/chaton/a/bv;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/bu;
    .locals 2

    .prologue
    .line 18530
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->e()Lcom/sec/chaton/a/bu;

    move-result-object v0

    .line 18531
    invoke-virtual {v0}, Lcom/sec/chaton/a/bu;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18532
    invoke-static {v0}, Lcom/sec/chaton/a/bv;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 18535
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/bv;
    .locals 2

    .prologue
    .line 18502
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 18503
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bv;->b:J

    .line 18504
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18505
    sget-object v0, Lcom/sec/chaton/a/bx;->a:Lcom/sec/chaton/a/bx;

    iput-object v0, p0, Lcom/sec/chaton/a/bv;->c:Lcom/sec/chaton/a/bx;

    .line 18506
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18507
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bv;->d:Ljava/lang/Object;

    .line 18508
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18509
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18627
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18628
    iput-wide p1, p0, Lcom/sec/chaton/a/bv;->b:J

    .line 18630
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bv;
    .locals 2

    .prologue
    .line 18581
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 18582
    sparse-switch v0, :sswitch_data_0

    .line 18587
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/bv;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18589
    :sswitch_0
    return-object p0

    .line 18594
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18595
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/bv;->b:J

    goto :goto_0

    .line 18599
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 18600
    invoke-static {v0}, Lcom/sec/chaton/a/bx;->a(I)Lcom/sec/chaton/a/bx;

    move-result-object v0

    .line 18601
    if-eqz v0, :cond_0

    .line 18602
    iget v1, p0, Lcom/sec/chaton/a/bv;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18603
    iput-object v0, p0, Lcom/sec/chaton/a/bv;->c:Lcom/sec/chaton/a/bx;

    goto :goto_0

    .line 18608
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18609
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bv;->d:Ljava/lang/Object;

    goto :goto_0

    .line 18582
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bu;)Lcom/sec/chaton/a/bv;
    .locals 2

    .prologue
    .line 18559
    invoke-static {}, Lcom/sec/chaton/a/bu;->a()Lcom/sec/chaton/a/bu;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 18569
    :cond_0
    :goto_0
    return-object p0

    .line 18560
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/bu;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 18561
    invoke-virtual {p1}, Lcom/sec/chaton/a/bu;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/bv;->a(J)Lcom/sec/chaton/a/bv;

    .line 18563
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/bu;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18564
    invoke-virtual {p1}, Lcom/sec/chaton/a/bu;->f()Lcom/sec/chaton/a/bx;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bv;->a(Lcom/sec/chaton/a/bx;)Lcom/sec/chaton/a/bv;

    .line 18566
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/bu;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18567
    invoke-virtual {p1}, Lcom/sec/chaton/a/bu;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bv;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/bx;)Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18648
    if-nez p1, :cond_0

    .line 18649
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18651
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18652
    iput-object p1, p0, Lcom/sec/chaton/a/bv;->c:Lcom/sec/chaton/a/bx;

    .line 18654
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18679
    if-nez p1, :cond_0

    .line 18680
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18682
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18683
    iput-object p1, p0, Lcom/sec/chaton/a/bv;->d:Ljava/lang/Object;

    .line 18685
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/bv;
    .locals 2

    .prologue
    .line 18513
    invoke-static {}, Lcom/sec/chaton/a/bv;->h()Lcom/sec/chaton/a/bv;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->e()Lcom/sec/chaton/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bv;->a(Lcom/sec/chaton/a/bu;)Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->d()Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->e()Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/bu;
    .locals 1

    .prologue
    .line 18517
    invoke-static {}, Lcom/sec/chaton/a/bu;->a()Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->a()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->a()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->b()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->b()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->b()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->b()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/bu;
    .locals 2

    .prologue
    .line 18521
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->e()Lcom/sec/chaton/a/bu;

    move-result-object v0

    .line 18522
    invoke-virtual {v0}, Lcom/sec/chaton/a/bu;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18523
    invoke-static {v0}, Lcom/sec/chaton/a/bv;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 18525
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/bu;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 18539
    new-instance v2, Lcom/sec/chaton/a/bu;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/bu;-><init>(Lcom/sec/chaton/a/bv;Lcom/sec/chaton/a/b;)V

    .line 18540
    iget v3, p0, Lcom/sec/chaton/a/bv;->a:I

    .line 18541
    const/4 v1, 0x0

    .line 18542
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 18545
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/bv;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/bu;->a(Lcom/sec/chaton/a/bu;J)J

    .line 18546
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 18547
    or-int/lit8 v0, v0, 0x2

    .line 18549
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/bv;->c:Lcom/sec/chaton/a/bx;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bu;->a(Lcom/sec/chaton/a/bu;Lcom/sec/chaton/a/bx;)Lcom/sec/chaton/a/bx;

    .line 18550
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 18551
    or-int/lit8 v0, v0, 0x4

    .line 18553
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/bv;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bu;->a(Lcom/sec/chaton/a/bu;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18554
    invoke-static {v2, v0}, Lcom/sec/chaton/a/bu;->a(Lcom/sec/chaton/a/bu;I)I

    .line 18555
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->c()Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0}, Lcom/sec/chaton/a/bv;->c()Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 18573
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bv;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    check-cast p1, Lcom/sec/chaton/a/bu;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/bv;->a(Lcom/sec/chaton/a/bu;)Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18486
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bv;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method
