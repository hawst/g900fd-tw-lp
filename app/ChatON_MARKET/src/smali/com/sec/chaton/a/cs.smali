.class public final Lcom/sec/chaton/a/cs;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/cr;",
        "Lcom/sec/chaton/a/cs;",
        ">;",
        "Lcom/sec/chaton/a/ct;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/protobuf/LazyStringList;

.field private d:J

.field private e:Ljava/lang/Object;

.field private f:J

.field private g:Lcom/sec/chaton/a/dp;

.field private h:Lcom/sec/chaton/a/bc;

.field private i:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3198
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 3409
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->b:Ljava/lang/Object;

    .line 3445
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    .line 3522
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->e:Ljava/lang/Object;

    .line 3579
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->g:Lcom/sec/chaton/a/dp;

    .line 3603
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->h:Lcom/sec/chaton/a/bc;

    .line 3627
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->i:Ljava/lang/Object;

    .line 3199
    invoke-direct {p0}, Lcom/sec/chaton/a/cs;->g()V

    .line 3200
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3193
    invoke-static {}, Lcom/sec/chaton/a/cs;->h()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 3203
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3205
    new-instance v0, Lcom/sec/chaton/a/cs;

    invoke-direct {v0}, Lcom/sec/chaton/a/cs;-><init>()V

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 3447
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 3448
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    .line 3449
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3451
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/cs;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 3209
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3210
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->b:Ljava/lang/Object;

    .line 3211
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3212
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    .line 3213
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3214
    iput-wide v1, p0, Lcom/sec/chaton/a/cs;->d:J

    .line 3215
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3216
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->e:Ljava/lang/Object;

    .line 3217
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3218
    iput-wide v1, p0, Lcom/sec/chaton/a/cs;->f:J

    .line 3219
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3220
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->g:Lcom/sec/chaton/a/dp;

    .line 3221
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3222
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->h:Lcom/sec/chaton/a/bc;

    .line 3223
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3224
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->i:Ljava/lang/Object;

    .line 3225
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3226
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3509
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3510
    iput-wide p1, p0, Lcom/sec/chaton/a/cs;->d:J

    .line 3512
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cs;
    .locals 2

    .prologue
    .line 3342
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3343
    sparse-switch v0, :sswitch_data_0

    .line 3348
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/cs;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3350
    :sswitch_0
    return-object p0

    .line 3355
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3356
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->b:Ljava/lang/Object;

    goto :goto_0

    .line 3360
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/a/cs;->i()V

    .line 3361
    iget-object v0, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 3365
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3366
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/cs;->d:J

    goto :goto_0

    .line 3370
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3371
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->e:Ljava/lang/Object;

    goto :goto_0

    .line 3375
    :sswitch_5
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3376
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/cs;->f:J

    goto :goto_0

    .line 3380
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 3381
    invoke-static {v0}, Lcom/sec/chaton/a/dp;->a(I)Lcom/sec/chaton/a/dp;

    move-result-object v0

    .line 3382
    if-eqz v0, :cond_0

    .line 3383
    iget v1, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3384
    iput-object v0, p0, Lcom/sec/chaton/a/cs;->g:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 3389
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 3390
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 3391
    if-eqz v0, :cond_0

    .line 3392
    iget v1, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3393
    iput-object v0, p0, Lcom/sec/chaton/a/cs;->h:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 3398
    :sswitch_8
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3399
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 3343
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3611
    if-nez p1, :cond_0

    .line 3612
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3614
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3615
    iput-object p1, p0, Lcom/sec/chaton/a/cs;->h:Lcom/sec/chaton/a/bc;

    .line 3617
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;
    .locals 2

    .prologue
    .line 3298
    invoke-static {}, Lcom/sec/chaton/a/cr;->a()Lcom/sec/chaton/a/cr;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3330
    :cond_0
    :goto_0
    return-object p0

    .line 3299
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3300
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cs;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cs;

    .line 3302
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/a/cr;->b(Lcom/sec/chaton/a/cr;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3303
    iget-object v0, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3304
    invoke-static {p1}, Lcom/sec/chaton/a/cr;->b(Lcom/sec/chaton/a/cr;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    .line 3305
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3312
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3313
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/cs;->a(J)Lcom/sec/chaton/a/cs;

    .line 3315
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3316
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cs;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cs;

    .line 3318
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3319
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/cs;->b(J)Lcom/sec/chaton/a/cs;

    .line 3321
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3322
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cs;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cs;

    .line 3324
    :cond_7
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3325
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->p()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cs;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cs;

    .line 3327
    :cond_8
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3328
    invoke-virtual {p1}, Lcom/sec/chaton/a/cr;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cs;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cs;

    goto :goto_0

    .line 3307
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/a/cs;->i()V

    .line 3308
    iget-object v0, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/cr;->b(Lcom/sec/chaton/a/cr;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3587
    if-nez p1, :cond_0

    .line 3588
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3590
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3591
    iput-object p1, p0, Lcom/sec/chaton/a/cs;->g:Lcom/sec/chaton/a/dp;

    .line 3593
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3424
    if-nez p1, :cond_0

    .line 3425
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3427
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3428
    iput-object p1, p0, Lcom/sec/chaton/a/cs;->b:Ljava/lang/Object;

    .line 3430
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/cs;
    .locals 2

    .prologue
    .line 3230
    invoke-static {}, Lcom/sec/chaton/a/cs;->h()Lcom/sec/chaton/a/cs;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->e()Lcom/sec/chaton/a/cr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cs;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3566
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3567
    iput-wide p1, p0, Lcom/sec/chaton/a/cs;->f:J

    .line 3569
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3537
    if-nez p1, :cond_0

    .line 3538
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3540
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3541
    iput-object p1, p0, Lcom/sec/chaton/a/cs;->e:Ljava/lang/Object;

    .line 3543
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->d()Lcom/sec/chaton/a/cr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->e()Lcom/sec/chaton/a/cr;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/cr;
    .locals 1

    .prologue
    .line 3234
    invoke-static {}, Lcom/sec/chaton/a/cr;->a()Lcom/sec/chaton/a/cr;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3642
    if-nez p1, :cond_0

    .line 3643
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3645
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cs;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3646
    iput-object p1, p0, Lcom/sec/chaton/a/cs;->i:Ljava/lang/Object;

    .line 3648
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->a()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->a()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->b()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->b()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->b()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->b()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/cr;
    .locals 2

    .prologue
    .line 3238
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->e()Lcom/sec/chaton/a/cr;

    move-result-object v0

    .line 3239
    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3240
    invoke-static {v0}, Lcom/sec/chaton/a/cs;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3242
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/cr;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 3256
    new-instance v2, Lcom/sec/chaton/a/cr;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/cr;-><init>(Lcom/sec/chaton/a/cs;Lcom/sec/chaton/a/b;)V

    .line 3257
    iget v3, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3258
    const/4 v1, 0x0

    .line 3259
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 3262
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/cs;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3263
    iget v1, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 3264
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    .line 3266
    iget v1, p0, Lcom/sec/chaton/a/cs;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/chaton/a/cs;->a:I

    .line 3268
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/cs;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 3269
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 3270
    or-int/lit8 v0, v0, 0x2

    .line 3272
    :cond_1
    iget-wide v4, p0, Lcom/sec/chaton/a/cs;->d:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;J)J

    .line 3273
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 3274
    or-int/lit8 v0, v0, 0x4

    .line 3276
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/cs;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cr;->b(Lcom/sec/chaton/a/cr;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3277
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 3278
    or-int/lit8 v0, v0, 0x8

    .line 3280
    :cond_3
    iget-wide v4, p0, Lcom/sec/chaton/a/cs;->f:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/cr;->b(Lcom/sec/chaton/a/cr;J)J

    .line 3281
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 3282
    or-int/lit8 v0, v0, 0x10

    .line 3284
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/cs;->g:Lcom/sec/chaton/a/dp;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;

    .line 3285
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 3286
    or-int/lit8 v0, v0, 0x20

    .line 3288
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/a/cs;->h:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 3289
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 3290
    or-int/lit8 v0, v0, 0x40

    .line 3292
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/a/cs;->i:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cr;->c(Lcom/sec/chaton/a/cr;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3293
    invoke-static {v2, v0}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;I)I

    .line 3294
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->c()Lcom/sec/chaton/a/cr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0}, Lcom/sec/chaton/a/cs;->c()Lcom/sec/chaton/a/cr;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 3334
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cs;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    check-cast p1, Lcom/sec/chaton/a/cr;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/cs;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3193
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cs;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method
