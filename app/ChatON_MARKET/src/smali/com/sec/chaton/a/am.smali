.class public final Lcom/sec/chaton/a/am;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ao;


# static fields
.field private static final a:Lcom/sec/chaton/a/am;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;

.field private e:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28673
    new-instance v0, Lcom/sec/chaton/a/am;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/am;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/am;->a:Lcom/sec/chaton/a/am;

    .line 28674
    sget-object v0, Lcom/sec/chaton/a/am;->a:Lcom/sec/chaton/a/am;

    invoke-direct {v0}, Lcom/sec/chaton/a/am;->k()V

    .line 28675
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/an;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 28277
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 28326
    iput-byte v0, p0, Lcom/sec/chaton/a/am;->f:B

    .line 28349
    iput v0, p0, Lcom/sec/chaton/a/am;->g:I

    .line 28278
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/an;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 28272
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/am;-><init>(Lcom/sec/chaton/a/an;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 28279
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 28326
    iput-byte v0, p0, Lcom/sec/chaton/a/am;->f:B

    .line 28349
    iput v0, p0, Lcom/sec/chaton/a/am;->g:I

    .line 28279
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/am;I)I
    .locals 0

    .prologue
    .line 28272
    iput p1, p0, Lcom/sec/chaton/a/am;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/am;J)J
    .locals 0

    .prologue
    .line 28272
    iput-wide p1, p0, Lcom/sec/chaton/a/am;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/am;
    .locals 1

    .prologue
    .line 28283
    sget-object v0, Lcom/sec/chaton/a/am;->a:Lcom/sec/chaton/a/am;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/am;
    .locals 1

    .prologue
    .line 28392
    invoke-static {}, Lcom/sec/chaton/a/am;->newBuilder()Lcom/sec/chaton/a/an;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/an;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/an;

    invoke-static {v0}, Lcom/sec/chaton/a/an;->a(Lcom/sec/chaton/a/an;)Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/am;)Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28448
    invoke-static {}, Lcom/sec/chaton/a/am;->newBuilder()Lcom/sec/chaton/a/an;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/an;->a(Lcom/sec/chaton/a/am;)Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/am;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 28272
    iput-object p1, p0, Lcom/sec/chaton/a/am;->d:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/am;J)J
    .locals 0

    .prologue
    .line 28272
    iput-wide p1, p0, Lcom/sec/chaton/a/am;->e:J

    return-wide p1
.end method

.method private k()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 28322
    iput-wide v1, p0, Lcom/sec/chaton/a/am;->c:J

    .line 28323
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/am;->d:Lcom/sec/chaton/a/ej;

    .line 28324
    iput-wide v1, p0, Lcom/sec/chaton/a/am;->e:J

    .line 28325
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28445
    invoke-static {}, Lcom/sec/chaton/a/an;->h()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/am;
    .locals 1

    .prologue
    .line 28287
    sget-object v0, Lcom/sec/chaton/a/am;->a:Lcom/sec/chaton/a/am;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 28295
    iget v1, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 28298
    iget-wide v0, p0, Lcom/sec/chaton/a/am;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 28305
    iget v0, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 28308
    iget-object v0, p0, Lcom/sec/chaton/a/am;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 28315
    iget v0, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28272
    invoke-virtual {p0}, Lcom/sec/chaton/a/am;->b()Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 28351
    iget v0, p0, Lcom/sec/chaton/a/am;->g:I

    .line 28352
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 28368
    :goto_0
    return v0

    .line 28354
    :cond_0
    const/4 v0, 0x0

    .line 28355
    iget v1, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 28356
    iget-wide v1, p0, Lcom/sec/chaton/a/am;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 28359
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 28360
    iget-object v1, p0, Lcom/sec/chaton/a/am;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28363
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 28364
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sec/chaton/a/am;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 28367
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/am;->g:I

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 28318
    iget-wide v0, p0, Lcom/sec/chaton/a/am;->e:J

    return-wide v0
.end method

.method public i()Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28446
    invoke-static {}, Lcom/sec/chaton/a/am;->newBuilder()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 28328
    iget-byte v1, p0, Lcom/sec/chaton/a/am;->f:B

    .line 28329
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 28332
    :goto_0
    return v0

    .line 28329
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 28331
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/am;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28450
    invoke-static {p0}, Lcom/sec/chaton/a/am;->a(Lcom/sec/chaton/a/am;)Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28272
    invoke-virtual {p0}, Lcom/sec/chaton/a/am;->i()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28272
    invoke-virtual {p0}, Lcom/sec/chaton/a/am;->j()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28375
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 28337
    invoke-virtual {p0}, Lcom/sec/chaton/a/am;->getSerializedSize()I

    .line 28338
    iget v0, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 28339
    iget-wide v0, p0, Lcom/sec/chaton/a/am;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 28341
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 28342
    iget-object v0, p0, Lcom/sec/chaton/a/am;->d:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 28344
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/am;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 28345
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sec/chaton/a/am;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 28347
    :cond_2
    return-void
.end method
