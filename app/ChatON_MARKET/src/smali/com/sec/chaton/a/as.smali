.class public final enum Lcom/sec/chaton/a/as;
.super Ljava/lang/Enum;
.source "SSMGPB.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/as;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/as;

.field public static final enum b:Lcom/sec/chaton/a/as;

.field private static c:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sec/chaton/a/as;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic e:[Lcom/sec/chaton/a/as;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 286
    new-instance v0, Lcom/sec/chaton/a/as;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sec/chaton/a/as;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/as;->a:Lcom/sec/chaton/a/as;

    .line 287
    new-instance v0, Lcom/sec/chaton/a/as;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sec/chaton/a/as;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/as;->b:Lcom/sec/chaton/a/as;

    .line 284
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/a/as;

    sget-object v1, Lcom/sec/chaton/a/as;->a:Lcom/sec/chaton/a/as;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/as;->b:Lcom/sec/chaton/a/as;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/a/as;->e:[Lcom/sec/chaton/a/as;

    .line 309
    new-instance v0, Lcom/sec/chaton/a/at;

    invoke-direct {v0}, Lcom/sec/chaton/a/at;-><init>()V

    sput-object v0, Lcom/sec/chaton/a/as;->c:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 318
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 319
    iput p4, p0, Lcom/sec/chaton/a/as;->d:I

    .line 320
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/a/as;
    .locals 1

    .prologue
    .line 297
    packed-switch p0, :pswitch_data_0

    .line 300
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 298
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/a/as;->a:Lcom/sec/chaton/a/as;

    goto :goto_0

    .line 299
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/a/as;->b:Lcom/sec/chaton/a/as;

    goto :goto_0

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/as;
    .locals 1

    .prologue
    .line 284
    const-class v0, Lcom/sec/chaton/a/as;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/as;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/as;
    .locals 1

    .prologue
    .line 284
    sget-object v0, Lcom/sec/chaton/a/as;->e:[Lcom/sec/chaton/a/as;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/as;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/as;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/sec/chaton/a/as;->d:I

    return v0
.end method
