.class public final Lcom/sec/chaton/a/s;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/r;",
        "Lcom/sec/chaton/a/s;",
        ">;",
        "Lcom/sec/chaton/a/t;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16280
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 16383
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    .line 16281
    invoke-direct {p0}, Lcom/sec/chaton/a/s;->g()V

    .line 16282
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/s;)Lcom/sec/chaton/a/r;
    .locals 1

    .prologue
    .line 16275
    invoke-direct {p0}, Lcom/sec/chaton/a/s;->i()Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16275
    invoke-static {}, Lcom/sec/chaton/a/s;->h()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 16285
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16287
    new-instance v0, Lcom/sec/chaton/a/s;

    invoke-direct {v0}, Lcom/sec/chaton/a/s;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/r;
    .locals 2

    .prologue
    .line 16315
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->e()Lcom/sec/chaton/a/r;

    move-result-object v0

    .line 16316
    invoke-virtual {v0}, Lcom/sec/chaton/a/r;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 16317
    invoke-static {v0}, Lcom/sec/chaton/a/s;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 16320
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 16386
    iget v0, p0, Lcom/sec/chaton/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 16387
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    .line 16388
    iget v0, p0, Lcom/sec/chaton/a/s;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/s;->a:I

    .line 16390
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16291
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 16292
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    .line 16293
    iget v0, p0, Lcom/sec/chaton/a/s;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/s;->a:I

    .line 16294
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16358
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 16359
    sparse-switch v0, :sswitch_data_0

    .line 16364
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/s;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16366
    :sswitch_0
    return-object p0

    .line 16371
    :sswitch_1
    invoke-static {}, Lcom/sec/chaton/a/x;->newBuilder()Lcom/sec/chaton/a/y;

    move-result-object v0

    .line 16372
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 16373
    invoke-virtual {v0}, Lcom/sec/chaton/a/y;->e()Lcom/sec/chaton/a/x;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/s;->a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/s;

    goto :goto_0

    .line 16359
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/r;)Lcom/sec/chaton/a/s;
    .locals 2

    .prologue
    .line 16335
    invoke-static {}, Lcom/sec/chaton/a/r;->a()Lcom/sec/chaton/a/r;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 16346
    :cond_0
    :goto_0
    return-object p0

    .line 16336
    :cond_1
    invoke-static {p1}, Lcom/sec/chaton/a/r;->b(Lcom/sec/chaton/a/r;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 16337
    iget-object v0, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16338
    invoke-static {p1}, Lcom/sec/chaton/a/r;->b(Lcom/sec/chaton/a/r;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    .line 16339
    iget v0, p0, Lcom/sec/chaton/a/s;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/s;->a:I

    goto :goto_0

    .line 16341
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/a/s;->j()V

    .line 16342
    iget-object v0, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/r;->b(Lcom/sec/chaton/a/r;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/s;
    .locals 1

    .prologue
    .line 16419
    if-nez p1, :cond_0

    .line 16420
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16422
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/s;->j()V

    .line 16423
    iget-object v0, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16425
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/s;
    .locals 2

    .prologue
    .line 16298
    invoke-static {}, Lcom/sec/chaton/a/s;->h()Lcom/sec/chaton/a/s;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->e()Lcom/sec/chaton/a/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/s;->a(Lcom/sec/chaton/a/r;)Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->d()Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->e()Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/r;
    .locals 1

    .prologue
    .line 16302
    invoke-static {}, Lcom/sec/chaton/a/r;->a()Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->a()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->a()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->b()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->b()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->b()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->b()Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/r;
    .locals 2

    .prologue
    .line 16306
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->e()Lcom/sec/chaton/a/r;

    move-result-object v0

    .line 16307
    invoke-virtual {v0}, Lcom/sec/chaton/a/r;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 16308
    invoke-static {v0}, Lcom/sec/chaton/a/s;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 16310
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/r;
    .locals 3

    .prologue
    .line 16324
    new-instance v0, Lcom/sec/chaton/a/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/a/r;-><init>(Lcom/sec/chaton/a/s;Lcom/sec/chaton/a/b;)V

    .line 16325
    iget v1, p0, Lcom/sec/chaton/a/s;->a:I

    .line 16326
    iget v1, p0, Lcom/sec/chaton/a/s;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 16327
    iget-object v1, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    .line 16328
    iget v1, p0, Lcom/sec/chaton/a/s;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sec/chaton/a/s;->a:I

    .line 16330
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/s;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/sec/chaton/a/r;->a(Lcom/sec/chaton/a/r;Ljava/util/List;)Ljava/util/List;

    .line 16331
    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->c()Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0}, Lcom/sec/chaton/a/s;->c()Lcom/sec/chaton/a/r;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 16350
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/s;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    check-cast p1, Lcom/sec/chaton/a/r;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/s;->a(Lcom/sec/chaton/a/r;)Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/s;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/s;

    move-result-object v0

    return-object v0
.end method
