.class public final Lcom/sec/chaton/a/de;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/df;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/dd;",
        "Lcom/sec/chaton/a/de;",
        ">;",
        "Lcom/sec/chaton/a/df;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/ej;

.field private d:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 18046
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 18195
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    .line 18047
    invoke-direct {p0}, Lcom/sec/chaton/a/de;->i()V

    .line 18048
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/de;)Lcom/sec/chaton/a/dd;
    .locals 1

    .prologue
    .line 18041
    invoke-direct {p0}, Lcom/sec/chaton/a/de;->k()Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18041
    invoke-static {}, Lcom/sec/chaton/a/de;->j()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 18051
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18053
    new-instance v0, Lcom/sec/chaton/a/de;

    invoke-direct {v0}, Lcom/sec/chaton/a/de;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/dd;
    .locals 2

    .prologue
    .line 18085
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->e()Lcom/sec/chaton/a/dd;

    move-result-object v0

    .line 18086
    invoke-virtual {v0}, Lcom/sec/chaton/a/dd;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18087
    invoke-static {v0}, Lcom/sec/chaton/a/de;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 18090
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/de;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 18057
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 18058
    iput-wide v1, p0, Lcom/sec/chaton/a/de;->b:J

    .line 18059
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18060
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    .line 18061
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18062
    iput-wide v1, p0, Lcom/sec/chaton/a/de;->d:J

    .line 18063
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18064
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18182
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18183
    iput-wide p1, p0, Lcom/sec/chaton/a/de;->b:J

    .line 18185
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/de;
    .locals 2

    .prologue
    .line 18136
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 18137
    sparse-switch v0, :sswitch_data_0

    .line 18142
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/de;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18144
    :sswitch_0
    return-object p0

    .line 18149
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18150
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/de;->b:J

    goto :goto_0

    .line 18154
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 18155
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18156
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 18158
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 18159
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/de;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/de;

    goto :goto_0

    .line 18163
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18164
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/de;->d:J

    goto :goto_0

    .line 18137
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dd;)Lcom/sec/chaton/a/de;
    .locals 2

    .prologue
    .line 18114
    invoke-static {}, Lcom/sec/chaton/a/dd;->a()Lcom/sec/chaton/a/dd;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 18124
    :cond_0
    :goto_0
    return-object p0

    .line 18115
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/dd;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 18116
    invoke-virtual {p1}, Lcom/sec/chaton/a/dd;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/de;->a(J)Lcom/sec/chaton/a/de;

    .line 18118
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/dd;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18119
    invoke-virtual {p1}, Lcom/sec/chaton/a/dd;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/de;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/de;

    .line 18121
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/dd;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18122
    invoke-virtual {p1}, Lcom/sec/chaton/a/dd;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/de;->b(J)Lcom/sec/chaton/a/de;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18203
    if-nez p1, :cond_0

    .line 18204
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18206
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    .line 18208
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18209
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/de;
    .locals 2

    .prologue
    .line 18068
    invoke-static {}, Lcom/sec/chaton/a/de;->j()Lcom/sec/chaton/a/de;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->e()Lcom/sec/chaton/a/dd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/de;->a(Lcom/sec/chaton/a/dd;)Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/de;
    .locals 1

    .prologue
    .line 18246
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18247
    iput-wide p1, p0, Lcom/sec/chaton/a/de;->d:J

    .line 18249
    return-object p0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/de;
    .locals 2

    .prologue
    .line 18219
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 18221
    iget-object v0, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    .line 18227
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18228
    return-object p0

    .line 18224
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->d()Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->e()Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/dd;
    .locals 1

    .prologue
    .line 18072
    invoke-static {}, Lcom/sec/chaton/a/dd;->a()Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->a()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->a()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->b()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->b()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->b()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->b()Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/dd;
    .locals 2

    .prologue
    .line 18076
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->e()Lcom/sec/chaton/a/dd;

    move-result-object v0

    .line 18077
    invoke-virtual {v0}, Lcom/sec/chaton/a/dd;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18078
    invoke-static {v0}, Lcom/sec/chaton/a/de;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 18080
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/dd;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 18094
    new-instance v2, Lcom/sec/chaton/a/dd;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/dd;-><init>(Lcom/sec/chaton/a/de;Lcom/sec/chaton/a/b;)V

    .line 18095
    iget v3, p0, Lcom/sec/chaton/a/de;->a:I

    .line 18096
    const/4 v1, 0x0

    .line 18097
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 18100
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/de;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/dd;->a(Lcom/sec/chaton/a/dd;J)J

    .line 18101
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 18102
    or-int/lit8 v0, v0, 0x2

    .line 18104
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dd;->a(Lcom/sec/chaton/a/dd;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 18105
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 18106
    or-int/lit8 v0, v0, 0x4

    .line 18108
    :cond_1
    iget-wide v3, p0, Lcom/sec/chaton/a/de;->d:J

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/a/dd;->b(Lcom/sec/chaton/a/dd;J)J

    .line 18109
    invoke-static {v2, v0}, Lcom/sec/chaton/a/dd;->a(Lcom/sec/chaton/a/dd;I)I

    .line 18110
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 18197
    iget v0, p0, Lcom/sec/chaton/a/de;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 18200
    iget-object v0, p0, Lcom/sec/chaton/a/de;->c:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->c()Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0}, Lcom/sec/chaton/a/de;->c()Lcom/sec/chaton/a/dd;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 18128
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/de;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    check-cast p1, Lcom/sec/chaton/a/dd;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/de;->a(Lcom/sec/chaton/a/dd;)Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18041
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/de;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/de;

    move-result-object v0

    return-object v0
.end method
