.class public Lcom/sec/chaton/a/a/b;
.super Lcom/sec/chaton/a/a/k;
.source "DeliveryChatResultEntry.java"


# instance fields
.field private d:Ljava/lang/String;

.field private e:Z

.field private f:I

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/chaton/e/w;


# direct methods
.method protected constructor <init>(Lcom/sec/chaton/a/a/c;)V
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p1, Lcom/sec/chaton/a/a/c;->a:Z

    iget-object v1, p1, Lcom/sec/chaton/a/a/c;->b:Lcom/sec/chaton/a/a/n;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/a/a/k;-><init>(ZLcom/sec/chaton/a/a/n;)V

    .line 96
    iget-object v0, p1, Lcom/sec/chaton/a/a/c;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/a/a/b;->d:Ljava/lang/String;

    .line 97
    iget-boolean v0, p1, Lcom/sec/chaton/a/a/c;->e:Z

    iput-boolean v0, p0, Lcom/sec/chaton/a/a/b;->e:Z

    .line 98
    iget v0, p1, Lcom/sec/chaton/a/a/c;->f:I

    iput v0, p0, Lcom/sec/chaton/a/a/b;->f:I

    .line 99
    iget-object v0, p1, Lcom/sec/chaton/a/a/c;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/a/a/b;->i:Ljava/lang/String;

    .line 100
    iget-object v0, p1, Lcom/sec/chaton/a/a/c;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/a/a/b;->h:Ljava/lang/String;

    .line 101
    iget-object v0, p1, Lcom/sec/chaton/a/a/c;->i:Lcom/sec/chaton/e/w;

    iput-object v0, p0, Lcom/sec/chaton/a/a/b;->j:Lcom/sec/chaton/e/w;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/chaton/a/a/c;->j:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/a/b;->g:Ljava/util/ArrayList;

    .line 103
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/chaton/a/a/b;->e:Z

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/sec/chaton/a/a/b;->f:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/chaton/a/a/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/chaton/a/a/b;->i:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/chaton/a/a/b;->j:Lcom/sec/chaton/e/w;

    return-object v0
.end method
