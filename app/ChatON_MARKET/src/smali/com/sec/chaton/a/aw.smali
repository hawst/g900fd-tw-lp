.class public final Lcom/sec/chaton/a/aw;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ay;


# static fields
.field private static final a:Lcom/sec/chaton/a/aw;


# instance fields
.field private b:I

.field private c:J

.field private d:Ljava/lang/Object;

.field private e:Lcom/sec/chaton/a/ey;

.field private f:J

.field private g:Lcom/sec/chaton/a/ej;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13408
    new-instance v0, Lcom/sec/chaton/a/aw;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/aw;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/aw;->a:Lcom/sec/chaton/a/aw;

    .line 13409
    sget-object v0, Lcom/sec/chaton/a/aw;->a:Lcom/sec/chaton/a/aw;

    invoke-direct {v0}, Lcom/sec/chaton/a/aw;->p()V

    .line 13410
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ax;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12843
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 12936
    iput-byte v0, p0, Lcom/sec/chaton/a/aw;->h:B

    .line 12965
    iput v0, p0, Lcom/sec/chaton/a/aw;->i:I

    .line 12844
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ax;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 12838
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/aw;-><init>(Lcom/sec/chaton/a/ax;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12845
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 12936
    iput-byte v0, p0, Lcom/sec/chaton/a/aw;->h:B

    .line 12965
    iput v0, p0, Lcom/sec/chaton/a/aw;->i:I

    .line 12845
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/aw;I)I
    .locals 0

    .prologue
    .line 12838
    iput p1, p0, Lcom/sec/chaton/a/aw;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/aw;J)J
    .locals 0

    .prologue
    .line 12838
    iput-wide p1, p0, Lcom/sec/chaton/a/aw;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/aw;
    .locals 1

    .prologue
    .line 12849
    sget-object v0, Lcom/sec/chaton/a/aw;->a:Lcom/sec/chaton/a/aw;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/aw;
    .locals 1

    .prologue
    .line 13016
    invoke-static {}, Lcom/sec/chaton/a/aw;->newBuilder()Lcom/sec/chaton/a/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ax;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/ax;

    invoke-static {v0}, Lcom/sec/chaton/a/ax;->a(Lcom/sec/chaton/a/ax;)Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/aw;)Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13072
    invoke-static {}, Lcom/sec/chaton/a/aw;->newBuilder()Lcom/sec/chaton/a/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ax;->a(Lcom/sec/chaton/a/aw;)Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/aw;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 12838
    iput-object p1, p0, Lcom/sec/chaton/a/aw;->g:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/aw;Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ey;
    .locals 0

    .prologue
    .line 12838
    iput-object p1, p0, Lcom/sec/chaton/a/aw;->e:Lcom/sec/chaton/a/ey;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/aw;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 12838
    iput-object p1, p0, Lcom/sec/chaton/a/aw;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/aw;J)J
    .locals 0

    .prologue
    .line 12838
    iput-wide p1, p0, Lcom/sec/chaton/a/aw;->f:J

    return-wide p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13069
    invoke-static {}, Lcom/sec/chaton/a/ax;->j()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 12888
    iget-object v0, p0, Lcom/sec/chaton/a/aw;->d:Ljava/lang/Object;

    .line 12889
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12890
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 12892
    iput-object v0, p0, Lcom/sec/chaton/a/aw;->d:Ljava/lang/Object;

    .line 12895
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 12930
    iput-wide v1, p0, Lcom/sec/chaton/a/aw;->c:J

    .line 12931
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/aw;->d:Ljava/lang/Object;

    .line 12932
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/aw;->e:Lcom/sec/chaton/a/ey;

    .line 12933
    iput-wide v1, p0, Lcom/sec/chaton/a/aw;->f:J

    .line 12934
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/aw;->g:Lcom/sec/chaton/a/ej;

    .line 12935
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/aw;
    .locals 1

    .prologue
    .line 12853
    sget-object v0, Lcom/sec/chaton/a/aw;->a:Lcom/sec/chaton/a/aw;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 12861
    iget v1, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 12864
    iget-wide v0, p0, Lcom/sec/chaton/a/aw;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 12871
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12874
    iget-object v0, p0, Lcom/sec/chaton/a/aw;->d:Ljava/lang/Object;

    .line 12875
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12876
    check-cast v0, Ljava/lang/String;

    .line 12884
    :goto_0
    return-object v0

    .line 12878
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 12880
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 12881
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12882
    iput-object v1, p0, Lcom/sec/chaton/a/aw;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 12884
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 12903
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12838
    invoke-virtual {p0}, Lcom/sec/chaton/a/aw;->b()Lcom/sec/chaton/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 12967
    iget v0, p0, Lcom/sec/chaton/a/aw;->i:I

    .line 12968
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 12992
    :goto_0
    return v0

    .line 12970
    :cond_0
    const/4 v0, 0x0

    .line 12971
    iget v1, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 12972
    iget-wide v1, p0, Lcom/sec/chaton/a/aw;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12975
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 12976
    invoke-direct {p0}, Lcom/sec/chaton/a/aw;->o()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12979
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    .line 12980
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/chaton/a/aw;->e:Lcom/sec/chaton/a/ey;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12983
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 12984
    iget-wide v1, p0, Lcom/sec/chaton/a/aw;->f:J

    invoke-static {v5, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12987
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 12988
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/chaton/a/aw;->g:Lcom/sec/chaton/a/ej;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12991
    :cond_5
    iput v0, p0, Lcom/sec/chaton/a/aw;->i:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 12906
    iget-object v0, p0, Lcom/sec/chaton/a/aw;->e:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 12913
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 12938
    iget-byte v1, p0, Lcom/sec/chaton/a/aw;->h:B

    .line 12939
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 12942
    :goto_0
    return v0

    .line 12939
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 12941
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/aw;->h:B

    goto :goto_0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 12916
    iget-wide v0, p0, Lcom/sec/chaton/a/aw;->f:J

    return-wide v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 12923
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 12926
    iget-object v0, p0, Lcom/sec/chaton/a/aw;->g:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public m()Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13070
    invoke-static {}, Lcom/sec/chaton/a/aw;->newBuilder()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/sec/chaton/a/ax;
    .locals 1

    .prologue
    .line 13074
    invoke-static {p0}, Lcom/sec/chaton/a/aw;->a(Lcom/sec/chaton/a/aw;)Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12838
    invoke-virtual {p0}, Lcom/sec/chaton/a/aw;->m()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12838
    invoke-virtual {p0}, Lcom/sec/chaton/a/aw;->n()Lcom/sec/chaton/a/ax;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12999
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12947
    invoke-virtual {p0}, Lcom/sec/chaton/a/aw;->getSerializedSize()I

    .line 12948
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 12949
    iget-wide v0, p0, Lcom/sec/chaton/a/aw;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 12951
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 12952
    invoke-direct {p0}, Lcom/sec/chaton/a/aw;->o()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 12954
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 12955
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/aw;->e:Lcom/sec/chaton/a/ey;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 12957
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 12958
    iget-wide v0, p0, Lcom/sec/chaton/a/aw;->f:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 12960
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/aw;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 12961
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sec/chaton/a/aw;->g:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 12963
    :cond_4
    return-void
.end method
