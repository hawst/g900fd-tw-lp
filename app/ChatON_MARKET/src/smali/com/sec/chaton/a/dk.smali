.class public final Lcom/sec/chaton/a/dk;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/dj;",
        "Lcom/sec/chaton/a/dk;",
        ">;",
        "Lcom/sec/chaton/a/dl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/sec/chaton/a/bc;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/sec/chaton/a/be;

.field private g:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 23521
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 23695
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->b:Lcom/sec/chaton/a/bc;

    .line 23719
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->c:Ljava/lang/Object;

    .line 23755
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->d:Ljava/lang/Object;

    .line 23791
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->e:Ljava/lang/Object;

    .line 23827
    sget-object v0, Lcom/sec/chaton/a/be;->a:Lcom/sec/chaton/a/be;

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->f:Lcom/sec/chaton/a/be;

    .line 23522
    invoke-direct {p0}, Lcom/sec/chaton/a/dk;->g()V

    .line 23523
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dk;)Lcom/sec/chaton/a/dj;
    .locals 1

    .prologue
    .line 23516
    invoke-direct {p0}, Lcom/sec/chaton/a/dk;->i()Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23516
    invoke-static {}, Lcom/sec/chaton/a/dk;->h()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 23526
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23528
    new-instance v0, Lcom/sec/chaton/a/dk;

    invoke-direct {v0}, Lcom/sec/chaton/a/dk;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/dj;
    .locals 2

    .prologue
    .line 23566
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->e()Lcom/sec/chaton/a/dj;

    move-result-object v0

    .line 23567
    invoke-virtual {v0}, Lcom/sec/chaton/a/dj;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 23568
    invoke-static {v0}, Lcom/sec/chaton/a/dk;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 23571
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23532
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 23533
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->b:Lcom/sec/chaton/a/bc;

    .line 23534
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23535
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->c:Ljava/lang/Object;

    .line 23536
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23537
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->d:Ljava/lang/Object;

    .line 23538
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23539
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->e:Ljava/lang/Object;

    .line 23540
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23541
    sget-object v0, Lcom/sec/chaton/a/be;->a:Lcom/sec/chaton/a/be;

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->f:Lcom/sec/chaton/a/be;

    .line 23542
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23543
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/dk;->g:I

    .line 23544
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23545
    return-object p0
.end method

.method public a(I)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23859
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23860
    iput p1, p0, Lcom/sec/chaton/a/dk;->g:I

    .line 23862
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dk;
    .locals 2

    .prologue
    .line 23638
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 23639
    sparse-switch v0, :sswitch_data_0

    .line 23644
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/dk;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23646
    :sswitch_0
    return-object p0

    .line 23651
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 23652
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 23653
    if-eqz v0, :cond_0

    .line 23654
    iget v1, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23655
    iput-object v0, p0, Lcom/sec/chaton/a/dk;->b:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 23660
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23661
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->c:Ljava/lang/Object;

    goto :goto_0

    .line 23665
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23666
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->d:Ljava/lang/Object;

    goto :goto_0

    .line 23670
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23671
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dk;->e:Ljava/lang/Object;

    goto :goto_0

    .line 23675
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 23676
    invoke-static {v0}, Lcom/sec/chaton/a/be;->a(I)Lcom/sec/chaton/a/be;

    move-result-object v0

    .line 23677
    if-eqz v0, :cond_0

    .line 23678
    iget v1, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23679
    iput-object v0, p0, Lcom/sec/chaton/a/dk;->f:Lcom/sec/chaton/a/be;

    goto :goto_0

    .line 23684
    :sswitch_6
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23685
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/a/dk;->g:I

    goto :goto_0

    .line 23639
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23703
    if-nez p1, :cond_0

    .line 23704
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23706
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23707
    iput-object p1, p0, Lcom/sec/chaton/a/dk;->b:Lcom/sec/chaton/a/bc;

    .line 23709
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/be;)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23835
    if-nez p1, :cond_0

    .line 23836
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23838
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23839
    iput-object p1, p0, Lcom/sec/chaton/a/dk;->f:Lcom/sec/chaton/a/be;

    .line 23841
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/dj;)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23607
    invoke-static {}, Lcom/sec/chaton/a/dj;->a()Lcom/sec/chaton/a/dj;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 23626
    :cond_0
    :goto_0
    return-object p0

    .line 23608
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 23609
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->d()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dk;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/dk;

    .line 23611
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 23612
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dk;->a(Ljava/lang/String;)Lcom/sec/chaton/a/dk;

    .line 23614
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 23615
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dk;->b(Ljava/lang/String;)Lcom/sec/chaton/a/dk;

    .line 23617
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 23618
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dk;->c(Ljava/lang/String;)Lcom/sec/chaton/a/dk;

    .line 23620
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 23621
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->l()Lcom/sec/chaton/a/be;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dk;->a(Lcom/sec/chaton/a/be;)Lcom/sec/chaton/a/dk;

    .line 23623
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23624
    invoke-virtual {p1}, Lcom/sec/chaton/a/dj;->n()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dk;->a(I)Lcom/sec/chaton/a/dk;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23734
    if-nez p1, :cond_0

    .line 23735
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23737
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23738
    iput-object p1, p0, Lcom/sec/chaton/a/dk;->c:Ljava/lang/Object;

    .line 23740
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/dk;
    .locals 2

    .prologue
    .line 23549
    invoke-static {}, Lcom/sec/chaton/a/dk;->h()Lcom/sec/chaton/a/dk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->e()Lcom/sec/chaton/a/dj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/dk;->a(Lcom/sec/chaton/a/dj;)Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23770
    if-nez p1, :cond_0

    .line 23771
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23773
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23774
    iput-object p1, p0, Lcom/sec/chaton/a/dk;->d:Ljava/lang/Object;

    .line 23776
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->d()Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->e()Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/dj;
    .locals 1

    .prologue
    .line 23553
    invoke-static {}, Lcom/sec/chaton/a/dj;->a()Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/dk;
    .locals 1

    .prologue
    .line 23806
    if-nez p1, :cond_0

    .line 23807
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23809
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23810
    iput-object p1, p0, Lcom/sec/chaton/a/dk;->e:Ljava/lang/Object;

    .line 23812
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->a()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->a()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->b()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->b()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->b()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->b()Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/dj;
    .locals 2

    .prologue
    .line 23557
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->e()Lcom/sec/chaton/a/dj;

    move-result-object v0

    .line 23558
    invoke-virtual {v0}, Lcom/sec/chaton/a/dj;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 23559
    invoke-static {v0}, Lcom/sec/chaton/a/dk;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 23561
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/dj;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 23575
    new-instance v2, Lcom/sec/chaton/a/dj;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/dj;-><init>(Lcom/sec/chaton/a/dk;Lcom/sec/chaton/a/b;)V

    .line 23576
    iget v3, p0, Lcom/sec/chaton/a/dk;->a:I

    .line 23577
    const/4 v1, 0x0

    .line 23578
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 23581
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/dk;->b:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dj;->a(Lcom/sec/chaton/a/dj;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 23582
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 23583
    or-int/lit8 v0, v0, 0x2

    .line 23585
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/dk;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dj;->a(Lcom/sec/chaton/a/dj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23586
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 23587
    or-int/lit8 v0, v0, 0x4

    .line 23589
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/dk;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dj;->b(Lcom/sec/chaton/a/dj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23590
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 23591
    or-int/lit8 v0, v0, 0x8

    .line 23593
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/dk;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dj;->c(Lcom/sec/chaton/a/dj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23594
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 23595
    or-int/lit8 v0, v0, 0x10

    .line 23597
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/dk;->f:Lcom/sec/chaton/a/be;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dj;->a(Lcom/sec/chaton/a/dj;Lcom/sec/chaton/a/be;)Lcom/sec/chaton/a/be;

    .line 23598
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 23599
    or-int/lit8 v0, v0, 0x20

    .line 23601
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/dk;->g:I

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dj;->a(Lcom/sec/chaton/a/dj;I)I

    .line 23602
    invoke-static {v2, v0}, Lcom/sec/chaton/a/dj;->b(Lcom/sec/chaton/a/dj;I)I

    .line 23603
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->c()Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0}, Lcom/sec/chaton/a/dk;->c()Lcom/sec/chaton/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 23630
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dk;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    check-cast p1, Lcom/sec/chaton/a/dj;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/dk;->a(Lcom/sec/chaton/a/dj;)Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23516
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dk;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dk;

    move-result-object v0

    return-object v0
.end method
