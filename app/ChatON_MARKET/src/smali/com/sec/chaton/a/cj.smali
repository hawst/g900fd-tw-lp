.class public final Lcom/sec/chaton/a/cj;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ck;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ci;",
        "Lcom/sec/chaton/a/cj;",
        ">;",
        "Lcom/sec/chaton/a/ck;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Lcom/sec/chaton/a/cu;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29895
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 30037
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->b:Ljava/lang/Object;

    .line 30073
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->c:Ljava/lang/Object;

    .line 30109
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->d:Ljava/lang/Object;

    .line 30145
    invoke-static {}, Lcom/sec/chaton/a/cu;->a()Lcom/sec/chaton/a/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    .line 29896
    invoke-direct {p0}, Lcom/sec/chaton/a/cj;->i()V

    .line 29897
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cj;)Lcom/sec/chaton/a/ci;
    .locals 1

    .prologue
    .line 29890
    invoke-direct {p0}, Lcom/sec/chaton/a/cj;->k()Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29890
    invoke-static {}, Lcom/sec/chaton/a/cj;->j()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 29900
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29902
    new-instance v0, Lcom/sec/chaton/a/cj;

    invoke-direct {v0}, Lcom/sec/chaton/a/cj;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/ci;
    .locals 2

    .prologue
    .line 29936
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->e()Lcom/sec/chaton/a/ci;

    move-result-object v0

    .line 29937
    invoke-virtual {v0}, Lcom/sec/chaton/a/ci;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29938
    invoke-static {v0}, Lcom/sec/chaton/a/cj;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 29941
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29906
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 29907
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->b:Ljava/lang/Object;

    .line 29908
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 29909
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->c:Ljava/lang/Object;

    .line 29910
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 29911
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->d:Ljava/lang/Object;

    .line 29912
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 29913
    invoke-static {}, Lcom/sec/chaton/a/cu;->a()Lcom/sec/chaton/a/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    .line 29914
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 29915
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cj;
    .locals 2

    .prologue
    .line 29994
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 29995
    sparse-switch v0, :sswitch_data_0

    .line 30000
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/cj;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30002
    :sswitch_0
    return-object p0

    .line 30007
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30008
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->b:Ljava/lang/Object;

    goto :goto_0

    .line 30012
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30013
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->c:Ljava/lang/Object;

    goto :goto_0

    .line 30017
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30018
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->d:Ljava/lang/Object;

    goto :goto_0

    .line 30022
    :sswitch_4
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v0

    .line 30023
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 30024
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->g()Lcom/sec/chaton/a/cu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;

    .line 30026
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 30027
    invoke-virtual {v0}, Lcom/sec/chaton/a/cv;->e()Lcom/sec/chaton/a/cu;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cj;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cj;

    goto :goto_0

    .line 29995
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ci;)Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29969
    invoke-static {}, Lcom/sec/chaton/a/ci;->a()Lcom/sec/chaton/a/ci;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 29982
    :cond_0
    :goto_0
    return-object p0

    .line 29970
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29971
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cj;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cj;

    .line 29973
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29974
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cj;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cj;

    .line 29976
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 29977
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cj;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cj;

    .line 29979
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29980
    invoke-virtual {p1}, Lcom/sec/chaton/a/ci;->j()Lcom/sec/chaton/a/cu;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cj;->b(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cj;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 30153
    if-nez p1, :cond_0

    .line 30154
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30156
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    .line 30158
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30159
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 30052
    if-nez p1, :cond_0

    .line 30053
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30055
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30056
    iput-object p1, p0, Lcom/sec/chaton/a/cj;->b:Ljava/lang/Object;

    .line 30058
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/cj;
    .locals 2

    .prologue
    .line 29919
    invoke-static {}, Lcom/sec/chaton/a/cj;->j()Lcom/sec/chaton/a/cj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->e()Lcom/sec/chaton/a/ci;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cj;->a(Lcom/sec/chaton/a/ci;)Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cj;
    .locals 2

    .prologue
    .line 30169
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    invoke-static {}, Lcom/sec/chaton/a/cu;->a()Lcom/sec/chaton/a/cu;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 30171
    iget-object v0, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    invoke-static {v0}, Lcom/sec/chaton/a/cu;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/cv;->e()Lcom/sec/chaton/a/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    .line 30177
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30178
    return-object p0

    .line 30174
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 30088
    if-nez p1, :cond_0

    .line 30089
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30091
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30092
    iput-object p1, p0, Lcom/sec/chaton/a/cj;->c:Ljava/lang/Object;

    .line 30094
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->d()Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->e()Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ci;
    .locals 1

    .prologue
    .line 29923
    invoke-static {}, Lcom/sec/chaton/a/ci;->a()Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 30124
    if-nez p1, :cond_0

    .line 30125
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30127
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 30128
    iput-object p1, p0, Lcom/sec/chaton/a/cj;->d:Ljava/lang/Object;

    .line 30130
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->a()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->a()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->b()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->b()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->b()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->b()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ci;
    .locals 2

    .prologue
    .line 29927
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->e()Lcom/sec/chaton/a/ci;

    move-result-object v0

    .line 29928
    invoke-virtual {v0}, Lcom/sec/chaton/a/ci;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29929
    invoke-static {v0}, Lcom/sec/chaton/a/cj;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 29931
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ci;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 29945
    new-instance v2, Lcom/sec/chaton/a/ci;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ci;-><init>(Lcom/sec/chaton/a/cj;Lcom/sec/chaton/a/b;)V

    .line 29946
    iget v3, p0, Lcom/sec/chaton/a/cj;->a:I

    .line 29947
    const/4 v1, 0x0

    .line 29948
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 29951
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/cj;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ci;->a(Lcom/sec/chaton/a/ci;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29952
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 29953
    or-int/lit8 v0, v0, 0x2

    .line 29955
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/cj;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ci;->b(Lcom/sec/chaton/a/ci;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29956
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 29957
    or-int/lit8 v0, v0, 0x4

    .line 29959
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/cj;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ci;->c(Lcom/sec/chaton/a/ci;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29960
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 29961
    or-int/lit8 v0, v0, 0x8

    .line 29963
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ci;->a(Lcom/sec/chaton/a/ci;Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cu;

    .line 29964
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ci;->a(Lcom/sec/chaton/a/ci;I)I

    .line 29965
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 30147
    iget v0, p0, Lcom/sec/chaton/a/cj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/cu;
    .locals 1

    .prologue
    .line 30150
    iget-object v0, p0, Lcom/sec/chaton/a/cj;->e:Lcom/sec/chaton/a/cu;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->c()Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0}, Lcom/sec/chaton/a/cj;->c()Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 29986
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cj;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    check-cast p1, Lcom/sec/chaton/a/ci;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/cj;->a(Lcom/sec/chaton/a/ci;)Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29890
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cj;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method
