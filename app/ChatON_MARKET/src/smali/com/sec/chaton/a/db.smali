.class public final Lcom/sec/chaton/a/db;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/da;",
        "Lcom/sec/chaton/a/db;",
        ">;",
        "Lcom/sec/chaton/a/dc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/dp;

.field private d:Lcom/sec/chaton/a/bc;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:J

.field private h:Ljava/lang/Object;

.field private i:Lcom/google/protobuf/LazyStringList;

.field private j:Ljava/lang/Object;

.field private k:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8933
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 9193
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/db;->c:Lcom/sec/chaton/a/dp;

    .line 9217
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/db;->d:Lcom/sec/chaton/a/bc;

    .line 9241
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->e:Ljava/lang/Object;

    .line 9277
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->f:Ljava/lang/Object;

    .line 9334
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->h:Ljava/lang/Object;

    .line 9370
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    .line 9426
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->j:Ljava/lang/Object;

    .line 8934
    invoke-direct {p0}, Lcom/sec/chaton/a/db;->g()V

    .line 8935
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/db;)Lcom/sec/chaton/a/da;
    .locals 1

    .prologue
    .line 8928
    invoke-direct {p0}, Lcom/sec/chaton/a/db;->i()Lcom/sec/chaton/a/da;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 8928
    invoke-static {}, Lcom/sec/chaton/a/db;->h()Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 8938
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 8940
    new-instance v0, Lcom/sec/chaton/a/db;

    invoke-direct {v0}, Lcom/sec/chaton/a/db;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/da;
    .locals 2

    .prologue
    .line 8986
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->e()Lcom/sec/chaton/a/da;

    move-result-object v0

    .line 8987
    invoke-virtual {v0}, Lcom/sec/chaton/a/da;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8988
    invoke-static {v0}, Lcom/sec/chaton/a/db;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 8991
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 9372
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 9373
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    .line 9374
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9376
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/db;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 8944
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 8945
    iput-wide v1, p0, Lcom/sec/chaton/a/db;->b:J

    .line 8946
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8947
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/db;->c:Lcom/sec/chaton/a/dp;

    .line 8948
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8949
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/db;->d:Lcom/sec/chaton/a/bc;

    .line 8950
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8951
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->e:Ljava/lang/Object;

    .line 8952
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8953
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->f:Ljava/lang/Object;

    .line 8954
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8955
    iput-wide v1, p0, Lcom/sec/chaton/a/db;->g:J

    .line 8956
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8957
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->h:Ljava/lang/Object;

    .line 8958
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8959
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    .line 8960
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8961
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/db;->j:Ljava/lang/Object;

    .line 8962
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8963
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/db;->k:Z

    .line 8964
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8965
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9180
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9181
    iput-wide p1, p0, Lcom/sec/chaton/a/db;->b:J

    .line 9183
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/db;
    .locals 2

    .prologue
    .line 9095
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 9096
    sparse-switch v0, :sswitch_data_0

    .line 9101
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/db;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9103
    :sswitch_0
    return-object p0

    .line 9108
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9109
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/db;->b:J

    goto :goto_0

    .line 9113
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 9114
    invoke-static {v0}, Lcom/sec/chaton/a/dp;->a(I)Lcom/sec/chaton/a/dp;

    move-result-object v0

    .line 9115
    if-eqz v0, :cond_0

    .line 9116
    iget v1, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9117
    iput-object v0, p0, Lcom/sec/chaton/a/db;->c:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 9122
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 9123
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 9124
    if-eqz v0, :cond_0

    .line 9125
    iget v1, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9126
    iput-object v0, p0, Lcom/sec/chaton/a/db;->d:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 9131
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9132
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/db;->e:Ljava/lang/Object;

    goto :goto_0

    .line 9136
    :sswitch_5
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9137
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/db;->f:Ljava/lang/Object;

    goto :goto_0

    .line 9141
    :sswitch_6
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9142
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/db;->g:J

    goto :goto_0

    .line 9146
    :sswitch_7
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9147
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/db;->h:Ljava/lang/Object;

    goto :goto_0

    .line 9151
    :sswitch_8
    invoke-direct {p0}, Lcom/sec/chaton/a/db;->j()V

    .line 9152
    iget-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    .line 9156
    :sswitch_9
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9157
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/db;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 9161
    :sswitch_a
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9162
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/a/db;->k:Z

    goto/16 :goto_0

    .line 9096
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9225
    if-nez p1, :cond_0

    .line 9226
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9228
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9229
    iput-object p1, p0, Lcom/sec/chaton/a/db;->d:Lcom/sec/chaton/a/bc;

    .line 9231
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/da;)Lcom/sec/chaton/a/db;
    .locals 2

    .prologue
    .line 9045
    invoke-static {}, Lcom/sec/chaton/a/da;->a()Lcom/sec/chaton/a/da;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 9083
    :cond_0
    :goto_0
    return-object p0

    .line 9046
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9047
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/db;->a(J)Lcom/sec/chaton/a/db;

    .line 9049
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 9050
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->f()Lcom/sec/chaton/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/db;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/db;

    .line 9052
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 9053
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->h()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/db;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/db;

    .line 9055
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 9056
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/db;->a(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    .line 9058
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 9059
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/db;->b(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    .line 9061
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 9062
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->n()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/db;->b(J)Lcom/sec/chaton/a/db;

    .line 9064
    :cond_7
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 9065
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/db;->c(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    .line 9067
    :cond_8
    invoke-static {p1}, Lcom/sec/chaton/a/da;->b(Lcom/sec/chaton/a/da;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 9068
    iget-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 9069
    invoke-static {p1}, Lcom/sec/chaton/a/da;->b(Lcom/sec/chaton/a/da;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    .line 9070
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9077
    :cond_9
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->r()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 9078
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/db;->e(Ljava/lang/String;)Lcom/sec/chaton/a/db;

    .line 9080
    :cond_a
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9081
    invoke-virtual {p1}, Lcom/sec/chaton/a/da;->u()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/db;->a(Z)Lcom/sec/chaton/a/db;

    goto/16 :goto_0

    .line 9072
    :cond_b
    invoke-direct {p0}, Lcom/sec/chaton/a/db;->j()V

    .line 9073
    iget-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/da;->b(Lcom/sec/chaton/a/da;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9201
    if-nez p1, :cond_0

    .line 9202
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9204
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9205
    iput-object p1, p0, Lcom/sec/chaton/a/db;->c:Lcom/sec/chaton/a/dp;

    .line 9207
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9256
    if-nez p1, :cond_0

    .line 9257
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9259
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9260
    iput-object p1, p0, Lcom/sec/chaton/a/db;->e:Ljava/lang/Object;

    .line 9262
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9470
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9471
    iput-boolean p1, p0, Lcom/sec/chaton/a/db;->k:Z

    .line 9473
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/db;
    .locals 2

    .prologue
    .line 8969
    invoke-static {}, Lcom/sec/chaton/a/db;->h()Lcom/sec/chaton/a/db;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->e()Lcom/sec/chaton/a/da;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/db;->a(Lcom/sec/chaton/a/da;)Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9321
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9322
    iput-wide p1, p0, Lcom/sec/chaton/a/db;->g:J

    .line 9324
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9292
    if-nez p1, :cond_0

    .line 9293
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9295
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9296
    iput-object p1, p0, Lcom/sec/chaton/a/db;->f:Ljava/lang/Object;

    .line 9298
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->d()Lcom/sec/chaton/a/da;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->e()Lcom/sec/chaton/a/da;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/da;
    .locals 1

    .prologue
    .line 8973
    invoke-static {}, Lcom/sec/chaton/a/da;->a()Lcom/sec/chaton/a/da;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9349
    if-nez p1, :cond_0

    .line 9350
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9352
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9353
    iput-object p1, p0, Lcom/sec/chaton/a/db;->h:Ljava/lang/Object;

    .line 9355
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->a()Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->a()Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->b()Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->b()Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->b()Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->b()Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/da;
    .locals 2

    .prologue
    .line 8977
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->e()Lcom/sec/chaton/a/da;

    move-result-object v0

    .line 8978
    invoke-virtual {v0}, Lcom/sec/chaton/a/da;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8979
    invoke-static {v0}, Lcom/sec/chaton/a/db;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 8981
    :cond_0
    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9398
    if-nez p1, :cond_0

    .line 9399
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9401
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/db;->j()V

    .line 9402
    iget-object v0, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 9404
    return-object p0
.end method

.method public e()Lcom/sec/chaton/a/da;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 8995
    new-instance v2, Lcom/sec/chaton/a/da;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/da;-><init>(Lcom/sec/chaton/a/db;Lcom/sec/chaton/a/b;)V

    .line 8996
    iget v3, p0, Lcom/sec/chaton/a/db;->a:I

    .line 8997
    const/4 v1, 0x0

    .line 8998
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    .line 9001
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/db;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/da;->a(Lcom/sec/chaton/a/da;J)J

    .line 9002
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 9003
    or-int/lit8 v0, v0, 0x2

    .line 9005
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/db;->c:Lcom/sec/chaton/a/dp;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->a(Lcom/sec/chaton/a/da;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;

    .line 9006
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 9007
    or-int/lit8 v0, v0, 0x4

    .line 9009
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/db;->d:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->a(Lcom/sec/chaton/a/da;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 9010
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 9011
    or-int/lit8 v0, v0, 0x8

    .line 9013
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/db;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->a(Lcom/sec/chaton/a/da;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9014
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 9015
    or-int/lit8 v0, v0, 0x10

    .line 9017
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/db;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->b(Lcom/sec/chaton/a/da;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9018
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 9019
    or-int/lit8 v0, v0, 0x20

    .line 9021
    :cond_4
    iget-wide v4, p0, Lcom/sec/chaton/a/db;->g:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/da;->b(Lcom/sec/chaton/a/da;J)J

    .line 9022
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 9023
    or-int/lit8 v0, v0, 0x40

    .line 9025
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/a/db;->h:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->c(Lcom/sec/chaton/a/da;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9026
    iget v1, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 9027
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    .line 9029
    iget v1, p0, Lcom/sec/chaton/a/db;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9031
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/a/db;->i:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->a(Lcom/sec/chaton/a/da;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 9032
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 9033
    or-int/lit16 v0, v0, 0x80

    .line 9035
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/a/db;->j:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->d(Lcom/sec/chaton/a/da;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9036
    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    .line 9037
    or-int/lit16 v0, v0, 0x100

    .line 9039
    :cond_8
    iget-boolean v1, p0, Lcom/sec/chaton/a/db;->k:Z

    invoke-static {v2, v1}, Lcom/sec/chaton/a/da;->a(Lcom/sec/chaton/a/da;Z)Z

    .line 9040
    invoke-static {v2, v0}, Lcom/sec/chaton/a/da;->a(Lcom/sec/chaton/a/da;I)I

    .line 9041
    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public e(Ljava/lang/String;)Lcom/sec/chaton/a/db;
    .locals 1

    .prologue
    .line 9441
    if-nez p1, :cond_0

    .line 9442
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9444
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/db;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sec/chaton/a/db;->a:I

    .line 9445
    iput-object p1, p0, Lcom/sec/chaton/a/db;->j:Ljava/lang/Object;

    .line 9447
    return-object p0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->c()Lcom/sec/chaton/a/da;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0}, Lcom/sec/chaton/a/db;->c()Lcom/sec/chaton/a/da;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 9087
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/db;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    check-cast p1, Lcom/sec/chaton/a/da;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/db;->a(Lcom/sec/chaton/a/da;)Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8928
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/db;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/db;

    move-result-object v0

    return-object v0
.end method
