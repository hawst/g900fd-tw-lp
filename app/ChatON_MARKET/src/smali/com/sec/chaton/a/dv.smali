.class public final Lcom/sec/chaton/a/dv;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/du;",
        "Lcom/sec/chaton/a/dv;",
        ">;",
        "Lcom/sec/chaton/a/dw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dr;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20899
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 21057
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    .line 21146
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    .line 20900
    invoke-direct {p0}, Lcom/sec/chaton/a/dv;->i()V

    .line 20901
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dv;)Lcom/sec/chaton/a/du;
    .locals 1

    .prologue
    .line 20894
    invoke-direct {p0}, Lcom/sec/chaton/a/dv;->k()Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 20894
    invoke-static {}, Lcom/sec/chaton/a/dv;->j()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 20904
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 20906
    new-instance v0, Lcom/sec/chaton/a/dv;

    invoke-direct {v0}, Lcom/sec/chaton/a/dv;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/du;
    .locals 2

    .prologue
    .line 20938
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->e()Lcom/sec/chaton/a/du;

    move-result-object v0

    .line 20939
    invoke-virtual {v0}, Lcom/sec/chaton/a/du;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20940
    invoke-static {v0}, Lcom/sec/chaton/a/dv;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 20943
    :cond_0
    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 21060
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 21061
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    .line 21062
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 21064
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/dv;
    .locals 2

    .prologue
    .line 20910
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 20911
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/dv;->b:J

    .line 20912
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 20913
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    .line 20914
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 20915
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    .line 20916
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 20917
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 21044
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 21045
    iput-wide p1, p0, Lcom/sec/chaton/a/dv;->b:J

    .line 21047
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dv;
    .locals 2

    .prologue
    .line 20997
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 20998
    sparse-switch v0, :sswitch_data_0

    .line 21003
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/dv;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21005
    :sswitch_0
    return-object p0

    .line 21010
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 21011
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/dv;->b:J

    goto :goto_0

    .line 21015
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/dr;->newBuilder()Lcom/sec/chaton/a/ds;

    move-result-object v0

    .line 21016
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 21017
    invoke-virtual {v0}, Lcom/sec/chaton/a/ds;->e()Lcom/sec/chaton/a/dr;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dv;->a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/dv;

    goto :goto_0

    .line 21021
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 21022
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 21023
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 21025
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 21026
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dv;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/dv;

    goto :goto_0

    .line 20998
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dr;)Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 21093
    if-nez p1, :cond_0

    .line 21094
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21096
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/dv;->l()V

    .line 21097
    iget-object v0, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21099
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/du;)Lcom/sec/chaton/a/dv;
    .locals 2

    .prologue
    .line 20968
    invoke-static {}, Lcom/sec/chaton/a/du;->a()Lcom/sec/chaton/a/du;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 20985
    :cond_0
    :goto_0
    return-object p0

    .line 20969
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/du;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20970
    invoke-virtual {p1}, Lcom/sec/chaton/a/du;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/dv;->a(J)Lcom/sec/chaton/a/dv;

    .line 20972
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/a/du;->b(Lcom/sec/chaton/a/du;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 20973
    iget-object v0, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 20974
    invoke-static {p1}, Lcom/sec/chaton/a/du;->b(Lcom/sec/chaton/a/du;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    .line 20975
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 20982
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/du;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20983
    invoke-virtual {p1}, Lcom/sec/chaton/a/du;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dv;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/dv;

    goto :goto_0

    .line 20977
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/a/dv;->l()V

    .line 20978
    iget-object v0, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/du;->b(Lcom/sec/chaton/a/du;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 21154
    if-nez p1, :cond_0

    .line 21155
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21157
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    .line 21159
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 21160
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/dv;
    .locals 2

    .prologue
    .line 20921
    invoke-static {}, Lcom/sec/chaton/a/dv;->j()Lcom/sec/chaton/a/dv;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->e()Lcom/sec/chaton/a/du;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/dv;->a(Lcom/sec/chaton/a/du;)Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/dv;
    .locals 2

    .prologue
    .line 21170
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 21172
    iget-object v0, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    .line 21178
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 21179
    return-object p0

    .line 21175
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->d()Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->e()Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/du;
    .locals 1

    .prologue
    .line 20925
    invoke-static {}, Lcom/sec/chaton/a/du;->a()Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->a()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->a()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->b()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->b()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->b()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->b()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/du;
    .locals 2

    .prologue
    .line 20929
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->e()Lcom/sec/chaton/a/du;

    move-result-object v0

    .line 20930
    invoke-virtual {v0}, Lcom/sec/chaton/a/du;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20931
    invoke-static {v0}, Lcom/sec/chaton/a/dv;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 20933
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/du;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 20947
    new-instance v2, Lcom/sec/chaton/a/du;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/du;-><init>(Lcom/sec/chaton/a/dv;Lcom/sec/chaton/a/b;)V

    .line 20948
    iget v3, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 20949
    const/4 v1, 0x0

    .line 20950
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 20953
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/dv;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/du;->a(Lcom/sec/chaton/a/du;J)J

    .line 20954
    iget v1, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 20955
    iget-object v1, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    .line 20956
    iget v1, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/chaton/a/dv;->a:I

    .line 20958
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/dv;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/du;->a(Lcom/sec/chaton/a/du;Ljava/util/List;)Ljava/util/List;

    .line 20959
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 20960
    or-int/lit8 v0, v0, 0x2

    .line 20962
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/du;->a(Lcom/sec/chaton/a/du;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 20963
    invoke-static {v2, v0}, Lcom/sec/chaton/a/du;->a(Lcom/sec/chaton/a/du;I)I

    .line 20964
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 21148
    iget v0, p0, Lcom/sec/chaton/a/dv;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 21151
    iget-object v0, p0, Lcom/sec/chaton/a/dv;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->c()Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0}, Lcom/sec/chaton/a/dv;->c()Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 20989
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dv;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    check-cast p1, Lcom/sec/chaton/a/du;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/dv;->a(Lcom/sec/chaton/a/du;)Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20894
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dv;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method
