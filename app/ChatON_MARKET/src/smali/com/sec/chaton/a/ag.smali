.class public final Lcom/sec/chaton/a/ag;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ai;


# static fields
.field private static final a:Lcom/sec/chaton/a/ag;


# instance fields
.field private b:I

.field private c:Lcom/sec/chaton/a/bc;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/protobuf/LazyStringList;

.field private f:J

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7119
    new-instance v0, Lcom/sec/chaton/a/ag;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ag;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ag;->a:Lcom/sec/chaton/a/ag;

    .line 7120
    sget-object v0, Lcom/sec/chaton/a/ag;->a:Lcom/sec/chaton/a/ag;

    invoke-direct {v0}, Lcom/sec/chaton/a/ag;->m()V

    .line 7121
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ah;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6599
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 6685
    iput-byte v0, p0, Lcom/sec/chaton/a/ag;->g:B

    .line 6711
    iput v0, p0, Lcom/sec/chaton/a/ag;->h:I

    .line 6600
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ah;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 6594
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ag;-><init>(Lcom/sec/chaton/a/ah;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6601
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 6685
    iput-byte v0, p0, Lcom/sec/chaton/a/ag;->g:B

    .line 6711
    iput v0, p0, Lcom/sec/chaton/a/ag;->h:I

    .line 6601
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ag;I)I
    .locals 0

    .prologue
    .line 6594
    iput p1, p0, Lcom/sec/chaton/a/ag;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ag;J)J
    .locals 0

    .prologue
    .line 6594
    iput-wide p1, p0, Lcom/sec/chaton/a/ag;->f:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ag;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 6594
    iput-object p1, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/ag;
    .locals 1

    .prologue
    .line 6605
    sget-object v0, Lcom/sec/chaton/a/ag;->a:Lcom/sec/chaton/a/ag;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 6819
    invoke-static {}, Lcom/sec/chaton/a/ag;->newBuilder()Lcom/sec/chaton/a/ah;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ah;->a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ag;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 6594
    iput-object p1, p0, Lcom/sec/chaton/a/ag;->c:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ag;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 6594
    iput-object p1, p0, Lcom/sec/chaton/a/ag;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ag;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 6594
    iget-object v0, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method private l()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6644
    iget-object v0, p0, Lcom/sec/chaton/a/ag;->d:Ljava/lang/Object;

    .line 6645
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6646
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6648
    iput-object v0, p0, Lcom/sec/chaton/a/ag;->d:Ljava/lang/Object;

    .line 6651
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 6680
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/ag;->c:Lcom/sec/chaton/a/bc;

    .line 6681
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ag;->d:Ljava/lang/Object;

    .line 6682
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    .line 6683
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ag;->f:J

    .line 6684
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 6816
    invoke-static {}, Lcom/sec/chaton/a/ah;->f()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ag;
    .locals 1

    .prologue
    .line 6609
    sget-object v0, Lcom/sec/chaton/a/ag;->a:Lcom/sec/chaton/a/ag;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6617
    iget v1, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 6620
    iget-object v0, p0, Lcom/sec/chaton/a/ag;->c:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 6627
    iget v0, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6630
    iget-object v0, p0, Lcom/sec/chaton/a/ag;->d:Ljava/lang/Object;

    .line 6631
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6632
    check-cast v0, Ljava/lang/String;

    .line 6640
    :goto_0
    return-object v0

    .line 6634
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6636
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6637
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6638
    iput-object v1, p0, Lcom/sec/chaton/a/ag;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6640
    goto :goto_0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6660
    iget-object v0, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6594
    invoke-virtual {p0}, Lcom/sec/chaton/a/ag;->b()Lcom/sec/chaton/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 6713
    iget v0, p0, Lcom/sec/chaton/a/ag;->h:I

    .line 6714
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 6739
    :goto_0
    return v0

    .line 6717
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 6718
    iget-object v0, p0, Lcom/sec/chaton/a/ag;->c:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 6721
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 6722
    invoke-direct {p0}, Lcom/sec/chaton/a/ag;->l()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    .line 6727
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 6728
    iget-object v3, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6727
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6731
    :cond_2
    add-int/2addr v0, v2

    .line 6732
    invoke-virtual {p0}, Lcom/sec/chaton/a/ag;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6734
    iget v1, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    .line 6735
    iget-wide v1, p0, Lcom/sec/chaton/a/ag;->f:J

    invoke-static {v5, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6738
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/ag;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 6673
    iget v0, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 6676
    iget-wide v0, p0, Lcom/sec/chaton/a/ag;->f:J

    return-wide v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 6687
    iget-byte v1, p0, Lcom/sec/chaton/a/ag;->g:B

    .line 6688
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 6691
    :goto_0
    return v0

    .line 6688
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6690
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ag;->g:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 6817
    invoke-static {}, Lcom/sec/chaton/a/ag;->newBuilder()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/sec/chaton/a/ah;
    .locals 1

    .prologue
    .line 6821
    invoke-static {p0}, Lcom/sec/chaton/a/ag;->a(Lcom/sec/chaton/a/ag;)Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6594
    invoke-virtual {p0}, Lcom/sec/chaton/a/ag;->j()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6594
    invoke-virtual {p0}, Lcom/sec/chaton/a/ag;->k()Lcom/sec/chaton/a/ah;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6746
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 6696
    invoke-virtual {p0}, Lcom/sec/chaton/a/ag;->getSerializedSize()I

    .line 6697
    iget v0, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 6698
    iget-object v0, p0, Lcom/sec/chaton/a/ag;->c:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 6700
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 6701
    invoke-direct {p0}, Lcom/sec/chaton/a/ag;->l()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6703
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 6704
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/chaton/a/ag;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6706
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/ag;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 6707
    iget-wide v0, p0, Lcom/sec/chaton/a/ag;->f:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 6709
    :cond_3
    return-void
.end method
