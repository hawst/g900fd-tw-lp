.class public final Lcom/sec/chaton/a/cg;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ch;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/cf;",
        "Lcom/sec/chaton/a/cg;",
        ">;",
        "Lcom/sec/chaton/a/ch;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/sec/chaton/a/cr;

.field private d:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29369
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 29497
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cg;->b:Ljava/lang/Object;

    .line 29533
    invoke-static {}, Lcom/sec/chaton/a/cr;->a()Lcom/sec/chaton/a/cr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    .line 29370
    invoke-direct {p0}, Lcom/sec/chaton/a/cg;->i()V

    .line 29371
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cg;)Lcom/sec/chaton/a/cf;
    .locals 1

    .prologue
    .line 29364
    invoke-direct {p0}, Lcom/sec/chaton/a/cg;->k()Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29364
    invoke-static {}, Lcom/sec/chaton/a/cg;->j()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 29374
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29376
    new-instance v0, Lcom/sec/chaton/a/cg;

    invoke-direct {v0}, Lcom/sec/chaton/a/cg;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/cf;
    .locals 2

    .prologue
    .line 29408
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->e()Lcom/sec/chaton/a/cf;

    move-result-object v0

    .line 29409
    invoke-virtual {v0}, Lcom/sec/chaton/a/cf;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29410
    invoke-static {v0}, Lcom/sec/chaton/a/cg;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 29413
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29380
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 29381
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cg;->b:Ljava/lang/Object;

    .line 29382
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29383
    invoke-static {}, Lcom/sec/chaton/a/cr;->a()Lcom/sec/chaton/a/cr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    .line 29384
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/cg;->d:Z

    .line 29386
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29387
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cg;
    .locals 2

    .prologue
    .line 29459
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 29460
    sparse-switch v0, :sswitch_data_0

    .line 29465
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/cg;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29467
    :sswitch_0
    return-object p0

    .line 29472
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29473
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cg;->b:Ljava/lang/Object;

    goto :goto_0

    .line 29477
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/cr;->newBuilder()Lcom/sec/chaton/a/cs;

    move-result-object v0

    .line 29478
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29479
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->g()Lcom/sec/chaton/a/cr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cs;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;

    .line 29481
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 29482
    invoke-virtual {v0}, Lcom/sec/chaton/a/cs;->e()Lcom/sec/chaton/a/cr;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cg;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cg;

    goto :goto_0

    .line 29486
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29487
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/a/cg;->d:Z

    goto :goto_0

    .line 29460
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/cf;)Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29437
    invoke-static {}, Lcom/sec/chaton/a/cf;->a()Lcom/sec/chaton/a/cf;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 29447
    :cond_0
    :goto_0
    return-object p0

    .line 29438
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/cf;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29439
    invoke-virtual {p1}, Lcom/sec/chaton/a/cf;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cg;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cg;

    .line 29441
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/cf;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29442
    invoke-virtual {p1}, Lcom/sec/chaton/a/cf;->f()Lcom/sec/chaton/a/cr;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cg;->b(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cg;

    .line 29444
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/cf;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29445
    invoke-virtual {p1}, Lcom/sec/chaton/a/cf;->h()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cg;->a(Z)Lcom/sec/chaton/a/cg;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29541
    if-nez p1, :cond_0

    .line 29542
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29544
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    .line 29546
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29547
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29512
    if-nez p1, :cond_0

    .line 29513
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29515
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29516
    iput-object p1, p0, Lcom/sec/chaton/a/cg;->b:Ljava/lang/Object;

    .line 29518
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/cg;
    .locals 1

    .prologue
    .line 29584
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29585
    iput-boolean p1, p0, Lcom/sec/chaton/a/cg;->d:Z

    .line 29587
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/cg;
    .locals 2

    .prologue
    .line 29391
    invoke-static {}, Lcom/sec/chaton/a/cg;->j()Lcom/sec/chaton/a/cg;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->e()Lcom/sec/chaton/a/cf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cg;->a(Lcom/sec/chaton/a/cf;)Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cg;
    .locals 2

    .prologue
    .line 29557
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    invoke-static {}, Lcom/sec/chaton/a/cr;->a()Lcom/sec/chaton/a/cr;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 29559
    iget-object v0, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    invoke-static {v0}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/cs;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/cs;->e()Lcom/sec/chaton/a/cr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    .line 29565
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29566
    return-object p0

    .line 29562
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->d()Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->e()Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/cf;
    .locals 1

    .prologue
    .line 29395
    invoke-static {}, Lcom/sec/chaton/a/cf;->a()Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->a()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->a()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->b()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->b()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->b()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->b()Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/cf;
    .locals 2

    .prologue
    .line 29399
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->e()Lcom/sec/chaton/a/cf;

    move-result-object v0

    .line 29400
    invoke-virtual {v0}, Lcom/sec/chaton/a/cf;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29401
    invoke-static {v0}, Lcom/sec/chaton/a/cg;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 29403
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/cf;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 29417
    new-instance v2, Lcom/sec/chaton/a/cf;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/cf;-><init>(Lcom/sec/chaton/a/cg;Lcom/sec/chaton/a/b;)V

    .line 29418
    iget v3, p0, Lcom/sec/chaton/a/cg;->a:I

    .line 29419
    const/4 v1, 0x0

    .line 29420
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 29423
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/cg;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cf;->a(Lcom/sec/chaton/a/cf;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29424
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 29425
    or-int/lit8 v0, v0, 0x2

    .line 29427
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cf;->a(Lcom/sec/chaton/a/cf;Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cr;

    .line 29428
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 29429
    or-int/lit8 v0, v0, 0x4

    .line 29431
    :cond_1
    iget-boolean v1, p0, Lcom/sec/chaton/a/cg;->d:Z

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cf;->a(Lcom/sec/chaton/a/cf;Z)Z

    .line 29432
    invoke-static {v2, v0}, Lcom/sec/chaton/a/cf;->a(Lcom/sec/chaton/a/cf;I)I

    .line 29433
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 29535
    iget v0, p0, Lcom/sec/chaton/a/cg;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/cr;
    .locals 1

    .prologue
    .line 29538
    iget-object v0, p0, Lcom/sec/chaton/a/cg;->c:Lcom/sec/chaton/a/cr;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->c()Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0}, Lcom/sec/chaton/a/cg;->c()Lcom/sec/chaton/a/cf;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 29451
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cg;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    check-cast p1, Lcom/sec/chaton/a/cf;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/cg;->a(Lcom/sec/chaton/a/cf;)Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29364
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cg;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cg;

    move-result-object v0

    return-object v0
.end method
