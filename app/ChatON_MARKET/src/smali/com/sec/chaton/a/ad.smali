.class public final Lcom/sec/chaton/a/ad;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/af;


# static fields
.field private static final a:Lcom/sec/chaton/a/ad;


# instance fields
.field private b:I

.field private c:J

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/ag;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24493
    new-instance v0, Lcom/sec/chaton/a/ad;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ad;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ad;->a:Lcom/sec/chaton/a/ad;

    .line 24494
    sget-object v0, Lcom/sec/chaton/a/ad;->a:Lcom/sec/chaton/a/ad;

    invoke-direct {v0}, Lcom/sec/chaton/a/ad;->m()V

    .line 24495
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ae;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 23908
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 24023
    iput-byte v0, p0, Lcom/sec/chaton/a/ad;->g:B

    .line 24049
    iput v0, p0, Lcom/sec/chaton/a/ad;->h:I

    .line 23909
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ae;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 23903
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ad;-><init>(Lcom/sec/chaton/a/ae;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 23910
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24023
    iput-byte v0, p0, Lcom/sec/chaton/a/ad;->g:B

    .line 24049
    iput v0, p0, Lcom/sec/chaton/a/ad;->h:I

    .line 23910
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ad;I)I
    .locals 0

    .prologue
    .line 23903
    iput p1, p0, Lcom/sec/chaton/a/ad;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ad;J)J
    .locals 0

    .prologue
    .line 23903
    iput-wide p1, p0, Lcom/sec/chaton/a/ad;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/ad;
    .locals 1

    .prologue
    .line 23914
    sget-object v0, Lcom/sec/chaton/a/ad;->a:Lcom/sec/chaton/a/ad;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/ad;
    .locals 1

    .prologue
    .line 24096
    invoke-static {}, Lcom/sec/chaton/a/ad;->newBuilder()Lcom/sec/chaton/a/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ae;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/ae;

    invoke-static {v0}, Lcom/sec/chaton/a/ae;->a(Lcom/sec/chaton/a/ae;)Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ad;)Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24152
    invoke-static {}, Lcom/sec/chaton/a/ad;->newBuilder()Lcom/sec/chaton/a/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ae;->a(Lcom/sec/chaton/a/ad;)Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ad;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23903
    iput-object p1, p0, Lcom/sec/chaton/a/ad;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ad;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 23903
    iput-object p1, p0, Lcom/sec/chaton/a/ad;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ad;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23903
    iput-object p1, p0, Lcom/sec/chaton/a/ad;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ad;)Ljava/util/List;
    .locals 1

    .prologue
    .line 23903
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->f:Ljava/util/List;

    return-object v0
.end method

.method private k()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 23953
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->d:Ljava/lang/Object;

    .line 23954
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23955
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 23957
    iput-object v0, p0, Lcom/sec/chaton/a/ad;->d:Ljava/lang/Object;

    .line 23960
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private l()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 23985
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->e:Ljava/lang/Object;

    .line 23986
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23987
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 23989
    iput-object v0, p0, Lcom/sec/chaton/a/ad;->e:Ljava/lang/Object;

    .line 23992
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 24018
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ad;->c:J

    .line 24019
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ad;->d:Ljava/lang/Object;

    .line 24020
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ad;->e:Ljava/lang/Object;

    .line 24021
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ad;->f:Ljava/util/List;

    .line 24022
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24149
    invoke-static {}, Lcom/sec/chaton/a/ae;->f()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ad;
    .locals 1

    .prologue
    .line 23918
    sget-object v0, Lcom/sec/chaton/a/ad;->a:Lcom/sec/chaton/a/ad;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 23926
    iget v1, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 23929
    iget-wide v0, p0, Lcom/sec/chaton/a/ad;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 23936
    iget v0, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23939
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->d:Ljava/lang/Object;

    .line 23940
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23941
    check-cast v0, Ljava/lang/String;

    .line 23949
    :goto_0
    return-object v0

    .line 23943
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 23945
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 23946
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23947
    iput-object v1, p0, Lcom/sec/chaton/a/ad;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 23949
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 23968
    iget v0, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23903
    invoke-virtual {p0}, Lcom/sec/chaton/a/ad;->b()Lcom/sec/chaton/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 24051
    iget v2, p0, Lcom/sec/chaton/a/ad;->h:I

    .line 24052
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 24072
    :goto_0
    return v2

    .line 24055
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 24056
    iget-wide v2, p0, Lcom/sec/chaton/a/ad;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 24059
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 24060
    invoke-direct {p0}, Lcom/sec/chaton/a/ad;->k()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 24063
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 24064
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ad;->l()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    move v2, v0

    .line 24067
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 24068
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v6, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 24067
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 24071
    :cond_3
    iput v2, p0, Lcom/sec/chaton/a/ad;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23971
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->e:Ljava/lang/Object;

    .line 23972
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23973
    check-cast v0, Ljava/lang/String;

    .line 23981
    :goto_0
    return-object v0

    .line 23975
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 23977
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 23978
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23979
    iput-object v1, p0, Lcom/sec/chaton/a/ad;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 23981
    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24150
    invoke-static {}, Lcom/sec/chaton/a/ad;->newBuilder()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 24025
    iget-byte v1, p0, Lcom/sec/chaton/a/ad;->g:B

    .line 24026
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 24029
    :goto_0
    return v0

    .line 24026
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 24028
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ad;->g:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/ae;
    .locals 1

    .prologue
    .line 24154
    invoke-static {p0}, Lcom/sec/chaton/a/ad;->a(Lcom/sec/chaton/a/ad;)Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23903
    invoke-virtual {p0}, Lcom/sec/chaton/a/ad;->i()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23903
    invoke-virtual {p0}, Lcom/sec/chaton/a/ad;->j()Lcom/sec/chaton/a/ae;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24079
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 24034
    invoke-virtual {p0}, Lcom/sec/chaton/a/ad;->getSerializedSize()I

    .line 24035
    iget v0, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 24036
    iget-wide v0, p0, Lcom/sec/chaton/a/ad;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 24038
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 24039
    invoke-direct {p0}, Lcom/sec/chaton/a/ad;->k()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 24041
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/ad;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 24042
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ad;->l()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 24044
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 24045
    iget-object v0, p0, Lcom/sec/chaton/a/ad;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 24044
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 24047
    :cond_3
    return-void
.end method
