.class public final Lcom/sec/chaton/a/bu;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bw;


# static fields
.field private static final a:Lcom/sec/chaton/a/bu;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/bx;

.field private e:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18703
    new-instance v0, Lcom/sec/chaton/a/bu;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/bu;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/bu;->a:Lcom/sec/chaton/a/bu;

    .line 18704
    sget-object v0, Lcom/sec/chaton/a/bu;->a:Lcom/sec/chaton/a/bu;

    invoke-direct {v0}, Lcom/sec/chaton/a/bu;->l()V

    .line 18705
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/bv;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18289
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 18360
    iput-byte v0, p0, Lcom/sec/chaton/a/bu;->f:B

    .line 18383
    iput v0, p0, Lcom/sec/chaton/a/bu;->g:I

    .line 18290
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/bv;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 18284
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/bu;-><init>(Lcom/sec/chaton/a/bv;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18291
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 18360
    iput-byte v0, p0, Lcom/sec/chaton/a/bu;->f:B

    .line 18383
    iput v0, p0, Lcom/sec/chaton/a/bu;->g:I

    .line 18291
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bu;I)I
    .locals 0

    .prologue
    .line 18284
    iput p1, p0, Lcom/sec/chaton/a/bu;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bu;J)J
    .locals 0

    .prologue
    .line 18284
    iput-wide p1, p0, Lcom/sec/chaton/a/bu;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/bu;
    .locals 1

    .prologue
    .line 18295
    sget-object v0, Lcom/sec/chaton/a/bu;->a:Lcom/sec/chaton/a/bu;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/bu;
    .locals 1

    .prologue
    .line 18426
    invoke-static {}, Lcom/sec/chaton/a/bu;->newBuilder()Lcom/sec/chaton/a/bv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bv;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bv;

    invoke-static {v0}, Lcom/sec/chaton/a/bv;->a(Lcom/sec/chaton/a/bv;)Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/bu;)Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18482
    invoke-static {}, Lcom/sec/chaton/a/bu;->newBuilder()Lcom/sec/chaton/a/bv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bv;->a(Lcom/sec/chaton/a/bu;)Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/bu;Lcom/sec/chaton/a/bx;)Lcom/sec/chaton/a/bx;
    .locals 0

    .prologue
    .line 18284
    iput-object p1, p0, Lcom/sec/chaton/a/bu;->d:Lcom/sec/chaton/a/bx;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/bu;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 18284
    iput-object p1, p0, Lcom/sec/chaton/a/bu;->e:Ljava/lang/Object;

    return-object p1
.end method

.method private k()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 18344
    iget-object v0, p0, Lcom/sec/chaton/a/bu;->e:Ljava/lang/Object;

    .line 18345
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18346
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 18348
    iput-object v0, p0, Lcom/sec/chaton/a/bu;->e:Ljava/lang/Object;

    .line 18351
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 18356
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bu;->c:J

    .line 18357
    sget-object v0, Lcom/sec/chaton/a/bx;->a:Lcom/sec/chaton/a/bx;

    iput-object v0, p0, Lcom/sec/chaton/a/bu;->d:Lcom/sec/chaton/a/bx;

    .line 18358
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bu;->e:Ljava/lang/Object;

    .line 18359
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18479
    invoke-static {}, Lcom/sec/chaton/a/bv;->f()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/bu;
    .locals 1

    .prologue
    .line 18299
    sget-object v0, Lcom/sec/chaton/a/bu;->a:Lcom/sec/chaton/a/bu;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 18307
    iget v1, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 18310
    iget-wide v0, p0, Lcom/sec/chaton/a/bu;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 18317
    iget v0, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bx;
    .locals 1

    .prologue
    .line 18320
    iget-object v0, p0, Lcom/sec/chaton/a/bu;->d:Lcom/sec/chaton/a/bx;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 18327
    iget v0, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18284
    invoke-virtual {p0}, Lcom/sec/chaton/a/bu;->b()Lcom/sec/chaton/a/bu;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 18385
    iget v0, p0, Lcom/sec/chaton/a/bu;->g:I

    .line 18386
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 18402
    :goto_0
    return v0

    .line 18388
    :cond_0
    const/4 v0, 0x0

    .line 18389
    iget v1, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 18390
    iget-wide v1, p0, Lcom/sec/chaton/a/bu;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 18393
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 18394
    iget-object v1, p0, Lcom/sec/chaton/a/bu;->d:Lcom/sec/chaton/a/bx;

    invoke-virtual {v1}, Lcom/sec/chaton/a/bx;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 18397
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 18398
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/bu;->k()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18401
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/bu;->g:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18330
    iget-object v0, p0, Lcom/sec/chaton/a/bu;->e:Ljava/lang/Object;

    .line 18331
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18332
    check-cast v0, Ljava/lang/String;

    .line 18340
    :goto_0
    return-object v0

    .line 18334
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 18336
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 18337
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18338
    iput-object v1, p0, Lcom/sec/chaton/a/bu;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 18340
    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18480
    invoke-static {}, Lcom/sec/chaton/a/bu;->newBuilder()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 18362
    iget-byte v1, p0, Lcom/sec/chaton/a/bu;->f:B

    .line 18363
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 18366
    :goto_0
    return v0

    .line 18363
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 18365
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/bu;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/bv;
    .locals 1

    .prologue
    .line 18484
    invoke-static {p0}, Lcom/sec/chaton/a/bu;->a(Lcom/sec/chaton/a/bu;)Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18284
    invoke-virtual {p0}, Lcom/sec/chaton/a/bu;->i()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18284
    invoke-virtual {p0}, Lcom/sec/chaton/a/bu;->j()Lcom/sec/chaton/a/bv;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18409
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 18371
    invoke-virtual {p0}, Lcom/sec/chaton/a/bu;->getSerializedSize()I

    .line 18372
    iget v0, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 18373
    iget-wide v0, p0, Lcom/sec/chaton/a/bu;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 18375
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 18376
    iget-object v0, p0, Lcom/sec/chaton/a/bu;->d:Lcom/sec/chaton/a/bx;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bx;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 18378
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/bu;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 18379
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/bu;->k()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 18381
    :cond_2
    return-void
.end method
