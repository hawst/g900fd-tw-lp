.class public final Lcom/sec/chaton/a/em;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/eo;


# static fields
.field private static final a:Lcom/sec/chaton/a/em;


# instance fields
.field private b:I

.field private c:J

.field private d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/ev;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/sec/chaton/a/ey;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26504
    new-instance v0, Lcom/sec/chaton/a/em;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/em;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/em;->a:Lcom/sec/chaton/a/em;

    .line 26505
    sget-object v0, Lcom/sec/chaton/a/em;->a:Lcom/sec/chaton/a/em;

    invoke-direct {v0}, Lcom/sec/chaton/a/em;->m()V

    .line 26506
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/en;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 25930
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 26023
    iput-byte v0, p0, Lcom/sec/chaton/a/em;->g:B

    .line 26049
    iput v0, p0, Lcom/sec/chaton/a/em;->h:I

    .line 25931
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/en;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 25925
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/em;-><init>(Lcom/sec/chaton/a/en;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 25932
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 26023
    iput-byte v0, p0, Lcom/sec/chaton/a/em;->g:B

    .line 26049
    iput v0, p0, Lcom/sec/chaton/a/em;->h:I

    .line 25932
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/em;I)I
    .locals 0

    .prologue
    .line 25925
    iput p1, p0, Lcom/sec/chaton/a/em;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/em;J)J
    .locals 0

    .prologue
    .line 25925
    iput-wide p1, p0, Lcom/sec/chaton/a/em;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/em;
    .locals 1

    .prologue
    .line 25936
    sget-object v0, Lcom/sec/chaton/a/em;->a:Lcom/sec/chaton/a/em;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/em;
    .locals 1

    .prologue
    .line 26096
    invoke-static {}, Lcom/sec/chaton/a/em;->newBuilder()Lcom/sec/chaton/a/en;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/en;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/en;

    invoke-static {v0}, Lcom/sec/chaton/a/en;->a(Lcom/sec/chaton/a/en;)Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/em;)Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26152
    invoke-static {}, Lcom/sec/chaton/a/em;->newBuilder()Lcom/sec/chaton/a/en;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/en;->a(Lcom/sec/chaton/a/em;)Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/em;Lcom/sec/chaton/a/ey;)Lcom/sec/chaton/a/ey;
    .locals 0

    .prologue
    .line 25925
    iput-object p1, p0, Lcom/sec/chaton/a/em;->f:Lcom/sec/chaton/a/ey;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/em;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 25925
    iput-object p1, p0, Lcom/sec/chaton/a/em;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/em;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 25925
    iput-object p1, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/em;)Ljava/util/List;
    .locals 1

    .prologue
    .line 25925
    iget-object v0, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    return-object v0
.end method

.method private l()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 25975
    iget-object v0, p0, Lcom/sec/chaton/a/em;->d:Ljava/lang/Object;

    .line 25976
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25977
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 25979
    iput-object v0, p0, Lcom/sec/chaton/a/em;->d:Ljava/lang/Object;

    .line 25982
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 26018
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/em;->c:J

    .line 26019
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/em;->d:Ljava/lang/Object;

    .line 26020
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    .line 26021
    invoke-static {}, Lcom/sec/chaton/a/ey;->a()Lcom/sec/chaton/a/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/em;->f:Lcom/sec/chaton/a/ey;

    .line 26022
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26149
    invoke-static {}, Lcom/sec/chaton/a/en;->h()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/em;
    .locals 1

    .prologue
    .line 25940
    sget-object v0, Lcom/sec/chaton/a/em;->a:Lcom/sec/chaton/a/em;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 25948
    iget v1, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 25951
    iget-wide v0, p0, Lcom/sec/chaton/a/em;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 25958
    iget v0, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25961
    iget-object v0, p0, Lcom/sec/chaton/a/em;->d:Ljava/lang/Object;

    .line 25962
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25963
    check-cast v0, Ljava/lang/String;

    .line 25971
    :goto_0
    return-object v0

    .line 25965
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 25967
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 25968
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25969
    iput-object v1, p0, Lcom/sec/chaton/a/em;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 25971
    goto :goto_0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/ev;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25990
    iget-object v0, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 25925
    invoke-virtual {p0}, Lcom/sec/chaton/a/em;->b()Lcom/sec/chaton/a/em;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 26051
    iget v2, p0, Lcom/sec/chaton/a/em;->h:I

    .line 26052
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 26072
    :goto_0
    return v2

    .line 26055
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 26056
    iget-wide v2, p0, Lcom/sec/chaton/a/em;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 26059
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 26060
    invoke-direct {p0}, Lcom/sec/chaton/a/em;->l()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v0

    .line 26063
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 26064
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 26063
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 26067
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 26068
    iget-object v0, p0, Lcom/sec/chaton/a/em;->f:Lcom/sec/chaton/a/ey;

    invoke-static {v6, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 26071
    :cond_3
    iput v2, p0, Lcom/sec/chaton/a/em;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 26011
    iget v0, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ey;
    .locals 1

    .prologue
    .line 26014
    iget-object v0, p0, Lcom/sec/chaton/a/em;->f:Lcom/sec/chaton/a/ey;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 26025
    iget-byte v1, p0, Lcom/sec/chaton/a/em;->g:B

    .line 26026
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 26029
    :goto_0
    return v0

    .line 26026
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 26028
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/em;->g:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26150
    invoke-static {}, Lcom/sec/chaton/a/em;->newBuilder()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/sec/chaton/a/en;
    .locals 1

    .prologue
    .line 26154
    invoke-static {p0}, Lcom/sec/chaton/a/em;->a(Lcom/sec/chaton/a/em;)Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 25925
    invoke-virtual {p0}, Lcom/sec/chaton/a/em;->j()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 25925
    invoke-virtual {p0}, Lcom/sec/chaton/a/em;->k()Lcom/sec/chaton/a/en;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26079
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 26034
    invoke-virtual {p0}, Lcom/sec/chaton/a/em;->getSerializedSize()I

    .line 26035
    iget v0, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 26036
    iget-wide v0, p0, Lcom/sec/chaton/a/em;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 26038
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 26039
    invoke-direct {p0}, Lcom/sec/chaton/a/em;->l()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 26041
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 26042
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/em;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 26041
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 26044
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/em;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 26045
    iget-object v0, p0, Lcom/sec/chaton/a/em;->f:Lcom/sec/chaton/a/ey;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 26047
    :cond_3
    return-void
.end method
