.class public final Lcom/sec/chaton/a/bh;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/bg;",
        "Lcom/sec/chaton/a/bh;",
        ">;",
        "Lcom/sec/chaton/a/bi;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/sec/chaton/a/bc;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 13760
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 13939
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->b:Ljava/lang/Object;

    .line 13975
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->c:Lcom/sec/chaton/a/bc;

    .line 13999
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    .line 14088
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->e:Ljava/lang/Object;

    .line 14145
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->g:Ljava/lang/Object;

    .line 13761
    invoke-direct {p0}, Lcom/sec/chaton/a/bh;->g()V

    .line 13762
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/bh;)Lcom/sec/chaton/a/bg;
    .locals 1

    .prologue
    .line 13755
    invoke-direct {p0}, Lcom/sec/chaton/a/bh;->i()Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13755
    invoke-static {}, Lcom/sec/chaton/a/bh;->h()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 13765
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13767
    new-instance v0, Lcom/sec/chaton/a/bh;

    invoke-direct {v0}, Lcom/sec/chaton/a/bh;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/bg;
    .locals 2

    .prologue
    .line 13805
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->e()Lcom/sec/chaton/a/bg;

    move-result-object v0

    .line 13806
    invoke-virtual {v0}, Lcom/sec/chaton/a/bg;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13807
    invoke-static {v0}, Lcom/sec/chaton/a/bh;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 13810
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 14002
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 14003
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    .line 14004
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 14006
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13771
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 13772
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->b:Ljava/lang/Object;

    .line 13773
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13774
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->c:Lcom/sec/chaton/a/bc;

    .line 13775
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13776
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    .line 13777
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13778
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->e:Ljava/lang/Object;

    .line 13779
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13780
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/bh;->f:Z

    .line 13781
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13782
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->g:Ljava/lang/Object;

    .line 13783
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13784
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bh;
    .locals 2

    .prologue
    .line 13885
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 13886
    sparse-switch v0, :sswitch_data_0

    .line 13891
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/bh;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13893
    :sswitch_0
    return-object p0

    .line 13898
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13899
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->b:Ljava/lang/Object;

    goto :goto_0

    .line 13903
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 13904
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 13905
    if-eqz v0, :cond_0

    .line 13906
    iget v1, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13907
    iput-object v0, p0, Lcom/sec/chaton/a/bh;->c:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 13912
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/cr;->newBuilder()Lcom/sec/chaton/a/cs;

    move-result-object v0

    .line 13913
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 13914
    invoke-virtual {v0}, Lcom/sec/chaton/a/cs;->e()Lcom/sec/chaton/a/cr;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bh;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/bh;

    goto :goto_0

    .line 13918
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13919
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->e:Ljava/lang/Object;

    goto :goto_0

    .line 13923
    :sswitch_5
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13924
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/a/bh;->f:Z

    goto :goto_0

    .line 13928
    :sswitch_6
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13929
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->g:Ljava/lang/Object;

    goto :goto_0

    .line 13886
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13983
    if-nez p1, :cond_0

    .line 13984
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13986
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13987
    iput-object p1, p0, Lcom/sec/chaton/a/bh;->c:Lcom/sec/chaton/a/bc;

    .line 13989
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/bg;)Lcom/sec/chaton/a/bh;
    .locals 2

    .prologue
    .line 13847
    invoke-static {}, Lcom/sec/chaton/a/bg;->a()Lcom/sec/chaton/a/bg;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 13873
    :cond_0
    :goto_0
    return-object p0

    .line 13848
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 13849
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bh;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bh;

    .line 13851
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 13852
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->f()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bh;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bh;

    .line 13854
    :cond_3
    invoke-static {p1}, Lcom/sec/chaton/a/bg;->b(Lcom/sec/chaton/a/bg;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 13855
    iget-object v0, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 13856
    invoke-static {p1}, Lcom/sec/chaton/a/bg;->b(Lcom/sec/chaton/a/bg;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    .line 13857
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13864
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 13865
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bh;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bh;

    .line 13867
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 13868
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bh;->a(Z)Lcom/sec/chaton/a/bh;

    .line 13870
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13871
    invoke-virtual {p1}, Lcom/sec/chaton/a/bg;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bh;->c(Ljava/lang/String;)Lcom/sec/chaton/a/bh;

    goto :goto_0

    .line 13859
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/a/bh;->j()V

    .line 13860
    iget-object v0, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/bg;->b(Lcom/sec/chaton/a/bg;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 14035
    if-nez p1, :cond_0

    .line 14036
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14038
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/bh;->j()V

    .line 14039
    iget-object v0, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 14041
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 13954
    if-nez p1, :cond_0

    .line 13955
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13957
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13958
    iput-object p1, p0, Lcom/sec/chaton/a/bh;->b:Ljava/lang/Object;

    .line 13960
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 14132
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 14133
    iput-boolean p1, p0, Lcom/sec/chaton/a/bh;->f:Z

    .line 14135
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/bh;
    .locals 2

    .prologue
    .line 13788
    invoke-static {}, Lcom/sec/chaton/a/bh;->h()Lcom/sec/chaton/a/bh;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->e()Lcom/sec/chaton/a/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bh;->a(Lcom/sec/chaton/a/bg;)Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 14103
    if-nez p1, :cond_0

    .line 14104
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14106
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 14107
    iput-object p1, p0, Lcom/sec/chaton/a/bh;->e:Ljava/lang/Object;

    .line 14109
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->d()Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->e()Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/bg;
    .locals 1

    .prologue
    .line 13792
    invoke-static {}, Lcom/sec/chaton/a/bg;->a()Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/bh;
    .locals 1

    .prologue
    .line 14160
    if-nez p1, :cond_0

    .line 14161
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14163
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 14164
    iput-object p1, p0, Lcom/sec/chaton/a/bh;->g:Ljava/lang/Object;

    .line 14166
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->a()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->a()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->b()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->b()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->b()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->b()Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/bg;
    .locals 2

    .prologue
    .line 13796
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->e()Lcom/sec/chaton/a/bg;

    move-result-object v0

    .line 13797
    invoke-virtual {v0}, Lcom/sec/chaton/a/bg;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13798
    invoke-static {v0}, Lcom/sec/chaton/a/bh;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 13800
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/bg;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 13814
    new-instance v2, Lcom/sec/chaton/a/bg;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/bg;-><init>(Lcom/sec/chaton/a/bh;Lcom/sec/chaton/a/b;)V

    .line 13815
    iget v3, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13816
    const/4 v1, 0x0

    .line 13817
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 13820
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/bh;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bg;->a(Lcom/sec/chaton/a/bg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13821
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 13822
    or-int/lit8 v0, v0, 0x2

    .line 13824
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/bh;->c:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bg;->a(Lcom/sec/chaton/a/bg;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 13825
    iget v1, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 13826
    iget-object v1, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    .line 13827
    iget v1, p0, Lcom/sec/chaton/a/bh;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/sec/chaton/a/bh;->a:I

    .line 13829
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/bh;->d:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bg;->a(Lcom/sec/chaton/a/bg;Ljava/util/List;)Ljava/util/List;

    .line 13830
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 13831
    or-int/lit8 v0, v0, 0x4

    .line 13833
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/bh;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bg;->b(Lcom/sec/chaton/a/bg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13834
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 13835
    or-int/lit8 v0, v0, 0x8

    .line 13837
    :cond_3
    iget-boolean v1, p0, Lcom/sec/chaton/a/bh;->f:Z

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bg;->a(Lcom/sec/chaton/a/bg;Z)Z

    .line 13838
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 13839
    or-int/lit8 v0, v0, 0x10

    .line 13841
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/bh;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bg;->c(Lcom/sec/chaton/a/bg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13842
    invoke-static {v2, v0}, Lcom/sec/chaton/a/bg;->a(Lcom/sec/chaton/a/bg;I)I

    .line 13843
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->c()Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0}, Lcom/sec/chaton/a/bh;->c()Lcom/sec/chaton/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 13877
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bh;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    check-cast p1, Lcom/sec/chaton/a/bg;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/bh;->a(Lcom/sec/chaton/a/bg;)Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13755
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bh;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bh;

    move-result-object v0

    return-object v0
.end method
