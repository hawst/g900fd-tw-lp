.class public final Lcom/sec/chaton/a/v;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/u;",
        "Lcom/sec/chaton/a/v;",
        ">;",
        "Lcom/sec/chaton/a/w;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/x;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16717
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 16854
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/v;->b:Ljava/lang/Object;

    .line 16890
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    .line 16979
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    .line 16718
    invoke-direct {p0}, Lcom/sec/chaton/a/v;->k()V

    .line 16719
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/v;)Lcom/sec/chaton/a/u;
    .locals 1

    .prologue
    .line 16712
    invoke-direct {p0}, Lcom/sec/chaton/a/v;->m()Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j()Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16712
    invoke-static {}, Lcom/sec/chaton/a/v;->l()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 16722
    return-void
.end method

.method private static l()Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16724
    new-instance v0, Lcom/sec/chaton/a/v;

    invoke-direct {v0}, Lcom/sec/chaton/a/v;-><init>()V

    return-object v0
.end method

.method private m()Lcom/sec/chaton/a/u;
    .locals 2

    .prologue
    .line 16756
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->e()Lcom/sec/chaton/a/u;

    move-result-object v0

    .line 16757
    invoke-virtual {v0}, Lcom/sec/chaton/a/u;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 16758
    invoke-static {v0}, Lcom/sec/chaton/a/v;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 16761
    :cond_0
    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 16893
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 16894
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    .line 16895
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16897
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16728
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 16729
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/v;->b:Ljava/lang/Object;

    .line 16730
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16731
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    .line 16732
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16733
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    .line 16734
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16735
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/v;
    .locals 2

    .prologue
    .line 16815
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 16816
    sparse-switch v0, :sswitch_data_0

    .line 16821
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/v;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16823
    :sswitch_0
    return-object p0

    .line 16828
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16829
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/v;->b:Ljava/lang/Object;

    goto :goto_0

    .line 16833
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/x;->newBuilder()Lcom/sec/chaton/a/y;

    move-result-object v0

    .line 16834
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 16835
    invoke-virtual {v0}, Lcom/sec/chaton/a/y;->e()Lcom/sec/chaton/a/x;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/v;->a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/v;

    goto :goto_0

    .line 16839
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 16840
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 16841
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->i()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 16843
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 16844
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/v;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/v;

    goto :goto_0

    .line 16816
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16987
    if-nez p1, :cond_0

    .line 16988
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16990
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    .line 16992
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16993
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/u;)Lcom/sec/chaton/a/v;
    .locals 2

    .prologue
    .line 16786
    invoke-static {}, Lcom/sec/chaton/a/u;->a()Lcom/sec/chaton/a/u;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 16803
    :cond_0
    :goto_0
    return-object p0

    .line 16787
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/u;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16788
    invoke-virtual {p1}, Lcom/sec/chaton/a/u;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/v;->a(Ljava/lang/String;)Lcom/sec/chaton/a/v;

    .line 16790
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/a/u;->b(Lcom/sec/chaton/a/u;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 16791
    iget-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 16792
    invoke-static {p1}, Lcom/sec/chaton/a/u;->b(Lcom/sec/chaton/a/u;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    .line 16793
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16800
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/u;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16801
    invoke-virtual {p1}, Lcom/sec/chaton/a/u;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/v;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/v;

    goto :goto_0

    .line 16795
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/a/v;->n()V

    .line 16796
    iget-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/u;->b(Lcom/sec/chaton/a/u;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16926
    if-nez p1, :cond_0

    .line 16927
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16929
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/v;->n()V

    .line 16930
    iget-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16932
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16869
    if-nez p1, :cond_0

    .line 16870
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16872
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16873
    iput-object p1, p0, Lcom/sec/chaton/a/v;->b:Ljava/lang/Object;

    .line 16875
    return-object p0
.end method

.method public a(I)Lcom/sec/chaton/a/x;
    .locals 1

    .prologue
    .line 16906
    iget-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/x;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/v;
    .locals 2

    .prologue
    .line 16739
    invoke-static {}, Lcom/sec/chaton/a/v;->l()Lcom/sec/chaton/a/v;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->e()Lcom/sec/chaton/a/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/v;->a(Lcom/sec/chaton/a/u;)Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/v;
    .locals 2

    .prologue
    .line 17003
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 17005
    iget-object v0, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    .line 17011
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/v;->a:I

    .line 17012
    return-object p0

    .line 17008
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->d()Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->e()Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/u;
    .locals 1

    .prologue
    .line 16743
    invoke-static {}, Lcom/sec/chaton/a/u;->a()Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->a()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->a()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->b()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->b()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->b()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->b()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/u;
    .locals 2

    .prologue
    .line 16747
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->e()Lcom/sec/chaton/a/u;

    move-result-object v0

    .line 16748
    invoke-virtual {v0}, Lcom/sec/chaton/a/u;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 16749
    invoke-static {v0}, Lcom/sec/chaton/a/v;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 16751
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/u;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 16765
    new-instance v2, Lcom/sec/chaton/a/u;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/u;-><init>(Lcom/sec/chaton/a/v;Lcom/sec/chaton/a/b;)V

    .line 16766
    iget v3, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16767
    const/4 v1, 0x0

    .line 16768
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 16771
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/v;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/u;->a(Lcom/sec/chaton/a/u;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16772
    iget v1, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 16773
    iget-object v1, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    .line 16774
    iget v1, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/chaton/a/v;->a:I

    .line 16776
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/u;->a(Lcom/sec/chaton/a/u;Ljava/util/List;)Ljava/util/List;

    .line 16777
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 16778
    or-int/lit8 v0, v0, 0x2

    .line 16780
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/u;->a(Lcom/sec/chaton/a/u;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 16781
    invoke-static {v2, v0}, Lcom/sec/chaton/a/u;->a(Lcom/sec/chaton/a/u;I)I

    .line 16782
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16900
    iget-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 16903
    iget-object v0, p0, Lcom/sec/chaton/a/v;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->c()Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0}, Lcom/sec/chaton/a/v;->c()Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 16981
    iget v0, p0, Lcom/sec/chaton/a/v;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 16984
    iget-object v0, p0, Lcom/sec/chaton/a/v;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 16807
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/v;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    check-cast p1, Lcom/sec/chaton/a/u;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/v;->a(Lcom/sec/chaton/a/u;)Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16712
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/v;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method
