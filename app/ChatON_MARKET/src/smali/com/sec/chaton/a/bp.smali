.class public final enum Lcom/sec/chaton/a/bp;
.super Ljava/lang/Enum;
.source "SSMGPB.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/bp;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/bp;

.field public static final enum b:Lcom/sec/chaton/a/bp;

.field private static c:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sec/chaton/a/bp;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic e:[Lcom/sec/chaton/a/bp;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 204
    new-instance v0, Lcom/sec/chaton/a/bp;

    const-string v1, "CLOSE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sec/chaton/a/bp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bp;->a:Lcom/sec/chaton/a/bp;

    .line 205
    new-instance v0, Lcom/sec/chaton/a/bp;

    const-string v1, "HOLD_ON"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sec/chaton/a/bp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/bp;->b:Lcom/sec/chaton/a/bp;

    .line 202
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/a/bp;

    sget-object v1, Lcom/sec/chaton/a/bp;->a:Lcom/sec/chaton/a/bp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/a/bp;->b:Lcom/sec/chaton/a/bp;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/a/bp;->e:[Lcom/sec/chaton/a/bp;

    .line 227
    new-instance v0, Lcom/sec/chaton/a/bq;

    invoke-direct {v0}, Lcom/sec/chaton/a/bq;-><init>()V

    sput-object v0, Lcom/sec/chaton/a/bp;->c:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 237
    iput p4, p0, Lcom/sec/chaton/a/bp;->d:I

    .line 238
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/a/bp;
    .locals 1

    .prologue
    .line 215
    packed-switch p0, :pswitch_data_0

    .line 218
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 216
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/a/bp;->a:Lcom/sec/chaton/a/bp;

    goto :goto_0

    .line 217
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/a/bp;->b:Lcom/sec/chaton/a/bp;

    goto :goto_0

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/bp;
    .locals 1

    .prologue
    .line 202
    const-class v0, Lcom/sec/chaton/a/bp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bp;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/bp;
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lcom/sec/chaton/a/bp;->e:[Lcom/sec/chaton/a/bp;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/bp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/bp;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/sec/chaton/a/bp;->d:I

    return v0
.end method
