.class public final Lcom/sec/chaton/a/an;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/am;",
        "Lcom/sec/chaton/a/an;",
        ">;",
        "Lcom/sec/chaton/a/ao;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/ej;

.field private d:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 28457
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 28606
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    .line 28458
    invoke-direct {p0}, Lcom/sec/chaton/a/an;->i()V

    .line 28459
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/an;)Lcom/sec/chaton/a/am;
    .locals 1

    .prologue
    .line 28452
    invoke-direct {p0}, Lcom/sec/chaton/a/an;->k()Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28452
    invoke-static {}, Lcom/sec/chaton/a/an;->j()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 28462
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28464
    new-instance v0, Lcom/sec/chaton/a/an;

    invoke-direct {v0}, Lcom/sec/chaton/a/an;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/am;
    .locals 2

    .prologue
    .line 28496
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->e()Lcom/sec/chaton/a/am;

    move-result-object v0

    .line 28497
    invoke-virtual {v0}, Lcom/sec/chaton/a/am;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28498
    invoke-static {v0}, Lcom/sec/chaton/a/an;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 28501
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/an;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 28468
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 28469
    iput-wide v1, p0, Lcom/sec/chaton/a/an;->b:J

    .line 28470
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28471
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    .line 28472
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28473
    iput-wide v1, p0, Lcom/sec/chaton/a/an;->d:J

    .line 28474
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28475
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28593
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28594
    iput-wide p1, p0, Lcom/sec/chaton/a/an;->b:J

    .line 28596
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/an;
    .locals 2

    .prologue
    .line 28547
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 28548
    sparse-switch v0, :sswitch_data_0

    .line 28553
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/an;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28555
    :sswitch_0
    return-object p0

    .line 28560
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28561
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/an;->b:J

    goto :goto_0

    .line 28565
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 28566
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28567
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 28569
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 28570
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/an;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/an;

    goto :goto_0

    .line 28574
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28575
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/an;->d:J

    goto :goto_0

    .line 28548
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/am;)Lcom/sec/chaton/a/an;
    .locals 2

    .prologue
    .line 28525
    invoke-static {}, Lcom/sec/chaton/a/am;->a()Lcom/sec/chaton/a/am;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 28535
    :cond_0
    :goto_0
    return-object p0

    .line 28526
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/am;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 28527
    invoke-virtual {p1}, Lcom/sec/chaton/a/am;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/an;->a(J)Lcom/sec/chaton/a/an;

    .line 28529
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/am;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 28530
    invoke-virtual {p1}, Lcom/sec/chaton/a/am;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/an;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/an;

    .line 28532
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/am;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28533
    invoke-virtual {p1}, Lcom/sec/chaton/a/am;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/an;->b(J)Lcom/sec/chaton/a/an;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28614
    if-nez p1, :cond_0

    .line 28615
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28617
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    .line 28619
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28620
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/an;
    .locals 2

    .prologue
    .line 28479
    invoke-static {}, Lcom/sec/chaton/a/an;->j()Lcom/sec/chaton/a/an;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->e()Lcom/sec/chaton/a/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/an;->a(Lcom/sec/chaton/a/am;)Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/an;
    .locals 1

    .prologue
    .line 28657
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28658
    iput-wide p1, p0, Lcom/sec/chaton/a/an;->d:J

    .line 28660
    return-object p0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/an;
    .locals 2

    .prologue
    .line 28630
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 28632
    iget-object v0, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    .line 28638
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28639
    return-object p0

    .line 28635
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->d()Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->e()Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/am;
    .locals 1

    .prologue
    .line 28483
    invoke-static {}, Lcom/sec/chaton/a/am;->a()Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->a()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->a()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->b()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->b()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->b()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->b()Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/am;
    .locals 2

    .prologue
    .line 28487
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->e()Lcom/sec/chaton/a/am;

    move-result-object v0

    .line 28488
    invoke-virtual {v0}, Lcom/sec/chaton/a/am;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28489
    invoke-static {v0}, Lcom/sec/chaton/a/an;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 28491
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/am;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 28505
    new-instance v2, Lcom/sec/chaton/a/am;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/am;-><init>(Lcom/sec/chaton/a/an;Lcom/sec/chaton/a/b;)V

    .line 28506
    iget v3, p0, Lcom/sec/chaton/a/an;->a:I

    .line 28507
    const/4 v1, 0x0

    .line 28508
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 28511
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/an;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/am;->a(Lcom/sec/chaton/a/am;J)J

    .line 28512
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 28513
    or-int/lit8 v0, v0, 0x2

    .line 28515
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/am;->a(Lcom/sec/chaton/a/am;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 28516
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 28517
    or-int/lit8 v0, v0, 0x4

    .line 28519
    :cond_1
    iget-wide v3, p0, Lcom/sec/chaton/a/an;->d:J

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/a/am;->b(Lcom/sec/chaton/a/am;J)J

    .line 28520
    invoke-static {v2, v0}, Lcom/sec/chaton/a/am;->a(Lcom/sec/chaton/a/am;I)I

    .line 28521
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 28608
    iget v0, p0, Lcom/sec/chaton/a/an;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 28611
    iget-object v0, p0, Lcom/sec/chaton/a/an;->c:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->c()Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0}, Lcom/sec/chaton/a/an;->c()Lcom/sec/chaton/a/am;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 28539
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/an;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    check-cast p1, Lcom/sec/chaton/a/am;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/an;->a(Lcom/sec/chaton/a/am;)Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28452
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/an;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/an;

    move-result-object v0

    return-object v0
.end method
