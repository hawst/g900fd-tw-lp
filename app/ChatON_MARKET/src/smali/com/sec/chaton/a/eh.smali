.class public final Lcom/sec/chaton/a/eh;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ei;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/eg;",
        "Lcom/sec/chaton/a/eh;",
        ">;",
        "Lcom/sec/chaton/a/ei;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/Object;

.field private d:J

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/sec/chaton/a/c;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 31989
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 32203
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->c:Ljava/lang/Object;

    .line 32260
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->e:Ljava/lang/Object;

    .line 32296
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->f:Ljava/lang/Object;

    .line 32332
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    .line 32421
    invoke-static {}, Lcom/sec/chaton/a/c;->a()Lcom/sec/chaton/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    .line 31990
    invoke-direct {p0}, Lcom/sec/chaton/a/eh;->i()V

    .line 31991
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/eh;)Lcom/sec/chaton/a/eg;
    .locals 1

    .prologue
    .line 31984
    invoke-direct {p0}, Lcom/sec/chaton/a/eh;->k()Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 31984
    invoke-static {}, Lcom/sec/chaton/a/eh;->j()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 31994
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 31996
    new-instance v0, Lcom/sec/chaton/a/eh;

    invoke-direct {v0}, Lcom/sec/chaton/a/eh;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/eg;
    .locals 2

    .prologue
    .line 32036
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->e()Lcom/sec/chaton/a/eg;

    move-result-object v0

    .line 32037
    invoke-virtual {v0}, Lcom/sec/chaton/a/eg;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32038
    invoke-static {v0}, Lcom/sec/chaton/a/eh;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 32041
    :cond_0
    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 32335
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 32336
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    .line 32337
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32339
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/eh;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 32000
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 32001
    iput-wide v1, p0, Lcom/sec/chaton/a/eh;->b:J

    .line 32002
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32003
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->c:Ljava/lang/Object;

    .line 32004
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32005
    iput-wide v1, p0, Lcom/sec/chaton/a/eh;->d:J

    .line 32006
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32007
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->e:Ljava/lang/Object;

    .line 32008
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32009
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->f:Ljava/lang/Object;

    .line 32010
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32011
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    .line 32012
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32013
    invoke-static {}, Lcom/sec/chaton/a/c;->a()Lcom/sec/chaton/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    .line 32014
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32015
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 32190
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32191
    iput-wide p1, p0, Lcom/sec/chaton/a/eh;->b:J

    .line 32193
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/eh;
    .locals 2

    .prologue
    .line 32123
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 32124
    sparse-switch v0, :sswitch_data_0

    .line 32129
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/eh;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32131
    :sswitch_0
    return-object p0

    .line 32136
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32137
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/eh;->b:J

    goto :goto_0

    .line 32141
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32142
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->c:Ljava/lang/Object;

    goto :goto_0

    .line 32146
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32147
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/eh;->d:J

    goto :goto_0

    .line 32151
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32152
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->e:Ljava/lang/Object;

    goto :goto_0

    .line 32156
    :sswitch_5
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32157
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->f:Ljava/lang/Object;

    goto :goto_0

    .line 32161
    :sswitch_6
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v0

    .line 32162
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 32163
    invoke-virtual {v0}, Lcom/sec/chaton/a/cv;->e()Lcom/sec/chaton/a/cu;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/eh;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/eh;

    goto :goto_0

    .line 32167
    :sswitch_7
    invoke-static {}, Lcom/sec/chaton/a/c;->newBuilder()Lcom/sec/chaton/a/d;

    move-result-object v0

    .line 32168
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 32169
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->g()Lcom/sec/chaton/a/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/d;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;

    .line 32171
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 32172
    invoke-virtual {v0}, Lcom/sec/chaton/a/d;->e()Lcom/sec/chaton/a/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/eh;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/eh;

    goto :goto_0

    .line 32124
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 32429
    if-nez p1, :cond_0

    .line 32430
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32432
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    .line 32434
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32435
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 32368
    if-nez p1, :cond_0

    .line 32369
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32371
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/a/eh;->l()V

    .line 32372
    iget-object v0, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32374
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/eg;)Lcom/sec/chaton/a/eh;
    .locals 2

    .prologue
    .line 32082
    invoke-static {}, Lcom/sec/chaton/a/eg;->a()Lcom/sec/chaton/a/eg;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 32111
    :cond_0
    :goto_0
    return-object p0

    .line 32083
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 32084
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/eh;->a(J)Lcom/sec/chaton/a/eh;

    .line 32086
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 32087
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/eh;->a(Ljava/lang/String;)Lcom/sec/chaton/a/eh;

    .line 32089
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 32090
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/eh;->b(J)Lcom/sec/chaton/a/eh;

    .line 32092
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 32093
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/eh;->b(Ljava/lang/String;)Lcom/sec/chaton/a/eh;

    .line 32095
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 32096
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/eh;->c(Ljava/lang/String;)Lcom/sec/chaton/a/eh;

    .line 32098
    :cond_6
    invoke-static {p1}, Lcom/sec/chaton/a/eg;->b(Lcom/sec/chaton/a/eg;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 32099
    iget-object v0, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 32100
    invoke-static {p1}, Lcom/sec/chaton/a/eg;->b(Lcom/sec/chaton/a/eg;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    .line 32101
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32108
    :cond_7
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32109
    invoke-virtual {p1}, Lcom/sec/chaton/a/eg;->p()Lcom/sec/chaton/a/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/eh;->b(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/eh;

    goto :goto_0

    .line 32103
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/a/eh;->l()V

    .line 32104
    iget-object v0, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/chaton/a/eg;->b(Lcom/sec/chaton/a/eg;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 32218
    if-nez p1, :cond_0

    .line 32219
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32221
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32222
    iput-object p1, p0, Lcom/sec/chaton/a/eh;->c:Ljava/lang/Object;

    .line 32224
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/eh;
    .locals 2

    .prologue
    .line 32019
    invoke-static {}, Lcom/sec/chaton/a/eh;->j()Lcom/sec/chaton/a/eh;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->e()Lcom/sec/chaton/a/eg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eh;->a(Lcom/sec/chaton/a/eg;)Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 32247
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32248
    iput-wide p1, p0, Lcom/sec/chaton/a/eh;->d:J

    .line 32250
    return-object p0
.end method

.method public b(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/eh;
    .locals 2

    .prologue
    .line 32445
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    invoke-static {}, Lcom/sec/chaton/a/c;->a()Lcom/sec/chaton/a/c;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 32447
    iget-object v0, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    invoke-static {v0}, Lcom/sec/chaton/a/c;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/d;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/d;->e()Lcom/sec/chaton/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    .line 32453
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32454
    return-object p0

    .line 32450
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 32275
    if-nez p1, :cond_0

    .line 32276
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32278
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32279
    iput-object p1, p0, Lcom/sec/chaton/a/eh;->e:Ljava/lang/Object;

    .line 32281
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->d()Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->e()Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/eg;
    .locals 1

    .prologue
    .line 32023
    invoke-static {}, Lcom/sec/chaton/a/eg;->a()Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 32311
    if-nez p1, :cond_0

    .line 32312
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32314
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32315
    iput-object p1, p0, Lcom/sec/chaton/a/eh;->f:Ljava/lang/Object;

    .line 32317
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->a()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->a()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->b()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->b()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->b()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->b()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/eg;
    .locals 2

    .prologue
    .line 32027
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->e()Lcom/sec/chaton/a/eg;

    move-result-object v0

    .line 32028
    invoke-virtual {v0}, Lcom/sec/chaton/a/eg;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32029
    invoke-static {v0}, Lcom/sec/chaton/a/eh;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 32031
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/eg;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 32045
    new-instance v2, Lcom/sec/chaton/a/eg;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/eg;-><init>(Lcom/sec/chaton/a/eh;Lcom/sec/chaton/a/b;)V

    .line 32046
    iget v3, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32047
    const/4 v1, 0x0

    .line 32048
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    .line 32051
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/eh;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/eg;->a(Lcom/sec/chaton/a/eg;J)J

    .line 32052
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 32053
    or-int/lit8 v0, v0, 0x2

    .line 32055
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/eh;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/eg;->a(Lcom/sec/chaton/a/eg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32056
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 32057
    or-int/lit8 v0, v0, 0x4

    .line 32059
    :cond_1
    iget-wide v4, p0, Lcom/sec/chaton/a/eh;->d:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/eg;->b(Lcom/sec/chaton/a/eg;J)J

    .line 32060
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 32061
    or-int/lit8 v0, v0, 0x8

    .line 32063
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/eh;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/eg;->b(Lcom/sec/chaton/a/eg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32064
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 32065
    or-int/lit8 v0, v0, 0x10

    .line 32067
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/eh;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/eg;->c(Lcom/sec/chaton/a/eg;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32068
    iget v1, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 32069
    iget-object v1, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    .line 32070
    iget v1, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/sec/chaton/a/eh;->a:I

    .line 32072
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/eh;->g:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/eg;->a(Lcom/sec/chaton/a/eg;Ljava/util/List;)Ljava/util/List;

    .line 32073
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 32074
    or-int/lit8 v0, v0, 0x20

    .line 32076
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/eg;->a(Lcom/sec/chaton/a/eg;Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/c;

    .line 32077
    invoke-static {v2, v0}, Lcom/sec/chaton/a/eg;->a(Lcom/sec/chaton/a/eg;I)I

    .line 32078
    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 32423
    iget v0, p0, Lcom/sec/chaton/a/eh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/c;
    .locals 1

    .prologue
    .line 32426
    iget-object v0, p0, Lcom/sec/chaton/a/eh;->h:Lcom/sec/chaton/a/c;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->c()Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0}, Lcom/sec/chaton/a/eh;->c()Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 32115
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/eh;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    check-cast p1, Lcom/sec/chaton/a/eg;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/eh;->a(Lcom/sec/chaton/a/eg;)Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 31984
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/eh;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method
