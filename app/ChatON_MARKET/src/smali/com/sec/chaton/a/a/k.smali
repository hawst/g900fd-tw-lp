.class public Lcom/sec/chaton/a/a/k;
.super Lcom/sec/chaton/a/a/m;
.source "MessageResultEntry.java"


# instance fields
.field private d:I

.field private e:J


# direct methods
.method public constructor <init>(ZI)V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/a/m;-><init>(Z)V

    .line 9
    iput p2, p0, Lcom/sec/chaton/a/a/k;->d:I

    .line 10
    sget-object v0, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    iput-object v0, p0, Lcom/sec/chaton/a/a/k;->b:Lcom/sec/chaton/a/a/n;

    .line 11
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/a/a/k;->e:J

    .line 12
    return-void
.end method

.method public constructor <init>(ZIJ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/a/m;-><init>(Z)V

    .line 16
    iput p2, p0, Lcom/sec/chaton/a/a/k;->d:I

    .line 17
    sget-object v0, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    iput-object v0, p0, Lcom/sec/chaton/a/a/k;->b:Lcom/sec/chaton/a/a/n;

    .line 18
    iput-wide p3, p0, Lcom/sec/chaton/a/a/k;->e:J

    .line 19
    return-void
.end method

.method public constructor <init>(ZILjava/lang/String;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/a/a/m;-><init>(ZLjava/lang/String;)V

    .line 23
    iput p2, p0, Lcom/sec/chaton/a/a/k;->d:I

    .line 24
    sget-object v0, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    iput-object v0, p0, Lcom/sec/chaton/a/a/k;->b:Lcom/sec/chaton/a/a/n;

    .line 25
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/a/a/k;->e:J

    .line 26
    return-void
.end method

.method public constructor <init>(ZILjava/lang/String;J)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/a/a/m;-><init>(ZLjava/lang/String;)V

    .line 30
    iput p2, p0, Lcom/sec/chaton/a/a/k;->d:I

    .line 31
    sget-object v0, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    iput-object v0, p0, Lcom/sec/chaton/a/a/k;->b:Lcom/sec/chaton/a/a/n;

    .line 32
    iput-wide p4, p0, Lcom/sec/chaton/a/a/k;->e:J

    .line 33
    return-void
.end method

.method public constructor <init>(ZLcom/sec/chaton/a/a/n;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/a/a/m;-><init>(ZLcom/sec/chaton/a/a/n;)V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/a/a/k;->d:I

    .line 38
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/a/a/k;->e:J

    .line 39
    return-void
.end method


# virtual methods
.method public f()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/chaton/a/a/k;->d:I

    return v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/sec/chaton/a/a/k;->e:J

    return-wide v0
.end method
