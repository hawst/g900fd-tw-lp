.class public final Lcom/sec/chaton/a/ek;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/el;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ej;",
        "Lcom/sec/chaton/a/ek;",
        ">;",
        "Lcom/sec/chaton/a/el;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/sec/chaton/a/fb;

.field private e:Lcom/sec/chaton/a/ea;

.field private f:Lcom/google/protobuf/LazyStringList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2020
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2210
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->c:Ljava/lang/Object;

    .line 2246
    invoke-static {}, Lcom/sec/chaton/a/fb;->a()Lcom/sec/chaton/a/fb;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    .line 2289
    invoke-static {}, Lcom/sec/chaton/a/ea;->a()Lcom/sec/chaton/a/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    .line 2332
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    .line 2021
    invoke-direct {p0}, Lcom/sec/chaton/a/ek;->k()V

    .line 2022
    return-void
.end method

.method static synthetic j()Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2015
    invoke-static {}, Lcom/sec/chaton/a/ek;->l()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 2025
    return-void
.end method

.method private static l()Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2027
    new-instance v0, Lcom/sec/chaton/a/ek;

    invoke-direct {v0}, Lcom/sec/chaton/a/ek;-><init>()V

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2334
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 2335
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    .line 2336
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2338
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2031
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2032
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/a/ek;->b:I

    .line 2033
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2034
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->c:Ljava/lang/Object;

    .line 2035
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2036
    invoke-static {}, Lcom/sec/chaton/a/fb;->a()Lcom/sec/chaton/a/fb;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    .line 2037
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2038
    invoke-static {}, Lcom/sec/chaton/a/ea;->a()Lcom/sec/chaton/a/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    .line 2039
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2040
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    .line 2041
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2042
    return-object p0
.end method

.method public a(I)Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2197
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2198
    iput p1, p0, Lcom/sec/chaton/a/ek;->b:I

    .line 2200
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ek;
    .locals 2

    .prologue
    .line 2137
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2138
    sparse-switch v0, :sswitch_data_0

    .line 2143
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ek;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2145
    :sswitch_0
    return-object p0

    .line 2150
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2151
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/a/ek;->b:I

    goto :goto_0

    .line 2155
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2156
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->c:Ljava/lang/Object;

    goto :goto_0

    .line 2160
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/fb;->newBuilder()Lcom/sec/chaton/a/fc;

    move-result-object v0

    .line 2161
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2162
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->g()Lcom/sec/chaton/a/fb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/fc;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;

    .line 2164
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 2165
    invoke-virtual {v0}, Lcom/sec/chaton/a/fc;->e()Lcom/sec/chaton/a/fb;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/ek;

    goto :goto_0

    .line 2169
    :sswitch_4
    invoke-static {}, Lcom/sec/chaton/a/ea;->newBuilder()Lcom/sec/chaton/a/eb;

    move-result-object v0

    .line 2170
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2171
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->i()Lcom/sec/chaton/a/ea;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/eb;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;

    .line 2173
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 2174
    invoke-virtual {v0}, Lcom/sec/chaton/a/eb;->e()Lcom/sec/chaton/a/ea;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/ek;

    goto :goto_0

    .line 2178
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/chaton/a/ek;->m()V

    .line 2179
    iget-object v0, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 2138
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2297
    if-nez p1, :cond_0

    .line 2298
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2300
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    .line 2302
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2303
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;
    .locals 2

    .prologue
    .line 2102
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2125
    :cond_0
    :goto_0
    return-object p0

    .line 2103
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2104
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ek;->a(I)Lcom/sec/chaton/a/ek;

    .line 2106
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2107
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ek;->a(Ljava/lang/String;)Lcom/sec/chaton/a/ek;

    .line 2109
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2110
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->h()Lcom/sec/chaton/a/fb;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ek;->b(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/ek;

    .line 2112
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2113
    invoke-virtual {p1}, Lcom/sec/chaton/a/ej;->j()Lcom/sec/chaton/a/ea;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ek;->b(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/ek;

    .line 2115
    :cond_5
    invoke-static {p1}, Lcom/sec/chaton/a/ej;->b(Lcom/sec/chaton/a/ej;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2116
    iget-object v0, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2117
    invoke-static {p1}, Lcom/sec/chaton/a/ej;->b(Lcom/sec/chaton/a/ej;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    .line 2118
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    goto :goto_0

    .line 2120
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/a/ek;->m()V

    .line 2121
    iget-object v0, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/ej;->b(Lcom/sec/chaton/a/ej;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2254
    if-nez p1, :cond_0

    .line 2255
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2257
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    .line 2259
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2260
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/ek;
    .locals 1

    .prologue
    .line 2225
    if-nez p1, :cond_0

    .line 2226
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2228
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2229
    iput-object p1, p0, Lcom/sec/chaton/a/ek;->c:Ljava/lang/Object;

    .line 2231
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ek;
    .locals 2

    .prologue
    .line 2046
    invoke-static {}, Lcom/sec/chaton/a/ek;->l()Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/ek;
    .locals 2

    .prologue
    .line 2313
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    invoke-static {}, Lcom/sec/chaton/a/ea;->a()Lcom/sec/chaton/a/ea;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2315
    iget-object v0, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    invoke-static {v0}, Lcom/sec/chaton/a/ea;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/eb;->a(Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/eb;->e()Lcom/sec/chaton/a/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    .line 2321
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2322
    return-object p0

    .line 2318
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    goto :goto_0
.end method

.method public b(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/ek;
    .locals 2

    .prologue
    .line 2270
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    invoke-static {}, Lcom/sec/chaton/a/fb;->a()Lcom/sec/chaton/a/fb;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2272
    iget-object v0, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    invoke-static {v0}, Lcom/sec/chaton/a/fb;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/fc;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/fc;->e()Lcom/sec/chaton/a/fb;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    .line 2278
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2279
    return-object p0

    .line 2275
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->d()Lcom/sec/chaton/a/ej;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 2050
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->a()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->a()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->b()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->b()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->b()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->b()Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ej;
    .locals 2

    .prologue
    .line 2054
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    .line 2055
    invoke-virtual {v0}, Lcom/sec/chaton/a/ej;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2056
    invoke-static {v0}, Lcom/sec/chaton/a/ek;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2058
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ej;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2072
    new-instance v2, Lcom/sec/chaton/a/ej;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ej;-><init>(Lcom/sec/chaton/a/ek;Lcom/sec/chaton/a/b;)V

    .line 2073
    iget v3, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2074
    const/4 v1, 0x0

    .line 2075
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 2078
    :goto_0
    iget v1, p0, Lcom/sec/chaton/a/ek;->b:I

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;I)I

    .line 2079
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2080
    or-int/lit8 v0, v0, 0x2

    .line 2082
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ek;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2083
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2084
    or-int/lit8 v0, v0, 0x4

    .line 2086
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fb;

    .line 2087
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 2088
    or-int/lit8 v0, v0, 0x8

    .line 2090
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;Lcom/sec/chaton/a/ea;)Lcom/sec/chaton/a/ea;

    .line 2091
    iget v1, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 2092
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v3, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v3}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    .line 2094
    iget v1, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/sec/chaton/a/ek;->a:I

    .line 2096
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/ek;->f:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 2097
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ej;->b(Lcom/sec/chaton/a/ej;I)I

    .line 2098
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 2248
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/fb;
    .locals 1

    .prologue
    .line 2251
    iget-object v0, p0, Lcom/sec/chaton/a/ek;->d:Lcom/sec/chaton/a/fb;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->c()Lcom/sec/chaton/a/ej;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0}, Lcom/sec/chaton/a/ek;->c()Lcom/sec/chaton/a/ej;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 2291
    iget v0, p0, Lcom/sec/chaton/a/ek;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ea;
    .locals 1

    .prologue
    .line 2294
    iget-object v0, p0, Lcom/sec/chaton/a/ek;->e:Lcom/sec/chaton/a/ea;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 2129
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ek;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    check-cast p1, Lcom/sec/chaton/a/ej;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2015
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ek;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    return-object v0
.end method
