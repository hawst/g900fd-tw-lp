.class public final Lcom/sec/chaton/a/x;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/z;


# static fields
.field private static final a:Lcom/sec/chaton/a/x;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:J

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5712
    new-instance v0, Lcom/sec/chaton/a/x;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/x;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/x;->a:Lcom/sec/chaton/a/x;

    .line 5713
    sget-object v0, Lcom/sec/chaton/a/x;->a:Lcom/sec/chaton/a/x;

    invoke-direct {v0}, Lcom/sec/chaton/a/x;->j()V

    .line 5714
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/y;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5358
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 5418
    iput-byte v0, p0, Lcom/sec/chaton/a/x;->e:B

    .line 5438
    iput v0, p0, Lcom/sec/chaton/a/x;->f:I

    .line 5359
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/y;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 5353
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/x;-><init>(Lcom/sec/chaton/a/y;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5360
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5418
    iput-byte v0, p0, Lcom/sec/chaton/a/x;->e:B

    .line 5438
    iput v0, p0, Lcom/sec/chaton/a/x;->f:I

    .line 5360
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/x;I)I
    .locals 0

    .prologue
    .line 5353
    iput p1, p0, Lcom/sec/chaton/a/x;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/x;J)J
    .locals 0

    .prologue
    .line 5353
    iput-wide p1, p0, Lcom/sec/chaton/a/x;->d:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/x;
    .locals 1

    .prologue
    .line 5364
    sget-object v0, Lcom/sec/chaton/a/x;->a:Lcom/sec/chaton/a/x;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5533
    invoke-static {}, Lcom/sec/chaton/a/x;->newBuilder()Lcom/sec/chaton/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/y;->a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/x;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5353
    iput-object p1, p0, Lcom/sec/chaton/a/x;->c:Ljava/lang/Object;

    return-object p1
.end method

.method private i()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5393
    iget-object v0, p0, Lcom/sec/chaton/a/x;->c:Ljava/lang/Object;

    .line 5394
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5395
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5397
    iput-object v0, p0, Lcom/sec/chaton/a/x;->c:Ljava/lang/Object;

    .line 5400
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 5415
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/x;->c:Ljava/lang/Object;

    .line 5416
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/x;->d:J

    .line 5417
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5530
    invoke-static {}, Lcom/sec/chaton/a/y;->f()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/x;
    .locals 1

    .prologue
    .line 5368
    sget-object v0, Lcom/sec/chaton/a/x;->a:Lcom/sec/chaton/a/x;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5376
    iget v1, p0, Lcom/sec/chaton/a/x;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5379
    iget-object v0, p0, Lcom/sec/chaton/a/x;->c:Ljava/lang/Object;

    .line 5380
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5381
    check-cast v0, Ljava/lang/String;

    .line 5389
    :goto_0
    return-object v0

    .line 5383
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5385
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5386
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5387
    iput-object v1, p0, Lcom/sec/chaton/a/x;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 5389
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 5408
    iget v0, p0, Lcom/sec/chaton/a/x;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 5411
    iget-wide v0, p0, Lcom/sec/chaton/a/x;->d:J

    return-wide v0
.end method

.method public g()Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5531
    invoke-static {}, Lcom/sec/chaton/a/x;->newBuilder()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5353
    invoke-virtual {p0}, Lcom/sec/chaton/a/x;->b()Lcom/sec/chaton/a/x;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5440
    iget v0, p0, Lcom/sec/chaton/a/x;->f:I

    .line 5441
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5453
    :goto_0
    return v0

    .line 5443
    :cond_0
    const/4 v0, 0x0

    .line 5444
    iget v1, p0, Lcom/sec/chaton/a/x;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 5445
    invoke-direct {p0}, Lcom/sec/chaton/a/x;->i()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5448
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/x;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 5449
    iget-wide v1, p0, Lcom/sec/chaton/a/x;->d:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5452
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/x;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/y;
    .locals 1

    .prologue
    .line 5535
    invoke-static {p0}, Lcom/sec/chaton/a/x;->a(Lcom/sec/chaton/a/x;)Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 5420
    iget-byte v1, p0, Lcom/sec/chaton/a/x;->e:B

    .line 5421
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 5424
    :goto_0
    return v0

    .line 5421
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5423
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/x;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5353
    invoke-virtual {p0}, Lcom/sec/chaton/a/x;->g()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5353
    invoke-virtual {p0}, Lcom/sec/chaton/a/x;->h()Lcom/sec/chaton/a/y;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5460
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5429
    invoke-virtual {p0}, Lcom/sec/chaton/a/x;->getSerializedSize()I

    .line 5430
    iget v0, p0, Lcom/sec/chaton/a/x;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5431
    invoke-direct {p0}, Lcom/sec/chaton/a/x;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5433
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/x;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 5434
    iget-wide v0, p0, Lcom/sec/chaton/a/x;->d:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 5436
    :cond_1
    return-void
.end method
