.class public final Lcom/sec/chaton/a/ew;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ex;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ev;",
        "Lcom/sec/chaton/a/ew;",
        ">;",
        "Lcom/sec/chaton/a/ex;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8258
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 8428
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    .line 8259
    invoke-direct {p0}, Lcom/sec/chaton/a/ew;->i()V

    .line 8260
    return-void
.end method

.method static synthetic h()Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8253
    invoke-static {}, Lcom/sec/chaton/a/ew;->j()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 8263
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8265
    new-instance v0, Lcom/sec/chaton/a/ew;

    invoke-direct {v0}, Lcom/sec/chaton/a/ew;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ew;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 8269
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 8270
    iput-wide v1, p0, Lcom/sec/chaton/a/ew;->b:J

    .line 8271
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8272
    iput-wide v1, p0, Lcom/sec/chaton/a/ew;->c:J

    .line 8273
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8274
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    .line 8275
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8276
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8394
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8395
    iput-wide p1, p0, Lcom/sec/chaton/a/ew;->b:J

    .line 8397
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ew;
    .locals 2

    .prologue
    .line 8348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 8349
    sparse-switch v0, :sswitch_data_0

    .line 8354
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ew;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8356
    :sswitch_0
    return-object p0

    .line 8361
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8362
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ew;->b:J

    goto :goto_0

    .line 8366
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8367
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ew;->c:J

    goto :goto_0

    .line 8371
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 8372
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 8373
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 8375
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 8376
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ew;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ew;

    goto :goto_0

    .line 8349
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8436
    if-nez p1, :cond_0

    .line 8437
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8439
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    .line 8441
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8442
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/ew;
    .locals 2

    .prologue
    .line 8326
    invoke-static {}, Lcom/sec/chaton/a/ev;->a()Lcom/sec/chaton/a/ev;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 8336
    :cond_0
    :goto_0
    return-object p0

    .line 8327
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ev;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8328
    invoke-virtual {p1}, Lcom/sec/chaton/a/ev;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ew;->a(J)Lcom/sec/chaton/a/ew;

    .line 8330
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ev;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8331
    invoke-virtual {p1}, Lcom/sec/chaton/a/ev;->f()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ew;->b(J)Lcom/sec/chaton/a/ew;

    .line 8333
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/ev;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8334
    invoke-virtual {p1}, Lcom/sec/chaton/a/ev;->h()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ew;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ew;

    goto :goto_0
.end method

.method public b()Lcom/sec/chaton/a/ew;
    .locals 2

    .prologue
    .line 8280
    invoke-static {}, Lcom/sec/chaton/a/ew;->j()Lcom/sec/chaton/a/ew;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->e()Lcom/sec/chaton/a/ev;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ew;->a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/ew;
    .locals 1

    .prologue
    .line 8415
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8416
    iput-wide p1, p0, Lcom/sec/chaton/a/ew;->c:J

    .line 8418
    return-object p0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ew;
    .locals 2

    .prologue
    .line 8452
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 8454
    iget-object v0, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    .line 8460
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8461
    return-object p0

    .line 8457
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->d()Lcom/sec/chaton/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->e()Lcom/sec/chaton/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ev;
    .locals 1

    .prologue
    .line 8284
    invoke-static {}, Lcom/sec/chaton/a/ev;->a()Lcom/sec/chaton/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->a()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->a()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->b()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->b()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->b()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->b()Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ev;
    .locals 2

    .prologue
    .line 8288
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->e()Lcom/sec/chaton/a/ev;

    move-result-object v0

    .line 8289
    invoke-virtual {v0}, Lcom/sec/chaton/a/ev;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8290
    invoke-static {v0}, Lcom/sec/chaton/a/ew;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 8292
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ev;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 8306
    new-instance v2, Lcom/sec/chaton/a/ev;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ev;-><init>(Lcom/sec/chaton/a/ew;Lcom/sec/chaton/a/b;)V

    .line 8307
    iget v3, p0, Lcom/sec/chaton/a/ew;->a:I

    .line 8308
    const/4 v1, 0x0

    .line 8309
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 8312
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/ew;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/ev;->a(Lcom/sec/chaton/a/ev;J)J

    .line 8313
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 8314
    or-int/lit8 v0, v0, 0x2

    .line 8316
    :cond_0
    iget-wide v4, p0, Lcom/sec/chaton/a/ew;->c:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/ev;->b(Lcom/sec/chaton/a/ev;J)J

    .line 8317
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 8318
    or-int/lit8 v0, v0, 0x4

    .line 8320
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ev;->a(Lcom/sec/chaton/a/ev;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 8321
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ev;->a(Lcom/sec/chaton/a/ev;I)I

    .line 8322
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 8430
    iget v0, p0, Lcom/sec/chaton/a/ew;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 8433
    iget-object v0, p0, Lcom/sec/chaton/a/ew;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->c()Lcom/sec/chaton/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0}, Lcom/sec/chaton/a/ew;->c()Lcom/sec/chaton/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 8340
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ew;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    check-cast p1, Lcom/sec/chaton/a/ev;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ew;->a(Lcom/sec/chaton/a/ev;)Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8253
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ew;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ew;

    move-result-object v0

    return-object v0
.end method
