.class public final Lcom/sec/chaton/a/cv;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/cu;",
        "Lcom/sec/chaton/a/cv;",
        ">;",
        "Lcom/sec/chaton/a/cw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:J

.field private e:J

.field private f:Lcom/sec/chaton/a/dp;

.field private g:Lcom/sec/chaton/a/bc;

.field private h:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 4027
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 4215
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->b:Ljava/lang/Object;

    .line 4251
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->c:Ljava/lang/Object;

    .line 4329
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->f:Lcom/sec/chaton/a/dp;

    .line 4353
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->g:Lcom/sec/chaton/a/bc;

    .line 4377
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->h:Ljava/lang/Object;

    .line 4028
    invoke-direct {p0}, Lcom/sec/chaton/a/cv;->g()V

    .line 4029
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4022
    invoke-static {}, Lcom/sec/chaton/a/cv;->h()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 4032
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4034
    new-instance v0, Lcom/sec/chaton/a/cv;

    invoke-direct {v0}, Lcom/sec/chaton/a/cv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/cv;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 4038
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 4039
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->b:Ljava/lang/Object;

    .line 4040
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4041
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->c:Ljava/lang/Object;

    .line 4042
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4043
    iput-wide v1, p0, Lcom/sec/chaton/a/cv;->d:J

    .line 4044
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4045
    iput-wide v1, p0, Lcom/sec/chaton/a/cv;->e:J

    .line 4046
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4047
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->f:Lcom/sec/chaton/a/dp;

    .line 4048
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4049
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->g:Lcom/sec/chaton/a/bc;

    .line 4050
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4051
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->h:Ljava/lang/Object;

    .line 4052
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4053
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4295
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4296
    iput-wide p1, p0, Lcom/sec/chaton/a/cv;->d:J

    .line 4298
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cv;
    .locals 2

    .prologue
    .line 4153
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 4154
    sparse-switch v0, :sswitch_data_0

    .line 4159
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/cv;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4161
    :sswitch_0
    return-object p0

    .line 4166
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4167
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->b:Ljava/lang/Object;

    goto :goto_0

    .line 4171
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4172
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->c:Ljava/lang/Object;

    goto :goto_0

    .line 4176
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4177
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/cv;->d:J

    goto :goto_0

    .line 4181
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4182
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/cv;->e:J

    goto :goto_0

    .line 4186
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 4187
    invoke-static {v0}, Lcom/sec/chaton/a/dp;->a(I)Lcom/sec/chaton/a/dp;

    move-result-object v0

    .line 4188
    if-eqz v0, :cond_0

    .line 4189
    iget v1, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4190
    iput-object v0, p0, Lcom/sec/chaton/a/cv;->f:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 4195
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 4196
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 4197
    if-eqz v0, :cond_0

    .line 4198
    iget v1, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4199
    iput-object v0, p0, Lcom/sec/chaton/a/cv;->g:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 4204
    :sswitch_7
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4205
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/cv;->h:Ljava/lang/Object;

    goto :goto_0

    .line 4154
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4361
    if-nez p1, :cond_0

    .line 4362
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4364
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4365
    iput-object p1, p0, Lcom/sec/chaton/a/cv;->g:Lcom/sec/chaton/a/bc;

    .line 4367
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;
    .locals 2

    .prologue
    .line 4119
    invoke-static {}, Lcom/sec/chaton/a/cu;->a()Lcom/sec/chaton/a/cu;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 4141
    :cond_0
    :goto_0
    return-object p0

    .line 4120
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4121
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    .line 4123
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4124
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cv;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    .line 4126
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4127
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/cv;->a(J)Lcom/sec/chaton/a/cv;

    .line 4129
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4130
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/cv;->b(J)Lcom/sec/chaton/a/cv;

    .line 4132
    :cond_5
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4133
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->l()Lcom/sec/chaton/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;

    .line 4135
    :cond_6
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4136
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->n()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;

    .line 4138
    :cond_7
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4139
    invoke-virtual {p1}, Lcom/sec/chaton/a/cu;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/cv;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4337
    if-nez p1, :cond_0

    .line 4338
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4340
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4341
    iput-object p1, p0, Lcom/sec/chaton/a/cv;->f:Lcom/sec/chaton/a/dp;

    .line 4343
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4230
    if-nez p1, :cond_0

    .line 4231
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4233
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4234
    iput-object p1, p0, Lcom/sec/chaton/a/cv;->b:Ljava/lang/Object;

    .line 4236
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/cv;
    .locals 2

    .prologue
    .line 4057
    invoke-static {}, Lcom/sec/chaton/a/cv;->h()Lcom/sec/chaton/a/cv;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->e()Lcom/sec/chaton/a/cu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4316
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4317
    iput-wide p1, p0, Lcom/sec/chaton/a/cv;->e:J

    .line 4319
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4266
    if-nez p1, :cond_0

    .line 4267
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4269
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4270
    iput-object p1, p0, Lcom/sec/chaton/a/cv;->c:Ljava/lang/Object;

    .line 4272
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->d()Lcom/sec/chaton/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->e()Lcom/sec/chaton/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/cu;
    .locals 1

    .prologue
    .line 4061
    invoke-static {}, Lcom/sec/chaton/a/cu;->a()Lcom/sec/chaton/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/cv;
    .locals 1

    .prologue
    .line 4392
    if-nez p1, :cond_0

    .line 4393
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4395
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cv;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4396
    iput-object p1, p0, Lcom/sec/chaton/a/cv;->h:Ljava/lang/Object;

    .line 4398
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->a()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->a()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->b()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->b()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->b()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->b()Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/cu;
    .locals 2

    .prologue
    .line 4065
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->e()Lcom/sec/chaton/a/cu;

    move-result-object v0

    .line 4066
    invoke-virtual {v0}, Lcom/sec/chaton/a/cu;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4067
    invoke-static {v0}, Lcom/sec/chaton/a/cv;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 4069
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/cu;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 4083
    new-instance v2, Lcom/sec/chaton/a/cu;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/cu;-><init>(Lcom/sec/chaton/a/cv;Lcom/sec/chaton/a/b;)V

    .line 4084
    iget v3, p0, Lcom/sec/chaton/a/cv;->a:I

    .line 4085
    const/4 v1, 0x0

    .line 4086
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    .line 4089
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/cv;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cu;->a(Lcom/sec/chaton/a/cu;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4090
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 4091
    or-int/lit8 v0, v0, 0x2

    .line 4093
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/cv;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cu;->b(Lcom/sec/chaton/a/cu;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4094
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 4095
    or-int/lit8 v0, v0, 0x4

    .line 4097
    :cond_1
    iget-wide v4, p0, Lcom/sec/chaton/a/cv;->d:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/cu;->a(Lcom/sec/chaton/a/cu;J)J

    .line 4098
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 4099
    or-int/lit8 v0, v0, 0x8

    .line 4101
    :cond_2
    iget-wide v4, p0, Lcom/sec/chaton/a/cv;->e:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/cu;->b(Lcom/sec/chaton/a/cu;J)J

    .line 4102
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 4103
    or-int/lit8 v0, v0, 0x10

    .line 4105
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/a/cv;->f:Lcom/sec/chaton/a/dp;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cu;->a(Lcom/sec/chaton/a/cu;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;

    .line 4106
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 4107
    or-int/lit8 v0, v0, 0x20

    .line 4109
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/a/cv;->g:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cu;->a(Lcom/sec/chaton/a/cu;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 4110
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 4111
    or-int/lit8 v0, v0, 0x40

    .line 4113
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/a/cv;->h:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/cu;->c(Lcom/sec/chaton/a/cu;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4114
    invoke-static {v2, v0}, Lcom/sec/chaton/a/cu;->a(Lcom/sec/chaton/a/cu;I)I

    .line 4115
    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->c()Lcom/sec/chaton/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/a/cv;->c()Lcom/sec/chaton/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 4145
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cv;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    check-cast p1, Lcom/sec/chaton/a/cu;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4022
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/cv;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/cv;

    move-result-object v0

    return-object v0
.end method
