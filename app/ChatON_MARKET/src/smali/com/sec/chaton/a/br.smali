.class public final Lcom/sec/chaton/a/br;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bt;


# static fields
.field private static final a:Lcom/sec/chaton/a/br;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/bp;

.field private e:Lcom/sec/chaton/a/ej;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19133
    new-instance v0, Lcom/sec/chaton/a/br;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/br;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/br;->a:Lcom/sec/chaton/a/br;

    .line 19134
    sget-object v0, Lcom/sec/chaton/a/br;->a:Lcom/sec/chaton/a/br;

    invoke-direct {v0}, Lcom/sec/chaton/a/br;->k()V

    .line 19135
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/bs;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18730
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 18779
    iput-byte v0, p0, Lcom/sec/chaton/a/br;->f:B

    .line 18802
    iput v0, p0, Lcom/sec/chaton/a/br;->g:I

    .line 18731
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/bs;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 18725
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/br;-><init>(Lcom/sec/chaton/a/bs;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18732
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 18779
    iput-byte v0, p0, Lcom/sec/chaton/a/br;->f:B

    .line 18802
    iput v0, p0, Lcom/sec/chaton/a/br;->g:I

    .line 18732
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/br;I)I
    .locals 0

    .prologue
    .line 18725
    iput p1, p0, Lcom/sec/chaton/a/br;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/br;J)J
    .locals 0

    .prologue
    .line 18725
    iput-wide p1, p0, Lcom/sec/chaton/a/br;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/br;Lcom/sec/chaton/a/bp;)Lcom/sec/chaton/a/bp;
    .locals 0

    .prologue
    .line 18725
    iput-object p1, p0, Lcom/sec/chaton/a/br;->d:Lcom/sec/chaton/a/bp;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/br;
    .locals 1

    .prologue
    .line 18736
    sget-object v0, Lcom/sec/chaton/a/br;->a:Lcom/sec/chaton/a/br;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/br;
    .locals 1

    .prologue
    .line 18845
    invoke-static {}, Lcom/sec/chaton/a/br;->newBuilder()Lcom/sec/chaton/a/bs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bs;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/bs;

    invoke-static {v0}, Lcom/sec/chaton/a/bs;->a(Lcom/sec/chaton/a/bs;)Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/br;)Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 18901
    invoke-static {}, Lcom/sec/chaton/a/br;->newBuilder()Lcom/sec/chaton/a/bs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/bs;->a(Lcom/sec/chaton/a/br;)Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/br;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 18725
    iput-object p1, p0, Lcom/sec/chaton/a/br;->e:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method private k()V
    .locals 2

    .prologue
    .line 18775
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/br;->c:J

    .line 18776
    sget-object v0, Lcom/sec/chaton/a/bp;->a:Lcom/sec/chaton/a/bp;

    iput-object v0, p0, Lcom/sec/chaton/a/br;->d:Lcom/sec/chaton/a/bp;

    .line 18777
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/br;->e:Lcom/sec/chaton/a/ej;

    .line 18778
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 18898
    invoke-static {}, Lcom/sec/chaton/a/bs;->j()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/br;
    .locals 1

    .prologue
    .line 18740
    sget-object v0, Lcom/sec/chaton/a/br;->a:Lcom/sec/chaton/a/br;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 18748
    iget v1, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 18751
    iget-wide v0, p0, Lcom/sec/chaton/a/br;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 18758
    iget v0, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bp;
    .locals 1

    .prologue
    .line 18761
    iget-object v0, p0, Lcom/sec/chaton/a/br;->d:Lcom/sec/chaton/a/bp;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 18768
    iget v0, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18725
    invoke-virtual {p0}, Lcom/sec/chaton/a/br;->b()Lcom/sec/chaton/a/br;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 18804
    iget v0, p0, Lcom/sec/chaton/a/br;->g:I

    .line 18805
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 18821
    :goto_0
    return v0

    .line 18807
    :cond_0
    const/4 v0, 0x0

    .line 18808
    iget v1, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 18809
    iget-wide v1, p0, Lcom/sec/chaton/a/br;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 18812
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 18813
    iget-object v1, p0, Lcom/sec/chaton/a/br;->d:Lcom/sec/chaton/a/bp;

    invoke-virtual {v1}, Lcom/sec/chaton/a/bp;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 18816
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 18817
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/chaton/a/br;->e:Lcom/sec/chaton/a/ej;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18820
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/br;->g:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 18771
    iget-object v0, p0, Lcom/sec/chaton/a/br;->e:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public i()Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 18899
    invoke-static {}, Lcom/sec/chaton/a/br;->newBuilder()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 18781
    iget-byte v1, p0, Lcom/sec/chaton/a/br;->f:B

    .line 18782
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 18785
    :goto_0
    return v0

    .line 18782
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 18784
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/br;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/bs;
    .locals 1

    .prologue
    .line 18903
    invoke-static {p0}, Lcom/sec/chaton/a/br;->a(Lcom/sec/chaton/a/br;)Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18725
    invoke-virtual {p0}, Lcom/sec/chaton/a/br;->i()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18725
    invoke-virtual {p0}, Lcom/sec/chaton/a/br;->j()Lcom/sec/chaton/a/bs;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18828
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 18790
    invoke-virtual {p0}, Lcom/sec/chaton/a/br;->getSerializedSize()I

    .line 18791
    iget v0, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 18792
    iget-wide v0, p0, Lcom/sec/chaton/a/br;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 18794
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 18795
    iget-object v0, p0, Lcom/sec/chaton/a/br;->d:Lcom/sec/chaton/a/bp;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bp;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 18797
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/br;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 18798
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/br;->e:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 18800
    :cond_2
    return-void
.end method
