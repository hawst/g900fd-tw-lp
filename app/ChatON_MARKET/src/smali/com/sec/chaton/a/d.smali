.class public final Lcom/sec/chaton/a/d;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/c;",
        "Lcom/sec/chaton/a/d;",
        ">;",
        "Lcom/sec/chaton/a/e;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/sec/chaton/a/bc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 4623
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 4737
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/d;->b:Ljava/lang/Object;

    .line 4773
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/d;->c:Lcom/sec/chaton/a/bc;

    .line 4624
    invoke-direct {p0}, Lcom/sec/chaton/a/d;->g()V

    .line 4625
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4618
    invoke-static {}, Lcom/sec/chaton/a/d;->h()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 4628
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4630
    new-instance v0, Lcom/sec/chaton/a/d;

    invoke-direct {v0}, Lcom/sec/chaton/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4634
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 4635
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/d;->b:Ljava/lang/Object;

    .line 4636
    iget v0, p0, Lcom/sec/chaton/a/d;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/d;->a:I

    .line 4637
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/d;->c:Lcom/sec/chaton/a/bc;

    .line 4638
    iget v0, p0, Lcom/sec/chaton/a/d;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/d;->a:I

    .line 4639
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/d;
    .locals 2

    .prologue
    .line 4704
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 4705
    sparse-switch v0, :sswitch_data_0

    .line 4710
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/d;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4712
    :sswitch_0
    return-object p0

    .line 4717
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/d;->a:I

    .line 4718
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/d;->b:Ljava/lang/Object;

    goto :goto_0

    .line 4722
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 4723
    invoke-static {v0}, Lcom/sec/chaton/a/bc;->a(I)Lcom/sec/chaton/a/bc;

    move-result-object v0

    .line 4724
    if-eqz v0, :cond_0

    .line 4725
    iget v1, p0, Lcom/sec/chaton/a/d;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/d;->a:I

    .line 4726
    iput-object v0, p0, Lcom/sec/chaton/a/d;->c:Lcom/sec/chaton/a/bc;

    goto :goto_0

    .line 4705
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4781
    if-nez p1, :cond_0

    .line 4782
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4784
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/d;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/d;->a:I

    .line 4785
    iput-object p1, p0, Lcom/sec/chaton/a/d;->c:Lcom/sec/chaton/a/bc;

    .line 4787
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4685
    invoke-static {}, Lcom/sec/chaton/a/c;->a()Lcom/sec/chaton/a/c;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 4692
    :cond_0
    :goto_0
    return-object p0

    .line 4686
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/c;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4687
    invoke-virtual {p1}, Lcom/sec/chaton/a/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/d;->a(Ljava/lang/String;)Lcom/sec/chaton/a/d;

    .line 4689
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4690
    invoke-virtual {p1}, Lcom/sec/chaton/a/c;->f()Lcom/sec/chaton/a/bc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/d;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/d;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4752
    if-nez p1, :cond_0

    .line 4753
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4755
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/d;->a:I

    .line 4756
    iput-object p1, p0, Lcom/sec/chaton/a/d;->b:Ljava/lang/Object;

    .line 4758
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/d;
    .locals 2

    .prologue
    .line 4643
    invoke-static {}, Lcom/sec/chaton/a/d;->h()Lcom/sec/chaton/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->e()Lcom/sec/chaton/a/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/d;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->d()Lcom/sec/chaton/a/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->e()Lcom/sec/chaton/a/c;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/c;
    .locals 1

    .prologue
    .line 4647
    invoke-static {}, Lcom/sec/chaton/a/c;->a()Lcom/sec/chaton/a/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->a()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->a()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->b()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->b()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->b()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->b()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/c;
    .locals 2

    .prologue
    .line 4651
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->e()Lcom/sec/chaton/a/c;

    move-result-object v0

    .line 4652
    invoke-virtual {v0}, Lcom/sec/chaton/a/c;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4653
    invoke-static {v0}, Lcom/sec/chaton/a/d;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 4655
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/c;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4669
    new-instance v2, Lcom/sec/chaton/a/c;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/c;-><init>(Lcom/sec/chaton/a/d;Lcom/sec/chaton/a/b;)V

    .line 4670
    iget v3, p0, Lcom/sec/chaton/a/d;->a:I

    .line 4671
    const/4 v1, 0x0

    .line 4672
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 4675
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/d;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/c;->a(Lcom/sec/chaton/a/c;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4676
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 4677
    or-int/lit8 v0, v0, 0x2

    .line 4679
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/d;->c:Lcom/sec/chaton/a/bc;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/c;->a(Lcom/sec/chaton/a/c;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;

    .line 4680
    invoke-static {v2, v0}, Lcom/sec/chaton/a/c;->a(Lcom/sec/chaton/a/c;I)I

    .line 4681
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->c()Lcom/sec/chaton/a/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0}, Lcom/sec/chaton/a/d;->c()Lcom/sec/chaton/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 4696
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/d;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    check-cast p1, Lcom/sec/chaton/a/c;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/d;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4618
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/d;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method
