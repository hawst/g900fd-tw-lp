.class public final Lcom/sec/chaton/a/f;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/h;


# static fields
.field private static final a:Lcom/sec/chaton/a/f;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/ej;

.field private e:Lcom/google/protobuf/LazyStringList;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11608
    new-instance v0, Lcom/sec/chaton/a/f;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/f;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/f;->a:Lcom/sec/chaton/a/f;

    .line 11609
    sget-object v0, Lcom/sec/chaton/a/f;->a:Lcom/sec/chaton/a/f;

    invoke-direct {v0}, Lcom/sec/chaton/a/f;->j()V

    .line 11610
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/g;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 11159
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 11212
    iput-byte v0, p0, Lcom/sec/chaton/a/f;->f:B

    .line 11235
    iput v0, p0, Lcom/sec/chaton/a/f;->g:I

    .line 11160
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/g;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 11154
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/f;-><init>(Lcom/sec/chaton/a/g;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 11161
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 11212
    iput-byte v0, p0, Lcom/sec/chaton/a/f;->f:B

    .line 11235
    iput v0, p0, Lcom/sec/chaton/a/f;->g:I

    .line 11161
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/f;I)I
    .locals 0

    .prologue
    .line 11154
    iput p1, p0, Lcom/sec/chaton/a/f;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/f;J)J
    .locals 0

    .prologue
    .line 11154
    iput-wide p1, p0, Lcom/sec/chaton/a/f;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/f;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 11154
    iput-object p1, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/f;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 11154
    iput-object p1, p0, Lcom/sec/chaton/a/f;->d:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/f;
    .locals 1

    .prologue
    .line 11165
    sget-object v0, Lcom/sec/chaton/a/f;->a:Lcom/sec/chaton/a/f;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/f;
    .locals 1

    .prologue
    .line 11283
    invoke-static {}, Lcom/sec/chaton/a/f;->newBuilder()Lcom/sec/chaton/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/g;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/g;

    invoke-static {v0}, Lcom/sec/chaton/a/g;->a(Lcom/sec/chaton/a/g;)Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/f;)Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11339
    invoke-static {}, Lcom/sec/chaton/a/f;->newBuilder()Lcom/sec/chaton/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/g;->a(Lcom/sec/chaton/a/f;)Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/f;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 11154
    iget-object v0, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 11208
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/f;->c:J

    .line 11209
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/f;->d:Lcom/sec/chaton/a/ej;

    .line 11210
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    .line 11211
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11336
    invoke-static {}, Lcom/sec/chaton/a/g;->h()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/f;
    .locals 1

    .prologue
    .line 11169
    sget-object v0, Lcom/sec/chaton/a/f;->a:Lcom/sec/chaton/a/f;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 11177
    iget v1, p0, Lcom/sec/chaton/a/f;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 11180
    iget-wide v0, p0, Lcom/sec/chaton/a/f;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 11187
    iget v0, p0, Lcom/sec/chaton/a/f;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 11190
    iget-object v0, p0, Lcom/sec/chaton/a/f;->d:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11198
    iget-object v0, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11154
    invoke-virtual {p0}, Lcom/sec/chaton/a/f;->b()Lcom/sec/chaton/a/f;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 11237
    iget v0, p0, Lcom/sec/chaton/a/f;->g:I

    .line 11238
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 11259
    :goto_0
    return v0

    .line 11241
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/f;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 11242
    iget-wide v2, p0, Lcom/sec/chaton/a/f;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 11245
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/f;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 11246
    iget-object v2, p0, Lcom/sec/chaton/a/f;->d:Lcom/sec/chaton/a/ej;

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    .line 11251
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 11252
    iget-object v3, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 11251
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 11255
    :cond_2
    add-int/2addr v0, v2

    .line 11256
    invoke-virtual {p0}, Lcom/sec/chaton/a/f;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11258
    iput v0, p0, Lcom/sec/chaton/a/f;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public h()Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11337
    invoke-static {}, Lcom/sec/chaton/a/f;->newBuilder()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public i()Lcom/sec/chaton/a/g;
    .locals 1

    .prologue
    .line 11341
    invoke-static {p0}, Lcom/sec/chaton/a/f;->a(Lcom/sec/chaton/a/f;)Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 11214
    iget-byte v1, p0, Lcom/sec/chaton/a/f;->f:B

    .line 11215
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 11218
    :goto_0
    return v0

    .line 11215
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 11217
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/f;->f:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11154
    invoke-virtual {p0}, Lcom/sec/chaton/a/f;->h()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11154
    invoke-virtual {p0}, Lcom/sec/chaton/a/f;->i()Lcom/sec/chaton/a/g;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11266
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 11223
    invoke-virtual {p0}, Lcom/sec/chaton/a/f;->getSerializedSize()I

    .line 11224
    iget v0, p0, Lcom/sec/chaton/a/f;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 11225
    iget-wide v0, p0, Lcom/sec/chaton/a/f;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 11227
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/f;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 11228
    iget-object v0, p0, Lcom/sec/chaton/a/f;->d:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 11230
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 11231
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/chaton/a/f;->e:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 11230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11233
    :cond_2
    return-void
.end method
