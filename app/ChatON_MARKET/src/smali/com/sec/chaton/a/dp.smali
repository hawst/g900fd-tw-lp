.class public final enum Lcom/sec/chaton/a/dp;
.super Ljava/lang/Enum;
.source "SSMGPB.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/a/dp;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/a/dp;

.field public static final enum b:Lcom/sec/chaton/a/dp;

.field public static final enum c:Lcom/sec/chaton/a/dp;

.field public static final enum d:Lcom/sec/chaton/a/dp;

.field public static final enum e:Lcom/sec/chaton/a/dp;

.field public static final enum f:Lcom/sec/chaton/a/dp;

.field private static g:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sec/chaton/a/dp;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic i:[Lcom/sec/chaton/a/dp;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 63
    new-instance v0, Lcom/sec/chaton/a/dp;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sec/chaton/a/dp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    .line 64
    new-instance v0, Lcom/sec/chaton/a/dp;

    const-string v1, "MEDIA"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sec/chaton/a/dp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/dp;->b:Lcom/sec/chaton/a/dp;

    .line 65
    new-instance v0, Lcom/sec/chaton/a/dp;

    const-string v1, "NOTI"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/sec/chaton/a/dp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/dp;->c:Lcom/sec/chaton/a/dp;

    .line 66
    new-instance v0, Lcom/sec/chaton/a/dp;

    const-string v1, "ANS"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/sec/chaton/a/dp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/dp;->d:Lcom/sec/chaton/a/dp;

    .line 67
    new-instance v0, Lcom/sec/chaton/a/dp;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/sec/chaton/a/dp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/dp;->e:Lcom/sec/chaton/a/dp;

    .line 68
    new-instance v0, Lcom/sec/chaton/a/dp;

    const-string v1, "MACK"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/a/dp;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/chaton/a/dp;->f:Lcom/sec/chaton/a/dp;

    .line 61
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/chaton/a/dp;

    sget-object v1, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/a/dp;->b:Lcom/sec/chaton/a/dp;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/a/dp;->c:Lcom/sec/chaton/a/dp;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/a/dp;->d:Lcom/sec/chaton/a/dp;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/chaton/a/dp;->e:Lcom/sec/chaton/a/dp;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/a/dp;->f:Lcom/sec/chaton/a/dp;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/a/dp;->i:[Lcom/sec/chaton/a/dp;

    .line 98
    new-instance v0, Lcom/sec/chaton/a/dq;

    invoke-direct {v0}, Lcom/sec/chaton/a/dq;-><init>()V

    sput-object v0, Lcom/sec/chaton/a/dp;->g:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 108
    iput p4, p0, Lcom/sec/chaton/a/dp;->h:I

    .line 109
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 82
    packed-switch p0, :pswitch_data_0

    .line 89
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 83
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 84
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/a/dp;->b:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 85
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/a/dp;->c:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 86
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/a/dp;->d:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 87
    :pswitch_4
    sget-object v0, Lcom/sec/chaton/a/dp;->e:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 88
    :pswitch_5
    sget-object v0, Lcom/sec/chaton/a/dp;->f:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/sec/chaton/a/dp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dp;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/chaton/a/dp;->i:[Lcom/sec/chaton/a/dp;

    invoke-virtual {v0}, [Lcom/sec/chaton/a/dp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/a/dp;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/chaton/a/dp;->h:I

    return v0
.end method
