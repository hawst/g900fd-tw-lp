.class public final Lcom/sec/chaton/a/u;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/w;


# static fields
.field private static final a:Lcom/sec/chaton/a/u;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/x;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/a/ej;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17025
    new-instance v0, Lcom/sec/chaton/a/u;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/u;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/u;->a:Lcom/sec/chaton/a/u;

    .line 17026
    sget-object v0, Lcom/sec/chaton/a/u;->a:Lcom/sec/chaton/a/u;

    invoke-direct {v0}, Lcom/sec/chaton/a/u;->j()V

    .line 17027
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 16504
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 16586
    iput-byte v0, p0, Lcom/sec/chaton/a/u;->f:B

    .line 16609
    iput v0, p0, Lcom/sec/chaton/a/u;->g:I

    .line 16505
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/v;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 16499
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/u;-><init>(Lcom/sec/chaton/a/v;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 16506
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 16586
    iput-byte v0, p0, Lcom/sec/chaton/a/u;->f:B

    .line 16609
    iput v0, p0, Lcom/sec/chaton/a/u;->g:I

    .line 16506
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/u;I)I
    .locals 0

    .prologue
    .line 16499
    iput p1, p0, Lcom/sec/chaton/a/u;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/u;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 16499
    iput-object p1, p0, Lcom/sec/chaton/a/u;->e:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/u;
    .locals 1

    .prologue
    .line 16510
    sget-object v0, Lcom/sec/chaton/a/u;->a:Lcom/sec/chaton/a/u;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/u;
    .locals 1

    .prologue
    .line 16652
    invoke-static {}, Lcom/sec/chaton/a/u;->newBuilder()Lcom/sec/chaton/a/v;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/v;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/v;

    invoke-static {v0}, Lcom/sec/chaton/a/v;->a(Lcom/sec/chaton/a/v;)Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/u;)Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16708
    invoke-static {}, Lcom/sec/chaton/a/u;->newBuilder()Lcom/sec/chaton/a/v;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/v;->a(Lcom/sec/chaton/a/u;)Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/u;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 16499
    iput-object p1, p0, Lcom/sec/chaton/a/u;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/u;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 16499
    iput-object p1, p0, Lcom/sec/chaton/a/u;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/u;)Ljava/util/List;
    .locals 1

    .prologue
    .line 16499
    iget-object v0, p0, Lcom/sec/chaton/a/u;->d:Ljava/util/List;

    return-object v0
.end method

.method private i()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 16539
    iget-object v0, p0, Lcom/sec/chaton/a/u;->c:Ljava/lang/Object;

    .line 16540
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16541
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 16543
    iput-object v0, p0, Lcom/sec/chaton/a/u;->c:Ljava/lang/Object;

    .line 16546
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 16582
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/u;->c:Ljava/lang/Object;

    .line 16583
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/u;->d:Ljava/util/List;

    .line 16584
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/u;->e:Lcom/sec/chaton/a/ej;

    .line 16585
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16705
    invoke-static {}, Lcom/sec/chaton/a/v;->j()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/u;
    .locals 1

    .prologue
    .line 16514
    sget-object v0, Lcom/sec/chaton/a/u;->a:Lcom/sec/chaton/a/u;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 16522
    iget v1, p0, Lcom/sec/chaton/a/u;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16525
    iget-object v0, p0, Lcom/sec/chaton/a/u;->c:Ljava/lang/Object;

    .line 16526
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16527
    check-cast v0, Ljava/lang/String;

    .line 16535
    :goto_0
    return-object v0

    .line 16529
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 16531
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 16532
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16533
    iput-object v1, p0, Lcom/sec/chaton/a/u;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 16535
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 16575
    iget v0, p0, Lcom/sec/chaton/a/u;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 16578
    iget-object v0, p0, Lcom/sec/chaton/a/u;->e:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16706
    invoke-static {}, Lcom/sec/chaton/a/u;->newBuilder()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16499
    invoke-virtual {p0}, Lcom/sec/chaton/a/u;->b()Lcom/sec/chaton/a/u;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 16611
    iget v2, p0, Lcom/sec/chaton/a/u;->g:I

    .line 16612
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 16628
    :goto_0
    return v2

    .line 16615
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/u;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 16616
    invoke-direct {p0}, Lcom/sec/chaton/a/u;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v0

    .line 16619
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/u;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 16620
    iget-object v0, p0, Lcom/sec/chaton/a/u;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 16619
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 16623
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/u;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 16624
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/u;->e:Lcom/sec/chaton/a/ej;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 16627
    :cond_2
    iput v2, p0, Lcom/sec/chaton/a/u;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public h()Lcom/sec/chaton/a/v;
    .locals 1

    .prologue
    .line 16710
    invoke-static {p0}, Lcom/sec/chaton/a/u;->a(Lcom/sec/chaton/a/u;)Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 16588
    iget-byte v1, p0, Lcom/sec/chaton/a/u;->f:B

    .line 16589
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 16592
    :goto_0
    return v0

    .line 16589
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 16591
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/u;->f:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16499
    invoke-virtual {p0}, Lcom/sec/chaton/a/u;->g()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16499
    invoke-virtual {p0}, Lcom/sec/chaton/a/u;->h()Lcom/sec/chaton/a/v;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16635
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 16597
    invoke-virtual {p0}, Lcom/sec/chaton/a/u;->getSerializedSize()I

    .line 16598
    iget v0, p0, Lcom/sec/chaton/a/u;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 16599
    invoke-direct {p0}, Lcom/sec/chaton/a/u;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 16601
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/u;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 16602
    iget-object v0, p0, Lcom/sec/chaton/a/u;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 16601
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 16604
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/u;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 16605
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/u;->e:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 16607
    :cond_2
    return-void
.end method
