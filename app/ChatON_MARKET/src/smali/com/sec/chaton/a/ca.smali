.class public final Lcom/sec/chaton/a/ca;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/cb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/bz;",
        "Lcom/sec/chaton/a/ca;",
        ">;",
        "Lcom/sec/chaton/a/cb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 15924
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 16059
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    .line 15925
    invoke-direct {p0}, Lcom/sec/chaton/a/ca;->i()V

    .line 15926
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ca;)Lcom/sec/chaton/a/bz;
    .locals 1

    .prologue
    .line 15919
    invoke-direct {p0}, Lcom/sec/chaton/a/ca;->k()Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 15919
    invoke-static {}, Lcom/sec/chaton/a/ca;->j()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 15929
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 15931
    new-instance v0, Lcom/sec/chaton/a/ca;

    invoke-direct {v0}, Lcom/sec/chaton/a/ca;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/bz;
    .locals 2

    .prologue
    .line 15961
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->e()Lcom/sec/chaton/a/bz;

    move-result-object v0

    .line 15962
    invoke-virtual {v0}, Lcom/sec/chaton/a/bz;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15963
    invoke-static {v0}, Lcom/sec/chaton/a/ca;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 15966
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ca;
    .locals 2

    .prologue
    .line 15935
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 15936
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ca;->b:J

    .line 15937
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ca;->a:I

    .line 15938
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    .line 15939
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ca;->a:I

    .line 15940
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 16046
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ca;->a:I

    .line 16047
    iput-wide p1, p0, Lcom/sec/chaton/a/ca;->b:J

    .line 16049
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ca;
    .locals 2

    .prologue
    .line 16005
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 16006
    sparse-switch v0, :sswitch_data_0

    .line 16011
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ca;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16013
    :sswitch_0
    return-object p0

    .line 16018
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ca;->a:I

    .line 16019
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ca;->b:J

    goto :goto_0

    .line 16023
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 16024
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 16025
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 16027
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 16028
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ca;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ca;

    goto :goto_0

    .line 16006
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bz;)Lcom/sec/chaton/a/ca;
    .locals 2

    .prologue
    .line 15986
    invoke-static {}, Lcom/sec/chaton/a/bz;->a()Lcom/sec/chaton/a/bz;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 15993
    :cond_0
    :goto_0
    return-object p0

    .line 15987
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/bz;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15988
    invoke-virtual {p1}, Lcom/sec/chaton/a/bz;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ca;->a(J)Lcom/sec/chaton/a/ca;

    .line 15990
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/bz;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15991
    invoke-virtual {p1}, Lcom/sec/chaton/a/bz;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ca;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ca;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ca;
    .locals 1

    .prologue
    .line 16067
    if-nez p1, :cond_0

    .line 16068
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16070
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    .line 16072
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ca;->a:I

    .line 16073
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ca;
    .locals 2

    .prologue
    .line 15944
    invoke-static {}, Lcom/sec/chaton/a/ca;->j()Lcom/sec/chaton/a/ca;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->e()Lcom/sec/chaton/a/bz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ca;->a(Lcom/sec/chaton/a/bz;)Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ca;
    .locals 2

    .prologue
    .line 16083
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 16085
    iget-object v0, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    .line 16091
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ca;->a:I

    .line 16092
    return-object p0

    .line 16088
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->d()Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->e()Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/bz;
    .locals 1

    .prologue
    .line 15948
    invoke-static {}, Lcom/sec/chaton/a/bz;->a()Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->a()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->a()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->b()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->b()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->b()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->b()Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/bz;
    .locals 2

    .prologue
    .line 15952
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->e()Lcom/sec/chaton/a/bz;

    move-result-object v0

    .line 15953
    invoke-virtual {v0}, Lcom/sec/chaton/a/bz;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15954
    invoke-static {v0}, Lcom/sec/chaton/a/ca;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 15956
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/bz;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 15970
    new-instance v2, Lcom/sec/chaton/a/bz;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/bz;-><init>(Lcom/sec/chaton/a/ca;Lcom/sec/chaton/a/b;)V

    .line 15971
    iget v3, p0, Lcom/sec/chaton/a/ca;->a:I

    .line 15972
    const/4 v1, 0x0

    .line 15973
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 15976
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/ca;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/bz;->a(Lcom/sec/chaton/a/bz;J)J

    .line 15977
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 15978
    or-int/lit8 v0, v0, 0x2

    .line 15980
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bz;->a(Lcom/sec/chaton/a/bz;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 15981
    invoke-static {v2, v0}, Lcom/sec/chaton/a/bz;->a(Lcom/sec/chaton/a/bz;I)I

    .line 15982
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 16061
    iget v0, p0, Lcom/sec/chaton/a/ca;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 16064
    iget-object v0, p0, Lcom/sec/chaton/a/ca;->c:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->c()Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0}, Lcom/sec/chaton/a/ca;->c()Lcom/sec/chaton/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 15997
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ca;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    check-cast p1, Lcom/sec/chaton/a/bz;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ca;->a(Lcom/sec/chaton/a/bz;)Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15919
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ca;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ca;

    move-result-object v0

    return-object v0
.end method
