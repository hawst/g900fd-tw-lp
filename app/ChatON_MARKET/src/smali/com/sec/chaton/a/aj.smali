.class public final Lcom/sec/chaton/a/aj;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/al;


# static fields
.field private static final a:Lcom/sec/chaton/a/aj;


# instance fields
.field private b:I

.field private c:Lcom/sec/chaton/a/au;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6567
    new-instance v0, Lcom/sec/chaton/a/aj;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/aj;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/aj;->a:Lcom/sec/chaton/a/aj;

    .line 6568
    sget-object v0, Lcom/sec/chaton/a/aj;->a:Lcom/sec/chaton/a/aj;

    invoke-direct {v0}, Lcom/sec/chaton/a/aj;->m()V

    .line 6569
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/ak;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6116
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 6209
    iput-byte v0, p0, Lcom/sec/chaton/a/aj;->f:B

    .line 6232
    iput v0, p0, Lcom/sec/chaton/a/aj;->g:I

    .line 6117
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/ak;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 6111
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/aj;-><init>(Lcom/sec/chaton/a/ak;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6118
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 6209
    iput-byte v0, p0, Lcom/sec/chaton/a/aj;->f:B

    .line 6232
    iput v0, p0, Lcom/sec/chaton/a/aj;->g:I

    .line 6118
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/aj;I)I
    .locals 0

    .prologue
    .line 6111
    iput p1, p0, Lcom/sec/chaton/a/aj;->b:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/aj;
    .locals 1

    .prologue
    .line 6122
    sget-object v0, Lcom/sec/chaton/a/aj;->a:Lcom/sec/chaton/a/aj;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6331
    invoke-static {}, Lcom/sec/chaton/a/aj;->newBuilder()Lcom/sec/chaton/a/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/ak;->a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/aj;Lcom/sec/chaton/a/au;)Lcom/sec/chaton/a/au;
    .locals 0

    .prologue
    .line 6111
    iput-object p1, p0, Lcom/sec/chaton/a/aj;->c:Lcom/sec/chaton/a/au;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/aj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 6111
    iput-object p1, p0, Lcom/sec/chaton/a/aj;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/aj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 6111
    iput-object p1, p0, Lcom/sec/chaton/a/aj;->e:Ljava/lang/Object;

    return-object p1
.end method

.method private k()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6161
    iget-object v0, p0, Lcom/sec/chaton/a/aj;->d:Ljava/lang/Object;

    .line 6162
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6163
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6165
    iput-object v0, p0, Lcom/sec/chaton/a/aj;->d:Ljava/lang/Object;

    .line 6168
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private l()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6193
    iget-object v0, p0, Lcom/sec/chaton/a/aj;->e:Ljava/lang/Object;

    .line 6194
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6195
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6197
    iput-object v0, p0, Lcom/sec/chaton/a/aj;->e:Ljava/lang/Object;

    .line 6200
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 6205
    sget-object v0, Lcom/sec/chaton/a/au;->a:Lcom/sec/chaton/a/au;

    iput-object v0, p0, Lcom/sec/chaton/a/aj;->c:Lcom/sec/chaton/a/au;

    .line 6206
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/aj;->d:Ljava/lang/Object;

    .line 6207
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/aj;->e:Ljava/lang/Object;

    .line 6208
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6328
    invoke-static {}, Lcom/sec/chaton/a/ak;->f()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/aj;
    .locals 1

    .prologue
    .line 6126
    sget-object v0, Lcom/sec/chaton/a/aj;->a:Lcom/sec/chaton/a/aj;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6134
    iget v1, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lcom/sec/chaton/a/au;
    .locals 1

    .prologue
    .line 6137
    iget-object v0, p0, Lcom/sec/chaton/a/aj;->c:Lcom/sec/chaton/a/au;

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 6144
    iget v0, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6147
    iget-object v0, p0, Lcom/sec/chaton/a/aj;->d:Ljava/lang/Object;

    .line 6148
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6149
    check-cast v0, Ljava/lang/String;

    .line 6157
    :goto_0
    return-object v0

    .line 6151
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6153
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6154
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6155
    iput-object v1, p0, Lcom/sec/chaton/a/aj;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6157
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 6176
    iget v0, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6111
    invoke-virtual {p0}, Lcom/sec/chaton/a/aj;->b()Lcom/sec/chaton/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 6234
    iget v0, p0, Lcom/sec/chaton/a/aj;->g:I

    .line 6235
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6251
    :goto_0
    return v0

    .line 6237
    :cond_0
    const/4 v0, 0x0

    .line 6238
    iget v1, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 6239
    iget-object v1, p0, Lcom/sec/chaton/a/aj;->c:Lcom/sec/chaton/a/au;

    invoke-virtual {v1}, Lcom/sec/chaton/a/au;->getNumber()I

    move-result v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6242
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 6243
    invoke-direct {p0}, Lcom/sec/chaton/a/aj;->k()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6246
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 6247
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/aj;->l()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6250
    :cond_3
    iput v0, p0, Lcom/sec/chaton/a/aj;->g:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6179
    iget-object v0, p0, Lcom/sec/chaton/a/aj;->e:Ljava/lang/Object;

    .line 6180
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6181
    check-cast v0, Ljava/lang/String;

    .line 6189
    :goto_0
    return-object v0

    .line 6183
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6185
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6186
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6187
    iput-object v1, p0, Lcom/sec/chaton/a/aj;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6189
    goto :goto_0
.end method

.method public i()Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6329
    invoke-static {}, Lcom/sec/chaton/a/aj;->newBuilder()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 6211
    iget-byte v1, p0, Lcom/sec/chaton/a/aj;->f:B

    .line 6212
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 6215
    :goto_0
    return v0

    .line 6212
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6214
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/aj;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/ak;
    .locals 1

    .prologue
    .line 6333
    invoke-static {p0}, Lcom/sec/chaton/a/aj;->a(Lcom/sec/chaton/a/aj;)Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6111
    invoke-virtual {p0}, Lcom/sec/chaton/a/aj;->i()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6111
    invoke-virtual {p0}, Lcom/sec/chaton/a/aj;->j()Lcom/sec/chaton/a/ak;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6258
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 6220
    invoke-virtual {p0}, Lcom/sec/chaton/a/aj;->getSerializedSize()I

    .line 6221
    iget v0, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 6222
    iget-object v0, p0, Lcom/sec/chaton/a/aj;->c:Lcom/sec/chaton/a/au;

    invoke-virtual {v0}, Lcom/sec/chaton/a/au;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 6224
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 6225
    invoke-direct {p0}, Lcom/sec/chaton/a/aj;->k()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6227
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/aj;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 6228
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/aj;->l()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6230
    :cond_2
    return-void
.end method
