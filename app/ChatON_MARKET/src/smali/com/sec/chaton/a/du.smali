.class public final Lcom/sec/chaton/a/du;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dw;


# static fields
.field private static final a:Lcom/sec/chaton/a/du;


# instance fields
.field private b:I

.field private c:J

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dr;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/a/ej;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21192
    new-instance v0, Lcom/sec/chaton/a/du;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/du;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/du;->a:Lcom/sec/chaton/a/du;

    .line 21193
    sget-object v0, Lcom/sec/chaton/a/du;->a:Lcom/sec/chaton/a/du;

    invoke-direct {v0}, Lcom/sec/chaton/a/du;->i()V

    .line 21194
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/dv;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 20708
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 20768
    iput-byte v0, p0, Lcom/sec/chaton/a/du;->f:B

    .line 20791
    iput v0, p0, Lcom/sec/chaton/a/du;->g:I

    .line 20709
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/dv;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 20703
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/du;-><init>(Lcom/sec/chaton/a/dv;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 20710
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 20768
    iput-byte v0, p0, Lcom/sec/chaton/a/du;->f:B

    .line 20791
    iput v0, p0, Lcom/sec/chaton/a/du;->g:I

    .line 20710
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/du;I)I
    .locals 0

    .prologue
    .line 20703
    iput p1, p0, Lcom/sec/chaton/a/du;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/du;J)J
    .locals 0

    .prologue
    .line 20703
    iput-wide p1, p0, Lcom/sec/chaton/a/du;->c:J

    return-wide p1
.end method

.method public static a()Lcom/sec/chaton/a/du;
    .locals 1

    .prologue
    .line 20714
    sget-object v0, Lcom/sec/chaton/a/du;->a:Lcom/sec/chaton/a/du;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/du;
    .locals 1

    .prologue
    .line 20834
    invoke-static {}, Lcom/sec/chaton/a/du;->newBuilder()Lcom/sec/chaton/a/dv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dv;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dv;

    invoke-static {v0}, Lcom/sec/chaton/a/dv;->a(Lcom/sec/chaton/a/dv;)Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/du;)Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 20890
    invoke-static {}, Lcom/sec/chaton/a/du;->newBuilder()Lcom/sec/chaton/a/dv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/dv;->a(Lcom/sec/chaton/a/du;)Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/du;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;
    .locals 0

    .prologue
    .line 20703
    iput-object p1, p0, Lcom/sec/chaton/a/du;->e:Lcom/sec/chaton/a/ej;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/du;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 20703
    iput-object p1, p0, Lcom/sec/chaton/a/du;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/du;)Ljava/util/List;
    .locals 1

    .prologue
    .line 20703
    iget-object v0, p0, Lcom/sec/chaton/a/du;->d:Ljava/util/List;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 20764
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/du;->c:J

    .line 20765
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/du;->d:Ljava/util/List;

    .line 20766
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/du;->e:Lcom/sec/chaton/a/ej;

    .line 20767
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 20887
    invoke-static {}, Lcom/sec/chaton/a/dv;->h()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/du;
    .locals 1

    .prologue
    .line 20718
    sget-object v0, Lcom/sec/chaton/a/du;->a:Lcom/sec/chaton/a/du;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 20726
    iget v1, p0, Lcom/sec/chaton/a/du;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 20729
    iget-wide v0, p0, Lcom/sec/chaton/a/du;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 20757
    iget v0, p0, Lcom/sec/chaton/a/du;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 20760
    iget-object v0, p0, Lcom/sec/chaton/a/du;->e:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 20888
    invoke-static {}, Lcom/sec/chaton/a/du;->newBuilder()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20703
    invoke-virtual {p0}, Lcom/sec/chaton/a/du;->b()Lcom/sec/chaton/a/du;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 20793
    iget v2, p0, Lcom/sec/chaton/a/du;->g:I

    .line 20794
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 20810
    :goto_0
    return v2

    .line 20797
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/du;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 20798
    iget-wide v2, p0, Lcom/sec/chaton/a/du;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v0

    .line 20801
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/du;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 20802
    iget-object v0, p0, Lcom/sec/chaton/a/du;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 20801
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 20805
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/du;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 20806
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/du;->e:Lcom/sec/chaton/a/ej;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 20809
    :cond_2
    iput v2, p0, Lcom/sec/chaton/a/du;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public h()Lcom/sec/chaton/a/dv;
    .locals 1

    .prologue
    .line 20892
    invoke-static {p0}, Lcom/sec/chaton/a/du;->a(Lcom/sec/chaton/a/du;)Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 20770
    iget-byte v1, p0, Lcom/sec/chaton/a/du;->f:B

    .line 20771
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 20774
    :goto_0
    return v0

    .line 20771
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 20773
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/du;->f:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20703
    invoke-virtual {p0}, Lcom/sec/chaton/a/du;->g()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20703
    invoke-virtual {p0}, Lcom/sec/chaton/a/du;->h()Lcom/sec/chaton/a/dv;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20817
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 20779
    invoke-virtual {p0}, Lcom/sec/chaton/a/du;->getSerializedSize()I

    .line 20780
    iget v0, p0, Lcom/sec/chaton/a/du;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 20781
    iget-wide v0, p0, Lcom/sec/chaton/a/du;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 20783
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/du;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 20784
    iget-object v0, p0, Lcom/sec/chaton/a/du;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 20783
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 20786
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/du;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 20787
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/chaton/a/du;->e:Lcom/sec/chaton/a/ej;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 20789
    :cond_2
    return-void
.end method
