.class public final Lcom/sec/chaton/a/ep;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/er;


# static fields
.field private static final a:Lcom/sec/chaton/a/ep;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/bc;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/protobuf/LazyStringList;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:J

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/es;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25897
    new-instance v0, Lcom/sec/chaton/a/ep;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ep;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ep;->a:Lcom/sec/chaton/a/ep;

    .line 25898
    sget-object v0, Lcom/sec/chaton/a/ep;->a:Lcom/sec/chaton/a/ep;

    invoke-direct {v0}, Lcom/sec/chaton/a/ep;->y()V

    .line 25899
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/eq;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24913
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 25131
    iput-byte v0, p0, Lcom/sec/chaton/a/ep;->l:B

    .line 25172
    iput v0, p0, Lcom/sec/chaton/a/ep;->m:I

    .line 24914
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/eq;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 24908
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ep;-><init>(Lcom/sec/chaton/a/eq;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24915
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25131
    iput-byte v0, p0, Lcom/sec/chaton/a/ep;->l:B

    .line 25172
    iput v0, p0, Lcom/sec/chaton/a/ep;->m:I

    .line 24915
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ep;I)I
    .locals 0

    .prologue
    .line 24908
    iput p1, p0, Lcom/sec/chaton/a/ep;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ep;J)J
    .locals 0

    .prologue
    .line 24908
    iput-wide p1, p0, Lcom/sec/chaton/a/ep;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ep;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 24908
    iput-object p1, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ep;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 24908
    iput-object p1, p0, Lcom/sec/chaton/a/ep;->d:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/ep;
    .locals 1

    .prologue
    .line 24919
    sget-object v0, Lcom/sec/chaton/a/ep;->a:Lcom/sec/chaton/a/ep;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/ep;
    .locals 1

    .prologue
    .line 25244
    invoke-static {}, Lcom/sec/chaton/a/ep;->newBuilder()Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/eq;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/eq;

    invoke-static {v0}, Lcom/sec/chaton/a/eq;->a(Lcom/sec/chaton/a/eq;)Lcom/sec/chaton/a/ep;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ep;)Lcom/sec/chaton/a/eq;
    .locals 1

    .prologue
    .line 25300
    invoke-static {}, Lcom/sec/chaton/a/ep;->newBuilder()Lcom/sec/chaton/a/eq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/eq;->a(Lcom/sec/chaton/a/ep;)Lcom/sec/chaton/a/eq;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ep;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 24908
    iput-object p1, p0, Lcom/sec/chaton/a/ep;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ep;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 24908
    iput-object p1, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ep;J)J
    .locals 0

    .prologue
    .line 24908
    iput-wide p1, p0, Lcom/sec/chaton/a/ep;->j:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ep;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 24908
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/ep;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 24908
    iput-object p1, p0, Lcom/sec/chaton/a/ep;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/ep;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 24908
    iput-object p1, p0, Lcom/sec/chaton/a/ep;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/ep;)Ljava/util/List;
    .locals 1

    .prologue
    .line 24908
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/a/ep;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 24908
    iput-object p1, p0, Lcom/sec/chaton/a/ep;->i:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/eq;
    .locals 1

    .prologue
    .line 25297
    invoke-static {}, Lcom/sec/chaton/a/eq;->f()Lcom/sec/chaton/a/eq;

    move-result-object v0

    return-object v0
.end method

.method private u()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 24968
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->e:Ljava/lang/Object;

    .line 24969
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24970
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 24972
    iput-object v0, p0, Lcom/sec/chaton/a/ep;->e:Ljava/lang/Object;

    .line 24975
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private v()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 25000
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->f:Ljava/lang/Object;

    .line 25001
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25002
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 25004
    iput-object v0, p0, Lcom/sec/chaton/a/ep;->f:Ljava/lang/Object;

    .line 25007
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private w()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 25046
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->h:Ljava/lang/Object;

    .line 25047
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25048
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 25050
    iput-object v0, p0, Lcom/sec/chaton/a/ep;->h:Ljava/lang/Object;

    .line 25053
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private x()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 25078
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->i:Ljava/lang/Object;

    .line 25079
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25080
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 25082
    iput-object v0, p0, Lcom/sec/chaton/a/ep;->i:Ljava/lang/Object;

    .line 25085
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private y()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 25121
    iput-wide v1, p0, Lcom/sec/chaton/a/ep;->c:J

    .line 25122
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/ep;->d:Lcom/sec/chaton/a/bc;

    .line 25123
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ep;->e:Ljava/lang/Object;

    .line 25124
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ep;->f:Ljava/lang/Object;

    .line 25125
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    .line 25126
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ep;->h:Ljava/lang/Object;

    .line 25127
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ep;->i:Ljava/lang/Object;

    .line 25128
    iput-wide v1, p0, Lcom/sec/chaton/a/ep;->j:J

    .line 25129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    .line 25130
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ep;
    .locals 1

    .prologue
    .line 24923
    sget-object v0, Lcom/sec/chaton/a/ep;->a:Lcom/sec/chaton/a/ep;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 24931
    iget v1, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 24934
    iget-wide v0, p0, Lcom/sec/chaton/a/ep;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 24941
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 24944
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->d:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 24951
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24908
    invoke-virtual {p0}, Lcom/sec/chaton/a/ep;->b()Lcom/sec/chaton/a/ep;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 25174
    iget v2, p0, Lcom/sec/chaton/a/ep;->m:I

    .line 25175
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 25220
    :goto_0
    return v2

    .line 25178
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 25179
    iget-wide v2, p0, Lcom/sec/chaton/a/ep;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 25182
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 25183
    iget-object v2, p0, Lcom/sec/chaton/a/ep;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 25186
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 25187
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->u()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 25190
    :cond_2
    iget v2, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_3

    .line 25191
    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->v()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v1

    .line 25196
    :goto_2
    iget-object v4, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 25197
    iget-object v4, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v2}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v3, v4

    .line 25196
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 25200
    :cond_4
    add-int/2addr v0, v3

    .line 25201
    invoke-virtual {p0}, Lcom/sec/chaton/a/ep;->k()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 25203
    iget v2, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    .line 25204
    const/4 v2, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->w()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 25207
    :cond_5
    iget v2, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    .line 25208
    const/4 v2, 0x7

    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->x()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 25211
    :cond_6
    iget v2, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    .line 25212
    iget-wide v2, p0, Lcom/sec/chaton/a/ep;->j:J

    invoke-static {v7, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    move v2, v0

    .line 25215
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 25216
    const/16 v3, 0x9

    iget-object v0, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 25215
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_3

    .line 25219
    :cond_8
    iput v2, p0, Lcom/sec/chaton/a/ep;->m:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24954
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->e:Ljava/lang/Object;

    .line 24955
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24956
    check-cast v0, Ljava/lang/String;

    .line 24964
    :goto_0
    return-object v0

    .line 24958
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 24960
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 24961
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24962
    iput-object v1, p0, Lcom/sec/chaton/a/ep;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 24964
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 24983
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 25133
    iget-byte v1, p0, Lcom/sec/chaton/a/ep;->l:B

    .line 25134
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 25137
    :goto_0
    return v0

    .line 25134
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 25136
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ep;->l:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24986
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->f:Ljava/lang/Object;

    .line 24987
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24988
    check-cast v0, Ljava/lang/String;

    .line 24996
    :goto_0
    return-object v0

    .line 24990
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 24992
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 24993
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24994
    iput-object v1, p0, Lcom/sec/chaton/a/ep;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 24996
    goto :goto_0
.end method

.method public k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25016
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 25029
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25032
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->h:Ljava/lang/Object;

    .line 25033
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25034
    check-cast v0, Ljava/lang/String;

    .line 25042
    :goto_0
    return-object v0

    .line 25036
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 25038
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 25039
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25040
    iput-object v1, p0, Lcom/sec/chaton/a/ep;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 25042
    goto :goto_0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 25061
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24908
    invoke-virtual {p0}, Lcom/sec/chaton/a/ep;->s()Lcom/sec/chaton/a/eq;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25064
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->i:Ljava/lang/Object;

    .line 25065
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25066
    check-cast v0, Ljava/lang/String;

    .line 25074
    :goto_0
    return-object v0

    .line 25068
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 25070
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 25071
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25072
    iput-object v1, p0, Lcom/sec/chaton/a/ep;->i:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 25074
    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 25093
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 25096
    iget-wide v0, p0, Lcom/sec/chaton/a/ep;->j:J

    return-wide v0
.end method

.method public r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/es;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25103
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    return-object v0
.end method

.method public s()Lcom/sec/chaton/a/eq;
    .locals 1

    .prologue
    .line 25298
    invoke-static {}, Lcom/sec/chaton/a/ep;->newBuilder()Lcom/sec/chaton/a/eq;

    move-result-object v0

    return-object v0
.end method

.method public t()Lcom/sec/chaton/a/eq;
    .locals 1

    .prologue
    .line 25302
    invoke-static {p0}, Lcom/sec/chaton/a/ep;->a(Lcom/sec/chaton/a/ep;)Lcom/sec/chaton/a/eq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24908
    invoke-virtual {p0}, Lcom/sec/chaton/a/ep;->t()Lcom/sec/chaton/a/eq;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25227
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 25142
    invoke-virtual {p0}, Lcom/sec/chaton/a/ep;->getSerializedSize()I

    .line 25143
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 25144
    iget-wide v2, p0, Lcom/sec/chaton/a/ep;->c:J

    invoke-virtual {p1, v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 25146
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 25147
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 25149
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 25150
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->u()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 25152
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 25153
    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->v()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    move v0, v1

    .line 25155
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 25156
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/sec/chaton/a/ep;->g:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 25155
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25158
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 25159
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->w()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 25161
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_6

    .line 25162
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sec/chaton/a/ep;->x()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 25164
    :cond_6
    iget v0, p0, Lcom/sec/chaton/a/ep;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_7

    .line 25165
    iget-wide v2, p0, Lcom/sec/chaton/a/ep;->j:J

    invoke-virtual {p1, v7, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 25167
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 25168
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/sec/chaton/a/ep;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 25167
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 25170
    :cond_8
    return-void
.end method
