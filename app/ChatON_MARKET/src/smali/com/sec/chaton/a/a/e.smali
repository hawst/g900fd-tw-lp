.class public Lcom/sec/chaton/a/a/e;
.super Ljava/lang/Object;
.source "ForwarMessageResultEntry.java"


# instance fields
.field protected a:Z

.field protected b:Lcom/sec/chaton/a/a/n;

.field protected c:I

.field protected d:Ljava/lang/String;

.field protected e:Z

.field protected f:I

.field protected g:Ljava/lang/String;

.field protected h:Ljava/lang/String;

.field protected i:Lcom/sec/chaton/e/w;

.field protected j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean v1, p0, Lcom/sec/chaton/a/a/e;->a:Z

    .line 24
    sget-object v0, Lcom/sec/chaton/a/a/n;->a:Lcom/sec/chaton/a/a/n;

    iput-object v0, p0, Lcom/sec/chaton/a/a/e;->b:Lcom/sec/chaton/a/a/n;

    .line 25
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/chaton/a/a/e;->c:I

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/a/e;->d:Ljava/lang/String;

    .line 27
    iput-boolean v1, p0, Lcom/sec/chaton/a/a/e;->e:Z

    .line 28
    iput v1, p0, Lcom/sec/chaton/a/a/e;->f:I

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/a/a/e;->j:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/a/d;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/sec/chaton/a/a/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/a/a/d;-><init>(Lcom/sec/chaton/a/a/e;)V

    return-object v0
.end method

.method public a(I)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/chaton/a/a/e;->f:I

    .line 61
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/a/n;)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/a/a/e;->b:Lcom/sec/chaton/a/a/n;

    .line 46
    return-object p0
.end method

.method public a(Lcom/sec/chaton/e/w;)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/chaton/a/a/e;->i:Lcom/sec/chaton/e/w;

    .line 76
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/chaton/a/a/e;->d:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/chaton/a/a/e;->a:Z

    .line 36
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/a/a/e;->h:Ljava/lang/String;

    .line 66
    return-object p0
.end method

.method public b(Z)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/sec/chaton/a/a/e;->e:Z

    .line 56
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/chaton/a/a/e;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/chaton/a/a/e;->g:Ljava/lang/String;

    .line 71
    return-object p0
.end method
