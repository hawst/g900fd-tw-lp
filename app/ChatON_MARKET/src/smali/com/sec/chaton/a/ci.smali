.class public final Lcom/sec/chaton/a/ci;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ck;


# static fields
.field private static final a:Lcom/sec/chaton/a/ci;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/sec/chaton/a/cu;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30191
    new-instance v0, Lcom/sec/chaton/a/ci;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/ci;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/ci;->a:Lcom/sec/chaton/a/ci;

    .line 30192
    sget-object v0, Lcom/sec/chaton/a/ci;->a:Lcom/sec/chaton/a/ci;

    invoke-direct {v0}, Lcom/sec/chaton/a/ci;->p()V

    .line 30193
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cj;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 29631
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 29757
    iput-byte v0, p0, Lcom/sec/chaton/a/ci;->g:B

    .line 29783
    iput v0, p0, Lcom/sec/chaton/a/ci;->h:I

    .line 29632
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cj;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 29626
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/ci;-><init>(Lcom/sec/chaton/a/cj;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 29633
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 29757
    iput-byte v0, p0, Lcom/sec/chaton/a/ci;->g:B

    .line 29783
    iput v0, p0, Lcom/sec/chaton/a/ci;->h:I

    .line 29633
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ci;I)I
    .locals 0

    .prologue
    .line 29626
    iput p1, p0, Lcom/sec/chaton/a/ci;->b:I

    return p1
.end method

.method public static a()Lcom/sec/chaton/a/ci;
    .locals 1

    .prologue
    .line 29637
    sget-object v0, Lcom/sec/chaton/a/ci;->a:Lcom/sec/chaton/a/ci;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/ci;
    .locals 1

    .prologue
    .line 29830
    invoke-static {}, Lcom/sec/chaton/a/ci;->newBuilder()Lcom/sec/chaton/a/cj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cj;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cj;

    invoke-static {v0}, Lcom/sec/chaton/a/cj;->a(Lcom/sec/chaton/a/cj;)Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/ci;)Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29886
    invoke-static {}, Lcom/sec/chaton/a/ci;->newBuilder()Lcom/sec/chaton/a/cj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cj;->a(Lcom/sec/chaton/a/ci;)Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/ci;Lcom/sec/chaton/a/cu;)Lcom/sec/chaton/a/cu;
    .locals 0

    .prologue
    .line 29626
    iput-object p1, p0, Lcom/sec/chaton/a/ci;->f:Lcom/sec/chaton/a/cu;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/ci;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 29626
    iput-object p1, p0, Lcom/sec/chaton/a/ci;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/ci;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 29626
    iput-object p1, p0, Lcom/sec/chaton/a/ci;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/ci;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 29626
    iput-object p1, p0, Lcom/sec/chaton/a/ci;->e:Ljava/lang/Object;

    return-object p1
.end method

.method private m()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 29666
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->c:Ljava/lang/Object;

    .line 29667
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29668
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 29670
    iput-object v0, p0, Lcom/sec/chaton/a/ci;->c:Ljava/lang/Object;

    .line 29673
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private n()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 29698
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->d:Ljava/lang/Object;

    .line 29699
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29700
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 29702
    iput-object v0, p0, Lcom/sec/chaton/a/ci;->d:Ljava/lang/Object;

    .line 29705
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29883
    invoke-static {}, Lcom/sec/chaton/a/cj;->h()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 29730
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->e:Ljava/lang/Object;

    .line 29731
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29732
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 29734
    iput-object v0, p0, Lcom/sec/chaton/a/ci;->e:Ljava/lang/Object;

    .line 29737
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 29752
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ci;->c:Ljava/lang/Object;

    .line 29753
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ci;->d:Ljava/lang/Object;

    .line 29754
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/ci;->e:Ljava/lang/Object;

    .line 29755
    invoke-static {}, Lcom/sec/chaton/a/cu;->a()Lcom/sec/chaton/a/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ci;->f:Lcom/sec/chaton/a/cu;

    .line 29756
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/ci;
    .locals 1

    .prologue
    .line 29641
    sget-object v0, Lcom/sec/chaton/a/ci;->a:Lcom/sec/chaton/a/ci;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 29649
    iget v1, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29652
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->c:Ljava/lang/Object;

    .line 29653
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29654
    check-cast v0, Ljava/lang/String;

    .line 29662
    :goto_0
    return-object v0

    .line 29656
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 29658
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 29659
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29660
    iput-object v1, p0, Lcom/sec/chaton/a/ci;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 29662
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 29681
    iget v0, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29684
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->d:Ljava/lang/Object;

    .line 29685
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29686
    check-cast v0, Ljava/lang/String;

    .line 29694
    :goto_0
    return-object v0

    .line 29688
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 29690
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 29691
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29692
    iput-object v1, p0, Lcom/sec/chaton/a/ci;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 29694
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 29713
    iget v0, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29626
    invoke-virtual {p0}, Lcom/sec/chaton/a/ci;->b()Lcom/sec/chaton/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 29785
    iget v0, p0, Lcom/sec/chaton/a/ci;->h:I

    .line 29786
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 29806
    :goto_0
    return v0

    .line 29788
    :cond_0
    const/4 v0, 0x0

    .line 29789
    iget v1, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 29790
    invoke-direct {p0}, Lcom/sec/chaton/a/ci;->m()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29793
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 29794
    invoke-direct {p0}, Lcom/sec/chaton/a/ci;->n()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29797
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 29798
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ci;->o()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29801
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 29802
    iget-object v1, p0, Lcom/sec/chaton/a/ci;->f:Lcom/sec/chaton/a/cu;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29805
    :cond_4
    iput v0, p0, Lcom/sec/chaton/a/ci;->h:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29716
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->e:Ljava/lang/Object;

    .line 29717
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29718
    check-cast v0, Ljava/lang/String;

    .line 29726
    :goto_0
    return-object v0

    .line 29720
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 29722
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 29723
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29724
    iput-object v1, p0, Lcom/sec/chaton/a/ci;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 29726
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 29745
    iget v0, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 29759
    iget-byte v1, p0, Lcom/sec/chaton/a/ci;->g:B

    .line 29760
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 29763
    :goto_0
    return v0

    .line 29760
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 29762
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/ci;->g:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/cu;
    .locals 1

    .prologue
    .line 29748
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->f:Lcom/sec/chaton/a/cu;

    return-object v0
.end method

.method public k()Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29884
    invoke-static {}, Lcom/sec/chaton/a/ci;->newBuilder()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/sec/chaton/a/cj;
    .locals 1

    .prologue
    .line 29888
    invoke-static {p0}, Lcom/sec/chaton/a/ci;->a(Lcom/sec/chaton/a/ci;)Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29626
    invoke-virtual {p0}, Lcom/sec/chaton/a/ci;->k()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29626
    invoke-virtual {p0}, Lcom/sec/chaton/a/ci;->l()Lcom/sec/chaton/a/cj;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29813
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 29768
    invoke-virtual {p0}, Lcom/sec/chaton/a/ci;->getSerializedSize()I

    .line 29769
    iget v0, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 29770
    invoke-direct {p0}, Lcom/sec/chaton/a/ci;->m()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 29772
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 29773
    invoke-direct {p0}, Lcom/sec/chaton/a/ci;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 29775
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 29776
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/ci;->o()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 29778
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/ci;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 29779
    iget-object v0, p0, Lcom/sec/chaton/a/ci;->f:Lcom/sec/chaton/a/cu;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 29781
    :cond_3
    return-void
.end method
