.class public final Lcom/sec/chaton/a/c;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/e;


# static fields
.field private static final a:Lcom/sec/chaton/a/c;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/sec/chaton/a/bc;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4800
    new-instance v0, Lcom/sec/chaton/a/c;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/c;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/c;->a:Lcom/sec/chaton/a/c;

    .line 4801
    sget-object v0, Lcom/sec/chaton/a/c;->a:Lcom/sec/chaton/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/a/c;->j()V

    .line 4802
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/d;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4439
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 4499
    iput-byte v0, p0, Lcom/sec/chaton/a/c;->e:B

    .line 4519
    iput v0, p0, Lcom/sec/chaton/a/c;->f:I

    .line 4440
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/d;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 4434
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/c;-><init>(Lcom/sec/chaton/a/d;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4441
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4499
    iput-byte v0, p0, Lcom/sec/chaton/a/c;->e:B

    .line 4519
    iput v0, p0, Lcom/sec/chaton/a/c;->f:I

    .line 4441
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/c;I)I
    .locals 0

    .prologue
    .line 4434
    iput p1, p0, Lcom/sec/chaton/a/c;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/c;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 4434
    iput-object p1, p0, Lcom/sec/chaton/a/c;->d:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/c;
    .locals 1

    .prologue
    .line 4445
    sget-object v0, Lcom/sec/chaton/a/c;->a:Lcom/sec/chaton/a/c;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4614
    invoke-static {}, Lcom/sec/chaton/a/c;->newBuilder()Lcom/sec/chaton/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/d;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 4434
    iput-object p1, p0, Lcom/sec/chaton/a/c;->c:Ljava/lang/Object;

    return-object p1
.end method

.method private i()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 4474
    iget-object v0, p0, Lcom/sec/chaton/a/c;->c:Ljava/lang/Object;

    .line 4475
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4476
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 4478
    iput-object v0, p0, Lcom/sec/chaton/a/c;->c:Ljava/lang/Object;

    .line 4481
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 4496
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/c;->c:Ljava/lang/Object;

    .line 4497
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/c;->d:Lcom/sec/chaton/a/bc;

    .line 4498
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4611
    invoke-static {}, Lcom/sec/chaton/a/d;->f()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/c;
    .locals 1

    .prologue
    .line 4449
    sget-object v0, Lcom/sec/chaton/a/c;->a:Lcom/sec/chaton/a/c;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4457
    iget v1, p0, Lcom/sec/chaton/a/c;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4460
    iget-object v0, p0, Lcom/sec/chaton/a/c;->c:Ljava/lang/Object;

    .line 4461
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4462
    check-cast v0, Ljava/lang/String;

    .line 4470
    :goto_0
    return-object v0

    .line 4464
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 4466
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 4467
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4468
    iput-object v1, p0, Lcom/sec/chaton/a/c;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 4470
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 4489
    iget v0, p0, Lcom/sec/chaton/a/c;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 4492
    iget-object v0, p0, Lcom/sec/chaton/a/c;->d:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public g()Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4612
    invoke-static {}, Lcom/sec/chaton/a/c;->newBuilder()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4434
    invoke-virtual {p0}, Lcom/sec/chaton/a/c;->b()Lcom/sec/chaton/a/c;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4521
    iget v0, p0, Lcom/sec/chaton/a/c;->f:I

    .line 4522
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4534
    :goto_0
    return v0

    .line 4524
    :cond_0
    const/4 v0, 0x0

    .line 4525
    iget v1, p0, Lcom/sec/chaton/a/c;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 4526
    invoke-direct {p0}, Lcom/sec/chaton/a/c;->i()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4529
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/c;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 4530
    iget-object v1, p0, Lcom/sec/chaton/a/c;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v1}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4533
    :cond_2
    iput v0, p0, Lcom/sec/chaton/a/c;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/a/d;
    .locals 1

    .prologue
    .line 4616
    invoke-static {p0}, Lcom/sec/chaton/a/c;->a(Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 4501
    iget-byte v1, p0, Lcom/sec/chaton/a/c;->e:B

    .line 4502
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 4505
    :goto_0
    return v0

    .line 4502
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4504
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/c;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4434
    invoke-virtual {p0}, Lcom/sec/chaton/a/c;->g()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4434
    invoke-virtual {p0}, Lcom/sec/chaton/a/c;->h()Lcom/sec/chaton/a/d;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4541
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4510
    invoke-virtual {p0}, Lcom/sec/chaton/a/c;->getSerializedSize()I

    .line 4511
    iget v0, p0, Lcom/sec/chaton/a/c;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4512
    invoke-direct {p0}, Lcom/sec/chaton/a/c;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 4514
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/c;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4515
    iget-object v0, p0, Lcom/sec/chaton/a/c;->d:Lcom/sec/chaton/a/bc;

    invoke-virtual {v0}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 4517
    :cond_1
    return-void
.end method
