.class public final Lcom/sec/chaton/a/bm;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/bn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/bl;",
        "Lcom/sec/chaton/a/bm;",
        ">;",
        "Lcom/sec/chaton/a/bn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:J

.field private e:Lcom/sec/chaton/a/dp;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5073
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 5215
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->b:Ljava/lang/Object;

    .line 5251
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->c:Ljava/lang/Object;

    .line 5308
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->e:Lcom/sec/chaton/a/dp;

    .line 5074
    invoke-direct {p0}, Lcom/sec/chaton/a/bm;->g()V

    .line 5075
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5068
    invoke-static {}, Lcom/sec/chaton/a/bm;->h()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 5078
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5080
    new-instance v0, Lcom/sec/chaton/a/bm;

    invoke-direct {v0}, Lcom/sec/chaton/a/bm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/bm;
    .locals 2

    .prologue
    .line 5084
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 5085
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->b:Ljava/lang/Object;

    .line 5086
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5087
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->c:Ljava/lang/Object;

    .line 5088
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5089
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/bm;->d:J

    .line 5090
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5091
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->e:Lcom/sec/chaton/a/dp;

    .line 5092
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5093
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5295
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5296
    iput-wide p1, p0, Lcom/sec/chaton/a/bm;->d:J

    .line 5298
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bm;
    .locals 2

    .prologue
    .line 5172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 5173
    sparse-switch v0, :sswitch_data_0

    .line 5178
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/bm;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5180
    :sswitch_0
    return-object p0

    .line 5185
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5186
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->b:Ljava/lang/Object;

    goto :goto_0

    .line 5190
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5191
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/bm;->c:Ljava/lang/Object;

    goto :goto_0

    .line 5195
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5196
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/bm;->d:J

    goto :goto_0

    .line 5200
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 5201
    invoke-static {v0}, Lcom/sec/chaton/a/dp;->a(I)Lcom/sec/chaton/a/dp;

    move-result-object v0

    .line 5202
    if-eqz v0, :cond_0

    .line 5203
    iget v1, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5204
    iput-object v0, p0, Lcom/sec/chaton/a/bm;->e:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 5173
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bm;
    .locals 2

    .prologue
    .line 5147
    invoke-static {}, Lcom/sec/chaton/a/bl;->a()Lcom/sec/chaton/a/bl;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 5160
    :cond_0
    :goto_0
    return-object p0

    .line 5148
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5149
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bm;->a(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    .line 5151
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5152
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bm;->b(Ljava/lang/String;)Lcom/sec/chaton/a/bm;

    .line 5154
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5155
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/bm;->a(J)Lcom/sec/chaton/a/bm;

    .line 5157
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5158
    invoke-virtual {p1}, Lcom/sec/chaton/a/bl;->j()Lcom/sec/chaton/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/bm;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5316
    if-nez p1, :cond_0

    .line 5317
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5319
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5320
    iput-object p1, p0, Lcom/sec/chaton/a/bm;->e:Lcom/sec/chaton/a/dp;

    .line 5322
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5230
    if-nez p1, :cond_0

    .line 5231
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5233
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5234
    iput-object p1, p0, Lcom/sec/chaton/a/bm;->b:Ljava/lang/Object;

    .line 5236
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/bm;
    .locals 2

    .prologue
    .line 5097
    invoke-static {}, Lcom/sec/chaton/a/bm;->h()Lcom/sec/chaton/a/bm;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->e()Lcom/sec/chaton/a/bl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/bm;
    .locals 1

    .prologue
    .line 5266
    if-nez p1, :cond_0

    .line 5267
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5269
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/bm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5270
    iput-object p1, p0, Lcom/sec/chaton/a/bm;->c:Ljava/lang/Object;

    .line 5272
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->d()Lcom/sec/chaton/a/bl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->e()Lcom/sec/chaton/a/bl;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/bl;
    .locals 1

    .prologue
    .line 5101
    invoke-static {}, Lcom/sec/chaton/a/bl;->a()Lcom/sec/chaton/a/bl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->a()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->a()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->b()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->b()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->b()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->b()Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/bl;
    .locals 2

    .prologue
    .line 5105
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->e()Lcom/sec/chaton/a/bl;

    move-result-object v0

    .line 5106
    invoke-virtual {v0}, Lcom/sec/chaton/a/bl;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5107
    invoke-static {v0}, Lcom/sec/chaton/a/bm;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5109
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/bl;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 5123
    new-instance v2, Lcom/sec/chaton/a/bl;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/bl;-><init>(Lcom/sec/chaton/a/bm;Lcom/sec/chaton/a/b;)V

    .line 5124
    iget v3, p0, Lcom/sec/chaton/a/bm;->a:I

    .line 5125
    const/4 v1, 0x0

    .line 5126
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 5129
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/bm;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bl;->a(Lcom/sec/chaton/a/bl;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5130
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 5131
    or-int/lit8 v0, v0, 0x2

    .line 5133
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/bm;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bl;->b(Lcom/sec/chaton/a/bl;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5134
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 5135
    or-int/lit8 v0, v0, 0x4

    .line 5137
    :cond_1
    iget-wide v4, p0, Lcom/sec/chaton/a/bm;->d:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/bl;->a(Lcom/sec/chaton/a/bl;J)J

    .line 5138
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 5139
    or-int/lit8 v0, v0, 0x8

    .line 5141
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/a/bm;->e:Lcom/sec/chaton/a/dp;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/bl;->a(Lcom/sec/chaton/a/bl;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;

    .line 5142
    invoke-static {v2, v0}, Lcom/sec/chaton/a/bl;->a(Lcom/sec/chaton/a/bl;I)I

    .line 5143
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->c()Lcom/sec/chaton/a/bl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0}, Lcom/sec/chaton/a/bm;->c()Lcom/sec/chaton/a/bl;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 5164
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bm;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    check-cast p1, Lcom/sec/chaton/a/bl;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/bm;->a(Lcom/sec/chaton/a/bl;)Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5068
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/bm;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/bm;

    move-result-object v0

    return-object v0
.end method
