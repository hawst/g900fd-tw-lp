.class public final Lcom/sec/chaton/a/fb;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/fd;


# static fields
.field private static final a:Lcom/sec/chaton/a/fb;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/google/protobuf/LazyStringList;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dm;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1367
    new-instance v0, Lcom/sec/chaton/a/fb;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/fb;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/fb;->a:Lcom/sec/chaton/a/fb;

    .line 1368
    sget-object v0, Lcom/sec/chaton/a/fb;->a:Lcom/sec/chaton/a/fb;

    invoke-direct {v0}, Lcom/sec/chaton/a/fb;->m()V

    .line 1369
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/fc;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 766
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 863
    iput-byte v0, p0, Lcom/sec/chaton/a/fb;->g:B

    .line 889
    iput v0, p0, Lcom/sec/chaton/a/fb;->h:I

    .line 767
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/fc;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 761
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/fb;-><init>(Lcom/sec/chaton/a/fc;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 768
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 863
    iput-byte v0, p0, Lcom/sec/chaton/a/fb;->g:B

    .line 889
    iput v0, p0, Lcom/sec/chaton/a/fb;->h:I

    .line 768
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/fb;I)I
    .locals 0

    .prologue
    .line 761
    iput p1, p0, Lcom/sec/chaton/a/fb;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/fb;J)J
    .locals 0

    .prologue
    .line 761
    iput-wide p1, p0, Lcom/sec/chaton/a/fb;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/fb;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 761
    iput-object p1, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/fb;
    .locals 1

    .prologue
    .line 772
    sget-object v0, Lcom/sec/chaton/a/fb;->a:Lcom/sec/chaton/a/fb;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 997
    invoke-static {}, Lcom/sec/chaton/a/fb;->newBuilder()Lcom/sec/chaton/a/fc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/fc;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/fb;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 761
    iput-object p1, p0, Lcom/sec/chaton/a/fb;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/fb;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 761
    iput-object p1, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/fb;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/a/fb;)Ljava/util/List;
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    return-object v0
.end method

.method private l()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 846
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->f:Ljava/lang/Object;

    .line 847
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 848
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 850
    iput-object v0, p0, Lcom/sec/chaton/a/fb;->f:Ljava/lang/Object;

    .line 853
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 858
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/fb;->c:J

    .line 859
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    .line 860
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    .line 861
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/fb;->f:Ljava/lang/Object;

    .line 862
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 994
    invoke-static {}, Lcom/sec/chaton/a/fc;->f()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/fb;
    .locals 1

    .prologue
    .line 776
    sget-object v0, Lcom/sec/chaton/a/fb;->a:Lcom/sec/chaton/a/fb;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 784
    iget v1, p0, Lcom/sec/chaton/a/fb;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 787
    iget-wide v0, p0, Lcom/sec/chaton/a/fb;->c:J

    return-wide v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/dm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 808
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/sec/chaton/a/fb;->b()Lcom/sec/chaton/a/fb;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 891
    iget v2, p0, Lcom/sec/chaton/a/fb;->h:I

    .line 892
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 917
    :goto_0
    return v2

    .line 895
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/fb;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 896
    iget-wide v2, p0, Lcom/sec/chaton/a/fb;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v1

    move v3, v1

    .line 901
    :goto_2
    iget-object v4, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 902
    iget-object v4, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v2}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v3, v4

    .line 901
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 905
    :cond_1
    add-int/2addr v0, v3

    .line 906
    invoke-virtual {p0}, Lcom/sec/chaton/a/fb;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 908
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 909
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 908
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_3

    .line 912
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/fb;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 913
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/sec/chaton/a/fb;->l()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 916
    :cond_3
    iput v2, p0, Lcom/sec/chaton/a/fb;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 829
    iget v0, p0, Lcom/sec/chaton/a/fb;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 832
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->f:Ljava/lang/Object;

    .line 833
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 834
    check-cast v0, Ljava/lang/String;

    .line 842
    :goto_0
    return-object v0

    .line 836
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 838
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 839
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 840
    iput-object v1, p0, Lcom/sec/chaton/a/fb;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 842
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 865
    iget-byte v1, p0, Lcom/sec/chaton/a/fb;->g:B

    .line 866
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 869
    :goto_0
    return v0

    .line 866
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 868
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/fb;->g:B

    goto :goto_0
.end method

.method public j()Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 995
    invoke-static {}, Lcom/sec/chaton/a/fb;->newBuilder()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/sec/chaton/a/fc;
    .locals 1

    .prologue
    .line 999
    invoke-static {p0}, Lcom/sec/chaton/a/fb;->a(Lcom/sec/chaton/a/fb;)Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/sec/chaton/a/fb;->j()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/sec/chaton/a/fb;->k()Lcom/sec/chaton/a/fc;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 924
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 874
    invoke-virtual {p0}, Lcom/sec/chaton/a/fb;->getSerializedSize()I

    .line 875
    iget v0, p0, Lcom/sec/chaton/a/fb;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 876
    iget-wide v2, p0, Lcom/sec/chaton/a/fb;->c:J

    invoke-virtual {p1, v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    move v0, v1

    .line 878
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 879
    iget-object v2, p0, Lcom/sec/chaton/a/fb;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v5, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 878
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 881
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 882
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sec/chaton/a/fb;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 881
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 884
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/fb;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_3

    .line 885
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/sec/chaton/a/fb;->l()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 887
    :cond_3
    return-void
.end method
