.class public final Lcom/sec/chaton/a/cr;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ct;


# static fields
.field private static final a:Lcom/sec/chaton/a/cr;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/protobuf/LazyStringList;

.field private e:J

.field private f:Ljava/lang/Object;

.field private g:J

.field private h:Lcom/sec/chaton/a/dp;

.field private i:Lcom/sec/chaton/a/bc;

.field private j:Ljava/lang/Object;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3666
    new-instance v0, Lcom/sec/chaton/a/cr;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/cr;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/cr;->a:Lcom/sec/chaton/a/cr;

    .line 3667
    sget-object v0, Lcom/sec/chaton/a/cr;->a:Lcom/sec/chaton/a/cr;

    invoke-direct {v0}, Lcom/sec/chaton/a/cr;->x()V

    .line 3668
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/cs;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2853
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3027
    iput-byte v0, p0, Lcom/sec/chaton/a/cr;->k:B

    .line 3065
    iput v0, p0, Lcom/sec/chaton/a/cr;->l:I

    .line 2854
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/cs;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 2848
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/cr;-><init>(Lcom/sec/chaton/a/cs;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2855
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3027
    iput-byte v0, p0, Lcom/sec/chaton/a/cr;->k:B

    .line 3065
    iput v0, p0, Lcom/sec/chaton/a/cr;->l:I

    .line 2855
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/cr;I)I
    .locals 0

    .prologue
    .line 2848
    iput p1, p0, Lcom/sec/chaton/a/cr;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cr;J)J
    .locals 0

    .prologue
    .line 2848
    iput-wide p1, p0, Lcom/sec/chaton/a/cr;->e:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cr;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 2848
    iput-object p1, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cr;Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/bc;
    .locals 0

    .prologue
    .line 2848
    iput-object p1, p0, Lcom/sec/chaton/a/cr;->i:Lcom/sec/chaton/a/bc;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/cr;
    .locals 1

    .prologue
    .line 2859
    sget-object v0, Lcom/sec/chaton/a/cr;->a:Lcom/sec/chaton/a/cr;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3189
    invoke-static {}, Lcom/sec/chaton/a/cr;->newBuilder()Lcom/sec/chaton/a/cs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/cs;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/cr;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;
    .locals 0

    .prologue
    .line 2848
    iput-object p1, p0, Lcom/sec/chaton/a/cr;->h:Lcom/sec/chaton/a/dp;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/cr;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2848
    iput-object p1, p0, Lcom/sec/chaton/a/cr;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cr;J)J
    .locals 0

    .prologue
    .line 2848
    iput-wide p1, p0, Lcom/sec/chaton/a/cr;->g:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/cr;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 2848
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/a/cr;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2848
    iput-object p1, p0, Lcom/sec/chaton/a/cr;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/a/cr;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2848
    iput-object p1, p0, Lcom/sec/chaton/a/cr;->j:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3186
    invoke-static {}, Lcom/sec/chaton/a/cs;->f()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method private u()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2888
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->c:Ljava/lang/Object;

    .line 2889
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2890
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2892
    iput-object v0, p0, Lcom/sec/chaton/a/cr;->c:Ljava/lang/Object;

    .line 2895
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private v()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2944
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->f:Ljava/lang/Object;

    .line 2945
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2946
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2948
    iput-object v0, p0, Lcom/sec/chaton/a/cr;->f:Ljava/lang/Object;

    .line 2951
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private w()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3006
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->j:Ljava/lang/Object;

    .line 3007
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3008
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3010
    iput-object v0, p0, Lcom/sec/chaton/a/cr;->j:Ljava/lang/Object;

    .line 3013
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private x()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 3018
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cr;->c:Ljava/lang/Object;

    .line 3019
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    .line 3020
    iput-wide v1, p0, Lcom/sec/chaton/a/cr;->e:J

    .line 3021
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cr;->f:Ljava/lang/Object;

    .line 3022
    iput-wide v1, p0, Lcom/sec/chaton/a/cr;->g:J

    .line 3023
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/cr;->h:Lcom/sec/chaton/a/dp;

    .line 3024
    sget-object v0, Lcom/sec/chaton/a/bc;->a:Lcom/sec/chaton/a/bc;

    iput-object v0, p0, Lcom/sec/chaton/a/cr;->i:Lcom/sec/chaton/a/bc;

    .line 3025
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/cr;->j:Ljava/lang/Object;

    .line 3026
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2910
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/cr;
    .locals 1

    .prologue
    .line 2863
    sget-object v0, Lcom/sec/chaton/a/cr;->a:Lcom/sec/chaton/a/cr;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2871
    iget v1, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2874
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->c:Ljava/lang/Object;

    .line 2875
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2876
    check-cast v0, Ljava/lang/String;

    .line 2884
    :goto_0
    return-object v0

    .line 2878
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2880
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2881
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2882
    iput-object v1, p0, Lcom/sec/chaton/a/cr;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2884
    goto :goto_0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2904
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 2907
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 2917
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2848
    invoke-virtual {p0}, Lcom/sec/chaton/a/cr;->b()Lcom/sec/chaton/a/cr;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3067
    iget v0, p0, Lcom/sec/chaton/a/cr;->l:I

    .line 3068
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3109
    :goto_0
    return v0

    .line 3071
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 3072
    invoke-direct {p0}, Lcom/sec/chaton/a/cr;->u()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v1

    .line 3077
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 3078
    iget-object v3, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3077
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3081
    :cond_1
    add-int/2addr v0, v2

    .line 3082
    invoke-virtual {p0}, Lcom/sec/chaton/a/cr;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3084
    iget v1, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 3085
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sec/chaton/a/cr;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3088
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 3089
    invoke-direct {p0}, Lcom/sec/chaton/a/cr;->v()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3092
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 3093
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/sec/chaton/a/cr;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3096
    :cond_4
    iget v1, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 3097
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/chaton/a/cr;->h:Lcom/sec/chaton/a/dp;

    invoke-virtual {v2}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3100
    :cond_5
    iget v1, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 3101
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/chaton/a/cr;->i:Lcom/sec/chaton/a/bc;

    invoke-virtual {v2}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3104
    :cond_6
    iget v1, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 3105
    invoke-direct {p0}, Lcom/sec/chaton/a/cr;->w()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3108
    :cond_7
    iput v0, p0, Lcom/sec/chaton/a/cr;->l:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public h()J
    .locals 2

    .prologue
    .line 2920
    iget-wide v0, p0, Lcom/sec/chaton/a/cr;->e:J

    return-wide v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 2927
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3029
    iget-byte v1, p0, Lcom/sec/chaton/a/cr;->k:B

    .line 3030
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 3033
    :goto_0
    return v0

    .line 3030
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3032
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/cr;->k:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2930
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->f:Ljava/lang/Object;

    .line 2931
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2932
    check-cast v0, Ljava/lang/String;

    .line 2940
    :goto_0
    return-object v0

    .line 2934
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2936
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2937
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2938
    iput-object v1, p0, Lcom/sec/chaton/a/cr;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2940
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 2959
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 2962
    iget-wide v0, p0, Lcom/sec/chaton/a/cr;->g:J

    return-wide v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 2969
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 2972
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->h:Lcom/sec/chaton/a/dp;

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2848
    invoke-virtual {p0}, Lcom/sec/chaton/a/cr;->s()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 2979
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Lcom/sec/chaton/a/bc;
    .locals 1

    .prologue
    .line 2982
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->i:Lcom/sec/chaton/a/bc;

    return-object v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 2989
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2992
    iget-object v0, p0, Lcom/sec/chaton/a/cr;->j:Ljava/lang/Object;

    .line 2993
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2994
    check-cast v0, Ljava/lang/String;

    .line 3002
    :goto_0
    return-object v0

    .line 2996
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2998
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2999
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3000
    iput-object v1, p0, Lcom/sec/chaton/a/cr;->j:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3002
    goto :goto_0
.end method

.method public s()Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3187
    invoke-static {}, Lcom/sec/chaton/a/cr;->newBuilder()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public t()Lcom/sec/chaton/a/cs;
    .locals 1

    .prologue
    .line 3191
    invoke-static {p0}, Lcom/sec/chaton/a/cr;->a(Lcom/sec/chaton/a/cr;)Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2848
    invoke-virtual {p0}, Lcom/sec/chaton/a/cr;->t()Lcom/sec/chaton/a/cs;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3116
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3038
    invoke-virtual {p0}, Lcom/sec/chaton/a/cr;->getSerializedSize()I

    .line 3039
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3040
    invoke-direct {p0}, Lcom/sec/chaton/a/cr;->u()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3042
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 3043
    iget-object v1, p0, Lcom/sec/chaton/a/cr;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3042
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3045
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 3046
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sec/chaton/a/cr;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 3048
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 3049
    invoke-direct {p0}, Lcom/sec/chaton/a/cr;->v()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3051
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 3052
    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sec/chaton/a/cr;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 3054
    :cond_4
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 3055
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/sec/chaton/a/cr;->h:Lcom/sec/chaton/a/dp;

    invoke-virtual {v1}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 3057
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 3058
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sec/chaton/a/cr;->i:Lcom/sec/chaton/a/bc;

    invoke-virtual {v1}, Lcom/sec/chaton/a/bc;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 3060
    :cond_6
    iget v0, p0, Lcom/sec/chaton/a/cr;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 3061
    invoke-direct {p0}, Lcom/sec/chaton/a/cr;->w()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3063
    :cond_7
    return-void
.end method
