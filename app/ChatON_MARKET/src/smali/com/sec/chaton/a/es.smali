.class public final Lcom/sec/chaton/a/es;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/eu;


# static fields
.field private static final a:Lcom/sec/chaton/a/es;


# instance fields
.field private b:I

.field private c:J

.field private d:Lcom/sec/chaton/a/dp;

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8051
    new-instance v0, Lcom/sec/chaton/a/es;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/es;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/es;->a:Lcom/sec/chaton/a/es;

    .line 8052
    sget-object v0, Lcom/sec/chaton/a/es;->a:Lcom/sec/chaton/a/es;

    invoke-direct {v0}, Lcom/sec/chaton/a/es;->n()V

    .line 8053
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/et;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 7584
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 7666
    iput-byte v0, p0, Lcom/sec/chaton/a/es;->g:B

    .line 7692
    iput v0, p0, Lcom/sec/chaton/a/es;->h:I

    .line 7585
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/et;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 7579
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/es;-><init>(Lcom/sec/chaton/a/et;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 7586
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 7666
    iput-byte v0, p0, Lcom/sec/chaton/a/es;->g:B

    .line 7692
    iput v0, p0, Lcom/sec/chaton/a/es;->h:I

    .line 7586
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/es;I)I
    .locals 0

    .prologue
    .line 7579
    iput p1, p0, Lcom/sec/chaton/a/es;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/es;J)J
    .locals 0

    .prologue
    .line 7579
    iput-wide p1, p0, Lcom/sec/chaton/a/es;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/es;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;
    .locals 0

    .prologue
    .line 7579
    iput-object p1, p0, Lcom/sec/chaton/a/es;->d:Lcom/sec/chaton/a/dp;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/es;
    .locals 1

    .prologue
    .line 7590
    sget-object v0, Lcom/sec/chaton/a/es;->a:Lcom/sec/chaton/a/es;

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/es;)Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7795
    invoke-static {}, Lcom/sec/chaton/a/es;->newBuilder()Lcom/sec/chaton/a/et;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/et;->a(Lcom/sec/chaton/a/es;)Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/es;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 7579
    iput-object p1, p0, Lcom/sec/chaton/a/es;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/es;Z)Z
    .locals 0

    .prologue
    .line 7579
    iput-boolean p1, p0, Lcom/sec/chaton/a/es;->f:Z

    return p1
.end method

.method private m()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 7639
    iget-object v0, p0, Lcom/sec/chaton/a/es;->e:Ljava/lang/Object;

    .line 7640
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7641
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 7643
    iput-object v0, p0, Lcom/sec/chaton/a/es;->e:Ljava/lang/Object;

    .line 7646
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 7661
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/es;->c:J

    .line 7662
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/es;->d:Lcom/sec/chaton/a/dp;

    .line 7663
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/es;->e:Ljava/lang/Object;

    .line 7664
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/es;->f:Z

    .line 7665
    return-void
.end method

.method public static newBuilder()Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7792
    invoke-static {}, Lcom/sec/chaton/a/et;->f()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/a/es;
    .locals 1

    .prologue
    .line 7594
    sget-object v0, Lcom/sec/chaton/a/es;->a:Lcom/sec/chaton/a/es;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 7602
    iget v1, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 7605
    iget-wide v0, p0, Lcom/sec/chaton/a/es;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 7612
    iget v0, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/a/dp;
    .locals 1

    .prologue
    .line 7615
    iget-object v0, p0, Lcom/sec/chaton/a/es;->d:Lcom/sec/chaton/a/dp;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 7622
    iget v0, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7579
    invoke-virtual {p0}, Lcom/sec/chaton/a/es;->b()Lcom/sec/chaton/a/es;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 7694
    iget v0, p0, Lcom/sec/chaton/a/es;->h:I

    .line 7695
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7715
    :goto_0
    return v0

    .line 7697
    :cond_0
    const/4 v0, 0x0

    .line 7698
    iget v1, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 7699
    iget-wide v1, p0, Lcom/sec/chaton/a/es;->c:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7702
    :cond_1
    iget v1, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 7703
    iget-object v1, p0, Lcom/sec/chaton/a/es;->d:Lcom/sec/chaton/a/dp;

    invoke-virtual {v1}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7706
    :cond_2
    iget v1, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    .line 7707
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/es;->m()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7710
    :cond_3
    iget v1, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 7711
    iget-boolean v1, p0, Lcom/sec/chaton/a/es;->f:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7714
    :cond_4
    iput v0, p0, Lcom/sec/chaton/a/es;->h:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 7625
    iget-object v0, p0, Lcom/sec/chaton/a/es;->e:Ljava/lang/Object;

    .line 7626
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7627
    check-cast v0, Ljava/lang/String;

    .line 7635
    :goto_0
    return-object v0

    .line 7629
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 7631
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 7632
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7633
    iput-object v1, p0, Lcom/sec/chaton/a/es;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 7635
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 7654
    iget v0, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 7668
    iget-byte v1, p0, Lcom/sec/chaton/a/es;->g:B

    .line 7669
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 7672
    :goto_0
    return v0

    .line 7669
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 7671
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/es;->g:B

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 7657
    iget-boolean v0, p0, Lcom/sec/chaton/a/es;->f:Z

    return v0
.end method

.method public k()Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7793
    invoke-static {}, Lcom/sec/chaton/a/es;->newBuilder()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7797
    invoke-static {p0}, Lcom/sec/chaton/a/es;->a(Lcom/sec/chaton/a/es;)Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7579
    invoke-virtual {p0}, Lcom/sec/chaton/a/es;->k()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7579
    invoke-virtual {p0}, Lcom/sec/chaton/a/es;->l()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7722
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 7677
    invoke-virtual {p0}, Lcom/sec/chaton/a/es;->getSerializedSize()I

    .line 7678
    iget v0, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 7679
    iget-wide v0, p0, Lcom/sec/chaton/a/es;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 7681
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 7682
    iget-object v0, p0, Lcom/sec/chaton/a/es;->d:Lcom/sec/chaton/a/dp;

    invoke-virtual {v0}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 7684
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 7685
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/chaton/a/es;->m()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 7687
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/es;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 7688
    iget-boolean v0, p0, Lcom/sec/chaton/a/es;->f:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7690
    :cond_3
    return-void
.end method
