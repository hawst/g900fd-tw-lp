.class public final Lcom/sec/chaton/a/p;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/o;",
        "Lcom/sec/chaton/a/p;",
        ">;",
        "Lcom/sec/chaton/a/q;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20104
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 20239
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    .line 20105
    invoke-direct {p0}, Lcom/sec/chaton/a/p;->j()V

    .line 20106
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/p;)Lcom/sec/chaton/a/o;
    .locals 1

    .prologue
    .line 20099
    invoke-direct {p0}, Lcom/sec/chaton/a/p;->l()Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i()Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20099
    invoke-static {}, Lcom/sec/chaton/a/p;->k()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method private j()V
    .locals 0

    .prologue
    .line 20109
    return-void
.end method

.method private static k()Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20111
    new-instance v0, Lcom/sec/chaton/a/p;

    invoke-direct {v0}, Lcom/sec/chaton/a/p;-><init>()V

    return-object v0
.end method

.method private l()Lcom/sec/chaton/a/o;
    .locals 2

    .prologue
    .line 20141
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->e()Lcom/sec/chaton/a/o;

    move-result-object v0

    .line 20142
    invoke-virtual {v0}, Lcom/sec/chaton/a/o;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20143
    invoke-static {v0}, Lcom/sec/chaton/a/p;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 20146
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/p;
    .locals 2

    .prologue
    .line 20115
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 20116
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/p;->b:J

    .line 20117
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20118
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    .line 20119
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20120
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20226
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20227
    iput-wide p1, p0, Lcom/sec/chaton/a/p;->b:J

    .line 20229
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/p;
    .locals 2

    .prologue
    .line 20185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 20186
    sparse-switch v0, :sswitch_data_0

    .line 20191
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/p;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20193
    :sswitch_0
    return-object p0

    .line 20198
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20199
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/p;->b:J

    goto :goto_0

    .line 20203
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 20204
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 20205
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->h()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 20207
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 20208
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/p;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/p;

    goto :goto_0

    .line 20186
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20247
    if-nez p1, :cond_0

    .line 20248
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20250
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    .line 20252
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20253
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/ek;)Lcom/sec/chaton/a/p;
    .locals 1

    .prologue
    .line 20257
    invoke-virtual {p1}, Lcom/sec/chaton/a/ek;->d()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    .line 20259
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20260
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/o;)Lcom/sec/chaton/a/p;
    .locals 2

    .prologue
    .line 20166
    invoke-static {}, Lcom/sec/chaton/a/o;->a()Lcom/sec/chaton/a/o;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 20173
    :cond_0
    :goto_0
    return-object p0

    .line 20167
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/o;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20168
    invoke-virtual {p1}, Lcom/sec/chaton/a/o;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/p;->a(J)Lcom/sec/chaton/a/p;

    .line 20170
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/o;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20171
    invoke-virtual {p1}, Lcom/sec/chaton/a/o;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/p;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/p;

    goto :goto_0
.end method

.method public b()Lcom/sec/chaton/a/p;
    .locals 2

    .prologue
    .line 20124
    invoke-static {}, Lcom/sec/chaton/a/p;->k()Lcom/sec/chaton/a/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->e()Lcom/sec/chaton/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/p;->a(Lcom/sec/chaton/a/o;)Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/p;
    .locals 2

    .prologue
    .line 20263
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 20265
    iget-object v0, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    .line 20271
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20272
    return-object p0

    .line 20268
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->d()Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->e()Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/o;
    .locals 1

    .prologue
    .line 20128
    invoke-static {}, Lcom/sec/chaton/a/o;->a()Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->a()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->a()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->b()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->b()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->b()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->b()Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/o;
    .locals 2

    .prologue
    .line 20132
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->e()Lcom/sec/chaton/a/o;

    move-result-object v0

    .line 20133
    invoke-virtual {v0}, Lcom/sec/chaton/a/o;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20134
    invoke-static {v0}, Lcom/sec/chaton/a/p;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 20136
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/o;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 20150
    new-instance v2, Lcom/sec/chaton/a/o;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/o;-><init>(Lcom/sec/chaton/a/p;Lcom/sec/chaton/a/b;)V

    .line 20151
    iget v3, p0, Lcom/sec/chaton/a/p;->a:I

    .line 20152
    const/4 v1, 0x0

    .line 20153
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 20156
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/p;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/o;->a(Lcom/sec/chaton/a/o;J)J

    .line 20157
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 20158
    or-int/lit8 v0, v0, 0x2

    .line 20160
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/o;->a(Lcom/sec/chaton/a/o;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 20161
    invoke-static {v2, v0}, Lcom/sec/chaton/a/o;->a(Lcom/sec/chaton/a/o;I)I

    .line 20162
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 20223
    iget-wide v0, p0, Lcom/sec/chaton/a/p;->b:J

    return-wide v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 20241
    iget v0, p0, Lcom/sec/chaton/a/p;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->c()Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0}, Lcom/sec/chaton/a/p;->c()Lcom/sec/chaton/a/o;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 20244
    iget-object v0, p0, Lcom/sec/chaton/a/p;->c:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 20177
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/p;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    check-cast p1, Lcom/sec/chaton/a/o;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/p;->a(Lcom/sec/chaton/a/o;)Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20099
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/p;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/p;

    move-result-object v0

    return-object v0
.end method
