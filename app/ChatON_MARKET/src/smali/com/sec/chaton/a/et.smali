.class public final Lcom/sec/chaton/a/et;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/eu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/es;",
        "Lcom/sec/chaton/a/et;",
        ">;",
        "Lcom/sec/chaton/a/eu;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/dp;

.field private d:Ljava/lang/Object;

.field private e:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7804
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 7967
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/et;->c:Lcom/sec/chaton/a/dp;

    .line 7991
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/et;->d:Ljava/lang/Object;

    .line 7805
    invoke-direct {p0}, Lcom/sec/chaton/a/et;->g()V

    .line 7806
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7799
    invoke-static {}, Lcom/sec/chaton/a/et;->h()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 7809
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7811
    new-instance v0, Lcom/sec/chaton/a/et;

    invoke-direct {v0}, Lcom/sec/chaton/a/et;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/et;
    .locals 2

    .prologue
    .line 7815
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 7816
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/et;->b:J

    .line 7817
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7818
    sget-object v0, Lcom/sec/chaton/a/dp;->a:Lcom/sec/chaton/a/dp;

    iput-object v0, p0, Lcom/sec/chaton/a/et;->c:Lcom/sec/chaton/a/dp;

    .line 7819
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7820
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/et;->d:Ljava/lang/Object;

    .line 7821
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7822
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/a/et;->e:Z

    .line 7823
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7824
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7954
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7955
    iput-wide p1, p0, Lcom/sec/chaton/a/et;->b:J

    .line 7957
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/et;
    .locals 2

    .prologue
    .line 7903
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 7904
    sparse-switch v0, :sswitch_data_0

    .line 7909
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/et;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7911
    :sswitch_0
    return-object p0

    .line 7916
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7917
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/et;->b:J

    goto :goto_0

    .line 7921
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 7922
    invoke-static {v0}, Lcom/sec/chaton/a/dp;->a(I)Lcom/sec/chaton/a/dp;

    move-result-object v0

    .line 7923
    if-eqz v0, :cond_0

    .line 7924
    iget v1, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7925
    iput-object v0, p0, Lcom/sec/chaton/a/et;->c:Lcom/sec/chaton/a/dp;

    goto :goto_0

    .line 7930
    :sswitch_3
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7931
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/et;->d:Ljava/lang/Object;

    goto :goto_0

    .line 7935
    :sswitch_4
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7936
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/a/et;->e:Z

    goto :goto_0

    .line 7904
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 7975
    if-nez p1, :cond_0

    .line 7976
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7978
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7979
    iput-object p1, p0, Lcom/sec/chaton/a/et;->c:Lcom/sec/chaton/a/dp;

    .line 7981
    return-object p0
.end method

.method public a(Lcom/sec/chaton/a/es;)Lcom/sec/chaton/a/et;
    .locals 2

    .prologue
    .line 7878
    invoke-static {}, Lcom/sec/chaton/a/es;->a()Lcom/sec/chaton/a/es;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 7891
    :cond_0
    :goto_0
    return-object p0

    .line 7879
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7880
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/et;->a(J)Lcom/sec/chaton/a/et;

    .line 7882
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7883
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->f()Lcom/sec/chaton/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/et;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/et;

    .line 7885
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7886
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/et;->a(Ljava/lang/String;)Lcom/sec/chaton/a/et;

    .line 7888
    :cond_4
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7889
    invoke-virtual {p1}, Lcom/sec/chaton/a/es;->j()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/et;->a(Z)Lcom/sec/chaton/a/et;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 8006
    if-nez p1, :cond_0

    .line 8007
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8009
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 8010
    iput-object p1, p0, Lcom/sec/chaton/a/et;->d:Ljava/lang/Object;

    .line 8012
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/a/et;
    .locals 1

    .prologue
    .line 8035
    iget v0, p0, Lcom/sec/chaton/a/et;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/chaton/a/et;->a:I

    .line 8036
    iput-boolean p1, p0, Lcom/sec/chaton/a/et;->e:Z

    .line 8038
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/et;
    .locals 2

    .prologue
    .line 7828
    invoke-static {}, Lcom/sec/chaton/a/et;->h()Lcom/sec/chaton/a/et;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->e()Lcom/sec/chaton/a/es;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/et;->a(Lcom/sec/chaton/a/es;)Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->d()Lcom/sec/chaton/a/es;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->e()Lcom/sec/chaton/a/es;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/es;
    .locals 1

    .prologue
    .line 7832
    invoke-static {}, Lcom/sec/chaton/a/es;->a()Lcom/sec/chaton/a/es;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->a()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->a()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->b()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->b()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->b()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->b()Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/es;
    .locals 2

    .prologue
    .line 7836
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->e()Lcom/sec/chaton/a/es;

    move-result-object v0

    .line 7837
    invoke-virtual {v0}, Lcom/sec/chaton/a/es;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7838
    invoke-static {v0}, Lcom/sec/chaton/a/et;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 7840
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/es;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 7854
    new-instance v2, Lcom/sec/chaton/a/es;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/es;-><init>(Lcom/sec/chaton/a/et;Lcom/sec/chaton/a/b;)V

    .line 7855
    iget v3, p0, Lcom/sec/chaton/a/et;->a:I

    .line 7856
    const/4 v1, 0x0

    .line 7857
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 7860
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/et;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/es;->a(Lcom/sec/chaton/a/es;J)J

    .line 7861
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 7862
    or-int/lit8 v0, v0, 0x2

    .line 7864
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/et;->c:Lcom/sec/chaton/a/dp;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/es;->a(Lcom/sec/chaton/a/es;Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/dp;

    .line 7865
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 7866
    or-int/lit8 v0, v0, 0x4

    .line 7868
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/a/et;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/es;->a(Lcom/sec/chaton/a/es;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7869
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 7870
    or-int/lit8 v0, v0, 0x8

    .line 7872
    :cond_2
    iget-boolean v1, p0, Lcom/sec/chaton/a/et;->e:Z

    invoke-static {v2, v1}, Lcom/sec/chaton/a/es;->a(Lcom/sec/chaton/a/es;Z)Z

    .line 7873
    invoke-static {v2, v0}, Lcom/sec/chaton/a/es;->a(Lcom/sec/chaton/a/es;I)I

    .line 7874
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->c()Lcom/sec/chaton/a/es;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0}, Lcom/sec/chaton/a/et;->c()Lcom/sec/chaton/a/es;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 7895
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/et;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    check-cast p1, Lcom/sec/chaton/a/es;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/et;->a(Lcom/sec/chaton/a/es;)Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7799
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/et;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/et;

    move-result-object v0

    return-object v0
.end method
