.class public final Lcom/sec/chaton/a/eg;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ei;


# static fields
.field private static final a:Lcom/sec/chaton/a/eg;


# instance fields
.field private b:I

.field private c:J

.field private d:Ljava/lang/Object;

.field private e:J

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/sec/chaton/a/c;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32467
    new-instance v0, Lcom/sec/chaton/a/eg;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/chaton/a/eg;-><init>(Z)V

    sput-object v0, Lcom/sec/chaton/a/eg;->a:Lcom/sec/chaton/a/eg;

    .line 32468
    sget-object v0, Lcom/sec/chaton/a/eg;->a:Lcom/sec/chaton/a/eg;

    invoke-direct {v0}, Lcom/sec/chaton/a/eg;->v()V

    .line 32469
    return-void
.end method

.method private constructor <init>(Lcom/sec/chaton/a/eh;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 31660
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 31830
    iput-byte v0, p0, Lcom/sec/chaton/a/eg;->j:B

    .line 31865
    iput v0, p0, Lcom/sec/chaton/a/eg;->k:I

    .line 31661
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/a/eh;Lcom/sec/chaton/a/b;)V
    .locals 0

    .prologue
    .line 31655
    invoke-direct {p0, p1}, Lcom/sec/chaton/a/eg;-><init>(Lcom/sec/chaton/a/eh;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 31662
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31830
    iput-byte v0, p0, Lcom/sec/chaton/a/eg;->j:B

    .line 31865
    iput v0, p0, Lcom/sec/chaton/a/eg;->k:I

    .line 31662
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/eg;I)I
    .locals 0

    .prologue
    .line 31655
    iput p1, p0, Lcom/sec/chaton/a/eg;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/eg;J)J
    .locals 0

    .prologue
    .line 31655
    iput-wide p1, p0, Lcom/sec/chaton/a/eg;->c:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/eg;Lcom/sec/chaton/a/c;)Lcom/sec/chaton/a/c;
    .locals 0

    .prologue
    .line 31655
    iput-object p1, p0, Lcom/sec/chaton/a/eg;->i:Lcom/sec/chaton/a/c;

    return-object p1
.end method

.method public static a()Lcom/sec/chaton/a/eg;
    .locals 1

    .prologue
    .line 31666
    sget-object v0, Lcom/sec/chaton/a/eg;->a:Lcom/sec/chaton/a/eg;

    return-object v0
.end method

.method public static a([B)Lcom/sec/chaton/a/eg;
    .locals 1

    .prologue
    .line 31924
    invoke-static {}, Lcom/sec/chaton/a/eg;->newBuilder()Lcom/sec/chaton/a/eh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/eh;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/eh;

    invoke-static {v0}, Lcom/sec/chaton/a/eh;->a(Lcom/sec/chaton/a/eh;)Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/a/eg;)Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 31980
    invoke-static {}, Lcom/sec/chaton/a/eg;->newBuilder()Lcom/sec/chaton/a/eh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/a/eh;->a(Lcom/sec/chaton/a/eg;)Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/a/eg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 31655
    iput-object p1, p0, Lcom/sec/chaton/a/eg;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/a/eg;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 31655
    iput-object p1, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/eg;J)J
    .locals 0

    .prologue
    .line 31655
    iput-wide p1, p0, Lcom/sec/chaton/a/eg;->e:J

    return-wide p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/eg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 31655
    iput-object p1, p0, Lcom/sec/chaton/a/eg;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/a/eg;)Ljava/util/List;
    .locals 1

    .prologue
    .line 31655
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/a/eg;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 31655
    iput-object p1, p0, Lcom/sec/chaton/a/eg;->g:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 31977
    invoke-static {}, Lcom/sec/chaton/a/eh;->h()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31705
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->d:Ljava/lang/Object;

    .line 31706
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31707
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31709
    iput-object v0, p0, Lcom/sec/chaton/a/eg;->d:Ljava/lang/Object;

    .line 31712
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31747
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->f:Ljava/lang/Object;

    .line 31748
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31749
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31751
    iput-object v0, p0, Lcom/sec/chaton/a/eg;->f:Ljava/lang/Object;

    .line 31754
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private u()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31779
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->g:Ljava/lang/Object;

    .line 31780
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31781
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31783
    iput-object v0, p0, Lcom/sec/chaton/a/eg;->g:Ljava/lang/Object;

    .line 31786
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private v()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 31822
    iput-wide v1, p0, Lcom/sec/chaton/a/eg;->c:J

    .line 31823
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eg;->d:Ljava/lang/Object;

    .line 31824
    iput-wide v1, p0, Lcom/sec/chaton/a/eg;->e:J

    .line 31825
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eg;->f:Ljava/lang/Object;

    .line 31826
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/eg;->g:Ljava/lang/Object;

    .line 31827
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    .line 31828
    invoke-static {}, Lcom/sec/chaton/a/c;->a()Lcom/sec/chaton/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/eg;->i:Lcom/sec/chaton/a/c;

    .line 31829
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/a/cu;
    .locals 1

    .prologue
    .line 31804
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cu;

    return-object v0
.end method

.method public b()Lcom/sec/chaton/a/eg;
    .locals 1

    .prologue
    .line 31670
    sget-object v0, Lcom/sec/chaton/a/eg;->a:Lcom/sec/chaton/a/eg;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 31678
    iget v1, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 31681
    iget-wide v0, p0, Lcom/sec/chaton/a/eg;->c:J

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 31688
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31691
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->d:Ljava/lang/Object;

    .line 31692
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31693
    check-cast v0, Ljava/lang/String;

    .line 31701
    :goto_0
    return-object v0

    .line 31695
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31697
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31698
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31699
    iput-object v1, p0, Lcom/sec/chaton/a/eg;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31701
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 31720
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 31655
    invoke-virtual {p0}, Lcom/sec/chaton/a/eg;->b()Lcom/sec/chaton/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 31867
    iget v2, p0, Lcom/sec/chaton/a/eg;->k:I

    .line 31868
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 31900
    :goto_0
    return v2

    .line 31871
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 31872
    iget-wide v2, p0, Lcom/sec/chaton/a/eg;->c:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v1

    .line 31875
    :goto_1
    iget v2, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 31876
    invoke-direct {p0}, Lcom/sec/chaton/a/eg;->s()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 31879
    :cond_1
    iget v2, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 31880
    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/sec/chaton/a/eg;->e:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 31883
    :cond_2
    iget v2, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 31884
    invoke-direct {p0}, Lcom/sec/chaton/a/eg;->t()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 31887
    :cond_3
    iget v2, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 31888
    const/4 v2, 0x5

    invoke-direct {p0}, Lcom/sec/chaton/a/eg;->u()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    move v2, v0

    .line 31891
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 31892
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 31891
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 31895
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 31896
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sec/chaton/a/eg;->i:Lcom/sec/chaton/a/c;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 31899
    :cond_6
    iput v2, p0, Lcom/sec/chaton/a/eg;->k:I

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public h()J
    .locals 2

    .prologue
    .line 31723
    iget-wide v0, p0, Lcom/sec/chaton/a/eg;->e:J

    return-wide v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 31730
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 31832
    iget-byte v1, p0, Lcom/sec/chaton/a/eg;->j:B

    .line 31833
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 31836
    :goto_0
    return v0

    .line 31833
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 31835
    :cond_1
    iput-byte v0, p0, Lcom/sec/chaton/a/eg;->j:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31733
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->f:Ljava/lang/Object;

    .line 31734
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31735
    check-cast v0, Ljava/lang/String;

    .line 31743
    :goto_0
    return-object v0

    .line 31737
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31739
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31740
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31741
    iput-object v1, p0, Lcom/sec/chaton/a/eg;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31743
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 31762
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31765
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->g:Ljava/lang/Object;

    .line 31766
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31767
    check-cast v0, Ljava/lang/String;

    .line 31775
    :goto_0
    return-object v0

    .line 31769
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31771
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31772
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31773
    iput-object v1, p0, Lcom/sec/chaton/a/eg;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31775
    goto :goto_0
.end method

.method public m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31794
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    return-object v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 31801
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 31655
    invoke-virtual {p0}, Lcom/sec/chaton/a/eg;->q()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 31815
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Lcom/sec/chaton/a/c;
    .locals 1

    .prologue
    .line 31818
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->i:Lcom/sec/chaton/a/c;

    return-object v0
.end method

.method public q()Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 31978
    invoke-static {}, Lcom/sec/chaton/a/eg;->newBuilder()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public r()Lcom/sec/chaton/a/eh;
    .locals 1

    .prologue
    .line 31982
    invoke-static {p0}, Lcom/sec/chaton/a/eg;->a(Lcom/sec/chaton/a/eg;)Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 31655
    invoke-virtual {p0}, Lcom/sec/chaton/a/eg;->r()Lcom/sec/chaton/a/eh;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31907
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 31841
    invoke-virtual {p0}, Lcom/sec/chaton/a/eg;->getSerializedSize()I

    .line 31842
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 31843
    iget-wide v0, p0, Lcom/sec/chaton/a/eg;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 31845
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 31846
    invoke-direct {p0}, Lcom/sec/chaton/a/eg;->s()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31848
    :cond_1
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 31849
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sec/chaton/a/eg;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 31851
    :cond_2
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 31852
    invoke-direct {p0}, Lcom/sec/chaton/a/eg;->t()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31854
    :cond_3
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 31855
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sec/chaton/a/eg;->u()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31857
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 31858
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sec/chaton/a/eg;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 31857
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 31860
    :cond_5
    iget v0, p0, Lcom/sec/chaton/a/eg;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 31861
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sec/chaton/a/eg;->i:Lcom/sec/chaton/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 31863
    :cond_6
    return-void
.end method
