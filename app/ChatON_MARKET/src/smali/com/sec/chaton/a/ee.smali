.class public final Lcom/sec/chaton/a/ee;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/ef;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/ed;",
        "Lcom/sec/chaton/a/ee;",
        ">;",
        "Lcom/sec/chaton/a/ef;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/sec/chaton/a/ej;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32652
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 32787
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    .line 32653
    invoke-direct {p0}, Lcom/sec/chaton/a/ee;->i()V

    .line 32654
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/ee;)Lcom/sec/chaton/a/ed;
    .locals 1

    .prologue
    .line 32647
    invoke-direct {p0}, Lcom/sec/chaton/a/ee;->k()Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32647
    invoke-static {}, Lcom/sec/chaton/a/ee;->j()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 32657
    return-void
.end method

.method private static j()Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32659
    new-instance v0, Lcom/sec/chaton/a/ee;

    invoke-direct {v0}, Lcom/sec/chaton/a/ee;-><init>()V

    return-object v0
.end method

.method private k()Lcom/sec/chaton/a/ed;
    .locals 2

    .prologue
    .line 32689
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->e()Lcom/sec/chaton/a/ed;

    move-result-object v0

    .line 32690
    invoke-virtual {v0}, Lcom/sec/chaton/a/ed;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32691
    invoke-static {v0}, Lcom/sec/chaton/a/ee;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 32694
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/ee;
    .locals 2

    .prologue
    .line 32663
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 32664
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/ee;->b:J

    .line 32665
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/ee;->a:I

    .line 32666
    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    .line 32667
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/ee;->a:I

    .line 32668
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32774
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ee;->a:I

    .line 32775
    iput-wide p1, p0, Lcom/sec/chaton/a/ee;->b:J

    .line 32777
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ee;
    .locals 2

    .prologue
    .line 32733
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 32734
    sparse-switch v0, :sswitch_data_0

    .line 32739
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/ee;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32741
    :sswitch_0
    return-object p0

    .line 32746
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/ee;->a:I

    .line 32747
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/ee;->b:J

    goto :goto_0

    .line 32751
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/a/ej;->newBuilder()Lcom/sec/chaton/a/ek;

    move-result-object v0

    .line 32752
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 32753
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->g()Lcom/sec/chaton/a/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    .line 32755
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 32756
    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ee;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ee;

    goto :goto_0

    .line 32734
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/ed;)Lcom/sec/chaton/a/ee;
    .locals 2

    .prologue
    .line 32714
    invoke-static {}, Lcom/sec/chaton/a/ed;->a()Lcom/sec/chaton/a/ed;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 32721
    :cond_0
    :goto_0
    return-object p0

    .line 32715
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/ed;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 32716
    invoke-virtual {p1}, Lcom/sec/chaton/a/ed;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/ee;->a(J)Lcom/sec/chaton/a/ee;

    .line 32718
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/ed;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32719
    invoke-virtual {p1}, Lcom/sec/chaton/a/ed;->f()Lcom/sec/chaton/a/ej;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/ee;->b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ee;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ee;
    .locals 1

    .prologue
    .line 32795
    if-nez p1, :cond_0

    .line 32796
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32798
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    .line 32800
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ee;->a:I

    .line 32801
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/ee;
    .locals 2

    .prologue
    .line 32672
    invoke-static {}, Lcom/sec/chaton/a/ee;->j()Lcom/sec/chaton/a/ee;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->e()Lcom/sec/chaton/a/ed;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/ee;->a(Lcom/sec/chaton/a/ed;)Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ee;
    .locals 2

    .prologue
    .line 32811
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    invoke-static {}, Lcom/sec/chaton/a/ej;->a()Lcom/sec/chaton/a/ej;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 32813
    iget-object v0, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v0}, Lcom/sec/chaton/a/ej;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/a/ek;->a(Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/ek;->e()Lcom/sec/chaton/a/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    .line 32819
    :goto_0
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/ee;->a:I

    .line 32820
    return-object p0

    .line 32816
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    goto :goto_0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->d()Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->e()Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/ed;
    .locals 1

    .prologue
    .line 32676
    invoke-static {}, Lcom/sec/chaton/a/ed;->a()Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->a()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->a()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->b()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->b()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->b()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->b()Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/ed;
    .locals 2

    .prologue
    .line 32680
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->e()Lcom/sec/chaton/a/ed;

    move-result-object v0

    .line 32681
    invoke-virtual {v0}, Lcom/sec/chaton/a/ed;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32682
    invoke-static {v0}, Lcom/sec/chaton/a/ee;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 32684
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/ed;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 32698
    new-instance v2, Lcom/sec/chaton/a/ed;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/ed;-><init>(Lcom/sec/chaton/a/ee;Lcom/sec/chaton/a/b;)V

    .line 32699
    iget v3, p0, Lcom/sec/chaton/a/ee;->a:I

    .line 32700
    const/4 v1, 0x0

    .line 32701
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 32704
    :goto_0
    iget-wide v4, p0, Lcom/sec/chaton/a/ee;->b:J

    invoke-static {v2, v4, v5}, Lcom/sec/chaton/a/ed;->a(Lcom/sec/chaton/a/ed;J)J

    .line 32705
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 32706
    or-int/lit8 v0, v0, 0x2

    .line 32708
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/ed;->a(Lcom/sec/chaton/a/ed;Lcom/sec/chaton/a/ej;)Lcom/sec/chaton/a/ej;

    .line 32709
    invoke-static {v2, v0}, Lcom/sec/chaton/a/ed;->a(Lcom/sec/chaton/a/ed;I)I

    .line 32710
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 32789
    iget v0, p0, Lcom/sec/chaton/a/ee;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/sec/chaton/a/ej;
    .locals 1

    .prologue
    .line 32792
    iget-object v0, p0, Lcom/sec/chaton/a/ee;->c:Lcom/sec/chaton/a/ej;

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->c()Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0}, Lcom/sec/chaton/a/ee;->c()Lcom/sec/chaton/a/ed;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 32725
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ee;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    check-cast p1, Lcom/sec/chaton/a/ed;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/ee;->a(Lcom/sec/chaton/a/ed;)Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32647
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/ee;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/ee;

    move-result-object v0

    return-object v0
.end method
