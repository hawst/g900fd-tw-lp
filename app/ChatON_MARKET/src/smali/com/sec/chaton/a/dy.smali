.class public final Lcom/sec/chaton/a/dy;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/dz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/dx;",
        "Lcom/sec/chaton/a/dy;",
        ">;",
        "Lcom/sec/chaton/a/dz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/google/protobuf/LazyStringList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20480
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 20620
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    .line 20481
    invoke-direct {p0}, Lcom/sec/chaton/a/dy;->g()V

    .line 20482
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/a/dy;)Lcom/sec/chaton/a/dx;
    .locals 1

    .prologue
    .line 20475
    invoke-direct {p0}, Lcom/sec/chaton/a/dy;->i()Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/chaton/a/dy;
    .locals 1

    .prologue
    .line 20475
    invoke-static {}, Lcom/sec/chaton/a/dy;->h()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 20485
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/dy;
    .locals 1

    .prologue
    .line 20487
    new-instance v0, Lcom/sec/chaton/a/dy;

    invoke-direct {v0}, Lcom/sec/chaton/a/dy;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/chaton/a/dx;
    .locals 2

    .prologue
    .line 20517
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->e()Lcom/sec/chaton/a/dx;

    move-result-object v0

    .line 20518
    invoke-virtual {v0}, Lcom/sec/chaton/a/dx;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20519
    invoke-static {v0}, Lcom/sec/chaton/a/dy;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 20522
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 20622
    iget v0, p0, Lcom/sec/chaton/a/dy;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 20623
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    .line 20624
    iget v0, p0, Lcom/sec/chaton/a/dy;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/dy;->a:I

    .line 20626
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/dy;
    .locals 2

    .prologue
    .line 20491
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 20492
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/a/dy;->b:J

    .line 20493
    iget v0, p0, Lcom/sec/chaton/a/dy;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/dy;->a:I

    .line 20494
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    .line 20495
    iget v0, p0, Lcom/sec/chaton/a/dy;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/dy;->a:I

    .line 20496
    return-object p0
.end method

.method public a(J)Lcom/sec/chaton/a/dy;
    .locals 1

    .prologue
    .line 20607
    iget v0, p0, Lcom/sec/chaton/a/dy;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dy;->a:I

    .line 20608
    iput-wide p1, p0, Lcom/sec/chaton/a/dy;->b:J

    .line 20610
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dy;
    .locals 2

    .prologue
    .line 20570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 20571
    sparse-switch v0, :sswitch_data_0

    .line 20576
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/dy;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20578
    :sswitch_0
    return-object p0

    .line 20583
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/dy;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dy;->a:I

    .line 20584
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/a/dy;->b:J

    goto :goto_0

    .line 20588
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/a/dy;->j()V

    .line 20589
    iget-object v0, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 20571
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dx;)Lcom/sec/chaton/a/dy;
    .locals 2

    .prologue
    .line 20544
    invoke-static {}, Lcom/sec/chaton/a/dx;->a()Lcom/sec/chaton/a/dx;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 20558
    :cond_0
    :goto_0
    return-object p0

    .line 20545
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/dx;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20546
    invoke-virtual {p1}, Lcom/sec/chaton/a/dx;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/a/dy;->a(J)Lcom/sec/chaton/a/dy;

    .line 20548
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/a/dx;->b(Lcom/sec/chaton/a/dx;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20549
    iget-object v0, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 20550
    invoke-static {p1}, Lcom/sec/chaton/a/dx;->b(Lcom/sec/chaton/a/dx;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    .line 20551
    iget v0, p0, Lcom/sec/chaton/a/dy;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/dy;->a:I

    goto :goto_0

    .line 20553
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/a/dy;->j()V

    .line 20554
    iget-object v0, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/chaton/a/dx;->b(Lcom/sec/chaton/a/dx;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public b()Lcom/sec/chaton/a/dy;
    .locals 2

    .prologue
    .line 20500
    invoke-static {}, Lcom/sec/chaton/a/dy;->h()Lcom/sec/chaton/a/dy;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->e()Lcom/sec/chaton/a/dx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/dy;->a(Lcom/sec/chaton/a/dx;)Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->d()Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->e()Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/dx;
    .locals 1

    .prologue
    .line 20504
    invoke-static {}, Lcom/sec/chaton/a/dx;->a()Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->a()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->a()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->b()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->b()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->b()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->b()Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/dx;
    .locals 2

    .prologue
    .line 20508
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->e()Lcom/sec/chaton/a/dx;

    move-result-object v0

    .line 20509
    invoke-virtual {v0}, Lcom/sec/chaton/a/dx;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20510
    invoke-static {v0}, Lcom/sec/chaton/a/dy;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 20512
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/dx;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 20526
    new-instance v2, Lcom/sec/chaton/a/dx;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/dx;-><init>(Lcom/sec/chaton/a/dy;Lcom/sec/chaton/a/b;)V

    .line 20527
    iget v3, p0, Lcom/sec/chaton/a/dy;->a:I

    .line 20528
    const/4 v1, 0x0

    .line 20529
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    .line 20532
    :goto_0
    iget-wide v3, p0, Lcom/sec/chaton/a/dy;->b:J

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/a/dx;->a(Lcom/sec/chaton/a/dx;J)J

    .line 20533
    iget v1, p0, Lcom/sec/chaton/a/dy;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 20534
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v3, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v3}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    .line 20536
    iget v1, p0, Lcom/sec/chaton/a/dy;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/chaton/a/dy;->a:I

    .line 20538
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/dy;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dx;->a(Lcom/sec/chaton/a/dx;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 20539
    invoke-static {v2, v0}, Lcom/sec/chaton/a/dx;->a(Lcom/sec/chaton/a/dx;I)I

    .line 20540
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->c()Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0}, Lcom/sec/chaton/a/dy;->c()Lcom/sec/chaton/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 20562
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dy;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    check-cast p1, Lcom/sec/chaton/a/dx;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/dy;->a(Lcom/sec/chaton/a/dx;)Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20475
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dy;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dy;

    move-result-object v0

    return-object v0
.end method
