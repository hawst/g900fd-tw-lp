.class public final Lcom/sec/chaton/a/dn;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SSMGPB.java"

# interfaces
.implements Lcom/sec/chaton/a/do;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/chaton/a/dm;",
        "Lcom/sec/chaton/a/dn;",
        ">;",
        "Lcom/sec/chaton/a/do;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 547
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 657
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dn;->b:Ljava/lang/Object;

    .line 693
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dn;->c:Ljava/lang/Object;

    .line 548
    invoke-direct {p0}, Lcom/sec/chaton/a/dn;->g()V

    .line 549
    return-void
.end method

.method static synthetic f()Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 542
    invoke-static {}, Lcom/sec/chaton/a/dn;->h()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 552
    return-void
.end method

.method private static h()Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 554
    new-instance v0, Lcom/sec/chaton/a/dn;

    invoke-direct {v0}, Lcom/sec/chaton/a/dn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 558
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 559
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dn;->b:Ljava/lang/Object;

    .line 560
    iget v0, p0, Lcom/sec/chaton/a/dn;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/chaton/a/dn;->a:I

    .line 561
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/a/dn;->c:Ljava/lang/Object;

    .line 562
    iget v0, p0, Lcom/sec/chaton/a/dn;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/chaton/a/dn;->a:I

    .line 563
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 628
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 629
    sparse-switch v0, :sswitch_data_0

    .line 634
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/chaton/a/dn;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 636
    :sswitch_0
    return-object p0

    .line 641
    :sswitch_1
    iget v0, p0, Lcom/sec/chaton/a/dn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dn;->a:I

    .line 642
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dn;->b:Ljava/lang/Object;

    goto :goto_0

    .line 646
    :sswitch_2
    iget v0, p0, Lcom/sec/chaton/a/dn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/dn;->a:I

    .line 647
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/a/dn;->c:Ljava/lang/Object;

    goto :goto_0

    .line 629
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 609
    invoke-static {}, Lcom/sec/chaton/a/dm;->a()Lcom/sec/chaton/a/dm;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 616
    :cond_0
    :goto_0
    return-object p0

    .line 610
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/a/dm;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 611
    invoke-virtual {p1}, Lcom/sec/chaton/a/dm;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dn;->a(Ljava/lang/String;)Lcom/sec/chaton/a/dn;

    .line 613
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/a/dm;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    invoke-virtual {p1}, Lcom/sec/chaton/a/dm;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/a/dn;->b(Ljava/lang/String;)Lcom/sec/chaton/a/dn;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 672
    if-nez p1, :cond_0

    .line 673
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 675
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/a/dn;->a:I

    .line 676
    iput-object p1, p0, Lcom/sec/chaton/a/dn;->b:Ljava/lang/Object;

    .line 678
    return-object p0
.end method

.method public b()Lcom/sec/chaton/a/dn;
    .locals 2

    .prologue
    .line 567
    invoke-static {}, Lcom/sec/chaton/a/dn;->h()Lcom/sec/chaton/a/dn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->e()Lcom/sec/chaton/a/dm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/dn;->a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/chaton/a/dn;
    .locals 1

    .prologue
    .line 708
    if-nez p1, :cond_0

    .line 709
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 711
    :cond_0
    iget v0, p0, Lcom/sec/chaton/a/dn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/chaton/a/dn;->a:I

    .line 712
    iput-object p1, p0, Lcom/sec/chaton/a/dn;->c:Ljava/lang/Object;

    .line 714
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->d()Lcom/sec/chaton/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->e()Lcom/sec/chaton/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/chaton/a/dm;
    .locals 1

    .prologue
    .line 571
    invoke-static {}, Lcom/sec/chaton/a/dm;->a()Lcom/sec/chaton/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->a()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->a()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->b()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->b()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->b()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->b()Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/a/dm;
    .locals 2

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->e()Lcom/sec/chaton/a/dm;

    move-result-object v0

    .line 576
    invoke-virtual {v0}, Lcom/sec/chaton/a/dm;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 577
    invoke-static {v0}, Lcom/sec/chaton/a/dn;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 579
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/chaton/a/dm;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 593
    new-instance v2, Lcom/sec/chaton/a/dm;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/chaton/a/dm;-><init>(Lcom/sec/chaton/a/dn;Lcom/sec/chaton/a/b;)V

    .line 594
    iget v3, p0, Lcom/sec/chaton/a/dn;->a:I

    .line 595
    const/4 v1, 0x0

    .line 596
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 599
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/a/dn;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dm;->a(Lcom/sec/chaton/a/dm;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 601
    or-int/lit8 v0, v0, 0x2

    .line 603
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/a/dn;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/chaton/a/dm;->b(Lcom/sec/chaton/a/dm;Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    invoke-static {v2, v0}, Lcom/sec/chaton/a/dm;->a(Lcom/sec/chaton/a/dm;I)I

    .line 605
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->c()Lcom/sec/chaton/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/a/dn;->c()Lcom/sec/chaton/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 620
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dn;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    check-cast p1, Lcom/sec/chaton/a/dm;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/a/dn;->a(Lcom/sec/chaton/a/dm;)Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/a/dn;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/chaton/a/dn;

    move-result-object v0

    return-object v0
.end method
