.class Lcom/sec/chaton/x;
.super Landroid/os/Handler;
.source "HomeActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/HomeActivity;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/chaton/x;->a:Lcom/sec/chaton/HomeActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/x;->a:Lcom/sec/chaton/HomeActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/HomeActivity;->a(Lcom/sec/chaton/HomeActivity;Z)Z

    .line 71
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    invoke-virtual {v0}, Lcom/sec/chaton/m;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/sec/chaton/x;->a:Lcom/sec/chaton/HomeActivity;

    iget-object v2, p0, Lcom/sec/chaton/x;->a:Lcom/sec/chaton/HomeActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-static {v2, v0}, Lcom/sec/chaton/HomeActivity;->a(Lcom/sec/chaton/HomeActivity;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/x;->a:Lcom/sec/chaton/HomeActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/HomeActivity;->finish()V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/x;->a:Lcom/sec/chaton/HomeActivity;

    const/high16 v1, 0x10a0000

    const v2, 0x10a0001

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/HomeActivity;->overridePendingTransition(II)V

    .line 80
    :cond_0
    return-void
.end method
