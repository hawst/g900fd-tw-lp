.class public Lcom/sec/chaton/ChatONVUpgradeDialog;
.super Lcom/sec/chaton/registration/AbstractUpgradeDialog;
.source "ChatONVUpgradeDialog.java"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;-><init>()V

    .line 15
    const-class v0, Lcom/sec/chaton/ChatONVUpgradeDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/ChatONVUpgradeDialog;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 28
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 29
    const-string v0, "initializeView"

    iget-object v1, p0, Lcom/sec/chaton/ChatONVUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/ChatONVUpgradeDialog;->b(I)V

    .line 32
    invoke-virtual {p0, p0, v2}, Lcom/sec/chaton/ChatONVUpgradeDialog;->a(Landroid/content/Context;I)V

    .line 33
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->onCreate(Landroid/os/Bundle;)V

    .line 20
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 21
    const-string v0, "onCreate"

    iget-object v1, p0, Lcom/sec/chaton/ChatONVUpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/ChatONVUpgradeDialog;->a:Landroid/widget/TextView;

    const v1, 0x7f0b03d4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 24
    return-void
.end method
