.class Lcom/sec/chaton/bv;
.super Landroid/content/BroadcastReceiver;
.source "TabActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 1149
    iput-object p1, p0, Lcom/sec/chaton/bv;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1152
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "buddy_tab_badge_update"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1153
    const-string v0, "receive ACTION_BUDDY_TAB_BADGE_UPDATE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    :cond_0
    :goto_0
    return-void

    .line 1154
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "buddy_tab_name_update"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/sec/chaton/bv;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 1156
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/bv;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/chaton/buddy/BuddyFragment;

    if-eqz v1, :cond_0

    .line 1157
    iget-object v1, p0, Lcom/sec/chaton/bv;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    .line 1158
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->p()V

    .line 1159
    const-string v0, "receive ACTION_BUDDY_TAB_NAME_UPDATE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
