.class public interface abstract Lcom/sec/chaton/e/be;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2975
    const/16 v0, 0xcc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "INSERT "

    aput-object v2, v0, v1

    const-string v1, "INTO "

    aput-object v1, v0, v3

    const-string v1, "relation"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "relation_buddy_id"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "relation_last_msg_time"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ") "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "date(\'NOW\', \'localtime\', \'0 DAY\')"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "last_msg_time"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "send"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "recevied"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "CASE WHEN ("

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "send"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "recevied"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "send"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " , "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "recevied"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "message_date"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "last_msg_time"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "MAX("

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "send"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "),"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "send"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "MAX("

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "recevied"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "),"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "recevied"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "message_date"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "last_msg_time"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "MAX("

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " END) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "send"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, " IN ("

    aput-object v2, v0, v1

    const/16 v1, 0x66

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x68

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, ") THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "MAX("

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, " END) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "recevied"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "SUBSTR("

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "message_time_text"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "1"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "10"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "message_date"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "MAX("

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "message_time_text"

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "last_msg_time"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "COUNT("

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "inbox_buddy_relation"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, " a,"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "message"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, " IN "

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xac

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xae

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "message_time_text"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, " BETWEEN "

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "date(\'NOW\', \'localtime\', \'-7 DAY\') || \'00:00:00\'"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "date(\'NOW\', \'localtime\', \'0 DAY\') || \'99:99:99\'"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, " IN ("

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/be;->a:Ljava/lang/String;

    return-void
.end method
