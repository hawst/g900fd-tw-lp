.class public Lcom/sec/chaton/e/b/o;
.super Lcom/sec/chaton/e/b/a;
.source "PushReceivedTask.java"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/sec/chaton/e/b/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/e/b/a;-><init>(Lcom/sec/chaton/e/b/d;Landroid/os/Handler;)V

    .line 54
    iput-boolean p3, p0, Lcom/sec/chaton/e/b/o;->g:Z

    .line 55
    iput-object p4, p0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    .line 56
    iput-object p6, p0, Lcom/sec/chaton/e/b/o;->f:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    .line 58
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 412
    .line 413
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 415
    :try_start_0
    new-instance v1, Lcom/sec/chaton/util/a;

    invoke-static {p2}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {p2}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/util/a;-><init>([B[B)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_1

    .line 422
    :goto_0
    if-nez v1, :cond_1

    .line 423
    const-string v1, "AesCipher is null"

    sget-object v2, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :goto_1
    return-object v0

    .line 416
    :catch_0
    move-exception v1

    .line 417
    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    move-object v1, v0

    .line 420
    goto :goto_0

    .line 418
    :catch_1
    move-exception v1

    .line 419
    invoke-virtual {v1}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    move-object v1, v0

    goto :goto_0

    .line 427
    :cond_0
    const-string v1, "Fail in getting a key"

    sget-object v2, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 431
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Base64 body : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Decoded body : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/sec/chaton/util/e;->a(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    invoke-static {p1}, Lcom/sec/chaton/util/e;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/a;->c([B)[B

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 17

    .prologue
    .line 62
    new-instance v1, Lcom/sec/chaton/k/a/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/e/b/o;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/sec/chaton/k/a/a;-><init>(Ljava/lang/String;)V

    .line 66
    const-string v2, "push_message_encrypt_feature"

    invoke-static {v2}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 70
    :try_start_0
    const-class v2, Lcom/sec/chaton/io/entry/PushEncryptEntry;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/k/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/PushEncryptEntry;

    .line 72
    iget-object v2, v1, Lcom/sec/chaton/io/entry/PushEncryptEntry;->E:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 74
    iget-object v2, v1, Lcom/sec/chaton/io/entry/PushEncryptEntry;->E:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 75
    iget-object v1, v1, Lcom/sec/chaton/io/entry/PushEncryptEntry;->body:Ljava/lang/String;

    .line 77
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "E : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    if-eqz v2, :cond_2

    .line 80
    const-string v2, "decryptPushMessage with Primary Key"

    sget-object v3, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/b/o;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    .line 83
    if-nez v2, :cond_1

    .line 84
    const-string v2, "decryptPushMessage with Expired Key"

    sget-object v3, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/q;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/b/o;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    .line 86
    if-nez v1, :cond_0

    .line 87
    const-string v1, "decryptPushMessage Fail! "

    sget-object v2, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const/4 v1, 0x0

    .line 407
    :goto_0
    return-object v1

    :cond_0
    move-object v2, v1

    .line 91
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 92
    sget-object v2, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/en;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_2
    new-instance v2, Lcom/sec/chaton/k/a/a;

    invoke-direct {v2, v1}, Lcom/sec/chaton/k/a/a;-><init>(Ljava/lang/String;)V

    .line 97
    const-class v1, Lcom/sec/chaton/io/entry/PushEntry;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/k/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/PushEntry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v5, v1

    .line 118
    :goto_2
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 119
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/n;->k(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    .line 121
    :cond_3
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 122
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 131
    :cond_4
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "old_chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 137
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/chaton/e/b/o;->b:Z

    .line 138
    new-instance v1, Lcom/sec/chaton/e/b/p;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/e/b/o;->g:Z

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    const/4 v12, 0x1

    const-wide/16 v13, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/e/b/p;-><init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 100
    :cond_5
    :try_start_1
    new-instance v1, Lcom/sec/chaton/k/a/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/e/b/o;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/sec/chaton/k/a/a;-><init>(Ljava/lang/String;)V

    .line 101
    const-class v2, Lcom/sec/chaton/io/entry/PushEntry;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/k/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/PushEntry;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 103
    :catch_0
    move-exception v1

    .line 104
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 105
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 109
    :cond_6
    :try_start_2
    const-class v2, Lcom/sec/chaton/io/entry/PushEntry;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/k/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/PushEntry;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v5, v1

    .line 113
    goto/16 :goto_2

    .line 110
    :catch_1
    move-exception v1

    .line 111
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 112
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 152
    :cond_7
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 153
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/chaton/e/b/o;->b:Z

    .line 154
    new-instance v1, Lcom/sec/chaton/e/b/p;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/e/b/o;->g:Z

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    const/4 v12, 0x1

    const-wide/16 v13, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/e/b/p;-><init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 157
    :cond_8
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    const-string v2, "0999"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 158
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 159
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/chaton/e/b/o;->b:Z

    .line 160
    new-instance v1, Lcom/sec/chaton/e/b/p;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/e/b/o;->g:Z

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    const/4 v12, 0x1

    const-wide/16 v13, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/e/b/p;-><init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 165
    :cond_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 167
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->chatType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v7

    .line 171
    if-nez v7, :cond_a

    .line 175
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/chaton/e/b/o;->b:Z

    .line 176
    new-instance v1, Lcom/sec/chaton/e/b/p;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/e/b/o;->g:Z

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    const/4 v12, 0x1

    const-wide/16 v13, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/e/b/p;-><init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 183
    :cond_a
    sget-object v1, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    if-ne v7, v1, :cond_b

    .line 184
    new-instance v1, Lcom/sec/chaton/e/b/p;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/e/b/o;->g:Z

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    const/4 v12, 0x1

    const-wide/16 v13, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/e/b/p;-><init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 188
    :cond_b
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->message:Ljava/lang/String;

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->msgType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v9

    .line 197
    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v1, v9, :cond_d

    .line 198
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->message:Ljava/lang/String;

    .line 199
    invoke-static {v1}, Lcom/sec/chaton/settings/downloads/u;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 200
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_c

    .line 201
    const-string v1, "Received anicon message."

    sget-object v2, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_c
    sget-object v9, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    .line 210
    :cond_d
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 211
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v6, ""

    invoke-virtual {v2, v3, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 212
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    .line 215
    :cond_e
    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    invoke-static {v4, v7, v1, v2}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 217
    const-wide/16 v2, 0x0

    .line 218
    const/4 v12, 0x1

    .line 219
    const-string v1, ""

    .line 220
    const-wide/16 v13, 0x0

    .line 221
    if-eqz v6, :cond_16

    .line 223
    invoke-static {v4, v6, v9, v5, v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/PushEntry;Lcom/sec/chaton/e/r;)Z

    move-result v10

    .line 231
    if-nez v10, :cond_15

    .line 246
    iget-object v8, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 247
    iget-object v11, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    .line 248
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 249
    iput-object v11, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 250
    iput-object v8, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    .line 251
    const/4 v1, 0x1

    iput-boolean v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->isMirror:Z

    .line 255
    :cond_f
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-static {v1, v6, v2}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 257
    iget-boolean v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->isMirror:Z

    if-eqz v2, :cond_11

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    iget-object v3, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 259
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiverCount:Ljava/lang/Integer;

    .line 260
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_10

    .line 261
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Push from No budddies chat room. sender,receiver - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_10
    :goto_3
    invoke-static {v4, v6}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v15

    .line 279
    if-nez v15, :cond_13

    .line 284
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/chaton/e/b/o;->b:Z

    .line 285
    new-instance v1, Lcom/sec/chaton/e/b/p;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/e/b/o;->g:Z

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    const/4 v12, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/e/b/p;-><init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 264
    :cond_11
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 265
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    invoke-static {v4, v6, v1, v2}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 266
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    goto :goto_3

    .line 268
    :cond_12
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 269
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    invoke-static {v4, v6, v1, v2}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 270
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    goto :goto_3

    .line 289
    :cond_13
    iget-wide v2, v15, Lcom/sec/chaton/e/a/p;->a:J

    .line 292
    invoke-static {v4, v15, v7, v5}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/p;Lcom/sec/chaton/e/r;Lcom/sec/chaton/io/entry/PushEntry;)V

    .line 294
    iget-boolean v12, v15, Lcom/sec/chaton/e/a/p;->r:Z

    .line 308
    iget-object v7, v5, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 309
    iget-object v7, v15, Lcom/sec/chaton/e/a/p;->i:Ljava/lang/String;

    iput-object v7, v5, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    .line 313
    :cond_14
    iput-object v8, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 314
    iput-object v11, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    .line 317
    iget-wide v13, v15, Lcom/sec/chaton/e/a/p;->s:J

    move-object v11, v1

    move-wide v15, v2

    .line 405
    :goto_4
    invoke-static {v4}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;)I

    move-result v8

    .line 407
    new-instance v1, Lcom/sec/chaton/e/b/p;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/o;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/e/b/o;->g:Z

    long-to-int v7, v15

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/e/b/p;-><init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 319
    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    move-object v11, v1

    move-wide v15, v2

    goto :goto_4

    .line 331
    :cond_16
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v6

    .line 342
    iget-object v8, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 343
    iget-object v10, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    .line 344
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v11, "chaton_id"

    const-string v12, ""

    invoke-virtual {v1, v11, v12}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v11, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 345
    iput-object v10, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 346
    iput-object v8, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    .line 347
    const/4 v1, 0x1

    iput-boolean v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->isMirror:Z

    .line 350
    :cond_17
    iget-boolean v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->isMirror:Z

    if-eqz v1, :cond_1b

    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    if-eqz v1, :cond_1b

    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    iget-object v11, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 352
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiverCount:Ljava/lang/Integer;

    .line 353
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_18

    .line 354
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Push from No budddies chat room. sender,receiver - "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v11, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v11, Lcom/sec/chaton/e/b/o;->c:Ljava/lang/String;

    invoke-static {v1, v11}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :cond_18
    :goto_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v11, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-static {v1, v6, v11}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 361
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 362
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    .line 381
    :cond_19
    invoke-static {v4, v6, v7, v5}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/io/entry/PushEntry;)Landroid/net/Uri;

    move-result-object v11

    .line 382
    if-eqz v11, :cond_1a

    invoke-virtual {v11}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    const/4 v15, 0x1

    if-le v12, v15, :cond_1a

    .line 383
    invoke-static {v11}, Lcom/sec/chaton/provider/a;->a(Landroid/net/Uri;)Z

    move-result v12

    if-nez v12, :cond_1a

    .line 384
    invoke-static {v11}, Lcom/sec/chaton/e/q;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 390
    :cond_1a
    iput-object v8, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    .line 391
    iput-object v10, v5, Lcom/sec/chaton/io/entry/PushEntry;->receiver:Ljava/lang/String;

    .line 394
    invoke-static {v4, v6, v9, v5, v7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/io/entry/PushEntry;Lcom/sec/chaton/e/r;)Z

    move-result v10

    .line 397
    sget-object v8, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v7, v8, :cond_1c

    .line 398
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v7

    const-string v8, "Setting alert_new_groupchat"

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v7, v8, v11}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    move-object v11, v1

    move-wide v15, v2

    goto/16 :goto_4

    .line 357
    :cond_1b
    iget-object v1, v5, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/e/b/o;->d:Ljava/lang/String;

    invoke-static {v4, v6, v1, v11}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    goto :goto_5

    .line 400
    :cond_1c
    const/4 v12, 0x1

    move-object v11, v1

    move-wide v15, v2

    goto/16 :goto_4
.end method
