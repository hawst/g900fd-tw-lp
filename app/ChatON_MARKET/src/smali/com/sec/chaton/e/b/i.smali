.class public Lcom/sec/chaton/e/b/i;
.super Lcom/sec/chaton/e/b/a;
.source "BuddyGroupAddBuddyTask.java"


# instance fields
.field private c:I

.field private d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/sec/chaton/e/b/d;ILjava/util/Collection;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/e/b/d;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/e/b/a;-><init>(Lcom/sec/chaton/e/b/d;Landroid/os/Handler;)V

    .line 25
    iput p2, p0, Lcom/sec/chaton/e/b/i;->c:I

    .line 26
    iput-object p3, p0, Lcom/sec/chaton/e/b/i;->d:Ljava/util/Collection;

    .line 27
    iput-boolean p4, p0, Lcom/sec/chaton/e/b/i;->e:Z

    .line 28
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 34
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/e/b/i;->c:I

    iget-object v2, p0, Lcom/sec/chaton/e/b/i;->d:Ljava/util/Collection;

    iget-boolean v3, p0, Lcom/sec/chaton/e/b/i;->e:Z

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;ILjava/util/Collection;Z)V

    .line 36
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
