.class public interface abstract Lcom/sec/chaton/e/bi;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1630
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TRIGGER "

    aput-object v1, v0, v3

    const-string v1, "trigger_inbox_before_delete"

    aput-object v1, v0, v4

    const-string v1, " BEFORE DELETE ON "

    aput-object v1, v0, v5

    const-string v1, "inbox"

    aput-object v1, v0, v6

    const-string v1, " BEGIN"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " DELETE FROM "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "inbox_buddy_relation"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " = old."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ";"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " END;"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bi;->a:Ljava/lang/String;

    .line 1635
    const/16 v0, 0x21

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TRIGGER "

    aput-object v1, v0, v3

    const-string v1, "trigger_poll_relation_voter_after_delete"

    aput-object v1, v0, v4

    const-string v1, " BEFORE DELETE ON "

    aput-object v1, v0, v5

    const-string v1, "poll_relation_voter"

    aput-object v1, v0, v6

    const-string v1, " BEGIN"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " DELETE FROM "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "poll_voter"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "poll_voter"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " = old."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, " AND (SELECT COUNT("

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "answer_relation_voter"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ") "

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "answer_relation_voter"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "poll_relation_voter"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " "

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "answer_relation_voter"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "poll_relation_voter"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, ") = 0;"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " END;"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bi;->b:Ljava/lang/String;

    return-void
.end method
