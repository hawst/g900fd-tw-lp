.class public Lcom/sec/chaton/e/b/m;
.super Lcom/sec/chaton/e/b/a;
.source "MessageNotificationUpdateTask.java"


# instance fields
.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/chat/notification/g;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:Lcom/sec/chaton/chat/notification/a;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/sec/chaton/e/b/d;IZ)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/chaton/e/b/a;-><init>(Lcom/sec/chaton/e/b/d;)V

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/e/b/m;->d:Ljava/lang/String;

    .line 35
    iput p2, p0, Lcom/sec/chaton/e/b/m;->f:I

    .line 36
    iput-boolean p3, p0, Lcom/sec/chaton/e/b/m;->g:Z

    .line 37
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/e/b/m;->c:Ljava/util/ArrayList;

    .line 41
    return-void
.end method

.method private b()Z
    .locals 25

    .prologue
    .line 61
    .line 63
    const-string v2, ""

    .line 64
    const-string v2, ""

    .line 65
    const-string v2, ""

    .line 71
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "inbox_no"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "inbox_unread_count"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "inbox_chat_type"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "inbox_session_id"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "inbox_server_ip"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "inbox_server_port"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "lasst_session_merge_time"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "inbox_participants"

    aput-object v6, v4, v5

    const-string v5, "inbox_unread_count > 0"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 80
    if-nez v21, :cond_1

    .line 81
    const/4 v2, 0x0

    .line 142
    :goto_0
    return v2

    .line 121
    :cond_0
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 83
    :cond_1
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 84
    const-string v2, "inbox_no"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 85
    const-string v2, "inbox_server_ip"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 86
    const-string v2, "inbox_server_port"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 87
    const-string v2, "inbox_session_id"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 88
    const-string v2, "lasst_session_merge_time"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 89
    const-string v2, "inbox_participants"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 91
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "inbox_unread_count"

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/v;->b(I)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "message_inbox_no=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v7, v6, v8

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 92
    if-nez v22, :cond_2

    .line 93
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 94
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 97
    :cond_2
    :goto_1
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    const-string v2, "message_content"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 99
    const-string v2, "\n"

    invoke-virtual {v5, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 101
    const-string v2, "buddy_name"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\n+"

    const-string v4, " "

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 103
    new-instance v2, Lcom/sec/chaton/chat/notification/g;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "message_sender"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v6, "message_inbox_no"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "message_time"

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string v9, "inbox_chat_type"

    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-string v10, "message_content_type"

    move-object/from16 v0, v22

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v22

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v17, "message_is_truncated"

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const-string v18, "message_sever_id"

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    const-string v23, "mixed"

    const/16 v24, 0x0

    aget-object v20, v20, v24

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    const/16 v20, 0x1

    :goto_2
    invoke-direct/range {v2 .. v20}, Lcom/sec/chaton/chat/notification/g;-><init>(Lcom/sec/chaton/chat/notification/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;IJILjava/lang/String;JZ)V

    .line 119
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/e/b/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 103
    :cond_3
    const/16 v20, 0x0

    goto :goto_2

    .line 124
    :cond_4
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/e/b/m;->c:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/e/b/n;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/chaton/e/b/n;-><init>(Lcom/sec/chaton/e/b/m;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 138
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/e/b/m;->c()Z

    move-result v2

    if-nez v2, :cond_5

    .line 139
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 142
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private c()Z
    .locals 9

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget v1, v1, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/notification/a;->e(I)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x0

    .line 190
    :goto_0
    return v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/sec/chaton/chat/notification/g;

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 158
    new-instance v0, Lcom/sec/chaton/chat/notification/i;

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->j:Ljava/lang/String;

    iget v2, v7, Lcom/sec/chaton/chat/notification/g;->k:I

    iget-object v3, v7, Lcom/sec/chaton/chat/notification/g;->i:Ljava/lang/String;

    iget v4, v7, Lcom/sec/chaton/chat/notification/g;->h:I

    iget-wide v5, v7, Lcom/sec/chaton/chat/notification/g;->l:J

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/chat/notification/i;-><init>(Ljava/lang/String;ILjava/lang/String;IJ)V

    .line 159
    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    iget-object v2, v7, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[NOTIPannel] parseCursor inboxList: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/a;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v2, v7, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 166
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_3

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[NOTIPannel] get buddy Info ret = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/e/b/m;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_3
    if-gez v0, :cond_5

    .line 171
    iget-object v0, v7, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 172
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    .line 174
    :cond_4
    new-instance v0, Lcom/sec/chaton/chat/notification/e;

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    iget-object v2, v7, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    iget-object v3, v7, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/notification/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_5

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[NOTIPannel] parseCursor buddyInfoList: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[NOTIPannel] parseCursor LatestMsgList: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget-object v1, v1, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sender :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sender name :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "msgID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/e/b/m;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 188
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/e/b/m;->c:Ljava/util/ArrayList;

    .line 190
    const/4 v0, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/chaton/e/b/m;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/e/b/m;->e:Lcom/sec/chaton/chat/notification/a;

    iget v1, p0, Lcom/sec/chaton/e/b/m;->f:I

    iget-boolean v2, p0, Lcom/sec/chaton/e/b/m;->g:Z

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(IZ)V

    .line 55
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    return-object v0

    .line 51
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 52
    const-string v0, "noti is null"

    const-string v1, "MessageNotificationUpdateTask"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
