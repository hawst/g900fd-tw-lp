.class public final enum Lcom/sec/chaton/e/aj;
.super Ljava/lang/Enum;
.source "ChatONContract.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/e/aj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/e/aj;

.field public static final enum b:Lcom/sec/chaton/e/aj;

.field public static final enum c:Lcom/sec/chaton/e/aj;

.field public static final enum d:Lcom/sec/chaton/e/aj;

.field public static final enum e:Lcom/sec/chaton/e/aj;

.field public static final enum f:Lcom/sec/chaton/e/aj;

.field public static final enum g:Lcom/sec/chaton/e/aj;

.field public static final enum h:Lcom/sec/chaton/e/aj;

.field public static final enum i:Lcom/sec/chaton/e/aj;

.field public static final enum j:Lcom/sec/chaton/e/aj;

.field private static final synthetic l:[Lcom/sec/chaton/e/aj;


# instance fields
.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 40
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "UNKNOWN"

    const/16 v2, -0x63

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    .line 41
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "INVITE"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    .line 42
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "ENTER"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    .line 43
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "LEAVE"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    .line 44
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "INVALID_USER"

    invoke-direct {v0, v1, v8, v7}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->e:Lcom/sec/chaton/e/aj;

    .line 45
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "MEMBER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->f:Lcom/sec/chaton/e/aj;

    .line 46
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "MARK"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->g:Lcom/sec/chaton/e/aj;

    .line 47
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "DEACTIVATED"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->h:Lcom/sec/chaton/e/aj;

    .line 48
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "TRANSLATE_ON"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->i:Lcom/sec/chaton/e/aj;

    .line 49
    new-instance v0, Lcom/sec/chaton/e/aj;

    const-string v1, "TRANSLATE_OFF"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/aj;->j:Lcom/sec/chaton/e/aj;

    .line 39
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/chaton/e/aj;

    sget-object v1, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/e/aj;->e:Lcom/sec/chaton/e/aj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/e/aj;->f:Lcom/sec/chaton/e/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/e/aj;->g:Lcom/sec/chaton/e/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/e/aj;->h:Lcom/sec/chaton/e/aj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/e/aj;->i:Lcom/sec/chaton/e/aj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/e/aj;->j:Lcom/sec/chaton/e/aj;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/e/aj;->l:[Lcom/sec/chaton/e/aj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54
    iput p3, p0, Lcom/sec/chaton/e/aj;->k:I

    .line 55
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/e/aj;
    .locals 1

    .prologue
    .line 62
    packed-switch p0, :pswitch_data_0

    .line 82
    sget-object v0, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 66
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 68
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 70
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/e/aj;->e:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 72
    :pswitch_4
    sget-object v0, Lcom/sec/chaton/e/aj;->f:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 74
    :pswitch_5
    sget-object v0, Lcom/sec/chaton/e/aj;->g:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 76
    :pswitch_6
    sget-object v0, Lcom/sec/chaton/e/aj;->h:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 78
    :pswitch_7
    sget-object v0, Lcom/sec/chaton/e/aj;->i:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 80
    :pswitch_8
    sget-object v0, Lcom/sec/chaton/e/aj;->j:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;
    .locals 1

    .prologue
    .line 86
    const-string v0, "LEAVE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    sget-object v0, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    .line 114
    :goto_0
    return-object v0

    .line 89
    :cond_0
    const-string v0, "ENTER"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    sget-object v0, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 92
    :cond_1
    const-string v0, "INVITE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    sget-object v0, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 95
    :cond_2
    const-string v0, "INVALID_USER"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 96
    sget-object v0, Lcom/sec/chaton/e/aj;->e:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 98
    :cond_3
    const-string v0, "MEMBER"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 99
    sget-object v0, Lcom/sec/chaton/e/aj;->f:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 101
    :cond_4
    const-string v0, "MARK"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 102
    sget-object v0, Lcom/sec/chaton/e/aj;->g:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 104
    :cond_5
    const-string v0, "DEACTIVATED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 105
    sget-object v0, Lcom/sec/chaton/e/aj;->h:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 107
    :cond_6
    const-string v0, "TRANSLATED_ON"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 108
    sget-object v0, Lcom/sec/chaton/e/aj;->i:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 110
    :cond_7
    const-string v0, "TRANSLATED_OFF"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 111
    sget-object v0, Lcom/sec/chaton/e/aj;->j:Lcom/sec/chaton/e/aj;

    goto :goto_0

    .line 114
    :cond_8
    sget-object v0, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/e/aj;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/chaton/e/aj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/aj;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/e/aj;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/chaton/e/aj;->l:[Lcom/sec/chaton/e/aj;

    invoke-virtual {v0}, [Lcom/sec/chaton/e/aj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/e/aj;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/chaton/e/aj;->k:I

    return v0
.end method
