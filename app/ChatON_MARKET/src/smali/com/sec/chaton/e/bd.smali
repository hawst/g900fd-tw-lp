.class public interface abstract Lcom/sec/chaton/e/bd;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2918
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "p."

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v5

    const-string v1, ", CASE WHEN (b."

    aput-object v1, v0, v6

    const-string v1, "buddy_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " IS NOT NULL ) THEN b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " ELSE p."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " END AS joined_name, p."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "poston"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "poston"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, " p LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " b ON p."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " = b."

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bd;->a:Ljava/lang/String;

    .line 2922
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "p."

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v5

    const-string v1, ", CASE WHEN (b."

    aput-object v1, v0, v6

    const-string v1, "buddy_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " IS NOT NULL ) THEN b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " ELSE p."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " END AS joined_name, p."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " AS joined_no, p."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "poston"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "poston_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "multimedia_list"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "my_poston"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, " p LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, " b ON p."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " = b."

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bd;->b:Ljava/lang/String;

    .line 2927
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "p."

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v5

    const-string v1, ", CASE WHEN (b."

    aput-object v1, v0, v6

    const-string v1, "buddy_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " IS NOT NULL ) THEN b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " ELSE p."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " END AS joined_name, p."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " AS joined_no, p."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "poston"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "poston_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "multimedia_list"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "buddy_poston"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, " p LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, " b ON p."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " = b."

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bd;->c:Ljava/lang/String;

    .line 2933
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "c."

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v5

    const-string v1, ", CASE WHEN (b."

    aput-object v1, v0, v6

    const-string v1, "buddy_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " IS NOT NULL ) THEN b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " ELSE c."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " END AS joined_name, c."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "comment"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ", c."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ", c."

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", c."

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "commentid"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "poston_comments"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " c LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, " b ON c."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " = b."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bd;->d:Ljava/lang/String;

    .line 2939
    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "p."

    aput-object v1, v0, v4

    const-string v1, "poston_no"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "poston"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " AS joined_my_poston"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " AS joined_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " AS joined_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " AS joined_my_poston_time"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " AS joined_my_poston_comment_read"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " AS joined_my_poston_comment_unread"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "poston_metaid"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "poston_metatype"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "poston_url"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "poston_seq"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "my_multimedia_poston"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " p LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "my_poston"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, " b ON p."

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "poston_no"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " = b."

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "poston_id"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bd;->e:Ljava/lang/String;

    .line 2952
    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "p."

    aput-object v1, v0, v4

    const-string v1, "poston_no"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "poston"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " AS joined_buddy_poston"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " AS joined_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " AS joined_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " AS joined_buddy_poston_time"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " AS joined_buddy_poston_comment_read"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " AS joined_buddy_poston_comment_unread"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "poston_metaid"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "poston_metatype"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "poston_url"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ", p."

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "poston_seq"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_multimedia_poston"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " p LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "buddy_poston"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, " b ON p."

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "poston_no"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " = b."

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "poston_id"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bd;->f:Ljava/lang/String;

    return-void
.end method
