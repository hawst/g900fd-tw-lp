.class public interface abstract Lcom/sec/chaton/e/bj;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2897
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "UPDATE "

    aput-object v1, v0, v4

    const-string v1, "message"

    aput-object v1, v0, v3

    const-string v1, " SET "

    aput-object v1, v0, v5

    const-string v1, "message_read_status"

    aput-object v1, v0, v6

    const-string v1, "="

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " CASE WHEN "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "message_read_status"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " < ? THEN 0 "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " ELSE ("

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "message_read_status"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "-?)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " END WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "=?"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bj;->a:Ljava/lang/String;

    .line 2901
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "UPDATE "

    aput-object v1, v0, v4

    const-string v1, "message"

    aput-object v1, v0, v3

    const-string v1, " SET "

    aput-object v1, v0, v5

    const-string v1, "message_read_status"

    aput-object v1, v0, v6

    const-string v1, " = 0 WHERE "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " = ? AND "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " = ? AND "

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " <= (SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "message"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " = ?)"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bj;->b:Ljava/lang/String;

    return-void
.end method
